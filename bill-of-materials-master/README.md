# Bill of Materials

This project is intended to gather the bills of materials and the documentation of its items.

The Bill of Material created by Bevatech for WP 9.4 have been gathered in the injector BOM.xlsm file. A tree was created and the available documents linked to the items of this BoM.

**Colums:**

* *lvl* sets the hierachy level of the item in the tree
* *item#* gives the reference of an item within its group
* *item ref* is the reference used by the designer or the manufacturer
* *item desc* is the description of this item
* *qty* is the quantity of this item with its group
* *mat* is the material used to manufacture this item
* *doc1*, *doc2*,... are the documents related to this item


Items having the same level in the tree are grouped to facilitate the navigation in the tree.

**Macros:**

* *FormatStructure* This macro is used to structure the document following the *lvl* set. It indents the 3rd column to ease the readability of the BoM, it colors the rows and finally it groups the items of the same level.
The first item of the structure (BoM) has to be selected to start this macro.
* *FindDocs* This macro is used to find the documents related to the selected elements. It seeks in the foler ./documents all the files that have a similar name. A relative link is then created in the "doc1" column. If multiple files are found, the links are stored in the following columns.
* *FileList* This macro lists all the files in the ./documents folder.

Each user is invited to improve this BoM and create a local copy for its own usage. Note that, as it works with Excel files and not ASCII files, it may be very difficult to gather changes done by multiple users at the same time.

The password to edit the macro is 'm~Akw5WjA4dDd%uP=!BhZ--


## Ancillary software

Two software modules have been created:

- BoM.py
- BoM2Kroki.py

The first one reads an excel sheet structured as above described and produces equivalent data structures (a JSON string and a Python list). A dictionary is also created, which associates system components to their reference diagrams.

The second code converts the data structures produced by the first code into a PlantUML and a PyDot diagram. The output diagram is then transfered on tdr.myrrha.lan, where it can be visualized [here](http://tdr.myrrha.lan/vfolders/bill-of-materials/output.svg). 
Note that a few of the nodes of this graph are hypertextual links to their associated diagrams.
