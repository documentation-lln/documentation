#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Name:        BoM
# Category:    Knowledge extractor
# Description: converts the excel bill-of-material, as structured by Jorik, into a Python data structure
# Maturity:    Syntactically correct (data structure can be imported). Not tested yet.
# Author:      vdflorio
version =     "0.7"
# Version:     0.7  / Corrected bug in list creation        / Do 10 Jun 2021 09:41:14 CEST
# Version:     0.6  / Records the whole excel. Outputs json / Mi 12 Mai 2021 13:42:31 CEST
# Version:     0.5  / Corrected list production algorithm   / Mi 12 Mai 2021 10:53:20 CEST
# Version:     0.4  / Added URLs to documents               / Di 11 Mai 2021 13:49:20 CEST
# Version:     0.3  / Added instance field (row[10])        / Di 11 Mai 2021 08:59:13 CEST
# Version:     0.2  / Syntactically correct                 / Mo 10 Mai 2021 09:33:55 CEST
# History:     0.1  / Incomplete and incorrect              / Sa 08 Mai 2021 16:46:51 CEST


from sys import argv
from os import  system
import collections
import openpyxl
import pathlib
from pathlib import Path
from termcolor import colored
import urllib.parse


# Centos:
# sudo yum install python-pip

class CreatElem:
    def __init__(self, row):
        self.wor  = row
        self.tsil = []
    def getRow(self):
        return self.wor
    def getList(self):
        return self.tsil
    def append(self, x):
        self.tsil.append(x)
        

class stack:
    def __init__(self):
        self.sp = -1;
        self.tsil = []
    def push(self, x):
        self.sp += 1
        self.tsil.append(x)
    def nelem(self):
        return sp
    def top(self):
        if self.sp >= 0:
            return self.tsil[-1]
        raise Exception('No elements on this stack -- cannot top()')
        return None

    def pop(self):
        self.sp -= 1
        if self.sp >= 0:
            return self.tsil.pop()
        raise Exception('No elements on this stack -- cannot pop()')
        return None

def fieldnamex(row):
    try:
        link = row[6].hyperlink.target
    except:
        link = ""

    s = row[2].value
    if (isinstance(s, str)):
        s = s.replace('-', '_')
    if row[10].value != None:
        return "'{}_I{}', '{}'".format(s, row[10].value, link)
    return "'{}', '{}'".format(s, link)

def fieldname(row):
    try:
        # link = row[6].hyperlink.target
        p = pathlib.PureWindowsPath(row[6].hyperlink.target)
        link = p.as_posix()
    except:
        link = ""

    s = row[2].value
    if (isinstance(s, str)):
        s = s.replace('-', '_')
    if row[10].value != None:
        return "'{}_I{}', '{}', {}".format(s, row[10].value, link, [cell.value for cell in row[1:5]])
    return "'{}', '{}', {}".format(s, link, [cell.value for cell in row[1:5]])

def old_fieldname(row):
    if row[10].value != None:
        return '{}-{}'.format(row[2].value,row[10].value)
    return row[2].value

verbose=False

if len(argv) != 2:
    xlsx_file = Path('.', 'injector BOM-test.xlsm')
else:
    xlsx_file = Path('.', argv[1])

print(colored("...Processing sheet '{}'".format(xlsx_file),'green'))

wb_obj = openpyxl.load_workbook(xlsx_file) 

# Read the active sheet:
sheet = wb_obj.active

rows=sheet.max_row
cols=sheet.max_column

# First row is sheet[1]
# First elem of that row is sheet[1][0]
# >>> print(sheet[1][0].value)
# lvl

with open("list_representation.py", 'w') as f:
    f.write("from json import dumps\n\n")
    f.write("# BoM is a recursive data structure with the full bill-of-material hierarchy in the input Excel\n")
    f.write("BoM = ('{}', '', [], [\n".format(sheet[2][2].value))
    prev_level = sheet[2][0].value
    first = True
    for i, row in enumerate(sheet.iter_rows(min_row=3)):
        new_level = row[0].value

        if new_level == None:
            continue

        # for j in range(new_level*2):
        #     f.write(' ')

        if   new_level > prev_level:
            # newstr= "({}, [\n".format(fieldname(row))
            for j in range(new_level*2):
                f.write(' ')
            f.write("({}, [\n".format(fieldname(row)))

        elif new_level == prev_level:
            f.write("]),\n")
            for j in range(new_level*2):
                f.write(' ')
            f.write("({}, [".format(fieldname(row)))

        else:
            pn = prev_level - new_level + 1
            for j in range(pn):
                f.write('])')

            # for j in range(n):
            #     f.write('])')
            # newstr= "])]), ({}, [".format(fieldname(row))
            # f.write('])]),\n')
            for j in range(new_level*2):
                f.write(' ')
            f.write(', ({}, ['.format(fieldname(row)))

        prev_level = new_level
    # print(new_level)
    # print(lvl)
    # for i in range(new_level):
    for i in range(prev_level):
        f.write("]) ")
    f.write("\njBoM = dumps(BoM)\n")
    f.write("\n\n")

    seen = {}
    f.write("# BoM_URLs is a dictionary that associates a system component to its reference document\n")
    f.write("BoM_URLs = {\n")
    for row in sheet.iter_rows(min_row=3):
        try:
            # link = row[6].hyperlink.target
            link = row[6].hyperlink.display
            # print(row[6].hyperlink)
            # print('target ', row[6].hyperlink.target)
            # print('display ', row[6].hyperlink.display)
            # sys.exit(1)
            p = pathlib.PureWindowsPath(row[6].hyperlink.target)
            link = p.as_posix()
        except:
            link = None
        if link:
            s = row[2].value
            s = s.replace('-', '_')
            if not s in seen:
                seen[s] = True
                # slink = urllib.parse.urlsplit(link)
                slink = link
                if row[10].value == None:
                    f.write("'{}' : r'{}',\n".format(s, link))
                    # f.write("'{}' : r'{}',\n".format(s, slink.path))
                else:
                    f.write("'{}_I{}' : r'{}',\n".format(s, row[10].value, link))
                    # f.write("'{}_I{}' : r'{}',\n".format(s, row[10].value, slink.path))
    f.write("}\n")

print('list created.')

from list_representation import jBoM

with open("list_representation.plantuml.json", 'w') as f:
    f.write('@startjson\n')
    f.write(jBoM)
    f.write('\n@endjson\n')

with open("list_representation.json", 'w') as f:
    f.write(jBoM)

# system('plantuml list_representation.plantuml.json')

print(colored("...done (output=list_representation.py, list_representation.json, list_representation.plantuml.json).",'green'))
#
# Eof (BoM.py)
#
