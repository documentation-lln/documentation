#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Name:           BoM2Kroki
# Category:       Knowledge consumer
# Description:    converts the Python data structure produced by BoM.py into a PlantUML diagram and/or a PyDot diagram
# Maturity:       Moderate (no verification that the produced graph is compliant to the Excel)
# Author:         vdflorio
version =         "0.10"
# Date:           Do 20 Mai 2021 19:42:54 CEST
#
# Note:           From version 0.7, option -p creates a plantuml source, and option -d creates a pydot source
# Note(plantuml): command `export PLANTUML_LIMIT_SIZE=100000` is required to guarantee that the output file
#                 produced by plantuml is not truncated
#                 Generate a PNG graph and CMAPX hyperlinks via
#                 `plantuml bom.kroki` (which is equivalent to `java -jar ~/bin/plantuml.jar bom.kroki`)
#                 More info on CMAPX at http://www.graphviz.org/doc/info/output.html#d:cmapx
# Note(pydot):    Generate a PNG graph via `python bom.pydot`
#                 Output graph is in "output.png"


import sys, os
import getopt
import collections
import openpyxl
from   pathlib import Path
import pydot    # https://github.com/pydot/pydot
import urllib.parse
from   urllib.parse import quote

# from paste.util.multidict import MultiDict
from collections import defaultdict
from termcolor import colored

encountered = {}
pydot_encountered = {}
nodes = defaultdict(set)
pydot_nodes = defaultdict(set)

node_num = 0

def scan(bom, f, parent):
    try:
        (title, the_url, the_row, the_list) = bom
    except:
        print('len(bom) = {}, bom[2] = "{}", parent = {}\n'.format(len(bom), bom[2], parent))
        return

    for elem in the_list:
        scan(elem, f, title)

    # after the scan of your list, create the association between parent and title
    if not title in nodes[parent] and title != parent and title != None:
        f.write('[{}] --> [{}]\n'.format(parent, title))
        nodes[parent].add(title)

def pydot_scan(bom, f, parent):
    try:
        (title, the_url, the_row, the_list) = bom
    except:
        print('len(bom) = {}, bom[2] = "{}", parent = {}\n'.format(len(bom), bom[2], parent))
        return

    global node_num

    if not parent in pydot_encountered:
        node_num += 1
        pydot_encountered[parent] = node_num
        f.write("graph.add_node(pydot.Node('n{node_num}', label='{label}'))\n".format(node_num=node_num, label=parent))

    for elem in the_list:
        pydot_scan(elem, f, title)

    if not title in pydot_encountered:
        node_num += 1
        pydot_encountered[title] = node_num

        # url = quote(the_url).replace('\\','/') 
        url = the_url.replace('\\','/') 
        # url =  r"{}".format(the_url)
        f.write(f"graph.add_node(pydot.Node('n{node_num}', label='{title}', URL='{url}'))\n")

    # after the scan of your list, create the association between parent and title
    if not title in pydot_nodes[parent] and title != parent and title != None:
        pydot_nodes[parent].add(title)
        f.write("graph.add_edge(pydot.Edge('n{}', 'n{}', color='blue'))\n".format(pydot_encountered[parent],pydot_encountered[title]))


def prologue(bom, f, root):
    f.write('@startuml\n\n')
    f.write('package "{}" {{\n[{}]\n}}\n\n'.format(root, bom[0]))

def pydot_prologue(bom, f, root):
    f.write("import pydot\nimport os\n\n")
    f.write("graph = pydot.Dot('{}', graph_type='graph', bgcolor='yellow')\n\n".format(root)) #, bom[0]))

def epilogue(url_dictionary, f, root):
    f.write('\n')
    for key in url_dictionary:
        f.write('\turl of {} is [[{}]]\n'.format(key, url_dictionary[key]))
    f.write('@enduml\n\n')

def pydot_epilogue(url_dictionary, f, root, scp=False):
    f.write("\ngraph.write_png('output.png')\n")
    f.write("\ngraph.write_svg('output.svg')\n")
    if scp:
        f.write("\nos.system('scp output.svg admin@10.5.3.18:/home/admin/public_html/vfolders/bill-of-materials')\n")

def helper():
    print ('Bom2Kroki.py    -i <inputfile> -o <outputfile> -p -d -x -h')
    print ('In more detail: -i : list representation module')
    print ('                -o : graph creator code')
    print ('                -p : output PlantUML graph creation code')
    print ('                -d : output PyDot graph creation code')
    print ('                -s : scp the output (svg) graph on tdr.myrrha.lan')
    print ('                     (implicitly sets -x)')
    print ('                -x : execute the graph creation code(s)')
    print ('                -h : this help.')

def main(argv):
    try:
        opts, args = getopt.getopt(argv,"hpdsxi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
        helper()
        sys.exit(2)

    plantuml = False
    dot = False
    execute = False
    scp = False
    inputfile = "list_representation.py"
    outputfile = "bom.kroki"

    for opt, arg in opts:
        if   opt == '-p':
            plantuml = True
        elif opt == '-d':
            dot = True
        elif opt == '-x':
            execute = True
        elif opt == '-s':
            scp = True
        elif opt == '-h':
            helper()
            sys.exit(0)
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg

    if dot or plantuml:
        print(colored(f"Processing {inputfile}...", 'green'))
        print(colored("   ...Importing  list_representation.py...", 'yellow'))
        from list_representation import BoM, BoM_URLs

    if scp:
        execute = True

    if plantuml:
        print(colored("   ...Constructing the PlantUML graph code...", 'yellow'))
        with open(f"{outputfile}", 'w') as f:
            prologue(BoM, f, 'Bill-of-materials')
            scan(BoM, f, 'Injector')
            epilogue(BoM_URLs, f, 'Injector')

    if dot:
        print(colored("   ...Constructing the PyDot graph code...", 'yellow'))
        with open("bom.pydot", 'w') as f:
            pydot_prologue(BoM, f, 'Bill-of-materials')
            pydot_scan(BoM, f, 'Injector')
            pydot_epilogue(BoM_URLs, f, 'Injector', scp=scp)

    if execute:
        if plantuml:
            print(colored("   ...Executing the PlantUML graph code...", 'yellow'))
            os.system(f"plantuml {outputfile}")
        if dot:
            print(colored("   ...Executing the PyDot graph code...", 'yellow'))
            os.system("python bom.pydot")

    if dot or plantuml:
        print(colored('...done (output=', 'green'), end='')
        if plantuml:
            print(colored(f"{outputfile}", 'green'), end='')
            if dot:
                print(colored(', ', 'green'), end='')
        if dot:
            print(colored("bom.pydot", 'green'), end='')
        print(colored(').','green'))
    else:
        print(colored("Error: either of -p or -d must be specified.", 'red'))


if __name__ == "__main__":
    # execute only if run as a script
    main(sys.argv[1:])

#
# Eof (BoM2Kroki.py)
#
