#!/usr/bin/env bash

# call as NUMBERmS, e.g. 2345ms

spaces () {
	local i
	for i in `seq 1 $1` ; do
		printf ' '
	done
}

milliTime () {
	local sec msec info num_scale inp

	if [[ "$1" == *ms ]] ; then
		inp=${1%"ms"}
		if [ "$inp" -lt "1000" ]; then
			echo ${inp}ms
			return
		fi
	fi

	sed 's/\([[:digit:]]\{3\}\)ms/ \1ms/' <<< "$1" | while read sec msec info; do
	for scale in 86400 3600 60 1; do
			num_scale=$((sec / scale))
			sec=$((sec - (scale * num_scale)))

			if [[ $num_scale -gt 0 ]] ; then
				echo -n "$num_scale"
				case $scale in
					1)
					echo -n "s"
					;;
					60)
					echo -n "m"
					;;
					3600)
					echo -n "h"
					;;
					*)
					echo -n "d"
					;;
				esac
			fi
		done
		# echo "$msec $info"
		echo "${msec}"
	done
}

# Copyright http://stackoverflow.com/a/7400673/257479
myreadlink() { [ ! -h "$1" ] && echo "$1" || (local link="$(expr "$(command ls -ld -- "$1")" : '.*-> \(.*\)$')"; cd $(dirname $1); myreadlink "$link" | sed "s|^\([^/].*\)\$|$(dirname $1)/\1|"); }
whereis() { echo $1 | sed "s|^\([^/].*/.*\)|$(pwd)/\1|;s|^\([^/]*\)$|$(which -- $1)|;s|^$|$1|"; } 
whereis_realpath() { local SCRIPT_PATH=$(whereis $1); myreadlink ${SCRIPT_PATH} | sed "s|^\([^/].*\)\$|$(dirname ${SCRIPT_PATH})/\1|"; } 

script_root=$(dirname $(whereis_realpath "$0"))
