/*******************************************************************************
 * vdfio.h
 * 
 * standard definitions for the documentation's knowledge extractors and tools
 *
 * by vdflorio (vdflorio@sck.be)
 *
 * Versions:
 * 0.2 20201118T1058
 * 0.1 20201030T1120
 *
 *******************************************************************************/

#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

#define TRUE	(1)
#define FALSE	(0)

/* ANSI terminals color codes */
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

// Bold, underlined, reversed
#define KBLD  "\x1B[1m"
#define KUND  "\x1B[4m"
#define KREV  "\x1B[7m"

#define SETRED     puts(KRED)
#define SETGREEN   puts(KGRN)
#define SETYELLOW  puts(KYEL)
#define SETBLUE    puts(KBLU)
#define SETMAGENTA puts(KMAG)
#define SETCYAN    puts(KCYN)
#define SETWHITE   puts(KWHT)
#define SETNORMAL  puts(KNRM)

// Background colors
#define NRM   "\x1B[0m"
#define bBLK  "\x1B[40m"
#define bRED  "\x1B[41m"
#define bGRN  "\x1B[42m"
#define bYEL  "\x1B[43m"
#define bBLU  "\x1B[44m"
#define bMAG  "\x1B[45m"
#define bCYN  "\x1B[46m"
#define bWHT  "\x1B[47m"

// Background, bright version
#define BBLK  "\x1B[40;1m"
#define BRED  "\x1B[41;1m"
#define BGRN  "\x1B[42;1m"
#define BYEL  "\x1B[43;1m"
#define BBLU  "\x1B[44;1m"
#define BMAG  "\x1B[45;1m"
#define BCYN  "\x1B[46;1m"
#define BWHT  "\x1B[47;1m"

#define ERR_NONE         (0)
#define ERR_CANT_ACCESS  (-1)
#define ERR_WRONG_ARG    (-2)
#define ERR_RANGE        (-3)
#define ERR_MISSING_ARG  (-4)
#define ERR_OPTION_CLASH (-5)

#define WARN_TRAILING_CHARS (0)

#define S_ERR_NONE		"Processing concluded normally"
#define S_ERR_CANT_ACCESS 	"Cannot access a file"
#define S_ERR_WRONG_ARG		"Undefined command line option, or wrong argument number"
#define S_ERR_RANGE		"Illegal value for a parameter"
#define S_ERR_MISSING_ARG	"Missing value for a command line option"
#define S_ERR_OPTION_CLASH	"Options specified clash with each other"

#define S_WARN_TRAILING_CHARS	"Command line argument has trailing characters"

static
const char *ErrMsgs[] = { S_ERR_NONE, S_ERR_CANT_ACCESS,
			  S_ERR_WRONG_ARG, S_ERR_RANGE,
			  S_ERR_MISSING_ARG, S_ERR_OPTION_CLASH,
			};
static
const char *WarnMsgs[] = { S_WARN_TRAILING_CHARS,
			 };

#define verror(i)	{ if (i==0) \
				fprintf(stderr, "%sNo errors: %s%s.\n", KGRN, ErrMsgs[-i], KNRM); \
			  else \
				fprintf(stderr, "%sError code: %d%s. %sError message: %s%s.\n", (i)? KRED:KGRN, i, KNRM, (i)?KRED:KGRN, ErrMsgs[-i], KNRM); \
			}

#define vwarning(i)	fprintf(stderr, "%sWarning%s: %s%s%s.\n", KMAG, KNRM, KYEL, WarnMsgs[i], KNRM)

typedef	unsigned char boolean_t;	// like bool_t

#define ERRCODE_NULL_PTR	(-1)
#define ERRCODE_VALUE		(-2)
#define ERRCODE_CANTOPEN	(-3)
#define ERRCODE_MALLOC		(-4)
#define ERRCODE_INCONSISTENCY	(-5)
#define ERRCODE_MISSING_ARG	(-6)


extern int errcode;

// {{{ Stringstack
typedef struct stringstack_node_t { char *string; struct stringstack_node_t *next; } stringstack_node_t;
typedef struct { int n; stringstack_node_t *last; stringstack_node_t *first; } stringstack_t;
stringstack_t *sstack_open();
int sstack_add(stringstack_t* sst, char *s);
char **sstack_pack(stringstack_t* sst, int *n);
bool sstack_is_present(stringstack_t* sst, char *string);
int sstack_close(stringstack_t *sst);
// }}}

/* {{{ Mem */
typedef struct {
	char *vptr; int total; int used;
} mem_t ;
typedef struct { int total; int used; } memtell_t;
mem_t * memopen(int size);
int memclose(mem_t * mem);
void *memalloc(mem_t *mem, int sz);
memtell_t *memtell(mem_t * mem);
/* }}} Mem */

int  scat(FILE *, char []);		// :r ! command
int  fcat(FILE *, char []);		// :r file
void abnormal(int);
char *sysprint(char *cmd, char *buffer, int n);

/* EoF vdfio.h */
