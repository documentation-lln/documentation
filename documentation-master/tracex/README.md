[[_TOC_]]

# The Tracex codes

Within MINERVA, the most up-to-date system configuration for the accelerator is maintained via a TraceWin file.  Configurations, parameters, and related information about Ion Source, LEBT, MEBT-1, RFQ, the CH cavities, MEBT-2, both sides of the MEBT-3, and the Cryomodules, are detailed in that file both as TraceWin statements and as TraceWin commented statements.

The Tracex codes is a family of "knowledge extractors" all taking their input from TraceWin files.
In what follows I describe those tools and mention possible future tools based on the same approach.


## `tracex`
`tracex` parses the TraceWin file and extracts knowledge about the accelerator:

![](pictures/01.png)
*`tracex` parses a TraceWin file in the standard input.*

The output goes in a Python data structure called `modules`, included in Python code `modules/TraceWin.py`:

![](pictures/02.png)
*`tracex` creates the Python dictionary `modules`. Its keys are associated to a list of TraceWin statements.*

## `flow`
Another code, called `flow`, takes as input the Python data structure and creates a UML diagram representing the beam workflow throughout the accelerator:

![](pictures/03a.png)
*Invoking `flow`.*

![](pictures/03b.png)
*`flow` creates a PlantUML diagram. Its nodes are the accelerator components, as described in the TraceWin input.*


![](pictures/04b.png)<br/>
*Top of the PlantUML activity diagram created by `flow` from the `modules` data structures extracted by `tracex`.*

`flow` can also convert the `modules` dictionary into a JSON string for further manipulations.


## `LaTeXmacros`
A third code, `LaTeXmacros`, analyses the data parsed from the TraceWin file and creates LaTeX macros used in the TDR:

![](pictures/05.png)
*`LaTeXmacros` derives the amount of CH cavities, cryomodules, and spoke cavities, and re-creates the macros used in the TDR to refer to those entities.*

Those macros are used extensively in the various chapters of the TDR. Their characteristic is to be *self-checking*, meaning that arguments are checked before being used. As an example, macro `\ncCH{16}` would issue an error, because in the definition of said macro it is mentioned that at most 15 CH cavities are present.

Maximum and minimum values are thus *inherited* from the TraceWin file!

The net result is that, when values are changed in the TraceWin configuration file, macros are automatically adjusted.  This allows, to some extent, to detect **consistency failures** in the TDR. As an example, when I changed the RF chapter as per Franck's instruction, I specified that cryomodules were not 30 but 23. By changing the `\ncCRYOMODULE` macro, several warnings were issued in three other chapters. In other words, inconsistencies became apparent!

![](pictures/06.png)
*The first few lines of the `\ncCRYOMODULE` macro.*

Much, much more than this can be done with this approach! For this, I will need to learn more what the TraceWin code actually
specifies.

I hope, when time shall allow, to be allowed to write a technical report about the approach sketched in the present document.
I wonder if it could also be turned into a paper!

# Further ideas

## Stage-dependent hyperlinks
Diagrams such as [this one](https://git.myrrha.lan/vdflorio/documentation/-/blob/master/tracex/modules/flow.png) may be augmented with *hyperlinks*. Said hyperlinks may point to key documents. The choice of "what is key" may depend on the phase of our project!  In other words -- the association between hyperlink and document to be returned by the hyperlink may be defined dynamically.  As an example: if I'm in recommissioning step 1, say "Calibration", then I would associate to the hyperlinks all the documentation pertaining to calibration. Moving to a recommissioning step 2, say "Testing", then I would associate to the hyperlinks all the documentation pertaining to testing. **The diagram would bring to the foreground the documents best matching the recommissioning step we are in!**

## Fault tracking system
We are in the process to create a protypical fault tracking system. A key question to this regard is, "what are the elements
that we shall track?" In other words, what is the level of detail of our FTS?
A second important question is, "How do we (*quickly*) construct the client for logging said information?"

I think an easy way to do this could be to construct another tool in the present toolset -- in other words, another
*consumer* of the `modules` data structure, which creates a series of hyperlinked SVG files:

- The top layer would show the accelerator as a single component.
- By clicking on it, we would "see" a representation of the major sub-systems as blocks in a component diagram.
- Each sub-system would be "hyperlinked" to another SVG file, and so forth down to a given level of detailed.
- At any time, one could click on a "log" button, and that would open an HTML form.
- The HTML form would allow to enter data regarding a fault pertaining to the selected sub-system. A CGI script would then add an entry into the fault description database.

The good thing of the above approach would be that no actual GUI would be required -- the user would just see representations of the system at some level of detail; and then, a simple HTML form to enter a record.

A preliminary proof-of-concepts has been quickly developed. You can see how it looks like [here](http://tdr.myrrha.lan/vfolders/tracex/linac.svg).
The structure you can see here is "read" from the TraceWin. This shows how *family of tools* (for fault tracking, for instance) could offer their services by navigating throughout the structure of the accelerator.

**A significant feature of this approach is that multiple TraceWin "views" could be turned into multiple versions of the same tool.**
A "Louvain-la-Neuve" view and a "Mol" view could just be represented by two TraceWin databases.

![](pictures/linac_svg_01.png)
*Major subsystems view. Every block is a hyperlink. One such link is shown.*

![](pictures/linac_svg_02.png)
*By clicking on the LBS block, one expands it into the set of cryomodules. Again, every block is a hyperlink.*

![](pictures/linac_svg_03.png)
*By clicking, e.g., onto block LB-S19, one is shown all the TraceWin commands associated to that block.*

Next, I will associate a fault log HTML FORM to the leaves of the above hierarchy!

## `tracex` as a TraceWin editor
TraceWin is syntactically very poor -- parameters are purely positional, therefore it is very easy to make mistakes and pass meaningless or wrong information to a method. How to improve this? We already saw in previous figure that the leaves of the accelerator hierarchy is presented as the commands associated to each leaf. A description of the parameters of most methods is given. As suggested by Jorik, we could actually allow the user to *edit* those descriptions, and recreate a TraceWin source once the user so requests. This would translate in a higher level TraceWin ``editor'' able to assist the designer in her/his task.

# Latest news
`flow.py` now outputs an SVG graph when called with option `-s`. The graph is transfered automatically on [tdr.myrrha.lan](http://tdr.myrrha.lan/vfolders/tracex/flow.svg) and can be visualized via any modern web browser.
