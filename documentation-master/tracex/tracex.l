%{
// {{{ Intro
/* TraceWin scanner
 *
 * Aim: to create a strong link between the TraceWin code used to maintain a snapshot of the MINERVA accelerator
 *      and the TDR and other documents.
 * Strategy: the TraceWin shall be parsed and knowledge about the accelerator shall be extracted.
 *           In a second step, that knowledge shall be used to create LaTeX macros and media to be used in other documents.
 * This module: scans the TraceWin source and returns "args" for the parser to process and extract knowledge.
 *              It also checkes whether the cardinality of elements has changed and, if so, it emits warnings.
 *
 * Version 0.2, Mi 28 Apr 2021 09:43:53 CEST
 * Version 0.3, Do 29 Apr 2021 10:21:47 CEST
 *		Debugging of the first few rule completed
 * Version 0.4, Do 29 Apr 2021 14:31:42 CEST
 *		Scanning up to I1-RFQ (excluded) tested successfully
 * Version 0.5, Do 29 Apr 2021 16:30:37 CEST
 *		Scanning up to I1-RFQ:OP-REFP-01
 * Version 0.6, Fr 30 Apr 2021 09:03:59 CEST
 *		Added new rules. Added "Questions" section. Added a first question.
 * Version 0.7, Mi 26 Mai 2021 14:47:59 CEST
 *		Corrected bug. Cleaned up code.
 *
 * Author: vdflorio
 * }}} */
// {{{ Header, #define's, prototypes
char *lversion = "0.6";

#include "vdfio.h"
#include "tracex.h"
#include "tracex.tab.h"

#define		RIGHTSIDE(c)					\
			char *p = strchr(yytext, c) + 1;	\
			while ( *p == ' ' ) p++;		\
			yylval.objVal.n = 1;			\
			yylval.objVal.args = malloc(sizeof(char*)); \
			yylval.objVal.args[0] = strdup(p);

void    rightsize(int c);
void    tokenize(int c);
int     getdigit_after(char *, char after);
double  getnum_parentheses(char *);
void    w_unexpected();
bool    isCommentedOut();
int     getCHnum();
int     getSnum();
// }}}
// {{{ Questions
/* A few questions for the TraceWin experts:
 *
 * In some cases (e.g., ADJUST) we have commands that are commented out and commands that are not. Do I have
 * to take both into account or the commented out are to be ignored?
 *
 */
// }}}

bool verbose = false;

#ifdef STANDALONE
YYSTYPE yylval;
#endif

%}

%option caseless
%option yylineno

left		^(";"[ ;]*)?
right		(.)*$
i1Is		([Ii]1[-][Ii][Ss])
i1IsRefp	"I1-IS-OP-REFP-01"[ ]*":"
i1IsSrcSrcA	"I1-IS-SRC-SRCA-"
i1IsDfEinz	"I1-IS-DF-EINZ-01"[ ]*":"
SCC		"SPACE_CHARGE_COMP"
SMAP		"SUPERPOSE_MAP"
ADJUST		"ADJUST"
ADJUST_STEERER	"ADJUST_STEERER"
PLOT_DST	"PLOT_DST"

i1Le		([Ii]1[-][Ll][Ee])
i1LeDfSol	"I1-LE-DF-SOL-"
i1LeDfCorv	"I1-LE-DF-CORV-"
i1LeDfCorh	"I1-LE-DF-CORH-"
DRIFT		"DRIFT"
i1BdFcup	"I1-BD-FCUP-"
REPEAT_ELE	"REPEAT_ELE"
i1LeVacVValve	"I1-LE-VAC-VVALVE-"
i1LeDfChop	"I1-LE-DF-CHOP-"
FREQ		"FREQ"
i1LeIcdColh	"I1-LE-ICD-COLH-"
i1LeIcdColv	"I1-LE-ICD-COLV-"
APERTURE	"APERTURE"
i1LeBdEmit	"I1-LE-BD-EMIT-"
DiagSize	"DIAG_SIZE"
DiagTwiss	"DIAG_TWISS"
DiagEnergy	"DIAG_ENERGY"
DiagDsize2	"DIAG_DSIZE2"
DiagDsize3	"DIAG_DSIZE3"
DiagEmit	"DIAG_EMIT"
DiagPosition	"DIAG_POSITION"
DiagCurrent	"DIAG_CURRENT"
i1LeBdBcm	"I1-LE-BD-BCM-"
End		"END"

i1Rfq		"I1-RFQ"
i1RfqRfRfq	"I1-RFQ-RF-RFQ"
RfqGapRmsFfs	"RFQ_GAP_RMS_FFS"
RfqCell		"RFQ_CELL"
Lattice		"LATTICE"
LatticeEnd	"LATTICE_END"
ChangeBeam	"CHANGE_BEAM"
ReadDst		"READ_DST"
i1RfqOpRefp	"I1-RFQ"[:-]"OP-REFP-01"[ ]*":"
FieldMapPath	"FIELD_MAP_PATH"
i1RfqOpRp	"I1-RFQ"[:-]"OP-RP-01"[ ]*":"
ErrRfqCelNCPLstat	"ERROR_RFQ_CEL_NCPL_STAT"
ErrRfqCelNCPLdyn	"ERROR_RFQ_CEL_NCPL_DYN"
ErrQuadNCPLstat	"ERROR_QUAD_NCPL_STAT"
ErrQuadNCPLdyn	"ERROR_QUAD_NCPL_DYN"
ErrCavNCPLstat	"ERROR_CAV_NCPL_STAT"
ErrCavNCPLdyn	"ERROR_CAV_NCPL_DYN"

i1Me1		"I1-ME1"
i1Me1DfCorxy	"I1-ME1-DF-CORXY-"
SetSyncPhase	"SET_SYNC_PHASE"
i1Me1RfCav	"I1-ME1-RF-CAV-"
i1Me1BdBpm	"I1-ME1-BD-BPM-"
i1Me1DfQu	"I1-ME1-DF-QU-"
i1Me1BdBcm	"I1-ME1-BD-BCM-"
i1Me1VacVvalve	"I1-ME1-VAC-VVALVE-"

i1Ch		"I1-CH"
i1ChxxDfQu	"I1-CH"[0-9]*"-DF-QU-"
i1ChxxRfCav	"I1-CH"[0-9]*"-RF-CAV-"
i1ChxxBdBpm	"I1-CH"[0-9]*"-BD-BPM-"
i1ChxxDfCorxy	"I1-CH"[0-9]*"-DF-CORXY-"

i1Me2		"I1-ME2"
i1Me2DfCorxy	"I1-ME2-DF-CORXY-"
i1Me2BdBcm	"I1-ME2-BD-BCM-"
i1Me2BdBpm	"I1-ME2-BD-BPM-"
i1Me2DfQu	"I1-ME2-DF-QU-"
i1Me2RfCav	"I1-ME2-RF-CAV-"
SetAdv		"SET_ADV"

i1Me3		"I1-ME3"
i1Me3DfQu	"I1-ME3-DF-QU-"
i1Me3RfCav	"I1-ME3-RF-CAV-"
i1Me3BdBpm	"I1-ME3-BD-BPM-"
i1Me3DfCorxy	"I1-ME3-DF-CORXY-"
i1Me3OpRp	"I1-ME3-OP-RP-"
i1Me3MagDi	"I1-ME3-MAG-DI-"
i1Me3MagQu	"I1-ME3-MAG-QU-"
i1Me3IcdColh	"I1-ME3-ICD-COLH-"
i1Me3IcdColv	"I1-ME3-ICD-COLV-"

lbMe3		"LB"[L]?"-ME3"
lbMe3MagDi	"LB-ME3-MAG-DI-"
lbMe3BdBpm	"LB-ME3-BD-BPM-"
lbMe3DfCorh	"LB-ME3-DF-CORH-"
lbMe3DfCorv	"LB-ME3-DF-CORV-"
lbMe3RfCav	"LB-ME3-RF-CAV-"
lbMe3MagQu	"LB-ME3-MAG-QU-"
lbMe3OpRefp	"LB"[L]?"-ME3-OP-REFP-"


lbS		"LB"[L]?"-S"
lbSxxMagQu	"LB-S"[0-9]*[-]?"MAG-QU-"
lbSxxDfQu	"LB-S"[0-9]*"-DF-QU-"
lbSxxRfCav	"LB-S"[0-9]*"-RF-CAV-"
lbSxxBdBpm	"LB-S"[0-9]*"-BD-BPM-"
lbSxxDfCorxy	"LB-S"[0-9]*"-DF-CORXY-"
lbSxxDfCorh	"LB-S"[0-9]*"-DF-CORH-"
lbSxxDfCorv	"LB-S"[0-9]*"-DF-CORV-"

%%

^[;]*[#]*[#]*[ ]*{i1Is}[ ]*[#]*$	{
#ifdef L_VERBOSE
						fprintf(stderr, "I1-IS "); fflush(stderr);
#endif
						yylval.objVal.n = 0;
						yylval.objVal.args = NULL;
						return yylval.objVal.token = I1_IS;
					}

{left}{i1IsRefp}{right}		{
					tokenize(':');
					return yylval.objVal.token = I1_IS_OP_REFP;
				}

{left}{i1IsSrcSrcA}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 1)
						w_unexpected();

					return yylval.objVal.token = I1_IS_SRC_SRCA;
				}

{left}{i1IsDfEinz}{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = 1;
					if (yylval.objVal.unit_id > 1)
					return yylval.objVal.token = I1_IS_DF_EINZ;
				}

{left}{SCC}{right}  		{
					RIGHTSIDE(' ')
					return yylval.objVal.token = SPACE_CHARGE_COMP;
				}

{left}{SMAP}{right}  		{
					RIGHTSIDE(' ')
					return yylval.objVal.token = SUPERPOSE_MAP;
				}




^[;]*[#]*[#]*[ ]*{i1Le}[ ]*[#]*$	{
#ifdef L_VERBOSE
						fprintf(stderr, "➡ I1-LE "); fflush(stderr);
#endif
						yylval.objVal.n = 0;
						yylval.objVal.args = NULL;
						return yylval.objVal.token = I1_LE;
					}

{left}{ADJUST_STEERER}{right} 	{
					tokenize(' ');
					return yylval.objVal.token = ADJUST_STEERER;
				}

{left}{ADJUST}{right} 		{
					tokenize(' ');
					return yylval.objVal.token = ADJUST;
				}

{left}{PLOT_DST}{right} 		{
					tokenize(' ');
					return yylval.objVal.token = PLOT_DST;
				}


{left}{i1LeDfSol}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 2)
						w_unexpected();

					return yylval.objVal.token = I1_LE_DF_SOL;
				}

{left}{i1LeDfCorv}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 2)
						w_unexpected();
					return yylval.objVal.token = I1_LE_DF_CORV;
				}

{left}{i1LeDfCorh}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 2)
						w_unexpected();
					return yylval.objVal.token = I1_LE_DF_CORH;
				}

{left}{DRIFT}{right} 		{
					tokenize(' ');
					return yylval.objVal.token = DRIFT;
				}

{left}{i1BdFcup}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 1)
						w_unexpected();
					return yylval.objVal.token = I1_BD_FCUP;
				}

{left}{REPEAT_ELE}{right} 		{
					tokenize(' ');
					return yylval.objVal.token = REPEAT_ELE;
				}

{left}{i1LeVacVValve}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 2)
						w_unexpected();

					return yylval.objVal.token = I1_LE_VAC_VVALVE;
				}

{left}{i1LeDfChop}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 1)
						w_unexpected();

					return yylval.objVal.token = I1_LE_DF_CHOP;
				}

{left}{i1LeIcdColh}{right}  	{
					RIGHTSIDE('0')
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 1)
						w_unexpected();

					return I1_LE_ICD_COLH;
				}

{left}{i1LeIcdColv}{right}  	{
					RIGHTSIDE('0')
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 1)
						w_unexpected();

					return I1_LE_ICD_COLV;
				}

{left}{APERTURE}{right} 		{
					tokenize(' ');
					return yylval.objVal.token = APERTURE;
				}

{left}{FREQ}{right} 		{
					tokenize(' ');
					return yylval.objVal.token = FREQ;
				}

{left}{i1LeBdEmit}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 1)
						w_unexpected();

					return yylval.objVal.token = I1_LE_BD_EMIT;
				}

{left}{DiagSize}{right} 	{
					tokenize(' ');
					return yylval.objVal.token = DIAG_SIZE;
				}

{left}{DiagTwiss}[(]{right} 	{
					tokenize(' ');
					yylval.objVal.param = getnum_parentheses(yytext);

					return yylval.objVal.token = DIAG_TWISS;
				}

{left}{DiagEnergy}[(]{right} 	{
					tokenize(' ');
					yylval.objVal.param = getnum_parentheses(yytext);

					return yylval.objVal.token = DIAG_ENERGY;
				}

{left}{DiagDsize2}{right} 	{
					tokenize(' ');
					return yylval.objVal.token = DIAG_DSIZE2;
				}

{left}{DiagDsize3}{right} 	{
					tokenize(' ');
					return yylval.objVal.token = DIAG_DSIZE3;
				}

{left}{DiagEmit}{right} 	{
					tokenize(' ');
					return yylval.objVal.token = DIAG_EMIT;
				}

{left}{DiagPosition}{right} 		{
					tokenize(' ');
					return yylval.objVal.token = DIAG_POSITION;
				}

{left}{DiagCurrent}{right} 		{
					tokenize(' ');
					return yylval.objVal.token = DIAG_CURRENT;
				}

{left}{i1LeBdBcm}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 1)
						w_unexpected();

					return yylval.objVal.token = I1_LE_BD_BCM;
				}

{left}{End}{right}  		{
					yylval.objVal.n = 0;
					yylval.objVal.args = NULL;
					return yylval.objVal.token = END_SECTION;
				}

{End}{right}  		{
				yylval.objVal.n = 0;
				yylval.objVal.args = NULL;
				return yylval.objVal.token = END;
			}



^[;]*[#]*[#]*[ ]*{i1Rfq}[ ]*[#]*$	{
#ifdef L_VERBOSE
						fprintf(stderr, "➡ I1-RFQ "); fflush(stderr);
#endif
						yylval.objVal.n = 0;
						yylval.objVal.args = NULL;
						return I1_RFQ;
					}

{left}{i1RfqRfRfq}0[0-9][ ]*[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 1)
						w_unexpected();

					return yylval.objVal.token = I1_RFQ_RF_RFQ;
				}

{left}{RfqGapRmsFfs}{right} 	{
					tokenize(' ');
					return yylval.objVal.token = RFQ_GAP_RMS_FFS;
				}

{left}{RfqCell}{right} 		{
					tokenize(' ');
					return yylval.objVal.token = RFQ_CELL;
				}

{left}{LatticeEnd}{right} 	{
					yylval.objVal.n = 0;
					yylval.objVal.args = NULL;
					return yylval.objVal.token = LATTICE_END;
				}

{left}{Lattice}{right} 		{
					tokenize(' ');
					return yylval.objVal.token = LATTICE;
				}

{left}{ChangeBeam}{right} 	{
					tokenize(' ');
					return yylval.objVal.token = CHANGE_BEAM;
				}

{left}{ReadDst}{right} 		{
					tokenize(' ');
					return yylval.objVal.token = READ_DST;
				}

{left}{i1RfqOpRefp}{right}	{
					tokenize(':');
					return I1_RFQ_OP_REFP;
				}

{left}{i1RfqOpRp}{right}	{
					tokenize(':');
					return yylval.objVal.token = I1_RFQ_OP_RP;
				}

{left}{FieldMapPath}{right} 	{
					tokenize(' ');
					return yylval.objVal.token = FIELD_MAP_PATH;
				}

{left}{ErrQuadNCPLstat}{right}	{
					tokenize(' ');
					return yylval.objVal.token = ERROR_QUAD_NCPL_STAT;
				}
{left}{ErrQuadNCPLdyn}{right}	{
					tokenize(' ');
					return yylval.objVal.token = ERROR_QUAD_NCPL_DYN;
				}
{left}{ErrCavNCPLstat}{right}	{
					tokenize(' ');
					return yylval.objVal.token = ERROR_CAV_NCPL_STAT;
				}
{left}{ErrCavNCPLdyn}{right}	{
					tokenize(' ');
					return yylval.objVal.token = ERROR_CAV_NCPL_DYN;
				}
{left}{ErrRfqCelNCPLstat}{right}	{
					tokenize(' ');
					return yylval.objVal.token = ERROR_RFQ_CEL_NCPL_STAT;
				}
{left}{ErrRfqCelNCPLdyn}{right}	{
					tokenize(' ');
					return yylval.objVal.token = ERROR_RFQ_CEL_NCPL_DYN;
				}



^[;]*[#]*[#]*[ ]*{i1Me1}[ ]*[#]*$	{
#ifdef L_VERBOSE
						fprintf(stderr, "➡ I1-ME1 "); fflush(stderr);
#endif
						yylval.objVal.n = 0;
						yylval.objVal.args = NULL;
						return yylval.objVal.token = I1_ME1;
					}

{left}{i1Me1DfCorxy}0[0-9][ ]*[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 2)
						w_unexpected();

					return yylval.objVal.token = I1_ME1_DF_CORXY;
				}
{left}{i1Me2DfCorxy}0[0-9][ ]*[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 2)
						w_unexpected();

					return yylval.objVal.token = I1_ME2_DF_CORXY;
				}
{left}{i1Me3DfCorxy}0[0-9][ ]*[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 3)
						w_unexpected();

					return yylval.objVal.token = I1_ME3_DF_CORXY;
				}

{left}{SetSyncPhase}{right} 	{
					yylval.objVal.commented_out = (isCommentedOut())? true : false;
					yylval.objVal.n = 0;
					yylval.objVal.args = NULL;
					return yylval.objVal.token = SET_SYNC_PHASE;
				}

{left}{i1Me1RfCav}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 2)
						w_unexpected();

					return yylval.objVal.token = I1_ME1_RF_CAV;
				}

{left}{i1Me1BdBpm}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 1)
						w_unexpected();

					return yylval.objVal.token = I1_ME1_BD_BPM;
				}

{left}{i1Me1DfQu}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 3)
						w_unexpected();

					return yylval.objVal.token = I1_ME1_DF_QU;
				}

{left}{i1Me1BdBcm}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 1)
						w_unexpected();

					return yylval.objVal.token = I1_ME1_BD_BCM;
				}

{left}{i1Me1VacVvalve}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 1)
						w_unexpected();

					return yylval.objVal.token = I1_ME1_VAC_VVALVE;
				}



^[;]*[#]*[#]*[ ]*{i1Ch}[0-9]*[ ]*[#]*$	{
					yylval.objVal.unit_id = getCHnum();
#ifdef L_VERBOSE
					fprintf(stderr, "➡ I1-CH%02d ", yylval.objVal.unit_id); fflush(stderr);
#endif
					yylval.objVal.n = 0;
					yylval.objVal.args = NULL;
					return yylval.objVal.token = I1_CH;
					}

{left}{i1ChxxDfQu}[-]?[0-9]*[ ]?[:]{right}	{
						tokenize(':');
						if (strchr(yytext, 'U'))
							yylval.objVal.unit_id = getdigit_after(yytext, 'U');
						else
							yylval.objVal.unit_id = getdigit_after(yytext, 'u');
						if (yylval.objVal.unit_id > 2)
							w_unexpected();

						yylval.objVal.module_id = getCHnum();
						if (yylval.objVal.module_id < 0 || yylval.objVal.module_id > CHCAVITIES)
							w_unexpected();

						return yylval.objVal.token = I1_CHxx_DF_QU;
					}

{left}{i1ChxxRfCav}0[0-9][ ]?[:]{right}	{
						tokenize(':');
						if (strchr(yytext, 'V'))
							yylval.objVal.unit_id = getdigit_after(yytext, 'V');
						else
							yylval.objVal.unit_id = getdigit_after(yytext, 'v');

						if (yylval.objVal.unit_id > 1)
							w_unexpected();

						yylval.objVal.module_id = getCHnum();
						if (yylval.objVal.module_id < 0 || yylval.objVal.module_id > CHCAVITIES)
							w_unexpected();

						return yylval.objVal.token = I1_CHxx_RF_CAV;
					}

{left}{i1ChxxBdBpm}0[0-9][ ]?[:]{right}	{
						tokenize(':');
						if (strchr(yytext, 'M'))
							yylval.objVal.unit_id = getdigit_after(yytext, 'M');
						else
							yylval.objVal.unit_id = getdigit_after(yytext, 'm');

						if (yylval.objVal.unit_id > 1)
							w_unexpected();

						yylval.objVal.module_id = getCHnum();
						if (yylval.objVal.module_id < 0 || yylval.objVal.module_id > CHCAVITIES)
							w_unexpected();

						return yylval.objVal.token = I1_CHxx_BD_BPM;
					}

{left}{i1ChxxDfCorxy}[-]?[0-9]*[ ]?[:]{right}	{
						tokenize(':');
						if (strchr(yytext, 'Y'))
							yylval.objVal.unit_id = getdigit_after(yytext, 'Y');
						else
							yylval.objVal.unit_id = getdigit_after(yytext, 'y');
						if (yylval.objVal.unit_id > 2)
							w_unexpected();

						yylval.objVal.module_id = getCHnum();
						if (yylval.objVal.module_id < 0 || yylval.objVal.module_id > CHCAVITIES)
							w_unexpected();

						return yylval.objVal.token = I1_CHxx_DF_CORXY;
					}



^[;]*[#]*[#]*[ ]*{i1Me2}[ ]*[#]*$	{
#ifdef L_VERBOSE
						fprintf(stderr, "➡ I1-ME2 "); fflush(stderr);
#endif
						yylval.objVal.n = 0;
						yylval.objVal.args = NULL;
						return yylval.objVal.token = I1_ME2;
					}

{left}{i1Me2DfQu}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 2)
						w_unexpected();

					return yylval.objVal.token = I1_ME2_DF_QU;
				}

{left}{i1Me2BdBcm}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 1)
						w_unexpected();

					return yylval.objVal.token = I1_ME2_BD_BCM;
				}

{left}{i1Me2BdBpm}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 1)
						w_unexpected();

					return yylval.objVal.token = I1_ME2_BD_BPM;
				}

{left}{i1Me2RfCav}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 2)
						w_unexpected();

					return yylval.objVal.token = I1_ME2_RF_CAV;
				}

{left}{SetAdv}{right} 		{
					tokenize(' ');
					return yylval.objVal.token = SET_ADV;
				}



^[;]*[#]*[#]*[ ]*{i1Me3}[ ]*[#]*$	{
#ifdef L_VERBOSE
						fprintf(stderr, "➡ I1-ME3 "); fflush(stderr);
#endif
						yylval.objVal.n = 0;
						yylval.objVal.args = NULL;
						return yylval.objVal.token = I1_ME3;
					}

{left}{i1Me3DfQu}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 5)
						w_unexpected();

					return yylval.objVal.token = I1_ME3_DF_QU;
				}

{left}{i1Me3BdBpm}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 2)
						w_unexpected();

					return yylval.objVal.token = I1_ME3_BD_BPM;
				}

{left}{i1Me3RfCav}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 1)
						w_unexpected();

					return yylval.objVal.token = I1_ME3_RF_CAV;
				}

{left}{i1Me3OpRp}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 2)
						w_unexpected();

					return yylval.objVal.token = I1_ME3_OP_RP;
				}

{left}EDGE{right} 		{
					tokenize(' ');
					return yylval.objVal.token = EDGE;
				}

{left}{i1Me3MagDi}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 1)
						w_unexpected();

					return yylval.objVal.token = I1_ME3_MAG_DI;
				}

{left}{i1Me3MagQu}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id < 6 ||
					    yylval.objVal.unit_id > 9)
						w_unexpected();

					return yylval.objVal.token = I1_ME3_MAG_QU;
				}

{left}{i1Me3IcdColh}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 1)
						w_unexpected();

					return yylval.objVal.token = I1_ME3_ICD_COLH;
				}

{left}{i1Me3IcdColv}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 1)
						w_unexpected();

					return yylval.objVal.token = I1_ME3_ICD_COLV;
				}



^[;]*[#]*[#]*[ ]*{lbMe3}[ ]*[#]*$	{
#ifdef L_VERBOSE
						fprintf(stderr, "➡ LB-ME3 "); fflush(stderr);
#endif
						yylval.objVal.n = 0;
						yylval.objVal.args = NULL;
						return yylval.objVal.token = LB_ME3;
					}


{left}{lbMe3MagDi}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 1)
						w_unexpected();

					return yylval.objVal.token = LB_ME3_MAG_DI;
				}

{left}{lbMe3BdBpm}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 2)
						w_unexpected();

					return yylval.objVal.token = LB_ME3_BD_BPM;
				}

{left}{lbMe3DfCorh}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 2)
						w_unexpected();

					return yylval.objVal.token = LB_ME3_DF_CORH;
				}
{left}{lbMe3DfCorv}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 2)
						w_unexpected();

					return yylval.objVal.token = LB_ME3_DF_CORV;
				}


{left}{lbMe3RfCav}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 2)
						w_unexpected();

					return yylval.objVal.token = LB_ME3_RF_CAV;
				}

{left}{lbMe3MagQu}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 3)
						w_unexpected();

					return yylval.objVal.token = LB_ME3_MAG_QU;
				}

{left}{lbMe3OpRefp}0[0-9][ ]?[:]{right} 	{
					tokenize(':');
					yylval.objVal.unit_id = getdigit_after(yytext, '0');
					if (yylval.objVal.unit_id > 1)
						w_unexpected();

					return yylval.objVal.token = LBL_ME3_OP_REFP;
				}




^[;]*[#]*[#]*[ ]*{lbS}[0-9]*[ ]*[#]*$	{
						yylval.objVal.module_id = getSnum();
#ifdef L_VERBOSE
						fprintf(stderr, "➡ LB-S%02d ", yylval.objVal.module_id); fflush(stderr);
#endif
						yylval.objVal.n = 0;
						yylval.objVal.args = NULL;
						return yylval.objVal.token = LB_Sxx;
					}


{left}{lbSxxBdBpm}0[0-9][ ]?[:]{right}	{
						tokenize(':');
						if (strchr(yytext, 'M'))
							yylval.objVal.unit_id = getdigit_after(yytext, 'M');
						else
							yylval.objVal.unit_id = getdigit_after(yytext, 'm');

						if (yylval.objVal.unit_id > 1)
							w_unexpected();

						yylval.objVal.module_id = getSnum();
						if (yylval.objVal.module_id < 0 || yylval.objVal.module_id > QMODULES)
							w_unexpected();

						return yylval.objVal.token = LB_Sxx_BD_BPM;
					}

{left}{lbSxxMagQu}[0-9]*[ ]?[:]{right}	{
						tokenize(':');
						if (strchr(yytext, 'U'))
							yylval.objVal.unit_id = getdigit_after(yytext, 'U');
						else
							yylval.objVal.unit_id = getdigit_after(yytext, 'u');
						if (yylval.objVal.unit_id > 2)
							w_unexpected();

						yylval.objVal.module_id = getSnum();
						if (yylval.objVal.module_id < 0 || yylval.objVal.module_id > QMODULES)
							w_unexpected();

						return yylval.objVal.token = LB_Sxx_MAG_QU;
					}

{left}{lbSxxDfCorh}[-]?[0-9]*[ ]?[:]{right}	{
						tokenize(':');
						if (strchr(yytext, 'H'))
							yylval.objVal.unit_id = getdigit_after(yytext, 'H');
						else
							yylval.objVal.unit_id = getdigit_after(yytext, 'h');
						if (yylval.objVal.unit_id > 1)
							w_unexpected();

						yylval.objVal.module_id = getSnum();
						if (yylval.objVal.module_id < 0 || yylval.objVal.module_id > QMODULES)
							w_unexpected();

						return yylval.objVal.token = LB_Sxx_DF_CORH;
					}

{left}{lbSxxDfCorv}[-]?[0-9]*[ ]?[:]{right}	{
						tokenize(':');
						if (strchr(yytext, 'V'))
							yylval.objVal.unit_id = getdigit_after(yytext, 'V');
						else
							yylval.objVal.unit_id = getdigit_after(yytext, 'v');
						if (yylval.objVal.unit_id > 1)
							w_unexpected();

						yylval.objVal.module_id = getSnum();
						if (yylval.objVal.module_id < 0 || yylval.objVal.module_id > QMODULES)
							w_unexpected();

						return yylval.objVal.token = LB_Sxx_DF_CORH;
					}

{left}{lbSxxRfCav}[-]?[0-9]*[ ]?[:]{right}	{
						tokenize(':');
						if (strchr(yytext, 'V'))
							yylval.objVal.unit_id = getdigit_after(yytext, 'V');
						else
							yylval.objVal.unit_id = getdigit_after(yytext, 'v');
						if (yylval.objVal.unit_id > 2)
							w_unexpected();

						yylval.objVal.module_id = getSnum();
						if (yylval.objVal.module_id < 0 || yylval.objVal.module_id > QMODULES)
							w_unexpected();

						return yylval.objVal.token = LB_Sxx_RF_CAV;
					}


^[;]		{
			int c;
			// fprintf(stderr, "In COMMENT_LINE: ");
			while ( (c=input()) ) {
				if (c == EOF) { unput(c); break; }
				if (c == '\n') break;
				// fprintf(stderr, "%c", c);
			}
			// fprintf(stderr, "\n");
			return COMMENT_LINE;
		}

\n	return NEWLINE;

.	;

<<EOF>>		{
#ifdef L_VERBOSE
			fprintf(stderr, "➡ ⏹️\n");
#endif
			yyterminate();
		}
%%

// {{{ Ancillary functions
char *strlwr(char *s) {
	unsigned c;
	unsigned char *p = (unsigned char *)s;

	while (c = *p) *p++ = tolower(c);
	return s;
}

char *strupr(char *s) {
	unsigned c;
	unsigned char *p = (unsigned char *)s;

	while (c = *p) *p++ = toupper(c);
	return s;
} 
int getSnum() {
	char *r, *q, *p, *upcopy;
	int n;

	upcopy = strdup(yytext);
	upcopy = strupr(upcopy);
	if   ((p = strstr(upcopy, "LB-S")) != NULL) p += 4;
	else { p = strstr(upcopy, "LBL-S");         p += 5; }

	r = p;
	q = strchr(p+1, '#');
	if (q == NULL) q = strchr(p+1, '-');
	//fprintf(stderr, "\nyytext=%s\np=%s, q=%s, ", yytext, p, q);
	if (q) *q = '\0';
	//fprintf(stderr, "r=%s\n", r);
	n=atoi(r);
	free(upcopy);
	return n;
}
int getCHnum() {
	char *r, *q, *p = strchr(yytext, '1');
	// ^[;]*[#]*[#]*[ ]*{i1
	p += 4;
	// Ch}
	r = p;
	q = strchr(p+1, '#');
	if (q == NULL) q = strchr(p+1, '-');
	// fprintf(stderr, "\nyytext=%s\np=%s, q=%s\n", yytext, p, q);
	if (q) *q = '\0';
	// [0-9]*[#]*$
	return atoi(r);
}
void rightside(int c) {
	char *p = strchr(yytext, c) + 1;
	while ( *p == ' ' ) p++;
	yylval.objVal.n = 1;
	yylval.objVal.args = malloc(sizeof(char*));
	yylval.objVal.args[0] = strdup(p);
}
void tokenize(int c) {
	char *p;
	char *token;
	int   i;
	char *the_args[TRACEWIN_MAXARGS];
	/*
	static int n;
	n++;
	if (n == 598)
		printf("n=%d, yytext=%s, c=%c\n", n, yytext, c);
	*/
	yylval.objVal.commented_out = (isCommentedOut())? true : false;
	p = strchr(yytext, c);
	if (p == NULL) {
		yylval.objVal.n = 0;
		yylval.objVal.args = NULL;
		return;
	}
	p++;
	while ( p != NULL && *p == ' ') p++;
	if (p == NULL) {
		yylval.objVal.n = 0;
		yylval.objVal.args = NULL;
		return;
	}
	for   ( i=0, token = strtok(p, " ");
		token != NULL;
		token = strtok(NULL, " "), i++
	      ) {
			// if one of the args starts a comment, we ignore what follows
			if (strcmp(token, ";")==0)
				break;
			the_args[i] = strdup( token );
			// if more than TRACEWIN_MAXARGS token are present, we ignore the ones in excess
			if (i+1 >= TRACEWIN_MAXARGS)
				break;
	}
	yylval.objVal.n = i; //+1;
	// only room for the encounteed args is allocated
	yylval.objVal.args = malloc(yylval.objVal.n * sizeof(char*));
	for (i=0; i < yylval.objVal.n; i++)
		yylval.objVal.args[i] = the_args[i];
	return;
}

int getdigit_after(char *s, char after) {
	char *p = strchr(s, after);
	if (p == NULL) return -1;
	return *(p+1) - '0';
}
void w_unexpected() {
	fprintf(stderr, "%sWarning: unexpected element: %s%s\n", KRED, yytext, KNRM);
}
double  getnum_parentheses(char *s) {
	char *p = strchr(s, '(');
	char *q;
	q = strchr(p+1, ')');
	if (q != NULL) *q = '\0';
	return atof(p+1);
}
bool isCommentedOut() {
	char *p = yytext;
	while ( *p ) {
		if (*p == ';') return true;
		if (! isspace(*p)) return false;
		p++;
	}
	return false;
}
// }}}

/*
int main() {
	while (yylex()) ;
#ifdef L_VERBOSE
	fprintf(stderr, "➡ ⏹️\n");
#endif
}
*/
/*
int main(int argc, char *argv[]) {
	int i;
	for (i=1; i < argc; i++) {
	  if (argv[i][0] == '-')
	    switch(argv[i][1]) {
		case 'v':	verbose = true;
				break;
		default: 	fprintf(stderr, "Erroneous option (%s) -- ignored\n", argv[i]);
				break;
	    }
	}
	while (yylex()) ;
	if (verbose) fprintf(stderr, "➡ ⏹️\n"); fflush(stderr);
	return 0;
}
*/
// EoF(tracex.l)
