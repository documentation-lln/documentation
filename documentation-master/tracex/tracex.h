#ifndef __Tr_ACE_x_h__
#define __Tr_ACE_x_h__
// max number of arguments in a TraceWin function
#define TRACEWIN_MAXARGS	30
#define CHCAVITIES		15
#define QMODULES		23

#define NO_ARGS			(-1)
#define STR_ARGS		(-2)
#define TOK_ARGS		(-3)
#define INT_ARGS		(-4)
#define FLT_ARGS		(-5)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

typedef struct {
		int    n;		// number of arguments
		char **args;		// arguments
		int    token;		// lexycal token
		int    unit_id;		// unit id, as in LB_S<module>_DF_CORH<unit>
		int    module_id;	// module number
		double param;		// a real number parameter
		bool   commented_out;	// true: the command was commented out
	} Token_t;

void yyerror (char const *s) ;
int yylex();
char *tok2str(int);

// module names

#define MODULE_DIR	"modules"
#define MODULE_SUFFIX	"_module.py"

#endif
