%{
/*
 * TraceWin ad-hoc "parser"
 *
 * Aim: to create a strong link between the TraceWin code used to maintain a snapshot of the MINERVA accelerator
 *      and the TDR and other documents.
 * Strategy: the TraceWin shall be parsed and knowledge about the accelerator shall be extracted.
 *           In a second step, that knowledge shall be used to create LaTeX macros and media to be used in other documents.
 *
 * Version 0.1, Mo 26 Apr 2021 15:18:58 CEST
 *		Preliminary version. Incomplete and buggy.
 * Version 0.2, Mi 26 Mai 2021 14:49:20 CEST
 *              Rewritten parser. Completed up to LB-S23. Tested up to LB-S23 *with old TraceWin file*.
 *
 * Author: vdflorio
 */
static char *version  = "0.3, Mi 09 Jun 2021 10:58:29 CEST";
			// Added explanations in the error messages. Tests: passed.

#include <stdio.h>
#include "vdfio.h"

//extern YYSTYPE yylval;

FILE *mfp; // module file pointer
char *current_module; // module name
char current_module_file[128];

char *expected_sequence[] = {
	"external",
	"i1is", "i1le", "i1rfq", "i1me1",
	"i1ch1", "i1ch2", "i1ch3", "i1ch4", "i1ch5", "i1ch6", "i1ch7",
	"i1me2",
	"i1ch8", "i1ch9", "i1ch10", "i1ch11", "i1ch12", "i1ch13", "i1ch14", "i1ch15",
	"i1me3",
	"lbme3",
	"lbs01", "lbs02", "lbs03", "lbs04", "lbs05",
	"lbs06", "lbs07", "lbs08", "lbs09", "lbs10",
	"lbs11", "lbs12", "lbs13", "lbs14", "lbs15",
	"lbs16", "lbs17", "lbs18", "lbs19", "lbs20",
	"lbs21", "lbs22", "lbs23", // "lsb24",
	"EoA" // EoA is the end-of-accelerator tag
	};
// int len_sequence = 46;

int current = 0;
%}

%code top {
#include "tracex.h"

// Enables debugging mode
#ifdef YYDEBUG
  yydebug = 1;
#endif

int storeObj(FILE *mfp, char *actions_list, Token_t ObjVal);

extern char *tok2str(int);
}

/* Type of yylval
   Example: in flex, do
	yylval.stringValue = strdup(yytext);
	return IDENTIFIER;
   Example: in bison, do
	nexp: nexp '+' nexp { $<floatValue>$ = $<floatValue>1 + $<floatValue>3 }
 */
%union
{
	Token_t objVal;
}

/* declare tokens */
%token I1_IS
%token I1_IS_OP_REFP // reference pointer
%token I1_IS_SRC_SRCA
%token I1_IS_DF_EINZ
%token SPACE_CHARGE_COMP
%token SUPERPOSE_MAP
//
%token I1_LE
%token ADJUST
%token I1_LE_DF_SOL
%token I1_LE_DF_CORV
%token I1_LE_DF_CORH
%token I1_BD_FCUP
%token I1_LE_VAC_VVALVE
%token I1_LE_ICD_COLH
%token I1_LE_ICD_COLV
%token I1_LE_BD_EMIT
%token I1_LE_DF_CHOP
%token I1_LE_BD_BCM
%token REPEAT_ELE
//
%token I1_RFQ
%token I1_RFQ_RF_RFQ
%token RFQ_GAP_RMS_FFS
%token RFQ_CELL
%token DRIFT
%token CHANGE_BEAM
%token READ_DST
%token DIAG_POSITION
%token I1_RFQ_OP_REFP // reference pointer
%token FREQ
%token I1_RFQ_OP_RP
%token ERROR_RFQ_CEL_NCPL_STAT
%token ERROR_RFQ_CEL_NCPL_DYN
%token ERROR_QUAD_NCPL_STAT
%token ERROR_QUAD_NCPL_DYN
%token ERROR_CAV_NCPL_STAT
%token ERROR_CAV_NCPL_DYN
//
%token I1_ME1
%token I1_ME1_DF_CORXY
%token SET_SYNC_PHASE
%token I1_ME1_RF_CAV
%token I1_ME1_BD_BPM
%token I1_ME1_DF_QU
%token I1_ME1_BD_BCM
%token I1_ME1_VAC_VVALVE
//
%token I1_CH
%token I1_CHxx_DF_QU
%token I1_CHxx_RF_CAV
%token I1_CHxx_BD_BPM
%token I1_CHxx_DF_CORXY
%token DIAG_DSIZE2
%token DIAG_DSIZE3
%token SET_ADV
//
%token LATTICE
%token LATTICE_END
//
%token I1_ME2
%token I1_ME2_DF_QU
%token I1_ME2_DF_CORXY
%token I1_ME2_BD_BCM
%token I1_ME2_RF_CAV
%token I1_ME2_BD_BPM
//
%token I1_ME3
%token I1_ME3_DF_QU
%token ADJUST_STEERER
%token I1_ME3_DF_CORXY
%token I1_ME3_OP_RP
%token I1_ME3_BD_BPM
%token I1_ME3_RF_CAV
%token I1_ME3_MAG_DI
//
%token EDGE
%token I1_ME3_MAG_QU
%token I1_ME3_ICD_COLH
%token I1_ME3_ICD_COLV
%token APERTURE
%token DIAG_SIZE
%token DIAG_WAIST
%token DIAG_ACHROMAT
%token PLOT_DST
//
%token LB_ME3
%token LB_ME3_MAG_DI
%token LB_ME3_BD_BPM
%token LB_ME3_DF_CORH
%token LB_ME3_DF_CORV
%token LB_ME3_RF_CAV
%token LB_ME3_MAG_QU
//
%token LBL_ME3_OP_REFP // reference pointer
%token DIAG_ENERGY
%token DIAG_TWISS
%token DIAG_EMIT
%token DIAG_CURRENT
//
%token LB_Sxx
%token LB_Sxx_BD_BPM
%token LB_SxxMAG_QU
%token LB_Sxx_DF_CORH
%token LB_Sxx_DF_CORV
%token LB_Sxx_MAG_QU
%token LB_Sxx_RF_CAV
//
%token LB_Sxx_OP_REFP_001 // reference pointer
%token COMMENT_LINE
%token OTHER
%token END_SECTION
%token END
%token FIELD_MAP_PATH
//
%token NEWLINE

%%

input:
     	|
	input line
	;


comment_line:	COMMENT_LINE
	    |
	    comment_line COMMENT_LINE
	    ;

secondary: secondary_grp
		{
			Token_t *t =  & $<objVal>$;
			storeObj(mfp, current_module, $<objVal>$);
		}
	     ;

secondary_grp:	SPACE_CHARGE_COMP | REPEAT_ELE | SUPERPOSE_MAP | ADJUST | DRIFT | APERTURE |
	     	DIAG_TWISS | DIAG_EMIT | END_SECTION | FREQ | ERROR_CAV_NCPL_DYN | ERROR_CAV_NCPL_STAT |
		ERROR_QUAD_NCPL_DYN | ERROR_QUAD_NCPL_STAT | FIELD_MAP_PATH | LATTICE | LATTICE_END |
		CHANGE_BEAM | READ_DST | DIAG_POSITION | DIAG_CURRENT |
		SET_SYNC_PHASE |
		DIAG_DSIZE2 | DIAG_DSIZE3 | SET_ADV |
		ADJUST_STEERER | PLOT_DST | EDGE | DIAG_SIZE |
		DIAG_ENERGY 
	 ;

line:	NEWLINE
	| comment_line 

	| secondary NEWLINE

	| I1_IS NEWLINE
		{
			if (strcmp(current_module, expected_sequence[current]) != 0) {
				fprintf(stderr, "%sUnexpected%s: I1-IS preceeded by %s instead of %s\n",
					KRED, KNRM, current_module, expected_sequence[current]);
			}
			// first, we close the previous module
			if (mfp != NULL) {
				fclose(mfp);
			}
			// secondly, we move to the next module
			current++;
			current_module = expected_sequence[current]; // i1is
			if (strcmp(current_module, "EoA") == 0) {
				fprintf(stderr, "\n%sUnexpected structure of the accelerator. This could be either an error or a result of a design modification. Please verify.%s\n", KYEL, KNRM);
				exit(0);
			}
			sprintf(current_module_file, "%s/%s%s", MODULE_DIR, current_module, MODULE_SUFFIX);
			// third, we open the current module -- in this case, I1_IS
			mfp = fopen(current_module_file, "w");
			if (mfp == NULL) {
				fprintf(stderr, "%sError%s: cannot open module %s\n", KRED, KNRM, current_module_file);
				exit(-2);
			}
		}
	| i1_is_element NEWLINE


	| I1_LE NEWLINE
		{
			if (strcmp(current_module, expected_sequence[current]) != 0) {
				fprintf(stderr, "%sUnexpected%s: I1-LE preceeded by %s instead of %s\n",
					KRED, KNRM, current_module, expected_sequence[current]);
			}
			// first, we close the previous module
			if (mfp != NULL) {
				fclose(mfp);
			}
			// secondly, we move to the next module
			current++;
			current_module = expected_sequence[current]; // i1le
			if (strcmp(current_module, "EoA") == 0) {
				fprintf(stderr, "\n%sUnexpected structure of the accelerator. This could be either an error or a result of a design modification. Please verify.%s\n", KYEL, KNRM);
				exit(0);
			}
			sprintf(current_module_file, "%s/%s%s", MODULE_DIR, current_module, MODULE_SUFFIX);
			// third, we open the current module -- in this case, I1_LE
			mfp = fopen(current_module_file, "w");
			if (mfp == NULL) {
				fprintf(stderr, "%sError%s: cannot open module %s\n", KRED, KNRM, current_module_file);
				exit(-2);
			}
		}
	| i1_le_element NEWLINE


	| I1_RFQ NEWLINE
		{
			if (strcmp(current_module, expected_sequence[current]) != 0) {
				fprintf(stderr, "%sUnexpected%s: I1-RFQ preceeded by %s instead of %s\n",
					KRED, KNRM, current_module, expected_sequence[current]);
			}
			// first, we close the previous module
			if (mfp != NULL) {
				fclose(mfp);
			}
			// secondly, we move to the next module
			current++;
			current_module = expected_sequence[current]; // i1le
			if (strcmp(current_module, "EoA") == 0) {
				fprintf(stderr, "\n%sUnexpected structure of the accelerator. This could be either an error or a result of a design modification. Please verify.%s\n", KYEL, KNRM);
				exit(0);
			}
			sprintf(current_module_file, "%s/%s%s", MODULE_DIR, current_module, MODULE_SUFFIX);
			// third, we open the current module -- in this case, I1_RFQ
			mfp = fopen(current_module_file, "w");
			if (mfp == NULL) {
				fprintf(stderr, "%sError%s: cannot open module %s\n", KRED, KNRM, current_module_file);
				exit(-2);
			}
		}
	| i1_rfq_element NEWLINE


	| I1_ME1 NEWLINE
		{
			if (strcmp(current_module, expected_sequence[current]) != 0) {
				fprintf(stderr, "%sUnexpected%s: I1-ME1 preceeded by %s instead of %s\n",
					KRED, KNRM, current_module, expected_sequence[current]);
			}
			// first, we close the previous module
			if (mfp != NULL) {
				fclose(mfp);
			}
			// secondly, we move to the next module
			current++;
			current_module = expected_sequence[current]; // i1me1
			if (strcmp(current_module, "EoA") == 0) {
				fprintf(stderr, "\n%sUnexpected structure of the accelerator. This could be either an error or a result of a design modification. Please verify.%s\n", KYEL, KNRM);
				exit(0);
			}
			sprintf(current_module_file, "%s/%s%s", MODULE_DIR, current_module, MODULE_SUFFIX);
			// third, we open the current module -- in this case, I1_LE
			mfp = fopen(current_module_file, "w");
			if (mfp == NULL) {
				fprintf(stderr, "%sError%s: cannot open module %s\n", KRED, KNRM, current_module_file);
				exit(-2);
			}
		}
	| i1_me1_element NEWLINE


	| I1_CH NEWLINE
		{
			Token_t *t =  & $<objVal>1;
			int ch_unit = t->unit_id;

			if (strcmp(current_module, expected_sequence[current]) != 0) {
				fprintf(stderr, "%sUnexpected%s: I1-CH%d preceeded by %s instead of %s\n",
					KRED, KNRM, ch_unit, current_module, expected_sequence[current]);
			}
			// first, we close the previous module
			if (mfp != NULL) {
				fclose(mfp);
			}
			// secondly, we move to the next module
			current++;
			current_module = expected_sequence[current]; // i1ch{ch_unit}
			if (strcmp(current_module, "EoA") == 0) {
				fprintf(stderr, "\n%sUnexpected structure of the accelerator. This could be either an error or a result of a design modification. Please verify.%s\n", KYEL, KNRM);
				exit(0);
			}
			sprintf(current_module_file, "%s/%s%s", MODULE_DIR, current_module, MODULE_SUFFIX);
			// third, we open the current module -- in this case, I1_CH{ch_unit}
			mfp = fopen(current_module_file, "w");
			if (mfp == NULL) {
				fprintf(stderr, "%sError%s: cannot open module %s\n", KRED, KNRM, current_module_file);
				exit(-2);
			}
		}
	| i1_ch_element NEWLINE



	| I1_ME2 NEWLINE
		{
			if (strcmp(current_module, expected_sequence[current]) != 0) {
				fprintf(stderr, "%sUnexpected%s: I1-ME2 preceeded by %s instead of %s\n",
					KRED, KNRM, current_module, expected_sequence[current]);
			}
			// first, we close the previous module
			if (mfp != NULL) {
				fclose(mfp);
			}
			// secondly, we move to the next module
			current++;
			current_module = expected_sequence[current]; // i1me2
			if (strcmp(current_module, "EoA") == 0) {
				fprintf(stderr, "\n%sUnexpected structure of the accelerator. This could be either an error or a result of a design modification. Please verify.%s\n", KYEL, KNRM);
				exit(0);
			}
			sprintf(current_module_file, "%s/%s%s", MODULE_DIR, current_module, MODULE_SUFFIX);
			// third, we open the current module -- in this case, I1-ME2
			mfp = fopen(current_module_file, "w");
			if (mfp == NULL) {
				fprintf(stderr, "%sError%s: cannot open module %s\n", KRED, KNRM, current_module_file);
				exit(-2);
			}
		}
	| i1_me2_element NEWLINE


	| I1_ME3 NEWLINE
		{
			if (strcmp(current_module, expected_sequence[current]) != 0) {
				fprintf(stderr, "%sUnexpected%s: I1-ME3 preceeded by %s instead of %s\n",
					KRED, KNRM, current_module, expected_sequence[current]);
			}
			// first, we close the previous module
			if (mfp != NULL) {
				fclose(mfp);
			}
			// secondly, we move to the next module
			current++;
			current_module = expected_sequence[current]; // i1me3
			if (strcmp(current_module, "EoA") == 0) {
				fprintf(stderr, "\n%sUnexpected structure of the accelerator. This could be either an error or a result of a design modification. Please verify.%s\n", KYEL, KNRM);
				exit(0);
			}
			sprintf(current_module_file, "%s/%s%s", MODULE_DIR, current_module, MODULE_SUFFIX);
			// third, we open the current module -- in this case, I1-ME3
			mfp = fopen(current_module_file, "w");
			if (mfp == NULL) {
				fprintf(stderr, "%sError%s: cannot open module %s\n", KRED, KNRM, current_module_file);
				exit(-2);
			}
		}
	| i1_me3_element NEWLINE

	| LB_ME3 NEWLINE
		{
			if (strcmp(current_module, expected_sequence[current]) != 0) {
				fprintf(stderr, "%sUnexpected%s: LB-ME3 preceeded by %s instead of %s\n",
					KRED, KNRM, current_module, expected_sequence[current]);
			}
			// first, we close the previous module
			if (mfp != NULL) {
				fclose(mfp);
			}
			// secondly, we move to the next module
			current++;
			current_module = expected_sequence[current]; // lbme3
			if (strcmp(current_module, "EoA") == 0) {
				fprintf(stderr, "\n%sUnexpected structure of the accelerator. This could be either an error or a result of a design modification. Please verify.%s\n", KYEL, KNRM);
				exit(0);
			}
			sprintf(current_module_file, "%s/%s%s", MODULE_DIR, current_module, MODULE_SUFFIX);
			// third, we open the current module -- in this case, LB-ME3
			mfp = fopen(current_module_file, "w");
			if (mfp == NULL) {
				fprintf(stderr, "%sError%s: cannot open module %s\n", KRED, KNRM, current_module_file);
				exit(-2);
			}
		}
	| lb_me3_element NEWLINE

	| LB_Sxx NEWLINE
		{
			Token_t *t =  & $<objVal>1;
			int ch_unit = t->unit_id;

			if (strcmp(current_module, expected_sequence[current]) != 0) {
				fprintf(stderr, "%sUnexpected%s: LB-S%02d preceeded by %s instead of %s\n",
					KRED, KNRM, ch_unit, current_module, expected_sequence[current]);
			}
			// first, we close the previous module
			if (mfp != NULL) {
				fclose(mfp);
			}
			// secondly, we move to the next module
			current++;
			current_module = expected_sequence[current]; // lb-s{ch_unit}
			if (strcmp(current_module, "EoA") == 0) {
				fprintf(stderr, "\n%sUnexpected structure of the accelerator. This could be either an error or a result of a design modification. Please verify.%s\n", KYEL, KNRM);
				exit(0);
			}
			sprintf(current_module_file, "%s/%s%s", MODULE_DIR, current_module, MODULE_SUFFIX);
			// third, we open the current module -- in this case, LB-S{ch_unit}
			mfp = fopen(current_module_file, "w");
			if (mfp == NULL) {
				fprintf(stderr, "%sError%s: cannot open module %s\n", KRED, KNRM, current_module_file);
				exit(-2);
			}
		}
	| lb_s_element NEWLINE

	;



i1_is_group:	I1_IS_OP_REFP | I1_IS_SRC_SRCA | I1_IS_DF_EINZ
	   ;

i1_is_element:	i1_is_group
	     	{
			storeObj(mfp, "i1is", $<objVal>$);
		}
	     ;


i1_le_group:	I1_LE_DF_SOL | I1_LE_DF_CORV | I1_LE_DF_CORH | I1_LE_DF_CHOP | I1_LE_BD_BCM |
		I1_LE_VAC_VVALVE | I1_LE_ICD_COLH | I1_LE_ICD_COLV | I1_LE_BD_EMIT
	   ;
i1_le_element:	i1_le_group 
	     	{
			storeObj(mfp, current_module, $<objVal>$);
		}
		|
		varia_i1le
	     ;

varia_i1le_obj:	I1_BD_FCUP
	;

varia_i1le:	varia_i1le_obj
	{
		storeObj(mfp, current_module, $<objVal>$);
	}
	;



i1_rfq_group:	I1_RFQ_RF_RFQ | RFQ_GAP_RMS_FFS | RFQ_CELL | I1_RFQ_OP_REFP | I1_RFQ_OP_RP | 
		ERROR_RFQ_CEL_NCPL_STAT | ERROR_RFQ_CEL_NCPL_DYN
	   ;
i1_rfq_element:	i1_rfq_group 
	     	{
			storeObj(mfp, current_module, $<objVal>$);
		}
	     ;


i1_me1_group:	I1_ME1_DF_CORXY | I1_ME1_RF_CAV | I1_ME1_BD_BPM | I1_ME1_DF_QU | I1_ME1_BD_BCM | I1_ME1_VAC_VVALVE
	   ;
i1_me1_element:	i1_me1_group 
	     	{
			storeObj(mfp, current_module, $<objVal>$);
		}
	     ;

i1_ch_group:	I1_CHxx_DF_QU | I1_CHxx_RF_CAV | I1_CHxx_BD_BPM | I1_CHxx_DF_CORXY
	   ;
i1_ch_element:	i1_ch_group 
	     	{
			storeObj(mfp, current_module, $<objVal>$);
		}
	     ;

i1_me2_group:	I1_ME2_DF_QU | I1_ME2_BD_BCM | I1_ME2_BD_BPM | I1_ME2_RF_CAV | I1_ME2_DF_CORXY
	   ;
i1_me2_element:	i1_me2_group 
	     	{
			storeObj(mfp, current_module, $<objVal>$);
		}
	     ;

i1_me3_group:	I1_ME3_DF_QU | I1_ME3_BD_BPM | I1_ME3_RF_CAV | I1_ME3_DF_CORXY | I1_ME3_OP_RP | I1_ME3_MAG_DI |
		I1_ME3_MAG_QU | I1_ME3_ICD_COLH | I1_ME3_ICD_COLV
	   ;
i1_me3_element:	i1_me3_group 
	     	{
			storeObj(mfp, current_module, $<objVal>$);
		}
	     ;

lb_me3_group:	LB_ME3_MAG_DI | LB_ME3_BD_BPM | LB_ME3_DF_CORH | LB_ME3_DF_CORV | LB_ME3_RF_CAV | LB_ME3_MAG_QU | LBL_ME3_OP_REFP
	   ;
lb_me3_element:	lb_me3_group 
	     	{
			storeObj(mfp, current_module, $<objVal>$);
		}
	     ;

lb_s_group:	LB_Sxx_BD_BPM | LB_SxxMAG_QU | LB_Sxx_DF_CORH | LB_Sxx_DF_CORV | LB_Sxx_MAG_QU | LB_Sxx_RF_CAV
	   ;
lb_s_element:	lb_s_group 
	     	{
			storeObj(mfp, current_module, $<objVal>$);
		}
	     ;


%%
void yyerror (char const *s) {
	fprintf (stderr, "%sParsing error%s: %s\n", KRED, KNRM, s);
	fprintf (stderr, "(TraceWin command not yet included in the tracex grammar.)\n");
	exit(1);
}

int storeObj(FILE *f, char *actions_list, Token_t objVal) {
	static char *old_actions_list;
	int i;

	if (old_actions_list == NULL && actions_list == NULL) {
		fprintf(stderr, "%sError%s: actions_list must be initially non NULL.\n", KRED, KNRM);
		return -1;
	}

	if (f == NULL) {
		fprintf(stderr, "%sError%s: module %s is not open\n", KRED, KNRM, current_module_file);
		return -3;
	}

	if (old_actions_list == NULL) old_actions_list = strdup(actions_list);
	else {
		if (strcmp(old_actions_list, actions_list) != 0) {
			free(old_actions_list);
			old_actions_list= strdup(actions_list);
			fprintf(f, "%s = []\n", actions_list);
		}
	}

	fprintf(f, "%s.append(['%s', ", actions_list, tok2str(objVal.token));	// [
	fprintf(f, "%d, [", objVal.n);					//    n,
	for (i=0; i < objVal.n; i++) {					//      [
		fprintf(f, "'%s'", objVal.args[i]);			//         args
		if (i+1 < objVal.n)
			fprintf(f, ", ");
	}
	fprintf(f, "], ");						//      ],
	fprintf(f, "%d, ", yylval.objVal.unit_id);			//    unit_id,
	fprintf(f, "%s", yylval.objVal.commented_out? "True":"False");	//    commented_out
	fprintf(f, "])\n");						// ]

	return 0;
}

int main() {
	int i;

	current_module = expected_sequence[current]; // external
	sprintf(current_module_file, "%s/%s%s", MODULE_DIR, current_module, MODULE_SUFFIX);
	mfp = fopen(current_module_file, "w");
	if (mfp == NULL) {
		mfp = stdout;
	}
	fprintf(mfp, "%s = []\n\n", current_module);

	fprintf(stderr, "Parsing begins...\n");
	while (yyparse()) ;
	fprintf(stderr, "...Parsing ends.\n");
	
	if (mfp != NULL && mfp != stdout) fclose(mfp);

	sprintf(current_module_file, "%s/%s", MODULE_DIR, "TraceWin.py");
	mfp = fopen(current_module_file, "w");
	if (mfp == NULL) {
		fprintf(stderr, "%sError%s: cannot open TraceWin.py for writing -- aborting...\n", KRED, KNRM);
		exit(-2);
	}
	fprintf(mfp, "#\n");
	fprintf(mfp, "# TraceWin.py module -- created by tracex on ");
	scat(mfp, "date");
	//fprintf(mfp, "\n");
	fprintf(mfp, "#\n");
	fprintf(mfp, "#  - for each component of the accelerator, a list is defined with the TraceWin commands\n");
	fprintf(mfp, "#    pertaining to that component.\n");
	fprintf(mfp, "#  - the 'modules' dictionary associates a component to its list\n");
	fprintf(mfp, "#\n");
	fprintf(mfp, "# Use as follows:\n");
	fprintf(mfp, "#     from TraceWin import modules\n");
	fprintf(mfp, "\n");
	fprintf(mfp, "\n");
	fprintf(mfp, "\nmodules = {}\n\n");
	for (i=0; i <= current; i++) {
		current_module = expected_sequence[i];
		fprintf(mfp, "from %s_module import %s\n", current_module, current_module);
		fprintf(mfp, "modules['%s'] = %s\n", current_module, current_module);
	}
	fclose(mfp);
	return 0;
}

//
// EoF tracex.y
//
