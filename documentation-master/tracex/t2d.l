%{
#include "vdfio.h"

#define MAX_ENTRIES	512

char *names[MAX_ENTRIES];
//char *numbers[MAX_ENTRIES];

char **pnames   = names   + 2; // pnames  [-2 .. MAX_ENTRIES-2]
//char **pnumbers = numbers + 2; // pnumbers[-2 .. MAX_ENTRIES-2]

bool dictionary = false,
     verbose	= false;

int minum  =  MAX_ENTRIES;
int maxnum = -MAX_ENTRIES;
%}

%s INSIDE_ENUM

%%

"enum yytokentype"	{
				BEGIN(INSIDE_ENUM);
			}

<INSIDE_ENUM>[A-Z][A-Za-z_0-9]*[ ][=][ ][-]?[0-9]*[, ]	{
							char *nambegin = yytext;
							char *namend = strchr(nambegin, ' ');
							char *numbegin = namend + 2;
							char *comma_or_space;
							int   number;
							/*
							static int n;
							n++;
							if (n == 107)
								printf("n=%d\n", n);
								*/
							*namend = '\0';
							comma_or_space		  = strchr(numbegin, ',');
							if (comma_or_space == NULL) strchr(numbegin, ' ');
							if (comma_or_space != NULL) *comma_or_space = '\0';
							// printf("%s -> %s\n", nambegin, numbegin);
							number = atoi(numbegin);
							if (number > -3 && number < MAX_ENTRIES-2) {
								pnames[number] = strdup(nambegin);
								if (number < minum)  minum  = number;
								if (number > maxnum) maxnum = number;
							}
							else {
								fprintf(stderr, "Illegal number: ignored.\n");
								fprintf(stderr, "Offending combination: %s -> %s\n", nambegin, numbegin);
							}
						}

<INSIDE_ENUM>[}][;]	{
				BEGIN(INITIAL);
			}

(.|\n)		;

%%

int main(int argc, char *argv[]) {
	int i;
	FILE *f;
	char *output_file = NULL;
	int offset = -minum;

	for (i=1; i < argc; ++i)
	  if (argv[i][0] == '-')
	    switch(argv[i][1]) {
	    case 'v':
	    	verbose = true;
		break;
	    case 'o':	/* outputfile */
		if (i + 1 < argc)
			output_file = argv[++i];
		else {
	    	fprintf(stderr, "%s: malformed command line -- bailing out...\n", argv[0]);
		exit(ERR_MISSING_ARG);
		}
		break;
	    default: 
	    	fprintf(stderr, "%s: unknown command line option -- don't know what to do, so I bail out...\n", argv[0]);
		exit(ERR_WRONG_ARG);
	    }
	  else {
	    	fprintf(stderr, "%s: unknown command line option -- don't know what to do, so I bail out...\n", argv[0]);
		exit(ERR_WRONG_ARG);
	}

	/************** Call the Scanner *************/
			while (yylex()) ;
	/*********************************************/

	if (output_file) {
		f = fopen(output_file, "w");
		if (f == NULL) {
		fprintf(stderr, "%s: fopen(%s) failed, so I bail out...\n", argv[0], output_file);
		exit(ERR_CANT_ACCESS);
		}
	}
	else f = stdout;


	fprintf(f, "// Token numbers to token names dictionary. Created by %s on ", argv[0]);
	scat(f, "date");
	fprintf(f, "\n\n");
	fprintf(f, "#include <stdio.h>\n");
	fprintf(f, "#include <stdlib.h>\n");
	fprintf(f, "#include <string.h>\n");

	fprintf(f, "#define MINUM	(%d)\n", minum);
	fprintf(f, "#define MAXNUM	(%d)\n", maxnum);
	fprintf(f, "#define OFFSET	(%d)\n\n", -minum);

	fprintf(f, "char *tnum2tnam[] = {\n");

	for (i=minum; i<=maxnum; i++) {
		fprintf(f, "/* %3d */ ", i);
		if (pnames[i])
			fprintf(f, "\"%s\"", pnames[i]);
		else
			fprintf(f, "NULL");
		if (i < maxnum) fprintf(f, ",");
		fprintf(f, "\n");
	}

	fprintf(f, "      };\n\n");
	fprintf(f, "char *tok2str(int n) {\n");
	fprintf(f, "\tif (n>=MINUM && n<=MAXNUM) return tnum2tnam[n+OFFSET];\n");
	fprintf(f, "\treturn NULL;\n}\n\n");
	if (f != stdout)
		fclose(f);
}

/*
YYEMPTY ->  -2
YYEOF ->  0
YYerror ->  256
YYUNDEF ->  257
I1_IS ->  258
I1_IS_OP_REFP ->  259
*/
