#!/usr/bin/env bash
#
# Module that creates the MEBT3 segment's SVG and HTML files
#
# by vdflorio
version="0.1"		# Mo 07 Jun 2021 11:16:34 CEST

module=$1
first=$2	# i1
second=$3	# lb

i1me3=i1-me3.png
lbme3=lb-me3.png

plant="java -jar ~/bin/plantuml.jar -encodesprite"
flow="~/Documents/Git/documentation/tracex/modules/flow.py"

SZ1=200
SZ2=240

SZ1=100
SZ2=120

convert $i1me3  -scale x$SZ2 i1me3.png
convert $lbme3  -scale x$SZ2 lbme3.png

eval $plant i1me3.png  > i1me3.sprite
eval $plant lbme3.png  > lbme3.sprite

eval  $flow -g i1me3 > i1me3.html
eval  $flow -g lbme3 > lbme3.html

echo '@startuml'				> $module.plantuml
echo '!include i1me3.sprite'			>> $module.plantuml
echo '!include lbme3.sprite'			>> $module.plantuml
echo						>> $module.plantuml
echo 'UP :  Move one level up the hirarchy'	>> $module.plantuml
echo						>> $module.plantuml
echo "I1ME3   : <\$i1me3>  I1-ME3"		>> $module.plantuml
echo "LBME3   : <\$lbme3>  LB-ME3"		>> $module.plantuml

echo "url of UP     is [[http://tdr.myrrha.lan/vfolders/tracex/linac.svg]]"  >> $module.plantuml
echo "url of I1ME3  is [[http://tdr.myrrha.lan/vfolders/tracex/i1me3.html]]" >> $module.plantuml
echo "url of LBME3  is [[http://tdr.myrrha.lan/vfolders/tracex/lbme3.html]]" >> $module.plantuml

echo "UP     -down-> I1ME3"	>> $module.plantuml
echo "I1ME3  -> LBME3"		>> $module.plantuml

echo '@enduml' >> $module.plantuml


export PLANTUML_LIMIT_SIZE=100000
java -jar ~/bin/plantuml.jar -tsvg $module.plantuml

scp $module.svg admin@10.5.3.18:public_html/vfolders/tracex/
scp i1me3.html lbme3.html admin@10.5.3.18:public_html/vfolders/tracex/
