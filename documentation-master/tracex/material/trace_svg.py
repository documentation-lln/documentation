#!/usr/bin/env python

#
# trace_svg: creates a hierarchy of svg and html files describing the contents of a TraceWin code
#
# By vdflorio
#
version = "0.1"     # Sa 05 Jun 2021 21:32:51 CEST

import sys
sys.path.append('/home/vdflorio/Documents/Git/documentation/tracex/modules')
from TraceWin import modules

import re, math, subprocess

module_classes = {}

def are_of_the_same_class(a, b):
    if a[:3] == b[:3]:
        return True
    return False

def run_length_encoding():
    keys = list(modules.keys())
    len_keys = len(keys)

    rle = [ [ keys[0], 1 ] ]
    for i in range(1,len_keys):
        if are_of_the_same_class(rle[-1][0], keys[i]):
            rle[-1][1] += 1
        else:
            rle.append( [keys[i], 1] )
    rledict = {}
    for r in rle:
        [a, b] = r
        rledict[a] = b
    return rledict

def analyze(regex):
    p = re.compile(regex)
    mi = math.inf
    ma = -math.inf
    num = 0
    for key in modules:
        m = p.match(key)
        if m:
            # print('{} matched the regex. Group is {}'.format(key, m.group(1)))
            num += 1
            nm = int(m.group(1))
            if nm < mi: mi = nm
            if nm > ma: ma = nm
    return (num, mi, ma)

def analyze_modules():
    (chnum, chmin, chmax) = analyze("i1ch([0-9]*)$")
    module_classes['i1ch'] = (chnum, chmin, chmax)
    (lbsnum, lbsmin, lbsmax) = analyze("lbs([0-9]*)$")
    module_classes['lbs'] = (lbsnum, lbsmin, lbsmax)

def main():
    rle_dict = run_length_encoding()
    # print(rle_dict)
    ch_one_min = 1
    ch_one_max = rle_dict['i1ch1']
    ch_two_min = ch_one_max + 1
    ch_two_min_str = f'i1ch{ch_two_min}'
    ch_two_max = rle_dict[ch_two_min_str] + ch_two_min - 1
    lbsmin = 1
    lbsmax = rle_dict['lbs01']

    print(f'Creating SVG generator: CH [{ch_one_min}, {ch_one_max}], CH [{ch_two_min}, {ch_two_max}], LB-S [{lbsmin}, {lbsmax}]')

    cmd = subprocess.run(['template.sh', 'gen_svg.sh', str(ch_one_min), str(ch_one_max), str(ch_two_min), str(ch_two_max), str(lbsmin), str(lbsmax)])

    if cmd.returncode != 0:
        print(f"template substitution failed. Return code is {cmd.returncode}", file=sys.stderr)
    else:
        print("SVG generator created in file gen_svg.sh", file=sys.stderr)



if __name__ == "__main__":
    # execute only if run as a script
    main()
