#!/usr/bin/env bash

#
# proof of concepts of the module that creates the CH segments' SVG and HTML files
#
# by vdflorio

version="0.1"		# Mo 07 Jun 2021 11:23:57 CEST

module=linac

i1le=i1le.png # ~/Documents/Git/documentation/TDR/MYRRHATDRPartB-img/Vacuum20System-img002.png
i1is=monogan-M1000.png
i1rfq=../MYRRHATDRPartB-img/RFQv13-img007.png
i1me1=i1me1.png
i1ch=i1ch.png
i1me2=i1me2.png
mebt3=mebt-3.png
lbs=qmodule.png

plant="java -jar ~/bin/plantuml.jar -encodesprite"
flow="~/Documents/Git/documentation/tracex/modules/flow.py"

SZ1=200
SZ2=240

SZ1=100
SZ2=120

convert $i1is  -scale x$SZ1 i1is.png
convert $i1le  -scale x$SZ2 i1le.png
convert $i1rfq -scale x$SZ2 i1rfq.png
convert $i1me1 -scale x$SZ2 i1me1.png
convert $i1ch  -scale x$SZ2 i1ch.png
convert $i1me2 -scale x$SZ2 i1me2.png
convert $mebt3 -scale x$SZ2 mebt3.png
convert $lbs   -scale x$SZ2 lbs.png

eval $plant i1is.png  > i1is.sprite
eval $plant i1le.png  > i1le.sprite
eval $plant i1rfq.png > i1rfq.sprite
eval $plant i1me1.png > i1me1.sprite
eval $plant i1ch.png  > i1ch.sprite
eval $plant i1me2.png > i1me2.sprite
eval $plant mebt3.png > mebt3.sprite
eval $plant lbs.png   > lbs.sprite

notei1is=$(eval  $flow -g i1is)
notei1le=$(eval  $flow -g i1le)
notei1rfq=$(eval $flow -g i1rfq)
notei1me1=$(eval $flow -g i1me1)

notei1me2=$(eval $flow -g i1me2)

eval  $flow -g i1is > i1is.html
eval  $flow -g i1le > i1le.html
eval $flow -g i1rfq > i1rfq.html
eval $flow -g i1me1 > i1me1.html

eval $flow -g i1me2 > i1me2.html

cat <<__PLanT__ > $module.plantuml
@startuml
!include i1is.sprite
!include i1le.sprite
!include i1rfq.sprite
!include i1me1.sprite
!include i1ch.sprite
!include i1me2.sprite
!include mebt3.sprite
!include lbs.sprite

Source : <\$i1is>  I1-IS
LEBT   : <\$i1le>  I1-LE
RFQ    : <\$i1rfq> I1-RFQ
MEBT1  : <\$i1me1> I1-ME1
CH1_7  : <\$i1ch> I1-CH01 - I1-CH07
MEBT2  : <\$i1me2> I1-ME2
CH8_15 : <\$i1ch> I1-CH08 - I1-CH15
MEBT3  : <\$mebt3> I1-ME3, LB-ME3
LBS    : <\$lbs> LB-S

url of Source is [[http://tdr.myrrha.lan/vfolders/tracex/i1is.html]]
url of LEBT   is [[http://tdr.myrrha.lan/vfolders/tracex/i1le.html]]
url of RFQ    is [[http://tdr.myrrha.lan/vfolders/tracex/i1rfq.html]]
url of MEBT1  is [[http://tdr.myrrha.lan/vfolders/tracex/i1me1.html]]
url of CH1_7  is [[http://tdr.myrrha.lan/vfolders/tracex/i1ch1_7.svg]]
url of MEBT2  is [[http://tdr.myrrha.lan/vfolders/tracex/i1me2.html]]
url of CH8_15 is [[http://tdr.myrrha.lan/vfolders/tracex/i1ch8_15.svg]]
url of MEBT3  is [[http://tdr.myrrha.lan/vfolders/tracex/mebt3.svg]]
url of LBS    is [[http://tdr.myrrha.lan/vfolders/tracex/lbs.svg]]

Source -> LEBT
LEBT   -> RFQ
RFQ    -> MEBT1
MEBT1  -down-> CH1_7
CH1_7  -left-> MEBT2
MEBT2  -left-> CH8_15
CH8_15 -down-> MEBT3
MEBT3  -> LBS

@enduml
__PLanT__

# Create i1ch1_7.svg
ch_render.sh i1ch1_7 i1 ch 1 7

# Create i1ch8_15.svg
ch_render.sh i1ch8_15 i1 ch 8 15

# Create mebt3.svg
mebt3_render.sh mebt3 i1 lb

# Create lbs.svg
lbs_render.sh lbs lb s 1 23

export PLANTUML_LIMIT_SIZE=100000
java -jar ~/bin/plantuml.jar -tsvg $module.plantuml

scp $module.svg admin@10.5.3.18:public_html/vfolders/tracex/
#scp *.html admin@10.5.3.18:public_html/vfolders/tracex/
scp i1is.html i1le.html i1rfq.html i1me1.html i1me2.html admin@10.5.3.18:public_html/vfolders/tracex/
