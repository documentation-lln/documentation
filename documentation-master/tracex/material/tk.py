#!/usr/bin/env python3

# Python program to illustrate the usage
# of hierarchical treeview in python GUI
# application using tkinter

# From https://www.geeksforgeeks.org/hierarchical-treeview-in-python-gui-application/
# Modified by vdflorio so as to display the accelerator structure and corresponding TraceWin
# statements
 
# Importing tkinter
from tkinter import * 
 
# Importing ttk from tkinter
from tkinter import ttk 
 
import sys
sys.path.append('/home/vdflorio/Documents/Git/documentation/tracex/modules')
from TraceWin import modules
from flow import getActions

import re, math, subprocess

item2module = {
        'Source':   'i1is',
        'LEBT':     'i1le',
        'RFQ':      'i1rfq',
        'MEBT-1':   'i1me1',
        'MEBT-2':   'i1me2',
        'I1-ME3':   'i1me3',
        'LB-ME3':   'lbme3',
        }
for i in range(1, 20):
    item2module[f'I1-CH{i}'] = f'i1ch{i}'
for i in range(9):
    item2module[f'LB-S0{i}'] = f'lbs0{i}'
for i in range(10, 40):
    item2module[f'LB-S{i}'] = f'lbs{i}'

module_classes = {}

def are_of_the_same_class(a, b):
    if a[:3] == b[:3]:
        return True
    return False

def run_length_encoding():
    keys = list(modules.keys())
    len_keys = len(keys)

    rle = [ [ keys[0], 1 ] ]
    for i in range(1,len_keys):
        if are_of_the_same_class(rle[-1][0], keys[i]):
            rle[-1][1] += 1
        else:
            rle.append( [keys[i], 1] )
    rledict = {}
    for r in rle:
        [a, b] = r
        rledict[a] = b
    return rledict

rle_dict = run_length_encoding()
# print(rle_dict)
ch_one_min = 1
ch_one_max = rle_dict['i1ch1']
ch_two_min = ch_one_max + 1
ch_two_min_str = f'i1ch{ch_two_min}'
ch_two_max = rle_dict[ch_two_min_str] + ch_two_min - 1
lbsmin = 1
lbsmax = rle_dict['lbs01']

print(f'Creating tkinter treeview: CH [{ch_one_min}, {ch_one_max}], CH [{ch_two_min}, {ch_two_max}], LB-S [{lbsmin}, {lbsmax}]')

#  cmd = subprocess.run(['template.sh', 'gen_svg.sh', str(ch_one_min), str(ch_one_max), str(ch_two_min), str(ch_two_max), str(lbsmin), str(lbsmax)])
#  
#  if cmd.returncode != 0:
#      print(f"template substitution failed. Return code is {cmd.returncode}", file=sys.stderr)
#  else:
#      print("SVG generator created in file gen_svg.sh", file=sys.stderr)

# Creating app window
app = Tk() 

varMessage = StringVar(app, value="TraceWin methods associated to\nthe clicked upon module.")

app.geometry("1280x720")

# Defining title of the app
app.title("Testing tkinter") 
 
# Defining label of the app and calling a geometry
# management method i.e, pack in order to organize
# widgets in form of blocks before locating them
# in the parent widget
ttk.Label(app, text ="Accelerator View").pack()
 
# Creating treeview window
treeview = ttk.Treeview(app,
		height=30) 

# function to be called when mouse enters in a frame
def enter(event):
    print('<Enter> event at x = % d, y = % d'%(event.x, event.y))
 
# function to be called when when mouse exits the frame
def Button2(event):
    print('Button-2 pressed at x = % d, y = % d'%(event.x, event.y))

def update_item():
    selected = tv.focus()
    temp = tv.item(selected, 'values')
    sal_up = float(temp[2]) + float(temp[2]) * 0.05
    tv.item(selected, values=(temp[0], temp[1], sal_up))

def selected_item():
    selected = treeview.focus()
    temp = treeview.item(selected, 'text') #, 'values')
    print(temp)
    return temp
    # print(temp['text'])
    # print(temp.text)	# {'text': 'MEBT-1', 'image': '', 'values': '', 'open': 0, 'tags': ''} 

# function to be called when when mouse exits the frame
def exit_(event):
    print('<Leave> event at x = % d, y = % d'%(event.x, event.y))

def Button1(event):
    #print('Button-1 pressed at x = % d, y = % d'%(event.x, event.y))
    r = selected_item()
    if r in item2module:
        r = item2module[r]
    if r in modules:
        s = getActions(r, modules)
        newmsg = ""
        for line in s:
            print(line, end='')
            newmsg += line
        varMessage.set(newmsg)
    else:
        varMessage.set(f'The user selected entry {r}')
    print(event)
 

#treeview.bind('<Enter>', enter)
#treeview.bind('<Leave>', exit_)
treeview.bind('<ButtonRelease>', Button1)
# treeview.bind('<Button>', Button1)

#treeview.bind('<Button-2>', Button2)

# Create a button widget and
# map the command parameter to
# selected_item function
btn = Button('', text='Print Selected', command=selected_item)

# Placing the button and listbox
btn.pack(side='bottom')


msg = Message(app, textvariable = varMessage) # s/Scrollbar/Message
msg.config(bg='lightgreen', font=('times', 12, 'italic'))
msg.pack(side='right')

# Calling pack method on the treeview
treeview.pack(fill='both') # 'x' 
 
# Inserting items to the treeview
# Inserting parent
treeview.insert('', '0', 'item1',
                text ='Accelerator')
 
# Inserting child
treeview.insert('', '1', 'item2',
                text ='Source')
treeview.insert('', '2', 'item3',
                text ='LEBT')
treeview.insert('', '3', 'item4',
                text ='RFQ')
treeview.insert('', '4', 'item5',
                text ='MEBT-1')
treeview.insert('', '5', 'item6',
                text =f'CH{ch_one_min}-{ch_one_max}')
treeview.insert('', '6', 'item7',
                text ='MEBT-2')
treeview.insert('', '7', 'item8',
                text =f'CH{ch_two_min}-{ch_two_max}')
treeview.insert('', '8', 'item9',
                text ='MEBT-3')
treeview.insert('', 'end', 'item10',
                text ='LB-S')

 
for i in range(ch_one_min, ch_one_max+1):
    treeview.insert('item6', 'end', f'CH-{i}', text =f'I1-CH{i}') 

for i in range(ch_two_min, ch_two_max+1):
    treeview.insert('item8', 'end', f'CH-{i}', text =f'I1-CH{i}') 

# Inserting more than one attribute of an item
treeview.insert('item9', 'end', 'I1-ME3',
                text ='I1-ME3') 
treeview.insert('item9', 'end', 'LB-ME3',
                text ='LB-ME3')

for i in range(lbsmin, lbsmax+1):
    if i<10:
        treeview.insert('item10', 'end', f'LB-S0{i}', text =f'LB-S0{i}') 
    else:
        treeview.insert('item10', 'end', f'LB-S{i}', text =f'LB-S{i}') 


# Placing each child items in parent widget
treeview.move('item2', 'item1', 'end') 
treeview.move('item3', 'item1', 'end')
treeview.move('item4', 'item1', 'end')
treeview.move('item5', 'item1', 'end')
treeview.move('item6', 'item1', 'end')
treeview.move('item7', 'item1', 'end')
treeview.move('item8', 'item1', 'end')
treeview.move('item9', 'item1', 'end')
treeview.move('item10', 'item1', 'end')

# Calling main() 
app.mainloop()
