#!/usr/bin/env bash

#
# Module that creates the CH segments' SVG and HTML files
#
# by vdflorio
version="0.1"		# Mo 07 Jun 2021 11:13:20 CEST

module=$1
elem1=$2	# i1
elem2=$3	# ch
from=$4
to=$5

i1le=i1le.png # ../MYRRHATDRPartB-img/Vacuum20System-img002.png
i1is=monogan-M1000.png
i1rfq=~/Documents/Git/documentation/TDR/MYRRHATDRPartB-img/RFQv13-img007.png
i1me1=i1me1.png
i1ch=i1ch.png
i1me2=i1me2.png
mebt3=mebt-3.png
lbs=qmodule.png

plant="java -jar ~/bin/plantuml.jar -encodesprite"
flow="~/Documents/Git/documentation/tracex/modules/flow.py"

SZ1=200
SZ2=240

SZ1=100
SZ2=120

convert $i1ch  -scale x$SZ2 i1ch.png

eval $plant i1ch.png  > i1ch.sprite

for i in `seq $from $to` ; do
	eval  $flow -g i1ch$i > i1ch$i.html
done

echo '@startuml'				> $module.plantuml
echo '!include i1ch.sprite'			>> $module.plantuml
echo						>> $module.plantuml
echo 'UP :  Move one level up the hirarchy'	>> $module.plantuml

for i in `seq $from $to` ; do
	echo "CH$i   : <\$i1ch>  I1-CH0$i" >> $module.plantuml
done

echo "url of UP     is [[http://tdr.myrrha.lan/vfolders/tracex/linac.svg]]" >> $module.plantuml

for i in `seq $from $to` ; do
	echo "url of CH$i    is [[http://tdr.myrrha.lan/vfolders/tracex/i1ch$i.html]]"  >> $module.plantuml
done

prev=UP

for i in `seq $from $to` ; do
	echo "$prev -> CH$i" >> $module.plantuml
	prev=CH$i
done

echo '@enduml' >> $module.plantuml


export PLANTUML_LIMIT_SIZE=100000
java -jar ~/bin/plantuml.jar -tsvg $module.plantuml

scp $module.svg admin@10.5.3.18:public_html/vfolders/tracex/
for i in `seq $from $to` ; do
	scp i1ch$i.html  admin@10.5.3.18:public_html/vfolders/tracex/
done
