#!/usr/bin/env bash

#
# Module that creates the LB-S segments' SVG and HTML files
#
# by vdflorio
version="0.1"		# Mo 07 Jun 2021 11:14:16 CEST

module=$1
elem1=$2	# lb
elem2=$3	# s
from=$4
to=$5

i1le=i1le.png # ../MYRRHATDRPartB-img/Vacuum20System-img002.png
i1is=monogan-M1000.png
i1rfq=~/Documents/Git/documentation/TDR/MYRRHATDRPartB-img/RFQv13-img007.png
i1me1=i1me1.png
i1ch=i1ch.png
i1me2=i1me2.png
mebt3=mebt-3.png
lbs=qmodule.png

plant="java -jar ~/bin/plantuml.jar -encodesprite"
flow="~/Documents/Git/documentation/tracex/modules/flow.py"

SZ1=200
SZ2=240

SZ1=50
SZ2=60

convert $lbs  -scale x$SZ2 lbs-s.png

eval $plant lbs-s.png  > lbs-s.sprite

for i in `seq -w $from $to` ; do
	eval  $flow -g lbs$i > lbs$i.html
done

echo '@startuml'				> $module.plantuml
echo '!include lbs-s.sprite'			>> $module.plantuml
echo						>> $module.plantuml
echo 'UP :  Move one level up the hirarchy'	>> $module.plantuml

for i in `seq -w $from $to` ; do
	echo "LBS$i   : <\$lbs>  LB-S$i" >> $module.plantuml
done

echo "url of UP     is [[http://tdr.myrrha.lan/vfolders/tracex/linac.svg]]" >> $module.plantuml

for i in `seq -w $from $to` ; do
	echo "url of LBS$i    is [[http://tdr.myrrha.lan/vfolders/tracex/lbs$i.html]]"  >> $module.plantuml
done

declare -a arrows=("", "-down->" "->" "->" "->" "->" "->" "->" "-down->" "-left->" "-left->" "-left->" "-left->" "-left->" "-left->" "-down->" "->" "->" "->" "->" "->" "->" "-down->" "-left->" "-left->" "-left->" "-left->")

prev=UP
for i in `seq -w $from $to` ; do
	j=$(echo $i | sed 's/^0*//')
	echo "$prev ${arrows[$j]} LBS$i" >> $module.plantuml
	prev=LBS$i
done

echo '@enduml' >> $module.plantuml


export PLANTUML_LIMIT_SIZE=100000
java -jar ~/bin/plantuml.jar -tsvg $module.plantuml

scp $module.svg admin@10.5.3.18:public_html/vfolders/tracex/
for i in `seq -w $from $to` ; do
	scp lbs$i.html  admin@10.5.3.18:public_html/vfolders/tracex/
done
