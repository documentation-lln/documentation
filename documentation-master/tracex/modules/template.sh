#!/usr/bin/env bash

t=LaTeX.template

if [ -z "$1" ]; then
	echo argument 1 is missing -- aborting
	exit -1
fi

if [ -z "$2" ]; then
	echo argument 2 is missing -- aborting
	exit -1
fi

if [ -z "$3" ]; then
	echo argument 3 is missing -- aborting
	exit -1
fi

if [ -z "$4" ]; then
	echo argument 4 is missing -- aborting
	exit -1
fi

if [ -z "$5" ]; then
	echo argument 5 is missing -- aborting
	exit -1
fi

spokemax=$(($5 * 2))

cp $t $1
fgres '@chmin@'  $2 $1
fgres '@chmax@'  $3 $1
fgres '@lbsmin@' $4 $1
fgres '@lbsmax@' $5 $1
fgres '@spokemin@' $4 $1
fgres '@spokemax@' $spokemax $1
