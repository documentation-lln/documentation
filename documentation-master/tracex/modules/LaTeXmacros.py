#!/usr/bin/env python

#
# LaTeXmacros:  analyses the data parsed from the TraceWin, populates a template file of LaTeX macros with
#               the obtained values and creates a set of "living macros" used by the TDR or any other
#               MINERVA "living document".
#
# By vdflorio
#
# v. 0.1 Do 27 Mai 2021 14:40:03 CEST
# version = "0.2"   # Mo 07 Jun 2021 09:59:25 CEST
                    # Minor corrections
# version = "0.3"   # Fr 20 Aug 2021 10:42:17 CEST
                    # Copy of output macros onto TDR's folder
version = "0.4"     # Mo 06 Sep 2021 14:31:26 CEST
                    # Living macros have been introduced

destination = '/home/vdflorio/Documents/Git/documentation/TDR/Annex'
macros = 'tracewin.tex'
module_name = 'tracex/LaTeXmacros'
living_data_module = '/home/vdflorio/Documents/Git/documentation/fault-tracking/livingdata.py'

import shutil, os
from TraceWin import modules
import re, sys, math, subprocess
from datetime import datetime
import calendar

def Now():
    now = datetime.now()
    dnow = {}
    dnow['ss']   = now.second
    dnow['mm']   = now.minute
    dnow['hh']   = now.hour

    dnow['D']   = now.day
    dnow['M']   = now.month
    dnow['Y']   = now.year
    return dnow

module_classes = {}

def analyze(regex):
    p = re.compile(regex)
    mi = math.inf
    ma = -math.inf
    num = 0
    for key in modules:
        m = p.match(key)
        if m:
            # print('{} matched the regex. Group is {}'.format(key, m.group(1)))
            num += 1
            nm = int(m.group(1))
            if nm < mi: mi = nm
            if nm > ma: ma = nm
    return (num, mi, ma)

def partition(regex):
    p = re.compile(regex)
    mi = math.inf
    ma = -math.inf
    num = 0
    nm = None
    for key in modules:
        m = p.match(key)
        if m:
            nm = int(m.group(1))
            if nm < mi: mi = nm
            if nm > ma: ma = nm
        else:
            if nm: return nm
    return 0
            
def analyze_modules():
    (chnum, chmin, chmax) = analyze("i1ch([0-9]*)$")
    module_classes['i1ch'] = (chnum, chmin, chmax)
    (lbsnum, lbsmin, lbsmax) = analyze("lbs([0-9]*)$")
    module_classes['lbs'] = (lbsnum, lbsmin, lbsmax)

def main():
    beforeME2 = partition("i1ch([0-9]*)$")

    analyze_modules()
    (chnum, chmin, chmax) = module_classes['i1ch']
    print(f"Standard CH cavities: {chnum} instances: CH-{chmin} to CH-{beforeME2} and CH-{beforeME2+1} to CH-{chmax}")

    (lbsnum, lbsmin, lbsmax) = module_classes['lbs']
    print(f"Cryomodules: {lbsnum} instances: LB-S{lbsmin} to LB-S{lbsmax}")
    spokemax = lbsmax * 2

    cmd = subprocess.run(['template.sh', macros, str(chmin), str(chmax), str(lbsmin), str(lbsmax)])
    if cmd.returncode != 0:
        print(f"template substitution failed. Return code is {cmd.returncode}", file=sys.stderr)
    else:
        print(f"LaTeX macros have been created in file {macros}", file=sys.stderr)

        freq1 = freq2 = bend = None

        for module in modules['i1le']:
            if module[0] == 'FREQ':
                freq1 = module[2][0]
        for module in modules['i1me3']:
            if 'I1_ME3_MAG_DI' in module[0]:
                bend = module[2][1]
                mmradius = int(module[2][2])
                mradius = mmradius / 1000.0
            if module[0] == 'FREQ':
                freq2 = module[2][0]

        CURLON  = '{'
        CURLOFF = '}'
        with open(macros, 'a') as f:
            f.write(f'% {CURLON}{CURLON}{CURLON} "Living" macros\n') # The three '{' open a text folder

            if freq1 == None:
                freq1  = "176.1"

            try:
                ifreq1 = freq1.split('.')[0]
            except:
                ifreq1 = freq1

            f.write(f'\\def\\TWinIiLEfreq{CURLON}\\SI{CURLON}{freq1}{CURLOFF}{CURLON}\\mega\\hertz{CURLOFF}{CURLOFF}             % default: 176.1\n')
            f.write(f'\\def\\TWinIiLEfreqValue{CURLON}{freq1}{CURLOFF}\n')
            f.write(f'\\def\\TWinIiLEfreqValueAsInt{CURLON}{ifreq1}{CURLOFF}\n\n')

            if freq2 == None:
                freq2  = "352.2"

            try:
                ifreq2 = freq2.split('.')[0]
            except:
                ifreq2 = freq2

            f.write(f'\\def\\TWinIiMEiiiFreq{CURLON}\\SI{CURLON}{freq2}{CURLOFF}{CURLON}\\mega\\hertz{CURLOFF}{CURLOFF}          % default: 352.2\n')
            f.write(f'\\def\\TWinIiMEiiiFreqValue{CURLON}{freq2}{CURLOFF}\n')
            f.write(f'\\def\\TWinIiMEiiiFreqValueAsInt{CURLON}{ifreq2}{CURLOFF}\n\n')

            if bend == None:
                bend = "45"
                mradius = "1.2"
                mmradius = "1200"
            f.write(f'\\def\\TWinIiMEiiiMAGDIbend{CURLON}\\SI{CURLON}{bend}{CURLOFF}{CURLON}\\degree{CURLOFF}{CURLOFF}            % default: 45\n')
            f.write(f'\\def\\TWinIiMEiiiMAGDIbendValue{CURLON}{bend}{CURLOFF}\n\n')

            f.write(f'\\def\\TWinIiMEiiiMAGDImradius{CURLON}\\SI{CURLON}{mradius}{CURLOFF}{CURLON}\\meter{CURLOFF}{CURLOFF}         % default: 1.2\n')
            f.write(f'\\def\\TWinIiMEiiiMAGDImradiusValue{CURLON}{mradius}{CURLOFF}\n\n')

            f.write(f'\\def\\TWinIiMEiiiMAGDImmradius{CURLON}\\SI{CURLON}{mmradius}{CURLOFF}{CURLON}\\milli\\meter{CURLOFF}{CURLOFF} % default: 1200\n')
            f.write(f'\\def\\TWinIiMEiiiMAGDImmradiusValue{CURLON}{mmradius}{CURLOFF}\n\n')

            f.write(f'\\def\\TWinLastCryomodule{CURLON}{lbsmax}{CURLOFF}                            % currently: 23\n')
            f.write(f'\\def\\TWinLastSpoke{CURLON}{spokemax}{CURLOFF}                                 % currently: 46\n')
            f.write(f'\\def\\TWinLastCHcavity{CURLON}{chmax}{CURLOFF}                              % currently: 15\n')
            f.write(f'\\def\\TWinBeforeMEii{CURLON}{beforeME2}{CURLOFF}                                 % currently: 7\n')
            f.write(f'\\def\\TWinAfterMEii{CURLON}{beforeME2+1}{CURLOFF}                                  % currently: 8\n\n')

            val = chmax - beforeME2
            f.write(f'\\def\\TWinCardCHa{CURLON}{beforeME2}{CURLOFF}                                    % currently: 7\n')
            f.write(f'\\def\\TWinCardCHb{CURLON}{val}{CURLOFF}                                    % currently: 8\n\n')

            now  = Now()
            year = now['Y']
            day  = now['D']
            hh   = now['hh']
            mm   = now['mm']
            ss   = now['ss']
            month = calendar.month_name[ now['M'] ]
            f.write(f'\\def\\TWinParseDate{CURLON}{month} {year}{CURLOFF}\n')
            f.write('\n% }}}\n\n')

            f.write('%\n')
            f.write(f'% End of processing ({macros}, created on {month} {day}, {year}, at {hh}:{mm}:{ss} by module {module_name} (v. {version}))\n')
            f.write('%\n')

        print(f'Copying macro file {macros} to destination {destination}')
        shutil.copy(macros, destination)

    print('')

    print(f'Converting image templates:', file=sys.stderr)

    image_location = '/home/vdflorio/Documents/Git/documentation/TDR'
    images  = [ 'MYRRHATDRPartB-img/RFQv13-img001%s.jpg',
                'MYRRHATDRPartB-img/HEBT-100MeV%s.png',
                'MYRRHATDRPartB-img/D-MYRTE-2.6-Fig50%s_t.png',
                'MYRRHATDRPartB-img/D-MYRTE-2.6-Fig50%s.png',
                'MYRRHATDRPartB-img/BeamInstStudies-img034%s.png',
                'MYRRHATDRPartB-img/FastSwitchingMagnets-img001%s_t.png',
                'MYRRHATDRPartB-img/FastSwitchingMagnets-img001%s.png',
                'MYRRHATDRPartA-img/MYRRHATDRPartA-img012%s_t.png',
                'MYRRHATDRPartA-img/MYRRHATDRPartA-img012%s.png',
                'MYRRHATDRPartA-img/MYRRHATDRPartA-img015%s.png',
                'MYRRHATDRPartA-img/MINERVA-ACC-Mnemonics%s_t_t_t_t_t_t_t.png',
                'MYRRHATDRPartA-img/MINERVA-ACC-Mnemonics%s_t_t_t_t_t_t.png',
                'MYRRHATDRPartA-img/MINERVA-ACC-Mnemonics%s_t_t_t_t_t.png',
                'MYRRHATDRPartA-img/MINERVA-ACC-Mnemonics%s_t_t_t_t.png',
                'MYRRHATDRPartA-img/MINERVA-ACC-Mnemonics%s_t_t_t.png',
                'MYRRHATDRPartA-img/MINERVA-ACC-Mnemonics%s_t_t.png',
                'MYRRHATDRPartA-img/MINERVA-ACC-Mnemonics%s_t.png',
                'MYRRHATDRPartA-img/MINERVA-ACC-Mnemonics%s.png',
                'MYRRHATDRPartB-img/MYRRHATDRPartB-img026%s_t.png',
                'MYRRHATDRPartB-img/MYRRHATDRPartB-img026%s.png',
                'MYRRHATDRPartB-img/MYRRHATDRPartB-img070b%s.png',
                ]
    image_cmds  = [ 'convert -font Arial -family Arial -pointsize 14  -annotate +1161+131 %s ' ,
                    'convert -font Arial -family Arial -pointsize 30  -annotate +76+195 %s ' ,
                    'convert -font Arial-Bold -family Arial -pointsize 22  -annotate +543+83  %s ',
                    'convert -font Arial-Bold -family Arial -pointsize 22  -annotate +543+556 %s ',
                    'convert -font Arial -family Arial -pointsize 44  -annotate +188+615 %s ',
                    'convert -font Arial-Bold -family Arial -pointsize 22  -annotate +594+110  %s ',
                    'convert -font Arial-Bold -family Arial -pointsize 22  -annotate +594+608  %s ',
                    'convert -font Arial -family Arial -pointsize 14  -annotate +1118+122 %s ',
                    'convert -font Arial -family Arial -pointsize 14  -annotate +1197+122 %s ',
                    'convert -font Arial -family Arial -pointsize 14  -annotate +1037+24  %s ',
                    'convert -quiet -font Arial -family Arial -pointsize 34  -annotate +716+355  %s ',
                    'convert -font Arial -family Arial -pointsize 34  -fill gray -annotate +721+287  %s ',
                    'convert -font Arial -family Arial -pointsize 34  -annotate +845+614  %s ',
                    'convert -font Arial -family Arial -pointsize 34  -fill gray -annotate +848+28  %s ',
                    'convert -font Arial -family Arial -pointsize 34  -annotate +1152+568  %s ',
                    'convert -font Arial -family Arial -pointsize 34  -fill gray -annotate +1157+73  %s ',
                    'convert -font Arial -family Arial -pointsize 34  -annotate +2298+418  %s ',
                    'convert -font Arial -family Arial -pointsize 34  -annotate +2444+418  %s ',
                    'convert -font Arial -family Arial -pointsize 92  -annotate +5060+1102 %s ',
                    'convert -font Arial -family Arial -pointsize 92  -annotate +1623+1404 %s ',
                    'convert -font Arial -family Arial -pointsize 10  -annotate +322+166   %s ',
                  ]
    param   = [ str(spokemax),
                str(lbsmax),
                bend,
                bend,
                str(chmax),
                bend,
                bend,
                freq2,
                str(spokemax),
                freq2,
                str(beforeME2),
                str(beforeME2),
                '0'+str(beforeME2+1),
                '0'+str(beforeME2+1),
                str(chmax),
                str(chmax),
                str(lbsmax-1),
                str(lbsmax),
                str(beforeME2+1),
                str(beforeME2),
                ifreq2,
              ]

    l = len(images)
    for i, image in enumerate(images):
        cmd = image_cmds[i] % param[i]
        inp = image % '_t'
        out = image % ''
        print(f'{i+1}/{l}: Converting image {out}...', end='', file=sys.stderr)
        return_code = subprocess.call(f'{cmd} {image_location}/{inp} {image_location}/{out}', shell=True)
        if return_code != 0:
            print(f"Image template command failed. Return code is {return_code}", file=sys.stderr)
        else:
            print('', file=sys.stderr)

    print(f'\nRetrieving living data from the control room via module {living_data_module}...', end='', file=sys.stderr)
    return_code = subprocess.call(f'{living_data_module}', shell=True)
    # print(' Storing consistency checking code... ', end='', file=sys.stderr)
    if return_code != 0:
        print(f"Module {living_data_module} failed. Return code is {return_code}.", file=sys.stderr)
    else:
        print('Done.', file=sys.stderr)


if __name__ == "__main__":
    # execute only if run as a script
    main()
