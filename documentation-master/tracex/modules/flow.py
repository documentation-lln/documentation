#!/usr/bin/env python

# {{{ flow.py -- prologue
#
# Constructs a PlantUML diagram from TraceWin data
#
# version = "0.2"   # Di 01 Jun 2021 10:15:23 CEST
                    # Added text folders
# version = "0.3"   # Di 01 Jun 2021 10:15:23 CEST
                    # Added option to create and transfer SVG file
# version = "0.4"   # Mi 02 Jun 2021 10:39:59 CEST
                    # Added option '-j' (JSON output)
version = "0.5"     # Do 03 Jun 2021 13:20:56 CEST
                    # Added option '-g' (get actions)
#
# by vdflorio
#

from termcolor import colored
import sys, getopt
import subprocess
import json
import os, tempfile
sys.path.append('/home/vdflorio/Documents/Git/documentation/tracex/modules')
from TraceWin import modules

# }}}

#{{{ filestr: a mini-class to redirect function writes into a string
class filestr:
    string = ""
    length = 0
    def __init__(self):
        self.length = 0

    def write(self, string):
        self.string = self.string + string
        length += len(string)

    def read(self):
        return self.string
#}}}

# {{{ writeAction
def writeAction(f, action):
    if action[4]:
        f.write('; ')
    f.write(action[0])
    f.write(': ')
    opcode = action[0]

    if   opcode == "DRIFT":
        if len(action[2]) == 5:
            f.write('{}mm length; {}mm Rx aperture; {}mm Ry aperture; {}mm hor. aper. shift; {}mm ver. aper. shift\n'.format(*(action[2])))
        else:
            print (*(action[2]), file=f, sep=';')

    elif opcode == "QUAD":
        if len(action[2]) == 9:
            f.write('{}mm length; {}T/m magnetic field gradient; {}mm aperture; {}° skew angle; {}T/m^2 6-pole grad.; {}T/m^3 8-pole grad.; {}T/m^4 10-pole grad.; {}T/m^5 12-pole grad.; {}mm Good field radius\n'.format(*(action[2])))
        else:
            print (*(action[2]), file=f, sep=';')

    elif opcode == "EDGE":
        if len(action[2]) == 7:
            act = action[2][0:-1]
            if  action[2][6] == '0':
                act.append("horizontal")
            elif action[2][6] == '1':
                act.append("vertical")
            else:
                act.append("unexpected value")
            f.write('{} deg Pole face rot. angle; {}mm Curvature radius of bend; {}mm Total gap of magnet; {} Fringe-field factor; {} Fringe-field factor; {}mm Aperture; {}\n'.format(*act))
        else:
            print (*(action[2]), file=f, sep=';')

    elif opcode == "APERTURE":
        if len(action[2]) == 3:
            act = []
            if action[2][2] == '2': # Pepperpot mode
                act.append('{}mm Radius hole'.format(action[2][0]))
                act.append('{}mm Distance b/w holes'.format(action[2][1]))
            else:
                act.append('{}mm X half width'.format(action[2][0]))
                act.append('{}mm Y half width'.format(action[2][1]))
            if   action[2][2] == '0':
                act.append("Rectangular aperture")
            elif action[2][2] == '1':
                act.append("Circular aperture")
            elif action[2][2] == '2':
                act.append("Pepperpot mode")
            elif action[2][2] == '3':
                act.append("Rectangular aperture")
            elif action[2][2] == '4':
                act.append("Horiz. finger")
            elif action[2][2] == '5':
                act.append("Vert. finger")
            elif action[2][2] == '6':
                act.append("Ring aperture")
            else:
                act.append("unexpected value")
            f.write('{}; {}; {}\n'.format(*act))
        else:
            print (*(action[2]), file=f, sep=';')

    elif opcode == "SPACE_CHARGE_COMP":
        if len(action[2]) == 1 and action[2][0] != '':
            f.write('Beam current is compensated by a factor {}\n'.format(action[2][0]))
        else:
            print (*(action[2]), file=f, sep=';')

    elif opcode == "RFQ_CELL":
        if len(action[2]) > 5:
            f.write('{}V: effective gap voltage; {}mm vane avg radius; {} acceleration parameter; {} vane modulation; {}mm cell length; {}deg: RF phase'.format(*(action[2][0:6])))
            if len(action[2]) > 6:
                f.write('; other params: {}'.format([float(f) for f in action[2][6:]]))
            f.write('\n')
        else:
            print (*(action[2]), file=f, sep=';')

    elif opcode == "DIAG_CURRENT":
        if len(action[2]) == 2:
            f.write('{}: diagnostic element id; {}mA: wanted beam current.\n'.format(*(action[2])))
        else:
            print (*(action[2]), file=f, sep=';')

    elif opcode == "DIAG_DCURRENT":
        if len(action[2]) == 3:
            f.write('{}: diagnostic element id; {}mA: wanted beam current; {}mA: accuracy.\n'.format(*(action[2])))
        else:
            print (*(action[2]), file=f, sep=';')

    elif opcode == "DIAG_POSITION":
        if len(action[2]) == 4:
            f.write('{}: diagnostic element id; {}mm: wanted X beam pos.; {}mm: wanted Y beam pos.; {}mm: diagnostic accuracy.\n'.format(*(action[2])))
        else:
            print (*(action[2]), file=f, sep=';')

    elif opcode == "DIAG_DIVERGENCE":
        if len(action[2]) == 4:
            f.write('{}: diagnostic element id; {}mm: wanted X rms size; {}mm: wanted Y rms size; {}mrad: diagnostic accuracy.\n'.format(*(action[2])))
        else:
            print (*(action[2]), file=f, sep=';')

    elif opcode == "DIAG_SIZE":
        if len(action[2]) == 7:
            f.write('{}: diagnostic element id; {}mm: wanted X rms size; {}mm: wanted Y rms size; {}°: wanted rms Phase spread; {}mm size accuracy; {}°: phase spread accuracy; {}MHz: ΔP Low-pass filter frequency.\n'.format(*(action[2])))
        else:
            print (*(action[2]), file=f, sep=';')

    elif opcode == "DIAG_SIZEP":
        if len(action[2]) == 6:
            f.write('{}: diagnostic element id; {}mm: wanted X rms size; {}mm: wanted Y rms size; {}MeV: wanted rms Energy spread; {}mrad: divergence accuracy; {}%: Energy spread accuracy.\n'.format(*(action[2])))
        else:
            print (*(action[2]), file=f, sep=';')

    elif opcode == "DIAG_DSIZE":
        if len(action[2]) == 3:
            f.write('{}: diagnostic element id; {}mm: wanted X-Y rms delta size; {}mm: dx-y size accuracy.\n'.format(*(action[2])))
        else:
            print (*(action[2]), file=f, sep=';')

    elif opcode == "DIAG_DSIZE2":
        if len(action[2]) == 4:
            f.write('{}: diagnostic element id; {}mm: wanted X rms delta size; {}mm: wanted Y rms delta size; {}mm: dx & dy size accuracy.\n'.format(*(action[2])))
        else:
            print (*(action[2]), file=f, sep=';')

    elif opcode == "DIAG_DSIZE3":
        if len(action[2]) == 4:
            f.write('{}: diagnostic element id; {}°: Wanted rms delta phase spread; {}°: Phase spread accuracy; {}MHz: dΔP Low-pass filter frequency.\n'.format(*(action[2])))
        elif len(action[2]) == 3:
            f.write('{}: diagnostic element id; {}°: Wanted rms delta phase spread; {}°: Phase spread accuracy.\n'.format(*(action[2])))
        else:
            print (*(action[2]), file=f, sep=';')

    elif opcode == "DIAG_ENERGY":
        if len(action[2]) == 3:
            f.write('{}: diagnostic element id; {}MeV: Wanted Energy; {}%: accuracy.\n'.format(*(action[2])))
        else:
            print (*(action[2]), file=f, sep=';')

    elif opcode == "DIAG_TWISS":
        if len(action[2]) == 7:
            f.write('{}: diagnostic element id; {}: Wanted αxx’; {}%: Wanted βxx’; {}: Wanted αyy’; {}%: Wanted βyy’; {} Wanted αzdp/p; {} Wanted βzdp/p’.\n'.format(*(action[2])))
        else:
            print (*(action[2]), file=f, sep=';')

    elif opcode == "FREQ":
        if len(action[2]) == 1:
            f.write('{}MHz: RF frequency of the following structure (beam frequency is not affected.\n'.format(action[2][0]))
        else:
            print (*(action[2]), file=f, sep=';')

    elif opcode == "LATTICE":
        if len(action[2]) == 2:
            f.write('{}:  number of elements per basic lattice; {}: number of lattices per macro-lattice.\n'.format(*(action[2])))
        else:
            print (*(action[2]), file=f, sep=';')

    elif opcode == "LATTICE_END":
        f.write('(ends the periodic focusing lattices).\n')

    elif opcode == "SET_ADV":
        act = [ action[2][0] ]
        if len(action[2]) == 2:
            act.append(action[2][1])
        else:
            act.append(action[2][0])
        if len(action[2]) == 2:
            f.write('{}: zero-current horizontal phase advance; {}: izero-current vertical phase advance.\n'.format(*act))
        else:
            print (*(action[2]), file=f, sep=';')

    elif opcode == "ADJUST":
        if len(action[2]) >= 2:
            f.write('{}-th variable of diagnostic element {} is to be adjusted'.format(action[2][1], action[2][0]))
            if len(action[2]) > 2:
                f.write(': {}'.format([float(f) for f in action[2][2:]]))
            f.write('.\n')
        else:
            print (*(action[2]), file=f, sep=';')






    else:
        print (*(action[2]), file=f, sep=';')
# }}} writeAction

# {{{ sequence_diagram
def sequence_diagram(f, modules):
    keys = list(modules.keys())
    nkeys = len(keys)

    f.write('@startuml\n')
    for key in keys:
        f.write(f'entity {key}\n')
    f.write('boundary exit\n')
    f.write('\n')
    for k in range(nkeys):
        if k < nkeys - 1:
            f.write("{} -> {}: {} to {}\n".format(keys[k], keys[k+1], keys[k], keys[k+1]))
        else:
            f.write("{} -> exit: from {} to the end\n".format(keys[k],keys[k]))
        f.write('rnote over {}\n'.format(keys[k]))
        for action in modules[keys[k]]:
            try:
                writeAction(f, action)
            except:
                print(colored('writeAction failed: action = {}'.format(action), 'red'), file=sys.stderr)
        f.write('endrnote\n')
        f.write('\n')
    f.write('@enduml\n')
# }}} sequence_diagram

# {{{ activity_diagram
def activity_diagram(f, modules):
    try:
        keys = list(modules.keys())
    except:
        return False

    nkeys = len(keys)
    lr = [ "left", "right" ]
    lri = 0
    f.write('@startuml\n')
    f.write("start\n")
    for k in range(nkeys):
        f.write(":{};\n".format(keys[k]))
        f.write('note {}\n'.format(lr[lri]))
        for action in modules[keys[k]]:
            try:
                writeAction(f, action)
            except:
                print(colored('writeAction failed: action = {}'.format(action), 'red'), file=sys.stderr)
        f.write('end note\n')
        lri = 1 - lri
        f.write('\n')
    f.write("stop\n")
    f.write('@enduml\n')
    return True
# }}} activity_diagram

def getActions(module, modules):
    try:
        keys = list(modules.keys())
    except:
        return None

    if not module in keys:
        return None

    fd, path = tempfile.mkstemp()
    try:
        with os.fdopen(fd, "w") as f:
            # f = filestr()
            for action in modules[module]:
                try:
                    writeAction(f, action)
                except:
                    print(colored('writeAction failed: action = {}'.format(action), 'red'), file=sys.stderr)
    except:
        print(colored('os.fdopen failed', 'red'), file=sys.stderr)

    with open(path, 'r') as f:
        s = f.readlines()

    os.remove(path)

    return s
    # return f.read()

def format():
    print ('flow.py -\x1B[1mh\x1B[0m(elp) -\x1B[1mS\x1B[0m(equence diagram on) -\x1B[1mA\x1B[0m(ctivity diagram on) -\x1B[1mx\x1B[0m(ecute plantuml) -\x1B[1ms\x1B[0m(vg output) -\x1B[1mj\x1B[0m(son output) -\x1B[1mg\x1B[0m(et) \x1B[3maction\x1B[23m', file=sys.stderr)
    print ('         \x1B[3mNote\x1B[23m: Get action returns the actions associated to a module.', file=sys.stderr)
    print ('         \x1B[3mNote\x1B[23m: You can only choose one type of diagram, not both.', file=sys.stderr)
    print ('         \x1B[3mNote\x1B[23m: Default: output = PNG.', file=sys.stderr)

# {{{ main
def main(argv):
    activity = False
    sequence = False
    execute  = False
    svg      = False
    json_f   = False

    module   = None

    try:
        opts, args = getopt.getopt(argv,"hAjg:Ssx",["get="])
    except getopt.GetoptError:
        format()
        sys.exit(2)
    for opt, arg in opts:
        if   opt == '-h':
            format()
            sys.exit(0)
        elif opt == '-A':
            activity = True
        elif opt in ('-g', '--get'):
            module = arg
        elif opt == '-j':
            json_f = True
        elif opt == '-S':
            sequence = True
        elif opt == '-s':
            svg = True
        elif opt == '-x':
            execute = True

    if not (activity or sequence or json_f or module):
        print(colored('\tNothing for me to do?', 'yellow'), file=sys.stderr)
        format()
        sys.exit(0)

    if activity and sequence:
        format();
        print(colored('Only one out of -s and -a must be specified --> Sequence diagram disabled.', 'yellow'), file=sys.stderr)
        sequence = False

    phase = 1

    if json_f:
        try:
            # m=list(modules)
            with open('flow.json', 'w') as f:
                s = json.dumps(modules)
                f.write(s)
            print(colored(f'{phase}: JSON creation:     completed.', 'green'), file=sys.stderr)
            phase += 1
        except:
            print(colored('JSON creation failed. Continuing despite the issue...', 'red'), file=sys.stderr)

    if execute and not (activity or sequence):
        print(colored('-x requires either -a or -s.', 'red'), file=sys.stderr)
        sys.exit(-1)

    if activity or sequence:
        try:
            with open("flow.plantuml", 'w') as f:
                if activity:
                    retval = activity_diagram(f, modules)
                else:
                    retval = sequence_diagram(f, modules)
        except:
            print(colored('Could not open output file. Bailing out...', 'red'), file=sys.stderr)
            sys.exit(-4)

        if not retval:
            print(colored('Diagram creation failed. Bailing out...', 'red'), file=sys.stderr)
            sys.exit(-5)

        print(colored(f'{phase}: Diagram creation:  completed.', 'green'), file=sys.stderr)
        phase += 1

        if execute:
            if svg:
                command = subprocess.call(['bash', 'plantuml-svg', 'flow.plantuml'])
                o = 'flow.svg'
            else:
                command = subprocess.call(['bash', 'plantuml', 'flow.plantuml'])
                o = 'flow.png'

            if command != 0:
                print(f"return code is {command}", file=sys.stderr)
            else:
                print(colored(f'{phase}: Diagram rendering: completed ({o}).', 'green'), file=sys.stderr)
                phase += 1

            command = subprocess.call(['scp', o, 'admin@10.5.3.18:public_html/vfolders/tracex'])
            if command != 0:
                print(f"return code is {command}", file=sys.stderr)
            else:
                print(colored(f'{phase}: Diagram transfer:  completed (http://tdr.myrrha.lan/vfolders/tracex/{o})', 'green'), file=sys.stderr)
                phase += 1

    if module:
        s = getActions(module, modules)
        print('<pre>')
        if s:
            for el in s:
                sys.stdout.write(el)
        else:
            print(colored(f'Error while querying actions of module {module}. Bailing out...', 'red'), file=sys.stderr)
            sys.exit(1)
        print('</pre>')
# }}} main

if __name__ == "__main__":
    main(sys.argv[1:])

# EoF (flow.py)
