/******************************************************************************/
/*                                                                            */
/*     assoc                                                                  */
/*                                                                            */
/*         a class of associative arrays and Web-oriented functions           */
/*                                                                            */
/*     conceived, designed, and implemented by                                */
/*     <a href=http://fourier.csata.it/enzo/enzo.html>Vincenzo De Florio</a>  */
/*     Copyright (c) 1995, Vincenzo De Florio. All rights reserved.           */
/*                                                                            */
/*     Modified in March 1996 by Cesare Antifora,                             */
/*     to fix a bug in the decoding procedure.                                */
/*                                                                            */
/*                                                                            */
/*      acgi.c                                                                */
/*      the source file for the CGI functions in the class                    */
/*                                                                            */
/*      Rel. date: Aug 1, 1995.   Version 0.1                                 */
/*                                                                            */
/******************************************************************************/

#include <stdio.h>
#include <string.h>
#include "assoc.h"

int  _brickalloc(brick**);
void _brickfree(brick*);



/******************************************************************************/
/*                                                                            */
/*              this function tokenize stdin according to the CGI standard    */
/*    acgi      and returns a pointer to an associative array or NULL in      */
/*              case of errors. aerror is set or burned accordingly.          */
/*                                                                            */
/******************************************************************************/

void url_decode(char *str) {
  int h1, h2;
  int i,j;
  /* i: index to encoded string; j: index to decoded string */
  for (i=0,j=0; str[i]; i++,j++)
      switch (str[i]) {
	case '+': str[j]=' ';
		  break;
	case '%': h1=str[i+1], h2=str[i+2]; 
		  if (isdigit(h1)) h1 -= '0'; else h1 = 10 + h1 - 'A';
		  if (isdigit(h2)) h2 -= '0'; else h2 = 10 + h2 - 'A';
		  str[j]=h1*16+h2;
		  i+=2;
		  break;
	default : str[j]=str[i];
		  break;
	/* are there any other cases that must be treated? */
      }
  str[j]='\0';
}



ASSOC *acgi() {
  int cl;
  int i;
  char *name, *value, *q, strinput[MAX_CGI_INPUT];
  ASSOC *a;

  cl = atoi(getenv("CONTENT_LENGTH"));

  if (cl>MAX_CGI_INPUT) {
    strcpy(aerror, "MAX_CGI_INPUT reached");
    return NULL;
  }
	
  if (cl <= 0)
  {
    printf("CONTENT_LENGTH is undefined -- This is not a CGI script.\nAborting...\n");
    exit(1);
  }

  for (i=0; i<cl; i++)
      strinput[i]=getchar();
      
  
  a=aopen(acmp);

#ifdef DEBUG

  if(*aerror) { printf("%s<p>\n", aerror); exit(1); }

#endif

  for ( i=0, name=strtok(strinput, "&");
	name !=NULL;
	i++ , name=strtok(NULL, "&")
      ) {
    q=strchr(name, '=');
    *q='\0';
    value=q+1;
    url_decode(name);
    url_decode(value);
    awrite(a, name, value);

#ifdef DEBUG

    printf("wrote (\"%s\", \"%s\")\n<p>\n", name, value);
    if(*aerror) { printf("%s<p>\n", aerror); exit(1); }

#endif
  }


  return a;
}
/******************************************************************************/
/*                              end of file acgi.c                            */
/******************************************************************************/
