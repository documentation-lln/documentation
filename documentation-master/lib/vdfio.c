/******************************************************************************************************
 * vdfio.c
 *
 * "Standard", commmon functions for knowledge extraction modules and services
 *
 * v. 0.1 (20201102T0859) (Preliminary version, yet to be thoroughly tested)
 *
 * by vdflorio (vincenzo.deflorio@sckcen.be)
 *
 * In order to compile this code, do as follows:
 *  gcc -c vdfio.c
 *
 ******************************************************************************************************/

static const char version[] = "v. 0.2 (20210226T1240)";
int errcode;

//typedef unsigned long size_t ;
//#include <stddef.h>

//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>

#include "vdfio.h"

int scat(FILE *g, char cmd[]) {
	int c;
	FILE *f = popen(cmd, "r");
	if (f == NULL) return ERR_CANT_ACCESS;
	while ( (c=fgetc(f)) != EOF ) fputc(c, g);
	//fclose(f);
	return WEXITSTATUS(pclose(f));
	//return ERR_NONE;
}

int fcat(FILE *g, char fname[]) {
	int c;
	FILE *f = fopen(fname, "r");
	if (f == NULL) return ERR_CANT_ACCESS;
	while ( (c=fgetc(f)) != EOF ) fputc(c, g);
	fclose(f);
	return ERR_NONE;
}

void abnormal(int code) {
	fprintf(stderr, "%sSomething went wrong and I cannot proceed, sorry (code = %d).%s\n", KRED, code, KNRM);
	fprintf(stderr, "%sDescription: %s.%s\n", KYEL, ErrMsgs[-code], KNRM);
}

/* {{{ stringstack class */
//typedef struct stringstack_node_t { char *string; struct stringstack_node_t *next; } stringstack_node_t;
//typedef struct { int n; stringstack_node_t *last; stringstack_node_t *first; } stringstack_t;
stringstack_t *sstack_open() {
	stringstack_t *sst;
	sst = malloc(sizeof(stringstack_t));
	if (sst == NULL) {
		errcode = ERRCODE_NULL_PTR;
		return NULL;
	}
	sst->n = 0;
	sst->last = sst->first = NULL;
	return sst;
}
int sstack_add(stringstack_t* sst, char *s) {
	stringstack_node_t *node;

	if (sst == NULL || s == NULL) return errcode = ERRCODE_NULL_PTR;

	node = malloc(sizeof(stringstack_node_t));
	if (node == NULL) return errcode = ERRCODE_MALLOC;

	node->string = strdup(s);
	node->next   = NULL;

	sst->n++;

	if (sst->first == NULL) {
		// sst was empty, thus `first` points to the item we just added, `last`
		sst->first = node;
	}
	else {
		// previous last points to `last`
		sst->last->next = node;
	}
	// `last` now points to `node`
	sst->last  = node;
	return 0;
}
char **sstack_pack(stringstack_t* sst, int *n) {
	int i;
	char **pack, **ppack;
	stringstack_node_t *node;

	if (sst == NULL || n == NULL) {
		errcode = ERRCODE_NULL_PTR;
		return NULL;
	}

	ppack = pack = malloc(sst->n * sizeof(char *));
	if (pack == NULL) {
		errcode = ERRCODE_MALLOC;
		return NULL;
	}

	for (ppack = pack, node = sst->first;
	     node != NULL;
	     node = node->next, ppack++)
		*ppack = strdup( node->string ); // yet another copy is created
	*n = sst->n;
	return pack;
}
bool sstack_is_present(stringstack_t* sst, char *string) {
	stringstack_node_t *node;
	if (sst == NULL || string == NULL) {
		errcode = ERRCODE_NULL_PTR;
		return false;
	}
	for (node = sst->first; node != NULL; node = node->next)
		if (strcmp(node->string, string) == 0) return true;
	return false;
}
int sstack_close(stringstack_t *sst) {
	stringstack_node_t *node;
	if (sst == NULL)   return errcode = ERRCODE_NULL_PTR;
	for (node = sst->first; node != NULL; node = node->next)
		free(node->string);
	free(sst);
	return 0;
}
/* }}} stringstack class */

char *sysprint(char *cmd, char *buffer, int n) {
	int i;
	FILE *f;
	char *p = buffer;

	f = popen(cmd, "r");
	if (f == NULL) return NULL;
	for (i=0; i < n; i++, p++) {
		*p = fgetc(f);
		if (*p == EOF)  break;
		if (*p == '\n') break;
	}
	*p = '\0';
	return buffer;
}

/* {{{ mem class */
mem_t * memopen(int size) {
	mem_t *mem;

	if (size < 1) return NULL;

	mem = malloc(sizeof(mem_t));
	if (mem == NULL) return NULL;

	mem->vptr = malloc(size);
	if (mem->vptr == NULL) {
		free(mem);
		return NULL;
	}
	mem->total  = size;
	mem->used = 0;
	return mem;
}
memtell_t *memtell(mem_t * mem) {
	memtell_t *mv;

	if (mem == NULL) return NULL;

	mv = malloc(sizeof(memtell_t));
	if (mv == NULL) return NULL;

	mv->used = mem->used;
	mv->total = mem->total;

	return mv;
}
int memclose(mem_t * mem) {
	if (mem == NULL)	return ERRCODE_NULL_PTR;
	if (mem->vptr == NULL)	return ERRCODE_NULL_PTR;
	free(mem->vptr);
	mem->vptr = NULL;
	free(mem);
	return 0;
}
void *memalloc(mem_t *mem, int sz) {
	if (sz < 1) return NULL;

	if ((sz % 8) != 0)
		sz = ((sz / 8) + 1) * 8;

	if (mem == NULL)		return NULL;
	if (mem->vptr == NULL)		return NULL;
	if (mem->used + sz > mem->total)return NULL;

	mem->used += sz;
	return mem->vptr + mem->used - sz;
}
/* }}} mem class */

/* EOF vdfio.c */
