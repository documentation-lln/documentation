
I asked our ICT Department whether it could be possible to map a local with Alexandria.
To my surprise, they let me
know that a hidden folder actually exists on our laptops that is
used to store temporary data *from and to Alexandria*:

```
c:\Users\YOUR_SCK_ID\AppData\Roaming\OpenText\OTEditC_sckcen\
```

(of course YOUR\_SCK\_ID should be... your SCK-CEN id.)

Now, [every time I compile a new version of the TDR](https://git.myrrha.lan/vdflorio/documentation/blob/master/TDR/p),
I execute the following command that copies the output PDF in a sub-directory of the hidden folder:

    # The following command instructs the upload of the TDR (without frontpage material) onto Alexandria
    cp MYRRHA-ACCELERATOR-TDR.pdf ~/c/Users/vdflorio/AppData/Roaming/OpenText/OTEditC_sckcen/c36807301/

This instructs the upload of the TDR onto Alexandria. Folder c36807301 corresponds to Alexandria folder
[MYRRHA ACCELERATOR TDR](https://ecm.sckcen.be/OTCS/llisapi.dll?func=ll&objId=19670138&objAction=browse&viewType=1)
in the MYRRHA ACCELERATOR Workspace.

*Regrettably, that upload is not instantaneous*. Several hours can elapse before the Alexandria local daemon,
OpenText Office Editor, notices a conflict between local and remote contents. When this finally happens,
you are asked to choose whether you want to accept the change. In case of a positive answer, the output PDF
finally gets onto Alexandria. Far from perfect, though better than nothing...

![](TDR/images/OpenText-Office-Editor.png)

![Synchronization with Alexandria requires a PulseSecure connection to have been established](ancillary/images/OpenText-Office-Editor-2.png)
[MYRRHA ACCELERATOR TDR](https://ecm.sckcen.be/OTCS/llisapi.dll?func=ll&objId=19670138&objAction=browse&viewType=1)
