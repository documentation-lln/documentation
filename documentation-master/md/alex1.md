I have recently submitted a ticket to verify the availability of such a functionality and, in case it would be missing,
to suggest its implementation.

<!--
[comment]: # "Comment" ![Ticket 42242: command line tool to create new version of a document on Alexandria](ancillary/Ticket42242.png)
-->

Apparently no such tool exist.


<!--
[comment]: # "Comment" ![Ticket 42242: Response](ancillary/Ticket42242followup.png)

