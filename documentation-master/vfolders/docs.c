record_t *init_docs(record_nr, int field_nr) {
	record_t *docs;
	int r, c;

	docs = malloc(record_nr * sizeof(record_t));
	if (docs == NULL) return NULL;

	for (r=0; r < record_nr; r++) {
		docs[r] = malloc(sizeof(char**));
		if (docs[r] == NULL) return NULL; // add free's later on
	}

	r=0;
		c=0;
		docs[r][c++] = "Name";
		docs[r][c++] = "Location";
		...
		docs[r][c++] = "Manufacturer";
	r=1;
		c=0;
	etc
}
record_t docs[] = {
{
"Name", "Location", "Owner", "Topic", "Last modified", 
	"Set of documents", "More info", "Discipline", "Naming", "Crosscut", 
	"None", "Project", "Manufacturer", },
{
"PTF_Shielding_Wall_Proton_Beam_20201118.pptx", "41065818", "Ryan Bodenstein", "Beam optics", "2020-11-19 00:00:00", 
	"None", "None", "A", "I1-RFQ", "Reliability", 
	"None", "MYRRHA", "BEVATECH", },
{
"2020_09_29_BeamOptics_Bodenstein_17MeVDump_Line.pptx", "40248200", "Ryan Bodenstein", "Beam optics", "2020-11-19 00:00:00", 
	"None", "None", "A", "I1-RFQ", "MachineProtection", 
	"None", "MINERVA", "COSYLAB", },
{
"2020-05-26 - UDO", "38673195", "UDO", "Beam optics", "2020-05-25 00:00:00", 
	"None", "None", "A", "I1-RFQ", "WasteManagement", 
	"None", "MYRTE", "COSYLAB", },
{
"2020-06-16 - OpticsMeeting.pptx", "38941856", "Ryan Bodenstein", "Beam optics", "2020-07-01 00:00:00", 
	"None", "None", "A", "I1-RFQ", "MachineProtection", 
	"None", "MINERVA", "IPNO", },
{
"2020-07-01-OpticsMeeting_Bodenstein.pptx", "39144935", "Ryan Bodenstein", "Beam optics", "2020-07-01 00:00:00", 
	"None", "None", "A", "I1-LE", "Commissioning", 
	"None", "MINERVA", "BEVATECH", },
{
"2020-09-09 - Aurelien - MEBT3_176MHz_option.pdf", "39666750", "UDO", "Beam optics", "2020-09-09 00:00:00", 
	"None", "None", "A", "I1-LE", "Reliability", 
	"None", "MYRTE", "COSYLAB", },
{
"MINERVA - Optics open questions", "38745629", "UDO", "Beam optics", "2020-08-18 00:00:00", 
	"None", "None", "A", "I1-LE", "WasteManagement", 
	"None", "MYRTE", "IPNO", },
{
"MINERVA 100 MeV Accelerator TDR", "37035102", "vdflorio", "Documentation", "2020-12-16 00:00:00", 
	"Yes", "None", "A", "I1-LE", "Reliability", 
	"None", "MYRRHA", "BEVATECH", },
{
"01 Meeting Planner Accelerator.xlsx", "39651369", "Toine Van Gils", "Management", "2021-01-19 00:00:00", 
	"None", "None", "A", "I1-LE", "Reliability", 
	"None", "MYRRHA", "BEVATECH", },
{
"02 Folderstructure Alexandria for ADB.xlsx", "40707579", "Toine Van Gils", "Management", "2020-10-22 00:00:00", 
	"None", "None", "A", "I1-LE", "None", 
	"None", "MAX", "COSYLAB", },
{
"ADB portal.xlsx", "39418700", "UDO", "Management", "2020-10-14 00:00:00", 
	"None", "None", "A", "I1-IS", "MachineProtection", 
	"None", "MYRRHA", "IBA", },
{
"0 - ADT - Essential Documents.pptx", "37414164", "fdoucet", "Management", "2020-07-14 00:00:00", 
	"None", "None", "A", "I1-LE:DF-CHOP-01", "Commissioning", 
	"None", "MINERVA", "Linac Dynamics", },
{
"ADT - MINERVA Status Meeting Feb 2020 - ADT Contribution (2 slides only).pptx", "37226988", "fdoucet", "Management", "2020-02-14 00:00:00", 
	"None", "None", "A", "I1-LE:DF-CHOP-01", "Reliability", 
	"None", "MINERVA", "BEVATECH", },
{
"ANS-21 ADT Contribution.pptx", "36219155", "fdoucet", "Management", "2019-11-29 00:00:00", 
	"None", "None", "A", "FT-S01", "None", 
	"None", "MINERVA", "BEVATECH", },
{
"ANS-23 ADT Contribution.pptx", "36220069", "fdoucet", "Management", "2019-11-29 00:00:00", 
	"None", "None", "B", "FT-S01", "WasteManagement", 
	"None", "MYRRHA", "IBA", },
{
"ANS-23 ADT Contribution (light).pptx", "38198911", "fdoucet", "Management", "2020-04-22 00:00:00", 
	"None", "None", "B", "FT-S02", "None", 
	"None", "MYRTE", "IPNO", },
{
"Governor board April 2020 - ADT Contribution.pptx", "38198915", "fdoucet", "Management", "2020-04-22 00:00:00", 
	"None", "None", "B", "FT-S02", "MachineProtection", 
	"None", "MYRRHA", "Linac Dynamics", },
{
"MoM ADT Team CRC Coordination Meeting - 2018 04.docx", "27885205", "fdoucet", "Management", "2018-02-02 00:00:00", 
	"Yes", "None", "B", "TS-DL", "Commissioning", 
	"None", "MYRRHA", "None", },
{
"MoM ADT Team CRC Coordination Meeting - CW 2020 28.docx", "39578420", "fdoucet", "Management", "2020-09-01 00:00:00", 
	"Yes", "None", "B", "I1:LLRF", "None", 
	"None", "MYRTE", "BEVATECH", },
{
"Planning2018_February_July_HIF_LIF.pdf", "29902106", "fdoucet", "Management", "2018-07-12 00:00:00", 
	"None", "None", "B", "I1:LLRF", "WasteManagement", 
	"None", "None", "BEVATECH", },
{
"2018 02 26.pdf", "28370458", "fdoucet", "Management", "2018-03-13 00:00:00", 
	"Yes", "MoM - ADT Team Internal Coord. Meetings", "B", "I1:LLRF", "Reliability", 
	"None", "MYRTE", "IBA", },
{
"2018 07 09.pdf", "29903139", "fdoucet", "Management", "2018-07-12 00:00:00", 
	"Yes", "MoM - ADT Team Internal Coord. Meetings", "B", "I1:LLRF", "None", 
	"None", "MYRRHA", "COSYLAB", },
{
"CW 12 - Weekly Dashboard.pptx", "28427025", "jbelmans", "Management", "2018-03-21 00:00:00", 
	"None", "None", "B", "I1-ME3:ICD-COLH-ME3", "None", 
	"None", "None", "BEVATECH", },
{
"Document1.docx", "29897689", "fdoucet", "Management", "2018-07-12 00:00:00", 
	"None", "None", "B", "I1-ME3:ICD-COLV-ME3", "Reliability", 
	"None", "MYRRHA", "BEVATECH", },
{
"2018_10_22_MYRTE_WP2_7th_F.Doucet.pptx", "31245351", "fdoucet", "Management", "2018-11-28 00:00:00", 
	"None", "None", "B", "I1-ME1:RF-CAV-01", "WasteManagement", 
	"None", "MINERVA", "COSYLAB", },
{
"7th_WP2_MYRTE_-_RFQ_SSA_commissioning Full (Franck P.).pptx", "31245003", "fdoucet", "Management", "2018-11-28 00:00:00", 
	"None", "None", "C", "I1-ME1:RF-CAV-02", "MachineProtection", 
	"None", "MYRRHA", "IPNO", },
{
"Status Full Task 2.2 (Franck P.).pptx", "31247371", "fdoucet", "Management", "2018-11-28 00:00:00", 
	"Yes", "None", "C", "I1-ME1", "Commissioning", 
	"None", "MINERVA", "Linac Dynamics", },
{
"Status Task 2.12 Costing (Dirk V.).pptx", "31244925", "fdoucet", "Management", "2018-11-28 00:00:00", 
	"None", "None", "C", "I1-ME2", "WasteManagement", 
	"None", "MINERVA", "None", },
{
"Status Task 2.6 Linac_design (Angelique G.).pptx", "31243974", "fdoucet", "Management", "2018-11-28 00:00:00", 
	"None", "None", "C", "I1-ME3", "MachineProtection", 
	"None", "MYRRHA", "COSYLAB", },
{
"Status Task 2.7 (Jorik B.).pptx", "31246670", "fdoucet", "Management", "2018-11-28 00:00:00", 
	"None", "None", "C", "I1-ME3", "None", 
	"None", "MINERVA", "COSYLAB", },
{
"Waste from Accelerator.pptx", "33077514", "fdoucet", "Management", "2019-03-05 00:00:00", 
	"None", "None", "C", "I1-ME3", "Commissioning", 
	"None", "MAX", "None", },
{
"MYRRHA MINERVA General Presentation + LLN Status + Cryomodule.pptx", "36396597", "fdoucet", "Management", "2020-10-15 00:00:00", 
	"None", "None", "C", "LB-ME3", "WasteManagement", 
	"None", "None", "IBA", },
{
"TCO - ADB Weekly Meeting", "s k I p p e d", "None", "None", "None", 
	"None", "None", "C", "LB-ME3", "Reliability", 
	"None", "MAX", "IPNO", },
{
"0 - Milestones - Tender Overview - Investments.xlsx", "29719229", "fdoucet", "Management", "2020-12-22 00:00:00", 
	"None", "None", "D", "I1-CH01:RF-CAV-01", "None", 
	"None", "MYRRHA", "COSYLAB", },
{
"0 - Planning 100 MeV.mpp", "28216375", "fdoucet", "Management", "2020-11-19 00:00:00", 
	"None", "None", "D", "I1-CH02:RF-CAV-01", "None", 
	"None", "MYRRHA", "COSYLAB", },
{
"0 - Planning ACC (w baseline) - ADT only.mpp", "41004802", "fdoucet", "Management", "2021-01-28 00:00:00", 
	"None", "None", "D", "I1-CH03:RF-CAV-01", "WasteManagement", 
	"None", "MINERVA", "BEVATECH", },
{
"2 - Cost book (Pierre)", "25219567", "jverpoor", "Management", "2019-03-15 00:00:00", 
	"None", "None", "E", "I1-CH07:RF-CAV-01", "Reliability", 
	"None", "MINERVA", "BEVATECH", },
{
"Budget files for MMT", "s k I p p e d", "None", "None", "None", 
	"None", "None", "E", "I1-CH09:RF-CAV-01", "MachineProtection", 
	"None", "MINERVA", "None", },
{
"Injector - CH Cavities - Facts & Figures if new call.pptx", "36461625", "fdoucet", "Management", "2019-12-18 00:00:00", 
	"None", "None", "E", "I1-CH11:RF-CAV-01", "None", 
	"None", "None", "Linac Dynamics", },
{
"Injector - CH Cavities Planning and Cost impacts.pptx", "36463135", "fdoucet", "Management", "2019-12-18 00:00:00", 
	"Yes", "None", "E", "I1-CH12:RF-CAV-01", "Commissioning", 
	"None", "MINERVA", "COSYLAB", },
{
"Control System - Cost Breakdown - Task - Resources - Budget - Table.xlsx", "41866117", "fdoucet", "Management", "2021-01-22 00:00:00", 
	"None", "None", "E", "I1-CH14:RF-CAV-01", "Reliability", 
	"None", "MINERVA", "COSYLAB", },
{
"Control System - Cost Breakdown.pptx", "41471102", "fdoucet", "Management", "2021-01-21 00:00:00", 
	"None", "None", "E", "I1-ME2:RF-CAV-01", "None", 
	"None", "MINERVA", "IBA", },
{
"Planning injector @ UCL.mpp", "26368749", "fpompon", "Management", "2020-06-12 00:00:00", 
	"None", "None", "E", "I1-ME3:RF-CAV-01", "Reliability", 
	"None", "None", "IPNO", },
{
"Planning MYRRHA_IPNO (v3.3) (003).xlsx", "26372801", "wdecock", "Management", "2020-06-12 00:00:00", 
	"None", "None", "F", "LB-ME3:RF-CAV-01", "None", 
	"None", "MAX", "COSYLAB", },
{
"RFQ commissioning schedule_171117.docx", "27792166", "fpompon", "Management", "2020-06-12 00:00:00", 
	"None", "None", "F", "LB-ME3:RF-CAV-01", "None", 
	"None", "None", "BEVATECH", },
{
"RFQ commissioning schedule_2018_180201.docx", "27898956", "fpompon", "Management", "2020-06-12 00:00:00", 
	"None", "None", "F", "LB-S01:RF-CAV-01", "MachineProtection", 
	"None", "MINERVA", "BEVATECH", },
{
"Test Bench Instrumentation - When needed and from who.xlsx", "35606841", "fdoucet", "Management", "2021-01-26 00:00:00", 
	"None", "None", "F", "LB-S11:RF-CAV-01", "Reliability", 
	"None", "MAX", "COSYLAB", },
{
"ADT Job Description - Accelerator Technical Writer & Technical Document Officer.docx", "29208299", "fdoucet", "Management", "2018-05-23 00:00:00", 
	"None", "None", "F", "LB-S21:RF-CAV-02", "None", 
	"None", "MYRRHA", "IPNO", },
};

