#!/usr/bin/env python3

from bottle import route, run, static_file

@route('/<filepath:path>')
def server_static(filepath):
    return static_file(filepath, root='./vfolder/')

run(host='localhost', port=8081, debug=True)
