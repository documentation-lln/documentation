#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from sys import argv
import collections
import openpyxl
from pathlib import Path

from unidecode import unidecode

# Centos:
# sudo yum install python-pip

verbose=False
if len(argv) != 2:
    xlsx_file = Path('.', 'MYRACC100_.xlsx')
else:
    xlsx_file = Path('.', argv[1])
    new_xlsx_file = Path('new', argv[1])

print('Processing sheet {}\n'.format(xlsx_file))

wb_obj = openpyxl.load_workbook(xlsx_file) 

# Read the active sheet:
sheet = wb_obj.active

rows=sheet.max_row
cols=sheet.max_column

for i, row in enumerate(sheet.iter_rows()):
    # print('row ', i, ' is ', row)
    row[1].hyperlink = 'https://ecm.sckcen.be/OTCS/llisapi.dll/link/{}'.format(row[1].value)
    if row[3].value == 'MYR100ACC':
        row[3].value = 'new'
    else:
        row[3].value = 'old'

wb_obj.save(new_xlsx_file) # ("new.xlsx")

