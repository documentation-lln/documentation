#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from sys import argv
import collections
import openpyxl
from pathlib import Path

from unidecode import unidecode

# Centos:
# sudo yum install python-pip

verbose=False
if len(argv) != 2:
    xlsx_file = Path('.', 'MYRACC100_.xlsx')
else:
    xlsx_file = Path('.', argv[1])

print('Processing sheet {}\n'.format(xlsx_file))

wb_obj = openpyxl.load_workbook(xlsx_file) 

# Read the active sheet:
sheet = wb_obj.active

rows=sheet.max_row
cols=sheet.max_column

for i, row in enumerate(sheet.iter_rows()):
    print('row ', i, ' is ', row)
    for j, col in enumerate(row):
        if isinstance(col.value, str):
            print('elem[', i, ',', j, '] = ', col, ', namely ', unidecode(col.value), '\n')
        else:
            print('elem[', i, ',', j, '] = ', col, ', namely ', col.value, '\n')

wb_obj.save("new.xlsx")

