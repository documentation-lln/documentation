# Vfolders

Vfolders is a prototypic implementation of a [virtual folder](https://ecm.sckcen.be/OTCS/llisapi.dll?func=ll&objaction=overview&objid=41214683).
- An excel file represents a list of documents on Alexandria. Columns represent filename, objid, and metadata.
- Vfolders allow to construct a virtual folder based on up to three metadata domain names.

Version is 0.4.4 (2021-03-05). Author is vdflorio.

A demo is now available [here](http://tdr.myrrha.lan/vfolders/vfolders.html).

Let me explain vfolders through an example. 

## Example
We shall use the excel file `MYRACC100_.xlsx` (see picture below).

Let me call Categories the entries in the first row of the excel sheet -- for instance Name, ObjId, Topic, Project, Owner...

The following commands partition the excel sheet into blocks according to the specified category -- for instance by Topic, and then by Owner:

    $ ./genclassheader.py            # executed only once, to translate the excel sheet into data structures
    $ make                           # to compile the data structures and other code
    $ ./vfolders -1 Topic -2 Owner   # to partitions `MYRACC100_.xlsx` by Topic and then by Owner

The result is visible as a file system: the root shows the Topic partitions; within each partition, documents are partitioned again by their Owners.

Let me explain this in more detail:

### Preprocessing: from excel sheet to internal data structures
 
    $ ./genclassheader.py $EXCELFILE ; make
 
Note: if `$EXCELFILE` is not mentioned, the module processes file `MYRACC100_.xlsx`

### Processing: from criteria to virtual folder

    $ ./vfolders -1 $FIRST -2 $SECOND -3 $THIRD -h

Note: `$FIRST`, `$SECOND`, and `$THIRD` refer to the strings in the first row of the input sheet

As an example:

    $ ./vfolders -1 Topic -2 Project -3 Owner -h

## Browsing: inspecting the output as a series of html pages

    $ $BROWSER http://localhost:8081/0.html

where $BROWSER could be e.g. firefox


# Output options

## Output as a set of html files
Option `-h` creates an html file system that offers the requested classification. Files are stored in local folder `vfolder`. The root of the file system can be accessed via URL `http://localhost:8081/0.html` after launching the local server via

    $ ./python server.py

Below, you may see pictures of the resulting web pages.

## Output as an `anytree` data structure
Option `-p` creates a python `anytree` initialized with the file system corresponding to the requested classification:

    $ ./vfolders -1 Topic -2 Owner -p # creates python_data.py and python_instructions.py

To visualize the structure of the obtained file system, execute

    $ ./python_instructions.py

The following output appears on the screen:

```
/
├── Topic = Beam optics
│   ├── Owner = Ryan Bodenstein (Beam optics)
│   └── Owner = UDO (Beam optics)
├── Topic = Documentation
│   └── Owner = vdflorio (Documentation)
├── Topic = Management
│   ├── Owner = Toine Van Gils (Management)
│   ├── Owner = UDO (Management)
│   ├── Owner = fdoucet (Management)
│   ├── Owner = jbelmans (Management)
│   ├── Owner = jverpoor (Management)
│   ├── Owner = fpompon (Management)
│   └── Owner = wdecock (Management)
└── Topic = None
    └── Owner = None (None)
```

and in file `folders.png`:

![](folders-ex.png)

Each of the nodes in the above `anytree` includes a data structures called `files`, listing the lines in the excel sheet that match the corresponding block of the partition.

# Full example

Let me summarize everything by means of a more complete example.

We shall consider the following excel sheet:

![](01.png)
*Excel sheet `MYRACC100_.xlsx`.*

As you can see, the first row classifies the entries according to, e.g., Filename, Location (Alexandria ObjId). Other fields here represent metadata: Topic, Discipline, Project, Owner, etc.

Please note that although Filenames, objids, and owners are correct, some of the other entries in this excel sheet are ficticious.

 

First, I need to "hand over" the excel sheet. This is done via

    $ ./genclassheader.py ; make

Secondly, I have to choose at least one and up to three keys. If I want to classify the documents by Owner; then by Topic; and then by Project, I will have to execute:

    $ ./vfolders -1 Owner -2 Topic -3 Project -h

Now, in a separate terminal, I will launch the Python Bottle server:

    $ python server.py

Finally, I can inspect the output by browsing `http://localhost:8081/0.html`:

![](02.png)
*Top level view: the documents have been partitioned into blocks defined by their Owner.*

![](03.png)
*When I select, e.g., `fdoucet`, I can see Frédéric's document, arranged by topic. The only topic in this case is 'Management'.*

![](04.png)
*I clicked on the only entry in previous menu. As a result, I see Frédéric's document related to Management classified according to the Project they were originated from.*

![](05.png)
*Here I selected "MYRTE". I could see that only three documents are present.*

![](06.png)
*I clicked on the second entry, which moved me to the corresponding document on Alexandria.*

# Installation
You need python3 and a number of ancillary python modules.

- python -m pip install bottle
- python -m pip install anytree
- pithon -m pip install anytree
- pithon -m pip install graphviz
- sudo apt install graphviz # or equivalent command to install packages

You also will need the standard Linux tools make and gcc

