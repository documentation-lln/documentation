#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from sys import argv
import collections
import openpyxl
from pathlib import Path

# Centos:
# sudo yum install python-pip

verbose=False
if len(argv) != 2:
    xlsx_file = Path('.', 'MYRACC100_.xlsx')
else:
    xlsx_file = Path('.', argv[1])

print('Processing sheet {}\n'.format(xlsx_file))

wb_obj = openpyxl.load_workbook(xlsx_file) 

# Read the active sheet:
sheet = wb_obj.active

rows=sheet.max_row
cols=sheet.max_column

for i, row in enumerate(sheet.iter_rows()):
    print('row ', i, ' is ', row)
    for j, col in enumerate(row):
        if isinstance(col.value, str):
            print('elem[', i, ',', j, '] = ', col, ', namely ', col.value.encode('utf-8'), '\n')
        else:
            print('elem[', i, ',', j, '] = ', col, ', namely ', col.value, '\n')

with open("tblshape.h","w") as f:
    f.write('\n')
    f.write("#define RECORD_NR {}\n".format( i+1 ))
    f.write("#define FIELD_NR  {}\n".format( j+1 ))
    f.write('\n')

#  with open("tbltypes.h","w") as f:
#      f.write('/* tbltypes.h\n')
#      f.write(' * defines the basic types of class table_t\n')
#      f.write(' */\n\n')
#      f.write('#ifndef __Tbl__Types__\n')
#      f.write('#define __Tbl__Types__\n')
#      f.write("typedef char **record_t;\n")
#      # f.write("typedef struct { record_t **table; int n; int ptr; } table_t;\n");
#      f.write('#endif\n')
#      f.write('\n')

#  with open("docs.h","w") as f:
#      f.write("record_t docs[] = {\n")
#      for i, row in enumerate(sheet.iter_rows()):
#          f.write('{\n')
#          for j, col in enumerate(row):
#              if isinstance(col.value, str):
#                  #f.write('"{}", '.format(col.value.encode('utf-8')))
#                  ##f.write('"{}", '.format(str(col.value.encode('utf-8')).decode('ascii')))
#                  f.write('"{}", '.format(str(col.value)))
#              else:
#                  f.write('"{}", '.format(col.value))
#              if (j+1) % 5 == 0:
#                  f.write("\n\t")
#          f.write('},\n')
#      f.write('};\n\n')
#  
#  with open("initdocs.h", "w") as f:
#      f.write('#include <stdio.h>\n')
#      f.write('#include <stdlib.h>\n\n')
#      f.write('#include "tblshape.h"\n')
#      f.write('#include "tbltypes.h"\n\n')
#      f.write('record_t *init_docs() {\n')
#      f.write('\tchar ***docs;\n')
#      f.write('\tint r, c;\n\n')
#      f.write('\tdocs = malloc(RECORD_NR * sizeof(record_t));\n')
#      f.write('\tif (docs == NULL) return NULL;\n\n')
#      f.write('\tfor (r=0; r < RECORD_NR; r++) {\n')
#      f.write('\t\tdocs[r] = malloc(FIELD_NR * sizeof(char*));\n')
#      f.write('\t\tif (docs[r] == NULL) return NULL; // add free()\'s later on\n')
#      f.write('\t}\n\n')
#  
#      for r, row in enumerate(sheet.iter_rows()):
#          f.write('\tr={};\n'.format(r))
#          for c, col in enumerate(row):
#              if isinstance(col.value, str):
#                  # f.write('\t\tc={}; docs[r][c] = "{}";\n'.format(c, col.value.encode('utf-8')))
#                  #f.write('\t\tc={}; docs[r][c] = "{}";\n'.format(c, col.value.encode('ascii', 'ignore').decode('ascii')))
#                  #f.write('\t\tc={}; docs[r][c] = "{}";\n'.format(c, col.value.encode('ascii', 'ignore')))
#                  #f.write('\t\tc={}; docs[r][c] = "{}";\n'.format(c, str(col.value.encode('ascii', 'ignore'))))
#                  f.write('\t\tc={}; docs[r][c] = strdup("{}");\n'.format(c, str(col.value).replace('"', '\'')))
#              else:
#                  f.write('\t\tc={}; docs[r][c] = strdup("{}");\n'.format(c, col.value))
#          f.write("\n")
#      f.write('\treturn docs;\n')
#      f.write('}\n\n')

with open("initdocs2.h", "w") as f:
    f.write('#include <stdio.h>\n')
    f.write('#include <stdlib.h>\n\n')
    f.write('#include "tblshape.h"\n')
    f.write('#include "tbltypes.h"\n\n')
    f.write('char *docs[RECORD_NR][FIELD_NR] = {\n')
    for r, row in enumerate(sheet.iter_rows()):
        f.write('\t//r={};\n'.format(r))
        f.write('\t{\n')
        for c, col in enumerate(row):
            if isinstance(col.value, str):
                f.write('\t\t"{}",\n'.format(str(col.value).replace('"', '\'')))
            else:
                f.write('\t\t"{}",\n'.format(col.value))
        f.write('\t},\n')
    f.write("};\n")

exit(0)

#     f.write('int block_nrs[] = {')
#     for name in name2index:
#         f.write('{}, '.format( len(name2set[name]) ))
#     f.write('};\n\n')
# 
#     f.write('char *blocks[RECORD_NR][FIELD_NR] = {\n')
#     for nameset in name2set:
#         f.write('\t{ ')
#         for i, name in enumerate(name2set[nameset]):
#             f.write('"{}",'.format(name))
#             if (i + 1) % 6 == 0:
#                 f.write("\n\t")
#         f.write('\t},\n')
#     f.write('};\n\n')
# 
# 
# exit(1)
# 
# # Read column names, fill col_values:
# col_names = []
# col_values = []
# for i, column in enumerate(sheet.iter_cols()):
#     if verbose:
#         print("column ", i, " = ", column, ", top value = ", column[0].value)
#     col_names.append( column[0].value )
#     col_values.append(       []       )
#     for cel in column[1:]:
#         if col_names[i] != None:
#             col_values[i].append( cel.value )
# 
# # name2index associates a column name to a column number
# name2index = collections.OrderedDict()
# for i, elem in enumerate(col_values):
#     if col_names[i] != None:
#         name2index[ col_names[i] ] = i
#         if verbose:
#             print("Category ", i, " (", col_names[i], "): ", elem)
# 
# # a column may be considered as a multiset;
# # name2set associates a column name to the distinct entries in the column multiset
# name2set = collections.OrderedDict()
# for name in name2index:
#     name2set[name] = set(col_values [ name2index[name] ])
#     print("name ", name, " is associated to set ", name2set[name])
# 
# 
# with open("tblclass.h","w") as f:
#     f.write("#define RECORD_NR {}\n".format( len(name2index) ))
#     f.write("#define FIELD_NR {}\n".format( len(col_values[0])+1 ))
#     f.write('')
# 
#     f.write("typedef char *record_t[FIELD_NR];\n")
#     f.write("typedef struct { record_t **table; int n; } table_t;\n");
#     f.write('')
# 
# with open("docs.h","w") as f:
#     f.write("record_t docs[] = {\n")
#     for i, name in enumerate(name2index):
#         f.write('\t/* {} */\t{{ "{}", '.format(i, name))
#         for j, value in enumerate( col_values [ name2index[name] ] ):
#             f.write('"{}",'.format(value))
#             if (j+1) % 5 == 0:
#                 f.write("\n\t")
#         f.write('},\n')
#     f.write('};\n\n')
# 
#     f.write('int block_nrs[] = {')
#     for name in name2index:
#         f.write('{}, '.format( len(name2set[name]) ))
#     f.write('};\n\n')
# 
#     f.write('char *blocks[RECORD_NR][FIELD_NR] = {\n')
#     for nameset in name2set:
#         f.write('\t{ ')
#         for i, name in enumerate(name2set[nameset]):
#             f.write('"{}",'.format(name))
#             if (i + 1) % 6 == 0:
#                 f.write("\n\t")
#         f.write('\t},\n')
#     f.write('};\n\n')
# 
# 
# 
