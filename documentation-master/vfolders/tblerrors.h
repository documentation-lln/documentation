#define ERRCODE_NULL_PTR	(-1)
#define ERRCODE_VALUE		(-2)
#define ERRCODE_CANTOPEN	(-3)
#define ERRCODE_MALLOC		(-4)
#define ERRCODE_INCONSISTENCY	(-5)
#define ERRCODE_MISSING_ARG	(-6)

int errcode;

void check_error() {
	char *s;
	switch(errcode) {
		case ERRCODE_NULL_PTR:		s = "unexpected null pointer encountered";
						break;
		case ERRCODE_VALUE:		s = "erroneous value of an input parameter";
						break;
		case ERRCODE_CANTOPEN:		s = "could not initialize object / file";
						break;
		case ERRCODE_MALLOC:		s = "allocation failed";
						break;
		case ERRCODE_INCONSISTENCY:	s = "inconsistency detected";
						break;
		case ERRCODE_MISSING_ARG:	s = "argument is missing";
						break;
		case 0:				s = "no error detected";
						break;
		default:			s = "unknown error code";
	}
	if (errcode < 0)
	fprintf(stderr, "Erroneous condition detected: \"%s\" (%d)\n", s, errcode);
}
