#!/usr/bin/env python

# Prerequisites:
#    sudo apt install graphviz
#    pithon -m pip install anytree
#    pithon -m pip install graphviz

from python_data import root
from anytree import Node, RenderTree
from anytree.exporter import DotExporter
from shutil import copy
from sys import argv


notree = False
if len(argv) == 2 and argv[1] == '--notree': notree = True

if not notree:
    for pre, fill, node in RenderTree(root):
        print("%s%s" % (pre, node.name))

DotExporter(root).to_picture('folders.png')
copy('folders.png', 'vfolder')
