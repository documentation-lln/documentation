	/* This simple lex program catches expressions like
		\n
		{
		blablahblah}
		\n

	   where blablahblah can also include { and }
	   and outputs
		\n
		blablahblah
		\n

	   v0.1, Di Aug 20 11:48:12 CEST 2019
	   v0.2, Mi Aug 21 11:48:40 CEST 2019
	 */

	#define BUFSIZE 100000
	char buffer[BUFSIZE];
	int  buffy = 0; /* = 0 is not really needed though it doesn't harm */
	void put_buffer(char buffer[], int size);

SPACE	[ \n\t\r]
NUMBER  ([0-9]*)

%%
\n\n	{
		if (buffy > 0) {
			put_buffer(buffer, buffy);
			buffy = 0;
		}
	}
(.|\n)	{
		if (buffy >= BUFSIZE) {
			fprintf(stderr, "Error, buffy is %d\n", buffy);
			put_buffer(buffer, buffy);
			exit(1);
		}
		buffer[buffy++] = *yytext;
	}
%%

int main() {
	yylex();
	if (buffy > 0) {
		put_buffer(buffer, buffy);
		buffy = 0;
	}
}

void put_buffer(char buffer[], int size) {
	if (buffer[0] == '{' && buffer[size-1] == '}') {
		buffer[size-1] = '\0';
		puts(buffer+1);
	} else {
		buffer[size] = '\0';
		puts(buffer);
	}
}
