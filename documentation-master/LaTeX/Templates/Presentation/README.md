# The Git documentation project: an example of a LaTeX presentation

In this folder you may find the LaTeX source of the presentation I gave on January 20, 2020, in whic I described the
Git documentation project and its intended use. It is a LaTeX beamer document that you may use as a template
for your own presentations.

Should you have questions please do not hesitate to contact me! ^\_^
