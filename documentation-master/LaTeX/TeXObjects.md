# TeXObjects

- Let me call "**standard object**" an entity -- such as a figure, or an equation -- that should be used
  consistently across all our documents.

- Let me call "**standard label**" a string structured in a way that captures the meaning of a standard object.
  For instance, "**fig:FrontEnd**"  will be the standard label of a picture representing the accelerator's front-end,
  while "**eq:RadialForce**" will be the standard label for the LaTeX code that should be used to render the equation
  for the radial force. Moreover, "**term:Injector** will be the standard label designating a beam line component
  according to the new naming convention that is being created by Dirk, Jorik, and Ulrich are currently defining.

- Let me call "**TeXObject**" the LaTeX code that renders a standard object referenced by its standard label.

I have written a Python module to dynamically create a TeXObject given its standard label.
I also created two LaTeX macros to exemplify the usage of said module.
If I type in a LaTeX document

```
    \TeXFigure{fig:FrontEnd}
```

standard label **fig:FrontEnd** will be turned in a LaTeX figure of the accelerator's front-end.

And if I type

```
    \TeXEquation{eq:RadialForce}
```

standard label **eq:RadialForce** will be turned in a LaTeX equation expressing radial force.

Moreover, if I type

```
    \TeXObject{term:Injector}
```

that standard label will be translated into an associated official term for beam line component
"term:Injector".

As an example,

![](images/TeXObjects1.png)

produces the following output:

![](images/TeXObjects2.png)



## ADVANTAGES

- Univocal, well-defined definition and representation for standard objects.
- Objects can also represent a "standard" name convention for the beam line components.
- Standard objects will be reusable consistently across all the documents that follow my approach.
- By changing the standard objects database, all documents using the database will be automatically updated and made consistent
  with the new naming convention.
- Writing LaTeX documents is simplified: you don't need to know how to add an image; you just specify its standard label!
  You don't need to bother about LaTeX's Math mode: you just specify a formula's standard label!


## Technicalia

TeXObjectsDB is a Python module that returns a string associated to a LaTeX label.
The returned string is the LaTeX code required to render an object associated to a LaTeX label.
The TeXObjects code makes use of TeXObjectsDB and exemplifies it. Its command line arguments
are interpreted as LaTeX labels and dereferenced via TeXObjectsDB. The obtained LaTeX code is printed out.

In this example, string "fig:FrontEnd" is found to be associated with a command to define a LaTeX figure:
```
$ ./TeXObjects.py fig:FrontEnd
\includegraphics[width=.8\textwidth]{MYRRHATDRPartB-img/MYRRHATDRPartB-img008.png}
\caption{The injector's front end section.}
\label{fig:FrontEnd}
```


In next example, string "eq:RadialForce" is found to be associated with the LaTeX representation of a mathematical formula:
```
$ ./TeXObjects.py eq:RadialForce
F_r=\frac{(1-\beta^2)}{\beta}\frac{\mathit{qI}}{2\pi\varepsilon_0c}\frac r{R^2}\ \ \ \ (\hbox{for }r<R)
\label{eq:RadialForce}
```

In this final example, string "term:Injector" is found to be equal to the standard term "injector". LaTeX code to add a new
entry in the table of indices is also issued:

```
$ ./TeXObjects.py "term:Injector" 
injector\index{injector}

```

## Further information
- The [report template](https://git.myrrha.lan/vdflorio/documentation/blob/master/LaTeX/Templates/Report/template-reports.pdf)
  and [presentation template](https://git.myrrha.lan/vdflorio/documentation/blob/master/LaTeX/Templates/Presentation/gitdoc.pdf)
  provide further information about TeXObjects!

- The first few chapters of Part B of the [TDR](https://git.myrrha.lan/vdflorio/documentation/tree/master/TDR) now adopts
  this approach. Have a look for instance to [Chapter 8](https://git.myrrha.lan/vdflorio/documentation/blob/master/TDR/TDR-B-Chapters/TDR-B01-Injector.tex):

  ![](images/TeXObjects4.png)

## Latest news
The TDR currently makes use of this approach only when the macro `\EnableTeXObjects` has been defined. This allows
the user to choose between the traditional and the new approach to rendering figures, equations, and standard terms.
This is exemplified by the following -- an excerpt from Section 8 of the TDR:


```
\ifdefined\EnableTeXObjects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\TeXFigure{fig:Reference-Linac}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\else
\begin{figure}[H]
\centering
  \includegraphics[width=5.611in,height=2.7563in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img007.png}
  \caption{The reference linac injectors.}\label{fig:Reference-Linac}
\end{figure}
\fi
```
