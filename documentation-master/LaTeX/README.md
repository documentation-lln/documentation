# Rationale

In this folder I will provide information about LaTeX: templates that could be used to create articles and presentations;
descriptions of macros and packages that I find useful... etc.

If you have to write a short paper with not so many figures; or a poster; or a document that needs to be dynamically updated with
the contents of, e.g., an Excel sheet, then probably using M\$ software is not such a bad idea;
but if your document is rather large; or it includes a lot of figures; then LaTeX *is* a solution to be considered.
On my Linux VM, one run of pdflatex processes the whole TDR (351 pages including figures, tables, formulas etc.) in about 42.5 seconds:

    $ time pdflatex MYRRHA-ACCELERATOR-TDR.tex
    real    0m42,448s
    user    0m33,316s
    sys     0m0,501s

(If references have changed, or the index table has changed, then LaTeX may require processing the document a second and in some cases a third time.)

An important aspect of LaTeX is that its time complexity is roughly linear with the number of input lines. In other words,
it exhibits an excellent _scalability_: If you process
a document ten times larger than the TDR, you can expect pdflatex to take slightly more than ten times as with the TDR.
The situation is slightly different with, e.g., M\$ Word...

LaTeX is a markup language, like XML or HTML. It describes contents ─ sentences, formulas, pictures, bibliographic references ─ and
containers ─ pages, chapters, sections, figures, tables, indices, "boxes"... The general shape is described, but the actual
arrangement of the elements and objects is carried out by a program. Typically nowadays that program is called _pdflatex_ ─ a name
that basically means "_give me your LaTeX input and I will produce a pdf with it_".

The LaTeX input is included in a file whose file type is ".tex". The output goes on an Adobe PDF file, which ends in ".pdf".

LaTeX's behaviour is specified and extended by using "packages"...

LaTeX was written by [Laslie Lamport](https://en.wikipedia.org/wiki/Leslie_Lamport)-sama, who extended the original
TeX system created by [Donald Knuth](https://en.wikipedia.org/wiki/Donald_Knuth)-kamisama.

It's just the beginning of the story of course; and if you want to learn more about LaTeX
through nice tutorials, try [LearnLaTeX.org](https://www.learnlatex.org/en/)!

#  Templates

Here I will collect LaTeX templates. For the time being, only one template is available: [template-reports.tex](https://git.myrrha.lan/vdflorio/documentation/tree/master/LaTeX/template-reports.tex). That's a very preliminary template for technical reports ─ the same one that I
use for the [TDR](https://git.myrrha.lan/vdflorio/documentation/tree/master/TDR).

Note that [template-reports.tex](https://git.myrrha.lan/vdflorio/documentation/tree/master/LaTeX/template-reports.tex)
refers to three external files, called 
[template-reports-section-one.tex](https://git.myrrha.lan/vdflorio/documentation/tree/master/LaTeX/template-reports-section-one.tex),
[template-reports-section-two.tex](https://git.myrrha.lan/vdflorio/documentation/tree/master/LaTeX/template-reports-section-two.tex),
and
[template-reports-section-three.tex](https://git.myrrha.lan/vdflorio/documentation/tree/master/LaTeX/template-reports-section-three.tex).

Once you download them, try and compile them by executing command

    $ rubber --pdf template-reports.tex

([rubber](https://launchpad.net/rubber/) is a utility that calls pdflatex and all the other tools necessary to create, e.g., indices, bibliography,
and tables. It's kind of an automagic "make" that "_just does what's needed_" to produce your pdf output from the LaTeX
sources... Nice to have!)

# Pictures
Often the pictures included in a LaTeX document are produced with external tools. Each tool usually comes with its
own fonts, which are often quite different from LaTeX's. A way to solve this is described [here](http://www.jchl.co.uk/computing/wordlatex.htm): you can install
[the following TTFs](https://ctan.org/tex-archive/fonts/cm/ps-type1/bakoma/ttf/).
I tested this approach on Windows and it works like a charm :smile:

Note:

- to install the TTFs on Windows, right-click each TTF file and select "install".
- to install the TTFs on Linux, do as follows:
  - create folder `/usr/share/fonts/truetype/tex`
  - copy your TTFs in `/usr/share/fonts/truetype/tex`
  - `chmod 755 /usr/share/fonts/truetype/tex/*`
  - `sudo mkfontdir`


# ChkTeX

"How I wish there would exist a lexical analyzer for LaTeX, as `lint` is for the C language!"
Oh but that exists. It's ChkTeX, available [here](http://git.savannah.nongnu.org/cgit/chktex.git/)
Instructions for installing it are as follows:

```
    git clone https://git.savannah.nongnu.org/git/chktex.git`
    cd chktex/chktex`
    ./autogen.sh
    ./configure
```

Then you have to apply the patch described [here](https://askubuntu.com/questions/1134226/error-while-installing-chktex?rq=1):
you have to edit the Makefile, look for the lines starting with `LIBS=` and `LDFLAGS=` and change them as follows

```
LIBS=-ltermcap -lpcreposix -lpcre
LDFLAGS= 
```

After this, do a `make` and then `sudo make install`.

ChkTeX makes it possible to find and correct a lot of issues; for instance, missing non-breaking spaces:

    Warning 2 in TDR-B-Chapters/TDR-B03-MainLinac.tex line 311: Non-breaking space (`~') should have been used.
    The main RF characteristics of the MINERVA spoke cavities are discussed in details in \ref{sect:ss-cavities}.
