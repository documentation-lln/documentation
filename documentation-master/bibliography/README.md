# Rationale

This directory includes what I hope is going to become the MYRRHA Accelerator Team's bibliographic references database.
In other words, here you have the [BibTeX](https://en.wikipedia.org/wiki/BibTeX) repository that is meant to centralize all of our bibliographic references.
Team members are encouraged to clone the repository and use it for their articles, posters, presentations ─ of course,
those based on LaTeX ;-)

The goal is to come up with a group-wise "standard" reference labelling scheme. I will work on this and propose my own,
though you are all very much encouraged to suggest a different and more effective scheme.


# Usage

The usage is simple: just add the following line to your LaTeX source:

    \bibliography{/path/to/the/bibliography/tdr}

where /path/to/the/bibliography/tdr is your local path to file tdr.bib

As an example, in documentation/TDR/MYRRHA-ACCELERATOR-TDR.tex I have the line

    \bibliography{../bibliography/tdr}

which loads tdr.bib and allows to cite any of the references in that database.


# Naming scheme

For the sake of efficient reusability,
it is important to adopt a coherent naming scheme for the labels of our bibliographic records. (Note: labels of the
bibliographic records are *not* the labels that will appear in your final document. They are identifiers used when
you want to *refer* to a bibliographic record in your document. They are typically used with the \cite command.)

I propose the following scheme:
the first letter of the label, when followed by the separator "-", identifies the reference class. Current classes are

- B for Books
- C for Chapters in books
- D for Deliverables
- J for Journals
- M for Minutes
- N for Notes
- P for Proceedings
- R for technical Reports and manuals
- S for presentation Slides
- T for Theses

Moreover, class "O" will be used for any Other type of reference ─ web pages, for instance.

Following letters are structure depending on the reference class. This is detailed below.

## Reference classes and examples

- Books: **B-PublicationYear-Author-Editor**

  As an example, \cite{_B-2008-Wangler-Wiley_} will produce the following entry in the bibliography:

  Wangler, T. P. *RF Linear Accelerators*. Wiley-VCH, 2008. 2nd completely revised and enlarged edition.

- Deliverables: **D-Project-number.number**

  As an example, \cite{_D-MAX-4.2_} will produce the following entry in the bibliography:

  - Pitigoi, B. A. E., and Fernandez Ramos. MAX Deliverable 4.2, November 2012.

- Journals: **J-JournalName-Year-Volume.Number-Author**

  As an example, \cite{_J-NuclInstrumMethodsPhysRes-2006-562.2-Bia_} will produce the following entry in the bibliography:

  - Biarrotte, J.-L., Bousson, S., Junquera, T., Mueller, A. C., and Olivier, A. A reference
    accelerator scheme for ADS applications. *Nuclear Instruments and Methods in Physics Research* Section
    A, Accelerators, Spectrometers, Detectors and Associated Equipment 562, 2 (June 2006), 656–661.

- Proceedings: **P-ConferenceYear-Author**

  As an example, \cite{_P-TCADS10-DeBruyn_} will produce the following entry in the bibliography:

  - De Bruyn, D., Aït Abderrahim *et al.*, Achievements and lessons learned within the
    Domain 1 "DESIGN" of the IP_EUROTRANS Integrated Project. In *Proceedings of the First Inter-
    national Workshop on Technology and Components for Accelerator-driven Systems* (Karlsruhe, Germany,
    2010), pp. 47–52.

- Technical reports: **R-Institution-Year-Number-Author**

  - Example still missing

- Theses: **T-Institution-Year-Author**

  - Example still missing

Other schemes will follow.

As a final note, as a side effect of applying the above naming scheme I have identified two duplicate entries in the reference database!

# Dynamic selection of reference entries

A coherent naming scheme allows a subset of entries from the bibliographic references database
matching a given pattern to be easily selected. I wrote the Python script *bib.py* to demonstrate this.
Its syntax is simple:

    $ bib.py -h
    usage: bib.py [-h] [-V] [--bibtex BIBTEX] [--regex REGEX] [--latex LATEX]

    bib.py applies regular expressions onto BibTex databases

    optional arguments:
      -h, --help            show this help message and exit
      -V, --version         show program version
      --bibtex BIBTEX, -b BIBTEX
                            specifies the BibTeX reference database to be
                            processed. Default value is tdr.bib
      --regex REGEX, -r REGEX
                            specifies the regular expression to be applied to the
                            BibTeX database. If absent, the user is required to
                            enter one
      --latex LATEX, -l LATEX
                            specifies the name of the output LaTeX report. Default
                            value is report.tex


## A detailed example

If one specifies

    $ ./bib.py --regex '^D-.*'

the regular expression '^D-.\*' is applied to all records in tdr.bib producing an output report in file report.tex:

    $ ./bib.py --regex '^D-.*'
    compiling report.tex...
    running: bibtex report.aux...
    compiling report.tex...
    running: bibtex report.aux...

The resulting report, listing in alphabetic order all records matching the given regular expression,
is compiled with pdflatex. Finally, the output is rendered with the evince pdf viewer:

![The output report produced by bib.py is displayed](bib-examplary-run.png)

The contents of report.tex are as follows:

```
\documentclass{article}
\usepackage{url}
\def\sckcen{SCK{\textbullet}CEN}
\begin{document}
  \nocite{D-CDT-2.4}
  \nocite{D-ESS-TDR}
  \nocite{D-EUROTRANS-1.40}
  \nocite{D-EUROTRANS-1.66}
  \nocite{D-EUROTRANS-1.69}
  \nocite{D-EUROTRANS-1.74}
  \nocite{D-KE1-2}
  \nocite{D-KE1-4}
  \nocite{D-KE1-D1a}
  \nocite{D-KE1-D1b}
  \nocite{D-KE1-D6}
  \nocite{D-MAX-1.1}
  \nocite{D-MAX-1.2}
  \nocite{D-MAX-1.4}
  \nocite{D-MAX-2.1}
  \nocite{D-MAX-2.4}
  \nocite{D-MAX-4.2}
  \nocite{D-MAX-4.3}
  \nocite{D-MAX-4.4}
  \nocite{D-PDSXADS-63}
\bibliographystyle{plain}
\bibliography{tdr.bib}
\end{document}
```


## Other examples

- bib -r '^P-[A-Za-z]\*10-.\*'

  selects all articles appeared in Proceedings in 2010

- bib -r 'O-WebPage-.\*'

  selects all web pages in the references database

## Hyperlinks in your bibliographic references

Hyperlinks are being added to the bibliographic records in file `tdr.bib`. Let me explain this with an
example. Say that in your document you include `tdr.bib` and cite Deliverable D1.1 of Project MAX.
In other words, you add

    \cite{D-MAX-1.1}

somewhere in your document.
When you produce the PDF of your document you'll have a bibliography record that looks like this:

![](hyperref1.png)

As you can see, in the above record there are three blue strings: MAX, SCK CEN/5382335, and 97.

- The first string points to whatever you labeled as `sect:projects:MAX` in your document.
  In other words, if you talk about MAX in your document, just add `\label{sect:projects:MAX}`
  there and your bibliographic record shall link to that page:
  ![](hyperref2.png)
  ![](hyperref3.png)
  (If you *do not* have a `sect:projects:MAX` label in your document, then there will just be no hyperlinks; no "broken link" will appear in this case.)
- The second string brings you to the document in question on Alexandria:
  ![](hyperref4.png)
  ![](hyperref5.png)
- and the third one brings you to the page where you cited this Deliverable:
  ![](hyperref6.png)

More information will follow...
