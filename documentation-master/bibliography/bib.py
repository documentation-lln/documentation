#!/usr/bin/env python
#
# bib.py, by Vincenzo De Florio, v1.0, Mi Dez  4 16:16:05 CET 2019
#
# Takes a input a regular expression on the BibTeX key
# Creates a report with all entries matching the regular expression
#
# Example:
#  $ python bib.py '^D-MAX.*' report.tex
#  Creates a LaTeX file with all references matching the example regular expression (in this case, all MAX Deliverables)
#  Converts the LaTeX file into PDF file report.pdf

# Requirements: pybtex
#   pip install pybtex

from pybtex.database import parse_file
import re
import os
import sys
import subprocess
import argparse

description = 'bib.py applies regular expressions onto BibTex databases'
parser = argparse.ArgumentParser(description = description)
parser.add_argument("-V", "--version", help="show program version", action="store_true")
parser.add_argument("--bibtex", "-b", help="specifies the BibTeX reference database to be processed. Default value is tdr.bib")
parser.add_argument("--regex",  "-r", help="specifies the regular expression to be applied to the BibTeX database. If absent, the user is required to enter one")
parser.add_argument("--latex",  "-l", help="specifies the name of the output LaTeX report. Default value is report.tex")
args = parser.parse_args()



if args.bibtex:
    bibtex = args.bibtex
else:
    bibtex = "tdr.bib"

bib_data = parse_file(bibtex)
keys = bib_data.entries.keys()



if args.regex:
    regex = args.regex
else:
    regex = raw_input("Insert a regular expression on the BibTeX keys (for instance, ^D-.*): ")

reObj = re.compile(regex) # reObj searches all keys matching regex myRegex; if myRegex is ^D-.* reObj searches all deliverables



if args.latex:
    latex = args.latex
else:
    latex = "report.tex"

with open(latex, 'w')  as f :
    f.write('\\documentclass{article}\n')
    f.write('\\usepackage{hyperref}\n')
    f.write('\\usepackage{url}\n')
    f.write('\\def\\sckcen{SCK{\\textbullet}CEN}\n')
    f.write('\\def\\plusplus{{{}\\nolinebreak[4]\\hspace{-.05em}\\raisebox{.4ex}{\\tiny\\bf ++}}}\n')
    f.write('\\begin{document}\n')
    f.write

    for key in sorted(keys) :
        if reObj.match(key):
            # print(key)
            f.write('  \\nocite{' + key + '}\n')

    f.write
    f.write('\\bibliographystyle{plain}\n')
    f.write('\\bibliography{%s}\n' % bibtex)

    f.write('\\end{document}\n')

subprocess.call(["rubber", "--pdf", latex])
subprocess.call(["rubber", "--pdf", latex])

basename = os.path.splitext(latex)[0]
if basename != "":
    subprocess.call(["evince", basename + ".pdf"])
