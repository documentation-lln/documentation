# bib.py, by Vincenzo De Florio, v1.0, Mi Dez  4 16:16:05 CET 2019
#
# Takes a input a regular expression on the BibTeX key
# Creates a report with all entries matching the regular expression
#
# Example:
#  $ python bib.py '^D-MAX.*' report.tex
#  Creates a LaTeX file with all references matching the example regular expression (in this case, all MAX Deliverables)
#  Converts the LaTeX file into PDF file report.pdf

# Requirements: pybtex
#   pip install pybtex

from pybtex.database import parse_file
import re
import sys
import subprocess
import argparse

description = 'bib.py applies regular expressions onto BibTex databases'
parser = argparse.ArgumentParser(description = description)
parser.add_argument("-V", "--version", help="show program version", action="store_true")
parser.add_argument("--bibtex", "-b", help="specifies the BibTeX reference database to be processed")
parser.add_argument("--regex",  "-r", help="specifies the regular expression to be applied to the BibTeX database")
parser.add_argument("--latex",  "-l", help="specifies the name of the output LaTeX report")
args = parser.parse_args()


if len(sys.argv) == 1:
    print('bib.py applies regular expressions onto BibTex databases')
    print('Usage: python bib.py -r REGEX -l LATEX -b BIBTEX')
    sys.exit (1)

if args.bibtex == "":
    args.bibtex = "tdr.bib"

bib_data = parse_file('tdr.bib')
keys = bib_data.entries.keys()

if args.regex == "":
    args.regex = raw_input("Insert a regular expression on the BibTeX keys (for instance, ^D-.*): ")

if len(sys.argv) > 1:
    myRegex = sys.argv[1]
else:
    # Try with    ^D-.*   or      ^P-.*
    myRegex = raw_input("Insert a regular expression on the BibTeX keys (for instance, ^D-.*): ")

# myRegex = raw_input("Insert a regular expression on the BibTeX keys: ")

reObj = re.compile(myRegex) # reObj searches all keys matching regex myRegex; if myRegex is ^D-.* reObj searches all deliverables

if len(sys.argv) > 2:
    outfile = sys.argv[2]
else:
    outfile = 'report.tex'

with open(outfile, 'w')  as f :
    f.write('\\documentclass{article}\n')
    f.write('\\begin{document}\n')
    f.write

    for key in sorted(keys) :
        if reObj.match(key):
            # print(key)
            f.write('  \\nocite{' + key + '}\n')

    f.write
    f.write('\\bibliographystyle{plain}\n')
    f.write('\\bibliography{tdr}\n')

    f.write('\\end{document}\n')

subprocess.call(["rubber", "--pdf", outfile])
subprocess.call(["rubber", "--pdf", outfile])
