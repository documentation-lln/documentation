# Documentation

This is the main repository for the documentation of the MYRRHA Accelerator.  This includes the TDR files, related data, and all LaTeX documents pertaining to the projects we take part into ─ for instance, the project deliverables. In a sense, this is the repository of the LaTeX _source documentation_ that can be used to produce a final _output documentation_, typically in the PDF format, to be uploaded on the Alexandria repository.

This repository and all its sub-repositories are maintained by Vincenzo De Florio. For information, suggestions, or any other related business, please contact me e.g. via [e-mail](mailto://vincenzo.de.florio@sckcen.be).

## Structure

The structure of this repository is currently being subjected to frequent revisions.

A partial, high-level representation is as follows:

```mermaid
graph TD;
%%{init: { 'securityLevel': 'loose' } }%%
  Documentation-->bibliography
  click bibliography "https://git.myrrha.lan/vdflorio/documentation/-/tree/master/bibliography"
  Documentation-->Deliverables
  click Deliverables "https://git.myrrha.lan/vdflorio/documentation/-/tree/master/Deliverables"
  Documentation-->include
  Documentation-->Proposals
  click Proposals "https://git.myrrha.lan/vdflorio/documentation/-/tree/master/Proposals"
  Documentation-->lib
  Deliverables-->MYRTE
  MYRTE-->D2.2
  Documentation-->other_stuff
  Documentation-->Util
  Documentation-->TDR
  Documentation-->fault-tracking
  click fault-tracking "https://git.myrrha.lan/vdflorio/documentation/-/tree/master/fault-tracking"
  click TDR "https://git.myrrha.lan/vdflorio/documentation/-/tree/master/TDR"
  TDR-->Part-A-Chapters+imgs
  TDR-->Part-B-Chapters+imgs
  Documentation-->LaTeX
  click LaTeX "https://git.myrrha.lan/vdflorio/documentation/-/tree/master/LaTeX"
  LaTeX-->Templates
  Templates-->Report
  Templates-->Presentation
  Proposals-->ACC-document-structure
  include-->Equations.tex
  include-->vdfio.h
  lib-->libvdfio.a
  lib-->vdfio.c
  click lib "https://git.myrrha.lan/vdflorio/documentation/-/tree/master/lib"
  Documentation-->vfolders
  click vfolders "https://git.myrrha.lan/vdflorio/documentation/-/tree/master/vfolders"
  Documentation-->util
  click utils "https://git.myrrha.lan/vdflorio/documentation/-/tree/master/util"
```

A more detailed, "tree" representation is as follows:

```
├── bibliography
│   ├── bib                                     Script to call bib.py (see below)
│   ├── bib-examplary-run.png			Picture showing how to use bib.py
│   ├── bib.py					Selects bibliographic reference matching a regular expression. Creates a report with the selected references.
│   ├── README.md				bibliography
│   └── tdr.bib					Main bibliographic references database
├── bin					Tools and utilities bin
├── Deliverables			Project deliverables
│   ├── MYRTE					Deliverables of MYRTE
│   │   ├── D2.2					MYRTE D2.2
│   │   │   └── RP2-MYRTE-662186-D2.2.tex                       Main LaTeX source
│   │   └── README.md					MYRTE Deliverables README
│   └── README.md				Project Deliverables README
├── fault-tracking			Prototypic implementation of a fault-tracking service for MINERVA
├── include				Standard "library" of definitions (e.g., equation macros)
│   ├── vdfio.h				Standard header file for the documentation project's knowledge extractors
│   └── Equations.tex
├── lib					Object code shared by the documentation project's knowledge extractors
│   └── libvdfio.a
├── LaTeX				LaTeX-related information
│   ├── Templates				LaTeX templates
│   │       ├── Report					A very rough template for a technical report
│   │       └── Presentation				A Beamer presentation of the Git documentation project
│   └── README.md				LaTeX folder README
├── README.md				The document you're reading now
├── TDR					The TDR
│   ├── a					Type "a" to visualize the PDF of the TDR
│   ├── acm.bst					Bibliographic style used in the TDR
│   ├── b					Type "b" to refresh the bibliographic references of the TDR
│   ├── f					Type "f 'whatever'" to edit the TDR section that mentions 'whatever'
│   ├── FrontPagePartB-v2p345.pdf		Fake front page of the TDR. To be replaced!!
│   ├── MYRRHA ACCELERATOR - TDR.pdf		PDF of the TDR
│   ├── MYRRHA-ACCELERATOR-TDR.tex		MAIN LaTeX MODULE OF THE TDR
│   ├── MYRRHATDRPartA-img			TDR pictures, Part A
│   ├── MYRRHATDRPartB-img			TDR pictures, Part B
│   ├── p					Type "p" to process MYRRHA-ACCELERATOR-TDR.tex into "MYRRHA ACCELERATOR - TDR.pdf"
│   ├── README.md
│   ├── TDR-A-Chapters				TDR chapters, part A
│   ├── TDR-B-Chapters				TDR chapters, part B
│   ├── v					Edits MYRRHA-ACCELERATOR-TDR.tex
│   └── vv					Launches editor by mentioning Part and Chapter
└── util					Utilities
    └── fgres-1.5					Fgres changes strings in files. Use wisely!
```

(see also [here](https://git.myrrha.lan/vdflorio/documentation/-/blob/master/tree.svg))


## Usage

In order to contribute to any of the documents in this repository, what you have to do is connect to [git.myrrha.lan](https://git.myrrha.lan) and clone the documentation repository.  For this you will need a Git client, such as [this one](https://desktop.github.com/).  "Cloning" a repository means that you will have a local copy of the documentation and TDR.  All changes to that documentation can then be "pushed" back to git.myrrha.lan: I will receive them and authorize their inclusion in the "official" repository (or discuss with you possible changes).  I'm the "owner" of the Git documentation project, so I'm the only one with write-access privileges to it.
Of course, in order to apply changes, you will need to edit the relevant LaTeX files. Or ask me to do it on your behalf :sweat_smile:

[This](https://git.myrrha.lan/vdflorio/documentation/blob/master/TDR/TDR-B-Chapters/TDR-B01-Injector.tex) is an example of a LaTeX file ─ the LaTeX section about the Injector. You can edit that file with any text editor, but if you want a higher-level, WYSIWYG editor you should install one. One such editor is [Lyx](https://www.lyx.org/).  It seems powerful, although personally I prefer to "see" the LaTeX code and control the processing more closely, so I use vim with the [macros I wrote](https://git.myrrha.lan/vdflorio/documentation/blob/master/util/.vimrc) and the [Kent vim extensions](https://www.informatico.de/kent-vim-ext/). Their text folds are quite powerful[^1]...

Another possibility is to use the all-powerful [Emacs](https://www.gnu.org/software/emacs/), which can be configured with the [auctex](https://www.gnu.org/software/auctex/) LaTeX macros and a [Git front end](https://www.emacswiki.org/emacs/Git).

Yet another possibility is that you work out your changes in Word and then I manage the LaTeX translation.  Hybrid solutions are also possible of course :simple_smile:

When editing your LaTeX branches, please use the following [guidelines](https://git.myrrha.lan/vdflorio/documentation/blob/master/remarks.md) -- thanks very much!

## Standard library and Equation macros

I have added an `include` folder at the root of the documentation packages. This folder is meant to be used as a sort of group-wise "standard library" of LaTeX definitions ─ files logically similar to C and C++ header files that may be included and used in multiple documents. Again, the rationale is to avoid "reinventing the wheel" for each new document we are to produce. For the time being, the folder only includes a file called [Equations.tex](https://git.myrrha.lan/vdflorio/documentation/blob/master/include/Equations.tex), which is meant to provide definitions for the major equations used in the documents in this repository:

- Radial force equation.
- Lorentz pressure equation.
- Multipactor equation.
- Cold Tuning Sensitivity equation.
- Beam lifetime equation.
- Neutron population evolution equation.
- Neutron flux ratio equation.

In order to use the above macros, just include [Equations.tex](https://git.myrrha.lan/vdflorio/documentation/blob/master/include/Equations.tex):

```
    \input{../include/Equations.tex}
```

and then use the macro names defined in it. As an example,

```
    The evolution of the neutron population during the beam interruption can be expressed (under point
    kinetics approximation with a single delayed neutron group with decay constant $\lambda$) through the
    formula~\eqref{eq:reacmon:neutron-evol}~\cite{T-NormandieUni-2016-Chevret}.

    \begin{equation}
            \EqNeutronPopulationEvolution,
            \label{eq:reacmon:neutron-evol}
    \end{equation}
```

produces

![](images/EquationsDotTexExample.png)


## Alexandria

An important prerequisite to consistency is that the _output documentation_ ─ again, the PDF files produced by LaTeX ─ be automatically uploaded onto Alexandria creating there a new version of, say, a Deliverable, or a technical report, or any other Alexandria document. In order to guarantee said consistency, the `git push` action that updates the present repository should be part of an atomic action that creates the Alexandria version: the action should succeed _if and only if_ both the "git push" and the version creation succeed, and fail otherwise.


[No such transaction service appears to be available](md/alex1.md) :anguished:, though an incomplete, simplified workaround has been identified and is being used as explained [here](md/alex2.md).


## Special features
I have been suggested to create a mechanism to refer automatically to figures and equations in a LaTeX document. I've given this a thought or two and come up with a tentative solution, which is described [here](https://git.myrrha.lan/vdflorio/documentation/blob/master/LaTeX/TeXObjects.md).  Please let me know your opinions and if I should develop it into a full-fledged service!

## Writing style
I recommend to loosely refer to the style guidelines [here](https://www.springer.com/journal/11004/submission-guidelines).  Note in particular that "unless falling at the beginning of a sentence, Section(s), Figure(s) and Equation(s) should be shortened to Sect(s). Fig(s)., and Eq(s). as appropriate."

## PDF viewers
Given the size of our documents, and in particular of the TDR, I suggest to make use of a viewer with a "back-to-previous-view" functionality. Okular is one such PDF viewer. On Linux systems it is often part of the standard software -- I could install it by issuing command

    sudo snap install okular

If you use Okular, ALT + SHIFT + LEFT shall bring you to the previous visited locus in your document.



### Footnotes
[^1]: Yes I worked on Transputers in the past...
