# Contributing to the documentation

In order to contribute to any of the documents in this repository,
what you have to do is connect to git.myrrha.lan and clone the documentation repository.
For this you will need a Git client, such as [this one](https://desktop.github.com/).
"Cloning" a repository means that you will have a local copy of the documentation and TDR.
All changes to that documentation can then be "pushed" back to git.myrrha.lan: I will receive them
and authorize their inclusion in the "official" repository (or discuss with you possible changes).
I'm the "owner" of the Git documentation project, so I'm the only one with write-access privileges to it.

Of course, in order to apply changes, you will need to edit the relevant LaTeX files. Or ask me to do it on your behalf :sweat_smile:

[This](https://git.myrrha.lan/vdflorio/documentation/blob/master/TDR/TDR-B-Chapters/TDR-B01-Injector.tex) is an example of a LaTeX
file ─ the LaTeX section about the Injector. You can edit that file with any text editor, but if you want a higher-level,
WYSIWYG editor you should install one. One such editor is [Lyx](https://www.lyx.org/).
It seems powerful, although personally I prefer to "see" the LaTeX code and control the processing more closely, so I use
vim with the [macros I wrote](https://git.myrrha.lan/vdflorio/documentation/blob/master/util/.vimrc).
(Recently I started using also the [Kent vim extensions](https://www.informatico.de/kent-vim-ext/). Their text folds
are quite powerful[^1]...)

Another possibility is to use the all-powerful [Emacs](https://www.gnu.org/software/emacs/), which
can be configured with the [auctex](https://www.gnu.org/software/auctex/) LaTeX macros and a
[Git front end](https://www.emacswiki.org/emacs/Git).

Yet another possibility is that you work out your changes in Word and then I manage the LaTeX translation.
Hybrid solutions are also possible of course :simple_smile:

When editing your LaTeX branches, please use the following [guidelines](https://git.myrrha.lan/vdflorio/documentation/blob/master/remarks.md) -- thanks very much!

# Style

I recommend to loosely refer to the style guidelines [here](https://www.springer.com/journal/11004/submission-guidelines).
Note in particular that "unless falling at the beginning of a sentence, Section(s), Figure(s) and Equation(s) should
be shortened to Sect(s). Fig(s)., and Eq(s). as appropriate."
