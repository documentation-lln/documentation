\section{Summary of the program development}
\subsection{Technical choices made for a dependable amplifier}
A list of decisions has been agreed together with experts to ensure the highest level of reliability:


\begin{itemize}
\item Oversized transistors and passive components.
\item LDMOS in Plastic package Rth= 0.05\textdegree{}C/W.
\item Brazed transistors for lower junction temperature.
\item High quality (low Dk- high thermal conductivity) RF laminate (PCB).
\item Optimized water cooling (heat carrier).
\item No single point of failure (one driver per pallet).
\item Ultra-fast current protection (one per LDMOS).
\item Real time LDMOS junction temperature calculation.
\item Water cooled hot swap-able PS.
\item Redundant PSU (all in parallel).
\item Fully isolated combiners.
\end{itemize}
\subsection{MYRTE SSA cabinet design in a nutshell}
Here is a brief description of the main cabinet architecture (see Fig.~\ref{fig:ctrlsys:cadview}).

\begin{itemize}
\item One RF cabinet can accept up to 16 RF modules.
\item Each module is designed for 6kW CW output and has 50\% margin on silicon (transistor of 1500
W).
\item The module is 60mm high. This means that a stack of 16 racks is less than 1m high.
\item External combiners, which are part of the mechanical structure of the cabinet, are located on
both sides. They can be easily redesigned to accommodate for different frequencies or power.
\foreignlanguage{french}{(106MHz, 176MHz and 352MHz already exist)}
\item The roof can serve also as a hybrid combiner when one single, high power output is required.
\item The DC power supplies are high quality, hot swapable, liquid cooled rectifiers.
\item Up to 9 PS can take place in the cabinet (150kW DC power available).
\end{itemize}

\begin{figure}[H]
\centering
	\includegraphics[height=3in]{Final20report20Myrte-img/Final20report20Myrte-img005.png} 
\caption{CAD view of the SSA cabinet.}
\label{fig:ctrlsys:cadview}
\end{figure}


\subsection{The 6kW module}
The development of the 6kW module was the main work of the project. 

The MARISA/MYRTE amplifier started from a blank page and the work done has been huge.This resulted in extra costs beyond
the agreed budget.

Each amplifier module is mainly composed of:

\begin{itemize}
\item \foreignlanguage{french}{A Capacitor bank+ fuses}.
\item \foreignlanguage{french}{6 amplifier pallets}.
\item A 6 way/6kW RF combiner.
\item \foreignlanguage{french}{A 6kW dummy load}.
\item Control logic and measurement hardware.
\item \foreignlanguage{french}{Three high speed fans}.
\item \foreignlanguage{french}{A copper cooling loop}.
\end{itemize}
Everything is compacted inside a frame carefully machined out of a bulk piece of aluminum.

An RF module's major characteristics are as follows:

\begin{itemize}
\item Top side is dedicated to RF components (pallet amplifiers, power combiners, directional
couplers,{\dots})
\item Bottom side receives all the control and measurement electronics. This insures a high level of
RF shielding and allows high precision-noise free measurements.
\item Air flows in the front panel leaving enough space in the rear panel for cooling ports and DC
power connections.
\end{itemize}

\begin{figure}[H]
	\centering
	\includegraphics[height=4in]{Final20report20Myrte-img/img006.png}~
	\includegraphics[height=4in]{Final20report20Myrte-img/img007.png} 
\caption{Top side (left) and bottom side (right) of a RF module.}
\label{fig:ctrlsys:RFmodule}
\end{figure}

Figure~\ref{fig:ctrlsys:RFmodule} show the top side and bottom side of an RF module.

\subsection{Pallet amplifier development}
In order to accomplish this task, we had to learn first how to design a good amplifier pallet (1kW CW)
that would be easy to produce.

A significant effort has been put on the choice of the transistor and how to cool it---a  critical
aspect for reliability. For that, we have used the best tools available on the market for RF simulations
and heat transfer (see Fig.~\ref{fig:ctrlsys:effort}).

\begin{figure}[H]
	\centering
	\includegraphics[height=1.6in]{Final20report20Myrte-img/Final20report20Myrte-img008.jpg}~
	\includegraphics[height=1.6in]{Final20report20Myrte-img/Final20report20Myrte-img009.jpg} 
\caption{R\&D activity on the pallet design (left: heat transfer simulation, right: temperature
	measurement on real pallet.}
\label{fig:ctrlsys:effort}
\end{figure}


\subsection[Power combining technique]{Power combining technique}
We realized during the MARISA study how important it was to master RF combining techniques. Up to
200 amplifier pallets must add their power to the final output. The summing process must be done
with minimum losses, good impedance matching and good isolation.  

Extensive 3D electromagnetic simulations have been done to optimize the design---as exemplified in
Fig.~\ref{fig:ctrlsys:simu}.

%\includegraphics{Final20report20Myrte-img/img010.pdf} 

\begin{figure}[H]%[tbp]
\centering\includegraphics{Final20report20Myrte-img/img010.jpg} 
\caption{View of a 3D EM simulation that shows the good isolation properties of the combiner.}
\label{fig:ctrlsys:simu}
\end{figure}


The 6 way pallet combiners are classical Wilkinson type that use a recombinant architecture. Cabinet
combiners in the MYRTE design is a combination of Gysel types (high power Wilkinson), quadrature
hybrids and branch-line combiners. 

Wilkinson combiners are only used at moderate power (6kW in this case) where they are still efficient
enough. The design is easily realized in PCB technology using high grade laminates. Up to 6kW at
352MHz can be carried by a PCB track of 50 ohms. This gives a measured temperature of the copper in
the order of 110\textdegree{}C, which is acceptable for good reliability.

Gysel type combiners are in-phase combiners that allow higher power than Wilkinson since their isolation
resistors are referenced to ground (thus, not floating). They are well suited for combining a group of
4 modules of 6kW.

Hybrid and branch-line are both quadrature types that are well suited for high power and have been used
to combine the two groups of 24kW and in the roof (branch-line).

Figure~\ref{fig:ctrlsys:combiners} shows the MYRTE combiners that have been used for EM simulations
(copper parts only).

\begin{figure}[H]
\hskip-30pt
	\includegraphics[height=3in]{Final20report20Myrte-img/Final20report20Myrte-img011.png} 
\caption{MYRTE combiners (copper parts only) that have been used for EM simulations.}
\label{fig:ctrlsys:combiners}
\end{figure}


\subsection{Control system}
The control system of the amplifier (see Fig.~\ref{fig:ctrlsys:ovv})
has been an important part of the design work. The control system
must insure the operability of the device, but it takes also an important role in the protection of
the hardware.

\begin{figure}[H]%[tbp]
	\centering\includegraphics[width=6in]{Final20report20Myrte-img/Final20report20Myrte-img012.png}
	\caption{Overview of the global control system.}
\label{fig:ctrlsys:ovv}
\end{figure}

The global cabinet control is built around an industrial PLC (B\&R). This PLC ``talks'' with the
power supplies through a CAN-BUS interface. The RF modules communicate via an Ethernet connection
in MODBUS-TCP. RF modules can be accessed from the PLC but also directly through the Network
switch. The 6kW rack control architecture is shown in Fig.~\ref{fig:ctrlsys:ctrlarch}.

The PLC can talk to the outside in ProfiBus-Ethercat-Profinet depending on its hardware
configuration. 

The PLC is equipped with a large touch-screen that makes it possible to access all parameters and commands of
the whole amplifier.

\begin{figure}[H]%[tbp]
	\centering\includegraphics[width=6in]{Final20report20Myrte-img/Final20report20Myrte-img013.png}
	\caption{6kW rack control architecture.}
	\label{fig:ctrlsys:ctrlarch}
\end{figure}

Each RF module has its own controller based on a $\mu$controller. This $\mu$P is in
communication with all electronic boards to read voltage, currents, powers, temperatures,{\dots}
and can control locally LDMOS protection switches and cooling fans. Its cycle time is about 2ms.

\begin{figure}[H]
\hskip-20pt
		\includegraphics[width=3.5in]{Final20report20Myrte-img/Final20report20Myrte-img014.png}~
		\includegraphics[width=3.5in]{Final20report20Myrte-img/Final20report20Myrte-img015.png} 
\caption{Examples of control windows on the local screen.}
	\label{fig:ctrlsys:wins}
\end{figure}

