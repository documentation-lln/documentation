# MYRTE Deliverable D2.2 files

To generate the pdf of the deliverable in file MYRTE-D2.2.pdf, execute the following two commands:

    rubber --pdf "RP2-MYRTE-662186-D2.2.tex"
    pdftk MYRTE_662186_D2.2_frontpag1-2.pdf "RP2-MYRTE-662186-D2.2.pdf" cat output MYRTE-D2.2.pdf

