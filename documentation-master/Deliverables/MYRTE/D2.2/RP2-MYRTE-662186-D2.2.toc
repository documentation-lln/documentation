\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Introduction}{7}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Goal of the MYRTE task 2.2}{7}{section.1.1}% 
\contentsline {section}{\numberline {1.2}Advantages of SSA compared to vacuum tubes}{8}{section.1.2}% 
\contentsline {section}{\numberline {1.3}Achievements on the task 2.2}{8}{section.1.3}% 
\contentsline {chapter}{\numberline {2}Design and Manufacturing}{10}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Summary of the program development}{10}{section.2.1}% 
\contentsline {subsection}{\numberline {2.1.1}Technical choices made for a dependable amplifier}{10}{subsection.2.1.1}% 
\contentsline {subsection}{\numberline {2.1.2}MYRTE SSA cabinet design in a nutshell}{10}{subsection.2.1.2}% 
\contentsline {subsection}{\numberline {2.1.3}The 6kW module}{11}{subsection.2.1.3}% 
\contentsline {subsection}{\numberline {2.1.4}Pallet amplifier development}{12}{subsection.2.1.4}% 
\contentsline {subsection}{\numberline {2.1.5}Power combining technique}{12}{subsection.2.1.5}% 
\contentsline {subsection}{\numberline {2.1.6}Control system}{13}{subsection.2.1.6}% 
\contentsline {section}{\numberline {2.2}Production and testing of the amplifiers}{15}{section.2.2}% 
\contentsline {chapter}{\numberline {3}Calibration and Testing}{17}{chapter.3}% 
\contentsline {chapter}{\numberline {4}Integration in the CRC Vault}{19}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Mechanical integration}{19}{section.4.1}% 
\contentsline {section}{\numberline {4.2}Water cooling integration}{20}{section.4.2}% 
\contentsline {section}{\numberline {4.3}Electrical integration}{21}{section.4.3}% 
\contentsline {chapter}{\numberline {5}Commissioning}{23}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Low level commissioning}{23}{section.5.1}% 
\contentsline {subsection}{\numberline {5.1.1}Integrated combiners}{23}{subsection.5.1.1}% 
\contentsline {subsubsection}{Test bench setup.}{23}{section*.4}% 
\contentsline {subsubsection}{Results}{24}{section*.5}% 
\contentsline {subsection}{\numberline {5.1.2}External combiner}{25}{subsection.5.1.2}% 
\contentsline {subsubsection}{Test bench setup}{25}{section*.6}% 
\contentsline {subsubsection}{Results}{25}{section*.7}% 
\contentsline {subsection}{\numberline {5.1.3}RF measurements calibration}{26}{subsection.5.1.3}% 
\contentsline {subsubsection}{Test bench setup}{26}{section*.8}% 
\contentsline {subsubsection}{Results}{27}{section*.9}% 
\contentsline {section}{\numberline {5.2}Interlock tests}{28}{section.5.2}% 
\contentsline {section}{\numberline {5.3}High power commissioning}{28}{section.5.3}% 
\contentsline {subsection}{\numberline {5.3.1}On matched load}{28}{subsection.5.3.1}% 
\contentsline {subsubsection}{Test bench setup}{28}{section*.10}% 
\contentsline {subsubsection}{Results}{29}{section*.11}% 
\contentsline {subsection}{\numberline {5.3.2}On mismatched load and short-circuit}{36}{subsection.5.3.2}% 
\contentsline {subsubsection}{Test bench setup}{36}{section*.12}% 
\contentsline {subsubsection}{Results}{37}{section*.13}% 
\contentsline {section}{\numberline {5.4}Results overview}{40}{section.5.4}% 
\contentsline {section}{\numberline {5.5}Issues}{40}{section.5.5}% 
\contentsline {subsection}{\numberline {5.5.1}Fuse failures}{40}{subsection.5.5.1}% 
\contentsline {subsection}{\numberline {5.5.2}PCB failure}{42}{subsection.5.5.2}% 
\contentsline {subsection}{\numberline {5.5.3}Transistor failure}{43}{subsection.5.5.3}% 
\contentsline {chapter}{\numberline {6}Conclusions}{44}{chapter.6}% 
