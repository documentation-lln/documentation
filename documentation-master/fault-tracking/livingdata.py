#!/usr/bin/env python

# livingdata.py queries the control system via its REST API and returns a compact description of the I1 subsystems
# By vdflorio, 2021-09-14

this = 'livingdata.py'

import sys, os
from naming import i1

verbose = True
verbose = False

sys.path.insert(0, os.path.realpath('/home/vdflorio/Documents/Git/documentation/tracex/modules/'))

from LaTeXmacros import macros, destination

maxch = 20

CHs = [False] * maxch
Other = {}

for elem in i1:
    if elem[0][:2] == 'CH':
        ch = int (elem[0][2:])
        if ch > 0 and ch < maxch:
            CHs[ch] = True
        else:
            print(f'Unexpected CH cavity: CH{ch}. Ignored.', file=sys.stderr)
    else:
        Other[elem[0]] = True

mi = 0
ma = 0

if verbose: print('The following I1-elements are currently registered in the control system:')
for i, elem in enumerate(CHs[1:]):
    e = i+1
    if mi == 0 and elem:
        mi = e
        continue
    if elem:
        ma = e
    else:
        if mi != 0 and ma != 0:
            if verbose:
                print('I1-CH', end='')
                if mi < 10: print('0',  end='')
                print(f'{mi} -- I1-CH', end='')
                if ma < 10: print('0',  end='')
                print(f'{ma}')
            beforeME2 = ma
            mi = 0
            ma = 0

if verbose:
    keys = Other.keys()
    for k in keys:
        print(f'I1-{k}')

CURLON  = '{'
CURLOFF = '}'
with open(destination+'/'+macros, 'a') as f:
    f.write('\n%\n')
    f.write(f'% Addendum to {macros}, created by {this}.\n')
    f.write('%\n\n')
    f.write(f'\\def\\RESTBeforeMEii{CURLON}{beforeME2}{CURLOFF}\n\n')
    f.write(f'\\ifthenelse{CURLON}\\equal{CURLON}\\RESTBeforeMEii{CURLOFF}{CURLON}')
    f.write(f'\\TWinBeforeMEii{CURLOFF}{CURLOFF}%\n')
    f.write(f'\t{CURLON}\\message{CURLON}TEST PASSED: NO INCONSISTENCY DETECTED BETWEEN TRACEWIN AND REST API VALUES{CURLOFF}{CURLOFF}%\n')
    f.write(f'\t{CURLON}\\message{CURLON}TEST FAILED: INCONSISTENCY DETECTED BETWEEN TRACEWIN AND REST API VALUES{CURLOFF}{CURLOFF}\n')
    f.write('\n%\n')
    f.write(f'% EoF\n')
    f.write('%\n')
