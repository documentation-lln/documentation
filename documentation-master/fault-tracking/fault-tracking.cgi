#!/usr/bin/python3
# -*- coding: UTF-8 -*-

#{{{ Versioning and authorship
# Name:        fault-tracking.cgi
# Category:    Front-end
# Description: Front-end page of the fault tracking service
# Maturity:    Preliminary, minor tests
# Author:      vdflorio
#              0.1  / Preliminary, minor tests                  / Fr 25 Jun 2021 10:54:22 CEST
#              0.2  / Replaced temporary file strategy          / Fr 25 Jun 2021 14:42:11 CEST
version =     "0.2"
version_date                                                   = "Fr 25 Jun 2021 14:42:11 CEST"
#}}}

verbose = True
verbose = False

# Import modules for CGI handling
import cgi, cgitb
import openpyxl
import os, sys
import os.path
import tempfile
import subprocess
import time

cgitb.enable()

def getSurnameFromName(f):
    f.write("  function getSurnameFromName(name) {\n")
    f.write("      switch(name) {\n")
    f.write("         case 'vdflorio':\n")
    f.write("            return 'Vincenzo';\n")
    f.write("         case 'jbelmans':\n")
    f.write("            return 'Jorik';\n")
    f.write("         case 'dvandepl':\n")
    f.write("            return 'Dirk';\n")
    f.write("         case 'fdavin':\n")
    f.write("            return 'Fran&ccedil;ois';\n")
    f.write("         case 'wdcock':\n")
    f.write("            return 'Wouter';\n")
    f.write("         case 'agatera':\n")
    f.write("            return 'Ang&eacute;lique';\n")
    f.write("         case 'mgxavier':\n")
    f.write("            return 'M&aacute;rio';\n")
    f.write("         case 'lparez':\n")
    f.write("            return 'Laurent';\n")
    f.write("         case 'fpompon':\n")
    f.write("            return 'Franck';\n")
    f.write("         case 'aponton':\n")
    f.write("            return 'Aur&eacute;lien';\n")
    f.write("         case 'sboussa':\n")
    f.write("            return 'Sofiane';\n")
    f.write("         case 'pdfaille':\n")
    f.write("            return 'Philippe';\n")
    f.write("         case 'fdoucet':\n")
    f.write("            return 'Fr&eacute;d&eacute;ric';\n")
    f.write("         case 'mpitruzz':\n")
    f.write("            return 'Marco';\n")
    f.write("         default:\n")
    f.write("            return name;\n")
    f.write("      }\n")
    f.write("  }\n")


# Create instance of FieldStorage
form = cgi.FieldStorage()

print("Content-type:text/html\r\n\r\n")
print('<!DOCTYPE html>\n')
print("<html>")
print("<head>")
print("<title>MINERVA Fault Tracking Service</title>")
print('<link rel="stylesheet" href="ft.css">')
print("<script>")
getSurnameFromName(sys.stdout)
print("</script>")
print("</head>")

print("<body>")
print("<h1>MINERVA Fault Tracking Service</h1>")
if "id" in form.keys():
    user = form["id"].value
    print(f'<script>\n')
    print('try {\n')
    print(f' document.write(\'<h2>Welcome to the MINERVA Fault Tracking Service, \', getSurnameFromName(\'{user}\'), \'.</h2>\');\n')
    print('}\n')
    print('catch(err) {\n')
    print(f' document.write(\'<h2>Welcome to the MINERVA Fault Tracking Service, {user}. Err is \', err, \'</h2>\');\n')
    print('}\n')
    print(f'</script>')
    print(f"<p>Please select the system component you want to add a fault tracking record for.</p>")

    t = tempfile.mktemp(suffix='.svg', dir='.')
    t = user + "diagram.svg"

    try:
        f = open("diagram.svg", "rt")
        if verbose:
            print("<h2>diagram.svg opened for reading.</h2>")
    except:
        print("<h2>Cannot open diagram.svg -- access denied.</h2>")

    try:
        g = open(t, "wt")
        if verbose:
            print(f"<h2>{t} opened for writing.</h2>")
    except:
        print(f"<h2>Cannot open {t} -- access denied.</h2>")

    try:
        newText=f.read().replace('form.html?', f'form.html?id={user}&amp;')
        f.close()
        if verbose:
            print("<h2>diagram.svg read into memory and processed</h2>")
    except:
        print("<h2>Cannot read from diagram.svg -- access denied.</h2>")

    try:
        g.write(newText)
        g.close()
        if verbose:
            print(f"<h2>{t} written and closed.</h2>")
    except:
        print(f"<h2>Cannot write onto {t} -- access denied.</h2>")

    # print('<object type="image/svg+xml" data=http://tdr.myrrha.lan/vfolders/fault-tracking/diagram.svg>')
    print(f'<object type="image/svg+xml" data=http://tdr.myrrha.lan/vfolders/fault-tracking/{t}>')
else:
    print(f"<h2>No user passed with POST -- access denied.</h2>")

print("</body>")
print("</html>")

# attempts=3
# for i in range(attempts):
#     try:
#         os.remove(f'{t}')
#     except:
#         time.sleep(1)
# os.remove(f'{t}')
# while True:
#     try:
#         os.remove(f'{t}')
#         break
#     except:
#         time.sleep(1)
