#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#{{{ Versioning and authorship
module_name =      "list2diagram"
# Category:         Knowledge consumer
# Description:      converts the Python data structure `linac` into a hierarchy of SVG diagrams
# How to invoke:    ./list2diagram.py -i linac2 -o diagram2 -m naming   # creates diagram2.svg from naming.py,
#                                                                       # using the linac2 data structure
#                   ./list2diagram.py                                   # creates diagram.svg from list_representation.py,
#                                                                       # using the linac data structure
# Maturity:         Prototypic. Tested (to some extent)
# Author:           vdflorio
# Version:          0.1  / No functionality yet              / Do 10 Jun 2021 14:49:02 CEST
#                   0.2  / Prototypical implementation       / Mo 14 Jun 2021 16:19:42 CEST
#                   0.3  / Input and output are now specified
#                          specified on the command line     / Fr 09 Jul 2021 10:05:13 CEST
#                   0.3b / added invoking examples           / Mo 12 Jul 2021 15:49:47 CEST
version =          "0.3b"
version_date =     "Mo 12 Jul 2021 15:49:47 CEST"
#}}}

#{{{ Imports
from sys import argv
import sys, getopt
from os import  system
import collections
import pathlib
from pathlib import Path
from termcolor import colored
import urllib.parse
import subprocess
import os
from urllib.parse import quote
import importlib

# from list_representation import linac
linac = None
import_module_name = "list_representation"

from system_categories import system_categories
#}}}

node_ids = {}
node_sp = 0


dependencies = {}
url ="http://tdr.myrrha.lan/vfolders/fault-tracking/form.html"

#{{{ PlantUML component diagram composer
def d(f, level, child, parent):
    global node_sp
    global linac
    category=system_categories[level]
    if not parent in node_ids:
        node_ids[parent] = f'n{node_sp}'
        q = quote(parent)
        print(f'[{parent}] as n{node_sp}', file=f)
        print(f'url of n{node_sp} is [[{url}?{category}={q}]]', file=f)
        node_sp += 1
    if not child in node_ids:
        print(f'[{child}] as n{node_sp}', file=f)
        node_ids[child] = f'n{node_sp}'
        q = quote(child)
        print(f'url of n{node_sp} is [[{url}?{category}={q}]]', file=f)
        node_sp += 1

    assoc = '{} --> {}'.format(node_ids[parent], node_ids[child])
    if not assoc in dependencies:
        print(assoc, file=f)
        dependencies[assoc] = True
#}}}

#{{{ list2diagram
def list2diagram(f, sublist, level, d, parent=None):
    for t in sublist:
        (child, child_list) = t

        # for i in range(level*4):
        #     f.write(' ')
        # f.write(f'{child}\n')

        d(f, level, child, parent)

        list2diagram(f=f, sublist=child_list, level=level+1, d=d, parent=child)
#}}}

def help(the_input, the_output):
    l = len(module_name)
    spaces = ' ' * l
    print (f'{module_name} [ -i <input sheet> ] [ -o <output sheet> ] [ -m <module name> [ -h ]')
    print (f'{spaces} Default values:')
    print (f'{module_name} -i {the_input} -o {the_output} -m {import_module_name}')

#{{{ Main: Creation and remote copy of the SVG graph
def main(argv):
    global linac

    the_diagram = 'diagram'
    the_list = 'linac'
    the_module = import_module_name
    the_root = "Linac"

    try:
        opts, args = getopt.getopt(argv,"hi:o:m:r:",["input=","output=","module=","root="])
    except getopt.GetoptError:
        help(the_list, the_diagram)
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            help(the_list, the_diagram)
            sys.exit()
        elif opt in ("-i", "--input"):
            the_list = arg
        elif opt in ("-o", "--output"):
            the_diagram = arg
        elif opt in ("-m", "--module"):
            the_module = arg
        elif opt in ("-r", "--root"):
            the_root = arg

    linac = getattr(__import__(f"{the_module}"), f"{the_list}")

    with open(f'{the_diagram}.plantuml', 'w') as f:
        f.write('@startuml\n')
        f.write('skinparam wrapWidth 100\n')
        f.write('skinparam componentStyle rectangle\n')
        f.write('skinparam BackgroundColor #EEEBDC\n')
        f.write('skinparam handwritten true\n')
        list2diagram(f, linac, 0, d, parent=f"{the_list}")
        f.write('@enduml\n')

    command = subprocess.call(['bash', 'plantuml-svg', f'{the_diagram}.plantuml'])
    if command != 0:
        print(f"return code is {command}", file=sys.stderr)
    else:
        o = f'{the_diagram}.svg'
        command = subprocess.call(['scp', o, 'admin@10.5.3.18:public_html/vfolders/fault-tracking'])
        if command != 0:
            print(f"return code is {command}", file=sys.stderr)
        else:
            print(colored(f'Diagram transfer:  completed (http://tdr.myrrha.lan/vfolders/fault-tracking/{o})', 'green'), file=sys.stderr)
#}}}

if __name__ == '__main__':
    main(sys.argv[1:])

#
# EoF (list2diagram.py)
#
