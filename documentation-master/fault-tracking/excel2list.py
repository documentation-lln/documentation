#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#{{{ Versioning and authorship
# Name:        excel2list
# Category:    Knowledge extractor
# Description: converts an excel with fault tracking information, derived from the one created by Aurélien, into a Python data structure
# Maturity:    Corrected and, to some extent, tested
# Author:      vdflorio
# Version:     0.1  / Derived from BoM.py v. 0.7            / Do 10 Jun 2021 10:42:14 CEST
version =     "0.2"
#              0.2  / Corrected and, to some extent, tested / Mo 14 Jun 2021 16:16:26 CEST
#}}}

#{{{ Exemplification of the algorithm
#         [
#    1 A
#           ('A', [
#    2 AA	
#    	       ('AA', [
#    3 AAA
#    	               ('AAA', [
#    1 B
#                                   ]		=> "qt" * (3-1+1)
#    		       )
#    	              ]
#    	       )
#    	      ]
#            )
#          , ('B', [					=> "," new elem
#    2 BB
#                   ('BB', [
#    3 BBB
#                           ('BBB', [
#    3 BBC
#                                   ])
#                         , ('BBC', [
#    3 BBD
#                                   ])
#                         , ('BBD', [
#    4 BBDA
#    	                        ('BBDA', [
#    1 C
#    			                 ]      => "qt" * (4-1+1)
#    			        )
#                                   ]
#    		       )
#    	              ]
#    	       )
#    	      ]
#    	)
#          , ('C', [					=> "," new elem
#    1 D
#                  ]
#    	)
#          , ('D', [					=> "," new elem
#    2 DD
#                   ('DD', [
#    2 DE
#                          ]
#    	       )
#                 , ('DE', [
#    
#    --end--
#                          ]				=> "qt" * (2-0)
#    	       )
#    	      ]
#    	 )
#       ]
#}}}

#{{{ Imports
from sys import argv
from os import  system
import collections
import openpyxl
import pathlib
from pathlib import Path
from termcolor import colored
import urllib.parse
#}}}

verbose=False

if len(argv) != 2:
    xlsx_file = Path('.', 'Fault_Tracking_2.xlsx')
else:
    xlsx_file = Path('.', argv[1])

print(colored("...Processing sheet '{}'".format(xlsx_file),'green'))

wb_obj = openpyxl.load_workbook(xlsx_file) 

# Read the active sheet:
sheet = wb_obj.active

rows=sheet.max_row
cols=sheet.max_column

# First row is sheet[1]
# First elem of that row is sheet[1][0]
# >>> print(sheet[1][0].value)
# lvl

with open("list_representation.py", 'w') as f:
    prev_level = sheet[2][0].value
    prev_value = sheet[2][2].value
    first = True

    f.write( "linac = [\n");
    f.write(f"    ('{prev_value}', [\n");

    for i, row in enumerate(sheet.iter_rows(min_row=3)):
        new_level = row[0].value
        new_value = '"{}"'.format(row[2].value)

        if new_level == None:
            continue

        if   new_level > prev_level:
            for j in range(new_level*4):
                f.write(' ')
            f.write(f"({new_value}, [\n")

        elif new_level == prev_level:
            f.write("])\n")
            for j in range(new_level*2):
                f.write(' ')
            f.write(f", ({new_value}, [\n")

        else:
            pn = prev_level - new_level + 1
            for j in range(pn):
                f.write('])')

            for j in range(new_level*2):
                f.write(' ')
            f.write(f', ({new_value}, [')

        prev_level = new_level

    for i in range(prev_level):
        f.write("]) ")
    f.write("\n    ]\n")

print(colored("...done (output=list_representation.py).",'green'))

#
# Eof (excel2list.py)
#
