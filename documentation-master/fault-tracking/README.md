# Fault tracking service
A fault tracking service is an important prerequisite to collecting evidence of the resilience of an observed system, in turn a prerequisite to computing estimates of key non-functional properties such as reliability and availability.  Our linac is characterized by hard reliability requirements, therefore we need to assess its reliability -- therefore we need to track salient characteristics of its faults. A service must exist for operators and researchers to log events disrupting the expected behaviour of the accelerator -- such as malfunctions, software failures, failures due to external events, etc. Moreover, logging fault events should also be possible for software systems running in the various control system components, without the intervention of a human supevisor.

In order to achieve this, a software architecture has been proposed. Both client-side and server-side are supported by web technologies. A server side implements a minimalistic REST interface that logs entries in a PostgreSQL database.  A client side provides a classic HTML+JS form for human users to compose and submit database entries.  The form is dynamically composed by using a system structure and a record entry structure derived from a reference document (an excel spreadsheet). In what follows I briefly describe the above architectural components and then provide a view to a future extension to this service, based on the REST interface to the naming service of our control system.

[[_TOC_]]

## Client side: selection of the system component experiencing a fault
The entry point is the last icon in the first column of service at [home.myrrha.lan](http://home.myrrha.lan/):

![](images/01b.png)

By clicking on that icon, the user is brought to a page similar to the following one:

![](images/02.png)

Here the user is shown a representation of the system components hierarchy and is asked to select the system component s/he wants to add a fault tracking record for.

The adopted system components hierarchy is available as an SVG hyperlinked graph. The graph is created dynamically from an input source. The input source currently is the excel file [Fault_Tracking.xlsx](Fault_Tracking.xlsx).  From said excel file, I also derived the record entry model used when entering the fault tracking data.

Note: a second input source is
[the control system's naming service](http://naming.myrrha.lan:8080/naming/parts.xhtml?type=deviceStructure).
A python code issues a GET requests and parses the returned device structure to create a second SVG graph.
The graph is not fully operational (hyperlinks are not correct), though it can already be visited
[here](http://tdr.myrrha.lan/vfolders/fault-tracking/diagram2.svg).

## Client side: HTML form
When the user clicks on one of the nodes of the SVG graph, s/he is redirected to an HTML / Javascript form. In that form, the selected node, the time of issue, and the current project phase are all pre-filled fields. All the form fields are created dynamically by python code [excel2excel2.py](https://git.myrrha.lan/vdflorio/documentation/-/blob/master/fault-tracking/excel2excel2.py).  That very same code is also responsible for the creation of a CGI script representing the server-side of the fault tracking service.

![](images/03.png)
*As an example, here I select the "HV generation" node.*

![](images/04.png)
*As a result, a partially pre-filled form opens up.*

The form uses Javascript to collapse / expand the data entry fields.

## Server side: CGI script
When the user clicks on the "Submit" button, the data in the form is shipped via HTTP POST to [a CGI script](https://git.myrrha.lan/vdflorio/documentation/-/blob/master/fault-tracking/form.cgi), also created dynamically by python code [excel2excel2.py](https://git.myrrha.lan/vdflorio/documentation/-/blob/master/fault-tracking/excel2excel2.py).

![](images/05.png)
*The CGI script that implements the server side of the fault tracking service.*

Said CGI script stores the received data in a SQL table and print the command that a machine would require to achieve the very same result. In fact, as mentioned already, the HTML form is nothing but a human-oriented way to issue the above mentioned command.

The server side script runs on host `tdr.myrrha.lan`, and there it makes use of a PostgreSQL server.  The following pictures show the tool used to access the PostgreSQL table with the fault tracking records:

![](images/06.png)
*The tool pgadmin3 allows the SQL tables available on the server to be inspected.*

![](images/07.png)
*The picture displays the fault tracking table, whose last record is the one just inserted.*
