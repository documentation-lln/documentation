#!/usr/bin/env python3

# Naming
#
# This module queries the naming service via the REST api and constructs data structures compatible with list2diagram.py
# The latter can be called, e.g., via   ./list2diagram.py -i linac -o diagram2 -m naming, or
# Also -i i1r and -i cab are possible

import requests
import xml.etree.ElementTree as ET
import sys
import xml.dom.minidom

verbose = True
verbose = False

url = 'http://naming.myrrha.lan:8080/naming/rest/deviceNames'
if verbose: print('deviceNames')

r = requests.get(url)

root = ET.fromstring(r.text)

if verbose: print(root.tag)

# dictionary of dictionaries
linac_d = {}

for child in root:
    for i, elem in enumerate(child.iter()):
        if elem.tag == 'system':
            system = elem.text
        if elem.tag == 'subsystem':
            subsystem = elem.text
        if elem.tag == 'discipline':
            discipline = elem.text
        if elem.tag == 'deviceType':
            deviceType = elem.text
        if elem.tag == 'instanceIndex':
            instanceIndex = elem.text
        if elem.tag == 'systemGroup':
            systemGroup = elem.text
        if elem.tag == 'description':
            description = elem.text
        if elem.tag == 'name':
            name = elem.text
        if elem.tag == 'uuid':
            uuid = elem.text
        if elem.tag == 'status':
            status = elem.text
        # print(i, elem.tag, elem.text, elem.attrib)
    try:
        desc = f'{system}-{subsystem}:{discipline}-{deviceType}-{instanceIndex}'
        if desc == description:
            if verbose: print(f'{systemGroup}:    {desc} ({status}, {uuid})')
        else:
            if verbose: print(f'{systemGroup}:    {system}-{subsystem}:{discipline}-{deviceType}-{instanceIndex} ({description}, {status}, {uuid})')
        if status == 'OBSOLETE' or status == 'DELETED':
            continue
        if system not in linac_d:
            linac_d [ system ] = {}
        node_dict = linac_d [ system ]
        if subsystem not in node_dict:
            node_dict [ subsystem ] = {}
        node_dict = node_dict [ subsystem ]
        if discipline not in node_dict:
            node_dict [ discipline ] = {}
        node_dict = node_dict [ discipline ]
        if deviceType not in node_dict:
            node_dict [ deviceType ] = []
        node = node_dict [ deviceType ]
        if instanceIndex not in node:
            node.append(instanceIndex)
    except:
        continue
        # print('system unknown')

#  >>> print(linac_d.keys())
#  dict_keys(['I1', 'I1R', 'CAB'])
#  >>> print(linac_d['I1'].keys())
#  dict_keys(['CH07', 'ME1', 'LE', 'CH03', 'CH01', 'CH02', 'CH04', 'CH05', 'RFQ', 'IS', 'CH06', 'VCAB', 'ME2'])
#  >>> print(linac_d['I1']['LE'].keys())
#  dict_keys(['HYD', 'VAC', 'DF'])
#  >>> print(linac_d['I1']['LE']['HYD'].keys())
#  dict_keys(['FS', 'TS'])
#  >>> print(linac_d['I1']['LE']['HYD']['FS'])
#  [('5', 'Détection de débit sur Chopper'), ('6', 'Détection de débit sur EMI horizontal'), ('7', 'Détection de débit sur Faraday Cup'), ('1', 'Détection de débit sur le solénoïde 1 LEBT'), ('2', 'Détection de débit sur le solénoïde 2 LEBT'), ('3', 'Détection de débit sur EMI vertical'), ('4', 'Détection de débit sur Slits')]

l1 = linac_d

#linac = [ (k1, [ (k2, [ (k3, [ (k4, []) for k4 in list(l1[k1][k2][k3].keys()) ]) for k3 in list(l1[k1][k2].keys()) ]) for k2 in list(l1[k1].keys()) ]) for k1 in l1.keys() ]
#linac = [ (k1, 
#            [ (k2,
#                [ (k3,
#                    [ (k4, 
#                        l1[k1][k2][k3][k4]
#                      ) for k4 in list(l1[k1][k2][k3].keys())
#                    ]) for k3 in list(l1[k1][k2].keys())
#                ]) for k2 in list(l1[k1].keys())
#            ]) for k1 in l1.keys()
#        ]
#
#linac = [ (k1, 
#            [ (k2,
#                [ (k3,
#                    [ (k4, 
#                        [ (k5, []) for k5 in l1[k1][k2][k3][k4]
#                        ]) for k4 in list(l1[k1][k2][k3].keys())
#                    ]) for k3 in list(l1[k1][k2].keys())
#                ]) for k2 in list(l1[k1].keys())
#            ]) for k1 in l1.keys()
#        ]

linac = [ (k1, 
            [ (k2,
                [ (k3,
                    [ (k4, 
                        [ ( k5, []
                          ) for k5 in l1[k1][k2][k3][k4]
                        ]
                      ) for k4 in list(l1[k1][k2][k3].keys())
                    ]
                  ) for k3 in list(l1[k1][k2].keys())
                ]
              ) for k2 in list(l1[k1].keys())
            ]
          ) for k1 in l1.keys()
        ]

linac2 = [ (k1, 
            [ (k1+'-'+k2,
                [ (k1+'-'+k2+':'+k3,
                    [ (k1+'-'+k2+':'+k3+'-'+k4, 
                        [ ( k1+'-'+k2+':'+k3+'-'+k4+'-'+k5, []
                          ) for k5 in l1[k1][k2][k3][k4]
                        ]
                      ) for k4 in list(l1[k1][k2][k3].keys())
                    ]
                  ) for k3 in list(l1[k1][k2].keys())
                ]
              ) for k2 in list(l1[k1].keys())
            ]
          ) for k1 in l1.keys()
        ]


dummy, i1 = linac[0]
dummy, i1r = linac[1]
dummy, cab = linac[2]

with open("system_categories.py", "a") as f:
    f.write('system_categories = ["System", "SubSystem", "Discipline", "DeviceType", "Instance" ]\n')
