#!/usr/bin/env python

from naming import i1
from openpyxl import Workbook

filepath="dump-live-data-i1.xlsx"

row = 1

def node(sheet, n, level=1):
    global row
    spaces = " " * (level*2)
    if type(n) is list or type(n) is tuple:
        if n == []:
            return
        for elem in n:
            # print(f'{spaces}{n[0]}')
            node(sheet, elem, level+1)
    else:
        print(f'{spaces}{n}')
        sheet.cell(row=row, column=level).value = n
        row += 1

wb=Workbook()

sheet=wb.active

node(sheet, i1)

wb.save(filepath)
