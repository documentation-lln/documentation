#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#{{{ Versioning and authorship
module_name =  "excel2excel2.py"
# Category:    Data translator
# Description: input:  Fault_Tracking excel from Aurélien: two worksheets (data, legend)
#              output: new excel files structured as the bill-of-materials excel
#              output: HTML form structured as in the 'data' worksheet
#              output: CGI script receiving the data in the HTML form
# Maturity:    Preliminary, end-to-end, untested
# Author:      vdflorio
# {{{ Versioning
# Version:     0.1  / Scketched functionality
#              0.2  / Preliminary, untested functionality       / Fr 11 Jun 2021 16:25:28 CEST
#              0.3  / Added section for the creation of CSS     / Do 17 Jun 2021 11:17:40 CEST
#              0.4  / popup fieldsets and other CSS/JS features / Fr 18 Jun 2021 14:44:54 CEST
#              0.5  / first end-to-end processing (incomplete,
#                     not fully correct, excel backend)         / Mo 21 Jun 2021 10:47:15 CEST
#              0.6  / Now current date-and-time is default
#                     value for "Date and Time"* fields         / Di 22 Jun 2021 11:07:23 CEST
#              0.7  / Now the application dumps the curl state-
#                     ment corresponding to the current request
#                     Note: the curl statement *WORKS*!         / Di 22 Jun 2021 12:07:20 CEST
#              0.8  / Corrected issue with Time_Now. Various
#                     minor adjustments.                        / Mi 23 Jun 2021 11:55:57 CEST
#              0.9  / Added tooltips                            / Mi 23 Jun 2021 13:54:07 CEST
#              0.10 / Fine-tuned tooltip                        / Mi 23 Jun 2021 16:56:09 CEST
#              0.11 / Identity of user is now stored in the db  / Do 24 Jun 2021 14:32:12 CEST
#              0.12 / Various corrections and adjustments       / Fr 25 Jun 2021 10:50:58 CEST
#              0.13 / Fixed bug in the creation of the remote db/ Fr 25 Jun 2021 14:40:52 CEST
#              0.14 / Cleaning up and adjustments               / Fr 25 Jun 2021 14:40:52 CEST
#              0.15 / Exception handling                        / Mo 28 Jun 2021 10:36:43 CEST
#              1.00 / Major bug corrected                       / Di 29 Jun 2021 15:16:02 CEST
#              1.0a / Inserted comments with next actions       / Mi 30 Jun 2021 11:46:20 CEST
#              1.1  / Non-ascii characters are now properly
#                     encoded in HTML. Various adjustments      / Do 01 Jul 2021 11:38:19 CEST
#              1.2  / The module now remote-copies itself on
#                     the server-side                           / Do 01 Jul 2021 14:47:11 CEST
#              1.3  / Before inclusion in the DB, the record is
#                     now checked for unicity and consistency   / Fr 02 Jul 2021 11:18:13 CEST
#              1.4  / New consistency checks have been added    / Fr 02 Jul 2021 13:20:16 CEST
#              1.5  / The server side now creates a script
#                     that creates a prototypic PostgreSQL table
#                     for the fault tracking service            / Fr 02 Jul 2021 17:02:08 CEST
#              1.6  / The server side now writes a row in
#                     PostgreSQL table                          / Di 06 Jul 2021 17:00:52 CEST
#}}}

version =      "1.6b"
version_date = "Mi 07 Jul 2021 16:38:27 CEST"

module_fname = module_name.split('.')[0]

PROMPT_1 = "please insert value"
DSN = "host=localhost dbname=tdr user=tdr password=tdr"

# To do:
#              Check that the record to be added is well structured
#              Modify back-end so as to use an actual DBMS
#              - Adapt code so as to use Psycopg (https://www.psycopg.org/docs/)
#              - Agree on the data model
#              - Define a FaultTracking table
#}}}

#{{{ Imports
from sys import argv
import sys, getopt
import collections
import openpyxl
from pathlib import Path
import subprocess
import cgi, cgitb
import re
import datetime
import html

from unidecode import unidecode

from termcolor import colored
#}}}

#{{{ Global variables
COUNTER = 0
SERVICE = "http://tdr.myrrha.lan/vfolders/fault-tracking"
SSH_ADDR = "admin@10.5.3.18:public_html/vfolders/fault-tracking"
SSH_CAR = SSH_ADDR.split(':')[0]
SSH_CDR = SSH_ADDR.split(':')[1]
EXCEL_DB = "ftdb4.xlsx"

SQL_TABLE = "FaultTracking"

verbose = False
#}}}

#{{{ Fieldset & Fieldget
#{{{ Fieldset
def fieldset(f, hierarchy, level):
    global COUNTER
    if not hasattr(f, 'seek'):
         raise TypeError('fieldset: first argument is not a file')
    try:
        (the_legend, the_list) = hierarchy
    except:
        raise TypeError('fieldset: second argument is not a couple')
    if not isinstance(level, (int, float)) :
        raise TypeError('fieldset: third argument is not a number')

    spaces = '  ' * level
    if level < 4:
        if level == 2:
            f.write(f'{spaces}{spaces}<div class="tooltip{COUNTER}"> \n')
            f.write(f'{spaces}{spaces}<h3  id="collapser{COUNTER}">{the_legend}</h3>\n')
            f.write(f'{spaces}{spaces}<span class="tooltiptext{COUNTER}">Click here to expand / collapse the "{the_legend}" section</span>\n')
            f.write(f'{spaces}{spaces}</div><br>\n')
            f.write(f'{spaces}  <div id="collapsable{COUNTER}">\n')
            COUNTER += 1
        else:
            f.write(f'{spaces}  <h4>{the_legend}</h4>\n')
            # f.write(f'{spaces} <h3>{the_legend}</h3><br/>\n')

        for elem in the_list:
            fieldset(f, elem, level + 1)

        if level == 2:
            f.write(f'{spaces}  </div>\n')
            # f.write(f'{spaces}</div>\n')
    else:
        f.write(f'{spaces}  <label for="{the_legend}">{the_legend}</label><br>\n')
        # f.write(f'{spaces}  <input type="text" id="{the_legend}" name="{the_legend}" value="John"><br>\n')

        if   the_legend.startswith("Sub-Section"):
            f.write(f'{spaces}   <script>\n')
            f.write(f'{spaces}    document.write(\'<input type="text" id="{the_legend}" name="{the_legend}" value="\', getParameterByName(\'SubSection\'), \'"><br>\')\n')
            f.write(f'{spaces}  </script>\n')

        elif the_legend.startswith("Device instance"):
            f.write(f'{spaces}   <script>\n')
            f.write(f'{spaces}    document.write(\'<input type="text" id="{the_legend}" name="{the_legend}" value="\', getParameterByName(\'DeviceInstance\'), \'"><br>\')\n')
            f.write(f'{spaces}  </script>\n')

        elif the_legend.startswith("Component"):
            f.write(f'{spaces}   <script>\n')
            f.write(f'{spaces}    document.write(\'<input type="text" id="{the_legend}" name="{the_legend}" value="\', getParameterByName(\'Component\'), \'"><br>\')\n')
            f.write(f'{spaces}  </script>\n')

        elif the_legend.startswith("Sub-Component"):
            f.write(f'{spaces}   <script>\n')
            f.write(f'{spaces}    document.write(\'<input type="text" id="{the_legend}" name="{the_legend}" value="\', getParameterByName(\'SubComponent\'), \'"><br>\')\n')
            f.write(f'{spaces}  </script>\n')

        elif the_legend.startswith("MINERVA 100MeV"):
            f.write(f'{spaces}  <input type="text" id="{the_legend}" name="{the_legend}" value="yes"><br>\n')
        elif the_legend.startswith("Minerva 600MeV"):
            f.write(f'{spaces}  <input type="text" id="{the_legend}" name="{the_legend}" value="no"><br>\n')
        elif the_legend.startswith("MYRRHA+REACTOR"):
            f.write(f'{spaces}  <input type="text" id="{the_legend}" name="{the_legend}" value="no"><br>\n')

        elif the_legend.startswith("Date and Time"):
            f.write('<script>\n')
            f.write(f'{spaces}  document.write(\'<input type="text" id="{the_legend}" name="{the_legend}" value="\');\n')
            f.write(f'{spaces}  document.write(Time_Now());\n')
            f.write(f'{spaces}  document.write(\'"><br>\\n\');')
            f.write('</script>\n')
            # now = datetime.datetime.now()
            # tnow = now.strftime("%Y-%m-%d %H:%M:%S")
            # f.write(f'{spaces}  <input type="text" id="{the_legend}" name="{the_legend}" value="{tnow}"><br>\n')

        else:
            f.write(f'{spaces}  <input type="text" id="{the_legend}" name="{the_legend}" value="{PROMPT_1}"><br>\n')
#}}}

def fieldget(f, hierarchy, level):
    global COUNTER

    if not hasattr(f, 'seek'):
         raise TypeError('fieldset: first argument is not a file')
    try:
        (the_legend, the_list) = hierarchy
    except:
        raise TypeError('fieldset: second argument is not a couple')
    if not isinstance(level, (int, float)) :
        raise TypeError('fieldset: third argument is not a number')

    # (the_legend, the_list) = hierarchy
    if level < 4:
        for elem in the_list:
            fieldget(f, elem, level + 1)
    else:
        f.write('try:\n')
        # f.write(f'    domain.append("{the_legend}"\n')
        f.write(f'    entry2id["{the_legend}"] = {COUNTER}\n')
        f.write(f'    entries.append( form.getvalue("{the_legend}") )\n')
        f.write(f'    keys.append( "{the_legend}" )\n')
        f.write('except:\n')
        f.write(f'    print("error accessing entry2id[{the_legend}]")\n')
        COUNTER += 1
#}}}
#{{{ adjustNames
# Input:   A list of excel field names;
# Process: Converts the list entries into identifiers
#          compatible with SQL or similar languages;
# Output:  List of converted entries.
def adjustNames(name_list):
    if not isinstance(name_list, list):
         raise TypeError('adjustNames: argument is not a list')

    l = []
    p = re.compile(r"([A-Za-z])([A-Za-z]*)(\-|\ )([A-Za-z])([A-Za-z]*)")
    for name in name_list:
        res = p.match(name)
        if res:
            res1 = res.group(1).upper()
            res2 = res.group(2)
            res4 = res.group(4).upper()
            res5 = res.group(5)
            res1 = res1.encode('ascii', 'ignore').decode('ascii')
            res2 = res2.encode('ascii', 'ignore').decode('ascii')
            res4 = res4.encode('ascii', 'ignore').decode('ascii')
            res5 = res5.encode('ascii', 'ignore').decode('ascii')
            l.append(res1 + res2 + res4 + res5)
        else:
            l.append(name.encode('ascii', 'ignore').decode('ascii'))
    return l
def string2identifier(varStr):
    s = re.sub('\W|^(?=\d)','_', varStr)
    if s[-1] == '_':
        s = s[:-1]
    return s
def adjustNamesII(name_list):
    if not isinstance(name_list, list):
         raise TypeError('adjustNamesII: argument is not a list')
    return [ string2identifier(name) for name in name_list ]
#}}}
#{{{ getSurnameFromName
def getSurnameFromName(f):
    if not hasattr(f, 'seek'):
         raise TypeError('getSurnameFromName: first argument is not a file')

    f.write("  function getSurnameFromName(name) {\n")
    f.write("      switch(name) {\n")
    f.write("         case 'vdflorio':\n")
    f.write("            return 'Vincenzo';\n")
    f.write("         case 'jbelmans':\n")
    f.write("            return 'Jorik';\n")
    f.write("         case 'dvandepl':\n")
    f.write("            return 'Dirk';\n")
    f.write("         case 'fdavin':\n")
    f.write("            return 'Fran&ccedil;ois';\n")
    f.write("         case 'wdcock':\n")
    f.write("            return 'Wouter';\n")
    f.write("         case 'agatera':\n")
    f.write("            return 'Ang&eacute;lique';\n")
    f.write("         case 'mgxavier':\n")
    f.write("            return 'M&aacute;rio';\n")
    f.write("         case 'lparez':\n")
    f.write("            return 'Laurent';\n")
    f.write("         case 'fpompon':\n")
    f.write("            return 'Franck';\n")
    f.write("         case 'aponton':\n")
    f.write("            return 'Aur&eacute;lien';\n")
    f.write("         case 'sboussa':\n")
    f.write("            return 'Sofiane';\n")
    f.write("         case 'pdfaille':\n")
    f.write("            return 'Philippe';\n")
    f.write("         case 'fdoucet':\n")
    f.write("            return 'Fr&eacute;d&eacute;ric';\n")
    f.write("         case 'mpitruzz':\n")
    f.write("            return 'Marco';\n")
    f.write("         default:\n")
    f.write("            return name;\n")
    f.write("      }\n")
    f.write("  }\n")
#}}}
#{{{ Functions to deal with the case of a duplicate entry in the excel
def atRow(ws, candidate, check_range=None):
    matching_row_nbr = None

    if not check_range:
        check_range = list(range(1, ws.max_column + 1))

    for rowNum in range(2, ws.max_row + 1 ):
        found = True
        for colNum in check_range:
            candidateNum = colNum - 1
            cell = ws.cell(row=rowNum,column=colNum).value
            cand = candidate[candidateNum]
            htmlcell = html.escape(cell).encode('ascii', 'xmlcharrefreplace').decode()
            htmlcand = html.escape(cand).encode('ascii', 'xmlcharrefreplace').decode()
            if cell != cand:
                found = False
            # print(f"<h2>Error: row={rowNum}, col={colNum}, maxrow={ws.max_row}, maxcol={ws.max_column}</h2>")
        if found:
            matching_row_nbr = rowNum
            break
    return matching_row_nbr

def replaceRowIfExists(wb, ws, filename, candidate, check_range):
    matching_row_nbr = atRow(ws, candidate, check_range)

    if matching_row_nbr:
        for colNum in range(1, ws.max_column + 1):
            candidateNum = colNum - 2
            try:
                ws.cell(row=matching_row_nbr, column=colNum).value = candidate[candidateNum]
            except:
                return False
        wb.save(filename=filename)

    return matching_row_nbr
#}}}
#{{{ consistency checks
consistency_failure = [
        "None",                                 # 0,
        "Report ID is not set",                 # 1,
        "Cannot access expected fields",        # 2
        "Unexpected number of set components",  # 3
        "Unexpected number of project phases",  # 4
        ]

def consistencyCheck(entries, keys, entry2id):
    global PROMPT_1

    key = entry2id['Report ID:']
    if entries[key] == PROMPT_1:
        return consistency_failure[1]

    try:
        key1 = entry2id['Sub-Section:']
        key2 = entry2id['Device instance:']
        key3 = entry2id['Component:']
        key4 = entry2id['Sub-Component']
    except:
        return consistency_failure[2]

    # the_four_entries = [ entries[key1], entries[key2], entries[key3], entries[key4] ]
    # if the_four_entries.count('null') != 3:
    #     return consistency_failure[3]

    try:
        key1 = entry2id['MINERVA 100MeV (Phase1)']
        key2 = entry2id['Minerva 600MeV (Phase 2)']
        key3 = entry2id['MYRRHA+REACTOR (Phase 3)']
    except:
        return consistency_failure[2]

    the_three_entries = [ entries[key1], entries[key2], entries[key3] ]
    if the_three_entries.count('no') != 2 or the_three_entries.count('yes') != 1:
        return consistency_failure[4]

    try:
        key1 = entry2id['MINERVA 100MeV (Phase1) (1)']
        key2 = entry2id['Minerva 600MeV (Phase 2) (1)']
        key3 = entry2id['MYRRHA+REACTOR (Phase 3) (1)']
    except:
        return consistency_failure[2]

    the_three_entries = [ entries[key1], entries[key2], entries[key3] ]
    if the_three_entries.count('no') != 2 or the_three_entries.count('yes') != 1:
        return consistency_failure[4]

    return consistency_failure[0]
#}}}
#{{{ create Excel
def createExcel(f, space):
    if not hasattr(f, 'seek'):
        raise TypeError('createExcel: argument f is not a file')
    if not isinstance(space, str):
        raise TypeError('createExcel: argument space is not a string')
    if not space.isspace():
        raise ValueError('createExcel: argument space is not whitespace')

    f.write(f'{space}# create excel\n')
    f.write(f'{space}wb_out = openpyxl.Workbook()\n')
    f.write(f'{space}ws_out = wb_out.active\n')
    f.write(f'{space}# create first row\n')
    # f.write(f'{space}for i in range(num_entries):\n')
    # f.write(f'{space}    k = keys[i]\n')
    # f.write(f'{space}    try:\n')
    # f.write(f'{space}        c = ws_out.cell(row=1, column=i+1)\n')
    # f.write(f'{space}        c.value = k\n')
    # f.write(f'{space}    except:\n')
    # f.write(f"{space}        print(f'<h1><b>Error:</b> Could not write entry {i} ({k}) into database {EXCEL_DB}</h1>')\n") 
    f.write(f'{space}ws_out.append(keys)\n')
    f.write(f'{space}wb_out.save(EXCEL_DB)\n')
#}}}
#{{{ SQL Create Table and Insert Into commands
def createTableCommand(the_list):
    sql = "CREATE TABLE test3 (id serial PRIMARY KEY, "
    for l in the_list[:-1]:
        sql += f'{l} varchar, '
    l = the_list[-1]
    sql += f'{l} varchar);'
    return sql

def createInsertCommand(the_list):
    sql = "INSERT INTO test3 ("
    for l in the_list[:-1]:
        sql += f'{l}, '
    l = the_list[-1]
    sql += f'{l}) VALUES(' + '%s, ' * (len(the_list)-1)
    sql += '%s) RETURNING id;'
    return sql


def createTable(f, dsn, space, the_list):
    if not hasattr(f, 'seek'):
        raise TypeError('createExcel: argument f is not a file')
    if not isinstance(space, str):
        raise TypeError('createExcel: argument space is not a string')
    if not space.isspace():
        raise ValueError('createExcel: argument space is not whitespace')

    sql = createTableCommand(the_list)
    f.write(f'{space}# create table\n')
    f.write(f'{space}sql = "{sql}"\n')
    f.write(f'{l} varchar);\n')
    f.write(f'{space}with psycopg2.connect({dsn}) as conn:\n')
    f.write(f'{space}    with conn.cursor() as curs:\n')
    f.write(f'{space}        curs.execute(sql)\n')
#}}}

def help(xlsx_file, xlsx_out):
    l = len(module_name)
    spaces = ' ' * l
    print (f'{module_name} [ -i <input sheet> ] [ -o <output sheet> ] [ -b excel|postgres ] [ -h ]')
    print (f'{spaces} Default values:')
    print (f'{module_name} -i {xlsx_file} -o {xlsx_out} -b excel')

# {{{ Main
def main(argv):
    global COUNTER

    xlsx_file = 'Fault_Tracking.xlsx'
    xlsx_out  = 'Fault_Tracking_2.xlsx'
    backend_is_Excel = True

    try:
        opts, args = getopt.getopt(argv,"hi:o:b:",["ifile=","ofile=","backend="])
    except getopt.GetoptError:
        help(xlsx_file, xlsx_out)
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            help(xlsx_file, xlsx_out)
            sys.exit()
        elif opt in ("-i", "--ifile"):
            xlsx_file = arg
        elif opt in ("-o", "--ofile"):
            xlsx_out = arg
        elif opt in ("-b", "--backend"):
            argument = arg.lower()
            if   argument == 'excel':
                backend_is_Excel = True
            elif argument == 'postgres' or argument == 'sql':
                backend_is_Excel = False
            else:
                help(xlsx_file, xlsx_out)
                sys.exit(2)

    if len(argv) != 2:
        xlsx_file = Path('.', 'Fault_Tracking.xlsx')
    else:
        xlsx_file = Path('.', argv[1])

    print(colored("...Processing sheet '{}'".format(xlsx_file),'green'))

    wb_obj = openpyxl.load_workbook(xlsx_file) 

    # Tune on the second sheet
    sheet = wb_obj['legend']

    #{{{ Save 'system categories'
    system_categories = [ sheet['C2'].value, sheet['D2'].value, sheet['E2'].value, sheet['F2'].value ]
    try:
        system_categories = adjustNames(system_categories)
    except TypeError:
        print(colored('adjustNames has been called with an erroneous parameter -- bailing out...', 'red'), file=sys.stderr)
        sys.exit(1)
    with open('system_categories.py', 'w') as f:
        f.write('system_categories = [')
        for category in system_categories[:-1]:
            f.write(f'"{category}", ')
        category = system_categories[-1]
        f.write(f'"{category}" ]\n')
    #}}}

    #{{{ excel 2 excel
    rows=sheet.max_row
    cols=sheet.max_column

    wb_out = openpyxl.Workbook()
    # Read the active sheet:
    ws_out = wb_out.active


    for i, row in enumerate(sheet.iter_rows(min_row=3)):
        # print('row ', i, ' is ', row)
        cella = f'A{i+2}'
        cellc = f'C{i+2}'   # was f'C{i+2}'
        cellk = f'K{i+2}'
        if   row[2].value:
            ws_out[cella] = 1
            if row[1].value:
                ws_out[cellc] = row[1].value
            else:
                ws_out[cellc] = row[2].value
        elif row[3].value:
            ws_out[cella] = 2
            if row[1].value:
                ws_out[cellc] = row[1].value
            else:
                ws_out[cellc] = row[3].value
        elif row[4].value:
            ws_out[cella] = 3
            if row[1].value:
                ws_out[cellc] = row[1].value
            else:
                ws_out[cellc] = row[4].value
        elif row[5].value:
            ws_out[cella] = 4
            if row[1].value:
                ws_out[cellc] = row[1].value
            else:
                ws_out[cellc] = row[5].value
        ws_out[cellk] = ' '

    wb_out.save("Fault_Tracking_2.xlsx")

    # print(colored("...done (output=Fault_Tracking_2.xlsx).",'green'))
    print(colored("...done: knowledge succesfully extracted.",'green'))
    #}}}

    #{{{ Populate the hierarchy list
    # Read the active sheet:
    sheet = wb_obj['data']
    rows=sheet.max_row
    cols=sheet.max_column

    hierarchy = ( '', []  )
    seen_already = {}

    for c in range(cols):
        one = sheet[1][c].value
        two = sheet[2][c].value
        thr = sheet[3][c].value
        fou = sheet[4][c].value

        if one and thr:
            two = 'General'

        if thr in seen_already.keys():
            thr = thr + ' (1)'
        else:
            seen_already[thr] = True

        if one:
            li = hierarchy[1]
            li.append((one, []))
        if two:
            li = hierarchy[1]
            tu = li[-1]
            va2, li2 = tu
            li2.append((two, []))
        if thr:
            li = hierarchy[1]
            tu = li[-1]
            va2, li2 = tu
            tu3 = li2[-1]
            va3, li3 = tu3
            li3.append((thr, fou))
    #}}}

    html_form = 'form.html'
    css = 'ft.css'

    #{{{ Create the HTML Form
    with open(html_form, 'w') as f:
        f.write('<!DOCTYPE html>\n')
        f.write('<html>\n')
        f.write('<head>\n')
        f.write('\n')
        f.write(' <script>\n')

        try:
            getSurnameFromName(f)
        except TypeError:
            print(colored('getSurnameFromName has been called with an erroneous parameter -- bailing out...', 'red'), file=sys.stderr)
            sys.exit(1)

        f.write('\n')
        f.write("  function getParameterByName(name, url = window.location.href) {\n")
        f.write("     name = name.replace(/[\[\]]/g, '\\$&');\n")
        f.write("     var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),\n")
        f.write("     results = regex.exec(url);\n")
        f.write("     if (!results) return null;\n")
        f.write("     if (!results[2]) return '';\n")
        f.write("     return decodeURIComponent(results[2].replace(/\+/g, ' '));\n")
        f.write("  }\n")
        f.write('\n')
        f.write('  window.addEventListener("load", () => {\n')
        f.write(f'    const collapser = document.getElementById("goback");\n')
        f.write('\n')
        f.write('     function goback(event) {\n')
        f.write('          if (event) {\n')
        f.write('             event.stopPropagation();\n')
        f.write('          }\n')
        f.write('\n')
        f.write(f'          window.location = "{SERVICE}/diagram.svg";\n')
        f.write('     }\n')
        f.write('\n')
        f.write('     collapser.addEventListener("click", goback);\n')
        f.write('  });\n')
        for i in range(4):
            f.write('  window.addEventListener("load", () => {\n')
            f.write(f'    const collapser = document.getElementById("collapser{i}");\n')
            f.write(f'    const collapsable = document.getElementById("collapsable{i}");\n')
            f.write('\n')
            f.write('     function collapse(event) {\n')
            f.write('          if (event) {\n')
            f.write('             event.stopPropagation();\n')
            f.write('          }\n')
            f.write('\n')
            f.write('          collapsable.hidden = !collapsable.hidden;\n')
            f.write('     }\n')
            f.write('\n')
            f.write('     collapser.addEventListener("click", collapse);\n')
            f.write('  });\n')
        f.write('\n\n')
        f.write('    function Time_Now() {\n')
        f.write('        var oDate  = new Date();\n')
        f.write('        var nHrs   = ("0"+oDate.getHours()).substr(-2);\n')
        f.write('        var nMin   = ("0"+oDate.getMinutes()).substr(-2);\n')
        f.write('        var nSec   = ("0"+oDate.getSeconds()).substr(-2);\n')
        f.write('        var nDay   = ("0"+oDate.getDate()).substr(-2);\n')
        f.write('        var sMonth = oDate.getMonth() + 1;\n')
        # f.write('        var nMonth = ("0"+oDate.getMonth()).substr(-2);\n')
        f.write('        var nMonth = ("0"+sMonth).substr(-2);\n')
        f.write('        var nYear  = oDate.getFullYear();\n')
        f.write("        return nYear + '-' + nMonth + '-' + nDay  + ' ' + nHrs + ':' + nMin + ':' + nSec;\n")
        f.write('    }\n')
        f.write(' </script>\n')
        f.write(f' <link rel="stylesheet" href="{css}">\n')
        f.write('\n')
        f.write('<title>\n')
        f.write(f'  Fault track service front-end form -- version {version} ({version_date})\n')
        f.write('</title>\n')
        f.write('</head>\n')
        f.write('\n')

        f.write('<body>\n')
        f.write('<pre>\n')
        # f.write('\n')
        f.write('</pre>\n')
        f.write('<div class="tooltip4">\n')
        f.write('<h2 id="goback" onclick="goback()">MINERVA fault tracking system</h2>\n')
        f.write('<span class="tooltiptext4">Click to go back to system diagram</span>\n')
        f.write('</div>\n')
        f.write('\n')
        f.write('<p>Please insert the following data, ')
        f.write(f'<script>\n')
        f.write(f'document.write(getSurnameFromName(getParameterByName(\'id\')),\'</p>\');\n')
        f.write(f'</script>\n')
        f.write('\n')
        f.write(f'<form action="{SERVICE}/form.cgi" method="post">\n')

        f.write('<fieldset>\n')

        #-----------------------#
        try:
            fieldset(f, hierarchy, 1)
        except TypeError:
            print(colored('fieldset has been called with erroneous parameters -- bailing out...', 'red'), file=sys.stderr)
            sys.exit(1)

        #-----------------------#

        f.write('<p></p><br/>\n')
        f.write('<input type="submit" value="Submit">\n')
        f.write('</fieldset>\n')
        f.write(f'<script>\n')
        f.write(f'document.write(\'<input type="hidden" name="user_id" value="\', getParameterByName(\'id\'), \'">\');\n')
        f.write(f'</script>\n')
        f.write('</form>\n')

    command = subprocess.call(['scp', html_form, SSH_ADDR ])
    if command != 0:
        print(f"return code is {command}", file=sys.stderr)
    else:
        print(colored(f'HTML form transfer:   completed ({SERVICE}/{html_form})', 'green'), file=sys.stderr)
    #}}}

    #{{{ Create the CSS style
    with open(css, 'w') as f:
        f.write(f' // Fault track CSS -- version {version} ({version_date})\n')
        f.write('body {\n')
        f.write('      background-color: lightblue;\n')
        f.write('    }\n')
        f.write('\n')
        f.write('    h1 {\n')
        f.write('      color: navy;\n')
        f.write('      margin-left: 20px;\n')
        f.write('    }\n')
        f.write('\n')
        f.write('    h2 {\n')
        f.write('      color: navy;\n')
        f.write('      margin-left: 16px;\n')
        f.write('      text-shadow: 2px 2px 5px red;\n')
        f.write('    }\n')
        f.write('    h3 {\n')
        f.write('      color: blue;\n')
        f.write('      margin-left: 20px;\n')
        f.write('      text-shadow: 2px 2px 5px yellow;\n')
        f.write('    }\n')
        f.write('\n')
        f.write('    h4 {\n')
        f.write('      color: black;\n')
        f.write('      margin-left: 20px;\n')
        f.write('      text-shadow: 2px 2px 5px yellow;\n')
        f.write('    }\n')
        f.write('\n')
        f.write('\n')
        # f.write('    p, legend {\n')
        f.write('    p {\n')
        f.write('      color: crimson;\n')
        f.write('      margin-left: 22px;\n')
        f.write('      font-size: 22px;\n')
        f.write('      text-shadow: 2px 2px 5px green;\n')
        f.write('    }\n')
        f.write('\n')
        f.write('    label {\n')
        f.write('      color: green;\n')
        f.write('      margin-left: 26px;\n')
        f.write('      text-shadow: 2px 2px 5px green;\n')
        f.write('    }\n')
        f.write('\n')
        f.write('    fieldset {\n')
        # f.write('      color: navy;\n')
        f.write('      border-style: solid;\n')
        f.write('      border-width: 4px;\n')
        f.write('      border-color: blue;\n')
        f.write('      background-color: lightgrey;\n')
        f.write('      border-radius: 5px;\n')
        f.write('      padding-top: 10px;;\n')
        f.write('    }\n')

        f.write('    legend {\n')
        f.write('      width: 85\%;\n')
        f.write('      border: 1px solid black;\n')
        f.write('      padding: 3px 6px;\n')
        f.write('      cursor: pointer;\n')
        f.write('      display: inline-block;\n')
        f.write('      color: crimson;\n')
        f.write('      margin-left: 6px;\n')
        f.write('      text-shadow: 2px 2px 5px green;\n')
        f.write('    }\n')
        f.write('\n')
        f.write('    legend::after {\n')
        f.write('      content: "▼";\n')
        f.write('      float: right;\n')
        f.write('    }\n')
        f.write('\n')

        f.write('#mainparent #mainpopup {\n')
        f.write('  display: none;\n')
        f.write('}\n')
        f.write('\n')

        f.write('#mainparent:hover #mainpopup {\n')
        f.write('  display: block;\n')
        f.write('}\n')

        # for j in range(4):
        #     f.write(f'#mainparent{j} #mainpopup{j} {{\n')
        #     f.write('  display: none;\n')
        #     f.write('}\n')
        #     f.write('\n')

        #     f.write(f'#mainparent{j}:hover #mainpopup{j} {{\n')
        #     f.write('  display: block;\n')
        #     f.write('}\n')

        for j in range(COUNTER+1):
            f.write('/* Tooltip container */\n')
            f.write(f'.tooltip{j} {{\n')
            f.write('  position: relative;\n')
            f.write('  display: inline-block;\n')
            f.write('  border-bottom: 1px dotted black; /* If you want dots under the hoverable text */\n')
            f.write('}\n')
            f.write('\n')
            f.write('/* Tooltip text */\n')
            f.write(f'.tooltip{j} .tooltiptext{j} {{\n')
            f.write('  visibility: hidden;\n')
            f.write('  width: 260px;\n') # 120px
            f.write('  background-color: #555;\n')
            f.write('  color: #fff;\n')
            f.write('  text-align: center;\n')
            f.write('  padding: 5px 0;\n')
            f.write('  border-radius: 6px;\n')
            f.write('\n')
            f.write('  /* Position the tooltip text */\n')
            f.write('  position: absolute;\n')
            f.write('  z-index: 1;\n')
            f.write('  bottom: 75%;\n') # 125%
            f.write('  left: 50%;\n')
            f.write('  margin-left: -60px;\n')
            f.write('\n')
            f.write('  /* Fade in tooltip */\n')
            f.write('  opacity: 0;\n')
            f.write('  transition: opacity 0.3s;\n')
            f.write('}\n')
            f.write('\n')
            f.write('/* Tooltip arrow */\n')
            f.write(f'.tooltip{j} .tooltiptext{j}::after {{\n')
            f.write('  content: "";\n')
            f.write('  position: absolute;\n')
            f.write('  top: 100%;\n')
            f.write('  left: 50%;\n')
            f.write('  margin-left: -5px;\n')
            f.write('  border-width: 5px;\n')
            f.write('  border-style: solid;\n')
            f.write('  border-color: #555 transparent transparent transparent;\n')
            f.write('}\n')
            f.write('\n')
            f.write('/* Show the tooltip text when you mouse over the tooltip container */\n')
            f.write(f'.tooltip{j}:hover .tooltiptext{j} {{\n')
            f.write('  visibility: visible;\n')
            f.write('  opacity: 1;\n')
            f.write('} \n')

        f.write('\n')

    command = subprocess.call(['scp', css, SSH_ADDR])
    if command != 0:
        print(f"return code is {command}", file=sys.stderr)
    else:
        print(colored(f'CSS file transfer:    completed ({SERVICE}/{css})', 'green'), file=sys.stderr)

    #}}}

    COUNTER = 0     # reset COUNTER

    #{{{ Create the CGI Script
    cgi_script = 'form.cgi'

    with open(cgi_script, 'w') as f:
        f.write('#!/usr/bin/python3\n')
        f.write('\n')
        f.write('# Server-side of the fault tracking service\n')
        f.write(f'# Written by vdflorio via {module_name}\n')
        f.write(f'# V. {version} ({version_date})\n')
        f.write('\n')
        f.write('# Import modules for CGI handling\n')
        f.write('import cgi, cgitb\n')
        f.write('import openpyxl\n')
        f.write('import os, sys\n')
        f.write('import os.path\n')
        f.write('import html\n')
        f.write('import psycopg2\n')
        f.write(f'import {module_fname} as x2x\n')
        f.write('\n')
        f.write(f'cgitb.enable()\n')
        f.write(f'EXCEL_DB = "{EXCEL_DB}"\n')
        f.write(f'DSN = "{DSN}"\n')
        f.write('\n')
        f.write('# Create instance of FieldStorage\n')
        f.write('form = cgi.FieldStorage()\n')
        f.write('\n')
        f.write('# Get data from fields\n')
        f.write('entry2id   = {}\n')
        f.write('entries    = []\n')
        f.write('keys       = []\n')
        f.write('\n')

        try:
            fieldget(f, hierarchy, 1)
        except TypeError:
            print(colored('fieldget has been called with erroneous parameters -- bailing out...', 'red'), file=sys.stderr)
            sys.exit(1)

        f.write('\n')

        f.write('num_entries = len(entry2id.keys())\n')
        f.write('\n')
        f.write(r'print("Content-type:text/html\r\n\r\n")')
        f.write('\n')
        f.write('print("<html>")\n')
        f.write('print("<head>")\n')
        f.write('print("<title>Server side - Stores a record in the fault tracking DB</title>")\n')
        f.write(f'print("<link rel=\'stylesheet\' href=\'{css}\'>")\n')

        f.write('print(\'<script>\')\n')
        f.write('print(\'   function Time_Now() {\')\n')
        f.write('print(\'       var oDate  = new Date();\')\n')
        f.write('print(\'       var nHrs   = ("0"+oDate.getHours()).substr(-2);\')\n')
        f.write('print(\'       var nMin   = ("0"+oDate.getMinutes()).substr(-2);\')\n')
        f.write('print(\'       var nSec   = ("0"+oDate.getSeconds()).substr(-2);\')\n')
        f.write('print(\'       var nDay   = ("0"+oDate.getDate()).substr(-2);\')\n')
        f.write('print(\'       var sMonth = oDate.getMonth() + 1;\')\n')
        f.write('print(\'       var nMonth = ("0"+sMonth).substr(-2);\')\n')
        f.write('print(\'       var nYear  = oDate.getFullYear();\')\n')
        f.write("print(\"       return nYear + '-' + nMonth + '-' + nDay  + ' ' + nHrs + ':' + nMin + ':' + nSec;\")\n")
        f.write('print(\'   }\')\n')
        f.write('print(\'</script>\')\n')

        f.write('print("</head>")\n')
        f.write('print("<body>")\n\n')
        f.write('print("<h2>Fault tracking service: server side.</h2>")\n')
        f.write('print("<h3>The following data shall be inserted in the fault tracking DB:</h3>")\n')
        f.write('print(" <fieldset>")\n')
        f.write('print("  <ol>")\n')
        f.write('for key in form.keys():\n')
        f.write('    value = form[key].value\n')
        f.write('    if value != "null" and value != "{PROMPT_1}" and type(value) != list:\n')
        f.write(f"        # value = value.encode('ascii', 'ignore').decode('ascii')\n")
        f.write(f"        value = html.escape(value).encode('ascii', 'xmlcharrefreplace').decode()\n")
        f.write(f'        print(f\'    <li>Field "<b>{{key}}</b>" is equal to "<b>{{value}}</b>"</li></br/>\\n\')\n')
        f.write('print("  </ol>")\n')
        f.write('print(" </fieldset>")\n')
        f.write('\n')

        f.write('names = x2x.adjustNamesII(keys)\n')
        f.write('sql   = x2x.createTableCommand(names)\n')
        f.write('with open("sql.creat.py", "w") as p:\n')

        f.write('    print("import psycopg2",  file=p)\n')
        f.write('    print(f"sql = \'{sql}\'", file=p)\n')
        f.write(f'    print("dsn = \'{DSN}\'", file=p)\n')
        f.write('    print("with psycopg2.connect(dsn) as conn:", file=p)\n')
        f.write('    print("    with conn.cursor() as curs:", file=p)\n')
        f.write('    print("        curs.execute(sql)", file=p)\n')

        f.write('\n')

        f.write('if not os.path.isfile(EXCEL_DB):\n')
        f.write('    try:\n')
        #SQL# f.write('        # create SQL table\n')
        #SQL# f.write(f"        print('<h2><b>SQL:</b> CREATE TABLE {SQL_TABLE} (')\n")
        #SQL# f.write('        for i in range(num_entries):\n')
        #SQL# f.write('            key   = keys[i]\n')
        #SQL# f.write('            # field = adjustNames(keys[i])\n')
        #SQL# f.write('            value = entries[i]\n')
        #SQL# f.write("            print(f'{{field}} varchar(255),')\n")
        #SQL# f.write('        print("); </h2>")\n')
        #

        try:
            createExcel(f=f, space='        ')
        except:
            sys.stderr.write('createExcel failed -- bailing out...\n')
            sys.exit(1)

        #  f.write('        # create excel\n')
        #  f.write('        wb_out = openpyxl.Workbook()\n')
        #  f.write('        ws_out = wb_out.active\n')
        #  f.write('        # create first row\n')
        #  f.write('        ws_out.append(keys)\n')
        #  f.write('        wb_out.save(EXCEL_DB)\n')
        f.write('    except:\n')
        f.write("        print(f'<h1><b>Error:</b> Could not initialize database {EXCEL_DB}</h1>')\n")
        f.write('cc = x2x.consistencyCheck(entries, keys, entry2id)\n')
        f.write('if cc != "None":\n')
        f.write("    print(f'<h1><b>Error:</b> Consistency check failed: {cc}. The record shall be ignored.</h1>')\n")
        f.write('else:\n')
        # The following commands will be substituted with SQL statements to add a record
        # to a fault tracking table in a Postgres database
        # After filling in the `le` list, one needs to update table `SQL_TABLE` with a
        # command like
        # INSERT INTO {SQL_TABLE} VALUES ( {le[0]}, {le[1]}, ..., {le[-1]} );
        # A promising candidate for interfacing Postgres in Python is
        # Psycopg – PostgreSQL database adapter for Python
        # https://www.psycopg.org/docs/
        f.write('    failed = False\n')
        f.write('    try:\n')
        f.write('        wb_out = openpyxl.load_workbook(EXCEL_DB)\n')
        f.write('        ws_out = wb_out.active\n')
        f.write('    except:\n')
        f.write("        print(f'<h1><b>Error:</b> Could not load the workbook of database {EXCEL_DB}</h1>')\n")
        f.write("        failed = True\n")
        f.write('\n')

        f.write('    try:\n')
        f.write('        le = []\n')
        # f.write('        for key in form.keys():\n')
        f.write('        for i in range(num_entries):\n')
        f.write('            key   = keys[i]\n')
        f.write('            value = entries[i]\n')
        f.write('            if type(value) == list:\n')
        f.write('                le.append(value[0])\n')
        f.write('            elif value == "{PROMPT_1}":\n')
        f.write('                le.append("null")\n')
        f.write('            else:\n')
        f.write('                le.append(value)\n')
        f.write('    except:\n')
        f.write("        print(f'<h1><b>Error:</b> Could not prepare the new database entry</h1>')\n")
        f.write("        failed = True\n")
        f.write('\n')

        f.write('    recNum = None\n')
        f.write('    # try:\n')
        f.write('    #     recNum = x2x.atRow(ws=ws_out, candidate=le)\n')
        f.write('    # except:\n')
        f.write("    #     print(f'<h1><b>Error:</b> atRow() method failed while accessing database {EXCEL_DB}</h1>')\n")

        f.write('    if recNum != None:\n')
        f.write("        print(f'<h2><b>Warning:</b> Record already present in database {EXCEL_DB} => Record ignored.</h2>')\n")
        f.write('    else:\n')

        f.write('        try:\n')
        f.write('            ws_out.append(le)\n')
        f.write('        except:\n')
        f.write("            print(f'<h1><b>Error:</b> Could not append new record onto database {EXCEL_DB}</h1>')\n")
        f.write("            failed = True\n")
        f.write('\n')
        f.write('        names = x2x.adjustNamesII(keys)\n')
        f.write('        sql   = x2x.createInsertCommand(names)\n')
        f.write('        letup = tuple(le)\n')

        f.write('        try:\n')
        f.write(f'            with psycopg2.connect("{DSN}") as conn:\n')
        f.write('                with conn.cursor() as curs:\n')
        f.write('                    curs.execute(sql, letup)\n')
        f.write("            print(f'<h1>Record correctly added to the SQL table.</h1>')\n")
        f.write('        except:\n')
        f.write("            print(f'<h1><b>Error:</b> Could not append new record onto SQL table.</h1>')\n")
        f.write("            failed = True\n")

        f.write('        with open("sql.insert.py", "w") as p:\n')

        f.write('            print("import psycopg2",  file=p)\n')
        f.write('            print(f"sql = \'{sql}\'", file=p)\n')
        f.write(f'            print("dsn = \'{DSN}\'", file=p)\n')
        f.write('            print(f"letup = {letup}", file=p)\n')
        f.write('            print("with psycopg2.connect(dsn) as conn:", file=p)\n')
        f.write('            print("    with conn.cursor() as curs:", file=p)\n')
        f.write('            print(f"        curs.execute(sql, {letup})", file=p)\n')


        f.write('        try:\n')
        f.write('            wb_out.save(EXCEL_DB)\n')
        f.write('        except:\n')
        f.write("            print(f'<h1><b>Error:</b> Could not add new record to database {EXCEL_DB}</h1>')\n")
        f.write("            failed = True\n")
        f.write('\n')

        f.write("    if failed:\n")
        f.write('        lerr = []\n')
        f.write('        for i in range(num_entries):\n')
        f.write('            lerr.append( html.escape(entries[i]).encode("ascii", "xmlcharrefreplace").decode() )\n')
        f.write("        print(f'\\n<p>{lerr}</p>\\n')\n")
        f.write('\n')

        f.write('print("<p><b>Note:</b>A machine would generate this same entry in the fault tracking database by issuing the following command:</p>")\n')
        f.write('print("<p><tt>curl -X POST ",end="")\n')
        f.write('\n')
        f.write('for key in form.keys():\n')
        f.write('    value = form[key].value\n')
        f.write('    if value != "null" and value != "{PROMPT_1}" and type(value) != list:\n')
        f.write(f"        value = html.escape(value).encode('ascii', 'xmlcharrefreplace').decode()\n")
        f.write(f'        print(f\'-F "{{key}}={{value}}" \', end="")\n')
        f.write(f'print("{SERVICE}/{cgi_script}</tt></p>")\n')

        f.write('print("<p>(of course, <tt>user_id</tt> would in that case be the machine-id)</p>")\n')

        f.write(f'print("<div>[This is <tt>form.cgi</tt>, created by <tt>excel2excel2</tt> v.{version} ({version_date}), invoked on ")\n')
        f.write('print("<script>")\n')
        f.write('print("document.write(Time_Now()+\'.]\');")\n')
        f.write('print("</script>")\n')
        f.write('print("</div>")\n')

        f.write('print("</body>")\n')
        f.write('print("</html>")\n')

    command = subprocess.call(['scp', cgi_script, SSH_ADDR])
    if command != 0:
        print(f"return code is {command}", file=sys.stderr)
    else:
        print(colored(f'CGI script transfer:  completed ({SERVICE}/{cgi_script})', 'green'), file=sys.stderr)
        command = subprocess.call(['ssh', SSH_CAR, 'chmod', '+x', f'{SSH_CDR}/{cgi_script}'])
    #}}}

    # remote copies the module, to allow for the server side to access its functions
    command = subprocess.call(['scp', module_name, SSH_ADDR])
    if command != 0:
        print(f"return code is {command}", file=sys.stderr)
    else:
        print(colored(f'This module transfer: completed ({SERVICE}/{module_name})', 'green'), file=sys.stderr)

    # print( 'Possibly useful commands:')
    # print(f'\tscp fault-tracking.cgi {SSH_ADDR}')
    # print(f'\tscp {SSH_ADDR}/{EXCEL_DB} .')
    # print(f'\tlibreoffice {EXCEL_DB}')

#}}}

if __name__ == '__main__':
    main(sys.argv[1:])

#
# EoF(excel2excel2)
#
