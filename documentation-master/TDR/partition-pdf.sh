INCLUDE=../include
LIB=../lib
GCCOPTS="-lfl -lvdfio"

printf '\033[45;1mPhase 1/4: Generate data structures\033[0m\n'
make
./sectionpages < MINERVA-100MeV-ACCELERATOR-TDR.toc > s-pages.h
gcc -I${INCLUDE} -L${LIB} -o s-pages  s-pages.c $GCCOPTS
printf '\033[45;1mPhase 2/4: Construct partitioning script\033[0m\n'
./s-pages MINERVA-100MeV-ACCELERATOR-TDR.pdf
printf '\033[45;1mPhase 3/4: Execute partitioning script\033[0m\n'
./s-pages.sh
printf '\033[45;1mPhase 4/4: Schedule for upload onto Alexandria\033[0m\n'
cp chapters/* ~/c/Users/vdflorio/AppData/Roaming/OpenText/OTEdit/EC_sckcen/c40793049/
printf '\033[32mNo errors: processing concluded normally.\033[0m\n'
