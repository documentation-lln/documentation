%{
#define ROWS 10
#define COLS 10
char *frac[ROWS+12][COLS+1] = 
		       {// 0	 1	2    3	   4	5     6     7     8     9     10
			{ NULL, NULL, NULL, "↉",  NULL, NULL, NULL, NULL, NULL, NULL, NULL }, // 0/
			{ NULL, NULL, "½",  "⅓",  "¼",  "⅕",  "⅙",  "⅐",   "⅛",  "⅑", "⅒ " }, // 1/
			{ NULL, NULL, "⅔",  NULL, NULL, "⅖",  NULL, NULL, NULL, NULL, NULL }, // 2/
			{ NULL, NULL, NULL, "⅔",  NULL, NULL, NULL, NULL, NULL, NULL, NULL }, // 3/
			{ NULL, NULL, NULL, NULL, NULL, "⅘",  NULL, NULL, NULL, NULL, NULL }, // 4/
			{ NULL, NULL, NULL, NULL, NULL, NULL, "⅚",  NULL, "⅝",  NULL, NULL }, // 5/
			{ NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL }, // 6/
			{ NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, "⅞",  NULL, NULL }, // 7/
			{ NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL }, // 8/
			{ NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL }, // 9/
			{ NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL }, // 10/
		       };
// Most incredible bug ever: Use [\] instead of \\...

int verbose=0;
// \\times	printf("×");
%}

%%

\\ncCHR[0-9]	{
		if (verbose) printf("\n\t1a) %s\n", yytext);
		if (yytext[6]=='2') printf("I1-ME2:RF-CAV-01");
		else printf("I1-ME3:RF-CAV-01");
		fflush(stdout);
		}

\\ncCHR[{][0-9][}]	{
			if (verbose) printf("\n\t1b) %s\n", yytext);
			if (yytext[7]=='2') printf("I1-ME2:RF-CAV-01");
			else printf("I1-ME3:RF-CAV-01");
			fflush(stdout);
			}

\\ncSPOKEREB[0-9]	{
			if (verbose) printf("\n\t2a) %s\n", yytext);
			printf("LB-ME3:RF-CAV-0%c", yytext[11]);
			fflush(stdout);
			}

\\ncSPOKEREB[{][0-9][}]	{
				if (verbose) printf("\n\t2b) %s\n", yytext);
				printf("LB-ME3:RF-CAV-0%c", yytext[12]);
				fflush(stdout);
				}

\\ncSPOKE[0-9]*	{
			int n = atoi(yytext + 8);
			int quot = n / 2, mod = n % 2;
			if (verbose) printf("\n\t3a) %s\n", yytext);
			if (mod == 0 ) printf("LB-S%02d:RF-CAV-02", quot);
			else           printf("LB-S%02d:RF-CAV-01", quot);
			fflush(stdout);
			}

\\ncSPOKE[{][0-9]*[}]	{
			char *p = strchr(yytext, '}');
			int n;
			int quot, mod;

			if (verbose) printf("\n\t3b) %s\n", yytext);
			*p = '\0';
			n = atoi(yytext + 9);
			quot = n / 2; mod = n % 2;
			if (mod == 0 ) printf("LB-S%02d:RF-CAV-02", quot);
			else           printf("LB-S%02d:RF-CAV-01", quot);
			fflush(stdout);
			}

\\multirow[{][0-9][}][{]([^}])*[}]	;

\\tablefootnote[{]	{
			int opencurly = 1;
			int c;
			while ( (c=input()) != EOF ) {
				if (c=='}') opencurly--;
				if (c=='{') opencurly++;
				if (opencurly == 0) break;
			}
		}

\$\\hbox[{]Ngap[}]\\cdot\\beta\\cdot\\lambda\/2c\$								printf("EQUA(1)");
\$\\text[{]MTBF[}]\_[{]\\tau\ \<\ \\SI[{]0\.1[}][{]\\second[}][}]\$						printf("EQUA(2a)");
\$\\text[{]MTBF[}]\_[{]\\SI[{]0\.1[}][{]\\second[}]\ \\,\ \<\ \\tau\ \<\ \\,\ \\SI[{]3[}][{]\\second[}][}]\$	printf("EQUA(2b)");
\$\\text[{]MTBF[}]\_[{]\\tau\ \>\ \\,\ \\SI[{]3[}][{]\\second[}][}]\$						printf("EQUA(2c)");

\\numrange[{]([^}])*[}]	{
			if (yytext[10]=='d')
				printf("TEN_TO(%s) to ", yytext+11);
			else
				printf("%s to ", yytext+10);
			}
\$>\$			printf(">");
\$\\tau\$		printf("SYM(tau)");
\\tau			printf("SYM(tau)");
\$\\mu\$		printf("µ");
\$\\times\$		printf("×");
\$D_[12]\$		if (yytext[3]=='1') printf("D₁"); else printf("D₂");

\\times		printf("×");
\\midtilde	printf("≈");
\\mA		printf("mA");
\\MeV		printf("MeV");
\\mega\\watt		printf("MW");
\\mega\\hertz		printf("MHz");
\\MHz			printf("MHz");
\\mega\\volt\\per\\meter	printf("MVpermt");
\\kilo\\ohm\\per\\meter	printf("kΩ m⁻¹");
\\mega\\ohm\\per\\meter	printf("MΩ m⁻¹");
\\tesla\\per\\meter		printf("TESLApermt");

\\textbeta	printf("SYM(beta)");
\$\\beta\$	printf("SYM(beta)");
\\beta		printf("SYM(beta)");
\\second 	printf("s");
\\liter\\per\\minute	printf("L min⁻¹");
\\kilo\\gram\\per\\cubic\\meter	printf("kg m⁻³");
\\hertz\\per\\milli\\bar	printf("Hz mbar⁻¹");
\\square\\meter	printf("m²");
\\squared	printf("²");
\\percent	printf("\\%%");
\\hour		printf("h");
\\CodesANSYS	printf("ANSYS");
\\kelvin	printf("K");
\\textpm	printf("±");
\\rpm		printf("±");
\\cubic\\meter	printf("m³");
\\kV		printf("kV");
\\liter		printf("L");
\\kilo		printf("k");
\\CDTFASTEF	printf("CDT/FASTEF");
\\Eacc		printf("SYM(Eacc)");
\\Leff		printf("SYM(Leff)");
\\Keff		printf("SYM(Keff)");
\\nano		printf("n");
\\LinacRFQ	printf("RFQ");
\\sfrac[0-9][0-9]\\si[{]\\inch[}]	{
		int d1=yytext[6]-'0',    d2=yytext[7]-'0';
		if (d1 < 0 || d1 > COLS)
			printf("%c/%c", yytext[6], yytext[7]);
		else
		if (d2 < 0 || d2 > COLS)
			printf("%c/%c", yytext[6], yytext[7]);
		else {
			if (frac[d1][d2]==NULL)
				printf("%c/%c", yytext[6], yytext[7]);
			else
				printf("%s", frac[d1][d2]);
		}
		printf("SYMINCH");
	}

\\sfrac[{][0-9][}][{][0-9][}]\\si[{]\\inch[}]	{
		printf("%c/%c″", yytext[7], yytext[10]);
		//printf("%c/%c", yytext[7], yytext[10]);
		}
\\SIrange[{][0-9]*[}][{][0-9]*[}] {
		char *p = strstr(yytext, "}{");
		char *q;
		q = strchr(p+2, '}');
		*p = *q = '\0';
		printf("%s–%s", yytext+9, p+2);
	}
\\kilo\\hertz\\per\\milli\\meter	printf("kHz mm⁻¹");
\\kilo\\[Nn]ewton\\per\\milli\\meter	printf("kN mm⁻¹");
\\inch		printf("″");
\\chemform[{]Al_2O_3[}]	printf("Al₂O₃");
\\ang[{][0-9.]*[}]	{
		char *p = strchr(yytext, '}');
		*p = '\0';
		printf("%s°", yytext+5);
		}
\\ohm		printf("SYM(ohm)");
\$\\mid\ S_[{]11[}]\ \\mid\$	printf("|S₁₁|");
\\leq		printf("≤");
\\tesla		printf("T");
\\joule		printf("J");
\\kW		printf("kW");
\\celsius		printf("℃");
\\keV		printf("keV");
\$[<]([0-9]*\.)?[0-9]*\$ {
		char *p = strchr(yytext+1, '$');
		*p = '\0';
		printf("\\textless%s\n", yytext+2);
		}
\\includegraphics\[width\=\\textwidth\][{]MYRRHATDRPartB-img\/D-MYRTE-2\.6-Table5\.png[}]	{
		printf("IMAGE(D-MYRTE-2.6-Table5)");
	}
\\LinacInjector		printf("Injector");
\\LinacTargetPTF	printf("PTF");
\\LinacTargetFPBD	printf("FPBD");
\\LinacTargetFTS	printf("FTS");
\\LinacTargetETBD	printf("ETBD");
\\LinacMEBTiii		printf("MEBT-3");
\\LinacMEBTii		printf("MEBT-2");
\\LinacMEBTi		printf("MEBT-1");
\\LinacQWR		printf("QWR");
\\LinacLEBT		printf("LEBT");
\\milli\\ampere		printf("mA");
\\ampere		printf("A");
\\mega		printf("M");
\\meV		printf("meV");
\\dBc		printf("dBc");
\\degree	printf("SYM(degree)");
\\ge		printf("SYM(ge)");
\\lt		printf("SYM(lt)");
\\dB		printf("dB");
\\watt		printf("W");
\\micro		printf("SYM(mu)");
\\LinacRF	printf("RF");
\\hertz		printf("Hz");
\\gram		printf("gram");
\\TSCLinac	printf("SC Linac");
\\decibel	printf("dB");
\\milli		printf("m");
\\micro\\second	printf("SYM(mu)s");
\\hyperref\[([^]])*\]	;
\\EURISOL	printf("EURISOL");
\\EUROTRANS	printf("EUROTRANS");
\\PDSXADS	printf("PDS/XADS");
\\MYRRHA	printf("MYRRHA");
\\MINERVA	printf("MINERVA");
\\MAX		printf("MAX");
\\MARISA	printf("MARISA");
\\MYRTE		printf("MYRTE");
\\metre		printf("m");
\\MVpermt	printf("MVpermt");
\\CodesMicroWave	printf("CST MicroWave Studio");
\\LinacLLRF	printf("LinacLLRF");
\\volt		printf("V");
\\radian	printf("radian");
\\mm		printf("mm");
\\newton		printf("N");
\\minute		printf("m");
\\mega\\pascal		printf("MPa");
\\pascal		printf("Pa");
[{]\\ms[}]	printf("ms");
\\href[{]([^}])*[}][{]([^}])*[}]	{
						char *p, *q, *r;
						p=strchr(yytext,'{');
						q=strchr(p+1,   '{'); *(q-1) = '\0';
						r=strchr(q+1,   '}'); *r     = '\0';
						printf("%%\nHREF(%s,%s)%%\n", p+1, q+1);
					}
\\meter		printf("m");
\\bar		printf("bar");
\$\\cdot\$	printf("·");
\$\\Rightarrow\$	printf(" ⇨ ");
\$\\Delta\$	printf("Δ");
\$\\num[{]([0-9]*\.)?[0-9]*[de][0-9]*[}]\$	{
		char *       q = strchr(yytext, '}');
		char *       p = strchr(yytext, 'd');
		if (p==NULL) p = strchr(yytext, 'e');

		*p = *q = '\0';
		printf("%s × TEN_TO(%s)", yytext + 6, p+1); // %s × 10
	}
\$_\\text[{]([^}])*[}]\$	{
		char *p = strchr(yytext, '}');
		*p = '\0';
		printf("HTMLSUB(%s)", yytext+7);
	}

\\textcolor[{][a-z]*[}]	;
\\per	printf(" / ");

(.|\n)	ECHO;

