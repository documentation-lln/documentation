%{
/**********************************************************************************************************
 sectionpages reads a .toc file from stdin and generates a statically allocated C array that
 associates section numbers to page numbers. If l is the latex source that produced the .toc file,
 then Section i begins at page LaTeXsections[i] of l. The array is output on stdout.

 By vdflorio
 Version 0.1: 20201027
 Version 0.2: 20201029T1419
 Version 0.3: 20201103T1533 
 	      The static array is now of type structure_t and includes both page and title of each section
 Version 0.4: 20201104T1521 
 	      A separate data structure has been created to store pages and titles of Appendix sections
	      Cleaning up. Testing.
 **********************************************************************************************************/

 #include <stdio.h>
 #include <stdlib.h>
 #include <string.h>
 #include <ctype.h>
 #include "vdfio.h"

#define BUFSIZE 4096
#define TDR	"MINERVA-100MeV-ACCELERATOR-TDR.toc"

int  col = 0;
int  section = 1;
bool Inside = false;
int Appendix = -1, Bib = -1;
bool clang = false, pythonlang = false;
bool verbose = false;

// We look for patterns structured as follows:

// \contentsline {section}{\numberline {\leavevmode {\color {blue}1}}TitleOfTheSection}{
// \contentsline {section}{\numberline {A}Parameters List}{215}{section.Alph0.1}%
// \contentsline {part}{Bibliography}{235}{section*.301}%

char sect_title[BUFSIZE];
char myytext[BUFSIZE];
int  myysp = 0;

// \contentsline {section}{\numberline {B}Appendices to Sect.~\ref {sect:injector}}{219}{section.Alph0.2}%


%}

NEWLINE \n
ANY	.

PREFIX ^(\\contentsline[ ]\{section\}\{\\numberline[ ]\{\\leavevmode[ ]\{\\color[ ]\{blue\}[0-9]*\}\}[^}]*\}\{)
APPENDIX	^(\\contentsline[ ]\{section\}\{\\numberline[ ]\{[A-Z]\}(.)*\}\{section)
AAPPENDIX	^(\\contentsline[ ]\{section\}\{\\numberline[ ]\{[A-Z]\}[^}]*\}\{)
BIB	^(\\contentsline[ ]\{part\}\{Bibliography\}\{)

%%

{APPENDIX} { char *ptitle = yytext + yyleng - 14;
	     char *ppage, *p;

		if (verbose) printf("APPENDIX\n");
		Inside = true;
		myysp = 0;
		*ptitle = '\0';
		while (*ptitle != '{' || ! isalpha(ptitle[1]) || ptitle[2] != '}') ptitle--;
		if (verbose) printf("APPENDIX: ptitle[0..2] == %c%c%c\n", ptitle[0], ptitle[1], ptitle[2]);
		strcpy(sect_title, ptitle+3);
		if (verbose) printf("APPENDIX: sect_title == %s\n", sect_title);
		if (Appendix < 0) {
			p = ppage = ptitle + strlen(ptitle) + 2;
			if (verbose) printf("APPENDIX: ppage == '%s'\n", ppage);
			while (*++p != '}') ;
			*p = '\0';
			printf("\t/* Appendices then start on page */ { %s, NULL }\n", ppage);
			//Appendix = 0;
			Appendix = atoi(ppage);
			printf("};\n\n");
			if (verbose) printf("APPENDIX: page == '%s'\n", ppage);
			printf("static section_t Appendices[] = { { %s, \"%s\" },\n", ppage, sect_title);
		} else {
			p = ppage = ptitle + strlen(ptitle) + 2;
			if (verbose) printf("APPENDIX: ppage == '%s'\n", ppage);
			while (*++p != '}') ;
			*p = '\0';
			printf("\t\t\t{ %s, \"%s\" },\n", ppage, sect_title);
		}
	}

{BIB} {
		if (verbose) printf("BIB\n");
		Bib = 0;
		Inside = true;
		myysp = 0;
	}

{PREFIX}	{ char *ptitle = yytext + yyleng - 2;

		if (verbose) printf("PREFIX\n");
		Inside = true;
		myysp = 0;
		*ptitle = '\0';
		while (*--ptitle != '}') ;
		strcpy(sect_title, ptitle+1);
	}

{ANY}	{
		if (Inside) {
		if (verbose) printf("ANY/INSIDE\n");
			/*
			 If we are Inside, then what follows has the following structure:
			
			 PageWeAreLookingFor}{section.SomeNumber.SomeNumber}%
			 e.g.
			 21}{section.0.1}%
			*/
			if (isdigit(*yytext)) {
				/* At first, we expect digits, which are pushed onto myytext */
				myytext[myysp++] = *yytext;
			} else {
				/*
				  Whatever follows is garbage; actually, garbage structured as follows:

					}{section.SomeNumber.SomeNumber}%
					e.g.
					}{section.0.1}%
	
				  We simply don't care: "close" the stack, declare that we are no more Inside, and leave
				*/
				myytext[myysp] = '\0';
				Inside = false;
			}
		} /* if we are not Inside, we just ignore the character */
	}

{NEWLINE}	{
			if (verbose) printf("NEWLINE (Inside == %s)\n", Inside? "True": "False");

			//if (Inside == false) ;
			//else
			/* When we encounter a \n, we check whether myytext holds something: */
			if (myysp) {
				myysp = 0;
				if (Appendix == 0) { /* is it the appendix? */
					Appendix = atoi(myytext);
					//printf("\t\t\t{ %s, \"%s\" },\n", myytext, sect_title);
				} else if (Bib == 0) { /* is it the bibliography? */
					Bib = atoi(myytext);
					myysp = 0;
				} else { /* otherwise, it is a section, and we issue a new entry in LaTeXsectionss... */
					if (clang) {
						if (section > 0)
							printf("\t/* Sect. %3d begins at page */ { %8s, \"%s\" }, \n",
								section, myytext, sect_title);
						else
							printf("%4s,", myytext);
					} else // pythonlang
						printf("\n\t%2d: [ %s, \"%s\" ], ", section, myytext, sect_title);

					section++;
					myysp = 0; /* ...and "clear" the buffer */
				}
			} /* otherwise, we ignore the \n */
			//Inside = false;
		}
%%
void invalid_options(char s[]) {
	fprintf(stderr, "%s: %sinvalid option%s. Valid option are '-p', '-c', and '-t'\n", s, KRED, KNRM);
	fprintf(stderr, "'-c'  means 'output a C header file'     (default: true)\n");
	fprintf(stderr, "'-p'  means 'output a Python dictionary'\n");
	fprintf(stderr, "'-t'  means 'assume input is the TDR'    (default: false)\n");
	fprintf(stderr, "\n%sNote%s: '-c' and '-p' are mutually exclusive.\n", KMAG, KNRM);
}

int main(int argc, char *argv[]) {
	int i;
	bool tdr = false, verbose = false;

	for (i=1; i<argc; i++) {
		if (argv[i][0] == '-')			// first  char of argv[i]
		switch (argv[i][1]) {			// second char of argv[i]
			case 'c':
			case 'C':
				clang = true;
				break;
			case 'p':
			case 'P':
				pythonlang = true;
				break;
			case 't':
			case 'T':
				tdr = true;
				break;
			case 'v':
			case 'V':
				verbose = true;
				break;
			default:
				invalid_options(*argv);
				verror(ERR_WRONG_ARG);
				exit  (ERR_WRONG_ARG);
			}
		else {
			invalid_options(*argv);
			verror(ERR_WRONG_ARG);
			exit  (ERR_WRONG_ARG);
		}
		if (argv[i][2] != '\0')			// third  char of argv[i]
			vwarning(WARN_TRAILING_CHARS);
	}

	if (clang && pythonlang) {
		fprintf(stderr, "%s: %sOption '-p' and '-c' are mutually exclusive.%s\n", *argv, KRED, KNRM);
		verror(ERR_OPTION_CLASH);
		exit  (ERR_OPTION_CLASH);
	}

	if (tdr)
		if (freopen(TDR, "r", stdin) == NULL) {
			fprintf(stderr, "%s: %s'-t' specified, though %s cannot be accessed.%s\n", *argv, KRED, TDR, KNRM);
			perror("reopen");
			verror(ERR_CANT_ACCESS);
			exit  (ERR_CANT_ACCESS);
		}

	// Default: clang
	if (clang == false && pythonlang == false) clang = true;

	if (verbose)
		fprintf(stderr, "Processing %s. Requested output is a %s data structure.\n",
			(tdr)? TDR:"standard input stream",
			(clang)? "C": "Python"
		);

	if (clang) {
		printf("typedef struct { int page; char *title; } section_t;\n");
		printf("static section_t LaTeXsections[] = { { 0, NULL },\n");

		while ( yylex() ) ;

		printf("};\n");

		printf("\n/* Pages where the Appendices and Bibliography begin */\n");
		printf("\tint Appendix = %4d;\n", Appendix);
		printf("\tint Bib = %4d;\n", Bib);
		printf("\n/* Number of sections (cardinality of LaTeXsections[]) */\n");
		printf("\tint Sections = %4d;\n", section-1);
	} else { // pythonlang
		printf("LaTeXsections = { ");

		while ( yylex() ) ;

		printf("'Appendix' : %d, 'Bibliography' : %d, 'Number of Sections' : %d }\n", Appendix, Bib, section-1);
	}

	// ^"\\"contentsline[ ][{]section[}][{]"\\"numberline[ ][{]"\\"leavevmode[ ][{]"\\"color[ ][{]blue[}][0-9]*[}][}][A-Za-z ]*[}][{] 
	if (verbose)
		fprintf(stderr, "Processing concluded without errors. さようなら。\n");
	verror (ERR_NONE);
	return (ERR_NONE);
}

/* EoF sectionpages.l */
