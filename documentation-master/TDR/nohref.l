%{
// <SPAN CLASS="MATH"><A ID="tex2html_wrap_inline6944"
// HREF="img68.svg">[IMAGE svg]</A></SPAN>
int a;
#define ROWS 10
#define COLS 10
char *frac[ROWS+12][COLS+1] = 
		       {// 0	 1	2    3	   4	5     6     7     8     9     10
			{ NULL, NULL, NULL, "↉",  NULL, NULL, NULL, NULL, NULL, NULL, NULL }, // 0/
			{ NULL, NULL, "½",  "⅓",  "¼",  "⅕",  "⅙",  "⅐",   "⅛",  "⅑", "⅒ " }, // 1/
			{ NULL, NULL, "⅔",  NULL, NULL, "⅖",  NULL, NULL, NULL, NULL, NULL }, // 2/
			{ NULL, NULL, NULL, "⅔",  NULL, NULL, NULL, NULL, NULL, NULL, NULL }, // 3/
			{ NULL, NULL, NULL, NULL, NULL, "⅘",  NULL, NULL, NULL, NULL, NULL }, // 4/
			{ NULL, NULL, NULL, NULL, NULL, NULL, "⅚",  NULL, "⅝",  NULL, NULL }, // 5/
			{ NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL }, // 6/
			{ NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, "⅞",  NULL, NULL }, // 7/
			{ NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL }, // 8/
			{ NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL }, // 9/
			{ NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL }, // 10/
		       };
int table266;
int mathmode;
int tex2html_wrap_inline;
%}

%%
[<]SPAN[ ]CLASS=["]MATH["][>][<]A[ ]ID[=]["]tex2html_wrap_inline[0-9]*["] {
				char *p, *q;
				p = strstr(yytext, "inline");
				q = strchr(p+1, '"');
				*q = '\0';

				tex2html_wrap_inline = atoi(p + 6);
				a=0;
				//printf("tex2html_wrap_inline is %d\n", tex2html_wrap_inline);
			}

[<][/]SPAN[>]	a=1;

\\sfrac[0-9][0-9]\\si[{]\\inch[}]	{
		int d1=yytext[6]-'0',    d2=yytext[7]-'0';
		if (d1 < 0 || d1 > COLS)
			printf("%c/%c", yytext[6], yytext[7]);
		else
		if (d2 < 0 || d2 > COLS)
			printf("%c/%c", yytext[6], yytext[7]);
		else {
			if (frac[d1][d2]==NULL)
				printf("%c/%c", yytext[6], yytext[7]);
			else
				printf("%s", frac[d1][d2]);
		}
	}


[0-9][&]nbsp;[0-9][0-9]	{
			if (a)  {
				int d1=yytext[7]-'0',    d2=yytext[8]-'0';
				printf("%c&nbsp;", *yytext);
				if (d1 < 0 || d1 > COLS)
					printf("%c/%c", yytext[7], yytext[8]);
				else
				if (d2 < 0 || d2 > COLS)
					printf("%c/%c", yytext[7], yytext[8]);
				else {
					if (frac[d1][d2]==NULL)
						printf("%c/%c", yytext[7], yytext[8]);
					else
						printf("%s", frac[d1][d2]);
				}
				//printf("″");
				}
			}

(.|\n)		if (a) putchar(*yytext);

HREF=\"img[0-9]*\.svg\"[>]\[IMAGE\ svg\]\<\/A\>		{
	char *p = strdup(yytext);
	char *q = strchr(p, '.');
	int img;
	*q = '\0';
	img = atoi(p+9);
	//fprintf(stderr, "here\n"); fflush(stderr);
	switch(tex2html_wrap_inline) {
	case 6854: printf("</a>&le;");  break;
	case 6996: printf("</a>&ge; 100&mu;s");  break;
	case 6998: if (img==2) printf("</a>≈1");  break;
	case 7000: printf("</a>&ge; 10&mu;s");  break;
	case 7002: if (img==4) printf("<sub>acc</sub>");  break;

	case 7006: printf("</a><em>Q</em><sub>0</sub>");  break;
	case 7008: printf("</a><em>P</em><sub>0</sub>");  break;
	case 7010: printf("</a><em>P</em><sub>beam</sub>");  break;
	case 7012: printf("</a><em>P</em><sub>total</sub>");  break;

	case 7014: printf("</a><em>T</em>");  break;

	case 7018: printf("</a><em>Z</em><sub>sh</sub>");  break;

	case 7048: printf("</a><em>&beta;</em><sub>optimal</sub>");  break;
	case 7050: printf("</a><em>E</em><sub>pk</sub>");  break;
	case 7052: printf("</a><em>E</em><sub>acc</sub>");  break;
	case 7054: printf("</a><em>B</em><sub>pk</sub>");  break;
	case 7058: printf("<sub>0</sub>");  break;
	case 7060: printf("<sub><em>res</em></sub>");  break;
	case 7062: printf("<sub><em>cav</em></sub>");  break;
	case 7070: printf("</a><em>L</em><sub>acc</sub>");  break;
	case 7072: printf("</a><em>&beta;</em><sub>optimal</sub> · <em>c</em> · <em>f</em>");  break;

	case 7074: printf("</a><em>R</em><sub>p</sub>");  break;

	case 7078: printf("· &beta; · &lambda;/2</a><em>R</em><sub>p</sub>");  break;
	case 7084: printf("B</em><sub>pk</sub> / E</em><sub>acc</sub>");  break;
	case 7086: printf("‣ ");  break;

	case 7090: printf("<sub><em>p</em></sub>");  break;
	case 7092: printf("K<sub><em>L</em></sub> = &Delta;<em>f</em> / E²</em><sub>acc</sub>");  break;
	case 7094: printf("K<sub><em>L</em></sub>");  break;
	case 7098: printf("&Delta;<em>f</em>");  break;

	case 7106: printf("[2.2 × 10⁶, 3 × 10⁶]");  break;
	case 7108: printf("&epsilon;<sub><em>r</em></sub>");  break;
	case 7110: printf("tan &delta;");  break;
	case 7112: printf("</a><em>E</em><sub>max</sub>");  break;
	case 7114: printf("&lt;"); break;

	case 7134: printf("</a><em>E</em><sub>max</sub>");  break;
	case 7160: printf("<sub><em>ext</em></sub>");  break;
	case 7176: printf("&Delta;"); break;

	case 6868: printf("Q̇<sub>2K</sub> = 1 W m<sup>-1</sup> to 5 W m<sup>-1</sup>");
	case 0: 
	if (img==1) {
	//printf("</a><IMG SRC=https://git.myrrha.lan/vdflorio/documentation/-/raw/master/TDR/MYRRHATDRPartB-img/D-MYRTE-2.6-Table5.png");
	printf("</a><IMG SRC=MYRRHATDRPartB-img/D-MYRTE-2.6-Table5.png");
	printf(" ALT=\"Table 5, from D-MYRTE-2.6, rendered as an image\">");
	}
		   break;
	default:  printf("<b>TBD: inline%d, img%d</b>\n", tex2html_wrap_inline, img);
	}
	tex2html_wrap_inline=0;
	}

char00D7	printf("&times;");
HTMLSUB[(]([^)])*[)]	{
			char *p = strchr(yytext, ')');
			*p = '\0';
			printf("<sub>%s</sub>", yytext + 8);
		}
SYMINCH		printf("&#8243");
SYM[(]tau[)]	printf("&tau;");
SYM[(]mu[)]	printf("&mu;");
SYM[(]beta[)]	printf("&beta;");
SYM[(]degree[)]	printf("&deg;");
SYM[(]ge[)]	printf("&ge;");
SYM[(]lt[)]	printf("&lt;");
SYM[(]ohm[)]	printf("&#8486;");
TEN_TO[(]([^)])*[)]	{
				char *p = strchr(yytext, ')');
				*p = '\0';
				printf("10<sup>%s</sup>", yytext+7);
			}

SYM[(]Eacc[)]		printf("<em>E<sub>acc</sub></em>");
SYM[(]Keff[)]		printf("<em>K<sub>eff</sub></em>");
SYM[(]Leff[)]		printf("<em>L<sub>eff</sub></em>");

MVpermt		printf("MV m<sup>-1</sup>");
TESLApermt	printf("T m<sup>-1</sup>");

IMAGE[(]D-MYRTE-2\.6-Table5[)]	{
					printf("<IMG SRC=https://git.myrrha.lan/vdflorio/documentation/-/raw/master/TDR/MYRRHATDRPartB-img/D-MYRTE-2.6-Table5.png");
					printf(" ALT=\"Table 5, from D-MYRTE-2.6, rendered as an image\">");
				}

[<]A\ ID\=["]table266["][\n][ ]HREF\=["]img1\.svg["][>][<]\/A[>][<]\/TD[>][<]\/TR[>]	{
	printf("<IMG SRC=https://git.myrrha.lan/vdflorio/documentation/-/raw/master/TDR/MYRRHATDRPartB-img/D-MYRTE-2.6-Table5.png");
	printf(" ALT=\"Table 5, from D-MYRTE-2.6, rendered as an image\">");
	}

[<]A\ ID\=["]table266["]	{ table266=1; }
hREF=\"img1\.svg\"[>]\[IMAGE\ svg\]\<\/A\> 	{
	if (table266) {
	//printf("<IMG SRC=https://git.myrrha.lan/vdflorio/documentation/-/raw/master/TDR/MYRRHATDRPartB-img/D-MYRTE-2.6-Table5.png");
	printf("<IMG SRC=MYRRHATDRPartB-img/D-MYRTE-2.6-Table5.png");
	//printf("<IMG SRC=https://git.myrrha.lan/vdflorio/documentation/-/blob/master/TDR/MYRRHATDRPartB-img/D-MYRTE-2.6-Table5.png");
	printf(" ALT=\"Table 5, from D-MYRTE-2.6, rendered as an image\">");
	}
	}


HREF[(]([^,])*[,]([^)])*[)]	{
					char *p, *q, *r;
					p = strchr(yytext, '(');
					q = strchr(p + 1,  ','); *q = '\0';
					r = strchr(q + 1,  ')'); *r = '\0';
					printf("<a href=%s>%s</a>", p+1, q+1);
				}

EQUA[(]1[)]	printf("Ngap &middot; &beta; &middot; &lambda;/2c");
EQUA[(]2a[)]	printf("MTBF<sub>&tau;&lt;0.1s</sub>");
EQUA[(]2b[)]	printf("MTBF<sub>0.1s&lt;&tau;&lt;3s</sub>");
EQUA[(]2c[)]	printf("MTBF<sub>&tau;&gt;3s</sub>");


