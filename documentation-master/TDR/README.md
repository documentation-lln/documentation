# Documentation 

Welcome to the documentation section pertaining to the MINERVA 100-MeV Accelerator TDR.

This directory includes the "main" document, MINERVA-100MeV-ACCELERATOR-TDR.tex and a number of ancillary directory and files.
This is work-in-progress ─ the directory hierarchy and related contents will be introduced and explained progressively.

## Structure 

Here's the major contents as of Mo Feb 10 15:11:46 CET 2020. Minor changes have not been displayed yet.
The tree structure also shows ancillary files produced by LaTeX and not part of the repository.

```
├── a                                         Script: Opens the PDF output
├── acm.bst                                   Bibligraphy style used in the TDR
├── f                                         Script: Opens the LaTeX document that includes its first argument
├── FrontPagePartB-v2p345.pdf                 Front page of the TDR. Derived from old Part B TDR, needs to be updated
├── MINERVA-100MeV-ACCELERATOR-TDR.aux        ─ LaTeX byproduct. Not included in the repository ─
├── MINERVA-100MeV-ACCELERATOR-TDR.bbl        Bibliography produced by LaTeX when processing MINERVA-100MeV-ACCELERATOR-TDR.tex
├── MINERVA-100MeV-ACCELERATOR-TDR.blg        ─ LaTeX byproduct. Not included in the repository ─
├── MINERVA-100MeV-ACCELERATOR-TDR.idx        Index of terms produced by LaTeX when processing MINERVA-100MeV-ACCELERATOR-TDR.tex
├── MINERVA-100MeV-ACCELERATOR-TDR.lof        ─ LaTeX byproduct. Not included in the repository ─
├── MINERVA-100MeV-ACCELERATOR-TDR.log        ─ LaTeX byproduct. Not included in the repository ─
├── MINERVA-100MeV-ACCELERATOR-TDR.lot        ─ LaTeX byproduct. Not included in the repository ─
├── MINERVA-100MeV-ACCELERATOR-TDR.out        ─ LaTeX byproduct. Not included in the repository ─
├── MYRRHA ACCELERATOR - TDR.pdf              OLD PDF OUTPUT (Including Front Page). NOT UPDATED, please use MINERVA-100MeV-ACCELERATOR-TDR.pdf
├── MINERVA-100MeV-ACCELERATOR-TDR.pdf        PDF OUTPUT (Not including Front Page)
├── MINERVA-100MeV-ACCELERATOR-TDR.tex        MAIN LaTeX DOCUMENT
├── MINERVA-100MeV-ACCELERATOR-TDR.toc        Table of Contents produced by LaTeX when processing MINERVA-100MeV-ACCELERATOR-TDR.tex
├── MYRRHATDRPartA-img                        Folder with all images pertaining to Part A of the TDR
│   ├── MYRRHATDRPartA-img001.png
│   │    ...                                  (Part A images)
│   └── MYRRHATDRPartA-img014.png
├── MYRRHATDRPartB-img                        Folder with all images pertaining to Part A of the TDR
│   ├── MYRRHATDRPartB-img001.png
│   │    ...                                  (Part B images)
│   └── MYRRHATDRPartB-img283.png
├── MYRRHATDRPartC-img                        Folder with all images pertaining to Part C of the TDR
│   ├── MYRRHATDRPartC-img001.png
│   │    ...                                  (Part C images)
│   └── MYRRHATDRPartC-img011.jpg
├── p                                         Script: processes MINERVA-100MeV-ACCELERATOR-TDR.tex
├── README.md                                 This file
├── TDR-A-Chapters                            LaTeX sections of Part A
│   ├── README.md
│   ├── TDR-A01-GeneralIntro.tex
│   ├── TDR-A02-MyrrhaOverview.tex
│   ├── TDR-A03-ProjectHistory.tex
│   ├── TDR-A04-AcceleratorOverview.tex
│   ├── TDR-A05-Phases.tex
│   ├── TDR-A06-100MeV-Overview.tex
│   └── TDR-A07-RadiationProtection.tex
├── TDR-B-Chapters                            LaTeX sections of Part B
│   ├── README.md                             Markdown README
│   ├── TDR-B01-Injector.tex
│   ├── TDR-B02-MEBT3.tex
│   ├── TDR-B03-MainLinac.tex
│   ├── TDR-B04-HEBT.tex
│   ├── TDR-B05-ReaWinSpaTar.tex
│   ├── TDR-B06-ProTarBeamLines.tex
│   ├── TDR-B07-CHcavities.tex
│   ├── TDR-B08-SingleSpoke.tex
│   ├── TDR-B09-DoubleSpoke.tex
│   ├── TDR-B10-Elliptical.tex
│   ├── TDR-B11-RF.tex
│   ├── TDR-B12-Cryogenic.tex
│   ├── TDR-B13-BeamInstr.tex
│   ├── TDR-B14-MagnetsSteerers.tex
│   ├── TDR-B15-VacuumSystem.tex
│   ├── TDR-B16-WaterCooling.tex
│   ├── TDR-B17-ReactMonitoring.tex
│   ├── TDR-B18-ControlSystem.tex
│   ├── TDR-B19-SafetyReliability.tex
│   ├── TDR-B20-Building.tex
│   ├── TDR-B21-CommissDecommiss.tex
│   └── TDR-B99-AcronAbbrv.tex
├── tdr.bib                            BIBTEX REFERENCES DATABASE
├── TDR-C-Chapters                     LaTeX sections of Part C
│   ├── README.md                      Markdown README
│   └── TDR-C01-Reliability.tex
├── v                                  Script: launches editor
└── vv                                 Script: launches editor (improved version)
```

(see also [here](https://git.myrrha.lan/vdflorio/documentation/-/blob/master/TDR/tree.svg))

## Compiling the TDR

Compiling the TDR comes intwo flavours: simple and advanced.

- Simple mode is the default mode. For this, you only need LaTeX and a program such as `pdflatex` to produce the TDR.
  This is because, by default, the TDR does not make use of external modules: the support
  for [TeXObjects](https://git.myrrha.lan/vdflorio/documentation/blob/master/LaTeX/TeXObjects.md) is disabled.
- Advanced mode is when you do want to make use of so-called [TeXObjects](https://git.myrrha.lan/vdflorio/documentation/blob/master/LaTeX/TeXObjects.md). In such a case, you need to enable the execution of external
  modules from within a LaTeX compiler (I mean, a program such as `pdflatex` or `rubber`).

The advanced mode has three requirements:

- First, you need to compile your sources with option `--shell-escape`. In other words, your command line should look like
  `pdflatex --shell-escape` or `rubber --pdf --shell-escape`. This is done automatically by script `p`
  (see below). If you use a WYSIWYG production system, please
  configure it in such a way that LaTeX be invoken with said option. You will have to change a line
  that mentions `rubber` or `pdflatex` and add ` --shell-escape` immediately after the command.
- Secondly, you need to allow the execution of external module `./TeXObjects.py`. This is a Bash shell script that invokes
  Python. If you use Windows, you should have Bash already as it is included with most Git clients.
  You will possibly need to install [Python](https://www.python.org/downloads/).
  If you use Windows you might consider [cygwin](https://www.cygwin.com/) that allows to install LaTeX,
  Git, and Python as if you were on a Linux machine. Another option is of course to use a virtual machine
  running Linux (in fact that's what I do).
- Finally, you should uncomment the following line in file [MINERVA-100MeV-ACCELERATOR-TDR.tex](https://git.myrrha.lan/vdflorio/documentation/blob/master/TDR/MINERVA-100MeV-ACCELERATOR-TDR.tex):

  > `%\newcommand*{\EnableTeXObjects}{}%`

  (comments in latex start with `%` so you just need to remove the `%` character). If you're interested, I can tell you [what happens then](https://git.myrrha.lan/vdflorio/documentation/blob/master/TDR/zz.md).

Should you encounter problems, please do not hesitate to contact me!

  
## Comments

In order to place comments in the TDR, please use command `\mycomment`. Usage is simple: `\mycomment{your-id}{`*text*`}`
inlines *text* in a box, also displaying a progressive integer number and string `your-id`, representing the author
of the comment.

So for instance

![](images/mycomment1.png)

produces

![](images/mycomment2.png)

Latest news: I am now using package [pdfcomment](https://tex.stackexchange.com/questions/6306/how-to-annotate-pdf-files-generated-by-pdflatex) for comments,
which actually produces PDF box comments. This means in particular that the text of the comments does not "*pollute*"
the text of the document.



## Scripts

- Script `f` is used to find-and-edit TDR LaTeX documents that include a given sentence. Here's an example:

    ```
    $ f "described in F"
    TDR-B-Chapters/TDR-B01*.tex exists and can be accessed
    vim +/"described in F" TDR-B-Chapters/TDR-B01-Injector.tex
    Ready to commit the file? (y/*)
    ```

  String `described in F` is found in Part B, Section 1 (the "Injector" section). That LaTeX document is opened in the vim editor
  and the cursor is moved onto the first occurrence of `described in F`. Once the editor exits, they have the option
  to commit the just edited file or not. The file is also uploaded onto the SCK-CEN virtual machine `l2.sckcen.be`
  *provided that a PulseSecure/GlobalProtect session is active*.

- Script `v` is used to edit a TDR LaTeX for which we know Part and Section number. As an example,

    ```
    $ v b 1
    TDR-B-Chapters/TDR-B01*.tex exists and can be accessed
    ```

  opens Part B, Section 1 (the "Injector" section). Once the editor exits, they have the option
  to commit the just edited file or not. The file is also uploaded onto the SCK-CEN virtual machine `l2.sckcen.be`
  *provided that a PulseSecure/GlobalProtect session is active*.

- Script `p` is used to compile the TDR LaTeX, produce the PDF output, and instruct the upload of a new version on Alexandria.
  The user is given the possibility to upload the output uploaded onto the SCK-CEN virtual machine `l2.sckcen.be`.
  The latter assumes that a PulseSecure or GlobalProtect session is active.
  *NOTE*: In order to process the TDR, the shell-escape mode must be turned on. This is done automatically in
  script `p`, but needs to be esplicitly activated if you use `pdflatex` instead. The correct parameter is
  `--shell-escape`.

  Note: The upload on Alexandria is managed via OpenText Office Editor on Windows:
  ![](images/OpenText-Office-Editor.png)

## Makefile

It is also possible to compile the TDR just by typing "make" -- provided that "make" is available in your operating system[^make].
Note that, if the command `make` is interrupted, you may need to explicitly execute your `pdflatex` or `rubber` command.

## News
- I introduced symbol ``EnableBeyondMINERVA''. By commenting out its definition in the main TDR module,
  the sections on double spoke and elliptical cavities, as well as those on reactor window and spallation
  target and reactor reactivity monitoring are being temporarily omitted from the TDR.  This will be
  used to omit other sections and other information from the TDR.

- I introduced symbol ``NewVacuumSection''. When defined, the symbol enables the compilation of a new, tentative version of the
  Vacuum System section, yet to be reviewed. When commented out, the old version is compiled instead.

- The output of `git log`, with dates and descriptions of all changes committed to the Documentation project,
  is now attached to the TDR so as to provide the reader with a "*history of changes*" applied to the document
  he or she is reading. On Windows you can use Acrobat Reader to access the attachment:

  ![](images/GitLog.png)

  On Linux, Okular provides a similar feature:

  ![](images/GitLogLinux.png)

  The same mechanism is used to attach the project `CHANGELOG` and the `versions.dat` file, which mentions all
  version changes and maps them to the major Alexandria versions.

- Command `p` now copies the PDF of the TDR onto folder *MYRRHA ACCELERATOR TDR* on [Alexandria]( https://ecm.sckcen.be/OTCS/llisapi.dll?func=ll&objId=19670138&objAction=browse&viewType=1), from which it can be accessed by any Alexandria user.

- I recently added text folders to the main source of the TDR.

## Known Issues
Please refer to [this](https://git.myrrha.lan/vdflorio/documentation/blob/master/TDR/KNOWN-ISSUES.md) document.

## Selective compilation of TDR sections
Jorik requested a tool to choose which sections to compile and which ections to exclude from the TDR.
This is fairly possible now via the new command `andy` (the name is a pun on "*en*able" and "*di*sable").
Please have a look [here](https://git.myrrha.lan/vdflorio/documentation/blob/master/TDR/andy.md) for more information on this!

## Spelling checker
I use aspell for checking spelling. The command I use is

    aspell --lang=en_GB check *document.tex*

To install aspell on ubuntu systems, execute `sudo apt-get install aspell*`.

## Naming convention macros

A set of macros now associates the internal terminology to the official naming convention. This means that
  - there is a unique repository that defines and maps the terms
  - there is a standard way to type out the terms with the same typographical conventions

The set of macros may also check that the passed values are coherent. It is not possible, e.g., to refer to a QWR3.
This checking at the moment is incomplete, though shall be completed in the future.

The Authors are urged to make use of the naming convention macros in their chapters. 
The following macros have been realized and tested:

```
      - \ncRFQ       --> i1rfqrfcav01
      - \ncQWR{i}    --> i1me1rfcav0{i}		i == 1, i == 2
      - \ncCH{i}     --> i1ch{i}rfcav01		1 <= i <= 15
      - \ncCHR1      --> i1me2rfcav01 
      - \ncCHR2      --> i1me3rfcav01 
      - \ncSPOKEREB2 --> lbme3rfcav01 [^] 
      - \ncSPOKEREB3 --> lbme3rfcav02 [^]
      - \ncSPOKE{i}   --> lbs{j}rfcav01		i odd,  1 <= i <= 59, j = (i div 2) + 1 
      - \ncSPOKE{i}   --> lbs{j}rfcav02		i even, 2 <= i <= 60, j = (i div 2)
```

I will implement missing macros on demand.

The naming convention macros are available [here](https://git.myrrha.lan/vdflorio/documentation/-/blob/master/TDR/Annex/Naming.tex).

[^]: Most likely to be renamed so as to use \ncSPOKEREB1 and \ncSPOKEREB2.

----

[^make]: if your operating system has no `make`, do not install `make` -- change operating system instead[^joke]
[^joke]: I'm just kidding /play yeah
