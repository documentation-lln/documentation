%{
#include "vdfio.h"
char mybuffer[2048];
%}

%%

^[^:]*:%        {
			int c;
			while ( (c = input()) != EOF )
				if (c == '\n') break;
			*mybuffer = '\0';
		}
^[^:]*:		{
			sprintf(mybuffer, "%s%s%s", KGRN, yytext, KNRM);
			//strcpy(mybuffer, yytext);
		}
\\%		{
			strcat(mybuffer, yytext);
		}
(%|\n)		{
			printf("%s\n", mybuffer);

			if (*yytext == '%') {
				int c;
				while ( (c = input()) != EOF )
					if (c == '\n') break;
			}

			// in both cases
			*mybuffer = '\0';
		}
.		{
			strcat(mybuffer, yytext);
		}
