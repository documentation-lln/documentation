%{
/***************************************************************************************************
 figurepages reads a .lof file from stdin and generates a statically allocated C array that
 associates figure numbers to page numbers. If l is the latex source that produced the .lof file,
 then Figure i appears in page fig2page[i] of l. The array is output on stdout.

 By vdflorio, October 2020
 ***************************************************************************************************/
#include "vdfio.h"

#define TDR	"MINERVA-100MeV-ACCELERATOR-TDR.lof"
int col = 0;
int fig;
bool  clang = false, pythonlang = false;
%}

%%
\{[0-9]*\}\{figure.caption.[0-9]*\}\%$	{
		char *p = yytext;
		col++;
		fig++;
		while (*++p != '}') ;
		*p = '\0';
		if (clang) {
			// printf("%3s - 4,", yytext+1); // "-4" because we will not actually have a \listoffigures in the TDR
			printf("%3s,", yytext+1); // "-4" because we will not actually have a \listoffigures in the TDR
			if (col == 10) { putchar('\n'); col = 0; } else putchar(' ');
		} else { // pythonlang
			printf("%d: %s, ", fig, yytext+1);
		}
	}
.	;
\n	;
%%

void invalid_options(char s[]) {
	fprintf(stderr, "%s: %sinvalid option%s. Valid option are '-p', '-c', and '-t'\n", s, KRED, KNRM);
	fprintf(stderr, "'-c'  means 'output a C header file'     (default: true)\n");
	fprintf(stderr, "'-p'  means 'output a Python dictionary'\n");
	fprintf(stderr, "'-t'  means 'assume input is the TDR'    (default: false)\n");
	fprintf(stderr, "\n%sNote%s: '-c' and '-p' are mutually exclusive.\n", KMAG, KNRM);
}

int main(int argc, char *argv[]) {
	int i;
	bool  tdr = false, verbose = false;

	for (i=1; i<argc; i++) {
		if (argv[i][0] == '-')			// first  char of argv[i]
		switch (argv[i][1]) {			// second char of argv[i]
			case 'c':
			case 'C':
				clang = true;
				break;
			case 'p':
			case 'P':
				pythonlang = true;
				break;
			case 't':
			case 'T':
				tdr = true;
				break;
			case 'v':
			case 'V':
				verbose = true;
				break;
			default:
				invalid_options(*argv);
				verror(ERR_WRONG_ARG);
				exit  (ERR_WRONG_ARG);
			}
		else {
			invalid_options(*argv);
			verror(ERR_WRONG_ARG);
			exit  (ERR_WRONG_ARG);
		}
		if (argv[i][2] != '\0')			// third  char of argv[i]
			vwarning(WARN_TRAILING_CHARS);
	}

	if (clang && pythonlang) {
		fprintf(stderr, "%s: %sOption '-p' and '-c' are mutually exclusive%s\n", *argv, KRED, KNRM);
		verror(ERR_OPTION_CLASH);
		exit  (ERR_OPTION_CLASH);
	}

	// Default: clang
	if (clang == false && pythonlang == false) clang = true;

	if (tdr)
		if (freopen(TDR, "r", stdin) == NULL) {
			fprintf(stderr, "%s: %s'-t' specified, though %s cannot be accessed.%s\n", *argv, KRED, TDR, KNRM);
			perror("reopen");
			verror(ERR_CANT_ACCESS);
			exit  (ERR_CANT_ACCESS);
		}

	if (verbose)
		fprintf(stderr, "Processing %s. Requested output is a %s data structure.\n",
			(tdr)? TDR:"standard input stream",
			(clang)? "C": "Python"
		);

	if (clang) {
		printf("static int fig2page[] = { 0, \n");
		while ( yylex() ) ;
		printf("\t};\n");
	} else { // pythonlang
		printf("fig2page = { ");
		while ( yylex() ) ;
		printf(" }\n");
	}

	if (verbose)
		fprintf(stderr, "Processing concluded without errors. さようなら。\n");
	verror (ERR_NONE);
	return (ERR_NONE);
}
