#!/usr/bin/env bash

echo "Args are: $1 and $2"
echo

if [ "$1" = "$2" ]; then
  echo "$1 is equal to $2"
  echo "true"
  exit 0
fi
echo "$1 is different from $2"
echo "false"
exit 1
