#!/usr/bin/env python
# TeXObjectDB v0.1, by vdflorio
# This module defines the TeXObject dictionary and a function to print records matching an input key

#{{{ TeXObject dictionary
d = {
        #{{{ Figures
        "fig:Reference-Linac"     : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img007.png",
                                        "width"      : r".8\textwidth",
                                        "height"     : "",
                                        "caption"    : "The reference Linac injectors.",
                                        "abbrev"     : "",
                                    },
        "fig:FrontEnd"            : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img008.png",
                                        "width"      : r".8\textwidth",
                                        "height"     : "",
                                        "caption"    : "The injector's front end section.",
                                        "abbrev"     : "",
                                    },
        "fig:FrontEnd:ProtonSource" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img009.png",
                                        "width"      : r".8\textwidth",
                                        "height"     : "",
                                        "caption"    : "Front end section, proton source.",
                                        "abbrev"     : "",
                                    },
        "fig:BeamTimeStructure"  : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img010.png",
                                        "width"      : r".8\textwidth",
                                        "height"     : "",
                                        "caption"    : "Proposed beam time structure for full power nominal operation.",
                                        "abbrev"     : "",
                                   },
        "fig:RFQlosses"          : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRTE-D2.6-Fig19.png",
                                        "width"      : r".9\textwidth",
                                        "height"     : "",
                                        "caption"    : "Losses in the RFQ as function of the input beam distribution.",
                                        "abbrev"     : "",
                                   },
        "fig:Chopper-CollimationCone" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRTE-D2.6-Fig14.png",
                                        "width"      : r".9\textwidth",
                                        "height"     : "",
                                        "caption"    : "Chopper and collimation cone.",
                                        "abbrev"     : "",
                                   },
        "fig:LEBTprotonTracking" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img011.png",
                                        "width"      : r".9\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Multi-particle proton tracking in the LEBT.\newline
The input beam parameters of the \index{simulation codes!TraceWin}TraceWin code~\cite{P-ICCS02-Duper}
simulation were: $\varepsilon$\textsubscript{RMS.norm.proton}=0.2 \hbox{$\mathit{mm}\cdot\mathit{mrad}$},
$\alpha=-1.7$; $\beta=0.12$ mm/($\pi$\hbox{$\cdot\mathit{mrad}$}). An overall space charge compensation
of 90\% was assumed except in the chopper area (no compensation).""",
                                        "abbrev"     : "Multi-particle proton tracking in the LEBT.",
                                   },
        "fig:MultiParticleH2PlusSimulation" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img012.png",
                                        "width"      : r".9\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Multi-particle \hbox{$H_2^{\kern-1.2pt{+}}$} beam simulation.\newline
Multi-particle \hbox{$H_2^{\kern-1.2pt{+}}$} beam simulation and percentage of beam lost
along the line. The input beam parameters are the same than for the proton beam. An overall space
charge compensation of 90\% was assumed.""",
                                        "abbrev"     : r"Multi-particle \hbox{$H_2^{\kern-1.2pt{+}}$} beam simulation.",
                                   },
        "fig:SpaceChargeCompensation" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img013.png",
                                        "width"      : r".9\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Description of the space charge compensation process~\cite{M-MYRTE-2016-Gerardin}.""",
                                        "abbrev"     : "",
                                   },
        "fig:GrenobleLEBT"       : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img014-NEW.png",
                                        "width"      : r"\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""The LEBT at LPSC Grenoble.""",
                                        "abbrev"     : "",
                                   },
        "fig:CollimationSystem"  : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRTE-D2.6-Fig13.png",
                                        "width"      : r".4\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""View of the collimation system and Allison scanner (from backside) placed in the middle of the LEBT.""",
                                        "abbrev"     : "",
                                   },
        "fig:BeamParticleDistr" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img015.png",
                                        "width"      : r".5\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Beam particle distributions after the first focusing solenoid (hor.)\newline
Measurement of the beam particle distributions in the horizontal phase space after the first
focusing solenoid (Ibeam\_total = 9~mA, Isolenoid = 93~A).""",
                                        "abbrev"     : r"Beam particle distributions after the first focusing solenoid (hor.)",
                                   },
        "fig:TWISSGOprinciples"  : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img016.png",
                                        "width"      : r".8\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Principles of the developed emittance analysis tool TWISSGO.""",
                                        "abbrev"     : "",
                                   },
        "fig:ProtonHorEmittance" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img017.png",
                                        "width"      : r".7\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Proton horizontal emittance
(as a function of the residual gas pressure in the middle of the LEBT).""",
                                        "abbrev"     : r"Proton horizontal emittance.",
                                   },
        "fig:TWISSalphaParam"   : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img018.png",
                                        "width"      : r".65\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Proton beam Twiss parameter $\alpha$ (horizontal plane)\newline
(as a function of the residual gas pressure in the middle of the LEBT).""",
                                        "abbrev"     : r"Proton beam Twiss parameter $\alpha$ (horizontal plane).",
                                   },
    "fig:BeamPartDensity-Hor" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img019.png",
                                        "width"      : r".65\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Beam particles density in the horizontal phase space.\newline
Measurements for two different residual gas pressure: argon injection with pressures of
\SI{9.2d-6}{\mbar} for pattern $a$, and \SI{5.4d-5}{\mbar} for pattern $b$.",
                                        "abbrev"     : r"Beam particles density in the horizontal phase space.",
                                   },
        "fig:RFQ-RpValues"       : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img020.png",
                                        "width"      : r".6\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""A survey of Rp values for RFQ accelerators.""",
                                        "abbrev"     : "",
                                   },
    "fig:RFQresonantStructures" :  {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img021.png",
                                        "width"      : r".65\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""RFQ resonant structures.\newline
Green: current; Blue: inductive contributors; Red: capacitive contributors.""",
                                        "abbrev"     : r"RFQ resonant structures.",
                                   },
        "fig:RFQprototype"       : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img022.png",
                                        "width"      : r".65\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""MINERVA RFQ prototype (\hbox{\protect\midtilde}\SI{1}{\meter} long).""",
                                        "abbrev"     : "",
                                   },
      "fig:RFQsimulationResults" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img023.png",
                                        "width"      : r".92\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""RFQ simulation results (top: transient, bottom: nominal).""",
                                        "abbrev"     : "",
                                   },
     "fig:InjectorBoosterSection": {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img024and25.png",
                                        "width"      : r".92\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Injector booster section.""",
                                        "abbrev"     : "",
                                   },
       "fig:Booster-MEBT1toMEBT2": {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img026.png",
                                        "width"      : r".92\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Booster section, MEBT1 to MEBT2 and beyond.""",
                                        "abbrev"     : "",
                                   },
      "fig:SynchronousParticle": {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img041new.png",
                                        "width"      : r"\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""(a): ${\Delta}$W(${\Delta}\Phi $)/W\textsubscript{s} for $\Phi $s= 0
and various values of C = H\textsubscript{const}; (b): E\textsubscript{z} ($\Phi$) around $\Phi_\mathit{s}$ = 0.""",
                                        "abbrev"     : "",
                                   },
    "fig:SynchronousParticle-b": {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img042new.png",
                                        "width"      : r"\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""(a): ${\Delta}$W(${\Delta}\Phi $)/W\textsubscript{s}
for $\Phi_\mathit{s}$ = --30{\textdegree} and various values of C = H\textsubscript{const}.
(b): Ez($\Phi $) around $\Phi_\mathit{s}$ = --30{\textdegree} (blue dot: synchronous particle).""",
                                        "abbrev"     : "",
                                   },
    "fig:SynchronousParticle-c": {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img043new.png",
                                        "width"      : r"\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""(a): ${\Delta}$W(${\Delta}\Phi $)/W\textsubscript{s} for $\Phi_\mathit{s}$
= $-90${\textdegree} and various values of C = H\textsubscript{const}.
(b): E\textsubscript{z}($\Phi $) around $\Phi_\mathit{s}$ = $-90${\textdegree} (blue dot: synchronous particle).""",
                                        "abbrev"     : "",
                                   },
"fig:TransformationDuringBunching": {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img044new.png",
                                        "width"      : r"\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""(a): Transformation of the longitudinal phase space ellipse during
bunching $\Theta=\arctan(300\times \Delta W / (W_s\Delta\Phi))$.
(b): Angular velocity $\dv{\theta}{t}(\theta)$ on different phase trajectories parametrised by $\Delta\Phi_0$.""",
                                        "abbrev"     : "",
                                   },
    "fig:PhaseSpaceDistribution" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img045new.png",
                                        "width"      : r".9\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""(a): Defocused phase space ellipse prior to rebuncher entry.
(b): Rotated phase space distribution after first gap.
(c): Rotated phase space distribution after second gap.
(d): Focused phase space distribution after a drift of $\ell $\textsubscript{focal} = \SI{335.5}{\milli\meter}.""",
                                        "abbrev"     : "",
                                   },
              "fig:MEBT3Section" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/D-MYRTE-2.6-Fig50.png",
                                        "width"      : r"\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""The MEBT-3 section between the two injectors and
the main LINAC (dashed green line). It is divided into seven parts separated by two 45\textdegree-deviation
dipole magnets and a switching dipole connecting the two injectors to the LINAC section.""",
                                        "abbrev"     : r"The MEBT-3 section between the two injectors and the main LINAC.",
                                   },
       "fig:MEBT3LatticeDesigns" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/D-MYRTE-2.6-Fig51.png",
                                        "width"      : r"\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Two alternative designs of the MEBT-3 lattice
are shown. Design \#1 (top) uses 22.5\textdegree{} for the pole face angles in section \ding{173},
whereas Design \#2 (bottom) has straight pole faces.""",
                                        "abbrev"     : r"Two alternative designs of the MEBT-3 lattice.",
                                   },
       "fig:MEBT3SigmaEnvelopes" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/D-MYRTE-2.6-Fig52.png",
                                        "width"      : r"\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""The top left panel shows ${\pm}$3$\sigma$ envelopes along the MEBT{}-3 section
in the two transverse directions. Purple line represents the envelope in the horizontal plane and
green colour for the vertical. The top right is showing the dispersion of the section being
compensated after the switching dipole and satisfying the double achromaticity condition. Lower
panels show the longitudinal envelopes---phase (left) and energy (right).""",
                                        "abbrev"     : r"Envelopes and dispersion along the MEBT-3 section",
                                   },
  "fig:MEBT3ProtonDistributions" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/D-MYRTE-2.6-Fig53.png",
                                        "width"      : r"\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""The three panels show proton distributions at the exit of the
MEBT-3 line in the horizontal (left), vertical (center), and longitudinal (right) directions. The mismatch compared to
the desired distributions at the entrance of the main LINAC is smaller than 0.12\% in all directions. The red ellipses
correspond to 9 rms of the emittance.""",
                                        "abbrev"     : r"Proton distributions at the exit of the MEBT-3 line.",
                                   },
    "fig:MEBT3ChangeOfEmittance" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/D-MYRTE-2.6-Fig54.png",
                                        "width"      : r"\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""The three panels show the change of emittance in the three planes along the MEBT-3 for
three different beam currents (4 mA, 3 mA, and 2 mA). Red and blue lines correspond to transverse
emittances---horizontal and vertical, respectively. Green lines correspond to longitudinal emittance.""",
                                        "abbrev"     : r"Change of emittance along the MEBT-3.",
                                   },
    "fig:MEBT3LayoutTwoInjectors": {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img042.png",
                                        "width"      : r".8\textwidth",
                                        "height"     : "",
                                        "caption"    : r"Layout of MEBT3 (shown with 2 injectors installed).",
                                        "abbrev"     : "",
                                   },
          "fig:MEBT3RunningCases": {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img043.png",
                                        "width"      : r".65\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""The two MEBT3 running options.
Top: beam injection from first injector (in green); bottom: injection from second injector (in purple).""",
                                        "abbrev"     : r"The two MEBT3 running options.",
                                   },
       "fig:MEBT3SchematicsDev3" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img044.png",
                                        "width"      : r".6\textwidth",
                                        "height"     : "",
                                        "caption"    : r"Schematics of device 3.",
                                        "abbrev"     : "",
                                   },
          "fig:BeamDumpLocation" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img045.png",
                                        "width"      : r".7\textwidth",
                                        "height"     : "",
                                        "caption"    : r"Beam dump location.",
                                        "abbrev"     : "",
                                   },
    "fig:BeamDumpStoppingBlocks" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img046.png",
                                        "width"      : r".6\textwidth",
                                        "height"     : "",
                                        "caption"    : r"Beam dump made by assembly of stopping blocks.",
                                        "abbrev"     : "",
                                   },
        "fig:Spiral2BeamDumpCut" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img047.png",
                                        "width"      : r".9\textwidth",
                                        "height"     : "",
                                        "caption"    : r"Cut view of Spiral2 beam dump.",
                                        "abbrev"     : "",
                                   },
   "fig:BeamDumpWithWaterJacket" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img048.png",
                                        "width"      : r".6\textwidth",
                                        "height"     : "",
                                        "caption"    : r"Beam dump with a water jacket (from LPSC for IPHI).",
                                        "abbrev"     : "",
                                   },
      "fig:MEBT3BeamStopDesigns" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/Table4-4.png",
                                        "width"      : r".8\textwidth",
                                        "height"     : "",
                                        "caption"    : r"Beam stop design examples.",
                                        "abbrev"     : "",
                                   },
     "fig:MEBT3BeamStopDesigns2" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/Table4-5.png",
                                        "width"      : r".8\textwidth",
                                        "height"     : "",
                                        "caption"    : r"Beam stop design examples (continued).",
                                        "abbrev"     : "",
                                   },
"fig:S2StopBlocksBeforeAssembly" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img049.png",
                                        "width"      : r".4\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Parts of one of Spiral2's stopping blocks before assembly.\newline
One copper stopping block with machined external water channel (bottom), one copper channel closing tube with water input and output (top).""",
                                        "abbrev"     : r"Parts of one of Spiral2's stopping blocks before assembly.",
                                   },
          "fig:IPHIstoppingCone" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img050.png",
                                        "width"      : r".5\textwidth",
                                        "height"     : "",
                                        "caption"    : r"IPHI stopping cone in nickel (left) and tip detail (right).",
                                        "abbrev"     : "IPHI stopping cone.",
                                   },
              "fig:IPHIbeamDump" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img051.png",
                                        "width"      : r".5\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""IPHI beam dump.\newline
Beam dump on support (left) and cooling system (pump, exchanger{\dots}, right).""",
                                        "abbrev"     : "IPHI beam dump.",
                                   },
     "fig:IPHIbeamProtectionBox" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img052.png",
                                        "width"      : r".5\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""IPHI beam dump in its radiological protection box (lead and steel).""",
                                        "abbrev"     : "",
                                   },
         "fig:ReferenceLattices" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/D-MYRTE-2.6-Fig56.png",
                                        "width"      : r".8\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Reference lattices of the MYRRHA Linac (distances in mm).
Picture from~\cite{D-MYRTE-2.6}.""",
                                        "abbrev"     : "Reference lattices of the MYRRHA Linac.",
                                   },
     "fig:LinacLongitProperties" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/D-MYRTE-2.6-Fig57.png",
                                        "width"      : r".8\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Longitudinal properties of the MYRRHA main Linac.\newline
Zero-current phase advances per lattice \& per meter, accelerating voltages, synchronous phases, beam energy evolution
and required RF (beam loading) power per cavity. Picture from~\cite{D-MYRTE-2.6}.""",
                                        "abbrev"     : "Longitudinal properties of the MYRRHA main Linac.",
                                   },
"fig:BeamDynamics-MultiParticles": {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/D-MYRTE-2.6-Fig58.png",
                                        "width"      : r".82\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Beam dynamics through the MYRRHA main linac, multi-particle envelopes. Picture from~\cite{D-MYRTE-2.6}.""",
                                        "abbrev"     : "",
                                   },
       "fig:IObeamDistributions" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/D-MYRTE-2.6-Fig59.png",
                                        "width"      : r".9\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Input beam distribution used for the linac tuning and output beam distribution
and evolution of the RMS normalised emittances along the linac. Picture from~\cite{D-MYRTE-2.6}.""",
                                        "abbrev"     : "",
                                   },
 "fig:LinacTransverseProperties" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/D-MYRTE-2.6-Fig61.png",
                                        "width"      : r"\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Transverse properties of the MYRRHA main linac:
zero-current phase advance per meter (before and after inter-sections matching and longitudinal and
transverse planes crossing), tuning set-points in the Hofmann diagram (after matching), and consequence
on the quadrupoles gradients values. Picture from~\cite{D-MYRTE-2.6}.""",
                                        "abbrev"     : "Transverse properties of the MYRRHA main linac",
                                   },
"fig:LongitudinalAcceptance17MeV": {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img057.png",
                                        "width"      : r"\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Main Linac longitudinal acceptance and \SI{17}{\MeV} input beam.\newline
Left: transverse acceptance of the main Linac;\newline
Right: longitudinal acceptance (white area of the plot) and \SI{17}{\MeV} input beam.""",
                                        "abbrev"     : "Main Linac longitudinal acceptance and \SI{17}{\MeV} input beam.",
                                   },
"fig:LongitudinalAcceptance16.6MeV": {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/D-MYRTE-2.6-Fig62.png",
                                        "width"      : r"0.65\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Longitudinal acceptance (white area of the plot) and considered \SI{16.6}{\MeV} input beam distribution.""",
                                        "abbrev"     : "",
                                   },
"fig:AlternativeTransverseTunings":{
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img058.png",
                                        "width"      : r"\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Alternative transverse tunings for the MYRRHA main Linac.\newline
                                                       Up: ``Low focusing'' tuning; Bottom: ``Emittance mixing'' tuning.""",
                                        "abbrev"     : "Alternative transverse tunings.",
                                   },
         "fig:RealBeamTransport" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img059.png",
                                        "width"      : r"\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Transport of a ``real'' beam through the MYRRHA main Linac.\newline
                                                       Left: \SI{17}{\MeV} input from the preliminary injector\index{Linac!injector}
                                                       LORASR simulations of D2.1; Right: \SI{600}{\MeV} output.""",
                                        "abbrev"     : "Transport of a ``real'' beam through the MYRRHA main Linac.",
                                   },
         "fig:PMPmismatchedBeam" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/D-MYRTE-2.6-Fig64.png",
                                        "width"      : r"0.9\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Transport of a +/-/+ mismatched beam through the MYRRHA main linac
                                                       (+30\% in X, -30\% in Y, +30\% in Z). Picture from~\cite{D-MYRTE-2.6}.""",
                                        "abbrev"     : "Transport of a +/-/+ mismatched beam through the MYRRHA main linac",
                                   },
 "fig:Sensitivity2CurrentChange" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/D-MYRTE-2.6-Fig65.png",
                                        "width"      : r"0.8\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Sensitivity to current change. Emittances growth (difference between input and
                                                       output beam) in the superconducting linac as function of the beam current and
                                                       evolution of the emittance for a 0~mA and a 6~mA beam. Picture from~\cite{D-MYRTE-2.6}.""",
                                        "abbrev"     : "Sensitivity to current change.",
                                   },
   "fig:OutputBeamJitterDistrib" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img062.png",
                                        "width"      : r"\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Distribution of the output beam energy jitter.\newline
                                                       From 1000 envelope simulations, including random error distributions for RF cavities
                                                       fields and phases of respectively (from left to right): 0.1\%/ 0.1{\textdegree},
                                                       0.2\%/0.2{\textdegree}, 0.3\%/0.3{\textdegree}, and 0.4\%/0.4{\textdegree} RMS.""",
                                        "abbrev"     : "Distribution of the output beam energy jitter.",
                                   },
       "fig:ReTuningCompOptim18" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img063.png",
                                        "width"      : r".8\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Re-tuning strategy used for compensation optimisation as used in TraceWin code.
                                                       Case where spoke cryomodule \#18 is off-line.""",
                                        "abbrev"     : "Re-tuning strategy used for compensation optimisation as used in TraceWin code.",
                                   },
   "fig:SpokeCryo18Compensation" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img064.png",
                                        "width"      : r"\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Spoke cryomodule \#18 loss compensated by spoke section new tunings.\newline
                                                        New tunings in the Linac spoke section: blue plots.""",
                                        "abbrev"     : "Spoke cryomodule \#18 loss compensated by spoke section new tunings.",
                                   },
        "fig:SpokeCryo18OffLine" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img065.png",
                                        "width"      : r".8\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Main Linac beam dynamics with spoke cryomodule \#18 off line.\newline
                                                       Multi-particle envelopes and evolution of the RMS \& 99.99\% beam emittances.""",
                                        "abbrev"     : "Main Linac beam dynamics with spoke cryomodule \#18 off line.",
                                   },
           "fig:RFampPowerNeeds" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img066.png",
                                        "width"      : r"\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Preliminary assessment of the solid-state RF amplifier power needs.\newline
                                                       Left: re-tuning set-points for a few fault recovery test cases;
                                                       Right: preliminary assessments of the MYRRHA RF amplifier power needs,
                                                       including margins for RF control~\cite{S-MYRRHA-2012-Bouly}.""",
                                        "abbrev"     : "Preliminary assessment of the solid-state RF amplifier power needs.",
                                   },
   "fig:BeamLodingOnFailedSpoke" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img067.png",
                                        "width"      : r"0.95\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Effect of beam loading on a failed spoke cavity.\newline
                                                       If the cavity is still superconducting, the important criterion is the induced
                                                       decelerating voltage (to be lower below 0.5\% of nominal voltage); otherwise,
                                                       it is the dissipated power, especially for a quenched but still cold cavity.""",
                                        "abbrev"     : "Effect of beam loading on a failed spoke cavity.",
                                   },
       "fig:ReTuningCompOptim19" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img068.png",
                                        "width"      : r".8\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Re-tuning strategy used for compensation optimisation.\newline
                                                       As used in TraceWin code. Case where quadrupole doublet \#19 is off-line.""",
                                        "abbrev"     : "Re-tuning strategy used for compensation optimisation (Quadrupole doublet \#19 off-line)",
                                   },
        "fig:SpokeCryo19OffLine" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img069.png",
                                        "width"      : r"\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Main Linac beam dynamics with quadrupole \#19 off line.\newline
                                                       Multi-particle envelopes and evolution of the RMS \& 99.99\% beam emittances.""",
                                        "abbrev"     : "Main Linac beam dynamics with spoke cryomodule \#19 off line.",
                                   },
          "fig:600MeV-HEBTlines" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img070.png",
                                        "width"      : r"\textwidth",
                                        "height"     : "",
                                        "caption"    : r"Location of the \SI{600}{\MeV} HEBT lines.",
                                        "abbrev"     : "",
                                   },
             "fig:BeamFootprint" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img071.png",
                                        "width"      : r".6\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Beam footprint on the window target.\newline
                                             Left: nominal case; right: with errors.
                                             The \SI{99}{\percent} beam spot size (thin dark circle) is respectively
                                             \SI{605}{\square\mm} (left) and \SI{618}{\square\mm} (right).
                                             The thick dark circle represents the tube size at the window location.""",
                                        "abbrev"     : "Beam footprint on the window target.",
                                   },
      "fig:600MeVHEBT-Obj2Image" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img072.png",
                                        "width"      : r".8\textwidth",
                                        "height"     : "",
                                        "caption"    : r"The \SI{600}{\MeV} HEBT from Object to Image point (reactor window).",
                                        "abbrev"     : "",
                                   },
  "fig:TraceWin-DistrAtObjPoint" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img075.png",
                                        "width"      : r".8\textwidth",
                                        "height"     : "",
                                        "caption"    : r"TraceWin distributions at the Object point (105 macro-particles).",
                                        "abbrev"     : "",
                                   },
"fig:TransferMatrix-Obj2ImgPoint": {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img078.png",
                                        "width"      : r".8\textwidth",
                                        "height"     : "",
                                        "caption"    : r"Transfer matrix from Object to Image point (reactor window).",
                                        "abbrev"     : "",
                                   },
"fig:VertDispersion-Obj2ImgPoint": {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img079.png",
                                        "width"      : r".8\textwidth",
                                        "height"     : "",
                                        "caption"    : r"Vertical dispersion from Object to Image point (reactor window).",
                                        "abbrev"     : "",
                                   },
"fig:RMSemittances-Obj2ImgPoint" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img080.png",
                                        "width"      : r".7\textwidth",
                                        "height"     : "",
                                        "caption"    : r"RMS normalised emittances from Object to Image point (reactor window).",
                                        "abbrev"     : "",
                                   },
    "fig:1SigmaRMS-Obj2ImgPoint" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img081.png",
                                        "width"      : r".7\textwidth",
                                        "height"     : "",
                                        "caption"    : r"H and V $1\sigma$ RMS beam sizes from Object to Image points.",
                                        "abbrev"     : "",
                                   },
    "fig:5SigmaRMS-Obj2ImgPoint" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img082.png",
                                        "width"      : r".71\textwidth",
                                        "height"     : "",
                                        "caption"    : r"H and V $5\sigma$ RMS beam sizes from Object to Image points.",
                                        "abbrev"     : "",
                                   },
   "fig:TraceWinAtImgPoint105mp" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img083.png",
                                        "width"      : r".8\textwidth",
                                        "height"     : "",
                                        "caption"    : r"TraceWin output at the Image point with 105 macro-particles.",
                                        "abbrev"     : "",
                                   },
  "fig:MainSrcTerms-EngBarriers" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img084.png",
                                        "width"      : r".4\textwidth",
                                        "height"     : "",
                                        "caption"    : r"Main source terms (red) and engineered barriers (black).",
                                        "abbrev"     : "",
                                   },
   "fig:SafetyCompAlongBeamLine" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img085.png",
                                        "width"      : r".6\textwidth",
                                        "height"     : "",
                                        "caption"    : r"Reference layout of the break event safety elements.",
                                        "abbrev"     : "",
                                   },
      "fig:SecWindowRefGeometry" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img086.png",
                                        "width"      : r".4\textwidth",
                                        "height"     : "",
                                        "caption"    : r"Reference geometry of the radiatively-cooled secondary window.",
                                        "abbrev"     : "",
                                   },
 "fig:RMSscatterAngleEstimation" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img087.png",
                                        "width"      : r".5\textwidth",
                                        "height"     : "",
                                        "caption"    : r"Estimation of the RMS scattering angle.",
                                        "abbrev"     : "",
                                   },
      "fig:EmittanceThroughFoil" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img088.png",
                                        "width"      : r".4\textwidth",
                                        "height"     : "",
                                        "caption"    : r"Emittance before and after scattering through a foil.",
                                        "abbrev"     : "",
                                   },
  "fig:BeamTransvEnvelopes-noSW" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img089.png",
                                        "width"      : r".4\textwidth",
                                        "height"     : "",
                                        "caption"    : r"Beam transverse envelopes, no secondary window.",
                                        "abbrev"     : "",
                                   },
  "fig:BeamTransvEnvelopes-10SW" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img090.png",
                                        "width"      : r".4\textwidth",
                                        "height"     : "",
                                        "caption"    : r"Beam transverse envelopes, 10~$\mu$m-foil secondary window.",
                                        "abbrev"     : "",
                                   },
  "fig:BeamTransvEnvelopes-30SW" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img091.png",
                                        "width"      : r".4\textwidth",
                                        "height"     : "",
                                        "caption"    : r"Beam transverse envelopes, 30~$\mu$m-foil secondary window.",
                                        "abbrev"     : "",
                                   },
      "fig:NominalBeamEnvelopes" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img092.png",
                                        "width"      : r".9\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Nominal beam envelopes through the beam line to dump
                                                        (99\% envelopes @$3\sigma$).""",
                                        "abbrev"     : "",
                                   },
"fig:BeamLossesProfileAtDumpSurf": {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img093.png",
                                        "width"      : r".7\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Beam losses profile inside the vault at the dump surface.""",
                                        "abbrev"     : "",
                                   },
          "fig:PSI1.2MWBeamDump" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img094.png",
                                        "width"      : r".85\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""PSI 1.2 MW beam dump.""",
                                        "abbrev"     : "",
                                   },
     "fig:HiEnergyBeamDumpVault" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img095.png",
                                        "width"      : r".8\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""The high-energy full power beam dump vault.""",
                                        "abbrev"     : "",
                                   },
  "fig:CarbonTarget-Yields-Flux" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img096.png",
                                        "width"      : r".8\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Carbon target: neutron double
                                                  differential yields and neutron flux.\newline
                                                  Left: neutron double differential yields from the carbon target.
                                                  Right: neutron flux (n/cm${}^2$ per beam proton) in and around the target.""",
                                        "abbrev"     : "Carbon target: neutron double differential yields and neutron flux.",
                                   },
   "fig:NeutronDoubleDiffYields" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img097.png",
                                        "width"      : r".8\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""Neutron double differential yields.\newline
                                                       Left: from the stainless steel. Right: from the copper target.""",
                                        "abbrev"     : "Neutron double differential yields.",
                                   },
   "Hstar10ResidDoseRate-Copper" : {
                                        "class"      : "FIGURE",
                                        "location"   : "MYRRHATDRPartB-img/MYRRHATDRPartB-img098.png",
                                        "width"      : r".9\textwidth",
                                        "height"     : "",
                                        "caption"    : r"""H*(10) residual dose rate around the copper target\newline
                                                       (at different cooling times after a 24~h irradiation).""",
                                        "abbrev"     : r"H*(10) residual dose rate around the copper target.",
                                   },
        #}}}


        #{{{  Equations
        "eq:RadialForce"         : {
                                        "class"    : "EQUATION",
                                        "formula"  : r"""F_r=\frac{(1-\beta^2)}{\beta}\frac{\mathit{qI}}{2\pi\varepsilon_0c}\frac r{R^2}\ \ \ \ (\hbox{for }r<R)""",
                                        "where"    : r"""where $I$ is the beam current, $q$ the beam particles charge,
                                        $\varepsilon_0$ the vacuum permittivity, $r$ the radial position of the particle
                                        inside the beam, $R$ the beam radius, $c$ the celerity of light, and $\beta$
                                        the reduced velocity of the beam particles.""",
                                   },
        "eq:Multipactor"         : {
                                        "class"    : "EQUATION",
                                        "formula"  : r'P [\text{kW}] = (f [\text{MHz}] \times \text{Diam\_port} [\text{mm}])^4 \times Z [\text{Ohm}]',
                                        "where"    : "",
                                   },
        #}}}


        #{{{  Standard terms
        "term:Injector"          : {
                                        "class"      : "TERM",
                                        "name"       : "injector",
                                        "definition" : "The MYRRHA Injector comprises an initial part, the Front End, bringing the protons up to \SI{1.5}{\MeV}, and a final part, the Booster, which brings them up to \SI{16.6}{\MeV}",
                                        "ref"        : "term:Injector",
                                        "index"      : "injector",
                                   },
        #}}}
}
#}}}

# process returns data in the TeX objects dictionary, structured according to the class of the object associated with the input key
def process(key):
    if key in d:
        val=d[key]
        # if the object class is FIGURE, I return the LaTeX code to define a figure
        if val["class"] == "FIGURE":
            if val["abbrev"] == "":
                return "\\centering\\includegraphics[width={0}]{{{1}}}\n\\caption{{{2}}}\n\\label{{{3}}}".format(val["width"], val["location"], val["caption"], key)
            else:
                return "\\centering\\includegraphics[width={0}]{{{1}}}\n\\caption[{4}]{{{2}}}\n\\label{{{3}}}".format(val["width"], val["location"], val["caption"], key, val["abbrev"])

        # if the object class is EQUATION, I return the LaTeX code to render an equation
        # Nota bene: Math mode is assumed
        if val["class"] == "EQUATION":
            return "{0}\n\\label{{{1}}}".format(val["formula"], key)
            # return r"""{0}\n\\label{{{1}}}\n\noindent {2}\n""".format(val["formula"], key, val["where"])

        # if the object class is TERM, I return the standard definition of a term, possibly
        # inserting an entry in the table of indices
        if val["class"] == "TERM":
            if val["index"] == "":
                return val["name"]
            else:
                return "{0}\index{{{1}}}".format(val["name"], val["index"])
    return ""
