 

**Table:** Contributing projects. (Word "Alx" in the first column links
to the corresponding folder on Alexandria).

<table>
<caption>Table: Contributing projects. (Word “Alx” in the first column links to the corresponding folder on Alexandria).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Contributing projects</td>
<td>Status</td>
<td>(Co-)funding EC Framework Programmes (FPs)</td>
</tr>
<tr class="even">
<td>PDS/XADS (ID: FIKW-CT- 2001-00179)</td>
<td>Completed (2001–2004)</td>
<td>FP5 (1998–2002)</td>
</tr>
<tr class="odd">
<td>EUROTRANS (ID: 516520)</td>
<td>Completed (2005–2010)</td>
<td>FP6 (2002–2006)</td>
</tr>
<tr class="even">
<td>CDT/FASTEF (ID: 232527)</td>
<td>Completed (2009–2012)</td>
<td>FP7 (2007–2013)</td>
</tr>
<tr class="odd">
<td>MAX (ID: 269565) (<a href="http://ecm.sckcen.be/OTCS/llisapi.dll/open/5328035">Alx</a>)</td>
<td>Completed (2011–2014)</td>
<td>FP7 (2007–2013)</td>
</tr>
<tr class="even">
<td>MARISA (ID: 605318) (<a href="http://ecm.sckcen.be/OTCS/llisapi.dll/open/4213200">Alx</a>)</td>
<td>Completed (2013–2016)</td>
<td>FP7 (2007–2013)</td>
</tr>
<tr class="odd">
<td>MYRTE (ID: 662186) (<a href="http://ecm.sckcen.be/OTCS/llisapi.dll/open/5734589">Alx</a>)</td>
<td>Completed (2015–2019)</td>
<td>H2020 (2014–2020)</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Contributing projects. (Word "Alx" in the first column links to
the corresponding folder on Alexandria).

 

**Table:** Contributing projects. (Word "Alx" in the first column links
to the corresponding folder on Alexandria).

<table>
<caption>Table: Contributing projects. (Word “Alx” in the first column links to the corresponding folder on Alexandria).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Contributing projects</td>
<td>Status</td>
<td>(Co-)funding EC Framework Programmes (FPs)</td>
</tr>
<tr class="even">
<td>PDS/XADS (ID: FIKW-CT- 2001-00179)</td>
<td>Completed (2001–2004)</td>
<td>FP5 (1998–2002)</td>
</tr>
<tr class="odd">
<td>EUROTRANS (ID: 516520)</td>
<td>Completed (2005–2010)</td>
<td>FP6 (2002–2006)</td>
</tr>
<tr class="even">
<td>CDT/FASTEF (ID: 232527)</td>
<td>Completed (2009–2012)</td>
<td>FP7 (2007–2013)</td>
</tr>
<tr class="odd">
<td>MAX (ID: 269565) (<a href="http://ecm.sckcen.be/OTCS/llisapi.dll/open/5328035">Alx</a>)</td>
<td>Completed (2011–2014)</td>
<td>FP7 (2007–2013)</td>
</tr>
<tr class="even">
<td>MARISA (ID: 605318) (<a href="http://ecm.sckcen.be/OTCS/llisapi.dll/open/4213200">Alx</a>)</td>
<td>Completed (2013–2016)</td>
<td>FP7 (2007–2013)</td>
</tr>
<tr class="odd">
<td>MYRTE (ID: 662186) (<a href="http://ecm.sckcen.be/OTCS/llisapi.dll/open/5734589">Alx</a>)</td>
<td>Completed (2015–2019)</td>
<td>H2020 (2014–2020)</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Contributing projects. (Word "Alx" in the first column links to
the corresponding folder on Alexandria).

 

**Table:** Contributing projects. (Word "Alx" in the first column links
to the corresponding folder on Alexandria).

<table>
<caption>Table: Contributing projects. (Word “Alx” in the first column links to the corresponding folder on Alexandria).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Contributing projects</td>
<td>Status</td>
<td>(Co-)funding EC Framework Programmes (FPs)</td>
</tr>
<tr class="even">
<td>PDS/XADS (ID: FIKW-CT- 2001-00179)</td>
<td>Completed (2001–2004)</td>
<td>FP5 (1998–2002)</td>
</tr>
<tr class="odd">
<td>EUROTRANS (ID: 516520)</td>
<td>Completed (2005–2010)</td>
<td>FP6 (2002–2006)</td>
</tr>
<tr class="even">
<td>CDT/FASTEF (ID: 232527)</td>
<td>Completed (2009–2012)</td>
<td>FP7 (2007–2013)</td>
</tr>
<tr class="odd">
<td>MAX (ID: 269565) (<a href="http://ecm.sckcen.be/OTCS/llisapi.dll/open/5328035">Alx</a>)</td>
<td>Completed (2011–2014)</td>
<td>FP7 (2007–2013)</td>
</tr>
<tr class="even">
<td>MARISA (ID: 605318) (<a href="http://ecm.sckcen.be/OTCS/llisapi.dll/open/4213200">Alx</a>)</td>
<td>Completed (2013–2016)</td>
<td>FP7 (2007–2013)</td>
</tr>
<tr class="odd">
<td>MYRTE (ID: 662186) (<a href="http://ecm.sckcen.be/OTCS/llisapi.dll/open/5734589">Alx</a>)</td>
<td>Completed (2015–2019)</td>
<td>H2020 (2014–2020)</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Contributing projects. (Word "Alx" in the first column links to
the corresponding folder on Alexandria).

 

**Table:** Contributing projects. (Word "Alx" in the first column links
to the corresponding folder on Alexandria).

<table>
<caption>Table: Contributing projects. (Word “Alx” in the first column links to the corresponding folder on Alexandria).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Contributing projects</td>
<td>Status</td>
<td>(Co-)funding EC Framework Programmes (FPs)</td>
</tr>
<tr class="even">
<td>PDS/XADS (ID: FIKW-CT- 2001-00179)</td>
<td>Completed (2001–2004)</td>
<td>FP5 (1998–2002)</td>
</tr>
<tr class="odd">
<td>EUROTRANS (ID: 516520)</td>
<td>Completed (2005–2010)</td>
<td>FP6 (2002–2006)</td>
</tr>
<tr class="even">
<td>CDT/FASTEF (ID: 232527)</td>
<td>Completed (2009–2012)</td>
<td>FP7 (2007–2013)</td>
</tr>
<tr class="odd">
<td>MAX (ID: 269565) (<a href="http://ecm.sckcen.be/OTCS/llisapi.dll/open/5328035">Alx</a>)</td>
<td>Completed (2011–2014)</td>
<td>FP7 (2007–2013)</td>
</tr>
<tr class="even">
<td>MARISA (ID: 605318) (<a href="http://ecm.sckcen.be/OTCS/llisapi.dll/open/4213200">Alx</a>)</td>
<td>Completed (2013–2016)</td>
<td>FP7 (2007–2013)</td>
</tr>
<tr class="odd">
<td>MYRTE (ID: 662186) (<a href="http://ecm.sckcen.be/OTCS/llisapi.dll/open/5734589">Alx</a>)</td>
<td>Completed (2015–2019)</td>
<td>H2020 (2014–2020)</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Contributing projects. (Word "Alx" in the first column links to
the corresponding folder on Alexandria).

 

**Table:** European transmuters general specifications.

<table>
<caption>Table: European transmuters general specifications.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Transmuter demonstrator (XT-ADS/MYRRHA)</td>
<td>Industrial transmuter (EFIT)</td>
</tr>
<tr class="even">
<td>Power: 50–100 MW<sub>th</sub></td>
<td>Power: several 100MW<sub>th</sub></td>
</tr>
<tr class="odd">
<td><em>K<sub>eff</sub></em> value: ≈0.95</td>
<td><em>K<sub>eff</sub></em> value: ≈0.97</td>
</tr>
<tr class="even">
<td>Fuel: highly-enriched MOX</td>
<td>Fuel: Minor Actinide fuel</td>
</tr>
<tr class="odd">
<td>Coolant &amp; target: Pb-Bi eutectic</td>
<td>Coolant &amp; target: Pb</td>
</tr>
<tr class="even">
<td>Beam: CW 4mA 600MeV protons</td>
<td>Beam: CW 20mA 800MeV protons</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: European transmuters general specifications.

 

**Table:** European transmuters general specifications.

<table>
<caption>Table: European transmuters general specifications.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Transmuter demonstrator (XT-ADS/MYRRHA)</td>
<td>Industrial transmuter (EFIT)</td>
</tr>
<tr class="even">
<td>Power: 50–100 MW<sub>th</sub></td>
<td>Power: several 100MW<sub>th</sub></td>
</tr>
<tr class="odd">
<td><em>K<sub>eff</sub></em> value: ≈0.95</td>
<td><em>K<sub>eff</sub></em> value: ≈0.97</td>
</tr>
<tr class="even">
<td>Fuel: highly-enriched MOX</td>
<td>Fuel: Minor Actinide fuel</td>
</tr>
<tr class="odd">
<td>Coolant &amp; target: Pb-Bi eutectic</td>
<td>Coolant &amp; target: Pb</td>
</tr>
<tr class="even">
<td>Beam: CW 4mA 600MeV protons</td>
<td>Beam: CW 20mA 800MeV protons</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: European transmuters general specifications.

 

**Table:** European transmuters general specifications.

<table>
<caption>Table: European transmuters general specifications.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Transmuter demonstrator (XT-ADS/MYRRHA)</td>
<td>Industrial transmuter (EFIT)</td>
</tr>
<tr class="even">
<td>Power: 50–100 MW<sub>th</sub></td>
<td>Power: several 100MW<sub>th</sub></td>
</tr>
<tr class="odd">
<td><em>K<sub>eff</sub></em> value: ≈0.95</td>
<td><em>K<sub>eff</sub></em> value: ≈0.97</td>
</tr>
<tr class="even">
<td>Fuel: highly-enriched MOX</td>
<td>Fuel: Minor Actinide fuel</td>
</tr>
<tr class="odd">
<td>Coolant &amp; target: Pb-Bi eutectic</td>
<td>Coolant &amp; target: Pb</td>
</tr>
<tr class="even">
<td>Beam: CW 4mA 600MeV protons</td>
<td>Beam: CW 20mA 800MeV protons</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: European transmuters general specifications.

 

**Table:** European transmuters general specifications.

<table>
<caption>Table: European transmuters general specifications.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Transmuter demonstrator (XT-ADS/MYRRHA)</td>
<td>Industrial transmuter (EFIT)</td>
</tr>
<tr class="even">
<td>Power: 50–100 MW<sub>th</sub></td>
<td>Power: several 100MW<sub>th</sub></td>
</tr>
<tr class="odd">
<td><em>K<sub>eff</sub></em> value: ≈0.95</td>
<td><em>K<sub>eff</sub></em> value: ≈0.97</td>
</tr>
<tr class="even">
<td>Fuel: highly-enriched MOX</td>
<td>Fuel: Minor Actinide fuel</td>
</tr>
<tr class="odd">
<td>Coolant &amp; target: Pb-Bi eutectic</td>
<td>Coolant &amp; target: Pb</td>
</tr>
<tr class="even">
<td>Beam: CW 4mA 600MeV protons</td>
<td>Beam: CW 20mA 800MeV protons</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: European transmuters general specifications.

 

**Table:** MYRRHA accelerator main beam requirements. τ represents here
the beam trip duration. MTBF \[[1](t.html#R-SCKCEN-2020-39795792)\] here
should be intended as "beam MTBF" and refer to the linac actually
delivering its nominal beam.

<table>
<caption>Table: MYRRHA accelerator main beam requirements. τ represents here the beam trip duration. MTBF [1] here should be intended as “beam MTBF” and refer to the linac actually delivering its nominal beam.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Particle</td>
<td> </td>
<td>Protons</td>
</tr>
<tr class="even">
<td>Beam energy ± accuracy</td>
<td> </td>
<td>600MeV ± 1%</td>
</tr>
<tr class="odd">
<td>Nominal instantaneous beam current ± accuracy</td>
<td> </td>
<td>4mA ± 2%</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>Beam power stability</td>
<td> </td>
<td>±1% (100 ms integration time)</td>
</tr>
<tr class="odd">
<td>Beam trip tolerance</td>
<td>MTBF<sub>τ&lt;0.1s</sub></td>
<td>&gt; 10s</td>
</tr>
<tr class="even">
<td> </td>
<td>MTBF<sub>0.1s&lt;τ&lt;3s</sub></td>
<td>&gt; 900s</td>
</tr>
<tr class="odd">
<td> </td>
<td>MTBF<sub>τ&gt;3s</sub></td>
<td>&gt; 250h</td>
</tr>
<tr class="even">
<td>Time structure</td>
<td> </td>
<td>CW with PWM at 250Hz maximum</td>
</tr>
<tr class="odd">
<td>Beam duty factor</td>
<td> </td>
<td>10<sup>-4</sup> to 1</td>
</tr>
<tr class="even">
<td>Minimal pulse duration</td>
<td> </td>
<td>≥ 100μs</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: MYRRHA accelerator main beam requirements. τ represents here the
beam trip duration. MTBF \[1\] here should be intended as "beam MTBF"
and refer to the linac actually delivering its nominal beam.

 

**Table:** MYRRHA accelerator main beam requirements. τ represents here
the beam trip duration. MTBF \[[1](t.html#R-SCKCEN-2020-39795792)\] here
should be intended as "beam MTBF" and refer to the linac actually
delivering its nominal beam.

<table>
<caption>Table: MYRRHA accelerator main beam requirements. τ represents here the beam trip duration. MTBF [1] here should be intended as “beam MTBF” and refer to the linac actually delivering its nominal beam.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Particle</td>
<td> </td>
<td>Protons</td>
</tr>
<tr class="even">
<td>Beam energy ± accuracy</td>
<td> </td>
<td>600MeV ± 1%</td>
</tr>
<tr class="odd">
<td>Nominal instantaneous beam current ± accuracy</td>
<td> </td>
<td>4mA ± 2%</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>Beam power stability</td>
<td> </td>
<td>±1% (100 ms integration time)</td>
</tr>
<tr class="odd">
<td>Beam trip tolerance</td>
<td>MTBF<sub>τ&lt;0.1s</sub></td>
<td>&gt; 10s</td>
</tr>
<tr class="even">
<td> </td>
<td>MTBF<sub>0.1s&lt;τ&lt;3s</sub></td>
<td>&gt; 900s</td>
</tr>
<tr class="odd">
<td> </td>
<td>MTBF<sub>τ&gt;3s</sub></td>
<td>&gt; 250h</td>
</tr>
<tr class="even">
<td>Time structure</td>
<td> </td>
<td>CW with PWM at 250Hz maximum</td>
</tr>
<tr class="odd">
<td>Beam duty factor</td>
<td> </td>
<td>10<sup>-4</sup> to 1</td>
</tr>
<tr class="even">
<td>Minimal pulse duration</td>
<td> </td>
<td>≥ 100μs</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: MYRRHA accelerator main beam requirements. τ represents here the
beam trip duration. MTBF \[1\] here should be intended as "beam MTBF"
and refer to the linac actually delivering its nominal beam.

 

**Table:** MYRRHA accelerator main beam requirements. τ represents here
the beam trip duration. MTBF \[[1](t.html#R-SCKCEN-2020-39795792)\] here
should be intended as "beam MTBF" and refer to the linac actually
delivering its nominal beam.

<table>
<caption>Table: MYRRHA accelerator main beam requirements. τ represents here the beam trip duration. MTBF [1] here should be intended as “beam MTBF” and refer to the linac actually delivering its nominal beam.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Particle</td>
<td> </td>
<td>Protons</td>
</tr>
<tr class="even">
<td>Beam energy ± accuracy</td>
<td> </td>
<td>600MeV ± 1%</td>
</tr>
<tr class="odd">
<td>Nominal instantaneous beam current ± accuracy</td>
<td> </td>
<td>4mA ± 2%</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>Beam power stability</td>
<td> </td>
<td>±1% (100 ms integration time)</td>
</tr>
<tr class="odd">
<td>Beam trip tolerance</td>
<td>MTBF<sub>τ&lt;0.1s</sub></td>
<td>&gt; 10s</td>
</tr>
<tr class="even">
<td> </td>
<td>MTBF<sub>0.1s&lt;τ&lt;3s</sub></td>
<td>&gt; 900s</td>
</tr>
<tr class="odd">
<td> </td>
<td>MTBF<sub>τ&gt;3s</sub></td>
<td>&gt; 250h</td>
</tr>
<tr class="even">
<td>Time structure</td>
<td> </td>
<td>CW with PWM at 250Hz maximum</td>
</tr>
<tr class="odd">
<td>Beam duty factor</td>
<td> </td>
<td>10<sup>-4</sup> to 1</td>
</tr>
<tr class="even">
<td>Minimal pulse duration</td>
<td> </td>
<td>≥ 100μs</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: MYRRHA accelerator main beam requirements. τ represents here the
beam trip duration. MTBF \[1\] here should be intended as "beam MTBF"
and refer to the linac actually delivering its nominal beam.

 

**Table:** MYRRHA accelerator main beam requirements. τ represents here
the beam trip duration. MTBF \[[1](t.html#R-SCKCEN-2020-39795792)\] here
should be intended as "beam MTBF" and refer to the linac actually
delivering its nominal beam.

<table>
<caption>Table: MYRRHA accelerator main beam requirements. τ represents here the beam trip duration. MTBF [1] here should be intended as “beam MTBF” and refer to the linac actually delivering its nominal beam.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Particle</td>
<td> </td>
<td>Protons</td>
</tr>
<tr class="even">
<td>Beam energy ± accuracy</td>
<td> </td>
<td>600MeV ± 1%</td>
</tr>
<tr class="odd">
<td>Nominal instantaneous beam current ± accuracy</td>
<td> </td>
<td>4mA ± 2%</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>Beam power stability</td>
<td> </td>
<td>±1% (100 ms integration time)</td>
</tr>
<tr class="odd">
<td>Beam trip tolerance</td>
<td>MTBF<sub>τ&lt;0.1s</sub></td>
<td>&gt; 10s</td>
</tr>
<tr class="even">
<td> </td>
<td>MTBF<sub>0.1s&lt;τ&lt;3s</sub></td>
<td>&gt; 900s</td>
</tr>
<tr class="odd">
<td> </td>
<td>MTBF<sub>τ&gt;3s</sub></td>
<td>&gt; 250h</td>
</tr>
<tr class="even">
<td>Time structure</td>
<td> </td>
<td>CW with PWM at 250Hz maximum</td>
</tr>
<tr class="odd">
<td>Beam duty factor</td>
<td> </td>
<td>10<sup>-4</sup> to 1</td>
</tr>
<tr class="even">
<td>Minimal pulse duration</td>
<td> </td>
<td>≥ 100μs</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: MYRRHA accelerator main beam requirements. τ represents here the
beam trip duration. MTBF \[1\] here should be intended as "beam MTBF"
and refer to the linac actually delivering its nominal beam.

 

**Table:** MINERVA accelerator main beam
requirements \[[2](t.html#R-SCKCEN-2020-37685655.0.13)\].

<table>
<caption>Table: MINERVA accelerator main beam requirements [2].</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Particle</td>
<td> </td>
<td>Protons</td>
</tr>
<tr class="even">
<td>Beam energy ± accuracy</td>
<td> </td>
<td>100MeV ±1%</td>
</tr>
<tr class="odd">
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>Nominal instantaneous beam current ± accuracy</td>
<td> </td>
<td>4mA ± 2%</td>
</tr>
<tr class="odd">
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>Beam power stability</td>
<td> </td>
<td>±1% (100 ms integration time)</td>
</tr>
<tr class="odd">
<td>Beam trip tolerance</td>
<td> </td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Time structure</td>
<td> </td>
<td>CW with PWM at 250Hz maximum</td>
</tr>
<tr class="odd">
<td>Beam duty factor</td>
<td> </td>
<td>10<sup>-4</sup> to 1</td>
</tr>
<tr class="even">
<td>Duty cycle on target</td>
<td>PTF</td>
<td>up to 12.5% of the full beam (50 kW)</td>
</tr>
<tr class="odd">
<td> </td>
<td>FPBD</td>
<td>up to 100% of the full beam (400 kW)</td>
</tr>
<tr class="even">
<td> </td>
<td>FTS</td>
<td>up to 100% of the full beam (400 kW)</td>
</tr>
<tr class="odd">
<td> </td>
<td>ETBD</td>
<td>TBD (expected: ≈1 kW)</td>
</tr>
<tr class="even">
<td>Minimal pulse duration</td>
<td> </td>
<td>≥ 10μs</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: MINERVA accelerator main beam requirements \[2\].

 

**Table:** MINERVA accelerator main beam
requirements \[[2](t.html#R-SCKCEN-2020-37685655.0.13)\].

<table>
<caption>Table: MINERVA accelerator main beam requirements [2].</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Particle</td>
<td> </td>
<td>Protons</td>
</tr>
<tr class="even">
<td>Beam energy ± accuracy</td>
<td> </td>
<td>100MeV ±1%</td>
</tr>
<tr class="odd">
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>Nominal instantaneous beam current ± accuracy</td>
<td> </td>
<td>4mA ± 2%</td>
</tr>
<tr class="odd">
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>Beam power stability</td>
<td> </td>
<td>±1% (100 ms integration time)</td>
</tr>
<tr class="odd">
<td>Beam trip tolerance</td>
<td> </td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Time structure</td>
<td> </td>
<td>CW with PWM at 250Hz maximum</td>
</tr>
<tr class="odd">
<td>Beam duty factor</td>
<td> </td>
<td>10<sup>-4</sup> to 1</td>
</tr>
<tr class="even">
<td>Duty cycle on target</td>
<td>PTF</td>
<td>up to 12.5% of the full beam (50 kW)</td>
</tr>
<tr class="odd">
<td> </td>
<td>FPBD</td>
<td>up to 100% of the full beam (400 kW)</td>
</tr>
<tr class="even">
<td> </td>
<td>FTS</td>
<td>up to 100% of the full beam (400 kW)</td>
</tr>
<tr class="odd">
<td> </td>
<td>ETBD</td>
<td>TBD (expected: ≈1 kW)</td>
</tr>
<tr class="even">
<td>Minimal pulse duration</td>
<td> </td>
<td>≥ 10μs</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: MINERVA accelerator main beam requirements \[2\].

 

**Table:** MINERVA accelerator main beam
requirements \[[2](t.html#R-SCKCEN-2020-37685655.0.13)\].

<table>
<caption>Table: MINERVA accelerator main beam requirements [2].</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Particle</td>
<td> </td>
<td>Protons</td>
</tr>
<tr class="even">
<td>Beam energy ± accuracy</td>
<td> </td>
<td>100MeV ±1%</td>
</tr>
<tr class="odd">
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>Nominal instantaneous beam current ± accuracy</td>
<td> </td>
<td>4mA ± 2%</td>
</tr>
<tr class="odd">
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>Beam power stability</td>
<td> </td>
<td>±1% (100 ms integration time)</td>
</tr>
<tr class="odd">
<td>Beam trip tolerance</td>
<td> </td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Time structure</td>
<td> </td>
<td>CW with PWM at 250Hz maximum</td>
</tr>
<tr class="odd">
<td>Beam duty factor</td>
<td> </td>
<td>10<sup>-4</sup> to 1</td>
</tr>
<tr class="even">
<td>Duty cycle on target</td>
<td>PTF</td>
<td>up to 12.5% of the full beam (50 kW)</td>
</tr>
<tr class="odd">
<td> </td>
<td>FPBD</td>
<td>up to 100% of the full beam (400 kW)</td>
</tr>
<tr class="even">
<td> </td>
<td>FTS</td>
<td>up to 100% of the full beam (400 kW)</td>
</tr>
<tr class="odd">
<td> </td>
<td>ETBD</td>
<td>TBD (expected: ≈1 kW)</td>
</tr>
<tr class="even">
<td>Minimal pulse duration</td>
<td> </td>
<td>≥ 10μs</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: MINERVA accelerator main beam requirements \[2\].

 

**Table:** MINERVA accelerator main beam
requirements \[[2](t.html#R-SCKCEN-2020-37685655.0.13)\].

<table>
<caption>Table: MINERVA accelerator main beam requirements [2].</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Particle</td>
<td> </td>
<td>Protons</td>
</tr>
<tr class="even">
<td>Beam energy ± accuracy</td>
<td> </td>
<td>100MeV ±1%</td>
</tr>
<tr class="odd">
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>Nominal instantaneous beam current ± accuracy</td>
<td> </td>
<td>4mA ± 2%</td>
</tr>
<tr class="odd">
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>Beam power stability</td>
<td> </td>
<td>±1% (100 ms integration time)</td>
</tr>
<tr class="odd">
<td>Beam trip tolerance</td>
<td> </td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Time structure</td>
<td> </td>
<td>CW with PWM at 250Hz maximum</td>
</tr>
<tr class="odd">
<td>Beam duty factor</td>
<td> </td>
<td>10<sup>-4</sup> to 1</td>
</tr>
<tr class="even">
<td>Duty cycle on target</td>
<td>PTF</td>
<td>up to 12.5% of the full beam (50 kW)</td>
</tr>
<tr class="odd">
<td> </td>
<td>FPBD</td>
<td>up to 100% of the full beam (400 kW)</td>
</tr>
<tr class="even">
<td> </td>
<td>FTS</td>
<td>up to 100% of the full beam (400 kW)</td>
</tr>
<tr class="odd">
<td> </td>
<td>ETBD</td>
<td>TBD (expected: ≈1 kW)</td>
</tr>
<tr class="even">
<td>Minimal pulse duration</td>
<td> </td>
<td>≥ 10μs</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: MINERVA accelerator main beam requirements \[2\].

 

**Table:** Error magnitude for quadrupoles and cavities applied to this
section. Beside the tolerance on cavity displacement that is different
in case of RT cavities, all the other tolerances are the same as the one
considered in MAX \[[3](t.html#D-MAX-1.4)\].

<table>
<caption>Table: Error magnitude for quadrupoles and cavities applied to this section. Beside the tolerance on cavity displacement that is different in case of RT cavities, all the other tolerances are the same as the one considered in MAX [3].</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td colspan="3">Quadrupole</td>
<td colspan="3">Cavity</td>
</tr>
<tr class="even">
<td>Errors</td>
<td>Corrected</td>
<td>Uncorrected</td>
<td>Errors</td>
<td>Corrected</td>
<td>Uncorrected</td>
</tr>
<tr class="odd">
<td>Gradient (%)</td>
<td>± 1</td>
<td>± 0.1</td>
<td>RF Field amplitude (%)</td>
<td>± 1</td>
<td>± 0.1</td>
</tr>
<tr class="even">
<td>Displacement (mm)</td>
<td>± 0.2</td>
<td>± 0.01</td>
<td>RF field phase (deg.)</td>
<td>± 1</td>
<td>± 0.1</td>
</tr>
<tr class="odd">
<td>Rotation (OX, OY) (deg.)</td>
<td>± 0.2</td>
<td>± 0.01</td>
<td>Displacement (mm)</td>
<td>± 0.5</td>
<td>± 0.01</td>
</tr>
<tr class="even">
<td>Rotation (OS) (deg.)</td>
<td>± 0.2</td>
<td>± 0.01</td>
<td>Rotation (OX, OY) (deg.)</td>
<td>± 0.1</td>
<td>± 0.01</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Error magnitude for quadrupoles and cavities applied to this
section. Beside the tolerance on cavity displacement that is different
in case of RT cavities, all the other tolerances are the same as the one
considered in MAX \[3\].

 

**Table:** Error magnitude for quadrupoles and cavities applied to this
section. Beside the tolerance on cavity displacement that is different
in case of RT cavities, all the other tolerances are the same as the one
considered in MAX \[[3](t.html#D-MAX-1.4)\].

<table>
<caption>Table: Error magnitude for quadrupoles and cavities applied to this section. Beside the tolerance on cavity displacement that is different in case of RT cavities, all the other tolerances are the same as the one considered in MAX [3].</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td colspan="3">Quadrupole</td>
<td colspan="3">Cavity</td>
</tr>
<tr class="even">
<td>Errors</td>
<td>Corrected</td>
<td>Uncorrected</td>
<td>Errors</td>
<td>Corrected</td>
<td>Uncorrected</td>
</tr>
<tr class="odd">
<td>Gradient (%)</td>
<td>± 1</td>
<td>± 0.1</td>
<td>RF Field amplitude (%)</td>
<td>± 1</td>
<td>± 0.1</td>
</tr>
<tr class="even">
<td>Displacement (mm)</td>
<td>± 0.2</td>
<td>± 0.01</td>
<td>RF field phase (deg.)</td>
<td>± 1</td>
<td>± 0.1</td>
</tr>
<tr class="odd">
<td>Rotation (OX, OY) (deg.)</td>
<td>± 0.2</td>
<td>± 0.01</td>
<td>Displacement (mm)</td>
<td>± 0.5</td>
<td>± 0.01</td>
</tr>
<tr class="even">
<td>Rotation (OS) (deg.)</td>
<td>± 0.2</td>
<td>± 0.01</td>
<td>Rotation (OX, OY) (deg.)</td>
<td>± 0.1</td>
<td>± 0.01</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Error magnitude for quadrupoles and cavities applied to this
section. Beside the tolerance on cavity displacement that is different
in case of RT cavities, all the other tolerances are the same as the one
considered in MAX \[3\].

 

**Table:** Error magnitude for quadrupoles and cavities applied to this
section. Beside the tolerance on cavity displacement that is different
in case of RT cavities, all the other tolerances are the same as the one
considered in MAX \[[3](t.html#D-MAX-1.4)\].

<table>
<caption>Table: Error magnitude for quadrupoles and cavities applied to this section. Beside the tolerance on cavity displacement that is different in case of RT cavities, all the other tolerances are the same as the one considered in MAX [3].</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td colspan="3">Quadrupole</td>
<td colspan="3">Cavity</td>
</tr>
<tr class="even">
<td>Errors</td>
<td>Corrected</td>
<td>Uncorrected</td>
<td>Errors</td>
<td>Corrected</td>
<td>Uncorrected</td>
</tr>
<tr class="odd">
<td>Gradient (%)</td>
<td>± 1</td>
<td>± 0.1</td>
<td>RF Field amplitude (%)</td>
<td>± 1</td>
<td>± 0.1</td>
</tr>
<tr class="even">
<td>Displacement (mm)</td>
<td>± 0.2</td>
<td>± 0.01</td>
<td>RF field phase (deg.)</td>
<td>± 1</td>
<td>± 0.1</td>
</tr>
<tr class="odd">
<td>Rotation (OX, OY) (deg.)</td>
<td>± 0.2</td>
<td>± 0.01</td>
<td>Displacement (mm)</td>
<td>± 0.5</td>
<td>± 0.01</td>
</tr>
<tr class="even">
<td>Rotation (OS) (deg.)</td>
<td>± 0.2</td>
<td>± 0.01</td>
<td>Rotation (OX, OY) (deg.)</td>
<td>± 0.1</td>
<td>± 0.01</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Error magnitude for quadrupoles and cavities applied to this
section. Beside the tolerance on cavity displacement that is different
in case of RT cavities, all the other tolerances are the same as the one
considered in MAX \[3\].

 

**Table:** Error magnitude for quadrupoles and cavities applied to this
section. Beside the tolerance on cavity displacement that is different
in case of RT cavities, all the other tolerances are the same as the one
considered in MAX \[[3](t.html#D-MAX-1.4)\].

<table>
<caption>Table: Error magnitude for quadrupoles and cavities applied to this section. Beside the tolerance on cavity displacement that is different in case of RT cavities, all the other tolerances are the same as the one considered in MAX [3].</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td colspan="3">Quadrupole</td>
<td colspan="3">Cavity</td>
</tr>
<tr class="even">
<td>Errors</td>
<td>Corrected</td>
<td>Uncorrected</td>
<td>Errors</td>
<td>Corrected</td>
<td>Uncorrected</td>
</tr>
<tr class="odd">
<td>Gradient (%)</td>
<td>± 1</td>
<td>± 0.1</td>
<td>RF Field amplitude (%)</td>
<td>± 1</td>
<td>± 0.1</td>
</tr>
<tr class="even">
<td>Displacement (mm)</td>
<td>± 0.2</td>
<td>± 0.01</td>
<td>RF field phase (deg.)</td>
<td>± 1</td>
<td>± 0.1</td>
</tr>
<tr class="odd">
<td>Rotation (OX, OY) (deg.)</td>
<td>± 0.2</td>
<td>± 0.01</td>
<td>Displacement (mm)</td>
<td>± 0.5</td>
<td>± 0.01</td>
</tr>
<tr class="even">
<td>Rotation (OS) (deg.)</td>
<td>± 0.2</td>
<td>± 0.01</td>
<td>Rotation (OX, OY) (deg.)</td>
<td>± 0.1</td>
<td>± 0.01</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Error magnitude for quadrupoles and cavities applied to this
section. Beside the tolerance on cavity displacement that is different
in case of RT cavities, all the other tolerances are the same as the one
considered in MAX \[3\].

  

**Table:** The differences in the simulation results using Design #1 and
Design #2 are listed. The main difference is the larger maximal vertical
envelope in the case of Design #2 due to the lack of a vertical focusing
effect in the case of straight pole faces. The results for the emittance
growth follow the trends shown in
Fig. [\[\*\]](#fig:MEBT3ChangeOfEmittance). Since all the results
presented here were obtained for tuning optimisations at 4mA beam
current, there are large mismatches at lower current values.

|                                                                                                                                 |
|---------------------------------------------------------------------------------------------------------------------------------|
| ![Table 5, from D-MYRTE-2.6, rendered as an image](media/rId68.png){width="3.9130424321959754in" height="1.3745811461067368in"} |

Table: The differences in the simulation results using Design #1 and
Design #2 are listed. The main difference is the larger maximal vertical
envelope in the case of Design #2 due to the lack of a vertical focusing
effect in the case of straight pole faces. The results for the emittance
growth follow the trends shown in Fig. \[\*\]. Since all the results
presented here were obtained for tuning optimisations at 4mA beam
current, there are large mismatches at lower current values.

  

**Table:** The differences in the simulation results using Design #1 and
Design #2 are listed. The main difference is the larger maximal vertical
envelope in the case of Design #2 due to the lack of a vertical focusing
effect in the case of straight pole faces. The results for the emittance
growth follow the trends shown in
Fig. [\[\*\]](#fig:MEBT3ChangeOfEmittance). Since all the results
presented here were obtained for tuning optimisations at 4mA beam
current, there are large mismatches at lower current values.

|                                                                                                                                 |
|---------------------------------------------------------------------------------------------------------------------------------|
| ![Table 5, from D-MYRTE-2.6, rendered as an image](media/rId68.png){width="3.9130424321959754in" height="1.3745811461067368in"} |

Table: The differences in the simulation results using Design #1 and
Design #2 are listed. The main difference is the larger maximal vertical
envelope in the case of Design #2 due to the lack of a vertical focusing
effect in the case of straight pole faces. The results for the emittance
growth follow the trends shown in Fig. \[\*\]. Since all the results
presented here were obtained for tuning optimisations at 4mA beam
current, there are large mismatches at lower current values.

  

**Table:** The differences in the simulation results using Design #1 and
Design #2 are listed. The main difference is the larger maximal vertical
envelope in the case of Design #2 due to the lack of a vertical focusing
effect in the case of straight pole faces. The results for the emittance
growth follow the trends shown in
Fig. [\[\*\]](#fig:MEBT3ChangeOfEmittance). Since all the results
presented here were obtained for tuning optimisations at 4mA beam
current, there are large mismatches at lower current values.

|                                                                                                                                 |
|---------------------------------------------------------------------------------------------------------------------------------|
| ![Table 5, from D-MYRTE-2.6, rendered as an image](media/rId68.png){width="3.9130424321959754in" height="1.3745811461067368in"} |

Table: The differences in the simulation results using Design #1 and
Design #2 are listed. The main difference is the larger maximal vertical
envelope in the case of Design #2 due to the lack of a vertical focusing
effect in the case of straight pole faces. The results for the emittance
growth follow the trends shown in Fig. \[\*\]. Since all the results
presented here were obtained for tuning optimisations at 4mA beam
current, there are large mismatches at lower current values.

  

**Table:** The differences in the simulation results using Design #1 and
Design #2 are listed. The main difference is the larger maximal vertical
envelope in the case of Design #2 due to the lack of a vertical focusing
effect in the case of straight pole faces. The results for the emittance
growth follow the trends shown in
Fig. [\[\*\]](#fig:MEBT3ChangeOfEmittance). Since all the results
presented here were obtained for tuning optimisations at 4mA beam
current, there are large mismatches at lower current values.

|                                                                                                                                 |
|---------------------------------------------------------------------------------------------------------------------------------|
| ![Table 5, from D-MYRTE-2.6, rendered as an image](media/rId68.png){width="3.9130424321959754in" height="1.3745811461067368in"} |

Table: The differences in the simulation results using Design #1 and
Design #2 are listed. The main difference is the larger maximal vertical
envelope in the case of Design #2 due to the lack of a vertical focusing
effect in the case of straight pole faces. The results for the emittance
growth follow the trends shown in Fig. \[\*\]. Since all the results
presented here were obtained for tuning optimisations at 4mA beam
current, there are large mismatches at lower current values.

 

**Table:** Main parameters of the 2020 reference MINERVA design.

<table>
<caption>Table: Main parameters of the 2020 reference MINERVA design.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>Section #</th>
<th>#1</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>hline E<sub>input</sub> (MeV)</td>
<td>16.6</td>
</tr>
<tr class="even">
<td>E<sub>output</sub> (MeV)</td>
<td>101.4</td>
</tr>
<tr class="odd">
<td>Cav. technology</td>
<td>Spoke</td>
</tr>
<tr class="even">
<td>Cav. freq. (MHz)</td>
<td>352.2</td>
</tr>
<tr class="odd">
<td>Cavity optim. β</td>
<td>0.375</td>
</tr>
<tr class="even">
<td>Nb of cells / cav.</td>
<td>2</td>
</tr>
<tr class="odd">
<td>Focusing type</td>
<td>NC quadrupole doublets</td>
</tr>
<tr class="even">
<td>Nb cav / cryom.</td>
<td>2</td>
</tr>
<tr class="odd">
<td>Total nb of cav.</td>
<td>46</td>
</tr>
<tr class="even">
<td>Nominal E <sub>acc</sub> (MV m<sup>-1</sup>) *</td>
<td>6.4</td>
</tr>
<tr class="odd">
<td>Max E <sub>acc</sub> (MV m<sup>-1</sup>)* (fault recovery)</td>
<td>8.2</td>
</tr>
<tr class="even">
<td>Synch. phase (deg)</td>
<td>-45 to -18</td>
</tr>
<tr class="odd">
<td>Beam load / cav (kW)</td>
<td>1.1 to 8.6</td>
</tr>
<tr class="even">
<td>Nom. Qpole grad. (T m<sup>-1</sup>)</td>
<td>5.1 to 7.9</td>
</tr>
<tr class="odd">
<td>Section length (m)</td>
<td>91.2</td>
</tr>
<tr class="even">
<td colspan="2">*E<sub>acc</sub>
is given at β
<sub>opt</sub>
normalised to L<sub>acc</sub> = Ngap · β · λ/2c.</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main parameters of the 2020 reference MINERVA design.

 

**Table:** Main parameters of the 2020 reference MINERVA design.

<table>
<caption>Table: Main parameters of the 2020 reference MINERVA design.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>Section #</th>
<th>#1</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>hline E<sub>input</sub> (MeV)</td>
<td>16.6</td>
</tr>
<tr class="even">
<td>E<sub>output</sub> (MeV)</td>
<td>101.4</td>
</tr>
<tr class="odd">
<td>Cav. technology</td>
<td>Spoke</td>
</tr>
<tr class="even">
<td>Cav. freq. (MHz)</td>
<td>352.2</td>
</tr>
<tr class="odd">
<td>Cavity optim. β</td>
<td>0.375</td>
</tr>
<tr class="even">
<td>Nb of cells / cav.</td>
<td>2</td>
</tr>
<tr class="odd">
<td>Focusing type</td>
<td>NC quadrupole doublets</td>
</tr>
<tr class="even">
<td>Nb cav / cryom.</td>
<td>2</td>
</tr>
<tr class="odd">
<td>Total nb of cav.</td>
<td>46</td>
</tr>
<tr class="even">
<td>Nominal E <sub>acc</sub> (MV m<sup>-1</sup>) *</td>
<td>6.4</td>
</tr>
<tr class="odd">
<td>Max E <sub>acc</sub> (MV m<sup>-1</sup>)* (fault recovery)</td>
<td>8.2</td>
</tr>
<tr class="even">
<td>Synch. phase (deg)</td>
<td>-45 to -18</td>
</tr>
<tr class="odd">
<td>Beam load / cav (kW)</td>
<td>1.1 to 8.6</td>
</tr>
<tr class="even">
<td>Nom. Qpole grad. (T m<sup>-1</sup>)</td>
<td>5.1 to 7.9</td>
</tr>
<tr class="odd">
<td>Section length (m)</td>
<td>91.2</td>
</tr>
<tr class="even">
<td colspan="2">*E<sub>acc</sub>
is given at β
<sub>opt</sub>
normalised to L<sub>acc</sub> = Ngap · β · λ/2c.</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main parameters of the 2020 reference MINERVA design.

 

**Table:** Main parameters of the 2020 reference MINERVA design.

<table>
<caption>Table: Main parameters of the 2020 reference MINERVA design.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>Section #</th>
<th>#1</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>hline E<sub>input</sub> (MeV)</td>
<td>16.6</td>
</tr>
<tr class="even">
<td>E<sub>output</sub> (MeV)</td>
<td>101.4</td>
</tr>
<tr class="odd">
<td>Cav. technology</td>
<td>Spoke</td>
</tr>
<tr class="even">
<td>Cav. freq. (MHz)</td>
<td>352.2</td>
</tr>
<tr class="odd">
<td>Cavity optim. β</td>
<td>0.375</td>
</tr>
<tr class="even">
<td>Nb of cells / cav.</td>
<td>2</td>
</tr>
<tr class="odd">
<td>Focusing type</td>
<td>NC quadrupole doublets</td>
</tr>
<tr class="even">
<td>Nb cav / cryom.</td>
<td>2</td>
</tr>
<tr class="odd">
<td>Total nb of cav.</td>
<td>46</td>
</tr>
<tr class="even">
<td>Nominal E <sub>acc</sub> (MV m<sup>-1</sup>) *</td>
<td>6.4</td>
</tr>
<tr class="odd">
<td>Max E <sub>acc</sub> (MV m<sup>-1</sup>)* (fault recovery)</td>
<td>8.2</td>
</tr>
<tr class="even">
<td>Synch. phase (deg)</td>
<td>-45 to -18</td>
</tr>
<tr class="odd">
<td>Beam load / cav (kW)</td>
<td>1.1 to 8.6</td>
</tr>
<tr class="even">
<td>Nom. Qpole grad. (T m<sup>-1</sup>)</td>
<td>5.1 to 7.9</td>
</tr>
<tr class="odd">
<td>Section length (m)</td>
<td>91.2</td>
</tr>
<tr class="even">
<td colspan="2">*E<sub>acc</sub>
is given at β
<sub>opt</sub>
normalised to L<sub>acc</sub> = Ngap · β · λ/2c.</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main parameters of the 2020 reference MINERVA design.

 

**Table:** Main parameters of the 2020 reference MINERVA design.

<table>
<caption>Table: Main parameters of the 2020 reference MINERVA design.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>Section #</th>
<th>#1</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>hline E<sub>input</sub> (MeV)</td>
<td>16.6</td>
</tr>
<tr class="even">
<td>E<sub>output</sub> (MeV)</td>
<td>101.4</td>
</tr>
<tr class="odd">
<td>Cav. technology</td>
<td>Spoke</td>
</tr>
<tr class="even">
<td>Cav. freq. (MHz)</td>
<td>352.2</td>
</tr>
<tr class="odd">
<td>Cavity optim. β</td>
<td>0.375</td>
</tr>
<tr class="even">
<td>Nb of cells / cav.</td>
<td>2</td>
</tr>
<tr class="odd">
<td>Focusing type</td>
<td>NC quadrupole doublets</td>
</tr>
<tr class="even">
<td>Nb cav / cryom.</td>
<td>2</td>
</tr>
<tr class="odd">
<td>Total nb of cav.</td>
<td>46</td>
</tr>
<tr class="even">
<td>Nominal E <sub>acc</sub> (MV m<sup>-1</sup>) *</td>
<td>6.4</td>
</tr>
<tr class="odd">
<td>Max E <sub>acc</sub> (MV m<sup>-1</sup>)* (fault recovery)</td>
<td>8.2</td>
</tr>
<tr class="even">
<td>Synch. phase (deg)</td>
<td>-45 to -18</td>
</tr>
<tr class="odd">
<td>Beam load / cav (kW)</td>
<td>1.1 to 8.6</td>
</tr>
<tr class="even">
<td>Nom. Qpole grad. (T m<sup>-1</sup>)</td>
<td>5.1 to 7.9</td>
</tr>
<tr class="odd">
<td>Section length (m)</td>
<td>91.2</td>
</tr>
<tr class="even">
<td colspan="2">*E<sub>acc</sub>
is given at β
<sub>opt</sub>
normalised to L<sub>acc</sub> = Ngap · β · λ/2c.</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main parameters of the 2020 reference MINERVA design.

 

**Table:** Main parameters of the RFQ cavity (I1-RFQ).

<table>
<caption>Table: Main parameters of the RFQ cavity (I1-RFQ).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Unit</td>
<td>RFQ</td>
</tr>
<tr class="even">
<td>W (in)</td>
<td>MeV</td>
<td>0.03</td>
</tr>
<tr class="odd">
<td>W (out)</td>
<td>MeV</td>
<td>1.50</td>
</tr>
<tr class="even">
<td>Length</td>
<td>m</td>
<td>4</td>
</tr>
<tr class="odd">
<td>Number of cell</td>
<td>–</td>
<td>244</td>
</tr>
<tr class="even">
<td>Inter-Rod voltage</td>
<td>kV</td>
<td>44</td>
</tr>
<tr class="odd">
<td>Kilpatrick Factor</td>
<td>–</td>
<td>1.05</td>
</tr>
<tr class="even">
<td>Rp</td>
<td>kΩ m⁻¹</td>
<td>73</td>
</tr>
<tr class="odd">
<td><em>Q</em><sub>0</sub></td>
<td>–</td>
<td>4000</td>
</tr>
<tr class="even">
<td><em>P</em><sub>0</sub></td>
<td>kW</td>
<td>102.1</td>
</tr>
<tr class="odd">
<td><em>P</em><sub>beam</sub></td>
<td>kW</td>
<td>5.9</td>
</tr>
<tr class="even">
<td><em>P</em><sub>total</sub></td>
<td>kW</td>
<td>108</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main parameters of the RFQ cavity (I1-RFQ).

 

**Table:** Mechanical specifications of the RFQ cavity (I1-RFQ).

<table>
<caption>Table: Mechanical specifications of the RFQ cavity (I1-RFQ).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Specification</td>
<td>Value</td>
</tr>
<tr class="even">
<td>Total length (mm)</td>
<td>4154.50</td>
</tr>
<tr class="odd">
<td>Rod length (mm)</td>
<td>4013.66</td>
</tr>
<tr class="even">
<td>Overall dimensions (mm)</td>
<td>L = 4155; H = 1815; W = 830 (without gate valves)</td>
</tr>
<tr class="odd">
<td>Approx. weight (t)</td>
<td>2</td>
</tr>
<tr class="even">
<td>Stems</td>
<td>40</td>
</tr>
<tr class="odd">
<td>Rod Elements</td>
<td>12</td>
</tr>
<tr class="even">
<td>Tuning Plates</td>
<td>39</td>
</tr>
<tr class="odd">
<td>Cooling Flow for <strong>TBD: inline26087, img1</strong> = 5K (L min⁻¹)</td>
<td>345</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Mechanical specifications of the RFQ cavity (I1-RFQ).

 

**Table:** Interfaces specifications of the RFQ cavity (I1-RFQ).

<table>
<caption>Table: Interfaces specifications of the RFQ cavity (I1-RFQ).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Interface</td>
<td>Qty</td>
<td>Type</td>
</tr>
<tr class="even">
<td>Tuner</td>
<td>2</td>
<td>CF63</td>
</tr>
<tr class="odd">
<td>Power Coupler</td>
<td>1</td>
<td>CF100</td>
</tr>
<tr class="even">
<td>Pick-up and RGA</td>
<td>4</td>
<td>CF40</td>
</tr>
<tr class="odd">
<td>Auxiliary vacuum ports</td>
<td>4</td>
<td>KF40</td>
</tr>
<tr class="even">
<td>Main vacuum pumping ports</td>
<td>3</td>
<td>CF100</td>
</tr>
<tr class="odd">
<td>Beam input</td>
<td>1</td>
<td>Tailor-made with elastomer seal</td>
</tr>
<tr class="even">
<td>Beam output</td>
<td>1</td>
<td>Tailor-made with elastomer seal</td>
</tr>
<tr class="odd">
<td>Rods manifold</td>
<td>2</td>
<td>1″ BSPT male</td>
</tr>
<tr class="even">
<td>Stems manifold</td>
<td>2</td>
<td>1″ BSPT male</td>
</tr>
<tr class="odd">
<td>Tuning Plates manifold</td>
<td>2</td>
<td>1″ BSPT male</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Interfaces specifications of the RFQ cavity (I1-RFQ).

 

**Table:** Main parameters of I1-ME1:RF-CAV-0

0 and I1-ME1:RF-CAV-0

1\.

<table>
<caption>Table: Main parameters of I1-ME1:RF-CAV-00 and I1-ME1:RF-CAV-01.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Unit</td>
<td>QWR1</td>
<td>QWR2</td>
</tr>
<tr class="even">
<td>W (in)</td>
<td>MeV</td>
<td>1.50</td>
<td>1.50</td>
</tr>
<tr class="odd">
<td>W (out)</td>
<td>MeV</td>
<td>1.50</td>
<td>1.50</td>
</tr>
<tr class="even">
<td>Nb of gaps</td>
<td>–</td>
<td>2</td>
<td>2</td>
</tr>
<tr class="odd">
<td><em>L<sub>eff</sub></em></td>
<td>m</td>
<td>0.096</td>
<td>0.096</td>
</tr>
<tr class="even">
<td><em>E<sub>acc</sub></em></td>
<td>MV m<sup>-1</sup></td>
<td>0.83</td>
<td>0.94</td>
</tr>
<tr class="odd">
<td><em>Q</em><sub>0</sub></td>
<td>–</td>
<td>11400</td>
<td>11400</td>
</tr>
<tr class="even">
<td><em>Z</em><sub>sh</sub></td>
<td>MΩ m⁻¹</td>
<td>45.2</td>
<td>45.2</td>
</tr>
<tr class="odd">
<td><em>P</em><sub>0</sub></td>
<td>kW</td>
<td>2</td>
<td>2.4</td>
</tr>
<tr class="even">
<td><em>P</em><sub>beam</sub></td>
<td>kW</td>
<td>0</td>
<td>0</td>
</tr>
<tr class="odd">
<td><em>P</em><sub>total</sub></td>
<td>kW</td>
<td>2</td>
<td>2.4</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main parameters of I1-ME1:RF-CAV-00 and I1-ME1:RF-CAV-01.

 

**Table:** RF losses distribution for both QWRs (I1-ME1:RF-CAV-0

2 and I1-ME1:RF-CAV-0

1).

<table>
<caption>Table: RF losses distribution for both QWRs (I1-ME1:RF-CAV-02 and I1-ME1:RF-CAV-01).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>P
<sub>Tank</sub>
(%)</td>
<td>P
<sub>Lids</sub>
(%)</td>
<td>P
<sub>Stems</sub>
(%)</td>
<td>P
<sub>Drift Tubes</sub>
(%)</td>
<td>P
<sub>Tuners</sub>
(%)</td>
</tr>
<tr class="even">
<td>20.4</td>
<td>10.2</td>
<td>68.6</td>
<td>0.7</td>
<td>0.1</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: RF losses distribution for both QWRs (I1-ME1:RF-CAV-02 and
I1-ME1:RF-CAV-01).

 

**Table:** Interfaces list of the QWR cavities.

<table>
<caption>Table: Interfaces list of the QWR cavities.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th>Interface name</th>
<th>Quantity</th>
<th>Interface type</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Tuner Flange</td>
<td>2</td>
<td>CF63 Rotatable</td>
</tr>
<tr class="even">
<td>Pumping Flange</td>
<td>1</td>
<td>CF100 Threaded Bolt Holes</td>
</tr>
<tr class="odd">
<td>Power Coupler Flange</td>
<td>1</td>
<td>CF63 Rotatable</td>
</tr>
<tr class="even">
<td>Beam Line Connection</td>
<td>2</td>
<td>CF100 Threaded Bolt Holes</td>
</tr>
<tr class="odd">
<td>Gauges + Pickup</td>
<td>4</td>
<td>CF40 Rotatable</td>
</tr>
<tr class="even">
<td>Pre-Vacuum Notch</td>
<td>1</td>
<td>KF16</td>
</tr>
<tr class="odd">
<td>Water Connector</td>
<td>34</td>
<td>3/8″ BSP</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Interfaces list of the QWR cavities.

 

**Table:** Interfaces list for the QWR dynamic tuner.

<table>
<caption>Table: Interfaces list for the QWR dynamic tuner.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Water Supply</td>
<td>2 × 1/4″ BSPT female</td>
</tr>
<tr class="even">
<td>Motor Driver</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td>Potentiometer (Optional)</td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Hardware Interlock Switches</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td>Mechanical Interface to QWR cavity</td>
<td>CF63</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Interfaces list for the QWR dynamic tuner.

 

**Table:** Main parameters of the first section of CH cavities (up to
CHR1).

<table>
<caption>Table: Main parameters of the first section of CH cavities (up to CHR1).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Unit</td>
<td>CH01</td>
<td>CH02</td>
<td>CH03</td>
<td>CH04</td>
<td>CH05</td>
<td>CH06</td>
<td>CH07</td>
<td>CHR1</td>
</tr>
<tr class="even">
<td>W (in)</td>
<td>MeV</td>
<td>1.50</td>
<td>1.68</td>
<td>1.98</td>
<td>2.43</td>
<td>3.02</td>
<td>3.77</td>
<td>4.83</td>
<td>5.85</td>
</tr>
<tr class="odd">
<td>W (out)</td>
<td>MeV</td>
<td>1.68</td>
<td>1.98</td>
<td>2.43</td>
<td>3.02</td>
<td>3.77</td>
<td>4.83</td>
<td>5.85</td>
<td>5.85</td>
</tr>
<tr class="even">
<td>Nb of gap</td>
<td>–</td>
<td>3</td>
<td>4</td>
<td>5</td>
<td>6</td>
<td>7</td>
<td>9</td>
<td>9</td>
<td>5</td>
</tr>
<tr class="odd">
<td><em>L<sub>eff</sub></em></td>
<td>m</td>
<td>0.15</td>
<td>0.21</td>
<td>0.29</td>
<td>0.39</td>
<td>0.51</td>
<td>0.73</td>
<td>0.81</td>
<td>0.47</td>
</tr>
<tr class="even">
<td><em>E<sub>acc</sub></em></td>
<td>MV m<sup>-1</sup></td>
<td>1.36</td>
<td>1.65</td>
<td>1.78</td>
<td>1.77</td>
<td>1.71</td>
<td>1.66</td>
<td>1.46</td>
<td>–</td>
</tr>
<tr class="odd">
<td><em>Q</em><sub>0</sub></td>
<td>–</td>
<td>13000</td>
<td>14800</td>
<td>16400</td>
<td>15800</td>
<td>18200</td>
<td>16400</td>
<td>18000</td>
<td>15800</td>
</tr>
<tr class="even">
<td><em>Z</em><sub>sh</sub></td>
<td>MΩ m⁻¹</td>
<td>47.60</td>
<td>58.40</td>
<td>70.10</td>
<td>74.10</td>
<td>74.40</td>
<td>72.50</td>
<td>67.60</td>
<td>82.00</td>
</tr>
<tr class="odd">
<td><em>P</em><sub>0</sub></td>
<td>kW</td>
<td>8</td>
<td>11.7</td>
<td>15</td>
<td>17</td>
<td>20</td>
<td>27.5</td>
<td>25</td>
<td>7.2</td>
</tr>
<tr class="even">
<td><em>P</em><sub>beam</sub></td>
<td>kW</td>
<td>0.7</td>
<td>1.2</td>
<td>1.8</td>
<td>2.4</td>
<td>3</td>
<td>4.2</td>
<td>4.1</td>
<td>0</td>
</tr>
<tr class="odd">
<td><em>P</em><sub>total</sub></td>
<td>kW</td>
<td>8.7</td>
<td>12.9</td>
<td>16.8</td>
<td>19.4</td>
<td>23</td>
<td>31.7</td>
<td>29.1</td>
<td>7.2</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main parameters of the first section of CH cavities (up to CHR1).

 

**Table:** Main parameters of the second section of CH cavities (from
CH08).

<table>
<caption>Table: Main parameters of the second section of CH cavities (from CH08).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Unit</td>
<td>CH08</td>
<td>CH09</td>
<td>CH10</td>
<td>CH11</td>
<td>CH12</td>
<td>CH13</td>
<td>CH14</td>
<td>CH15</td>
<td>CHR2</td>
</tr>
<tr class="even">
<td>W (in)</td>
<td>MeV</td>
<td>5.85</td>
<td>7.24</td>
<td>8.70</td>
<td>10.14</td>
<td>11.51</td>
<td>12.80</td>
<td>14.09</td>
<td>15.33</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td>W (out)</td>
<td>MeV</td>
<td>7.24</td>
<td>8.70</td>
<td>10.14</td>
<td>11.51</td>
<td>12.80</td>
<td>14.09</td>
<td>15.33</td>
<td>16.59</td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Nb of gaps</td>
<td>–</td>
<td>11</td>
<td>12</td>
<td>12</td>
<td>11</td>
<td>10</td>
<td>10</td>
<td>9</td>
<td>9</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td><em>L<sub>eff</sub></em></td>
<td>m</td>
<td>1.11</td>
<td>1.33</td>
<td>1.43</td>
<td>1.39</td>
<td>1.36</td>
<td>1.43</td>
<td>1.34</td>
<td>1.40</td>
<td>TBD</td>
</tr>
<tr class="even">
<td><em>E<sub>acc</sub></em></td>
<td>MV m<sup>-1</sup></td>
<td>1.44</td>
<td>1.27</td>
<td>1.16</td>
<td>1.14</td>
<td>1.10</td>
<td>1.04</td>
<td>1.07</td>
<td>1.04</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td><em>Q</em><sub>0</sub></td>
<td>–</td>
<td>17300</td>
<td>17300</td>
<td>17400</td>
<td>17400</td>
<td>17200</td>
<td>17200</td>
<td>17200</td>
<td>17200</td>
<td>TBD</td>
</tr>
<tr class="even">
<td><em>Z</em><sub>sh</sub></td>
<td>MΩ m⁻¹</td>
<td>62.89</td>
<td>59.22</td>
<td>54.89</td>
<td>53.00</td>
<td>47.89</td>
<td>46.11</td>
<td>44.44</td>
<td>43.00</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td><em>P</em><sub>0</sub></td>
<td>kW</td>
<td>33.9</td>
<td>33.2</td>
<td>33.2</td>
<td>33.2</td>
<td>33</td>
<td>32.6</td>
<td>32.4</td>
<td>32.2</td>
<td>TBD</td>
</tr>
<tr class="even">
<td><em>P</em><sub>beam</sub></td>
<td>kW</td>
<td>5.6</td>
<td>5.8</td>
<td>5.8</td>
<td>5.5</td>
<td>5.2</td>
<td>5.2</td>
<td>5.0</td>
<td>5.0</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td><em>P</em><sub>total</sub></td>
<td>kW</td>
<td>39.5</td>
<td>39.0</td>
<td>39.0</td>
<td>38.7</td>
<td>38.2</td>
<td>37.8</td>
<td>37.4</td>
<td>37.2</td>
<td>TBD</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main parameters of the second section of CH cavities (from CH08).

 

**Table:** RF Losses distribution per cavity.

<table>
<caption>Table: RF Losses distribution per cavity.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td> </td>
<td>P
<sub>Tank</sub>
(%)</td>
<td>P
<sub>Lids</sub>
(%)</td>
<td>P
<sub>Stems</sub>
(%)</td>
<td>P
<sub>Drift Tubes</sub>
(%)</td>
<td>P
<sub>Tuners</sub>
(%)</td>
</tr>
<tr class="even">
<td>CH01</td>
<td>I1-CH
1:RF-CAV-01</td>
<td>14.7</td>
<td>13.7</td>
<td>69.6</td>
<td>0.5</td>
<td>1.5</td>
</tr>
<tr class="odd">
<td>CH02</td>
<td>I1-CH
2:RF-CAV-01</td>
<td>17.9</td>
<td>11.1</td>
<td>66.7</td>
<td>0.7</td>
<td>3.6</td>
</tr>
<tr class="even">
<td>CH03</td>
<td>I1-CH
3:RF-CAV-01</td>
<td>21.1</td>
<td>7.9</td>
<td>64.9</td>
<td>0.7</td>
<td>5.4</td>
</tr>
<tr class="odd">
<td>CH04</td>
<td>I1-CH
4:RF-CAV-01</td>
<td>23.8</td>
<td>5.2</td>
<td>63.9</td>
<td>0.8</td>
<td>6.3</td>
</tr>
<tr class="even">
<td>CH05</td>
<td>I1-CH
5:RF-CAV-01</td>
<td>27.9</td>
<td>3.3</td>
<td>61.7</td>
<td>0.9</td>
<td>6.2</td>
</tr>
<tr class="odd">
<td>CH06</td>
<td>I1-CH
6:RF-CAV-01</td>
<td>28.7</td>
<td>1.6</td>
<td>63.2</td>
<td>0.9</td>
<td>5.6</td>
</tr>
<tr class="even">
<td>CH07</td>
<td>I1-CH
7:RF-CAV-01</td>
<td>28.5</td>
<td>1.1</td>
<td>63.8</td>
<td>0.9</td>
<td>5.7</td>
</tr>
<tr class="odd">
<td>CHR</td>
<td>I1-ME3:RF-CAV-01</td>
<td>23.7</td>
<td>3.3</td>
<td>67.9</td>
<td>0.9</td>
<td>4.2</td>
</tr>
<tr class="even">
<td>CH08</td>
<td>I1-CH
8:RF-CAV-01</td>
<td>29.8</td>
<td>0.6</td>
<td>64.5</td>
<td>1.2</td>
<td>3.9</td>
</tr>
<tr class="odd">
<td>CH09</td>
<td>I1-CH
9:RF-CAV-01</td>
<td>29.6</td>
<td>0.4</td>
<td>64.8</td>
<td>1.2</td>
<td>4.0</td>
</tr>
<tr class="even">
<td>CH10</td>
<td>I1-CH
10:RF-CAV-01</td>
<td>29.0</td>
<td>0.4</td>
<td>64.9</td>
<td>1.3</td>
<td>4.4</td>
</tr>
<tr class="odd">
<td>CH11</td>
<td>I1-CH
11:RF-CAV-01</td>
<td>28.5</td>
<td>0.3</td>
<td>66.8</td>
<td>1.3</td>
<td>3.1</td>
</tr>
<tr class="even">
<td>CH12</td>
<td>I1-CH
12:RF-CAV-01</td>
<td>27.7</td>
<td>0.3</td>
<td>66.3</td>
<td>1.2</td>
<td>4.5</td>
</tr>
<tr class="odd">
<td>CH13</td>
<td>I1-CH
13:RF-CAV-01</td>
<td>27.5</td>
<td>0.2</td>
<td>66.9</td>
<td>1.3</td>
<td>4.1</td>
</tr>
<tr class="even">
<td>CH14</td>
<td>I1-CH
14:RF-CAV-01</td>
<td>27.1</td>
<td>0.3</td>
<td>68.5</td>
<td>1.6</td>
<td>2.5</td>
</tr>
<tr class="odd">
<td>CH15</td>
<td>I1-CH
15:RF-CAV-01</td>
<td>26.8</td>
<td>0.3</td>
<td>69.2</td>
<td>1.4</td>
<td>2.3</td>
</tr>
<tr class="even">
<td>CHR2</td>
<td>I1-ME2:RF-CAV-01</td>
<td>TBD</td>
<td>TBD</td>
<td>TBD</td>
<td>TBD</td>
<td>TBD</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: RF Losses distribution per cavity.

 

**Table:** Main mechanical characteristics of the CH cavities (up to
CHR1).

<table>
<caption>Table: Main mechanical characteristics of the CH cavities (up to CHR1).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>CH01</td>
<td>CH02</td>
<td>CH03</td>
<td>CH04</td>
<td>CH05</td>
<td>CH06</td>
<td>CH07</td>
<td>CHR1</td>
</tr>
<tr class="even">
<td> </td>
<td>I1-CH
1:RF-CAV-01</td>
<td>I1-CH
2:RF-CAV-01</td>
<td>I1-CH
3:RF-CAV-01</td>
<td>I1-CH
4:RF-CAV-01</td>
<td>I1-CH
5:RF-CAV-01</td>
<td>I1-CH
6:RF-CAV-01</td>
<td>I1-CH
7:RF-CAV-01</td>
<td>I1-ME3:RF-CAV-01</td>
</tr>
<tr class="odd">
<td>Outer diameter (mm)</td>
<td>888</td>
<td>810</td>
<td>789</td>
<td>769</td>
<td>762</td>
<td>756</td>
<td>769</td>
<td>813</td>
</tr>
<tr class="even">
<td>Length (mm, flange to flange)</td>
<td>214</td>
<td>277</td>
<td>353</td>
<td>447</td>
<td>560</td>
<td>782</td>
<td>859</td>
<td>537</td>
</tr>
<tr class="odd">
<td>Weight (kg)</td>
<td>640</td>
<td>640</td>
<td>672</td>
<td>794</td>
<td>900</td>
<td>1112</td>
<td>1214</td>
<td>982</td>
</tr>
<tr class="even">
<td>Overall dimension (mm)</td>
<td>414 × 964 × 964</td>
<td>477 × 900 × 900</td>
<td>557 × 865 × 900</td>
<td>612 × 872 × 630</td>
<td>726 × 892 × 926</td>
<td>947 × 886 × 923</td>
<td>1030 × 899 × 930</td>
<td>702 × 943 × 973</td>
</tr>
<tr class="odd">
<td>Nb of drift tubes</td>
<td>2</td>
<td>3</td>
<td>4</td>
<td>5</td>
<td>6</td>
<td>8</td>
<td>8</td>
<td>4</td>
</tr>
<tr class="even">
<td>Nb of gaps</td>
<td>3</td>
<td>4</td>
<td>5</td>
<td>6</td>
<td>7</td>
<td>9</td>
<td>9</td>
<td>5</td>
</tr>
<tr class="odd">
<td>Nb of CF40 ports</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main mechanical characteristics of the CH cavities (up to CHR1).

 

**Table:** Main mechanical characteristics of the CH cavities (from
CH08).

<table>
<caption>Table: Main mechanical characteristics of the CH cavities (from CH08).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>CH08</td>
<td>CH09</td>
<td>CH10</td>
<td>CH11</td>
<td>CH12</td>
<td>CH13</td>
<td>CH14</td>
<td>CH15</td>
<td>CHR2</td>
</tr>
<tr class="even">
<td> </td>
<td>I1-CH
8:RF-CAV-01</td>
<td>I1-CH
9:RF-CAV-01</td>
<td>I1-CH
10:RF-CAV-01</td>
<td>I1-CH
11:RF-CAV-01</td>
<td>I1-CH
12:RF-CAV-01</td>
<td>I1-CH
13:RF-CAV-01</td>
<td>I1-CH
14:RF-CAV-01</td>
<td>I1-CH
15:RF-CAV-01</td>
<td>I1-ME2:RF-CAV-01</td>
</tr>
<tr class="odd">
<td>Outer diameter (mm)</td>
<td>766</td>
<td>776</td>
<td>786</td>
<td>798</td>
<td>806</td>
<td>812</td>
<td>782</td>
<td>825</td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Length (mm, flange to flange)</td>
<td>1140</td>
<td>1357</td>
<td>1468</td>
<td>1437</td>
<td>1380</td>
<td>1466</td>
<td>1358</td>
<td>1409</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td>Weight (kg)</td>
<td>1460</td>
<td>1802</td>
<td>1973</td>
<td>1968</td>
<td>1922</td>
<td>2052</td>
<td>1834</td>
<td>2017</td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Overall dimension (mm)</td>
<td>1310 × 896 × 938</td>
<td>1567 × 906 × 943</td>
<td>1678 × 916 × 948</td>
<td>1648 × 927 × 958</td>
<td>1590 × 936 × 966</td>
<td>1676 × 942 × 972</td>
<td>1568 × 912 × 946</td>
<td>1619 × 955 × 985</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td>Nb of drift tubes</td>
<td>10</td>
<td>11</td>
<td>11</td>
<td>10</td>
<td>9</td>
<td>9</td>
<td>8</td>
<td>8</td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Nb of gaps</td>
<td>11</td>
<td>12</td>
<td>12</td>
<td>11</td>
<td>10</td>
<td>10</td>
<td>9</td>
<td>9</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td>Nb of CF40 ports</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>TBD</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main mechanical characteristics of the CH cavities (from CH08).

 

**Table:** List of mechanical interfaces for each CH cavities.

<table>
<caption>Table: List of mechanical interfaces for each CH cavities.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th>Interface name</th>
<th>Quantity</th>
<th>Interface type</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Beam input /output flanges</td>
<td>2</td>
<td>CF100 Fixed Blind Threaded Holes</td>
</tr>
<tr class="even">
<td>Power coupler flange (top)</td>
<td>1</td>
<td>CF100 rotatable</td>
</tr>
<tr class="odd">
<td>vacuum pump flange (bottom)</td>
<td>1</td>
<td>CF100 rotatable</td>
</tr>
<tr class="even">
<td>Tuners flanges (left/right)</td>
<td>2</td>
<td>CF100 rotatable</td>
</tr>
<tr class="odd">
<td>Auxiliary flanges (gauges + pick-up)</td>
<td>6</td>
<td>CF40 rotatable</td>
</tr>
<tr class="even">
<td>Water inlet/outlet</td>
<td>2</td>
<td>BSPT female</td>
</tr>
<tr class="odd">
<td>Differential vacuum pumping hole</td>
<td>2</td>
<td>KF16 flange</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: List of mechanical interfaces for each CH cavities.

 

**Table:** Flow requirements for each CH cavity.

<table>
<caption>Table: Flow requirements for each CH cavity.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Cavity</td>
<td>Dissipated power (kW)</td>
<td>Flow (L min⁻¹)</td>
</tr>
<tr class="even">
<td>CH01 (I1-CH
1:RF-CAV-01)</td>
<td>8</td>
<td>22.9</td>
</tr>
<tr class="odd">
<td>CH02 (I1-CH
2:RF-CAV-01)</td>
<td>11.7</td>
<td>33.4</td>
</tr>
<tr class="even">
<td>CH03 (I1-CH
3:RF-CAV-01)</td>
<td>15</td>
<td>42.9</td>
</tr>
<tr class="odd">
<td>CH04 (I1-CH
4:RF-CAV-01)</td>
<td>17</td>
<td>48.6</td>
</tr>
<tr class="even">
<td>CH05 (I1-CH
5:RF-CAV-01)</td>
<td>20</td>
<td>57.1</td>
</tr>
<tr class="odd">
<td>CH06 (I1-CH
6:RF-CAV-01)</td>
<td>27.5</td>
<td>78.6</td>
</tr>
<tr class="even">
<td>CH07 (I1-CH
7:RF-CAV-01)</td>
<td>25</td>
<td>71.4</td>
</tr>
<tr class="odd">
<td>CHR1 (I1-ME3:RF-CAV-01)</td>
<td>7.2</td>
<td>20.6</td>
</tr>
<tr class="even">
<td>CH08 (I1-CH
8:RF-CAV-01)</td>
<td>33.9</td>
<td>96.9</td>
</tr>
<tr class="odd">
<td>CH09 (I1-CH
9:RF-CAV-01)</td>
<td>33.2</td>
<td>94.9</td>
</tr>
<tr class="even">
<td>CH10 (I1-CH
10:RF-CAV-01)</td>
<td>33.2</td>
<td>94.9</td>
</tr>
<tr class="odd">
<td>CH11 (I1-CH
11:RF-CAV-01)</td>
<td>33.2</td>
<td>94.9</td>
</tr>
<tr class="even">
<td>CH12 (I1-CH
12:RF-CAV-01)</td>
<td>33</td>
<td>94.3</td>
</tr>
<tr class="odd">
<td>CH13 (I1-CH
13:RF-CAV-01)</td>
<td>32.6</td>
<td>93.1</td>
</tr>
<tr class="even">
<td>CH14 (I1-CH
14:RF-CAV-01)</td>
<td>32.4</td>
<td>92.6</td>
</tr>
<tr class="odd">
<td>CH15 (I1-CH
15:RF-CAV-01)</td>
<td>32.2</td>
<td>92.0</td>
</tr>
<tr class="even">
<td>CHR2 (I1-ME2:RF-CAV-01)</td>
<td>TBD</td>
<td>TBD</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Flow requirements for each CH cavity.

 

**Table:** Specifications of distances for the static tuners from CH01
(I1-CH

7:RF-CAV-01) to CH07 (I1-CH

1:RF-CAV-01).

<table>
<caption>Table: Specifications of distances for the static tuners from CH01 (I1-CH7:RF-CAV-01) to CH07 (I1-CH1:RF-CAV-01).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Distance between working point</td>
<td>Distance between working</td>
</tr>
<tr class="even">
<td> </td>
<td>and beam axis in mm (D1)</td>
<td>point to flange in mm (D2)</td>
</tr>
<tr class="odd">
<td>CH01 (I1-CH
1:RF-CAV-01)</td>
<td>105.29</td>
<td>376.46</td>
</tr>
<tr class="even">
<td>CH02 (I1-CH
2:RF-CAV-01)</td>
<td>122.29</td>
<td>327.69</td>
</tr>
<tr class="odd">
<td>CH03 (I1-CH
3:RF-CAV-01)</td>
<td>133.63</td>
<td>298.55</td>
</tr>
<tr class="even">
<td>CH04 (I1-CH
4:RF-CAV-01)</td>
<td>124.49</td>
<td>297.7</td>
</tr>
<tr class="odd">
<td>CH05 (I1-CH
5:RF-CAV-01)</td>
<td>121.71</td>
<td>297.05</td>
</tr>
<tr class="even">
<td>CH06 (I1-CH
6:RF-CAV-01)</td>
<td>114.18</td>
<td>301.68</td>
</tr>
<tr class="odd">
<td>CH07 (I1-CH
7:RF-CAV-01)</td>
<td>123.74</td>
<td>298.71</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Specifications of distances for the static tuners from CH01
(I1-CH7:RF-CAV-01) to CH07 (I1-CH1:RF-CAV-01).

 

**Table:** Main specifications for the CH dynamic tuner.

<table>
<caption>Table: Main specifications for the CH dynamic tuner.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Stroke</td>
<td>100mm</td>
</tr>
<tr class="even">
<td>Max plunger diameter</td>
<td>95mm</td>
</tr>
<tr class="odd">
<td>RF Contacts material</td>
<td>Cu-Be, Ag Plated</td>
</tr>
<tr class="even">
<td>Bounding Box (Without plunger)</td>
<td>300 × 385 × 300mm</td>
</tr>
<tr class="odd">
<td>Weight</td>
<td>35 kg</td>
</tr>
<tr class="even">
<td>Water Flow</td>
<td>Max 10L min⁻¹ @ 5bar</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main specifications for the CH dynamic tuner.

 

**Table:** Interfaces list for the CH dynamic tuner.

<table>
<caption>Table: Interfaces list for the CH dynamic tuner.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Plunger Interface</td>
<td>CF100</td>
</tr>
<tr class="even">
<td>Water Supply</td>
<td>2 × 1/4″ BSPT female</td>
</tr>
<tr class="odd">
<td>Motor Driver</td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Potentiometer (Optional)</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td>Hardware Interlock Switches</td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Mechanical Interface to CH cavity</td>
<td>CF100</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Interfaces list for the CH dynamic tuner.

 

**Table:** Main parameters of the RFQ cavity (I1-RFQ).

<table>
<caption>Table: Main parameters of the RFQ cavity (I1-RFQ).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Unit</td>
<td>RFQ</td>
</tr>
<tr class="even">
<td>W (in)</td>
<td>MeV</td>
<td>0.03</td>
</tr>
<tr class="odd">
<td>W (out)</td>
<td>MeV</td>
<td>1.50</td>
</tr>
<tr class="even">
<td>Length</td>
<td>m</td>
<td>4</td>
</tr>
<tr class="odd">
<td>Number of cell</td>
<td>–</td>
<td>244</td>
</tr>
<tr class="even">
<td>Inter-Rod voltage</td>
<td>kV</td>
<td>44</td>
</tr>
<tr class="odd">
<td>Kilpatrick Factor</td>
<td>–</td>
<td>1.05</td>
</tr>
<tr class="even">
<td>Rp</td>
<td>kΩ m⁻¹</td>
<td>73</td>
</tr>
<tr class="odd">
<td><em>Q</em><sub>0</sub></td>
<td>–</td>
<td>4000</td>
</tr>
<tr class="even">
<td><em>P</em><sub>0</sub></td>
<td>kW</td>
<td>102.1</td>
</tr>
<tr class="odd">
<td><em>P</em><sub>beam</sub></td>
<td>kW</td>
<td>5.9</td>
</tr>
<tr class="even">
<td><em>P</em><sub>total</sub></td>
<td>kW</td>
<td>108</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main parameters of the RFQ cavity (I1-RFQ).

 

**Table:** Mechanical specifications of the RFQ cavity (I1-RFQ).

<table>
<caption>Table: Mechanical specifications of the RFQ cavity (I1-RFQ).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Specification</td>
<td>Value</td>
</tr>
<tr class="even">
<td>Total length (mm)</td>
<td>4154.50</td>
</tr>
<tr class="odd">
<td>Rod length (mm)</td>
<td>4013.66</td>
</tr>
<tr class="even">
<td>Overall dimensions (mm)</td>
<td>L = 4155; H = 1815; W = 830 (without gate valves)</td>
</tr>
<tr class="odd">
<td>Approx. weight (t)</td>
<td>2</td>
</tr>
<tr class="even">
<td>Stems</td>
<td>40</td>
</tr>
<tr class="odd">
<td>Rod Elements</td>
<td>12</td>
</tr>
<tr class="even">
<td>Tuning Plates</td>
<td>39</td>
</tr>
<tr class="odd">
<td>Cooling Flow for <strong>TBD: inline26087, img1</strong> = 5K (L min⁻¹)</td>
<td>345</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Mechanical specifications of the RFQ cavity (I1-RFQ).

 

**Table:** Interfaces specifications of the RFQ cavity (I1-RFQ).

<table>
<caption>Table: Interfaces specifications of the RFQ cavity (I1-RFQ).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Interface</td>
<td>Qty</td>
<td>Type</td>
</tr>
<tr class="even">
<td>Tuner</td>
<td>2</td>
<td>CF63</td>
</tr>
<tr class="odd">
<td>Power Coupler</td>
<td>1</td>
<td>CF100</td>
</tr>
<tr class="even">
<td>Pick-up and RGA</td>
<td>4</td>
<td>CF40</td>
</tr>
<tr class="odd">
<td>Auxiliary vacuum ports</td>
<td>4</td>
<td>KF40</td>
</tr>
<tr class="even">
<td>Main vacuum pumping ports</td>
<td>3</td>
<td>CF100</td>
</tr>
<tr class="odd">
<td>Beam input</td>
<td>1</td>
<td>Tailor-made with elastomer seal</td>
</tr>
<tr class="even">
<td>Beam output</td>
<td>1</td>
<td>Tailor-made with elastomer seal</td>
</tr>
<tr class="odd">
<td>Rods manifold</td>
<td>2</td>
<td>1″ BSPT male</td>
</tr>
<tr class="even">
<td>Stems manifold</td>
<td>2</td>
<td>1″ BSPT male</td>
</tr>
<tr class="odd">
<td>Tuning Plates manifold</td>
<td>2</td>
<td>1″ BSPT male</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Interfaces specifications of the RFQ cavity (I1-RFQ).

 

**Table:** Main parameters of I1-ME1:RF-CAV-0

7 and I1-ME1:RF-CAV-0

1\.

<table>
<caption>Table: Main parameters of I1-ME1:RF-CAV-07 and I1-ME1:RF-CAV-01.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Unit</td>
<td>QWR1</td>
<td>QWR2</td>
</tr>
<tr class="even">
<td>W (in)</td>
<td>MeV</td>
<td>1.50</td>
<td>1.50</td>
</tr>
<tr class="odd">
<td>W (out)</td>
<td>MeV</td>
<td>1.50</td>
<td>1.50</td>
</tr>
<tr class="even">
<td>Nb of gaps</td>
<td>–</td>
<td>2</td>
<td>2</td>
</tr>
<tr class="odd">
<td><em>L<sub>eff</sub></em></td>
<td>m</td>
<td>0.096</td>
<td>0.096</td>
</tr>
<tr class="even">
<td><em>E<sub>acc</sub></em></td>
<td>MV m<sup>-1</sup></td>
<td>0.83</td>
<td>0.94</td>
</tr>
<tr class="odd">
<td><em>Q</em><sub>0</sub></td>
<td>–</td>
<td>11400</td>
<td>11400</td>
</tr>
<tr class="even">
<td><em>Z</em><sub>sh</sub></td>
<td>MΩ m⁻¹</td>
<td>45.2</td>
<td>45.2</td>
</tr>
<tr class="odd">
<td><em>P</em><sub>0</sub></td>
<td>kW</td>
<td>2</td>
<td>2.4</td>
</tr>
<tr class="even">
<td><em>P</em><sub>beam</sub></td>
<td>kW</td>
<td>0</td>
<td>0</td>
</tr>
<tr class="odd">
<td><em>P</em><sub>total</sub></td>
<td>kW</td>
<td>2</td>
<td>2.4</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main parameters of I1-ME1:RF-CAV-07 and I1-ME1:RF-CAV-01.

 

**Table:** RF losses distribution for both QWRs (I1-ME1:RF-CAV-0

2 and I1-ME1:RF-CAV-0

1).

<table>
<caption>Table: RF losses distribution for both QWRs (I1-ME1:RF-CAV-02 and I1-ME1:RF-CAV-01).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>P
<sub>Tank</sub>
(%)</td>
<td>P
<sub>Lids</sub>
(%)</td>
<td>P
<sub>Stems</sub>
(%)</td>
<td>P
<sub>Drift Tubes</sub>
(%)</td>
<td>P
<sub>Tuners</sub>
(%)</td>
</tr>
<tr class="even">
<td>20.4</td>
<td>10.2</td>
<td>68.6</td>
<td>0.7</td>
<td>0.1</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: RF losses distribution for both QWRs (I1-ME1:RF-CAV-02 and
I1-ME1:RF-CAV-01).

 

**Table:** Interfaces list of the QWR cavities.

<table>
<caption>Table: Interfaces list of the QWR cavities.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th>Interface name</th>
<th>Quantity</th>
<th>Interface type</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Tuner Flange</td>
<td>2</td>
<td>CF63 Rotatable</td>
</tr>
<tr class="even">
<td>Pumping Flange</td>
<td>1</td>
<td>CF100 Threaded Bolt Holes</td>
</tr>
<tr class="odd">
<td>Power Coupler Flange</td>
<td>1</td>
<td>CF63 Rotatable</td>
</tr>
<tr class="even">
<td>Beam Line Connection</td>
<td>2</td>
<td>CF100 Threaded Bolt Holes</td>
</tr>
<tr class="odd">
<td>Gauges + Pickup</td>
<td>4</td>
<td>CF40 Rotatable</td>
</tr>
<tr class="even">
<td>Pre-Vacuum Notch</td>
<td>1</td>
<td>KF16</td>
</tr>
<tr class="odd">
<td>Water Connector</td>
<td>34</td>
<td>3/8″ BSP</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Interfaces list of the QWR cavities.

 

**Table:** Interfaces list for the QWR dynamic tuner.

<table>
<caption>Table: Interfaces list for the QWR dynamic tuner.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Water Supply</td>
<td>2 × 1/4″ BSPT female</td>
</tr>
<tr class="even">
<td>Motor Driver</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td>Potentiometer (Optional)</td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Hardware Interlock Switches</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td>Mechanical Interface to QWR cavity</td>
<td>CF63</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Interfaces list for the QWR dynamic tuner.

 

**Table:** Main parameters of the first section of CH cavities (up to
CHR1).

<table>
<caption>Table: Main parameters of the first section of CH cavities (up to CHR1).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Unit</td>
<td>CH01</td>
<td>CH02</td>
<td>CH03</td>
<td>CH04</td>
<td>CH05</td>
<td>CH06</td>
<td>CH07</td>
<td>CHR1</td>
</tr>
<tr class="even">
<td>W (in)</td>
<td>MeV</td>
<td>1.50</td>
<td>1.68</td>
<td>1.98</td>
<td>2.43</td>
<td>3.02</td>
<td>3.77</td>
<td>4.83</td>
<td>5.85</td>
</tr>
<tr class="odd">
<td>W (out)</td>
<td>MeV</td>
<td>1.68</td>
<td>1.98</td>
<td>2.43</td>
<td>3.02</td>
<td>3.77</td>
<td>4.83</td>
<td>5.85</td>
<td>5.85</td>
</tr>
<tr class="even">
<td>Nb of gap</td>
<td>–</td>
<td>3</td>
<td>4</td>
<td>5</td>
<td>6</td>
<td>7</td>
<td>9</td>
<td>9</td>
<td>5</td>
</tr>
<tr class="odd">
<td><em>L<sub>eff</sub></em></td>
<td>m</td>
<td>0.15</td>
<td>0.21</td>
<td>0.29</td>
<td>0.39</td>
<td>0.51</td>
<td>0.73</td>
<td>0.81</td>
<td>0.47</td>
</tr>
<tr class="even">
<td><em>E<sub>acc</sub></em></td>
<td>MV m<sup>-1</sup></td>
<td>1.36</td>
<td>1.65</td>
<td>1.78</td>
<td>1.77</td>
<td>1.71</td>
<td>1.66</td>
<td>1.46</td>
<td>–</td>
</tr>
<tr class="odd">
<td><em>Q</em><sub>0</sub></td>
<td>–</td>
<td>13000</td>
<td>14800</td>
<td>16400</td>
<td>15800</td>
<td>18200</td>
<td>16400</td>
<td>18000</td>
<td>15800</td>
</tr>
<tr class="even">
<td><em>Z</em><sub>sh</sub></td>
<td>MΩ m⁻¹</td>
<td>47.60</td>
<td>58.40</td>
<td>70.10</td>
<td>74.10</td>
<td>74.40</td>
<td>72.50</td>
<td>67.60</td>
<td>82.00</td>
</tr>
<tr class="odd">
<td><em>P</em><sub>0</sub></td>
<td>kW</td>
<td>8</td>
<td>11.7</td>
<td>15</td>
<td>17</td>
<td>20</td>
<td>27.5</td>
<td>25</td>
<td>7.2</td>
</tr>
<tr class="even">
<td><em>P</em><sub>beam</sub></td>
<td>kW</td>
<td>0.7</td>
<td>1.2</td>
<td>1.8</td>
<td>2.4</td>
<td>3</td>
<td>4.2</td>
<td>4.1</td>
<td>0</td>
</tr>
<tr class="odd">
<td><em>P</em><sub>total</sub></td>
<td>kW</td>
<td>8.7</td>
<td>12.9</td>
<td>16.8</td>
<td>19.4</td>
<td>23</td>
<td>31.7</td>
<td>29.1</td>
<td>7.2</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main parameters of the first section of CH cavities (up to CHR1).

 

**Table:** Main parameters of the second section of CH cavities (from
CH08).

<table>
<caption>Table: Main parameters of the second section of CH cavities (from CH08).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Unit</td>
<td>CH08</td>
<td>CH09</td>
<td>CH10</td>
<td>CH11</td>
<td>CH12</td>
<td>CH13</td>
<td>CH14</td>
<td>CH15</td>
<td>CHR2</td>
</tr>
<tr class="even">
<td>W (in)</td>
<td>MeV</td>
<td>5.85</td>
<td>7.24</td>
<td>8.70</td>
<td>10.14</td>
<td>11.51</td>
<td>12.80</td>
<td>14.09</td>
<td>15.33</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td>W (out)</td>
<td>MeV</td>
<td>7.24</td>
<td>8.70</td>
<td>10.14</td>
<td>11.51</td>
<td>12.80</td>
<td>14.09</td>
<td>15.33</td>
<td>16.59</td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Nb of gaps</td>
<td>–</td>
<td>11</td>
<td>12</td>
<td>12</td>
<td>11</td>
<td>10</td>
<td>10</td>
<td>9</td>
<td>9</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td><em>L<sub>eff</sub></em></td>
<td>m</td>
<td>1.11</td>
<td>1.33</td>
<td>1.43</td>
<td>1.39</td>
<td>1.36</td>
<td>1.43</td>
<td>1.34</td>
<td>1.40</td>
<td>TBD</td>
</tr>
<tr class="even">
<td><em>E<sub>acc</sub></em></td>
<td>MV m<sup>-1</sup></td>
<td>1.44</td>
<td>1.27</td>
<td>1.16</td>
<td>1.14</td>
<td>1.10</td>
<td>1.04</td>
<td>1.07</td>
<td>1.04</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td><em>Q</em><sub>0</sub></td>
<td>–</td>
<td>17300</td>
<td>17300</td>
<td>17400</td>
<td>17400</td>
<td>17200</td>
<td>17200</td>
<td>17200</td>
<td>17200</td>
<td>TBD</td>
</tr>
<tr class="even">
<td><em>Z</em><sub>sh</sub></td>
<td>MΩ m⁻¹</td>
<td>62.89</td>
<td>59.22</td>
<td>54.89</td>
<td>53.00</td>
<td>47.89</td>
<td>46.11</td>
<td>44.44</td>
<td>43.00</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td><em>P</em><sub>0</sub></td>
<td>kW</td>
<td>33.9</td>
<td>33.2</td>
<td>33.2</td>
<td>33.2</td>
<td>33</td>
<td>32.6</td>
<td>32.4</td>
<td>32.2</td>
<td>TBD</td>
</tr>
<tr class="even">
<td><em>P</em><sub>beam</sub></td>
<td>kW</td>
<td>5.6</td>
<td>5.8</td>
<td>5.8</td>
<td>5.5</td>
<td>5.2</td>
<td>5.2</td>
<td>5.0</td>
<td>5.0</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td><em>P</em><sub>total</sub></td>
<td>kW</td>
<td>39.5</td>
<td>39.0</td>
<td>39.0</td>
<td>38.7</td>
<td>38.2</td>
<td>37.8</td>
<td>37.4</td>
<td>37.2</td>
<td>TBD</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main parameters of the second section of CH cavities (from CH08).

 

**Table:** RF Losses distribution per cavity.

<table>
<caption>Table: RF Losses distribution per cavity.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td> </td>
<td>P
<sub>Tank</sub>
(%)</td>
<td>P
<sub>Lids</sub>
(%)</td>
<td>P
<sub>Stems</sub>
(%)</td>
<td>P
<sub>Drift Tubes</sub>
(%)</td>
<td>P
<sub>Tuners</sub>
(%)</td>
</tr>
<tr class="even">
<td>CH01</td>
<td>I1-CH
1:RF-CAV-01</td>
<td>14.7</td>
<td>13.7</td>
<td>69.6</td>
<td>0.5</td>
<td>1.5</td>
</tr>
<tr class="odd">
<td>CH02</td>
<td>I1-CH
2:RF-CAV-01</td>
<td>17.9</td>
<td>11.1</td>
<td>66.7</td>
<td>0.7</td>
<td>3.6</td>
</tr>
<tr class="even">
<td>CH03</td>
<td>I1-CH
3:RF-CAV-01</td>
<td>21.1</td>
<td>7.9</td>
<td>64.9</td>
<td>0.7</td>
<td>5.4</td>
</tr>
<tr class="odd">
<td>CH04</td>
<td>I1-CH
4:RF-CAV-01</td>
<td>23.8</td>
<td>5.2</td>
<td>63.9</td>
<td>0.8</td>
<td>6.3</td>
</tr>
<tr class="even">
<td>CH05</td>
<td>I1-CH
5:RF-CAV-01</td>
<td>27.9</td>
<td>3.3</td>
<td>61.7</td>
<td>0.9</td>
<td>6.2</td>
</tr>
<tr class="odd">
<td>CH06</td>
<td>I1-CH
6:RF-CAV-01</td>
<td>28.7</td>
<td>1.6</td>
<td>63.2</td>
<td>0.9</td>
<td>5.6</td>
</tr>
<tr class="even">
<td>CH07</td>
<td>I1-CH
7:RF-CAV-01</td>
<td>28.5</td>
<td>1.1</td>
<td>63.8</td>
<td>0.9</td>
<td>5.7</td>
</tr>
<tr class="odd">
<td>CHR</td>
<td>I1-ME3:RF-CAV-01</td>
<td>23.7</td>
<td>3.3</td>
<td>67.9</td>
<td>0.9</td>
<td>4.2</td>
</tr>
<tr class="even">
<td>CH08</td>
<td>I1-CH
8:RF-CAV-01</td>
<td>29.8</td>
<td>0.6</td>
<td>64.5</td>
<td>1.2</td>
<td>3.9</td>
</tr>
<tr class="odd">
<td>CH09</td>
<td>I1-CH
9:RF-CAV-01</td>
<td>29.6</td>
<td>0.4</td>
<td>64.8</td>
<td>1.2</td>
<td>4.0</td>
</tr>
<tr class="even">
<td>CH10</td>
<td>I1-CH
10:RF-CAV-01</td>
<td>29.0</td>
<td>0.4</td>
<td>64.9</td>
<td>1.3</td>
<td>4.4</td>
</tr>
<tr class="odd">
<td>CH11</td>
<td>I1-CH
11:RF-CAV-01</td>
<td>28.5</td>
<td>0.3</td>
<td>66.8</td>
<td>1.3</td>
<td>3.1</td>
</tr>
<tr class="even">
<td>CH12</td>
<td>I1-CH
12:RF-CAV-01</td>
<td>27.7</td>
<td>0.3</td>
<td>66.3</td>
<td>1.2</td>
<td>4.5</td>
</tr>
<tr class="odd">
<td>CH13</td>
<td>I1-CH
13:RF-CAV-01</td>
<td>27.5</td>
<td>0.2</td>
<td>66.9</td>
<td>1.3</td>
<td>4.1</td>
</tr>
<tr class="even">
<td>CH14</td>
<td>I1-CH
14:RF-CAV-01</td>
<td>27.1</td>
<td>0.3</td>
<td>68.5</td>
<td>1.6</td>
<td>2.5</td>
</tr>
<tr class="odd">
<td>CH15</td>
<td>I1-CH
15:RF-CAV-01</td>
<td>26.8</td>
<td>0.3</td>
<td>69.2</td>
<td>1.4</td>
<td>2.3</td>
</tr>
<tr class="even">
<td>CHR2</td>
<td>I1-ME2:RF-CAV-01</td>
<td>TBD</td>
<td>TBD</td>
<td>TBD</td>
<td>TBD</td>
<td>TBD</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: RF Losses distribution per cavity.

 

**Table:** Main mechanical characteristics of the CH cavities (up to
CHR1).

<table>
<caption>Table: Main mechanical characteristics of the CH cavities (up to CHR1).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>CH01</td>
<td>CH02</td>
<td>CH03</td>
<td>CH04</td>
<td>CH05</td>
<td>CH06</td>
<td>CH07</td>
<td>CHR1</td>
</tr>
<tr class="even">
<td> </td>
<td>I1-CH
1:RF-CAV-01</td>
<td>I1-CH
2:RF-CAV-01</td>
<td>I1-CH
3:RF-CAV-01</td>
<td>I1-CH
4:RF-CAV-01</td>
<td>I1-CH
5:RF-CAV-01</td>
<td>I1-CH
6:RF-CAV-01</td>
<td>I1-CH
7:RF-CAV-01</td>
<td>I1-ME3:RF-CAV-01</td>
</tr>
<tr class="odd">
<td>Outer diameter (mm)</td>
<td>888</td>
<td>810</td>
<td>789</td>
<td>769</td>
<td>762</td>
<td>756</td>
<td>769</td>
<td>813</td>
</tr>
<tr class="even">
<td>Length (mm, flange to flange)</td>
<td>214</td>
<td>277</td>
<td>353</td>
<td>447</td>
<td>560</td>
<td>782</td>
<td>859</td>
<td>537</td>
</tr>
<tr class="odd">
<td>Weight (kg)</td>
<td>640</td>
<td>640</td>
<td>672</td>
<td>794</td>
<td>900</td>
<td>1112</td>
<td>1214</td>
<td>982</td>
</tr>
<tr class="even">
<td>Overall dimension (mm)</td>
<td>414 × 964 × 964</td>
<td>477 × 900 × 900</td>
<td>557 × 865 × 900</td>
<td>612 × 872 × 630</td>
<td>726 × 892 × 926</td>
<td>947 × 886 × 923</td>
<td>1030 × 899 × 930</td>
<td>702 × 943 × 973</td>
</tr>
<tr class="odd">
<td>Nb of drift tubes</td>
<td>2</td>
<td>3</td>
<td>4</td>
<td>5</td>
<td>6</td>
<td>8</td>
<td>8</td>
<td>4</td>
</tr>
<tr class="even">
<td>Nb of gaps</td>
<td>3</td>
<td>4</td>
<td>5</td>
<td>6</td>
<td>7</td>
<td>9</td>
<td>9</td>
<td>5</td>
</tr>
<tr class="odd">
<td>Nb of CF40 ports</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main mechanical characteristics of the CH cavities (up to CHR1).

 

**Table:** Main mechanical characteristics of the CH cavities (from
CH08).

<table>
<caption>Table: Main mechanical characteristics of the CH cavities (from CH08).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>CH08</td>
<td>CH09</td>
<td>CH10</td>
<td>CH11</td>
<td>CH12</td>
<td>CH13</td>
<td>CH14</td>
<td>CH15</td>
<td>CHR2</td>
</tr>
<tr class="even">
<td> </td>
<td>I1-CH
8:RF-CAV-01</td>
<td>I1-CH
9:RF-CAV-01</td>
<td>I1-CH
10:RF-CAV-01</td>
<td>I1-CH
11:RF-CAV-01</td>
<td>I1-CH
12:RF-CAV-01</td>
<td>I1-CH
13:RF-CAV-01</td>
<td>I1-CH
14:RF-CAV-01</td>
<td>I1-CH
15:RF-CAV-01</td>
<td>I1-ME2:RF-CAV-01</td>
</tr>
<tr class="odd">
<td>Outer diameter (mm)</td>
<td>766</td>
<td>776</td>
<td>786</td>
<td>798</td>
<td>806</td>
<td>812</td>
<td>782</td>
<td>825</td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Length (mm, flange to flange)</td>
<td>1140</td>
<td>1357</td>
<td>1468</td>
<td>1437</td>
<td>1380</td>
<td>1466</td>
<td>1358</td>
<td>1409</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td>Weight (kg)</td>
<td>1460</td>
<td>1802</td>
<td>1973</td>
<td>1968</td>
<td>1922</td>
<td>2052</td>
<td>1834</td>
<td>2017</td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Overall dimension (mm)</td>
<td>1310 × 896 × 938</td>
<td>1567 × 906 × 943</td>
<td>1678 × 916 × 948</td>
<td>1648 × 927 × 958</td>
<td>1590 × 936 × 966</td>
<td>1676 × 942 × 972</td>
<td>1568 × 912 × 946</td>
<td>1619 × 955 × 985</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td>Nb of drift tubes</td>
<td>10</td>
<td>11</td>
<td>11</td>
<td>10</td>
<td>9</td>
<td>9</td>
<td>8</td>
<td>8</td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Nb of gaps</td>
<td>11</td>
<td>12</td>
<td>12</td>
<td>11</td>
<td>10</td>
<td>10</td>
<td>9</td>
<td>9</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td>Nb of CF40 ports</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>TBD</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main mechanical characteristics of the CH cavities (from CH08).

 

**Table:** List of mechanical interfaces for each CH cavities.

<table>
<caption>Table: List of mechanical interfaces for each CH cavities.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th>Interface name</th>
<th>Quantity</th>
<th>Interface type</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Beam input /output flanges</td>
<td>2</td>
<td>CF100 Fixed Blind Threaded Holes</td>
</tr>
<tr class="even">
<td>Power coupler flange (top)</td>
<td>1</td>
<td>CF100 rotatable</td>
</tr>
<tr class="odd">
<td>vacuum pump flange (bottom)</td>
<td>1</td>
<td>CF100 rotatable</td>
</tr>
<tr class="even">
<td>Tuners flanges (left/right)</td>
<td>2</td>
<td>CF100 rotatable</td>
</tr>
<tr class="odd">
<td>Auxiliary flanges (gauges + pick-up)</td>
<td>6</td>
<td>CF40 rotatable</td>
</tr>
<tr class="even">
<td>Water inlet/outlet</td>
<td>2</td>
<td>BSPT female</td>
</tr>
<tr class="odd">
<td>Differential vacuum pumping hole</td>
<td>2</td>
<td>KF16 flange</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: List of mechanical interfaces for each CH cavities.

 

**Table:** Flow requirements for each CH cavity.

<table>
<caption>Table: Flow requirements for each CH cavity.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Cavity</td>
<td>Dissipated power (kW)</td>
<td>Flow (L min⁻¹)</td>
</tr>
<tr class="even">
<td>CH01 (I1-CH
1:RF-CAV-01)</td>
<td>8</td>
<td>22.9</td>
</tr>
<tr class="odd">
<td>CH02 (I1-CH
2:RF-CAV-01)</td>
<td>11.7</td>
<td>33.4</td>
</tr>
<tr class="even">
<td>CH03 (I1-CH
3:RF-CAV-01)</td>
<td>15</td>
<td>42.9</td>
</tr>
<tr class="odd">
<td>CH04 (I1-CH
4:RF-CAV-01)</td>
<td>17</td>
<td>48.6</td>
</tr>
<tr class="even">
<td>CH05 (I1-CH
5:RF-CAV-01)</td>
<td>20</td>
<td>57.1</td>
</tr>
<tr class="odd">
<td>CH06 (I1-CH
6:RF-CAV-01)</td>
<td>27.5</td>
<td>78.6</td>
</tr>
<tr class="even">
<td>CH07 (I1-CH
7:RF-CAV-01)</td>
<td>25</td>
<td>71.4</td>
</tr>
<tr class="odd">
<td>CHR1 (I1-ME3:RF-CAV-01)</td>
<td>7.2</td>
<td>20.6</td>
</tr>
<tr class="even">
<td>CH08 (I1-CH
8:RF-CAV-01)</td>
<td>33.9</td>
<td>96.9</td>
</tr>
<tr class="odd">
<td>CH09 (I1-CH
9:RF-CAV-01)</td>
<td>33.2</td>
<td>94.9</td>
</tr>
<tr class="even">
<td>CH10 (I1-CH
10:RF-CAV-01)</td>
<td>33.2</td>
<td>94.9</td>
</tr>
<tr class="odd">
<td>CH11 (I1-CH
11:RF-CAV-01)</td>
<td>33.2</td>
<td>94.9</td>
</tr>
<tr class="even">
<td>CH12 (I1-CH
12:RF-CAV-01)</td>
<td>33</td>
<td>94.3</td>
</tr>
<tr class="odd">
<td>CH13 (I1-CH
13:RF-CAV-01)</td>
<td>32.6</td>
<td>93.1</td>
</tr>
<tr class="even">
<td>CH14 (I1-CH
14:RF-CAV-01)</td>
<td>32.4</td>
<td>92.6</td>
</tr>
<tr class="odd">
<td>CH15 (I1-CH
15:RF-CAV-01)</td>
<td>32.2</td>
<td>92.0</td>
</tr>
<tr class="even">
<td>CHR2 (I1-ME2:RF-CAV-01)</td>
<td>TBD</td>
<td>TBD</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Flow requirements for each CH cavity.

 

**Table:** Specifications of distances for the static tuners from CH01
(I1-CH

7:RF-CAV-01) to CH07 (I1-CH

1:RF-CAV-01).

<table>
<caption>Table: Specifications of distances for the static tuners from CH01 (I1-CH7:RF-CAV-01) to CH07 (I1-CH1:RF-CAV-01).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Distance between working point</td>
<td>Distance between working</td>
</tr>
<tr class="even">
<td> </td>
<td>and beam axis in mm (D1)</td>
<td>point to flange in mm (D2)</td>
</tr>
<tr class="odd">
<td>CH01 (I1-CH
1:RF-CAV-01)</td>
<td>105.29</td>
<td>376.46</td>
</tr>
<tr class="even">
<td>CH02 (I1-CH
2:RF-CAV-01)</td>
<td>122.29</td>
<td>327.69</td>
</tr>
<tr class="odd">
<td>CH03 (I1-CH
3:RF-CAV-01)</td>
<td>133.63</td>
<td>298.55</td>
</tr>
<tr class="even">
<td>CH04 (I1-CH
4:RF-CAV-01)</td>
<td>124.49</td>
<td>297.7</td>
</tr>
<tr class="odd">
<td>CH05 (I1-CH
5:RF-CAV-01)</td>
<td>121.71</td>
<td>297.05</td>
</tr>
<tr class="even">
<td>CH06 (I1-CH
6:RF-CAV-01)</td>
<td>114.18</td>
<td>301.68</td>
</tr>
<tr class="odd">
<td>CH07 (I1-CH
7:RF-CAV-01)</td>
<td>123.74</td>
<td>298.71</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Specifications of distances for the static tuners from CH01
(I1-CH7:RF-CAV-01) to CH07 (I1-CH1:RF-CAV-01).

 

**Table:** Main specifications for the CH dynamic tuner.

<table>
<caption>Table: Main specifications for the CH dynamic tuner.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Stroke</td>
<td>100mm</td>
</tr>
<tr class="even">
<td>Max plunger diameter</td>
<td>95mm</td>
</tr>
<tr class="odd">
<td>RF Contacts material</td>
<td>Cu-Be, Ag Plated</td>
</tr>
<tr class="even">
<td>Bounding Box (Without plunger)</td>
<td>300 × 385 × 300mm</td>
</tr>
<tr class="odd">
<td>Weight</td>
<td>35 kg</td>
</tr>
<tr class="even">
<td>Water Flow</td>
<td>Max 10L min⁻¹ @ 5bar</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main specifications for the CH dynamic tuner.

 

**Table:** Interfaces list for the CH dynamic tuner.

<table>
<caption>Table: Interfaces list for the CH dynamic tuner.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Plunger Interface</td>
<td>CF100</td>
</tr>
<tr class="even">
<td>Water Supply</td>
<td>2 × 1/4″ BSPT female</td>
</tr>
<tr class="odd">
<td>Motor Driver</td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Potentiometer (Optional)</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td>Hardware Interlock Switches</td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Mechanical Interface to CH cavity</td>
<td>CF100</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Interfaces list for the CH dynamic tuner.

 

**Table:** Main parameters of the RFQ cavity (I1-RFQ).

<table>
<caption>Table: Main parameters of the RFQ cavity (I1-RFQ).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Unit</td>
<td>RFQ</td>
</tr>
<tr class="even">
<td>W (in)</td>
<td>MeV</td>
<td>0.03</td>
</tr>
<tr class="odd">
<td>W (out)</td>
<td>MeV</td>
<td>1.50</td>
</tr>
<tr class="even">
<td>Length</td>
<td>m</td>
<td>4</td>
</tr>
<tr class="odd">
<td>Number of cell</td>
<td>–</td>
<td>244</td>
</tr>
<tr class="even">
<td>Inter-Rod voltage</td>
<td>kV</td>
<td>44</td>
</tr>
<tr class="odd">
<td>Kilpatrick Factor</td>
<td>–</td>
<td>1.05</td>
</tr>
<tr class="even">
<td>Rp</td>
<td>kΩ m⁻¹</td>
<td>73</td>
</tr>
<tr class="odd">
<td><em>Q</em><sub>0</sub></td>
<td>–</td>
<td>4000</td>
</tr>
<tr class="even">
<td><em>P</em><sub>0</sub></td>
<td>kW</td>
<td>102.1</td>
</tr>
<tr class="odd">
<td><em>P</em><sub>beam</sub></td>
<td>kW</td>
<td>5.9</td>
</tr>
<tr class="even">
<td><em>P</em><sub>total</sub></td>
<td>kW</td>
<td>108</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main parameters of the RFQ cavity (I1-RFQ).

 

**Table:** Mechanical specifications of the RFQ cavity (I1-RFQ).

<table>
<caption>Table: Mechanical specifications of the RFQ cavity (I1-RFQ).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Specification</td>
<td>Value</td>
</tr>
<tr class="even">
<td>Total length (mm)</td>
<td>4154.50</td>
</tr>
<tr class="odd">
<td>Rod length (mm)</td>
<td>4013.66</td>
</tr>
<tr class="even">
<td>Overall dimensions (mm)</td>
<td>L = 4155; H = 1815; W = 830 (without gate valves)</td>
</tr>
<tr class="odd">
<td>Approx. weight (t)</td>
<td>2</td>
</tr>
<tr class="even">
<td>Stems</td>
<td>40</td>
</tr>
<tr class="odd">
<td>Rod Elements</td>
<td>12</td>
</tr>
<tr class="even">
<td>Tuning Plates</td>
<td>39</td>
</tr>
<tr class="odd">
<td>Cooling Flow for <strong>TBD: inline26087, img1</strong> = 5K (L min⁻¹)</td>
<td>345</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Mechanical specifications of the RFQ cavity (I1-RFQ).

 

**Table:** Interfaces specifications of the RFQ cavity (I1-RFQ).

<table>
<caption>Table: Interfaces specifications of the RFQ cavity (I1-RFQ).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Interface</td>
<td>Qty</td>
<td>Type</td>
</tr>
<tr class="even">
<td>Tuner</td>
<td>2</td>
<td>CF63</td>
</tr>
<tr class="odd">
<td>Power Coupler</td>
<td>1</td>
<td>CF100</td>
</tr>
<tr class="even">
<td>Pick-up and RGA</td>
<td>4</td>
<td>CF40</td>
</tr>
<tr class="odd">
<td>Auxiliary vacuum ports</td>
<td>4</td>
<td>KF40</td>
</tr>
<tr class="even">
<td>Main vacuum pumping ports</td>
<td>3</td>
<td>CF100</td>
</tr>
<tr class="odd">
<td>Beam input</td>
<td>1</td>
<td>Tailor-made with elastomer seal</td>
</tr>
<tr class="even">
<td>Beam output</td>
<td>1</td>
<td>Tailor-made with elastomer seal</td>
</tr>
<tr class="odd">
<td>Rods manifold</td>
<td>2</td>
<td>1″ BSPT male</td>
</tr>
<tr class="even">
<td>Stems manifold</td>
<td>2</td>
<td>1″ BSPT male</td>
</tr>
<tr class="odd">
<td>Tuning Plates manifold</td>
<td>2</td>
<td>1″ BSPT male</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Interfaces specifications of the RFQ cavity (I1-RFQ).

 

**Table:** Main parameters of I1-ME1:RF-CAV-0

7 and I1-ME1:RF-CAV-0

1\.

<table>
<caption>Table: Main parameters of I1-ME1:RF-CAV-07 and I1-ME1:RF-CAV-01.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Unit</td>
<td>QWR1</td>
<td>QWR2</td>
</tr>
<tr class="even">
<td>W (in)</td>
<td>MeV</td>
<td>1.50</td>
<td>1.50</td>
</tr>
<tr class="odd">
<td>W (out)</td>
<td>MeV</td>
<td>1.50</td>
<td>1.50</td>
</tr>
<tr class="even">
<td>Nb of gaps</td>
<td>–</td>
<td>2</td>
<td>2</td>
</tr>
<tr class="odd">
<td><em>L<sub>eff</sub></em></td>
<td>m</td>
<td>0.096</td>
<td>0.096</td>
</tr>
<tr class="even">
<td><em>E<sub>acc</sub></em></td>
<td>MV m<sup>-1</sup></td>
<td>0.83</td>
<td>0.94</td>
</tr>
<tr class="odd">
<td><em>Q</em><sub>0</sub></td>
<td>–</td>
<td>11400</td>
<td>11400</td>
</tr>
<tr class="even">
<td><em>Z</em><sub>sh</sub></td>
<td>MΩ m⁻¹</td>
<td>45.2</td>
<td>45.2</td>
</tr>
<tr class="odd">
<td><em>P</em><sub>0</sub></td>
<td>kW</td>
<td>2</td>
<td>2.4</td>
</tr>
<tr class="even">
<td><em>P</em><sub>beam</sub></td>
<td>kW</td>
<td>0</td>
<td>0</td>
</tr>
<tr class="odd">
<td><em>P</em><sub>total</sub></td>
<td>kW</td>
<td>2</td>
<td>2.4</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main parameters of I1-ME1:RF-CAV-07 and I1-ME1:RF-CAV-01.

 

**Table:** RF losses distribution for both QWRs (I1-ME1:RF-CAV-0

2 and I1-ME1:RF-CAV-0

1).

<table>
<caption>Table: RF losses distribution for both QWRs (I1-ME1:RF-CAV-02 and I1-ME1:RF-CAV-01).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>P
<sub>Tank</sub>
(%)</td>
<td>P
<sub>Lids</sub>
(%)</td>
<td>P
<sub>Stems</sub>
(%)</td>
<td>P
<sub>Drift Tubes</sub>
(%)</td>
<td>P
<sub>Tuners</sub>
(%)</td>
</tr>
<tr class="even">
<td>20.4</td>
<td>10.2</td>
<td>68.6</td>
<td>0.7</td>
<td>0.1</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: RF losses distribution for both QWRs (I1-ME1:RF-CAV-02 and
I1-ME1:RF-CAV-01).

 

**Table:** Interfaces list of the QWR cavities.

<table>
<caption>Table: Interfaces list of the QWR cavities.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th>Interface name</th>
<th>Quantity</th>
<th>Interface type</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Tuner Flange</td>
<td>2</td>
<td>CF63 Rotatable</td>
</tr>
<tr class="even">
<td>Pumping Flange</td>
<td>1</td>
<td>CF100 Threaded Bolt Holes</td>
</tr>
<tr class="odd">
<td>Power Coupler Flange</td>
<td>1</td>
<td>CF63 Rotatable</td>
</tr>
<tr class="even">
<td>Beam Line Connection</td>
<td>2</td>
<td>CF100 Threaded Bolt Holes</td>
</tr>
<tr class="odd">
<td>Gauges + Pickup</td>
<td>4</td>
<td>CF40 Rotatable</td>
</tr>
<tr class="even">
<td>Pre-Vacuum Notch</td>
<td>1</td>
<td>KF16</td>
</tr>
<tr class="odd">
<td>Water Connector</td>
<td>34</td>
<td>3/8″ BSP</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Interfaces list of the QWR cavities.

 

**Table:** Interfaces list for the QWR dynamic tuner.

<table>
<caption>Table: Interfaces list for the QWR dynamic tuner.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Water Supply</td>
<td>2 × 1/4″ BSPT female</td>
</tr>
<tr class="even">
<td>Motor Driver</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td>Potentiometer (Optional)</td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Hardware Interlock Switches</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td>Mechanical Interface to QWR cavity</td>
<td>CF63</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Interfaces list for the QWR dynamic tuner.

 

**Table:** Main parameters of the first section of CH cavities (up to
CHR1).

<table>
<caption>Table: Main parameters of the first section of CH cavities (up to CHR1).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Unit</td>
<td>CH01</td>
<td>CH02</td>
<td>CH03</td>
<td>CH04</td>
<td>CH05</td>
<td>CH06</td>
<td>CH07</td>
<td>CHR1</td>
</tr>
<tr class="even">
<td>W (in)</td>
<td>MeV</td>
<td>1.50</td>
<td>1.68</td>
<td>1.98</td>
<td>2.43</td>
<td>3.02</td>
<td>3.77</td>
<td>4.83</td>
<td>5.85</td>
</tr>
<tr class="odd">
<td>W (out)</td>
<td>MeV</td>
<td>1.68</td>
<td>1.98</td>
<td>2.43</td>
<td>3.02</td>
<td>3.77</td>
<td>4.83</td>
<td>5.85</td>
<td>5.85</td>
</tr>
<tr class="even">
<td>Nb of gap</td>
<td>–</td>
<td>3</td>
<td>4</td>
<td>5</td>
<td>6</td>
<td>7</td>
<td>9</td>
<td>9</td>
<td>5</td>
</tr>
<tr class="odd">
<td><em>L<sub>eff</sub></em></td>
<td>m</td>
<td>0.15</td>
<td>0.21</td>
<td>0.29</td>
<td>0.39</td>
<td>0.51</td>
<td>0.73</td>
<td>0.81</td>
<td>0.47</td>
</tr>
<tr class="even">
<td><em>E<sub>acc</sub></em></td>
<td>MV m<sup>-1</sup></td>
<td>1.36</td>
<td>1.65</td>
<td>1.78</td>
<td>1.77</td>
<td>1.71</td>
<td>1.66</td>
<td>1.46</td>
<td>–</td>
</tr>
<tr class="odd">
<td><em>Q</em><sub>0</sub></td>
<td>–</td>
<td>13000</td>
<td>14800</td>
<td>16400</td>
<td>15800</td>
<td>18200</td>
<td>16400</td>
<td>18000</td>
<td>15800</td>
</tr>
<tr class="even">
<td><em>Z</em><sub>sh</sub></td>
<td>MΩ m⁻¹</td>
<td>47.60</td>
<td>58.40</td>
<td>70.10</td>
<td>74.10</td>
<td>74.40</td>
<td>72.50</td>
<td>67.60</td>
<td>82.00</td>
</tr>
<tr class="odd">
<td><em>P</em><sub>0</sub></td>
<td>kW</td>
<td>8</td>
<td>11.7</td>
<td>15</td>
<td>17</td>
<td>20</td>
<td>27.5</td>
<td>25</td>
<td>7.2</td>
</tr>
<tr class="even">
<td><em>P</em><sub>beam</sub></td>
<td>kW</td>
<td>0.7</td>
<td>1.2</td>
<td>1.8</td>
<td>2.4</td>
<td>3</td>
<td>4.2</td>
<td>4.1</td>
<td>0</td>
</tr>
<tr class="odd">
<td><em>P</em><sub>total</sub></td>
<td>kW</td>
<td>8.7</td>
<td>12.9</td>
<td>16.8</td>
<td>19.4</td>
<td>23</td>
<td>31.7</td>
<td>29.1</td>
<td>7.2</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main parameters of the first section of CH cavities (up to CHR1).

 

**Table:** Main parameters of the second section of CH cavities (from
CH08).

<table>
<caption>Table: Main parameters of the second section of CH cavities (from CH08).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Unit</td>
<td>CH08</td>
<td>CH09</td>
<td>CH10</td>
<td>CH11</td>
<td>CH12</td>
<td>CH13</td>
<td>CH14</td>
<td>CH15</td>
<td>CHR2</td>
</tr>
<tr class="even">
<td>W (in)</td>
<td>MeV</td>
<td>5.85</td>
<td>7.24</td>
<td>8.70</td>
<td>10.14</td>
<td>11.51</td>
<td>12.80</td>
<td>14.09</td>
<td>15.33</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td>W (out)</td>
<td>MeV</td>
<td>7.24</td>
<td>8.70</td>
<td>10.14</td>
<td>11.51</td>
<td>12.80</td>
<td>14.09</td>
<td>15.33</td>
<td>16.59</td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Nb of gaps</td>
<td>–</td>
<td>11</td>
<td>12</td>
<td>12</td>
<td>11</td>
<td>10</td>
<td>10</td>
<td>9</td>
<td>9</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td><em>L<sub>eff</sub></em></td>
<td>m</td>
<td>1.11</td>
<td>1.33</td>
<td>1.43</td>
<td>1.39</td>
<td>1.36</td>
<td>1.43</td>
<td>1.34</td>
<td>1.40</td>
<td>TBD</td>
</tr>
<tr class="even">
<td><em>E<sub>acc</sub></em></td>
<td>MV m<sup>-1</sup></td>
<td>1.44</td>
<td>1.27</td>
<td>1.16</td>
<td>1.14</td>
<td>1.10</td>
<td>1.04</td>
<td>1.07</td>
<td>1.04</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td><em>Q</em><sub>0</sub></td>
<td>–</td>
<td>17300</td>
<td>17300</td>
<td>17400</td>
<td>17400</td>
<td>17200</td>
<td>17200</td>
<td>17200</td>
<td>17200</td>
<td>TBD</td>
</tr>
<tr class="even">
<td><em>Z</em><sub>sh</sub></td>
<td>MΩ m⁻¹</td>
<td>62.89</td>
<td>59.22</td>
<td>54.89</td>
<td>53.00</td>
<td>47.89</td>
<td>46.11</td>
<td>44.44</td>
<td>43.00</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td><em>P</em><sub>0</sub></td>
<td>kW</td>
<td>33.9</td>
<td>33.2</td>
<td>33.2</td>
<td>33.2</td>
<td>33</td>
<td>32.6</td>
<td>32.4</td>
<td>32.2</td>
<td>TBD</td>
</tr>
<tr class="even">
<td><em>P</em><sub>beam</sub></td>
<td>kW</td>
<td>5.6</td>
<td>5.8</td>
<td>5.8</td>
<td>5.5</td>
<td>5.2</td>
<td>5.2</td>
<td>5.0</td>
<td>5.0</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td><em>P</em><sub>total</sub></td>
<td>kW</td>
<td>39.5</td>
<td>39.0</td>
<td>39.0</td>
<td>38.7</td>
<td>38.2</td>
<td>37.8</td>
<td>37.4</td>
<td>37.2</td>
<td>TBD</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main parameters of the second section of CH cavities (from CH08).

 

**Table:** RF Losses distribution per cavity.

<table>
<caption>Table: RF Losses distribution per cavity.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td> </td>
<td>P
<sub>Tank</sub>
(%)</td>
<td>P
<sub>Lids</sub>
(%)</td>
<td>P
<sub>Stems</sub>
(%)</td>
<td>P
<sub>Drift Tubes</sub>
(%)</td>
<td>P
<sub>Tuners</sub>
(%)</td>
</tr>
<tr class="even">
<td>CH01</td>
<td>I1-CH
1:RF-CAV-01</td>
<td>14.7</td>
<td>13.7</td>
<td>69.6</td>
<td>0.5</td>
<td>1.5</td>
</tr>
<tr class="odd">
<td>CH02</td>
<td>I1-CH
2:RF-CAV-01</td>
<td>17.9</td>
<td>11.1</td>
<td>66.7</td>
<td>0.7</td>
<td>3.6</td>
</tr>
<tr class="even">
<td>CH03</td>
<td>I1-CH
3:RF-CAV-01</td>
<td>21.1</td>
<td>7.9</td>
<td>64.9</td>
<td>0.7</td>
<td>5.4</td>
</tr>
<tr class="odd">
<td>CH04</td>
<td>I1-CH
4:RF-CAV-01</td>
<td>23.8</td>
<td>5.2</td>
<td>63.9</td>
<td>0.8</td>
<td>6.3</td>
</tr>
<tr class="even">
<td>CH05</td>
<td>I1-CH
5:RF-CAV-01</td>
<td>27.9</td>
<td>3.3</td>
<td>61.7</td>
<td>0.9</td>
<td>6.2</td>
</tr>
<tr class="odd">
<td>CH06</td>
<td>I1-CH
6:RF-CAV-01</td>
<td>28.7</td>
<td>1.6</td>
<td>63.2</td>
<td>0.9</td>
<td>5.6</td>
</tr>
<tr class="even">
<td>CH07</td>
<td>I1-CH
7:RF-CAV-01</td>
<td>28.5</td>
<td>1.1</td>
<td>63.8</td>
<td>0.9</td>
<td>5.7</td>
</tr>
<tr class="odd">
<td>CHR</td>
<td>I1-ME3:RF-CAV-01</td>
<td>23.7</td>
<td>3.3</td>
<td>67.9</td>
<td>0.9</td>
<td>4.2</td>
</tr>
<tr class="even">
<td>CH08</td>
<td>I1-CH
8:RF-CAV-01</td>
<td>29.8</td>
<td>0.6</td>
<td>64.5</td>
<td>1.2</td>
<td>3.9</td>
</tr>
<tr class="odd">
<td>CH09</td>
<td>I1-CH
9:RF-CAV-01</td>
<td>29.6</td>
<td>0.4</td>
<td>64.8</td>
<td>1.2</td>
<td>4.0</td>
</tr>
<tr class="even">
<td>CH10</td>
<td>I1-CH
10:RF-CAV-01</td>
<td>29.0</td>
<td>0.4</td>
<td>64.9</td>
<td>1.3</td>
<td>4.4</td>
</tr>
<tr class="odd">
<td>CH11</td>
<td>I1-CH
11:RF-CAV-01</td>
<td>28.5</td>
<td>0.3</td>
<td>66.8</td>
<td>1.3</td>
<td>3.1</td>
</tr>
<tr class="even">
<td>CH12</td>
<td>I1-CH
12:RF-CAV-01</td>
<td>27.7</td>
<td>0.3</td>
<td>66.3</td>
<td>1.2</td>
<td>4.5</td>
</tr>
<tr class="odd">
<td>CH13</td>
<td>I1-CH
13:RF-CAV-01</td>
<td>27.5</td>
<td>0.2</td>
<td>66.9</td>
<td>1.3</td>
<td>4.1</td>
</tr>
<tr class="even">
<td>CH14</td>
<td>I1-CH
14:RF-CAV-01</td>
<td>27.1</td>
<td>0.3</td>
<td>68.5</td>
<td>1.6</td>
<td>2.5</td>
</tr>
<tr class="odd">
<td>CH15</td>
<td>I1-CH
15:RF-CAV-01</td>
<td>26.8</td>
<td>0.3</td>
<td>69.2</td>
<td>1.4</td>
<td>2.3</td>
</tr>
<tr class="even">
<td>CHR2</td>
<td>I1-ME2:RF-CAV-01</td>
<td>TBD</td>
<td>TBD</td>
<td>TBD</td>
<td>TBD</td>
<td>TBD</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: RF Losses distribution per cavity.

 

**Table:** Main mechanical characteristics of the CH cavities (up to
CHR1).

<table>
<caption>Table: Main mechanical characteristics of the CH cavities (up to CHR1).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>CH01</td>
<td>CH02</td>
<td>CH03</td>
<td>CH04</td>
<td>CH05</td>
<td>CH06</td>
<td>CH07</td>
<td>CHR1</td>
</tr>
<tr class="even">
<td> </td>
<td>I1-CH
1:RF-CAV-01</td>
<td>I1-CH
2:RF-CAV-01</td>
<td>I1-CH
3:RF-CAV-01</td>
<td>I1-CH
4:RF-CAV-01</td>
<td>I1-CH
5:RF-CAV-01</td>
<td>I1-CH
6:RF-CAV-01</td>
<td>I1-CH
7:RF-CAV-01</td>
<td>I1-ME3:RF-CAV-01</td>
</tr>
<tr class="odd">
<td>Outer diameter (mm)</td>
<td>888</td>
<td>810</td>
<td>789</td>
<td>769</td>
<td>762</td>
<td>756</td>
<td>769</td>
<td>813</td>
</tr>
<tr class="even">
<td>Length (mm, flange to flange)</td>
<td>214</td>
<td>277</td>
<td>353</td>
<td>447</td>
<td>560</td>
<td>782</td>
<td>859</td>
<td>537</td>
</tr>
<tr class="odd">
<td>Weight (kg)</td>
<td>640</td>
<td>640</td>
<td>672</td>
<td>794</td>
<td>900</td>
<td>1112</td>
<td>1214</td>
<td>982</td>
</tr>
<tr class="even">
<td>Overall dimension (mm)</td>
<td>414 × 964 × 964</td>
<td>477 × 900 × 900</td>
<td>557 × 865 × 900</td>
<td>612 × 872 × 630</td>
<td>726 × 892 × 926</td>
<td>947 × 886 × 923</td>
<td>1030 × 899 × 930</td>
<td>702 × 943 × 973</td>
</tr>
<tr class="odd">
<td>Nb of drift tubes</td>
<td>2</td>
<td>3</td>
<td>4</td>
<td>5</td>
<td>6</td>
<td>8</td>
<td>8</td>
<td>4</td>
</tr>
<tr class="even">
<td>Nb of gaps</td>
<td>3</td>
<td>4</td>
<td>5</td>
<td>6</td>
<td>7</td>
<td>9</td>
<td>9</td>
<td>5</td>
</tr>
<tr class="odd">
<td>Nb of CF40 ports</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main mechanical characteristics of the CH cavities (up to CHR1).

 

**Table:** Main mechanical characteristics of the CH cavities (from
CH08).

<table>
<caption>Table: Main mechanical characteristics of the CH cavities (from CH08).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>CH08</td>
<td>CH09</td>
<td>CH10</td>
<td>CH11</td>
<td>CH12</td>
<td>CH13</td>
<td>CH14</td>
<td>CH15</td>
<td>CHR2</td>
</tr>
<tr class="even">
<td> </td>
<td>I1-CH
8:RF-CAV-01</td>
<td>I1-CH
9:RF-CAV-01</td>
<td>I1-CH
10:RF-CAV-01</td>
<td>I1-CH
11:RF-CAV-01</td>
<td>I1-CH
12:RF-CAV-01</td>
<td>I1-CH
13:RF-CAV-01</td>
<td>I1-CH
14:RF-CAV-01</td>
<td>I1-CH
15:RF-CAV-01</td>
<td>I1-ME2:RF-CAV-01</td>
</tr>
<tr class="odd">
<td>Outer diameter (mm)</td>
<td>766</td>
<td>776</td>
<td>786</td>
<td>798</td>
<td>806</td>
<td>812</td>
<td>782</td>
<td>825</td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Length (mm, flange to flange)</td>
<td>1140</td>
<td>1357</td>
<td>1468</td>
<td>1437</td>
<td>1380</td>
<td>1466</td>
<td>1358</td>
<td>1409</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td>Weight (kg)</td>
<td>1460</td>
<td>1802</td>
<td>1973</td>
<td>1968</td>
<td>1922</td>
<td>2052</td>
<td>1834</td>
<td>2017</td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Overall dimension (mm)</td>
<td>1310 × 896 × 938</td>
<td>1567 × 906 × 943</td>
<td>1678 × 916 × 948</td>
<td>1648 × 927 × 958</td>
<td>1590 × 936 × 966</td>
<td>1676 × 942 × 972</td>
<td>1568 × 912 × 946</td>
<td>1619 × 955 × 985</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td>Nb of drift tubes</td>
<td>10</td>
<td>11</td>
<td>11</td>
<td>10</td>
<td>9</td>
<td>9</td>
<td>8</td>
<td>8</td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Nb of gaps</td>
<td>11</td>
<td>12</td>
<td>12</td>
<td>11</td>
<td>10</td>
<td>10</td>
<td>9</td>
<td>9</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td>Nb of CF40 ports</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>TBD</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main mechanical characteristics of the CH cavities (from CH08).

 

**Table:** List of mechanical interfaces for each CH cavities.

<table>
<caption>Table: List of mechanical interfaces for each CH cavities.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th>Interface name</th>
<th>Quantity</th>
<th>Interface type</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Beam input /output flanges</td>
<td>2</td>
<td>CF100 Fixed Blind Threaded Holes</td>
</tr>
<tr class="even">
<td>Power coupler flange (top)</td>
<td>1</td>
<td>CF100 rotatable</td>
</tr>
<tr class="odd">
<td>vacuum pump flange (bottom)</td>
<td>1</td>
<td>CF100 rotatable</td>
</tr>
<tr class="even">
<td>Tuners flanges (left/right)</td>
<td>2</td>
<td>CF100 rotatable</td>
</tr>
<tr class="odd">
<td>Auxiliary flanges (gauges + pick-up)</td>
<td>6</td>
<td>CF40 rotatable</td>
</tr>
<tr class="even">
<td>Water inlet/outlet</td>
<td>2</td>
<td>BSPT female</td>
</tr>
<tr class="odd">
<td>Differential vacuum pumping hole</td>
<td>2</td>
<td>KF16 flange</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: List of mechanical interfaces for each CH cavities.

 

**Table:** Flow requirements for each CH cavity.

<table>
<caption>Table: Flow requirements for each CH cavity.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Cavity</td>
<td>Dissipated power (kW)</td>
<td>Flow (L min⁻¹)</td>
</tr>
<tr class="even">
<td>CH01 (I1-CH
1:RF-CAV-01)</td>
<td>8</td>
<td>22.9</td>
</tr>
<tr class="odd">
<td>CH02 (I1-CH
2:RF-CAV-01)</td>
<td>11.7</td>
<td>33.4</td>
</tr>
<tr class="even">
<td>CH03 (I1-CH
3:RF-CAV-01)</td>
<td>15</td>
<td>42.9</td>
</tr>
<tr class="odd">
<td>CH04 (I1-CH
4:RF-CAV-01)</td>
<td>17</td>
<td>48.6</td>
</tr>
<tr class="even">
<td>CH05 (I1-CH
5:RF-CAV-01)</td>
<td>20</td>
<td>57.1</td>
</tr>
<tr class="odd">
<td>CH06 (I1-CH
6:RF-CAV-01)</td>
<td>27.5</td>
<td>78.6</td>
</tr>
<tr class="even">
<td>CH07 (I1-CH
7:RF-CAV-01)</td>
<td>25</td>
<td>71.4</td>
</tr>
<tr class="odd">
<td>CHR1 (I1-ME3:RF-CAV-01)</td>
<td>7.2</td>
<td>20.6</td>
</tr>
<tr class="even">
<td>CH08 (I1-CH
8:RF-CAV-01)</td>
<td>33.9</td>
<td>96.9</td>
</tr>
<tr class="odd">
<td>CH09 (I1-CH
9:RF-CAV-01)</td>
<td>33.2</td>
<td>94.9</td>
</tr>
<tr class="even">
<td>CH10 (I1-CH
10:RF-CAV-01)</td>
<td>33.2</td>
<td>94.9</td>
</tr>
<tr class="odd">
<td>CH11 (I1-CH
11:RF-CAV-01)</td>
<td>33.2</td>
<td>94.9</td>
</tr>
<tr class="even">
<td>CH12 (I1-CH
12:RF-CAV-01)</td>
<td>33</td>
<td>94.3</td>
</tr>
<tr class="odd">
<td>CH13 (I1-CH
13:RF-CAV-01)</td>
<td>32.6</td>
<td>93.1</td>
</tr>
<tr class="even">
<td>CH14 (I1-CH
14:RF-CAV-01)</td>
<td>32.4</td>
<td>92.6</td>
</tr>
<tr class="odd">
<td>CH15 (I1-CH
15:RF-CAV-01)</td>
<td>32.2</td>
<td>92.0</td>
</tr>
<tr class="even">
<td>CHR2 (I1-ME2:RF-CAV-01)</td>
<td>TBD</td>
<td>TBD</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Flow requirements for each CH cavity.

 

**Table:** Specifications of distances for the static tuners from CH01
(I1-CH

7:RF-CAV-01) to CH07 (I1-CH

1:RF-CAV-01).

<table>
<caption>Table: Specifications of distances for the static tuners from CH01 (I1-CH7:RF-CAV-01) to CH07 (I1-CH1:RF-CAV-01).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Distance between working point</td>
<td>Distance between working</td>
</tr>
<tr class="even">
<td> </td>
<td>and beam axis in mm (D1)</td>
<td>point to flange in mm (D2)</td>
</tr>
<tr class="odd">
<td>CH01 (I1-CH
1:RF-CAV-01)</td>
<td>105.29</td>
<td>376.46</td>
</tr>
<tr class="even">
<td>CH02 (I1-CH
2:RF-CAV-01)</td>
<td>122.29</td>
<td>327.69</td>
</tr>
<tr class="odd">
<td>CH03 (I1-CH
3:RF-CAV-01)</td>
<td>133.63</td>
<td>298.55</td>
</tr>
<tr class="even">
<td>CH04 (I1-CH
4:RF-CAV-01)</td>
<td>124.49</td>
<td>297.7</td>
</tr>
<tr class="odd">
<td>CH05 (I1-CH
5:RF-CAV-01)</td>
<td>121.71</td>
<td>297.05</td>
</tr>
<tr class="even">
<td>CH06 (I1-CH
6:RF-CAV-01)</td>
<td>114.18</td>
<td>301.68</td>
</tr>
<tr class="odd">
<td>CH07 (I1-CH
7:RF-CAV-01)</td>
<td>123.74</td>
<td>298.71</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Specifications of distances for the static tuners from CH01
(I1-CH7:RF-CAV-01) to CH07 (I1-CH1:RF-CAV-01).

 

**Table:** Main specifications for the CH dynamic tuner.

<table>
<caption>Table: Main specifications for the CH dynamic tuner.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Stroke</td>
<td>100mm</td>
</tr>
<tr class="even">
<td>Max plunger diameter</td>
<td>95mm</td>
</tr>
<tr class="odd">
<td>RF Contacts material</td>
<td>Cu-Be, Ag Plated</td>
</tr>
<tr class="even">
<td>Bounding Box (Without plunger)</td>
<td>300 × 385 × 300mm</td>
</tr>
<tr class="odd">
<td>Weight</td>
<td>35 kg</td>
</tr>
<tr class="even">
<td>Water Flow</td>
<td>Max 10L min⁻¹ @ 5bar</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main specifications for the CH dynamic tuner.

 

**Table:** Interfaces list for the CH dynamic tuner.

<table>
<caption>Table: Interfaces list for the CH dynamic tuner.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Plunger Interface</td>
<td>CF100</td>
</tr>
<tr class="even">
<td>Water Supply</td>
<td>2 × 1/4″ BSPT female</td>
</tr>
<tr class="odd">
<td>Motor Driver</td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Potentiometer (Optional)</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td>Hardware Interlock Switches</td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Mechanical Interface to CH cavity</td>
<td>CF100</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Interfaces list for the CH dynamic tuner.

 

**Table:** Main parameters of the RFQ cavity (I1-RFQ).

<table>
<caption>Table: Main parameters of the RFQ cavity (I1-RFQ).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Unit</td>
<td>RFQ</td>
</tr>
<tr class="even">
<td>W (in)</td>
<td>MeV</td>
<td>0.03</td>
</tr>
<tr class="odd">
<td>W (out)</td>
<td>MeV</td>
<td>1.50</td>
</tr>
<tr class="even">
<td>Length</td>
<td>m</td>
<td>4</td>
</tr>
<tr class="odd">
<td>Number of cell</td>
<td>–</td>
<td>244</td>
</tr>
<tr class="even">
<td>Inter-Rod voltage</td>
<td>kV</td>
<td>44</td>
</tr>
<tr class="odd">
<td>Kilpatrick Factor</td>
<td>–</td>
<td>1.05</td>
</tr>
<tr class="even">
<td>Rp</td>
<td>kΩ m⁻¹</td>
<td>73</td>
</tr>
<tr class="odd">
<td><em>Q</em><sub>0</sub></td>
<td>–</td>
<td>4000</td>
</tr>
<tr class="even">
<td><em>P</em><sub>0</sub></td>
<td>kW</td>
<td>102.1</td>
</tr>
<tr class="odd">
<td><em>P</em><sub>beam</sub></td>
<td>kW</td>
<td>5.9</td>
</tr>
<tr class="even">
<td><em>P</em><sub>total</sub></td>
<td>kW</td>
<td>108</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main parameters of the RFQ cavity (I1-RFQ).

 

**Table:** Mechanical specifications of the RFQ cavity (I1-RFQ).

<table>
<caption>Table: Mechanical specifications of the RFQ cavity (I1-RFQ).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Specification</td>
<td>Value</td>
</tr>
<tr class="even">
<td>Total length (mm)</td>
<td>4154.50</td>
</tr>
<tr class="odd">
<td>Rod length (mm)</td>
<td>4013.66</td>
</tr>
<tr class="even">
<td>Overall dimensions (mm)</td>
<td>L = 4155; H = 1815; W = 830 (without gate valves)</td>
</tr>
<tr class="odd">
<td>Approx. weight (t)</td>
<td>2</td>
</tr>
<tr class="even">
<td>Stems</td>
<td>40</td>
</tr>
<tr class="odd">
<td>Rod Elements</td>
<td>12</td>
</tr>
<tr class="even">
<td>Tuning Plates</td>
<td>39</td>
</tr>
<tr class="odd">
<td>Cooling Flow for <strong>TBD: inline26087, img1</strong> = 5K (L min⁻¹)</td>
<td>345</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Mechanical specifications of the RFQ cavity (I1-RFQ).

 

**Table:** Interfaces specifications of the RFQ cavity (I1-RFQ).

<table>
<caption>Table: Interfaces specifications of the RFQ cavity (I1-RFQ).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Interface</td>
<td>Qty</td>
<td>Type</td>
</tr>
<tr class="even">
<td>Tuner</td>
<td>2</td>
<td>CF63</td>
</tr>
<tr class="odd">
<td>Power Coupler</td>
<td>1</td>
<td>CF100</td>
</tr>
<tr class="even">
<td>Pick-up and RGA</td>
<td>4</td>
<td>CF40</td>
</tr>
<tr class="odd">
<td>Auxiliary vacuum ports</td>
<td>4</td>
<td>KF40</td>
</tr>
<tr class="even">
<td>Main vacuum pumping ports</td>
<td>3</td>
<td>CF100</td>
</tr>
<tr class="odd">
<td>Beam input</td>
<td>1</td>
<td>Tailor-made with elastomer seal</td>
</tr>
<tr class="even">
<td>Beam output</td>
<td>1</td>
<td>Tailor-made with elastomer seal</td>
</tr>
<tr class="odd">
<td>Rods manifold</td>
<td>2</td>
<td>1″ BSPT male</td>
</tr>
<tr class="even">
<td>Stems manifold</td>
<td>2</td>
<td>1″ BSPT male</td>
</tr>
<tr class="odd">
<td>Tuning Plates manifold</td>
<td>2</td>
<td>1″ BSPT male</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Interfaces specifications of the RFQ cavity (I1-RFQ).

 

**Table:** Main parameters of I1-ME1:RF-CAV-0

7 and I1-ME1:RF-CAV-0

1\.

<table>
<caption>Table: Main parameters of I1-ME1:RF-CAV-07 and I1-ME1:RF-CAV-01.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Unit</td>
<td>QWR1</td>
<td>QWR2</td>
</tr>
<tr class="even">
<td>W (in)</td>
<td>MeV</td>
<td>1.50</td>
<td>1.50</td>
</tr>
<tr class="odd">
<td>W (out)</td>
<td>MeV</td>
<td>1.50</td>
<td>1.50</td>
</tr>
<tr class="even">
<td>Nb of gaps</td>
<td>–</td>
<td>2</td>
<td>2</td>
</tr>
<tr class="odd">
<td><em>L<sub>eff</sub></em></td>
<td>m</td>
<td>0.096</td>
<td>0.096</td>
</tr>
<tr class="even">
<td><em>E<sub>acc</sub></em></td>
<td>MV m<sup>-1</sup></td>
<td>0.83</td>
<td>0.94</td>
</tr>
<tr class="odd">
<td><em>Q</em><sub>0</sub></td>
<td>–</td>
<td>11400</td>
<td>11400</td>
</tr>
<tr class="even">
<td><em>Z</em><sub>sh</sub></td>
<td>MΩ m⁻¹</td>
<td>45.2</td>
<td>45.2</td>
</tr>
<tr class="odd">
<td><em>P</em><sub>0</sub></td>
<td>kW</td>
<td>2</td>
<td>2.4</td>
</tr>
<tr class="even">
<td><em>P</em><sub>beam</sub></td>
<td>kW</td>
<td>0</td>
<td>0</td>
</tr>
<tr class="odd">
<td><em>P</em><sub>total</sub></td>
<td>kW</td>
<td>2</td>
<td>2.4</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main parameters of I1-ME1:RF-CAV-07 and I1-ME1:RF-CAV-01.

 

**Table:** RF losses distribution for both QWRs (I1-ME1:RF-CAV-0

2 and I1-ME1:RF-CAV-0

1).

<table>
<caption>Table: RF losses distribution for both QWRs (I1-ME1:RF-CAV-02 and I1-ME1:RF-CAV-01).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>P
<sub>Tank</sub>
(%)</td>
<td>P
<sub>Lids</sub>
(%)</td>
<td>P
<sub>Stems</sub>
(%)</td>
<td>P
<sub>Drift Tubes</sub>
(%)</td>
<td>P
<sub>Tuners</sub>
(%)</td>
</tr>
<tr class="even">
<td>20.4</td>
<td>10.2</td>
<td>68.6</td>
<td>0.7</td>
<td>0.1</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: RF losses distribution for both QWRs (I1-ME1:RF-CAV-02 and
I1-ME1:RF-CAV-01).

 

**Table:** Interfaces list of the QWR cavities.

<table>
<caption>Table: Interfaces list of the QWR cavities.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th>Interface name</th>
<th>Quantity</th>
<th>Interface type</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Tuner Flange</td>
<td>2</td>
<td>CF63 Rotatable</td>
</tr>
<tr class="even">
<td>Pumping Flange</td>
<td>1</td>
<td>CF100 Threaded Bolt Holes</td>
</tr>
<tr class="odd">
<td>Power Coupler Flange</td>
<td>1</td>
<td>CF63 Rotatable</td>
</tr>
<tr class="even">
<td>Beam Line Connection</td>
<td>2</td>
<td>CF100 Threaded Bolt Holes</td>
</tr>
<tr class="odd">
<td>Gauges + Pickup</td>
<td>4</td>
<td>CF40 Rotatable</td>
</tr>
<tr class="even">
<td>Pre-Vacuum Notch</td>
<td>1</td>
<td>KF16</td>
</tr>
<tr class="odd">
<td>Water Connector</td>
<td>34</td>
<td>3/8″ BSP</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Interfaces list of the QWR cavities.

 

**Table:** Interfaces list for the QWR dynamic tuner.

<table>
<caption>Table: Interfaces list for the QWR dynamic tuner.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Water Supply</td>
<td>2 × 1/4″ BSPT female</td>
</tr>
<tr class="even">
<td>Motor Driver</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td>Potentiometer (Optional)</td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Hardware Interlock Switches</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td>Mechanical Interface to QWR cavity</td>
<td>CF63</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Interfaces list for the QWR dynamic tuner.

 

**Table:** Main parameters of the first section of CH cavities (up to
CHR1).

<table>
<caption>Table: Main parameters of the first section of CH cavities (up to CHR1).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Unit</td>
<td>CH01</td>
<td>CH02</td>
<td>CH03</td>
<td>CH04</td>
<td>CH05</td>
<td>CH06</td>
<td>CH07</td>
<td>CHR1</td>
</tr>
<tr class="even">
<td>W (in)</td>
<td>MeV</td>
<td>1.50</td>
<td>1.68</td>
<td>1.98</td>
<td>2.43</td>
<td>3.02</td>
<td>3.77</td>
<td>4.83</td>
<td>5.85</td>
</tr>
<tr class="odd">
<td>W (out)</td>
<td>MeV</td>
<td>1.68</td>
<td>1.98</td>
<td>2.43</td>
<td>3.02</td>
<td>3.77</td>
<td>4.83</td>
<td>5.85</td>
<td>5.85</td>
</tr>
<tr class="even">
<td>Nb of gap</td>
<td>–</td>
<td>3</td>
<td>4</td>
<td>5</td>
<td>6</td>
<td>7</td>
<td>9</td>
<td>9</td>
<td>5</td>
</tr>
<tr class="odd">
<td><em>L<sub>eff</sub></em></td>
<td>m</td>
<td>0.15</td>
<td>0.21</td>
<td>0.29</td>
<td>0.39</td>
<td>0.51</td>
<td>0.73</td>
<td>0.81</td>
<td>0.47</td>
</tr>
<tr class="even">
<td><em>E<sub>acc</sub></em></td>
<td>MV m<sup>-1</sup></td>
<td>1.36</td>
<td>1.65</td>
<td>1.78</td>
<td>1.77</td>
<td>1.71</td>
<td>1.66</td>
<td>1.46</td>
<td>–</td>
</tr>
<tr class="odd">
<td><em>Q</em><sub>0</sub></td>
<td>–</td>
<td>13000</td>
<td>14800</td>
<td>16400</td>
<td>15800</td>
<td>18200</td>
<td>16400</td>
<td>18000</td>
<td>15800</td>
</tr>
<tr class="even">
<td><em>Z</em><sub>sh</sub></td>
<td>MΩ m⁻¹</td>
<td>47.60</td>
<td>58.40</td>
<td>70.10</td>
<td>74.10</td>
<td>74.40</td>
<td>72.50</td>
<td>67.60</td>
<td>82.00</td>
</tr>
<tr class="odd">
<td><em>P</em><sub>0</sub></td>
<td>kW</td>
<td>8</td>
<td>11.7</td>
<td>15</td>
<td>17</td>
<td>20</td>
<td>27.5</td>
<td>25</td>
<td>7.2</td>
</tr>
<tr class="even">
<td><em>P</em><sub>beam</sub></td>
<td>kW</td>
<td>0.7</td>
<td>1.2</td>
<td>1.8</td>
<td>2.4</td>
<td>3</td>
<td>4.2</td>
<td>4.1</td>
<td>0</td>
</tr>
<tr class="odd">
<td><em>P</em><sub>total</sub></td>
<td>kW</td>
<td>8.7</td>
<td>12.9</td>
<td>16.8</td>
<td>19.4</td>
<td>23</td>
<td>31.7</td>
<td>29.1</td>
<td>7.2</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main parameters of the first section of CH cavities (up to CHR1).

 

**Table:** Main parameters of the second section of CH cavities (from
CH08).

<table>
<caption>Table: Main parameters of the second section of CH cavities (from CH08).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Unit</td>
<td>CH08</td>
<td>CH09</td>
<td>CH10</td>
<td>CH11</td>
<td>CH12</td>
<td>CH13</td>
<td>CH14</td>
<td>CH15</td>
<td>CHR2</td>
</tr>
<tr class="even">
<td>W (in)</td>
<td>MeV</td>
<td>5.85</td>
<td>7.24</td>
<td>8.70</td>
<td>10.14</td>
<td>11.51</td>
<td>12.80</td>
<td>14.09</td>
<td>15.33</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td>W (out)</td>
<td>MeV</td>
<td>7.24</td>
<td>8.70</td>
<td>10.14</td>
<td>11.51</td>
<td>12.80</td>
<td>14.09</td>
<td>15.33</td>
<td>16.59</td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Nb of gaps</td>
<td>–</td>
<td>11</td>
<td>12</td>
<td>12</td>
<td>11</td>
<td>10</td>
<td>10</td>
<td>9</td>
<td>9</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td><em>L<sub>eff</sub></em></td>
<td>m</td>
<td>1.11</td>
<td>1.33</td>
<td>1.43</td>
<td>1.39</td>
<td>1.36</td>
<td>1.43</td>
<td>1.34</td>
<td>1.40</td>
<td>TBD</td>
</tr>
<tr class="even">
<td><em>E<sub>acc</sub></em></td>
<td>MV m<sup>-1</sup></td>
<td>1.44</td>
<td>1.27</td>
<td>1.16</td>
<td>1.14</td>
<td>1.10</td>
<td>1.04</td>
<td>1.07</td>
<td>1.04</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td><em>Q</em><sub>0</sub></td>
<td>–</td>
<td>17300</td>
<td>17300</td>
<td>17400</td>
<td>17400</td>
<td>17200</td>
<td>17200</td>
<td>17200</td>
<td>17200</td>
<td>TBD</td>
</tr>
<tr class="even">
<td><em>Z</em><sub>sh</sub></td>
<td>MΩ m⁻¹</td>
<td>62.89</td>
<td>59.22</td>
<td>54.89</td>
<td>53.00</td>
<td>47.89</td>
<td>46.11</td>
<td>44.44</td>
<td>43.00</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td><em>P</em><sub>0</sub></td>
<td>kW</td>
<td>33.9</td>
<td>33.2</td>
<td>33.2</td>
<td>33.2</td>
<td>33</td>
<td>32.6</td>
<td>32.4</td>
<td>32.2</td>
<td>TBD</td>
</tr>
<tr class="even">
<td><em>P</em><sub>beam</sub></td>
<td>kW</td>
<td>5.6</td>
<td>5.8</td>
<td>5.8</td>
<td>5.5</td>
<td>5.2</td>
<td>5.2</td>
<td>5.0</td>
<td>5.0</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td><em>P</em><sub>total</sub></td>
<td>kW</td>
<td>39.5</td>
<td>39.0</td>
<td>39.0</td>
<td>38.7</td>
<td>38.2</td>
<td>37.8</td>
<td>37.4</td>
<td>37.2</td>
<td>TBD</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main parameters of the second section of CH cavities (from CH08).

 

**Table:** RF Losses distribution per cavity.

<table>
<caption>Table: RF Losses distribution per cavity.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td> </td>
<td>P
<sub>Tank</sub>
(%)</td>
<td>P
<sub>Lids</sub>
(%)</td>
<td>P
<sub>Stems</sub>
(%)</td>
<td>P
<sub>Drift Tubes</sub>
(%)</td>
<td>P
<sub>Tuners</sub>
(%)</td>
</tr>
<tr class="even">
<td>CH01</td>
<td>I1-CH
1:RF-CAV-01</td>
<td>14.7</td>
<td>13.7</td>
<td>69.6</td>
<td>0.5</td>
<td>1.5</td>
</tr>
<tr class="odd">
<td>CH02</td>
<td>I1-CH
2:RF-CAV-01</td>
<td>17.9</td>
<td>11.1</td>
<td>66.7</td>
<td>0.7</td>
<td>3.6</td>
</tr>
<tr class="even">
<td>CH03</td>
<td>I1-CH
3:RF-CAV-01</td>
<td>21.1</td>
<td>7.9</td>
<td>64.9</td>
<td>0.7</td>
<td>5.4</td>
</tr>
<tr class="odd">
<td>CH04</td>
<td>I1-CH
4:RF-CAV-01</td>
<td>23.8</td>
<td>5.2</td>
<td>63.9</td>
<td>0.8</td>
<td>6.3</td>
</tr>
<tr class="even">
<td>CH05</td>
<td>I1-CH
5:RF-CAV-01</td>
<td>27.9</td>
<td>3.3</td>
<td>61.7</td>
<td>0.9</td>
<td>6.2</td>
</tr>
<tr class="odd">
<td>CH06</td>
<td>I1-CH
6:RF-CAV-01</td>
<td>28.7</td>
<td>1.6</td>
<td>63.2</td>
<td>0.9</td>
<td>5.6</td>
</tr>
<tr class="even">
<td>CH07</td>
<td>I1-CH
7:RF-CAV-01</td>
<td>28.5</td>
<td>1.1</td>
<td>63.8</td>
<td>0.9</td>
<td>5.7</td>
</tr>
<tr class="odd">
<td>CHR</td>
<td>I1-ME3:RF-CAV-01</td>
<td>23.7</td>
<td>3.3</td>
<td>67.9</td>
<td>0.9</td>
<td>4.2</td>
</tr>
<tr class="even">
<td>CH08</td>
<td>I1-CH
8:RF-CAV-01</td>
<td>29.8</td>
<td>0.6</td>
<td>64.5</td>
<td>1.2</td>
<td>3.9</td>
</tr>
<tr class="odd">
<td>CH09</td>
<td>I1-CH
9:RF-CAV-01</td>
<td>29.6</td>
<td>0.4</td>
<td>64.8</td>
<td>1.2</td>
<td>4.0</td>
</tr>
<tr class="even">
<td>CH10</td>
<td>I1-CH
10:RF-CAV-01</td>
<td>29.0</td>
<td>0.4</td>
<td>64.9</td>
<td>1.3</td>
<td>4.4</td>
</tr>
<tr class="odd">
<td>CH11</td>
<td>I1-CH
11:RF-CAV-01</td>
<td>28.5</td>
<td>0.3</td>
<td>66.8</td>
<td>1.3</td>
<td>3.1</td>
</tr>
<tr class="even">
<td>CH12</td>
<td>I1-CH
12:RF-CAV-01</td>
<td>27.7</td>
<td>0.3</td>
<td>66.3</td>
<td>1.2</td>
<td>4.5</td>
</tr>
<tr class="odd">
<td>CH13</td>
<td>I1-CH
13:RF-CAV-01</td>
<td>27.5</td>
<td>0.2</td>
<td>66.9</td>
<td>1.3</td>
<td>4.1</td>
</tr>
<tr class="even">
<td>CH14</td>
<td>I1-CH
14:RF-CAV-01</td>
<td>27.1</td>
<td>0.3</td>
<td>68.5</td>
<td>1.6</td>
<td>2.5</td>
</tr>
<tr class="odd">
<td>CH15</td>
<td>I1-CH
15:RF-CAV-01</td>
<td>26.8</td>
<td>0.3</td>
<td>69.2</td>
<td>1.4</td>
<td>2.3</td>
</tr>
<tr class="even">
<td>CHR2</td>
<td>I1-ME2:RF-CAV-01</td>
<td>TBD</td>
<td>TBD</td>
<td>TBD</td>
<td>TBD</td>
<td>TBD</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: RF Losses distribution per cavity.

 

**Table:** Main mechanical characteristics of the CH cavities (up to
CHR1).

<table>
<caption>Table: Main mechanical characteristics of the CH cavities (up to CHR1).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>CH01</td>
<td>CH02</td>
<td>CH03</td>
<td>CH04</td>
<td>CH05</td>
<td>CH06</td>
<td>CH07</td>
<td>CHR1</td>
</tr>
<tr class="even">
<td> </td>
<td>I1-CH
1:RF-CAV-01</td>
<td>I1-CH
2:RF-CAV-01</td>
<td>I1-CH
3:RF-CAV-01</td>
<td>I1-CH
4:RF-CAV-01</td>
<td>I1-CH
5:RF-CAV-01</td>
<td>I1-CH
6:RF-CAV-01</td>
<td>I1-CH
7:RF-CAV-01</td>
<td>I1-ME3:RF-CAV-01</td>
</tr>
<tr class="odd">
<td>Outer diameter (mm)</td>
<td>888</td>
<td>810</td>
<td>789</td>
<td>769</td>
<td>762</td>
<td>756</td>
<td>769</td>
<td>813</td>
</tr>
<tr class="even">
<td>Length (mm, flange to flange)</td>
<td>214</td>
<td>277</td>
<td>353</td>
<td>447</td>
<td>560</td>
<td>782</td>
<td>859</td>
<td>537</td>
</tr>
<tr class="odd">
<td>Weight (kg)</td>
<td>640</td>
<td>640</td>
<td>672</td>
<td>794</td>
<td>900</td>
<td>1112</td>
<td>1214</td>
<td>982</td>
</tr>
<tr class="even">
<td>Overall dimension (mm)</td>
<td>414 × 964 × 964</td>
<td>477 × 900 × 900</td>
<td>557 × 865 × 900</td>
<td>612 × 872 × 630</td>
<td>726 × 892 × 926</td>
<td>947 × 886 × 923</td>
<td>1030 × 899 × 930</td>
<td>702 × 943 × 973</td>
</tr>
<tr class="odd">
<td>Nb of drift tubes</td>
<td>2</td>
<td>3</td>
<td>4</td>
<td>5</td>
<td>6</td>
<td>8</td>
<td>8</td>
<td>4</td>
</tr>
<tr class="even">
<td>Nb of gaps</td>
<td>3</td>
<td>4</td>
<td>5</td>
<td>6</td>
<td>7</td>
<td>9</td>
<td>9</td>
<td>5</td>
</tr>
<tr class="odd">
<td>Nb of CF40 ports</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main mechanical characteristics of the CH cavities (up to CHR1).

 

**Table:** Main mechanical characteristics of the CH cavities (from
CH08).

<table>
<caption>Table: Main mechanical characteristics of the CH cavities (from CH08).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>CH08</td>
<td>CH09</td>
<td>CH10</td>
<td>CH11</td>
<td>CH12</td>
<td>CH13</td>
<td>CH14</td>
<td>CH15</td>
<td>CHR2</td>
</tr>
<tr class="even">
<td> </td>
<td>I1-CH
8:RF-CAV-01</td>
<td>I1-CH
9:RF-CAV-01</td>
<td>I1-CH
10:RF-CAV-01</td>
<td>I1-CH
11:RF-CAV-01</td>
<td>I1-CH
12:RF-CAV-01</td>
<td>I1-CH
13:RF-CAV-01</td>
<td>I1-CH
14:RF-CAV-01</td>
<td>I1-CH
15:RF-CAV-01</td>
<td>I1-ME2:RF-CAV-01</td>
</tr>
<tr class="odd">
<td>Outer diameter (mm)</td>
<td>766</td>
<td>776</td>
<td>786</td>
<td>798</td>
<td>806</td>
<td>812</td>
<td>782</td>
<td>825</td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Length (mm, flange to flange)</td>
<td>1140</td>
<td>1357</td>
<td>1468</td>
<td>1437</td>
<td>1380</td>
<td>1466</td>
<td>1358</td>
<td>1409</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td>Weight (kg)</td>
<td>1460</td>
<td>1802</td>
<td>1973</td>
<td>1968</td>
<td>1922</td>
<td>2052</td>
<td>1834</td>
<td>2017</td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Overall dimension (mm)</td>
<td>1310 × 896 × 938</td>
<td>1567 × 906 × 943</td>
<td>1678 × 916 × 948</td>
<td>1648 × 927 × 958</td>
<td>1590 × 936 × 966</td>
<td>1676 × 942 × 972</td>
<td>1568 × 912 × 946</td>
<td>1619 × 955 × 985</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td>Nb of drift tubes</td>
<td>10</td>
<td>11</td>
<td>11</td>
<td>10</td>
<td>9</td>
<td>9</td>
<td>8</td>
<td>8</td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Nb of gaps</td>
<td>11</td>
<td>12</td>
<td>12</td>
<td>11</td>
<td>10</td>
<td>10</td>
<td>9</td>
<td>9</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td>Nb of CF40 ports</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>6</td>
<td>TBD</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main mechanical characteristics of the CH cavities (from CH08).

 

**Table:** List of mechanical interfaces for each CH cavities.

<table>
<caption>Table: List of mechanical interfaces for each CH cavities.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th>Interface name</th>
<th>Quantity</th>
<th>Interface type</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Beam input /output flanges</td>
<td>2</td>
<td>CF100 Fixed Blind Threaded Holes</td>
</tr>
<tr class="even">
<td>Power coupler flange (top)</td>
<td>1</td>
<td>CF100 rotatable</td>
</tr>
<tr class="odd">
<td>vacuum pump flange (bottom)</td>
<td>1</td>
<td>CF100 rotatable</td>
</tr>
<tr class="even">
<td>Tuners flanges (left/right)</td>
<td>2</td>
<td>CF100 rotatable</td>
</tr>
<tr class="odd">
<td>Auxiliary flanges (gauges + pick-up)</td>
<td>6</td>
<td>CF40 rotatable</td>
</tr>
<tr class="even">
<td>Water inlet/outlet</td>
<td>2</td>
<td>BSPT female</td>
</tr>
<tr class="odd">
<td>Differential vacuum pumping hole</td>
<td>2</td>
<td>KF16 flange</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: List of mechanical interfaces for each CH cavities.

 

**Table:** Flow requirements for each CH cavity.

<table>
<caption>Table: Flow requirements for each CH cavity.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Cavity</td>
<td>Dissipated power (kW)</td>
<td>Flow (L min⁻¹)</td>
</tr>
<tr class="even">
<td>CH01 (I1-CH
1:RF-CAV-01)</td>
<td>8</td>
<td>22.9</td>
</tr>
<tr class="odd">
<td>CH02 (I1-CH
2:RF-CAV-01)</td>
<td>11.7</td>
<td>33.4</td>
</tr>
<tr class="even">
<td>CH03 (I1-CH
3:RF-CAV-01)</td>
<td>15</td>
<td>42.9</td>
</tr>
<tr class="odd">
<td>CH04 (I1-CH
4:RF-CAV-01)</td>
<td>17</td>
<td>48.6</td>
</tr>
<tr class="even">
<td>CH05 (I1-CH
5:RF-CAV-01)</td>
<td>20</td>
<td>57.1</td>
</tr>
<tr class="odd">
<td>CH06 (I1-CH
6:RF-CAV-01)</td>
<td>27.5</td>
<td>78.6</td>
</tr>
<tr class="even">
<td>CH07 (I1-CH
7:RF-CAV-01)</td>
<td>25</td>
<td>71.4</td>
</tr>
<tr class="odd">
<td>CHR1 (I1-ME3:RF-CAV-01)</td>
<td>7.2</td>
<td>20.6</td>
</tr>
<tr class="even">
<td>CH08 (I1-CH
8:RF-CAV-01)</td>
<td>33.9</td>
<td>96.9</td>
</tr>
<tr class="odd">
<td>CH09 (I1-CH
9:RF-CAV-01)</td>
<td>33.2</td>
<td>94.9</td>
</tr>
<tr class="even">
<td>CH10 (I1-CH
10:RF-CAV-01)</td>
<td>33.2</td>
<td>94.9</td>
</tr>
<tr class="odd">
<td>CH11 (I1-CH
11:RF-CAV-01)</td>
<td>33.2</td>
<td>94.9</td>
</tr>
<tr class="even">
<td>CH12 (I1-CH
12:RF-CAV-01)</td>
<td>33</td>
<td>94.3</td>
</tr>
<tr class="odd">
<td>CH13 (I1-CH
13:RF-CAV-01)</td>
<td>32.6</td>
<td>93.1</td>
</tr>
<tr class="even">
<td>CH14 (I1-CH
14:RF-CAV-01)</td>
<td>32.4</td>
<td>92.6</td>
</tr>
<tr class="odd">
<td>CH15 (I1-CH
15:RF-CAV-01)</td>
<td>32.2</td>
<td>92.0</td>
</tr>
<tr class="even">
<td>CHR2 (I1-ME2:RF-CAV-01)</td>
<td>TBD</td>
<td>TBD</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Flow requirements for each CH cavity.

 

**Table:** Specifications of distances for the static tuners from CH01
(I1-CH

7:RF-CAV-01) to CH07 (I1-CH

1:RF-CAV-01).

<table>
<caption>Table: Specifications of distances for the static tuners from CH01 (I1-CH7:RF-CAV-01) to CH07 (I1-CH1:RF-CAV-01).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Distance between working point</td>
<td>Distance between working</td>
</tr>
<tr class="even">
<td> </td>
<td>and beam axis in mm (D1)</td>
<td>point to flange in mm (D2)</td>
</tr>
<tr class="odd">
<td>CH01 (I1-CH
1:RF-CAV-01)</td>
<td>105.29</td>
<td>376.46</td>
</tr>
<tr class="even">
<td>CH02 (I1-CH
2:RF-CAV-01)</td>
<td>122.29</td>
<td>327.69</td>
</tr>
<tr class="odd">
<td>CH03 (I1-CH
3:RF-CAV-01)</td>
<td>133.63</td>
<td>298.55</td>
</tr>
<tr class="even">
<td>CH04 (I1-CH
4:RF-CAV-01)</td>
<td>124.49</td>
<td>297.7</td>
</tr>
<tr class="odd">
<td>CH05 (I1-CH
5:RF-CAV-01)</td>
<td>121.71</td>
<td>297.05</td>
</tr>
<tr class="even">
<td>CH06 (I1-CH
6:RF-CAV-01)</td>
<td>114.18</td>
<td>301.68</td>
</tr>
<tr class="odd">
<td>CH07 (I1-CH
7:RF-CAV-01)</td>
<td>123.74</td>
<td>298.71</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Specifications of distances for the static tuners from CH01
(I1-CH7:RF-CAV-01) to CH07 (I1-CH1:RF-CAV-01).

 

**Table:** Main specifications for the CH dynamic tuner.

<table>
<caption>Table: Main specifications for the CH dynamic tuner.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Stroke</td>
<td>100mm</td>
</tr>
<tr class="even">
<td>Max plunger diameter</td>
<td>95mm</td>
</tr>
<tr class="odd">
<td>RF Contacts material</td>
<td>Cu-Be, Ag Plated</td>
</tr>
<tr class="even">
<td>Bounding Box (Without plunger)</td>
<td>300 × 385 × 300mm</td>
</tr>
<tr class="odd">
<td>Weight</td>
<td>35 kg</td>
</tr>
<tr class="even">
<td>Water Flow</td>
<td>Max 10L min⁻¹ @ 5bar</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main specifications for the CH dynamic tuner.

 

**Table:** Interfaces list for the CH dynamic tuner.

<table>
<caption>Table: Interfaces list for the CH dynamic tuner.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Plunger Interface</td>
<td>CF100</td>
</tr>
<tr class="even">
<td>Water Supply</td>
<td>2 × 1/4″ BSPT female</td>
</tr>
<tr class="odd">
<td>Motor Driver</td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Potentiometer (Optional)</td>
<td>TBD</td>
</tr>
<tr class="odd">
<td>Hardware Interlock Switches</td>
<td>TBD</td>
</tr>
<tr class="even">
<td>Mechanical Interface to CH cavity</td>
<td>CF100</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Interfaces list for the CH dynamic tuner.

 

**Table:** Single-spoke cavity main parameters.

<table>
<caption>Table: Single-spoke cavity main parameters.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Beam mode</td>
<td>CW</td>
</tr>
<tr class="even">
<td>Frequency (MHz)</td>
<td>352.2</td>
</tr>
<tr class="odd">
<td>Optimal β</td>
<td>0.37</td>
</tr>
<tr class="even">
<td>Temperature (K)</td>
<td>2</td>
</tr>
<tr class="odd">
<td>Vo·T (MV m<sup>-1</sup>) @ 1J &amp; <em>β</em><sub>optimal</sub></td>
<td>0.693</td>
</tr>
<tr class="even">
<td><em>E</em><sub>pk</sub>/ <em>E</em><sub>acc</sub></td>
<td>4.29</td>
</tr>
<tr class="odd">
<td><em>B</em><sub>pk</sub>/ <em>E</em><sub>acc</sub> (mT / (MV m<sup>-1</sup>))</td>
<td>7.32</td>
</tr>
<tr class="even">
<td>G (Ω)</td>
<td>109</td>
</tr>
<tr class="odd">
<td>r/Q (Ω)</td>
<td>217</td>
</tr>
<tr class="even">
<td>Q<sub>0</sub> @ 2K for R<em><sub>res</sub></em> = 20nΩ</td>
<td>5.2e9</td>
</tr>
<tr class="odd">
<td>P<em><sub>cav</sub></em> for Q<sub>0</sub> = 2e9 &amp; 6.4MV m<sup>-1</sup> (W)</td>
<td>9.35</td>
</tr>
<tr class="even">
<td><em>E</em><sub>acc</sub> for nominal operation (MV m<sup>-1</sup>)</td>
<td>6.4</td>
</tr>
<tr class="odd">
<td><em>E</em><sub>acc</sub> for fault tolerant operation (MV m<sup>-1</sup>)</td>
<td>8.2</td>
</tr>
<tr class="even">
<td><em>L</em><sub>acc</sub> = <em>β</em><sub>optimal</sub> · <em>c</em> · <em>f</em> (m)</td>
<td>0.315</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Single-spoke cavity main parameters.

 

**Table:** Material mechanical properties.

<table>
<caption>Table: Material mechanical properties.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Mechanical properties</td>
<td>Niobium</td>
<td>Titanium grade 2</td>
</tr>
<tr class="even">
<td>Young modulus E (GPa) at 300K</td>
<td>107</td>
<td>105</td>
</tr>
<tr class="odd">
<td>Poisson ratio</td>
<td>0.38</td>
<td>0.3</td>
</tr>
<tr class="even">
<td>Density (kg m⁻³)</td>
<td>8570</td>
<td>4500</td>
</tr>
<tr class="odd">
<td>Yield strength <em>R</em><sub>p</sub> 0.2% (MPa) @ 300K</td>
<td>50</td>
<td>275 (min.)</td>
</tr>
<tr class="even">
<td>Ultimate tensile strength (MPa) @ 300K</td>
<td>125</td>
<td>345 (min.)</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Material mechanical properties.

 

**Table:** Load case results.

<table>
<caption>Table: Load case results.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Vacuum in the cavity</td>
<td>Vacuum in the helium tank</td>
</tr>
<tr class="even">
<td>Displacement</td>
<td>0.2mm</td>
<td>0.29mm</td>
</tr>
<tr class="odd">
<td>Max. stress on the cavity walls</td>
<td>33MPa</td>
<td>35MPa</td>
</tr>
<tr class="even">
<td>Max. stress</td>
<td>42MPa (disc/ext. ring)</td>
<td>41MPa (disc/ext. ring)</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Load case results.

 

**Table:** Mechanical modes.

<table>
<caption>Table: Mechanical modes.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Mode number</td>
<td>Frequency (Hz)</td>
<td>Type of the mode</td>
</tr>
<tr class="even">
<td>1 &amp; 2</td>
<td>232</td>
<td>Beam tube mode</td>
</tr>
<tr class="odd">
<td>3</td>
<td>320</td>
<td>Spoke bar mode</td>
</tr>
<tr class="even">
<td>4</td>
<td>430</td>
<td>Spoke bar mode (beam axis)</td>
</tr>
<tr class="odd">
<td>5</td>
<td>433</td>
<td>End cap mode</td>
</tr>
<tr class="even">
<td>6, 7, &amp; 8</td>
<td>466 / 468 / 497</td>
<td>Coupled mode</td>
</tr>
<tr class="odd">
<td>9</td>
<td>511</td>
<td>Spoke bar mode</td>
</tr>
<tr class="even">
<td>10, 11 &amp; 12</td>
<td>525 / 531 / 539</td>
<td>Coupled mode</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Mechanical modes.

 

**Table:** RF results, CST MicroWave Studio vs ANSYS comparison.

<table>
<caption>Table: RF results, CST MicroWave Studio vs ANSYS comparison.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>RF parameters</td>
<td>CST MicroWave Studio</td>
<td>ANSYS</td>
</tr>
<tr class="even">
<td>Frequency (MHz)</td>
<td>352.2</td>
<td>352.2</td>
</tr>
<tr class="odd">
<td>Optimal β</td>
<td>0.37</td>
<td>0.37</td>
</tr>
<tr class="even">
<td><em>L</em><sub>acc</sub> = Ngap · β · λ/2<em>R</em><sub>p</sub> (m)</td>
<td>0.315</td>
<td>0.315</td>
</tr>
<tr class="odd">
<td><em>E</em><sub>pk</sub>/ <em>E</em><sub>acc</sub></td>
<td>4.29</td>
<td>4.32</td>
</tr>
<tr class="even">
<td>B<sub>pk</sub> / E<sub>acc</sub> (mT/MV m<sup>-1</sup>)</td>
<td>7.32</td>
<td>6.36</td>
</tr>
<tr class="odd">
<td>G (Ω)</td>
<td>109</td>
<td>109</td>
</tr>
<tr class="even">
<td>r/Q (Ω)</td>
<td>217</td>
<td>214</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: RF results, CST MicroWave Studio vs ANSYS comparison.

 

**Table:** Cavity stiffness.

<table>
<caption>Table: Cavity stiffness.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Detuning sensitivity</td>
<td>Stiffness</td>
<td>Von Mises stress</td>
</tr>
<tr class="even">
<td> </td>
<td>(kHz mm⁻¹)</td>
<td>(kN mm⁻¹)</td>
<td>for 1mm (MPa)</td>
</tr>
<tr class="odd">
<td>Cryomodule configuration</td>
<td>180</td>
<td>14.5</td>
<td>367</td>
</tr>
<tr class="even">
<td>Tensile test bench configuration</td>
<td>204</td>
<td>8.5</td>
<td>219</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Cavity stiffness.

 

**Table:** RF sensitivity.

<table>
<caption>Table: RF sensitivity.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Configuration</td>
<td>Kp (Hz mbar⁻¹)</td>
</tr>
<tr class="even">
<td>Bare cavity without rings</td>
<td>600</td>
</tr>
<tr class="odd">
<td>Bare cavity with rings</td>
<td>221</td>
</tr>
<tr class="even">
<td>Equipped cavity:</td>
<td> </td>
</tr>
<tr class="odd">
<td>‣ Without pressure on the vessel wall</td>
<td>–125</td>
</tr>
<tr class="even">
<td>‣ With pressure on the vessel wall</td>
<td>+25</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: RF sensitivity.

 

**Table:** RF sensitivity due to pressure.

<table>
<caption>Table: RF sensitivity due to pressure.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>K<em><sub>p</sub></em> (Hz mbar⁻¹)</td>
</tr>
<tr class="even">
<td>With 2 fixed ends</td>
<td>+23</td>
</tr>
<tr class="odd">
<td>With 1 fixed ends</td>
<td>+31</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: RF sensitivity due to pressure.

 

**Table:** Lorentz factor.

<table>
<caption>Table: Lorentz factor.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td colspan="2">Lorentz factor: K<em><sub>L</sub></em> = Δ<em>f</em> / E²<sub>acc</sub></td>
<td>For 8MV m<sup>-1</sup></td>
</tr>
<tr class="even">
<td>K<em><sub>L</sub></em> (Hz / (MV m<sup>-1</sup>)²), one free end</td>
<td>K<em><sub>L</sub></em> = -7.7</td>
<td>Δ<em>f</em> = 493Hz</td>
</tr>
<tr class="odd">
<td>K<em><sub>L</sub></em> (Hz / (MV m<sup>-1</sup>)²), fixed ends</td>
<td>K<em><sub>L</sub></em> = -4.7</td>
<td>Δ<em>f</em> = 300Hz</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Lorentz factor.

 

**Table:** Single-spoke cavity coupler main parameters.

<table>
<caption>Table: Single-spoke cavity coupler main parameters.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Beam mode</td>
<td>CW</td>
</tr>
<tr class="even">
<td>Frequency (MHz)</td>
<td>352.2</td>
</tr>
<tr class="odd">
<td>Temperature (K)</td>
<td>2</td>
</tr>
<tr class="even">
<td>P nominal (CW) (kW)</td>
<td>8</td>
</tr>
<tr class="odd">
<td>P Fault Tol. (CW) (kW)</td>
<td>14</td>
</tr>
<tr class="even">
<td>P design (CW) (kW)</td>
<td>20</td>
</tr>
<tr class="odd">
<td>Coupling mode</td>
<td>Capacitive</td>
</tr>
<tr class="even">
<td>Qext</td>
<td><strong>TBD: inline6838, img1</strong></td>
</tr>
<tr class="odd">
<td># of vacuum windows</td>
<td>1</td>
</tr>
<tr class="even">
<td>Mat. of vacuum window</td>
<td>Al₂O₃</td>
</tr>
<tr class="odd">
<td>thickness of vac. window (mm)</td>
<td>7</td>
</tr>
<tr class="even">
<td>ε<em><sub>r</sub></em> of vac. window</td>
<td>9.8</td>
</tr>
<tr class="odd">
<td>tan δ of vac. window</td>
<td>10e-4</td>
</tr>
<tr class="even">
<td><em>E</em><sub>max</sub> on window (MV m<sup>-1</sup>)</td>
<td>&lt; 1</td>
</tr>
<tr class="odd">
<td><em>E</em><sub>max</sub> triple junction (MV m<sup>-1</sup>)</td>
<td>&lt; 0.3</td>
</tr>
<tr class="even">
<td>Adjustability of antenna</td>
<td>fixed size</td>
</tr>
<tr class="odd">
<td>Max deviation of antenna</td>
<td>1.3°</td>
</tr>
<tr class="even">
<td>|S₁₁|</td>
<td>≤-30dB</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Single-spoke cavity coupler main parameters.

 

**Table:** Single-spoke cavity coupler results of electrical studies.

<table>
<caption>Table: Single-spoke cavity coupler results of electrical studies.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Without chokes</td>
<td>Mixed</td>
<td>With chokes</td>
<td>Spec</td>
</tr>
<tr class="even">
<td>|S₁₁|</td>
<td>≤ -50dB</td>
<td>≤ -52dB</td>
<td>≤ -77dB</td>
<td>≤ -30dB</td>
</tr>
<tr class="odd">
<td><em>E</em><sub>max</sub> on window (MV m<sup>-1</sup>)</td>
<td>&lt;0.50</td>
<td>&lt;0.98</td>
<td>&lt;0.65</td>
<td>&lt;1</td>
</tr>
<tr class="even">
<td><em>E</em><sub>max</sub> triple junction (MV m<sup>-1</sup>)</td>
<td>&lt;0.27</td>
<td>&lt;0.28</td>
<td>&lt;0.20</td>
<td>&lt;0.3</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Single-spoke cavity coupler results of electrical studies.

 

**Table:** Max E-field for a stored energy of 1J.

<table>
<caption>Table: Max E-field for a stored energy of 1J.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Flat-top</td>
<td>Spherical</td>
<td> </td>
</tr>
<tr class="even">
<td><em>E</em><sub>max</sub> on window (MV m<sup>-1</sup>)</td>
<td>&lt;0.50</td>
<td>&lt;0.66</td>
<td> </td>
</tr>
<tr class="odd">
<td>Penetration (mm)</td>
<td>99</td>
<td>104</td>
<td> </td>
</tr>
<tr class="even">
<td>Q<em><sub>ext</sub></em></td>
<td>2.34 × 10<sup>6</sup></td>
<td>2.24 × 10<sup>6</sup></td>
<td> </td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Max E-field for a stored energy of 1J.

 

**Table:** Multipactor thresholds.

<table>
<caption>Table: Multipactor thresholds.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Without chokes</td>
<td>Mixed</td>
<td>With chokes</td>
</tr>
<tr class="even">
<td>P (kW)</td>
<td>13</td>
<td>3</td>
<td>7.5</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Multipactor thresholds.

 

**Table:** K*~p~* (Hz mbar⁻¹).

<table>
<caption>Table: Kp (Hz mbar⁻¹).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Case</td>
<td>model 0</td>
<td>Model 1</td>
<td>Model 2</td>
<td>Model 3</td>
<td>Test results</td>
</tr>
<tr class="even">
<td>110</td>
<td>-123</td>
<td>-174</td>
<td>-374</td>
<td>-250</td>
<td>-170 (-245 max)</td>
</tr>
<tr class="odd">
<td>101</td>
<td>-32</td>
<td>+74</td>
<td>+280</td>
<td>+176</td>
<td>+188</td>
</tr>
<tr class="even">
<td>100</td>
<td>-154</td>
<td>-98</td>
<td>-94</td>
<td>-74</td>
<td>+14</td>
</tr>
<tr class="odd">
<td>121</td>
<td>+32</td>
<td>-76</td>
<td>-280</td>
<td>-176</td>
<td>-198</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Kp (Hz mbar⁻¹).

 

**Table:** Displacement at SAF side (mm).

<table>
<caption>Table: Displacement at SAF side (mm).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Case</td>
<td>Model 0</td>
<td>Model 1</td>
<td>Model 2</td>
<td>Model 3</td>
<td>Test results</td>
</tr>
<tr class="even">
<td>110</td>
<td>–0.19</td>
<td>–0.36</td>
<td>–0.65</td>
<td>–0.33</td>
<td>–0.64</td>
</tr>
<tr class="odd">
<td>101</td>
<td>≈0</td>
<td>+0.29</td>
<td>+0.5</td>
<td>+0.25</td>
<td>+0.38&lt;&gt;+0.6</td>
</tr>
<tr class="even">
<td>100</td>
<td>–0.19</td>
<td>–0.07</td>
<td>–0.12</td>
<td>–0.08</td>
<td>–0.2</td>
</tr>
<tr class="odd">
<td>121</td>
<td>≈0</td>
<td>–0.29</td>
<td>–0.5</td>
<td>–0.25</td>
<td>–0.47</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Displacement at SAF side (mm).

 

**Table:** Lorentz force factor (Hz / (MV m^-1^)²).

<table>
<caption>Table: Lorentz force factor (Hz / (MV m-1)²).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Case</td>
<td>Model 0</td>
<td>Model 1</td>
<td>Model 2</td>
<td>Model 3</td>
<td>Test results</td>
</tr>
<tr class="even">
<td>4K</td>
<td>Not evaluated</td>
<td>–8.8</td>
<td>Not evaluated</td>
<td>–8.15</td>
<td>–10.35</td>
</tr>
<tr class="odd">
<td>2K</td>
<td>–7.7</td>
<td>–8.7</td>
<td>–10.6</td>
<td>–8.1</td>
<td>–8.2</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Lorentz force factor (Hz / (MV m-1)²).

 

**Table:** Main components and conceptual design choices.

<table>
<caption>Table: Main components and conceptual design choices.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>1</td>
<td>From XFEL or Project X: the cavity axis moves from 300K to 2K. The design goal is to reduce the displacement and have a position adjustment at 2K;</td>
</tr>
<tr class="even">
<td>2</td>
<td>From XFEL, Project X, LEP2, SNS: cylindrical vacuum vessel and cryostating from the vacuum vessel axis;</td>
</tr>
<tr class="odd">
<td>3</td>
<td>Small 2 cavities train, required by beam dynamics and fault tolerance capabilities (reliability);</td>
</tr>
<tr class="even">
<td>4</td>
<td>From XFEL, Project X, LEP2, SNS: separated vacuum, compulsory due to the high accelerating gradient required;</td>
</tr>
<tr class="odd">
<td>5</td>
<td>From SPIRAL-2: only warm valves, so part of the vacuum vessel (as small as possible) has to be introduced inside clean room;</td>
</tr>
<tr class="even">
<td>6</td>
<td>From SPIRAL-2, SNS, RIA: RF power coupler with only one warm window already designed in the framework of EUROTRANS/EURISOL projects;</td>
</tr>
<tr class="odd">
<td>7</td>
<td>From XFEL, SNS, SPIRAL-2, LEP2: no cold focusing components inside the cryomodule;</td>
</tr>
<tr class="even">
<td>8</td>
<td>From SPIRAL-2: cold magnetic shield (2K) actively cooled;</td>
</tr>
<tr class="odd">
<td>9</td>
<td>From Project X, SNS, SPIRAL-2: one thermal shield cooled to around 60K;</td>
</tr>
<tr class="even">
<td>10</td>
<td>From XFEL: CTS at 2K/4K with stepping motor and piezoelectric actuators.</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main components and conceptual design choices.

 

**Table:** Thermo-mechanical evaluated values.

<table>
<caption>Table: Thermo-mechanical evaluated values.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>STATIC</td>
<td>Q (300K ⇨ 60K) &lt; 92W</td>
</tr>
<tr class="even">
<td>HEAT</td>
<td>Q (60K ⇨ 10K) &lt; 8W</td>
</tr>
<tr class="odd">
<td>LOADS</td>
<td>Q (60K ⇨ 2K) &lt; 1W</td>
</tr>
<tr class="even">
<td> </td>
<td>Q (10K ⇨ 2K) &lt; 2.2W</td>
</tr>
<tr class="odd">
<td>CAVITY DISPLACE-</td>
<td>(Vertical) ΔY<sub>max</sub> = -1mm</td>
</tr>
<tr class="even">
<td>MENT FROM 300K</td>
<td>(Longitudinal) ΔZ<sub>max</sub>= -0.4mm</td>
</tr>
<tr class="odd">
<td>TO 2K</td>
<td>(Lateral) ΔX<sub>max</sub> ≈ 0mm</td>
</tr>
<tr class="even">
<td>MECHANICAL EIGEN</td>
<td>Sliding blocks: f ≈90Hz</td>
</tr>
<tr class="odd">
<td>FREQUENCIES</td>
<td>Support table: f ≈20Hz</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Thermo-mechanical evaluated values.

 

**Table:** CTS design EM/mechanical parameters.

<table>
<caption>Table: CTS design EM/mechanical parameters.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Cavity tuning stiffness</td>
<td>14.5</td>
<td>kN mm⁻¹</td>
</tr>
<tr class="even">
<td>Elongation sensitivity</td>
<td>180</td>
<td>kHz mm⁻¹</td>
</tr>
<tr class="odd">
<td>Sensitivity to bath pressure (1 free end)</td>
<td>+31</td>
<td>Hz mbar⁻¹</td>
</tr>
<tr class="even">
<td>Sensitivity to bath pressure (2 fixed ends)</td>
<td>+23</td>
<td>Hz mbar⁻¹</td>
</tr>
<tr class="odd">
<td>Lorentz Force Factor (1 free end)</td>
<td>–7.7</td>
<td>Hz / (MV m<sup>-1</sup>)²</td>
</tr>
<tr class="even">
<td>Lorentz Force Factor (2 fixed ends)</td>
<td>–4.7</td>
<td>Hz / (MV m<sup>-1</sup>)²</td>
</tr>
<tr class="odd">
<td>Bandwidth</td>
<td>160</td>
<td>Hz</td>
</tr>
<tr class="even">
<td>CTS mechanical resolution (required: less than 1/20 of bandwidth)</td>
<td>8</td>
<td>Hz</td>
</tr>
<tr class="odd">
<td>CTS stiffness</td>
<td>110</td>
<td>kN mm⁻¹</td>
</tr>
<tr class="even">
<td>Ratio of deformation</td>
<td>88</td>
<td>%</td>
</tr>
<tr class="odd">
<td>Effective tuning sensitivity</td>
<td>158</td>
<td>kHz mm⁻¹</td>
</tr>
<tr class="even">
<td>Effective slow tuning resolution</td>
<td>0.2</td>
<td>kHz / step</td>
</tr>
<tr class="odd">
<td>Fast tuning range</td>
<td>500</td>
<td>Hz</td>
</tr>
<tr class="even">
<td>Maximum cavity deformation</td>
<td>1.1</td>
<td>mm</td>
</tr>
<tr class="odd">
<td>Maximum operating tuning range</td>
<td>174</td>
<td>kHz</td>
</tr>
<tr class="even">
<td>Maximum strength</td>
<td>16</td>
<td>kN</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: CTS design EM/mechanical parameters.

 

**Table:** Single-spoke cavity main parameters.

<table>
<caption>Table: Single-spoke cavity main parameters.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Beam mode</td>
<td>CW</td>
</tr>
<tr class="even">
<td>Frequency (MHz)</td>
<td>352.2</td>
</tr>
<tr class="odd">
<td>Optimal β</td>
<td>0.37</td>
</tr>
<tr class="even">
<td>Temperature (K)</td>
<td>2</td>
</tr>
<tr class="odd">
<td>Vo·T (MV m<sup>-1</sup>) @ 1J &amp; <em>β</em><sub>optimal</sub></td>
<td>0.693</td>
</tr>
<tr class="even">
<td><em>E</em><sub>pk</sub>/ <em>E</em><sub>acc</sub></td>
<td>4.29</td>
</tr>
<tr class="odd">
<td><em>B</em><sub>pk</sub>/ <em>E</em><sub>acc</sub> (mT / (MV m<sup>-1</sup>))</td>
<td>7.32</td>
</tr>
<tr class="even">
<td>G (Ω)</td>
<td>109</td>
</tr>
<tr class="odd">
<td>r/Q (Ω)</td>
<td>217</td>
</tr>
<tr class="even">
<td>Q<sub>0</sub> @ 2K for R<em><sub>res</sub></em> = 20nΩ</td>
<td>5.2e9</td>
</tr>
<tr class="odd">
<td>P<em><sub>cav</sub></em> for Q<sub>0</sub> = 2e9 &amp; 6.4MV m<sup>-1</sup> (W)</td>
<td>9.35</td>
</tr>
<tr class="even">
<td><em>E</em><sub>acc</sub> for nominal operation (MV m<sup>-1</sup>)</td>
<td>6.4</td>
</tr>
<tr class="odd">
<td><em>E</em><sub>acc</sub> for fault tolerant operation (MV m<sup>-1</sup>)</td>
<td>8.2</td>
</tr>
<tr class="even">
<td><em>L</em><sub>acc</sub> = <em>β</em><sub>optimal</sub> · <em>c</em> · <em>f</em> (m)</td>
<td>0.315</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Single-spoke cavity main parameters.

 

**Table:** Material mechanical properties.

<table>
<caption>Table: Material mechanical properties.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Mechanical properties</td>
<td>Niobium</td>
<td>Titanium grade 2</td>
</tr>
<tr class="even">
<td>Young modulus E (GPa) at 300K</td>
<td>107</td>
<td>105</td>
</tr>
<tr class="odd">
<td>Poisson ratio</td>
<td>0.38</td>
<td>0.3</td>
</tr>
<tr class="even">
<td>Density (kg m⁻³)</td>
<td>8570</td>
<td>4500</td>
</tr>
<tr class="odd">
<td>Yield strength <em>R</em><sub>p</sub> 0.2% (MPa) @ 300K</td>
<td>50</td>
<td>275 (min.)</td>
</tr>
<tr class="even">
<td>Ultimate tensile strength (MPa) @ 300K</td>
<td>125</td>
<td>345 (min.)</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Material mechanical properties.

 

**Table:** Load case results.

<table>
<caption>Table: Load case results.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Vacuum in the cavity</td>
<td>Vacuum in the helium tank</td>
</tr>
<tr class="even">
<td>Displacement</td>
<td>0.2mm</td>
<td>0.29mm</td>
</tr>
<tr class="odd">
<td>Max. stress on the cavity walls</td>
<td>33MPa</td>
<td>35MPa</td>
</tr>
<tr class="even">
<td>Max. stress</td>
<td>42MPa (disc/ext. ring)</td>
<td>41MPa (disc/ext. ring)</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Load case results.

 

**Table:** Mechanical modes.

<table>
<caption>Table: Mechanical modes.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Mode number</td>
<td>Frequency (Hz)</td>
<td>Type of the mode</td>
</tr>
<tr class="even">
<td>1 &amp; 2</td>
<td>232</td>
<td>Beam tube mode</td>
</tr>
<tr class="odd">
<td>3</td>
<td>320</td>
<td>Spoke bar mode</td>
</tr>
<tr class="even">
<td>4</td>
<td>430</td>
<td>Spoke bar mode (beam axis)</td>
</tr>
<tr class="odd">
<td>5</td>
<td>433</td>
<td>End cap mode</td>
</tr>
<tr class="even">
<td>6, 7, &amp; 8</td>
<td>466 / 468 / 497</td>
<td>Coupled mode</td>
</tr>
<tr class="odd">
<td>9</td>
<td>511</td>
<td>Spoke bar mode</td>
</tr>
<tr class="even">
<td>10, 11 &amp; 12</td>
<td>525 / 531 / 539</td>
<td>Coupled mode</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Mechanical modes.

 

**Table:** RF results, CST MicroWave Studio vs ANSYS comparison.

<table>
<caption>Table: RF results, CST MicroWave Studio vs ANSYS comparison.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>RF parameters</td>
<td>CST MicroWave Studio</td>
<td>ANSYS</td>
</tr>
<tr class="even">
<td>Frequency (MHz)</td>
<td>352.2</td>
<td>352.2</td>
</tr>
<tr class="odd">
<td>Optimal β</td>
<td>0.37</td>
<td>0.37</td>
</tr>
<tr class="even">
<td><em>L</em><sub>acc</sub> = Ngap · β · λ/2<em>R</em><sub>p</sub> (m)</td>
<td>0.315</td>
<td>0.315</td>
</tr>
<tr class="odd">
<td><em>E</em><sub>pk</sub>/ <em>E</em><sub>acc</sub></td>
<td>4.29</td>
<td>4.32</td>
</tr>
<tr class="even">
<td>B<sub>pk</sub> / E<sub>acc</sub> (mT/MV m<sup>-1</sup>)</td>
<td>7.32</td>
<td>6.36</td>
</tr>
<tr class="odd">
<td>G (Ω)</td>
<td>109</td>
<td>109</td>
</tr>
<tr class="even">
<td>r/Q (Ω)</td>
<td>217</td>
<td>214</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: RF results, CST MicroWave Studio vs ANSYS comparison.

 

**Table:** Cavity stiffness.

<table>
<caption>Table: Cavity stiffness.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Detuning sensitivity</td>
<td>Stiffness</td>
<td>Von Mises stress</td>
</tr>
<tr class="even">
<td> </td>
<td>(kHz mm⁻¹)</td>
<td>(kN mm⁻¹)</td>
<td>for 1mm (MPa)</td>
</tr>
<tr class="odd">
<td>Cryomodule configuration</td>
<td>180</td>
<td>14.5</td>
<td>367</td>
</tr>
<tr class="even">
<td>Tensile test bench configuration</td>
<td>204</td>
<td>8.5</td>
<td>219</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Cavity stiffness.

 

**Table:** RF sensitivity.

<table>
<caption>Table: RF sensitivity.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Configuration</td>
<td>Kp (Hz mbar⁻¹)</td>
</tr>
<tr class="even">
<td>Bare cavity without rings</td>
<td>600</td>
</tr>
<tr class="odd">
<td>Bare cavity with rings</td>
<td>221</td>
</tr>
<tr class="even">
<td>Equipped cavity:</td>
<td> </td>
</tr>
<tr class="odd">
<td>‣ Without pressure on the vessel wall</td>
<td>–125</td>
</tr>
<tr class="even">
<td>‣ With pressure on the vessel wall</td>
<td>+25</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: RF sensitivity.

 

**Table:** RF sensitivity due to pressure.

<table>
<caption>Table: RF sensitivity due to pressure.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>K<em><sub>p</sub></em> (Hz mbar⁻¹)</td>
</tr>
<tr class="even">
<td>With 2 fixed ends</td>
<td>+23</td>
</tr>
<tr class="odd">
<td>With 1 fixed ends</td>
<td>+31</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: RF sensitivity due to pressure.

 

**Table:** Lorentz factor.

<table>
<caption>Table: Lorentz factor.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td colspan="2">Lorentz factor: K<em><sub>L</sub></em> = Δ<em>f</em> / E²<sub>acc</sub></td>
<td>For 8MV m<sup>-1</sup></td>
</tr>
<tr class="even">
<td>K<em><sub>L</sub></em> (Hz / (MV m<sup>-1</sup>)²), one free end</td>
<td>K<em><sub>L</sub></em> = -7.7</td>
<td>Δ<em>f</em> = 493Hz</td>
</tr>
<tr class="odd">
<td>K<em><sub>L</sub></em> (Hz / (MV m<sup>-1</sup>)²), fixed ends</td>
<td>K<em><sub>L</sub></em> = -4.7</td>
<td>Δ<em>f</em> = 300Hz</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Lorentz factor.

 

**Table:** Single-spoke cavity coupler main parameters.

<table>
<caption>Table: Single-spoke cavity coupler main parameters.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Beam mode</td>
<td>CW</td>
</tr>
<tr class="even">
<td>Frequency (MHz)</td>
<td>352.2</td>
</tr>
<tr class="odd">
<td>Temperature (K)</td>
<td>2</td>
</tr>
<tr class="even">
<td>P nominal (CW) (kW)</td>
<td>8</td>
</tr>
<tr class="odd">
<td>P Fault Tol. (CW) (kW)</td>
<td>14</td>
</tr>
<tr class="even">
<td>P design (CW) (kW)</td>
<td>20</td>
</tr>
<tr class="odd">
<td>Coupling mode</td>
<td>Capacitive</td>
</tr>
<tr class="even">
<td>Qext</td>
<td><strong>TBD: inline6838, img1</strong></td>
</tr>
<tr class="odd">
<td># of vacuum windows</td>
<td>1</td>
</tr>
<tr class="even">
<td>Mat. of vacuum window</td>
<td>Al₂O₃</td>
</tr>
<tr class="odd">
<td>thickness of vac. window (mm)</td>
<td>7</td>
</tr>
<tr class="even">
<td>ε<em><sub>r</sub></em> of vac. window</td>
<td>9.8</td>
</tr>
<tr class="odd">
<td>tan δ of vac. window</td>
<td>10e-4</td>
</tr>
<tr class="even">
<td><em>E</em><sub>max</sub> on window (MV m<sup>-1</sup>)</td>
<td>&lt; 1</td>
</tr>
<tr class="odd">
<td><em>E</em><sub>max</sub> triple junction (MV m<sup>-1</sup>)</td>
<td>&lt; 0.3</td>
</tr>
<tr class="even">
<td>Adjustability of antenna</td>
<td>fixed size</td>
</tr>
<tr class="odd">
<td>Max deviation of antenna</td>
<td>1.3°</td>
</tr>
<tr class="even">
<td>|S₁₁|</td>
<td>≤-30dB</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Single-spoke cavity coupler main parameters.

 

**Table:** Single-spoke cavity coupler results of electrical studies.

<table>
<caption>Table: Single-spoke cavity coupler results of electrical studies.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Without chokes</td>
<td>Mixed</td>
<td>With chokes</td>
<td>Spec</td>
</tr>
<tr class="even">
<td>|S₁₁|</td>
<td>≤ -50dB</td>
<td>≤ -52dB</td>
<td>≤ -77dB</td>
<td>≤ -30dB</td>
</tr>
<tr class="odd">
<td><em>E</em><sub>max</sub> on window (MV m<sup>-1</sup>)</td>
<td>&lt;0.50</td>
<td>&lt;0.98</td>
<td>&lt;0.65</td>
<td>&lt;1</td>
</tr>
<tr class="even">
<td><em>E</em><sub>max</sub> triple junction (MV m<sup>-1</sup>)</td>
<td>&lt;0.27</td>
<td>&lt;0.28</td>
<td>&lt;0.20</td>
<td>&lt;0.3</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Single-spoke cavity coupler results of electrical studies.

 

**Table:** Max E-field for a stored energy of 1J.

<table>
<caption>Table: Max E-field for a stored energy of 1J.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Flat-top</td>
<td>Spherical</td>
<td> </td>
</tr>
<tr class="even">
<td><em>E</em><sub>max</sub> on window (MV m<sup>-1</sup>)</td>
<td>&lt;0.50</td>
<td>&lt;0.66</td>
<td> </td>
</tr>
<tr class="odd">
<td>Penetration (mm)</td>
<td>99</td>
<td>104</td>
<td> </td>
</tr>
<tr class="even">
<td>Q<em><sub>ext</sub></em></td>
<td>2.34 × 10<sup>6</sup></td>
<td>2.24 × 10<sup>6</sup></td>
<td> </td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Max E-field for a stored energy of 1J.

 

**Table:** Multipactor thresholds.

<table>
<caption>Table: Multipactor thresholds.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Without chokes</td>
<td>Mixed</td>
<td>With chokes</td>
</tr>
<tr class="even">
<td>P (kW)</td>
<td>13</td>
<td>3</td>
<td>7.5</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Multipactor thresholds.

 

**Table:** K*~p~* (Hz mbar⁻¹).

<table>
<caption>Table: Kp (Hz mbar⁻¹).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Case</td>
<td>model 0</td>
<td>Model 1</td>
<td>Model 2</td>
<td>Model 3</td>
<td>Test results</td>
</tr>
<tr class="even">
<td>110</td>
<td>-123</td>
<td>-174</td>
<td>-374</td>
<td>-250</td>
<td>-170 (-245 max)</td>
</tr>
<tr class="odd">
<td>101</td>
<td>-32</td>
<td>+74</td>
<td>+280</td>
<td>+176</td>
<td>+188</td>
</tr>
<tr class="even">
<td>100</td>
<td>-154</td>
<td>-98</td>
<td>-94</td>
<td>-74</td>
<td>+14</td>
</tr>
<tr class="odd">
<td>121</td>
<td>+32</td>
<td>-76</td>
<td>-280</td>
<td>-176</td>
<td>-198</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Kp (Hz mbar⁻¹).

 

**Table:** Displacement at SAF side (mm).

<table>
<caption>Table: Displacement at SAF side (mm).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Case</td>
<td>Model 0</td>
<td>Model 1</td>
<td>Model 2</td>
<td>Model 3</td>
<td>Test results</td>
</tr>
<tr class="even">
<td>110</td>
<td>–0.19</td>
<td>–0.36</td>
<td>–0.65</td>
<td>–0.33</td>
<td>–0.64</td>
</tr>
<tr class="odd">
<td>101</td>
<td>≈0</td>
<td>+0.29</td>
<td>+0.5</td>
<td>+0.25</td>
<td>+0.38&lt;&gt;+0.6</td>
</tr>
<tr class="even">
<td>100</td>
<td>–0.19</td>
<td>–0.07</td>
<td>–0.12</td>
<td>–0.08</td>
<td>–0.2</td>
</tr>
<tr class="odd">
<td>121</td>
<td>≈0</td>
<td>–0.29</td>
<td>–0.5</td>
<td>–0.25</td>
<td>–0.47</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Displacement at SAF side (mm).

 

**Table:** Lorentz force factor (Hz / (MV m^-1^)²).

<table>
<caption>Table: Lorentz force factor (Hz / (MV m-1)²).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Case</td>
<td>Model 0</td>
<td>Model 1</td>
<td>Model 2</td>
<td>Model 3</td>
<td>Test results</td>
</tr>
<tr class="even">
<td>4K</td>
<td>Not evaluated</td>
<td>–8.8</td>
<td>Not evaluated</td>
<td>–8.15</td>
<td>–10.35</td>
</tr>
<tr class="odd">
<td>2K</td>
<td>–7.7</td>
<td>–8.7</td>
<td>–10.6</td>
<td>–8.1</td>
<td>–8.2</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Lorentz force factor (Hz / (MV m-1)²).

 

**Table:** Main components and conceptual design choices.

<table>
<caption>Table: Main components and conceptual design choices.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>1</td>
<td>From XFEL or Project X: the cavity axis moves from 300K to 2K. The design goal is to reduce the displacement and have a position adjustment at 2K;</td>
</tr>
<tr class="even">
<td>2</td>
<td>From XFEL, Project X, LEP2, SNS: cylindrical vacuum vessel and cryostating from the vacuum vessel axis;</td>
</tr>
<tr class="odd">
<td>3</td>
<td>Small 2 cavities train, required by beam dynamics and fault tolerance capabilities (reliability);</td>
</tr>
<tr class="even">
<td>4</td>
<td>From XFEL, Project X, LEP2, SNS: separated vacuum, compulsory due to the high accelerating gradient required;</td>
</tr>
<tr class="odd">
<td>5</td>
<td>From SPIRAL-2: only warm valves, so part of the vacuum vessel (as small as possible) has to be introduced inside clean room;</td>
</tr>
<tr class="even">
<td>6</td>
<td>From SPIRAL-2, SNS, RIA: RF power coupler with only one warm window already designed in the framework of EUROTRANS/EURISOL projects;</td>
</tr>
<tr class="odd">
<td>7</td>
<td>From XFEL, SNS, SPIRAL-2, LEP2: no cold focusing components inside the cryomodule;</td>
</tr>
<tr class="even">
<td>8</td>
<td>From SPIRAL-2: cold magnetic shield (2K) actively cooled;</td>
</tr>
<tr class="odd">
<td>9</td>
<td>From Project X, SNS, SPIRAL-2: one thermal shield cooled to around 60K;</td>
</tr>
<tr class="even">
<td>10</td>
<td>From XFEL: CTS at 2K/4K with stepping motor and piezoelectric actuators.</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main components and conceptual design choices.

 

**Table:** Thermo-mechanical evaluated values.

<table>
<caption>Table: Thermo-mechanical evaluated values.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>STATIC</td>
<td>Q (300K ⇨ 60K) &lt; 92W</td>
</tr>
<tr class="even">
<td>HEAT</td>
<td>Q (60K ⇨ 10K) &lt; 8W</td>
</tr>
<tr class="odd">
<td>LOADS</td>
<td>Q (60K ⇨ 2K) &lt; 1W</td>
</tr>
<tr class="even">
<td> </td>
<td>Q (10K ⇨ 2K) &lt; 2.2W</td>
</tr>
<tr class="odd">
<td>CAVITY DISPLACE-</td>
<td>(Vertical) ΔY<sub>max</sub> = -1mm</td>
</tr>
<tr class="even">
<td>MENT FROM 300K</td>
<td>(Longitudinal) ΔZ<sub>max</sub>= -0.4mm</td>
</tr>
<tr class="odd">
<td>TO 2K</td>
<td>(Lateral) ΔX<sub>max</sub> ≈ 0mm</td>
</tr>
<tr class="even">
<td>MECHANICAL EIGEN</td>
<td>Sliding blocks: f ≈90Hz</td>
</tr>
<tr class="odd">
<td>FREQUENCIES</td>
<td>Support table: f ≈20Hz</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Thermo-mechanical evaluated values.

 

**Table:** CTS design EM/mechanical parameters.

<table>
<caption>Table: CTS design EM/mechanical parameters.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Cavity tuning stiffness</td>
<td>14.5</td>
<td>kN mm⁻¹</td>
</tr>
<tr class="even">
<td>Elongation sensitivity</td>
<td>180</td>
<td>kHz mm⁻¹</td>
</tr>
<tr class="odd">
<td>Sensitivity to bath pressure (1 free end)</td>
<td>+31</td>
<td>Hz mbar⁻¹</td>
</tr>
<tr class="even">
<td>Sensitivity to bath pressure (2 fixed ends)</td>
<td>+23</td>
<td>Hz mbar⁻¹</td>
</tr>
<tr class="odd">
<td>Lorentz Force Factor (1 free end)</td>
<td>–7.7</td>
<td>Hz / (MV m<sup>-1</sup>)²</td>
</tr>
<tr class="even">
<td>Lorentz Force Factor (2 fixed ends)</td>
<td>–4.7</td>
<td>Hz / (MV m<sup>-1</sup>)²</td>
</tr>
<tr class="odd">
<td>Bandwidth</td>
<td>160</td>
<td>Hz</td>
</tr>
<tr class="even">
<td>CTS mechanical resolution (required: less than 1/20 of bandwidth)</td>
<td>8</td>
<td>Hz</td>
</tr>
<tr class="odd">
<td>CTS stiffness</td>
<td>110</td>
<td>kN mm⁻¹</td>
</tr>
<tr class="even">
<td>Ratio of deformation</td>
<td>88</td>
<td>%</td>
</tr>
<tr class="odd">
<td>Effective tuning sensitivity</td>
<td>158</td>
<td>kHz mm⁻¹</td>
</tr>
<tr class="even">
<td>Effective slow tuning resolution</td>
<td>0.2</td>
<td>kHz / step</td>
</tr>
<tr class="odd">
<td>Fast tuning range</td>
<td>500</td>
<td>Hz</td>
</tr>
<tr class="even">
<td>Maximum cavity deformation</td>
<td>1.1</td>
<td>mm</td>
</tr>
<tr class="odd">
<td>Maximum operating tuning range</td>
<td>174</td>
<td>kHz</td>
</tr>
<tr class="even">
<td>Maximum strength</td>
<td>16</td>
<td>kN</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: CTS design EM/mechanical parameters.

 

**Table:** Single-spoke cavity main parameters.

<table>
<caption>Table: Single-spoke cavity main parameters.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Beam mode</td>
<td>CW</td>
</tr>
<tr class="even">
<td>Frequency (MHz)</td>
<td>352.2</td>
</tr>
<tr class="odd">
<td>Optimal β</td>
<td>0.37</td>
</tr>
<tr class="even">
<td>Temperature (K)</td>
<td>2</td>
</tr>
<tr class="odd">
<td>Vo·T (MV m<sup>-1</sup>) @ 1J &amp; <em>β</em><sub>optimal</sub></td>
<td>0.693</td>
</tr>
<tr class="even">
<td><em>E</em><sub>pk</sub>/ <em>E</em><sub>acc</sub></td>
<td>4.29</td>
</tr>
<tr class="odd">
<td><em>B</em><sub>pk</sub>/ <em>E</em><sub>acc</sub> (mT / (MV m<sup>-1</sup>))</td>
<td>7.32</td>
</tr>
<tr class="even">
<td>G (Ω)</td>
<td>109</td>
</tr>
<tr class="odd">
<td>r/Q (Ω)</td>
<td>217</td>
</tr>
<tr class="even">
<td>Q<sub>0</sub> @ 2K for R<em><sub>res</sub></em> = 20nΩ</td>
<td>5.2e9</td>
</tr>
<tr class="odd">
<td>P<em><sub>cav</sub></em> for Q<sub>0</sub> = 2e9 &amp; 6.4MV m<sup>-1</sup> (W)</td>
<td>9.35</td>
</tr>
<tr class="even">
<td><em>E</em><sub>acc</sub> for nominal operation (MV m<sup>-1</sup>)</td>
<td>6.4</td>
</tr>
<tr class="odd">
<td><em>E</em><sub>acc</sub> for fault tolerant operation (MV m<sup>-1</sup>)</td>
<td>8.2</td>
</tr>
<tr class="even">
<td><em>L</em><sub>acc</sub> = <em>β</em><sub>optimal</sub> · <em>c</em> · <em>f</em> (m)</td>
<td>0.315</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Single-spoke cavity main parameters.

 

**Table:** Material mechanical properties.

<table>
<caption>Table: Material mechanical properties.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Mechanical properties</td>
<td>Niobium</td>
<td>Titanium grade 2</td>
</tr>
<tr class="even">
<td>Young modulus E (GPa) at 300K</td>
<td>107</td>
<td>105</td>
</tr>
<tr class="odd">
<td>Poisson ratio</td>
<td>0.38</td>
<td>0.3</td>
</tr>
<tr class="even">
<td>Density (kg m⁻³)</td>
<td>8570</td>
<td>4500</td>
</tr>
<tr class="odd">
<td>Yield strength <em>R</em><sub>p</sub> 0.2% (MPa) @ 300K</td>
<td>50</td>
<td>275 (min.)</td>
</tr>
<tr class="even">
<td>Ultimate tensile strength (MPa) @ 300K</td>
<td>125</td>
<td>345 (min.)</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Material mechanical properties.

 

**Table:** Load case results.

<table>
<caption>Table: Load case results.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Vacuum in the cavity</td>
<td>Vacuum in the helium tank</td>
</tr>
<tr class="even">
<td>Displacement</td>
<td>0.2mm</td>
<td>0.29mm</td>
</tr>
<tr class="odd">
<td>Max. stress on the cavity walls</td>
<td>33MPa</td>
<td>35MPa</td>
</tr>
<tr class="even">
<td>Max. stress</td>
<td>42MPa (disc/ext. ring)</td>
<td>41MPa (disc/ext. ring)</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Load case results.

 

**Table:** Mechanical modes.

<table>
<caption>Table: Mechanical modes.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Mode number</td>
<td>Frequency (Hz)</td>
<td>Type of the mode</td>
</tr>
<tr class="even">
<td>1 &amp; 2</td>
<td>232</td>
<td>Beam tube mode</td>
</tr>
<tr class="odd">
<td>3</td>
<td>320</td>
<td>Spoke bar mode</td>
</tr>
<tr class="even">
<td>4</td>
<td>430</td>
<td>Spoke bar mode (beam axis)</td>
</tr>
<tr class="odd">
<td>5</td>
<td>433</td>
<td>End cap mode</td>
</tr>
<tr class="even">
<td>6, 7, &amp; 8</td>
<td>466 / 468 / 497</td>
<td>Coupled mode</td>
</tr>
<tr class="odd">
<td>9</td>
<td>511</td>
<td>Spoke bar mode</td>
</tr>
<tr class="even">
<td>10, 11 &amp; 12</td>
<td>525 / 531 / 539</td>
<td>Coupled mode</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Mechanical modes.

 

**Table:** RF results, CST MicroWave Studio vs ANSYS comparison.

<table>
<caption>Table: RF results, CST MicroWave Studio vs ANSYS comparison.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>RF parameters</td>
<td>CST MicroWave Studio</td>
<td>ANSYS</td>
</tr>
<tr class="even">
<td>Frequency (MHz)</td>
<td>352.2</td>
<td>352.2</td>
</tr>
<tr class="odd">
<td>Optimal β</td>
<td>0.37</td>
<td>0.37</td>
</tr>
<tr class="even">
<td><em>L</em><sub>acc</sub> = Ngap · β · λ/2<em>R</em><sub>p</sub> (m)</td>
<td>0.315</td>
<td>0.315</td>
</tr>
<tr class="odd">
<td><em>E</em><sub>pk</sub>/ <em>E</em><sub>acc</sub></td>
<td>4.29</td>
<td>4.32</td>
</tr>
<tr class="even">
<td>B<sub>pk</sub> / E<sub>acc</sub> (mT/MV m<sup>-1</sup>)</td>
<td>7.32</td>
<td>6.36</td>
</tr>
<tr class="odd">
<td>G (Ω)</td>
<td>109</td>
<td>109</td>
</tr>
<tr class="even">
<td>r/Q (Ω)</td>
<td>217</td>
<td>214</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: RF results, CST MicroWave Studio vs ANSYS comparison.

 

**Table:** Cavity stiffness.

<table>
<caption>Table: Cavity stiffness.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Detuning sensitivity</td>
<td>Stiffness</td>
<td>Von Mises stress</td>
</tr>
<tr class="even">
<td> </td>
<td>(kHz mm⁻¹)</td>
<td>(kN mm⁻¹)</td>
<td>for 1mm (MPa)</td>
</tr>
<tr class="odd">
<td>Cryomodule configuration</td>
<td>180</td>
<td>14.5</td>
<td>367</td>
</tr>
<tr class="even">
<td>Tensile test bench configuration</td>
<td>204</td>
<td>8.5</td>
<td>219</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Cavity stiffness.

 

**Table:** RF sensitivity.

<table>
<caption>Table: RF sensitivity.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Configuration</td>
<td>Kp (Hz mbar⁻¹)</td>
</tr>
<tr class="even">
<td>Bare cavity without rings</td>
<td>600</td>
</tr>
<tr class="odd">
<td>Bare cavity with rings</td>
<td>221</td>
</tr>
<tr class="even">
<td>Equipped cavity:</td>
<td> </td>
</tr>
<tr class="odd">
<td>‣ Without pressure on the vessel wall</td>
<td>–125</td>
</tr>
<tr class="even">
<td>‣ With pressure on the vessel wall</td>
<td>+25</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: RF sensitivity.

 

**Table:** RF sensitivity due to pressure.

<table>
<caption>Table: RF sensitivity due to pressure.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>K<em><sub>p</sub></em> (Hz mbar⁻¹)</td>
</tr>
<tr class="even">
<td>With 2 fixed ends</td>
<td>+23</td>
</tr>
<tr class="odd">
<td>With 1 fixed ends</td>
<td>+31</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: RF sensitivity due to pressure.

 

**Table:** Lorentz factor.

<table>
<caption>Table: Lorentz factor.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td colspan="2">Lorentz factor: K<em><sub>L</sub></em> = Δ<em>f</em> / E²<sub>acc</sub></td>
<td>For 8MV m<sup>-1</sup></td>
</tr>
<tr class="even">
<td>K<em><sub>L</sub></em> (Hz / (MV m<sup>-1</sup>)²), one free end</td>
<td>K<em><sub>L</sub></em> = -7.7</td>
<td>Δ<em>f</em> = 493Hz</td>
</tr>
<tr class="odd">
<td>K<em><sub>L</sub></em> (Hz / (MV m<sup>-1</sup>)²), fixed ends</td>
<td>K<em><sub>L</sub></em> = -4.7</td>
<td>Δ<em>f</em> = 300Hz</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Lorentz factor.

 

**Table:** Single-spoke cavity coupler main parameters.

<table>
<caption>Table: Single-spoke cavity coupler main parameters.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Beam mode</td>
<td>CW</td>
</tr>
<tr class="even">
<td>Frequency (MHz)</td>
<td>352.2</td>
</tr>
<tr class="odd">
<td>Temperature (K)</td>
<td>2</td>
</tr>
<tr class="even">
<td>P nominal (CW) (kW)</td>
<td>8</td>
</tr>
<tr class="odd">
<td>P Fault Tol. (CW) (kW)</td>
<td>14</td>
</tr>
<tr class="even">
<td>P design (CW) (kW)</td>
<td>20</td>
</tr>
<tr class="odd">
<td>Coupling mode</td>
<td>Capacitive</td>
</tr>
<tr class="even">
<td>Qext</td>
<td><strong>TBD: inline6838, img1</strong></td>
</tr>
<tr class="odd">
<td># of vacuum windows</td>
<td>1</td>
</tr>
<tr class="even">
<td>Mat. of vacuum window</td>
<td>Al₂O₃</td>
</tr>
<tr class="odd">
<td>thickness of vac. window (mm)</td>
<td>7</td>
</tr>
<tr class="even">
<td>ε<em><sub>r</sub></em> of vac. window</td>
<td>9.8</td>
</tr>
<tr class="odd">
<td>tan δ of vac. window</td>
<td>10e-4</td>
</tr>
<tr class="even">
<td><em>E</em><sub>max</sub> on window (MV m<sup>-1</sup>)</td>
<td>&lt; 1</td>
</tr>
<tr class="odd">
<td><em>E</em><sub>max</sub> triple junction (MV m<sup>-1</sup>)</td>
<td>&lt; 0.3</td>
</tr>
<tr class="even">
<td>Adjustability of antenna</td>
<td>fixed size</td>
</tr>
<tr class="odd">
<td>Max deviation of antenna</td>
<td>1.3°</td>
</tr>
<tr class="even">
<td>|S₁₁|</td>
<td>≤-30dB</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Single-spoke cavity coupler main parameters.

 

**Table:** Single-spoke cavity coupler results of electrical studies.

<table>
<caption>Table: Single-spoke cavity coupler results of electrical studies.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Without chokes</td>
<td>Mixed</td>
<td>With chokes</td>
<td>Spec</td>
</tr>
<tr class="even">
<td>|S₁₁|</td>
<td>≤ -50dB</td>
<td>≤ -52dB</td>
<td>≤ -77dB</td>
<td>≤ -30dB</td>
</tr>
<tr class="odd">
<td><em>E</em><sub>max</sub> on window (MV m<sup>-1</sup>)</td>
<td>&lt;0.50</td>
<td>&lt;0.98</td>
<td>&lt;0.65</td>
<td>&lt;1</td>
</tr>
<tr class="even">
<td><em>E</em><sub>max</sub> triple junction (MV m<sup>-1</sup>)</td>
<td>&lt;0.27</td>
<td>&lt;0.28</td>
<td>&lt;0.20</td>
<td>&lt;0.3</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Single-spoke cavity coupler results of electrical studies.

 

**Table:** Max E-field for a stored energy of 1J.

<table>
<caption>Table: Max E-field for a stored energy of 1J.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Flat-top</td>
<td>Spherical</td>
<td> </td>
</tr>
<tr class="even">
<td><em>E</em><sub>max</sub> on window (MV m<sup>-1</sup>)</td>
<td>&lt;0.50</td>
<td>&lt;0.66</td>
<td> </td>
</tr>
<tr class="odd">
<td>Penetration (mm)</td>
<td>99</td>
<td>104</td>
<td> </td>
</tr>
<tr class="even">
<td>Q<em><sub>ext</sub></em></td>
<td>2.34 × 10<sup>6</sup></td>
<td>2.24 × 10<sup>6</sup></td>
<td> </td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Max E-field for a stored energy of 1J.

 

**Table:** Multipactor thresholds.

<table>
<caption>Table: Multipactor thresholds.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Without chokes</td>
<td>Mixed</td>
<td>With chokes</td>
</tr>
<tr class="even">
<td>P (kW)</td>
<td>13</td>
<td>3</td>
<td>7.5</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Multipactor thresholds.

 

**Table:** K*~p~* (Hz mbar⁻¹).

<table>
<caption>Table: Kp (Hz mbar⁻¹).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Case</td>
<td>model 0</td>
<td>Model 1</td>
<td>Model 2</td>
<td>Model 3</td>
<td>Test results</td>
</tr>
<tr class="even">
<td>110</td>
<td>-123</td>
<td>-174</td>
<td>-374</td>
<td>-250</td>
<td>-170 (-245 max)</td>
</tr>
<tr class="odd">
<td>101</td>
<td>-32</td>
<td>+74</td>
<td>+280</td>
<td>+176</td>
<td>+188</td>
</tr>
<tr class="even">
<td>100</td>
<td>-154</td>
<td>-98</td>
<td>-94</td>
<td>-74</td>
<td>+14</td>
</tr>
<tr class="odd">
<td>121</td>
<td>+32</td>
<td>-76</td>
<td>-280</td>
<td>-176</td>
<td>-198</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Kp (Hz mbar⁻¹).

 

**Table:** Displacement at SAF side (mm).

<table>
<caption>Table: Displacement at SAF side (mm).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Case</td>
<td>Model 0</td>
<td>Model 1</td>
<td>Model 2</td>
<td>Model 3</td>
<td>Test results</td>
</tr>
<tr class="even">
<td>110</td>
<td>–0.19</td>
<td>–0.36</td>
<td>–0.65</td>
<td>–0.33</td>
<td>–0.64</td>
</tr>
<tr class="odd">
<td>101</td>
<td>≈0</td>
<td>+0.29</td>
<td>+0.5</td>
<td>+0.25</td>
<td>+0.38&lt;&gt;+0.6</td>
</tr>
<tr class="even">
<td>100</td>
<td>–0.19</td>
<td>–0.07</td>
<td>–0.12</td>
<td>–0.08</td>
<td>–0.2</td>
</tr>
<tr class="odd">
<td>121</td>
<td>≈0</td>
<td>–0.29</td>
<td>–0.5</td>
<td>–0.25</td>
<td>–0.47</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Displacement at SAF side (mm).

 

**Table:** Lorentz force factor (Hz / (MV m^-1^)²).

<table>
<caption>Table: Lorentz force factor (Hz / (MV m-1)²).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Case</td>
<td>Model 0</td>
<td>Model 1</td>
<td>Model 2</td>
<td>Model 3</td>
<td>Test results</td>
</tr>
<tr class="even">
<td>4K</td>
<td>Not evaluated</td>
<td>–8.8</td>
<td>Not evaluated</td>
<td>–8.15</td>
<td>–10.35</td>
</tr>
<tr class="odd">
<td>2K</td>
<td>–7.7</td>
<td>–8.7</td>
<td>–10.6</td>
<td>–8.1</td>
<td>–8.2</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Lorentz force factor (Hz / (MV m-1)²).

 

**Table:** Main components and conceptual design choices.

<table>
<caption>Table: Main components and conceptual design choices.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>1</td>
<td>From XFEL or Project X: the cavity axis moves from 300K to 2K. The design goal is to reduce the displacement and have a position adjustment at 2K;</td>
</tr>
<tr class="even">
<td>2</td>
<td>From XFEL, Project X, LEP2, SNS: cylindrical vacuum vessel and cryostating from the vacuum vessel axis;</td>
</tr>
<tr class="odd">
<td>3</td>
<td>Small 2 cavities train, required by beam dynamics and fault tolerance capabilities (reliability);</td>
</tr>
<tr class="even">
<td>4</td>
<td>From XFEL, Project X, LEP2, SNS: separated vacuum, compulsory due to the high accelerating gradient required;</td>
</tr>
<tr class="odd">
<td>5</td>
<td>From SPIRAL-2: only warm valves, so part of the vacuum vessel (as small as possible) has to be introduced inside clean room;</td>
</tr>
<tr class="even">
<td>6</td>
<td>From SPIRAL-2, SNS, RIA: RF power coupler with only one warm window already designed in the framework of EUROTRANS/EURISOL projects;</td>
</tr>
<tr class="odd">
<td>7</td>
<td>From XFEL, SNS, SPIRAL-2, LEP2: no cold focusing components inside the cryomodule;</td>
</tr>
<tr class="even">
<td>8</td>
<td>From SPIRAL-2: cold magnetic shield (2K) actively cooled;</td>
</tr>
<tr class="odd">
<td>9</td>
<td>From Project X, SNS, SPIRAL-2: one thermal shield cooled to around 60K;</td>
</tr>
<tr class="even">
<td>10</td>
<td>From XFEL: CTS at 2K/4K with stepping motor and piezoelectric actuators.</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main components and conceptual design choices.

 

**Table:** Thermo-mechanical evaluated values.

<table>
<caption>Table: Thermo-mechanical evaluated values.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>STATIC</td>
<td>Q (300K ⇨ 60K) &lt; 92W</td>
</tr>
<tr class="even">
<td>HEAT</td>
<td>Q (60K ⇨ 10K) &lt; 8W</td>
</tr>
<tr class="odd">
<td>LOADS</td>
<td>Q (60K ⇨ 2K) &lt; 1W</td>
</tr>
<tr class="even">
<td> </td>
<td>Q (10K ⇨ 2K) &lt; 2.2W</td>
</tr>
<tr class="odd">
<td>CAVITY DISPLACE-</td>
<td>(Vertical) ΔY<sub>max</sub> = -1mm</td>
</tr>
<tr class="even">
<td>MENT FROM 300K</td>
<td>(Longitudinal) ΔZ<sub>max</sub>= -0.4mm</td>
</tr>
<tr class="odd">
<td>TO 2K</td>
<td>(Lateral) ΔX<sub>max</sub> ≈ 0mm</td>
</tr>
<tr class="even">
<td>MECHANICAL EIGEN</td>
<td>Sliding blocks: f ≈90Hz</td>
</tr>
<tr class="odd">
<td>FREQUENCIES</td>
<td>Support table: f ≈20Hz</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Thermo-mechanical evaluated values.

 

**Table:** CTS design EM/mechanical parameters.

<table>
<caption>Table: CTS design EM/mechanical parameters.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Cavity tuning stiffness</td>
<td>14.5</td>
<td>kN mm⁻¹</td>
</tr>
<tr class="even">
<td>Elongation sensitivity</td>
<td>180</td>
<td>kHz mm⁻¹</td>
</tr>
<tr class="odd">
<td>Sensitivity to bath pressure (1 free end)</td>
<td>+31</td>
<td>Hz mbar⁻¹</td>
</tr>
<tr class="even">
<td>Sensitivity to bath pressure (2 fixed ends)</td>
<td>+23</td>
<td>Hz mbar⁻¹</td>
</tr>
<tr class="odd">
<td>Lorentz Force Factor (1 free end)</td>
<td>–7.7</td>
<td>Hz / (MV m<sup>-1</sup>)²</td>
</tr>
<tr class="even">
<td>Lorentz Force Factor (2 fixed ends)</td>
<td>–4.7</td>
<td>Hz / (MV m<sup>-1</sup>)²</td>
</tr>
<tr class="odd">
<td>Bandwidth</td>
<td>160</td>
<td>Hz</td>
</tr>
<tr class="even">
<td>CTS mechanical resolution (required: less than 1/20 of bandwidth)</td>
<td>8</td>
<td>Hz</td>
</tr>
<tr class="odd">
<td>CTS stiffness</td>
<td>110</td>
<td>kN mm⁻¹</td>
</tr>
<tr class="even">
<td>Ratio of deformation</td>
<td>88</td>
<td>%</td>
</tr>
<tr class="odd">
<td>Effective tuning sensitivity</td>
<td>158</td>
<td>kHz mm⁻¹</td>
</tr>
<tr class="even">
<td>Effective slow tuning resolution</td>
<td>0.2</td>
<td>kHz / step</td>
</tr>
<tr class="odd">
<td>Fast tuning range</td>
<td>500</td>
<td>Hz</td>
</tr>
<tr class="even">
<td>Maximum cavity deformation</td>
<td>1.1</td>
<td>mm</td>
</tr>
<tr class="odd">
<td>Maximum operating tuning range</td>
<td>174</td>
<td>kHz</td>
</tr>
<tr class="even">
<td>Maximum strength</td>
<td>16</td>
<td>kN</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: CTS design EM/mechanical parameters.

 

**Table:** Single-spoke cavity main parameters.

<table>
<caption>Table: Single-spoke cavity main parameters.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Beam mode</td>
<td>CW</td>
</tr>
<tr class="even">
<td>Frequency (MHz)</td>
<td>352.2</td>
</tr>
<tr class="odd">
<td>Optimal β</td>
<td>0.37</td>
</tr>
<tr class="even">
<td>Temperature (K)</td>
<td>2</td>
</tr>
<tr class="odd">
<td>Vo·T (MV m<sup>-1</sup>) @ 1J &amp; <em>β</em><sub>optimal</sub></td>
<td>0.693</td>
</tr>
<tr class="even">
<td><em>E</em><sub>pk</sub>/ <em>E</em><sub>acc</sub></td>
<td>4.29</td>
</tr>
<tr class="odd">
<td><em>B</em><sub>pk</sub>/ <em>E</em><sub>acc</sub> (mT / (MV m<sup>-1</sup>))</td>
<td>7.32</td>
</tr>
<tr class="even">
<td>G (Ω)</td>
<td>109</td>
</tr>
<tr class="odd">
<td>r/Q (Ω)</td>
<td>217</td>
</tr>
<tr class="even">
<td>Q<sub>0</sub> @ 2K for R<em><sub>res</sub></em> = 20nΩ</td>
<td>5.2e9</td>
</tr>
<tr class="odd">
<td>P<em><sub>cav</sub></em> for Q<sub>0</sub> = 2e9 &amp; 6.4MV m<sup>-1</sup> (W)</td>
<td>9.35</td>
</tr>
<tr class="even">
<td><em>E</em><sub>acc</sub> for nominal operation (MV m<sup>-1</sup>)</td>
<td>6.4</td>
</tr>
<tr class="odd">
<td><em>E</em><sub>acc</sub> for fault tolerant operation (MV m<sup>-1</sup>)</td>
<td>8.2</td>
</tr>
<tr class="even">
<td><em>L</em><sub>acc</sub> = <em>β</em><sub>optimal</sub> · <em>c</em> · <em>f</em> (m)</td>
<td>0.315</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Single-spoke cavity main parameters.

 

**Table:** Material mechanical properties.

<table>
<caption>Table: Material mechanical properties.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Mechanical properties</td>
<td>Niobium</td>
<td>Titanium grade 2</td>
</tr>
<tr class="even">
<td>Young modulus E (GPa) at 300K</td>
<td>107</td>
<td>105</td>
</tr>
<tr class="odd">
<td>Poisson ratio</td>
<td>0.38</td>
<td>0.3</td>
</tr>
<tr class="even">
<td>Density (kg m⁻³)</td>
<td>8570</td>
<td>4500</td>
</tr>
<tr class="odd">
<td>Yield strength <em>R</em><sub>p</sub> 0.2% (MPa) @ 300K</td>
<td>50</td>
<td>275 (min.)</td>
</tr>
<tr class="even">
<td>Ultimate tensile strength (MPa) @ 300K</td>
<td>125</td>
<td>345 (min.)</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Material mechanical properties.

 

**Table:** Load case results.

<table>
<caption>Table: Load case results.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Vacuum in the cavity</td>
<td>Vacuum in the helium tank</td>
</tr>
<tr class="even">
<td>Displacement</td>
<td>0.2mm</td>
<td>0.29mm</td>
</tr>
<tr class="odd">
<td>Max. stress on the cavity walls</td>
<td>33MPa</td>
<td>35MPa</td>
</tr>
<tr class="even">
<td>Max. stress</td>
<td>42MPa (disc/ext. ring)</td>
<td>41MPa (disc/ext. ring)</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Load case results.

 

**Table:** Mechanical modes.

<table>
<caption>Table: Mechanical modes.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Mode number</td>
<td>Frequency (Hz)</td>
<td>Type of the mode</td>
</tr>
<tr class="even">
<td>1 &amp; 2</td>
<td>232</td>
<td>Beam tube mode</td>
</tr>
<tr class="odd">
<td>3</td>
<td>320</td>
<td>Spoke bar mode</td>
</tr>
<tr class="even">
<td>4</td>
<td>430</td>
<td>Spoke bar mode (beam axis)</td>
</tr>
<tr class="odd">
<td>5</td>
<td>433</td>
<td>End cap mode</td>
</tr>
<tr class="even">
<td>6, 7, &amp; 8</td>
<td>466 / 468 / 497</td>
<td>Coupled mode</td>
</tr>
<tr class="odd">
<td>9</td>
<td>511</td>
<td>Spoke bar mode</td>
</tr>
<tr class="even">
<td>10, 11 &amp; 12</td>
<td>525 / 531 / 539</td>
<td>Coupled mode</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Mechanical modes.

 

**Table:** RF results, CST MicroWave Studio vs ANSYS comparison.

<table>
<caption>Table: RF results, CST MicroWave Studio vs ANSYS comparison.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>RF parameters</td>
<td>CST MicroWave Studio</td>
<td>ANSYS</td>
</tr>
<tr class="even">
<td>Frequency (MHz)</td>
<td>352.2</td>
<td>352.2</td>
</tr>
<tr class="odd">
<td>Optimal β</td>
<td>0.37</td>
<td>0.37</td>
</tr>
<tr class="even">
<td><em>L</em><sub>acc</sub> = Ngap · β · λ/2<em>R</em><sub>p</sub> (m)</td>
<td>0.315</td>
<td>0.315</td>
</tr>
<tr class="odd">
<td><em>E</em><sub>pk</sub>/ <em>E</em><sub>acc</sub></td>
<td>4.29</td>
<td>4.32</td>
</tr>
<tr class="even">
<td>B<sub>pk</sub> / E<sub>acc</sub> (mT/MV m<sup>-1</sup>)</td>
<td>7.32</td>
<td>6.36</td>
</tr>
<tr class="odd">
<td>G (Ω)</td>
<td>109</td>
<td>109</td>
</tr>
<tr class="even">
<td>r/Q (Ω)</td>
<td>217</td>
<td>214</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: RF results, CST MicroWave Studio vs ANSYS comparison.

 

**Table:** Cavity stiffness.

<table>
<caption>Table: Cavity stiffness.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Detuning sensitivity</td>
<td>Stiffness</td>
<td>Von Mises stress</td>
</tr>
<tr class="even">
<td> </td>
<td>(kHz mm⁻¹)</td>
<td>(kN mm⁻¹)</td>
<td>for 1mm (MPa)</td>
</tr>
<tr class="odd">
<td>Cryomodule configuration</td>
<td>180</td>
<td>14.5</td>
<td>367</td>
</tr>
<tr class="even">
<td>Tensile test bench configuration</td>
<td>204</td>
<td>8.5</td>
<td>219</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Cavity stiffness.

 

**Table:** RF sensitivity.

<table>
<caption>Table: RF sensitivity.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Configuration</td>
<td>Kp (Hz mbar⁻¹)</td>
</tr>
<tr class="even">
<td>Bare cavity without rings</td>
<td>600</td>
</tr>
<tr class="odd">
<td>Bare cavity with rings</td>
<td>221</td>
</tr>
<tr class="even">
<td>Equipped cavity:</td>
<td> </td>
</tr>
<tr class="odd">
<td>‣ Without pressure on the vessel wall</td>
<td>–125</td>
</tr>
<tr class="even">
<td>‣ With pressure on the vessel wall</td>
<td>+25</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: RF sensitivity.

 

**Table:** RF sensitivity due to pressure.

<table>
<caption>Table: RF sensitivity due to pressure.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>K<em><sub>p</sub></em> (Hz mbar⁻¹)</td>
</tr>
<tr class="even">
<td>With 2 fixed ends</td>
<td>+23</td>
</tr>
<tr class="odd">
<td>With 1 fixed ends</td>
<td>+31</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: RF sensitivity due to pressure.

 

**Table:** Lorentz factor.

<table>
<caption>Table: Lorentz factor.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td colspan="2">Lorentz factor: K<em><sub>L</sub></em> = Δ<em>f</em> / E²<sub>acc</sub></td>
<td>For 8MV m<sup>-1</sup></td>
</tr>
<tr class="even">
<td>K<em><sub>L</sub></em> (Hz / (MV m<sup>-1</sup>)²), one free end</td>
<td>K<em><sub>L</sub></em> = -7.7</td>
<td>Δ<em>f</em> = 493Hz</td>
</tr>
<tr class="odd">
<td>K<em><sub>L</sub></em> (Hz / (MV m<sup>-1</sup>)²), fixed ends</td>
<td>K<em><sub>L</sub></em> = -4.7</td>
<td>Δ<em>f</em> = 300Hz</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Lorentz factor.

 

**Table:** Single-spoke cavity coupler main parameters.

<table>
<caption>Table: Single-spoke cavity coupler main parameters.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Beam mode</td>
<td>CW</td>
</tr>
<tr class="even">
<td>Frequency (MHz)</td>
<td>352.2</td>
</tr>
<tr class="odd">
<td>Temperature (K)</td>
<td>2</td>
</tr>
<tr class="even">
<td>P nominal (CW) (kW)</td>
<td>8</td>
</tr>
<tr class="odd">
<td>P Fault Tol. (CW) (kW)</td>
<td>14</td>
</tr>
<tr class="even">
<td>P design (CW) (kW)</td>
<td>20</td>
</tr>
<tr class="odd">
<td>Coupling mode</td>
<td>Capacitive</td>
</tr>
<tr class="even">
<td>Qext</td>
<td><strong>TBD: inline6838, img1</strong></td>
</tr>
<tr class="odd">
<td># of vacuum windows</td>
<td>1</td>
</tr>
<tr class="even">
<td>Mat. of vacuum window</td>
<td>Al₂O₃</td>
</tr>
<tr class="odd">
<td>thickness of vac. window (mm)</td>
<td>7</td>
</tr>
<tr class="even">
<td>ε<em><sub>r</sub></em> of vac. window</td>
<td>9.8</td>
</tr>
<tr class="odd">
<td>tan δ of vac. window</td>
<td>10e-4</td>
</tr>
<tr class="even">
<td><em>E</em><sub>max</sub> on window (MV m<sup>-1</sup>)</td>
<td>&lt; 1</td>
</tr>
<tr class="odd">
<td><em>E</em><sub>max</sub> triple junction (MV m<sup>-1</sup>)</td>
<td>&lt; 0.3</td>
</tr>
<tr class="even">
<td>Adjustability of antenna</td>
<td>fixed size</td>
</tr>
<tr class="odd">
<td>Max deviation of antenna</td>
<td>1.3°</td>
</tr>
<tr class="even">
<td>|S₁₁|</td>
<td>≤-30dB</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Single-spoke cavity coupler main parameters.

 

**Table:** Single-spoke cavity coupler results of electrical studies.

<table>
<caption>Table: Single-spoke cavity coupler results of electrical studies.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Without chokes</td>
<td>Mixed</td>
<td>With chokes</td>
<td>Spec</td>
</tr>
<tr class="even">
<td>|S₁₁|</td>
<td>≤ -50dB</td>
<td>≤ -52dB</td>
<td>≤ -77dB</td>
<td>≤ -30dB</td>
</tr>
<tr class="odd">
<td><em>E</em><sub>max</sub> on window (MV m<sup>-1</sup>)</td>
<td>&lt;0.50</td>
<td>&lt;0.98</td>
<td>&lt;0.65</td>
<td>&lt;1</td>
</tr>
<tr class="even">
<td><em>E</em><sub>max</sub> triple junction (MV m<sup>-1</sup>)</td>
<td>&lt;0.27</td>
<td>&lt;0.28</td>
<td>&lt;0.20</td>
<td>&lt;0.3</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Single-spoke cavity coupler results of electrical studies.

 

**Table:** Max E-field for a stored energy of 1J.

<table>
<caption>Table: Max E-field for a stored energy of 1J.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Flat-top</td>
<td>Spherical</td>
<td> </td>
</tr>
<tr class="even">
<td><em>E</em><sub>max</sub> on window (MV m<sup>-1</sup>)</td>
<td>&lt;0.50</td>
<td>&lt;0.66</td>
<td> </td>
</tr>
<tr class="odd">
<td>Penetration (mm)</td>
<td>99</td>
<td>104</td>
<td> </td>
</tr>
<tr class="even">
<td>Q<em><sub>ext</sub></em></td>
<td>2.34 × 10<sup>6</sup></td>
<td>2.24 × 10<sup>6</sup></td>
<td> </td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Max E-field for a stored energy of 1J.

 

**Table:** Multipactor thresholds.

<table>
<caption>Table: Multipactor thresholds.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Without chokes</td>
<td>Mixed</td>
<td>With chokes</td>
</tr>
<tr class="even">
<td>P (kW)</td>
<td>13</td>
<td>3</td>
<td>7.5</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Multipactor thresholds.

 

**Table:** K*~p~* (Hz mbar⁻¹).

<table>
<caption>Table: Kp (Hz mbar⁻¹).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Case</td>
<td>model 0</td>
<td>Model 1</td>
<td>Model 2</td>
<td>Model 3</td>
<td>Test results</td>
</tr>
<tr class="even">
<td>110</td>
<td>-123</td>
<td>-174</td>
<td>-374</td>
<td>-250</td>
<td>-170 (-245 max)</td>
</tr>
<tr class="odd">
<td>101</td>
<td>-32</td>
<td>+74</td>
<td>+280</td>
<td>+176</td>
<td>+188</td>
</tr>
<tr class="even">
<td>100</td>
<td>-154</td>
<td>-98</td>
<td>-94</td>
<td>-74</td>
<td>+14</td>
</tr>
<tr class="odd">
<td>121</td>
<td>+32</td>
<td>-76</td>
<td>-280</td>
<td>-176</td>
<td>-198</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Kp (Hz mbar⁻¹).

 

**Table:** Displacement at SAF side (mm).

<table>
<caption>Table: Displacement at SAF side (mm).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Case</td>
<td>Model 0</td>
<td>Model 1</td>
<td>Model 2</td>
<td>Model 3</td>
<td>Test results</td>
</tr>
<tr class="even">
<td>110</td>
<td>–0.19</td>
<td>–0.36</td>
<td>–0.65</td>
<td>–0.33</td>
<td>–0.64</td>
</tr>
<tr class="odd">
<td>101</td>
<td>≈0</td>
<td>+0.29</td>
<td>+0.5</td>
<td>+0.25</td>
<td>+0.38&lt;&gt;+0.6</td>
</tr>
<tr class="even">
<td>100</td>
<td>–0.19</td>
<td>–0.07</td>
<td>–0.12</td>
<td>–0.08</td>
<td>–0.2</td>
</tr>
<tr class="odd">
<td>121</td>
<td>≈0</td>
<td>–0.29</td>
<td>–0.5</td>
<td>–0.25</td>
<td>–0.47</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Displacement at SAF side (mm).

 

**Table:** Lorentz force factor (Hz / (MV m^-1^)²).

<table>
<caption>Table: Lorentz force factor (Hz / (MV m-1)²).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Case</td>
<td>Model 0</td>
<td>Model 1</td>
<td>Model 2</td>
<td>Model 3</td>
<td>Test results</td>
</tr>
<tr class="even">
<td>4K</td>
<td>Not evaluated</td>
<td>–8.8</td>
<td>Not evaluated</td>
<td>–8.15</td>
<td>–10.35</td>
</tr>
<tr class="odd">
<td>2K</td>
<td>–7.7</td>
<td>–8.7</td>
<td>–10.6</td>
<td>–8.1</td>
<td>–8.2</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Lorentz force factor (Hz / (MV m-1)²).

 

**Table:** Main components and conceptual design choices.

<table>
<caption>Table: Main components and conceptual design choices.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>1</td>
<td>From XFEL or Project X: the cavity axis moves from 300K to 2K. The design goal is to reduce the displacement and have a position adjustment at 2K;</td>
</tr>
<tr class="even">
<td>2</td>
<td>From XFEL, Project X, LEP2, SNS: cylindrical vacuum vessel and cryostating from the vacuum vessel axis;</td>
</tr>
<tr class="odd">
<td>3</td>
<td>Small 2 cavities train, required by beam dynamics and fault tolerance capabilities (reliability);</td>
</tr>
<tr class="even">
<td>4</td>
<td>From XFEL, Project X, LEP2, SNS: separated vacuum, compulsory due to the high accelerating gradient required;</td>
</tr>
<tr class="odd">
<td>5</td>
<td>From SPIRAL-2: only warm valves, so part of the vacuum vessel (as small as possible) has to be introduced inside clean room;</td>
</tr>
<tr class="even">
<td>6</td>
<td>From SPIRAL-2, SNS, RIA: RF power coupler with only one warm window already designed in the framework of EUROTRANS/EURISOL projects;</td>
</tr>
<tr class="odd">
<td>7</td>
<td>From XFEL, SNS, SPIRAL-2, LEP2: no cold focusing components inside the cryomodule;</td>
</tr>
<tr class="even">
<td>8</td>
<td>From SPIRAL-2: cold magnetic shield (2K) actively cooled;</td>
</tr>
<tr class="odd">
<td>9</td>
<td>From Project X, SNS, SPIRAL-2: one thermal shield cooled to around 60K;</td>
</tr>
<tr class="even">
<td>10</td>
<td>From XFEL: CTS at 2K/4K with stepping motor and piezoelectric actuators.</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Main components and conceptual design choices.

 

**Table:** Thermo-mechanical evaluated values.

<table>
<caption>Table: Thermo-mechanical evaluated values.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>STATIC</td>
<td>Q (300K ⇨ 60K) &lt; 92W</td>
</tr>
<tr class="even">
<td>HEAT</td>
<td>Q (60K ⇨ 10K) &lt; 8W</td>
</tr>
<tr class="odd">
<td>LOADS</td>
<td>Q (60K ⇨ 2K) &lt; 1W</td>
</tr>
<tr class="even">
<td> </td>
<td>Q (10K ⇨ 2K) &lt; 2.2W</td>
</tr>
<tr class="odd">
<td>CAVITY DISPLACE-</td>
<td>(Vertical) ΔY<sub>max</sub> = -1mm</td>
</tr>
<tr class="even">
<td>MENT FROM 300K</td>
<td>(Longitudinal) ΔZ<sub>max</sub>= -0.4mm</td>
</tr>
<tr class="odd">
<td>TO 2K</td>
<td>(Lateral) ΔX<sub>max</sub> ≈ 0mm</td>
</tr>
<tr class="even">
<td>MECHANICAL EIGEN</td>
<td>Sliding blocks: f ≈90Hz</td>
</tr>
<tr class="odd">
<td>FREQUENCIES</td>
<td>Support table: f ≈20Hz</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Thermo-mechanical evaluated values.

 

**Table:** CTS design EM/mechanical parameters.

<table>
<caption>Table: CTS design EM/mechanical parameters.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Cavity tuning stiffness</td>
<td>14.5</td>
<td>kN mm⁻¹</td>
</tr>
<tr class="even">
<td>Elongation sensitivity</td>
<td>180</td>
<td>kHz mm⁻¹</td>
</tr>
<tr class="odd">
<td>Sensitivity to bath pressure (1 free end)</td>
<td>+31</td>
<td>Hz mbar⁻¹</td>
</tr>
<tr class="even">
<td>Sensitivity to bath pressure (2 fixed ends)</td>
<td>+23</td>
<td>Hz mbar⁻¹</td>
</tr>
<tr class="odd">
<td>Lorentz Force Factor (1 free end)</td>
<td>–7.7</td>
<td>Hz / (MV m<sup>-1</sup>)²</td>
</tr>
<tr class="even">
<td>Lorentz Force Factor (2 fixed ends)</td>
<td>–4.7</td>
<td>Hz / (MV m<sup>-1</sup>)²</td>
</tr>
<tr class="odd">
<td>Bandwidth</td>
<td>160</td>
<td>Hz</td>
</tr>
<tr class="even">
<td>CTS mechanical resolution (required: less than 1/20 of bandwidth)</td>
<td>8</td>
<td>Hz</td>
</tr>
<tr class="odd">
<td>CTS stiffness</td>
<td>110</td>
<td>kN mm⁻¹</td>
</tr>
<tr class="even">
<td>Ratio of deformation</td>
<td>88</td>
<td>%</td>
</tr>
<tr class="odd">
<td>Effective tuning sensitivity</td>
<td>158</td>
<td>kHz mm⁻¹</td>
</tr>
<tr class="even">
<td>Effective slow tuning resolution</td>
<td>0.2</td>
<td>kHz / step</td>
</tr>
<tr class="odd">
<td>Fast tuning range</td>
<td>500</td>
<td>Hz</td>
</tr>
<tr class="even">
<td>Maximum cavity deformation</td>
<td>1.1</td>
<td>mm</td>
</tr>
<tr class="odd">
<td>Maximum operating tuning range</td>
<td>174</td>
<td>kHz</td>
</tr>
<tr class="even">
<td>Maximum strength</td>
<td>16</td>
<td>kN</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: CTS design EM/mechanical parameters.

 

**Table:** Stability R&D objectives.

<table>
<caption>Table: Stability R&amp;D objectives.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Parameter</td>
<td>LinacLLRF-system</td>
<td>Master oscillator</td>
<td>PRDS</td>
</tr>
<tr class="even">
<td>Amplitude (RMS)</td>
<td>±0.2%</td>
<td>–</td>
<td>–</td>
</tr>
<tr class="odd">
<td>Phase (RMS)</td>
<td>±0.1%</td>
<td>±0.1%</td>
<td>±0.1%</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Stability R&D objectives.

 

**Table:** Jitter requirements.

<table>
<caption>Table: Jitter requirements.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Frequency (MHz)</td>
<td>Jitter RMS (Deg)</td>
<td>Jitter RMS (ps)</td>
</tr>
<tr class="even">
<td>176.1</td>
<td>0.1</td>
<td>1.58</td>
</tr>
<tr class="odd">
<td>352.2</td>
<td>0.1</td>
<td>0.79</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Jitter requirements.

 

**Table:** Major RF amplifier parameters requirements.

<table>
<caption>Table: Major RF amplifier parameters requirements.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th>Main RF requirements</th>
<th>Unit</th>
<th>Expected</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Center frequency</td>
<td>MHz</td>
<td>176.1 or 352.2</td>
</tr>
<tr class="even">
<td>Bandwidth (@ -1dB)</td>
<td>MHz</td>
<td>&gt; +/- 1</td>
</tr>
<tr class="odd">
<td>Nominal input level</td>
<td>dBm</td>
<td>&lt; 10</td>
</tr>
<tr class="even">
<td>Output connector type</td>
<td>EIA </td>
<td>EIA coaxial flange</td>
</tr>
<tr class="odd">
<td>Rated output power including losses and margins</td>
<td>kW</td>
<td>See Tables <a href="#tabrfsysinj-amp-reqs">[*]</a>–<a href="#tabrfsyslinac-amp-reqs-b">[*]</a></td>
</tr>
<tr class="even">
<td>Gain linearity over 20dB dynamic range</td>
<td>dB</td>
<td>&lt; 10</td>
</tr>
<tr class="odd">
<td>Phase linearity over 20dB dynamic range</td>
<td>deg</td>
<td>&lt; 25</td>
</tr>
<tr class="even">
<td>Gain linearity @ ±1.22dB around nominal output level</td>
<td>dB</td>
<td>&lt; 1</td>
</tr>
<tr class="odd">
<td>Phase linearity @ ±1.22dB around nominal output level</td>
<td>deg</td>
<td>&lt; 18</td>
</tr>
<tr class="even">
<td>Pulse duration</td>
<td>μs</td>
<td>100 to CW</td>
</tr>
<tr class="odd">
<td>Pulse repetition rate</td>
<td>Hz</td>
<td>1 to 250</td>
</tr>
<tr class="even">
<td>Pulse rise time (10-90%)</td>
<td>μs</td>
<td>&lt; 2</td>
</tr>
<tr class="odd">
<td>Pulse amplitude overshoot/undershoot</td>
<td>%</td>
<td>&lt; 5</td>
</tr>
<tr class="even">
<td>Input VSWR</td>
<td> </td>
<td>&lt; 1.2</td>
</tr>
<tr class="odd">
<td>Time with full reflection @ required output level – all phases</td>
<td>μs</td>
<td>50</td>
</tr>
<tr class="even">
<td>Resilience to fast load variations (from matched to full reflection)</td>
<td>μs</td>
<td>50</td>
</tr>
<tr class="odd">
<td>@ required output level – all phases</td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>Time with VSWR up to 2 @ required output level – all phases</td>
<td>s</td>
<td>&gt; 3600</td>
</tr>
<tr class="odd">
<td>Harmonics @ required output level</td>
<td>dBc</td>
<td>&lt; -30</td>
</tr>
<tr class="even">
<td>Spurious @ required output level</td>
<td>dBc</td>
<td>&lt; -60</td>
</tr>
<tr class="odd">
<td>Overall efficiency @ nominal output level</td>
<td>%</td>
<td>&gt; 50</td>
</tr>
<tr class="even">
<td>EMC compliance</td>
<td> </td>
<td>CISPR11, IEC61000-6-2</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Major RF amplifier parameters requirements.

 

**Table:** Major amplifier control requirements.

<table>
<caption>Table: Major amplifier control requirements.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>Main control requirements</th>
<th>Expected</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td colspan="2">Permanent and Slow transfer to</td>
</tr>
<tr class="even">
<td>Global transfer time</td>
<td>&lt; 1 sec</td>
</tr>
<tr class="odd">
<td>Transfer protocol</td>
<td>Ethercat &amp; Modbus TCP/IP</td>
</tr>
<tr class="even">
<td colspan="2">Fast acquisition buffering for post or online processing</td>
</tr>
<tr class="odd">
<td>Data of interest</td>
<td>Flags, RF powers, currents and voltages</td>
</tr>
<tr class="even">
<td>Embedded buffers storage</td>
<td>&gt; 10 sec</td>
</tr>
<tr class="odd">
<td>Quantity of embedded buffers</td>
<td>2 sets</td>
</tr>
<tr class="even">
<td>Granularity</td>
<td>&lt; 2 ms</td>
</tr>
<tr class="odd">
<td>Time stamp</td>
<td>For every buffered data</td>
</tr>
<tr class="even">
<td>Transfer protocol</td>
<td>Raw TCP</td>
</tr>
<tr class="odd">
<td colspan="2">Interlocks</td>
</tr>
<tr class="even">
<td>External interlock speed</td>
<td>&lt; 5μs</td>
</tr>
<tr class="odd">
<td>Internal interlocks</td>
<td>VSWR, overdrive, RF power, voltage and current, cooling temperature, water flow...</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Major amplifier control requirements.

 

**Table:** Major parameters requirements for the Injector amplifiers.

<table>
<caption>Table: Major parameters requirements for the Injector amplifiers.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Naming</td>
<td>Acronym</td>
<td>Nominal power</td>
<td>Required power</td>
<td>Output conn. size</td>
</tr>
<tr class="even">
<td>convention</td>
<td> </td>
<td>incl. losses (kW)</td>
<td>incl. margins (kW)</td>
<td>(EIA standard)</td>
</tr>
<tr class="odd">
<td>I1-RFQ</td>
<td>RFQ</td>
<td>111.5</td>
<td>167.3</td>
<td>6 ⅛″</td>
</tr>
<tr class="even">
<td>I1-ME1:RF-CAV-0
1</td>
<td>QWR1</td>
<td>2.3</td>
<td>3.4</td>
<td>1 ⅝″</td>
</tr>
<tr class="odd">
<td>I1-ME1:RF-CAV-0
2</td>
<td>QWR2</td>
<td>2.7</td>
<td>4.1</td>
<td>1 ⅝″</td>
</tr>
<tr class="even">
<td>I1-CH
1:RF-CAV-01</td>
<td>CH01</td>
<td>9.3</td>
<td>13.9</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>I1-CH
2:RF-CAV-01</td>
<td>CH02</td>
<td>13.8</td>
<td>20.7</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>I1-CH
3:RF-CAV-01</td>
<td>CH03</td>
<td>17.9</td>
<td>26.9</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>I1-CH
4:RF-CAV-01</td>
<td>CH04</td>
<td>20.7</td>
<td>31.0</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>I1-CH
5:RF-CAV-01</td>
<td>CH05</td>
<td>24.5</td>
<td>36.8</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>I1-CH
6:RF-CAV-01</td>
<td>CH06</td>
<td>33.8</td>
<td>50.7</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>I1-CH
7:RF-CAV-01</td>
<td>CH07</td>
<td>31.1</td>
<td>46.6</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>I1-ME3:RF-CAV-01</td>
<td>CHR1</td>
<td>8.2</td>
<td>12.3</td>
<td>1 ⅝″</td>
</tr>
<tr class="even">
<td>I1-CH
8:RF-CAV-01</td>
<td>CH08</td>
<td>41.1</td>
<td>61.7</td>
<td>4 ½″</td>
</tr>
<tr class="odd">
<td>I1-CH
9:RF-CAV-01</td>
<td>CH09</td>
<td>40.7</td>
<td>61.0</td>
<td>4 ½″</td>
</tr>
<tr class="even">
<td>I1-CH
10:RF-CAV-01</td>
<td>CH10</td>
<td>40.6</td>
<td>60.9</td>
<td>4 ½″</td>
</tr>
<tr class="odd">
<td>I1-CH
11:RF-CAV-01</td>
<td>CH11</td>
<td>40.3</td>
<td>60.5</td>
<td>4 ½″</td>
</tr>
<tr class="even">
<td>I1-CH
12:RF-CAV-01</td>
<td>CH12</td>
<td>39.8</td>
<td>59.7</td>
<td>4 ½″</td>
</tr>
<tr class="odd">
<td>I1-CH
13:RF-CAV-01</td>
<td>CH13</td>
<td>39.4</td>
<td>59.0</td>
<td>4 ½″</td>
</tr>
<tr class="even">
<td>I1-CH
14:RF-CAV-01</td>
<td>CH14</td>
<td>38.9</td>
<td>58.4</td>
<td>4 ½″</td>
</tr>
<tr class="odd">
<td>I1-CH
15:RF-CAV-01</td>
<td>CH15</td>
<td>38.8</td>
<td>58.2</td>
<td>4 ½″</td>
</tr>
<tr class="even">
<td>I1-ME2:RF-CAV-01</td>
<td>CHR2</td>
<td>TBD</td>
<td>TBD</td>
<td>TBD</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Major parameters requirements for the Injector amplifiers.

 

**Table:** Major parameters requirements for the amplifiers (Spokes
1--30).

<table>
<caption>Table: Major parameters requirements for the amplifiers (Spokes 1–30).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Naming</td>
<td>Acronym</td>
<td>Nominal power</td>
<td>Required power</td>
<td>Output conn. size</td>
</tr>
<tr class="even">
<td>Convention</td>
<td> </td>
<td>incl. losses (kW)</td>
<td>incl. margins (kW)</td>
<td>(EIA standard)</td>
</tr>
<tr class="odd">
<td>LB-ME3:RF-CAV-01</td>
<td>Spoke Reb. 1</td>
<td>3.847</td>
<td>5.77</td>
<td>1 ⅝″</td>
</tr>
<tr class="even">
<td>LB-ME3:RF-CAV-02</td>
<td>Spoke Reb. 2</td>
<td>3.85</td>
<td>5.77</td>
<td>1 ⅝″</td>
</tr>
<tr class="odd">
<td>LB-S00:RF-CAV-01</td>
<td>Spoke 1</td>
<td>8.44</td>
<td>12.66</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S01:RF-CAV-02</td>
<td>Spoke 2</td>
<td>8.50</td>
<td>12.75</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S01:RF-CAV-01</td>
<td>Spoke 3</td>
<td>8.57</td>
<td>12.85</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S02:RF-CAV-02</td>
<td>Spoke 4</td>
<td>8.63</td>
<td>12.95</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S02:RF-CAV-01</td>
<td>Spoke 5</td>
<td>8.72</td>
<td>13.08</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S03:RF-CAV-02</td>
<td>Spoke 6</td>
<td>8.80</td>
<td>13.19</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S03:RF-CAV-01</td>
<td>Spoke 7</td>
<td>8.92</td>
<td>13.37</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S04:RF-CAV-02</td>
<td>Spoke 8</td>
<td>9.00</td>
<td>13.50</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S04:RF-CAV-01</td>
<td>Spoke 9</td>
<td>9.16</td>
<td>13.74</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S05:RF-CAV-02</td>
<td>Spoke 10</td>
<td>9.25</td>
<td>13.88</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S05:RF-CAV-01</td>
<td>Spoke 11</td>
<td>9.47</td>
<td>14.21</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S06:RF-CAV-02</td>
<td>Spoke 12</td>
<td>9.58</td>
<td>14.37</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S06:RF-CAV-01</td>
<td>Spoke 13</td>
<td>9.89</td>
<td>14.84</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S07:RF-CAV-02</td>
<td>Spoke 14</td>
<td>10.02</td>
<td>15.03</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S07:RF-CAV-01</td>
<td>Spoke 15</td>
<td>10.46</td>
<td>15.70</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S08:RF-CAV-02</td>
<td>Spoke 16</td>
<td>10.62</td>
<td>15.92</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S08:RF-CAV-01</td>
<td>Spoke 17</td>
<td>11.29</td>
<td>16.93</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S09:RF-CAV-02</td>
<td>Spoke 18</td>
<td>11.47</td>
<td>17.21</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S09:RF-CAV-01</td>
<td>Spoke 19</td>
<td>12.54</td>
<td>18.81</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S10:RF-CAV-02</td>
<td>Spoke 20</td>
<td>12.77</td>
<td>19.16</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S10:RF-CAV-01</td>
<td>Spoke 21</td>
<td>13.85</td>
<td>20.78</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S11:RF-CAV-02</td>
<td>Spoke 22</td>
<td>14.10</td>
<td>21.15</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S11:RF-CAV-01</td>
<td>Spoke 23</td>
<td>14.39</td>
<td>21.58</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S12:RF-CAV-02</td>
<td>Spoke 24</td>
<td>14.58</td>
<td>21.87</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S12:RF-CAV-01</td>
<td>Spoke 25</td>
<td>14.77</td>
<td>22.16</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S13:RF-CAV-02</td>
<td>Spoke 26</td>
<td>14.91</td>
<td>22.36</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S13:RF-CAV-01</td>
<td>Spoke 27</td>
<td>15.05</td>
<td>22.58</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S14:RF-CAV-02</td>
<td>Spoke 28</td>
<td>15.15</td>
<td>22.72</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S14:RF-CAV-01</td>
<td>Spoke 29</td>
<td>15.25</td>
<td>22.88</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S15:RF-CAV-02</td>
<td>Spoke 30</td>
<td>15.31</td>
<td>22.97</td>
<td>3 ⅛″</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Major parameters requirements for the amplifiers (Spokes 1--30).

 

**Table:** Major parameters requirements for the amplifiers (Spokes
31--46).

<table>
<caption>Table: Major parameters requirements for the amplifiers (Spokes 31–46).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Naming</td>
<td>Acronym</td>
<td>Nominal power</td>
<td>Required power</td>
<td>Output conn.</td>
</tr>
<tr class="even">
<td>Convention</td>
<td> </td>
<td>incl. losses (kW)</td>
<td>incl. margins (kW)</td>
<td>size (EIA standard)</td>
</tr>
<tr class="odd">
<td>LB-S15:RF-CAV-01</td>
<td>Spoke 31</td>
<td>15.38</td>
<td>23.07</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S16:RF-CAV-02</td>
<td>Spoke 32</td>
<td>15.41</td>
<td>23.12</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S16:RF-CAV-01</td>
<td>Spoke 33</td>
<td>15.46</td>
<td>23.19</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S17:RF-CAV-02</td>
<td>Spoke 34</td>
<td>15.47</td>
<td>23.21</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S17:RF-CAV-01</td>
<td>Spoke 35</td>
<td>15.50</td>
<td>23.25</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S18:RF-CAV-02</td>
<td>Spoke 36</td>
<td>15.50</td>
<td>23.25</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S18:RF-CAV-01</td>
<td>Spoke 37</td>
<td>15.51</td>
<td>23.27</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S19:RF-CAV-02</td>
<td>Spoke 38</td>
<td>15.50</td>
<td>23.25</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S19:RF-CAV-01</td>
<td>Spoke 39</td>
<td>15.50</td>
<td>23.25</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S20:RF-CAV-02</td>
<td>Spoke 40</td>
<td>15.49</td>
<td>23.23</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S20:RF-CAV-01</td>
<td>Spoke 41</td>
<td>15.48</td>
<td>23.22</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S21:RF-CAV-02</td>
<td>Spoke 42</td>
<td>15.46</td>
<td>23.19</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S21:RF-CAV-01</td>
<td>Spoke 43</td>
<td>15.45</td>
<td>23.17</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S22:RF-CAV-02</td>
<td>Spoke 44</td>
<td>15.42</td>
<td>23.13</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S22:RF-CAV-01</td>
<td>Spoke 45</td>
<td>15.41</td>
<td>23.11</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S23:RF-CAV-02</td>
<td>Spoke 46</td>
<td>15.38</td>
<td>23.07</td>
<td>3 ⅛″</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Major parameters requirements for the amplifiers (Spokes 31--46).

 

**Table:** Results overview of the RF characterisation of the RFQ
amplifier.

<table>
<caption>Table: Results overview of the RF characterisation of the RFQ amplifier.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>PARAMETERS</td>
<td>UNITS</td>
<td> 
RESULTS</td>
</tr>
<tr class="even">
<td>Max output power on matched load</td>
<td>kW</td>
<td>192</td>
</tr>
<tr class="odd">
<td>Input return loss over the full power range</td>
<td>dB</td>
<td>-20</td>
</tr>
<tr class="even">
<td>Wall plug efficiency</td>
<td>%</td>
<td>73.6 (max level) / 61.5 (operating level)</td>
</tr>
<tr class="odd">
<td>AC/DC converters efficiency</td>
<td>%</td>
<td>90 – 92</td>
</tr>
<tr class="even">
<td>Harmonics level over the full power range</td>
<td>dBc</td>
<td>&lt; -45</td>
</tr>
<tr class="odd">
<td>Gain variation within ±1MHz BW</td>
<td>dB</td>
<td>1.4 (100kW), 0.95 (140kW)</td>
</tr>
<tr class="even">
<td>Bandwidth (@ -1 dB)</td>
<td>MHz</td>
<td>&gt; ±1</td>
</tr>
<tr class="odd">
<td>Maximum gain</td>
<td>dB</td>
<td>85</td>
</tr>
<tr class="even">
<td>Large dynamic range (up to required level)</td>
<td>dB</td>
<td>&gt; 20</td>
</tr>
<tr class="odd">
<td>Gain linearity over large dynamic range</td>
<td>dB</td>
<td>8</td>
</tr>
<tr class="even">
<td>Phase linearity over large dynamic range</td>
<td>deg</td>
<td>20.5</td>
</tr>
<tr class="odd">
<td>Reduced dynamic range (around operating level)</td>
<td>dB</td>
<td>±1.22</td>
</tr>
<tr class="even">
<td>Gain linearity over reduced dynamic range</td>
<td>dB</td>
<td>0.46</td>
</tr>
<tr class="odd">
<td>Phase linearity over reduced dynamic range</td>
<td>deg</td>
<td>14.5</td>
</tr>
<tr class="even">
<td>1 dB compression point</td>
<td>kW</td>
<td>70</td>
</tr>
<tr class="odd">
<td>Warm up time at 120kW</td>
<td>sec</td>
<td>60</td>
</tr>
<tr class="even">
<td>Gain variations during warm up</td>
<td>dB</td>
<td>0.15</td>
</tr>
<tr class="odd">
<td>Phase variations during warm up</td>
<td>deg</td>
<td>0.57</td>
</tr>
<tr class="even">
<td>Gain stability over 1 s after warm up</td>
<td>dB</td>
<td>&lt; 0.02</td>
</tr>
<tr class="odd">
<td>Phase stability over 1 s after warm up</td>
<td>deg</td>
<td>&lt; 0.15</td>
</tr>
<tr class="even">
<td>Pulse rise time</td>
<td>µs</td>
<td>1.6</td>
</tr>
<tr class="odd">
<td>Pulse amplitude overshoot/undershoot</td>
<td>%</td>
<td>&lt; 5</td>
</tr>
<tr class="even">
<td>Group delay</td>
<td>ns</td>
<td>87</td>
</tr>
<tr class="odd">
<td>External combiner return loss</td>
<td>dB</td>
<td>&lt; -25</td>
</tr>
<tr class="even">
<td>External combiner directivity</td>
<td>dB</td>
<td>&gt; 25</td>
</tr>
<tr class="odd">
<td>Max CW forward power under VSWR = 2</td>
<td>kW</td>
<td>143</td>
</tr>
<tr class="even">
<td>Max CW forward power under VSWR = 3</td>
<td>kW</td>
<td>124</td>
</tr>
<tr class="odd">
<td>Max pulsed forward power under full reflection</td>
<td>kW</td>
<td>92 (10 ms, 40 %)</td>
</tr>
<tr class="even">
<td>Delivery of required forward power up to VSWR</td>
<td> </td>
<td>2</td>
</tr>
<tr class="odd">
<td>RF leak level at 120kW</td>
<td>dBμV / m</td>
<td>&lt; 104.5</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Results overview of the RF characterisation of the RFQ amplifier.

 

**Table:** SSAs repartition per cabinet for the Injector prototyping
activity.

<table>
<caption>Table: SSAs repartition per cabinet for the Injector prototyping activity.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Acronym</td>
<td>QWR 1</td>
<td>QWR 2</td>
<td>CH01</td>
<td>CH02</td>
<td>CH03</td>
<td>CH04</td>
<td>CH05</td>
<td>CH06</td>
<td>CH07</td>
</tr>
<tr class="even">
<td>Naming convention</td>
<td>I1-ME1:RF-CAV-0
1</td>
<td>I1-ME1:RF-CAV-0
2</td>
<td>I1-CH
1:RF-CAV-01</td>
<td>I1-CH
2:RF-CAV-01</td>
<td>I1-CH
3:RF-CAV-01</td>
<td>I1-CH
4:RF-CAV-01</td>
<td>I1-CH
5:RF-CAV-01</td>
<td>I1-CH
6:RF-CAV-01</td>
<td>I1-CH
7:RF-CAV-01</td>
</tr>
<tr class="odd">
<td>Cabinet 1</td>
<td>6kW</td>
<td>6kW</td>
<td>24kW</td>
<td>24kW</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
</tr>
<tr class="even">
<td>Cabinet 2</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>36kW</td>
<td>36kW</td>
<td>–</td>
<td>–</td>
<td>–</td>
</tr>
<tr class="odd">
<td>Cabinet 3</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>48kW</td>
<td>–</td>
<td>48kW</td>
</tr>
<tr class="even">
<td>Cabinet 4</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>60kW</td>
<td>–</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: SSAs repartition per cabinet for the Injector prototyping
activity.

 

**Table:** Estimation of the SSA cabinet quantity for the MINERVA linac.

<table>
<caption>Table: Estimation of the SSA cabinet quantity for the MINERVA linac.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Acronym</td>
<td>RFQ</td>
<td>QWR1 ⇨ CH07</td>
<td>CHR1 ⇨ CH15</td>
<td>Spoke 1 ⇨ Spoke 46</td>
</tr>
<tr class="even">
<td>Naming</td>
<td>I1-RFQ</td>
<td>I1-ME1:RF-CAV-0
1 ⇨</td>
<td>I1-ME3:RF-CAV-01 ⇨</td>
<td>LB-S00:RF-CAV-01 ⇨</td>
</tr>
<tr class="odd">
<td>convention</td>
<td> </td>
<td>I1-CH
7:RF-CAV-01</td>
<td>I1-CH
15:RF-CAV-01</td>
<td>LB-S23:RF-CAV-02</td>
</tr>
<tr class="even">
<td>No. of cabinet</td>
<td>2</td>
<td>4</td>
<td>8</td>
<td>15</td>
</tr>
<tr class="odd">
<td>No. of output</td>
<td>0.5</td>
<td>4 (1<sup>st</sup> cabinet) 1 (4<sup>th</sup> cabinet)</td>
<td>2 (1<sup>st</sup> cabinet)</td>
<td>4</td>
</tr>
<tr class="even">
<td>per cabinet</td>
<td> </td>
<td>2 (others)</td>
<td>1 (others)</td>
<td> </td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Estimation of the SSA cabinet quantity for the MINERVA linac.

 

**Table:** Estimation of the water cooling requirements for the MINERVA
SSAs.

<table>
<caption>Table: Estimation of the water cooling requirements for the MINERVA SSAs.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td> </td>
<td>SSAs for Injector</td>
<td>SSAs for</td>
</tr>
<tr class="even">
<td>Medium</td>
<td> </td>
<td colspan="2">Normal water</td>
</tr>
<tr class="odd">
<td>Particle filtering size</td>
<td>μm</td>
<td colspan="2">&lt; 500</td>
</tr>
<tr class="even">
<td>Minimum flow rate</td>
<td>L min⁻¹</td>
<td>2105</td>
<td>1400</td>
</tr>
<tr class="odd">
<td>Input absolute pressure</td>
<td>bar</td>
<td colspan="2">&lt; 6</td>
</tr>
<tr class="even">
<td>Output absolute pressure</td>
<td>bar</td>
<td colspan="2">&gt; 1</td>
</tr>
<tr class="odd">
<td>Pressure drop</td>
<td>bar</td>
<td colspan="2">&lt; 4</td>
</tr>
<tr class="even">
<td>Input temperature</td>
<td>℃</td>
<td colspan="2">&lt; 25</td>
</tr>
<tr class="odd">
<td>Input temp. stability</td>
<td>℃</td>
<td colspan="2">&lt; ± 0.5</td>
</tr>
<tr class="even">
<td>Output temperature</td>
<td>℃</td>
<td colspan="2">&lt; 35</td>
</tr>
<tr class="odd">
<td>Delta temperature</td>
<td>℃</td>
<td colspan="2">&lt; 10</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Estimation of the water cooling requirements for the MINERVA
SSAs.

 

**Table:** Characteristics of some rectangular waveguides.

<table>
<caption>Table: Characteristics of some rectangular waveguides.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Waveguide EIA name</td>
<td>Recommended frequency band of operation (GHz)</td>
<td>Inner dimensions of waveguide opening (mm)</td>
<td>TE<sub>10</sub>-mode cut-off frequency (GHz)</td>
<td>TE<sub>20</sub>-mode cut-off frequency (GHz)</td>
</tr>
<tr class="even">
<td>WR2300</td>
<td>0.32 — 0.45</td>
<td>584.2 × 292.1</td>
<td>0.26</td>
<td>0.51</td>
</tr>
<tr class="odd">
<td>WR1150</td>
<td>0.63 — 0.97</td>
<td>292.1 × 146.05</td>
<td>0.51</td>
<td>1.03</td>
</tr>
<tr class="even">
<td>WR340</td>
<td>2.2 — 3.3</td>
<td>86.36 × 43.18</td>
<td>1.7</td>
<td>3.5</td>
</tr>
<tr class="odd">
<td>WR75</td>
<td>10 — 15</td>
<td>19.05 × 9.52</td>
<td>7.9</td>
<td>15.7</td>
</tr>
<tr class="even">
<td>WR10</td>
<td>70 — 110</td>
<td>2.54 × 1.27</td>
<td>59</td>
<td>118</td>
</tr>
<tr class="odd">
<td>WR3</td>
<td>220 — 330</td>
<td>0.864 × 0.432</td>
<td>173</td>
<td>346</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Characteristics of some rectangular waveguides.

 

**Table:** Dimensions and cut-off frequencies of most used rigid coaxial
waveguides.

<table>
<caption>Table: Dimensions and cut-off frequencies of most used rigid coaxial waveguides.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Coaxial EIA name</td>
<td colspan="2">Outer conductor</td>
<td colspan="2">Inner conductor</td>
<td>Cut-off frequency TE11-mode (GHz)</td>
</tr>
<tr class="even">
<td> </td>
<td>Outer diameter (mm)</td>
<td>Inner diameter (mm) = D₁</td>
<td>Outer diameter (mm) = D₂</td>
<td>Inner diameter (mm)</td>
<td> </td>
</tr>
<tr class="odd">
<td>⅞″</td>
<td>22.2</td>
<td>20</td>
<td>8.7</td>
<td>7.4</td>
<td>6.3</td>
</tr>
<tr class="even">
<td>1 ⅝″</td>
<td>41.3</td>
<td>38.8</td>
<td>16.9</td>
<td>15</td>
<td>3.2</td>
</tr>
<tr class="odd">
<td>3 ⅛″</td>
<td>79.4</td>
<td>76.9</td>
<td>33.4</td>
<td>31.3</td>
<td>1.6</td>
</tr>
<tr class="even">
<td>4 ½″</td>
<td>106</td>
<td>103</td>
<td>44.8</td>
<td>42.8</td>
<td>1.2</td>
</tr>
<tr class="odd">
<td>6 ⅛″</td>
<td>155.6</td>
<td>151.9</td>
<td>66</td>
<td>64</td>
<td>0.83</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Dimensions and cut-off frequencies of most used rigid coaxial
waveguides.

 

**Table:** Maximum power handling of coaxial waveguides.

<table>
<caption>Table: Maximum power handling of coaxial waveguides.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td colspan="5">CW power handling capability (kW)</td>
</tr>
<tr class="even">
<td>f (MHz)</td>
<td>⅞″</td>
<td>1 ⅝″</td>
<td>3 ⅛″</td>
<td>4 ½″</td>
<td>6 ⅛″</td>
</tr>
<tr class="odd">
<td>176.1</td>
<td>5.7</td>
<td>15</td>
<td>48</td>
<td>80</td>
<td>159</td>
</tr>
<tr class="even">
<td>352.2</td>
<td>4.1</td>
<td>11</td>
<td>34</td>
<td>57</td>
<td>111</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Maximum power handling of coaxial waveguides.

 

**Table:** Typical attenuations of coaxial waveguides.

<table>
<caption>Table: Typical attenuations of coaxial waveguides.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td colspan="5">Attenuation (dB/100m)</td>
</tr>
<tr class="even">
<td>f (MHz)</td>
<td>⅞″</td>
<td>1 ⅝″</td>
<td>3 ⅛″</td>
<td>4 ½″</td>
<td>6 ⅛″</td>
</tr>
<tr class="odd">
<td>176.1</td>
<td>1.61</td>
<td>0.99</td>
<td>0.49</td>
<td>0.37</td>
<td>0.25</td>
</tr>
<tr class="even">
<td>352.2</td>
<td>2.27</td>
<td>1.40</td>
<td>0.69</td>
<td>0.52</td>
<td>0.35</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Typical attenuations of coaxial waveguides.

 

**Table:** Configuration of the MINERVA Tx lines.

<table>
<caption>Table: Configuration of the MINERVA Tx lines.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Cavity name</td>
<td>Naming convention</td>
<td>Size of Tx line</td>
<td>Interface with SSA</td>
<td>Interface with power coupler</td>
</tr>
<tr class="even">
<td>RFQ</td>
<td>I1-RFQ</td>
<td>6 ⅛″</td>
<td>6 ⅛″ EIA</td>
<td>6 ⅛″ EIA</td>
</tr>
<tr class="odd">
<td>QWR1 &amp; 2</td>
<td>I1-ME1:RF-CAV-0
1 &amp; I1-ME1:RF-CAV-0
2</td>
<td>⅞″ (flexible)</td>
<td>1 ⅝″ EIA</td>
<td>1 ⅝″ EIA</td>
</tr>
<tr class="even">
<td>CH01 to 07</td>
<td>I1-CH
1:RF-CAV-01 to I1-CH
7:RF-CAV-01</td>
<td>3 ⅛″</td>
<td>4 ½″ EIA</td>
<td>6 ⅛″ EIA</td>
</tr>
<tr class="odd">
<td>CHR1</td>
<td>I1-ME3:RF-CAV-01</td>
<td>1 ⅝″</td>
<td>1 ⅝″ EIA</td>
<td>1 ⅝″ EIA</td>
</tr>
<tr class="even">
<td>CH08 to 15</td>
<td>I1-CH
8:RF-CAV-01 to I1-CH
15:RF-CAV-01</td>
<td>4 ½″</td>
<td>4 ½″ EIA</td>
<td>6 ⅛″ EIA</td>
</tr>
<tr class="odd">
<td>CHR2</td>
<td>I1-ME2:RF-CAV-01</td>
<td>TBD</td>
<td>TBD</td>
<td>6 ⅛″ EIA</td>
</tr>
<tr class="even">
<td>Spoke Reb. 1 &amp; 2</td>
<td>I1-ME3:RF-CAV-01 &amp; I1-ME2:RF-CAV-01</td>
<td>1 ⅝″ EIA</td>
<td>1 ⅝″ EIA</td>
<td>3 ⅛″ EIA</td>
</tr>
<tr class="odd">
<td>Spoke 1 to Spoke 46</td>
<td>LB-S00:RF-CAV-01 to LB-S23:RF-CAV-02</td>
<td>3 ⅛″ EIA</td>
<td>3 ⅛″ EIA</td>
<td>3 ⅛″ EIA</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Configuration of the MINERVA Tx lines.

 

**Table:** Transmission line ohmic losses per meter for the Injector
amplifiers.

<table>
<caption>Table: Transmission line ohmic losses per meter for the Injector amplifiers.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Naming</td>
<td>Acronym</td>
<td>Tx line losses per meter</td>
</tr>
<tr class="even">
<td>I1-RFQ</td>
<td>RFQ</td>
<td>71.89</td>
</tr>
<tr class="odd">
<td>I1-ME1:RF-CAV-0
1</td>
<td>QWR1</td>
<td>5.92</td>
</tr>
<tr class="even">
<td>I1-ME1:RF-CAV-0
2</td>
<td>QWR2</td>
<td>7.10</td>
</tr>
<tr class="odd">
<td>I1-CH
1:RF-CAV-01</td>
<td>CH01</td>
<td>11.96</td>
</tr>
<tr class="even">
<td>I1-CH
2:RF-CAV-01</td>
<td>CH02</td>
<td>17.75</td>
</tr>
<tr class="odd">
<td>I1-CH
3:RF-CAV-01</td>
<td>CH03</td>
<td>23.07</td>
</tr>
<tr class="even">
<td>I1-CH
4:RF-CAV-01</td>
<td>CH04</td>
<td>26.64</td>
</tr>
<tr class="odd">
<td>I1-CH
5:RF-CAV-01</td>
<td>CH05</td>
<td>31.61</td>
</tr>
<tr class="even">
<td>I1-CH
6:RF-CAV-01</td>
<td>CH06</td>
<td>43.59</td>
</tr>
<tr class="odd">
<td>I1-CH
7:RF-CAV-01</td>
<td>CH07</td>
<td>40.01</td>
</tr>
<tr class="even">
<td>I1-ME3:RF-CAV-01</td>
<td>CHR1</td>
<td>21.31</td>
</tr>
<tr class="odd">
<td>I1-CH
8:RF-CAV-01</td>
<td>CH08</td>
<td>34.08</td>
</tr>
<tr class="even">
<td>I1-CH
9:RF-CAV-01</td>
<td>CH09</td>
<td>33.72</td>
</tr>
<tr class="odd">
<td>I1-CH
10:RF-CAV-01</td>
<td>CH10</td>
<td>33.65</td>
</tr>
<tr class="even">
<td>I1-CH
11:RF-CAV-01</td>
<td>CH11</td>
<td>33.41</td>
</tr>
<tr class="odd">
<td>I1-CH
12:RF-CAV-01</td>
<td>CH12</td>
<td>32.96</td>
</tr>
<tr class="even">
<td>I1-CH
13:RF-CAV-01</td>
<td>CH13</td>
<td>32.61</td>
</tr>
<tr class="odd">
<td>I1-CH
14:RF-CAV-01</td>
<td>CH14</td>
<td>32.27</td>
</tr>
<tr class="even">
<td>I1-CH
15:RF-CAV-01</td>
<td>CH15</td>
<td>32.16</td>
</tr>
<tr class="odd">
<td>I1-ME2:RF-CAV-01</td>
<td>CHR2</td>
<td>TBD</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Transmission line ohmic losses per meter for the Injector
amplifiers.

 

**Table:** Transmission line ohmic losses per meter for the amplifiers
(Spoke rebunchers and spokes 1--22).

<table>
<caption>Table: Transmission line ohmic losses per meter for the amplifiers (Spoke rebunchers and spokes 1–22).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Naming</td>
<td>Acronym</td>
<td>Tx line losses per meter</td>
</tr>
<tr class="even">
<td>LB-ME3:RF-CAV-01</td>
<td>Spoke Reb. 1</td>
<td>12.38</td>
</tr>
<tr class="odd">
<td>LB-ME3:RF-CAV-02</td>
<td>Spoke Reb. 2</td>
<td>12.38</td>
</tr>
<tr class="even">
<td>LB-S00:RF-CAV-01</td>
<td>Spoke 1</td>
<td>13.40</td>
</tr>
<tr class="odd">
<td>LB-S01:RF-CAV-02</td>
<td>Spoke 2</td>
<td>13.49</td>
</tr>
<tr class="even">
<td>LB-S01:RF-CAV-01</td>
<td>Spoke 3</td>
<td>13.60</td>
</tr>
<tr class="odd">
<td>LB-S02:RF-CAV-02</td>
<td>Spoke 4</td>
<td>13.70</td>
</tr>
<tr class="even">
<td>LB-S02:RF-CAV-01</td>
<td>Spoke 5</td>
<td>13.85</td>
</tr>
<tr class="odd">
<td>LB-S03:RF-CAV-02</td>
<td>Spoke 6</td>
<td>13.96</td>
</tr>
<tr class="even">
<td>LB-S03:RF-CAV-01</td>
<td>Spoke 7</td>
<td>14.15</td>
</tr>
<tr class="odd">
<td>LB-S04:RF-CAV-02</td>
<td>Spoke 8</td>
<td>14.28</td>
</tr>
<tr class="even">
<td>LB-S04:RF-CAV-01</td>
<td>Spoke 9</td>
<td>14.54</td>
</tr>
<tr class="odd">
<td>LB-S05:RF-CAV-02</td>
<td>Spoke 10</td>
<td>14.69</td>
</tr>
<tr class="even">
<td>LB-S05:RF-CAV-01</td>
<td>Spoke 11</td>
<td>15.04</td>
</tr>
<tr class="odd">
<td>LB-S06:RF-CAV-02</td>
<td>Spoke 12</td>
<td>15.21</td>
</tr>
<tr class="even">
<td>LB-S06:RF-CAV-01</td>
<td>Spoke 13</td>
<td>15.70</td>
</tr>
<tr class="odd">
<td>LB-S07:RF-CAV-02</td>
<td>Spoke 14</td>
<td>15.90</td>
</tr>
<tr class="even">
<td>LB-S07:RF-CAV-01</td>
<td>Spoke 15</td>
<td>16.61</td>
</tr>
<tr class="odd">
<td>LB-S08:RF-CAV-02</td>
<td>Spoke 16</td>
<td>16.85</td>
</tr>
<tr class="even">
<td>LB-S08:RF-CAV-01</td>
<td>Spoke 17</td>
<td>17.92</td>
</tr>
<tr class="odd">
<td>LB-S09:RF-CAV-02</td>
<td>Spoke 18</td>
<td>18.21</td>
</tr>
<tr class="even">
<td>LB-S09:RF-CAV-01</td>
<td>Spoke 19</td>
<td>19.90</td>
</tr>
<tr class="odd">
<td>LB-S10:RF-CAV-02</td>
<td>Spoke 20</td>
<td>20.27</td>
</tr>
<tr class="even">
<td>LB-S10:RF-CAV-01</td>
<td>Spoke 21</td>
<td>21.99</td>
</tr>
<tr class="odd">
<td>LB-S11:RF-CAV-02</td>
<td>Spoke 22</td>
<td>22.38</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Transmission line ohmic losses per meter for the amplifiers
(Spoke rebunchers and spokes 1--22).

 

**Table:** Transmission line ohmic losses per meter for the amplifiers
(Spokes 23--46).

<table>
<caption>Table: Transmission line ohmic losses per meter for the amplifiers (Spokes 23–46).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Naming</td>
<td>Acronym</td>
<td>Tx line losses per meter</td>
</tr>
<tr class="even">
<td>LB-S11:RF-CAV-01</td>
<td>Spoke 23</td>
<td>22.84</td>
</tr>
<tr class="odd">
<td>LB-S12:RF-CAV-02</td>
<td>Spoke 24</td>
<td>23.14</td>
</tr>
<tr class="even">
<td>LB-S12:RF-CAV-01</td>
<td>Spoke 25</td>
<td>23.45</td>
</tr>
<tr class="odd">
<td>LB-S13:RF-CAV-02</td>
<td>Spoke 26</td>
<td>23.67</td>
</tr>
<tr class="even">
<td>LB-S13:RF-CAV-01</td>
<td>Spoke 27</td>
<td>23.90</td>
</tr>
<tr class="odd">
<td>LB-S14:RF-CAV-02</td>
<td>Spoke 28</td>
<td>24.05</td>
</tr>
<tr class="even">
<td>LB-S14:RF-CAV-01</td>
<td>Spoke 29</td>
<td>24.21</td>
</tr>
<tr class="odd">
<td>LB-S15:RF-CAV-02</td>
<td>Spoke 30</td>
<td>24.31</td>
</tr>
<tr class="even">
<td>LB-S15:RF-CAV-01</td>
<td>Spoke 31</td>
<td>24.42</td>
</tr>
<tr class="odd">
<td>LB-S16:RF-CAV-02</td>
<td>Spoke 32</td>
<td>24.47</td>
</tr>
<tr class="even">
<td>LB-S16:RF-CAV-01</td>
<td>Spoke 33</td>
<td>24.54</td>
</tr>
<tr class="odd">
<td>LB-S17:RF-CAV-02</td>
<td>Spoke 34</td>
<td>24.56</td>
</tr>
<tr class="even">
<td>LB-S17:RF-CAV-01</td>
<td>Spoke 35</td>
<td>24.60</td>
</tr>
<tr class="odd">
<td>LB-S18:RF-CAV-02</td>
<td>Spoke 36</td>
<td>24.61</td>
</tr>
<tr class="even">
<td>LB-S18:RF-CAV-01</td>
<td>Spoke 37</td>
<td>24.62</td>
</tr>
<tr class="odd">
<td>LB-S19:RF-CAV-02</td>
<td>Spoke 38</td>
<td>24.61</td>
</tr>
<tr class="even">
<td>LB-S19:RF-CAV-01</td>
<td>Spoke 39</td>
<td>24.61</td>
</tr>
<tr class="odd">
<td>LB-S20:RF-CAV-02</td>
<td>Spoke 40</td>
<td>24.59</td>
</tr>
<tr class="even">
<td>LB-S20:RF-CAV-01</td>
<td>Spoke 41</td>
<td>24.58</td>
</tr>
<tr class="odd">
<td>LB-S21:RF-CAV-02</td>
<td>Spoke 42</td>
<td>24.54</td>
</tr>
<tr class="even">
<td>LB-S21:RF-CAV-01</td>
<td>Spoke 43</td>
<td>24.52</td>
</tr>
<tr class="odd">
<td>LB-S22:RF-CAV-02</td>
<td>Spoke 44</td>
<td>24.48</td>
</tr>
<tr class="even">
<td>LB-S22:RF-CAV-01</td>
<td>Spoke 45</td>
<td>24.46</td>
</tr>
<tr class="odd">
<td>LB-S23:RF-CAV-02</td>
<td>Spoke 46</td>
<td>24.41</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Transmission line ohmic losses per meter for the amplifiers
(Spokes 23--46).

 

**Table:** General requirements for the isolators.

<table>
<caption>Table: General requirements for the isolators.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Component</td>
<td>Interface (EIA)</td>
<td>Quantity</td>
<td>Power range (kW)</td>
<td>Required power (kW)</td>
</tr>
<tr class="even">
<td>Circulator</td>
<td>1 ⅝″</td>
<td>20</td>
<td>7.3 – 10.8</td>
<td>15</td>
</tr>
<tr class="odd">
<td> —-</td>
<td>3 ⅛″</td>
<td>43</td>
<td>11.1 – 21.6</td>
<td>25</td>
</tr>
<tr class="even">
<td>Load</td>
<td>1 ⅝″</td>
<td>20</td>
<td>7.3 – 10.8</td>
<td>15</td>
</tr>
<tr class="odd">
<td> —-</td>
<td>3 ⅛″</td>
<td>43</td>
<td>11.1 – 21.6</td>
<td>25</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: General requirements for the isolators.

 

**Table:** Specific requirements for the circulators.

<table>
<caption>Table: Specific requirements for the circulators.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Parameter</td>
<td>Requirement</td>
</tr>
<tr class="even">
<td>Centre frequency</td>
<td>352.2MHz</td>
</tr>
<tr class="odd">
<td>Bandwidth</td>
<td>±1MHz</td>
</tr>
<tr class="even">
<td>CW forward RF power</td>
<td>As per “Required power including margin” given in Table <a href="#tabrfsysinj-amp-reqs">[*]</a>, Sect. <a href="#ssect:RFsys:specific:injector">[*]</a>.</td>
</tr>
<tr class="odd">
<td>CW reverse RF power</td>
<td>full reflection, any phase</td>
</tr>
<tr class="even">
<td>Pulse repetition rate</td>
<td>1–250Hz</td>
</tr>
<tr class="odd">
<td>Pulse Length</td>
<td>100μs to CW</td>
</tr>
<tr class="even">
<td>Insertion Loss</td>
<td>&lt; 0.2dB</td>
</tr>
<tr class="odd">
<td>Isolation at f0</td>
<td>&gt; 26dB</td>
</tr>
<tr class="even">
<td>Isolation in BW</td>
<td>&gt; 20dB</td>
</tr>
<tr class="odd">
<td>Return loss at f0</td>
<td>&gt; 26dB</td>
</tr>
<tr class="even">
<td>Return loss in BW</td>
<td>&gt; 20dB</td>
</tr>
<tr class="odd">
<td>Water Temperature Optimum</td>
<td>20℃</td>
</tr>
<tr class="even">
<td>Water temperature range</td>
<td>18–22℃</td>
</tr>
<tr class="odd">
<td>Arc detection</td>
<td>Integrated</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Specific requirements for the circulators.

 

**Table:** Stability R&D objectives.

<table>
<caption>Table: Stability R&amp;D objectives.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Parameter</td>
<td>LinacLLRF-system</td>
<td>Master oscillator</td>
<td>PRDS</td>
</tr>
<tr class="even">
<td>Amplitude (RMS)</td>
<td>±0.2%</td>
<td>–</td>
<td>–</td>
</tr>
<tr class="odd">
<td>Phase (RMS)</td>
<td>±0.1%</td>
<td>±0.1%</td>
<td>±0.1%</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Stability R&D objectives.

 

**Table:** Jitter requirements.

<table>
<caption>Table: Jitter requirements.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Frequency (MHz)</td>
<td>Jitter RMS (Deg)</td>
<td>Jitter RMS (ps)</td>
</tr>
<tr class="even">
<td>176.1</td>
<td>0.1</td>
<td>1.58</td>
</tr>
<tr class="odd">
<td>352.2</td>
<td>0.1</td>
<td>0.79</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Jitter requirements.

 

**Table:** Major RF amplifier parameters requirements.

<table>
<caption>Table: Major RF amplifier parameters requirements.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th>Main RF requirements</th>
<th>Unit</th>
<th>Expected</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Center frequency</td>
<td>MHz</td>
<td>176.1 or 352.2</td>
</tr>
<tr class="even">
<td>Bandwidth (@ -1dB)</td>
<td>MHz</td>
<td>&gt; +/- 1</td>
</tr>
<tr class="odd">
<td>Nominal input level</td>
<td>dBm</td>
<td>&lt; 10</td>
</tr>
<tr class="even">
<td>Output connector type</td>
<td>EIA </td>
<td>EIA coaxial flange</td>
</tr>
<tr class="odd">
<td>Rated output power including losses and margins</td>
<td>kW</td>
<td>See Tables <a href="#tabrfsysinj-amp-reqs">[*]</a>–<a href="#tabrfsyslinac-amp-reqs-b">[*]</a></td>
</tr>
<tr class="even">
<td>Gain linearity over 20dB dynamic range</td>
<td>dB</td>
<td>&lt; 10</td>
</tr>
<tr class="odd">
<td>Phase linearity over 20dB dynamic range</td>
<td>deg</td>
<td>&lt; 25</td>
</tr>
<tr class="even">
<td>Gain linearity @ ±1.22dB around nominal output level</td>
<td>dB</td>
<td>&lt; 1</td>
</tr>
<tr class="odd">
<td>Phase linearity @ ±1.22dB around nominal output level</td>
<td>deg</td>
<td>&lt; 18</td>
</tr>
<tr class="even">
<td>Pulse duration</td>
<td>μs</td>
<td>100 to CW</td>
</tr>
<tr class="odd">
<td>Pulse repetition rate</td>
<td>Hz</td>
<td>1 to 250</td>
</tr>
<tr class="even">
<td>Pulse rise time (10-90%)</td>
<td>μs</td>
<td>&lt; 2</td>
</tr>
<tr class="odd">
<td>Pulse amplitude overshoot/undershoot</td>
<td>%</td>
<td>&lt; 5</td>
</tr>
<tr class="even">
<td>Input VSWR</td>
<td> </td>
<td>&lt; 1.2</td>
</tr>
<tr class="odd">
<td>Time with full reflection @ required output level – all phases</td>
<td>μs</td>
<td>50</td>
</tr>
<tr class="even">
<td>Resilience to fast load variations (from matched to full reflection)</td>
<td>μs</td>
<td>50</td>
</tr>
<tr class="odd">
<td>@ required output level – all phases</td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>Time with VSWR up to 2 @ required output level – all phases</td>
<td>s</td>
<td>&gt; 3600</td>
</tr>
<tr class="odd">
<td>Harmonics @ required output level</td>
<td>dBc</td>
<td>&lt; -30</td>
</tr>
<tr class="even">
<td>Spurious @ required output level</td>
<td>dBc</td>
<td>&lt; -60</td>
</tr>
<tr class="odd">
<td>Overall efficiency @ nominal output level</td>
<td>%</td>
<td>&gt; 50</td>
</tr>
<tr class="even">
<td>EMC compliance</td>
<td> </td>
<td>CISPR11, IEC61000-6-2</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Major RF amplifier parameters requirements.

 

**Table:** Major amplifier control requirements.

<table>
<caption>Table: Major amplifier control requirements.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>Main control requirements</th>
<th>Expected</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td colspan="2">Permanent and Slow transfer to</td>
</tr>
<tr class="even">
<td>Global transfer time</td>
<td>&lt; 1 sec</td>
</tr>
<tr class="odd">
<td>Transfer protocol</td>
<td>Ethercat &amp; Modbus TCP/IP</td>
</tr>
<tr class="even">
<td colspan="2">Fast acquisition buffering for post or online processing</td>
</tr>
<tr class="odd">
<td>Data of interest</td>
<td>Flags, RF powers, currents and voltages</td>
</tr>
<tr class="even">
<td>Embedded buffers storage</td>
<td>&gt; 10 sec</td>
</tr>
<tr class="odd">
<td>Quantity of embedded buffers</td>
<td>2 sets</td>
</tr>
<tr class="even">
<td>Granularity</td>
<td>&lt; 2 ms</td>
</tr>
<tr class="odd">
<td>Time stamp</td>
<td>For every buffered data</td>
</tr>
<tr class="even">
<td>Transfer protocol</td>
<td>Raw TCP</td>
</tr>
<tr class="odd">
<td colspan="2">Interlocks</td>
</tr>
<tr class="even">
<td>External interlock speed</td>
<td>&lt; 5μs</td>
</tr>
<tr class="odd">
<td>Internal interlocks</td>
<td>VSWR, overdrive, RF power, voltage and current, cooling temperature, water flow...</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Major amplifier control requirements.

 

**Table:** Major parameters requirements for the Injector amplifiers.

<table>
<caption>Table: Major parameters requirements for the Injector amplifiers.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Naming</td>
<td>Acronym</td>
<td>Nominal power</td>
<td>Required power</td>
<td>Output conn. size</td>
</tr>
<tr class="even">
<td>convention</td>
<td> </td>
<td>incl. losses (kW)</td>
<td>incl. margins (kW)</td>
<td>(EIA standard)</td>
</tr>
<tr class="odd">
<td>I1-RFQ</td>
<td>RFQ</td>
<td>111.5</td>
<td>167.3</td>
<td>6 ⅛″</td>
</tr>
<tr class="even">
<td>I1-ME1:RF-CAV-0
1</td>
<td>QWR1</td>
<td>2.3</td>
<td>3.4</td>
<td>1 ⅝″</td>
</tr>
<tr class="odd">
<td>I1-ME1:RF-CAV-0
2</td>
<td>QWR2</td>
<td>2.7</td>
<td>4.1</td>
<td>1 ⅝″</td>
</tr>
<tr class="even">
<td>I1-CH
1:RF-CAV-01</td>
<td>CH01</td>
<td>9.3</td>
<td>13.9</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>I1-CH
2:RF-CAV-01</td>
<td>CH02</td>
<td>13.8</td>
<td>20.7</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>I1-CH
3:RF-CAV-01</td>
<td>CH03</td>
<td>17.9</td>
<td>26.9</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>I1-CH
4:RF-CAV-01</td>
<td>CH04</td>
<td>20.7</td>
<td>31.0</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>I1-CH
5:RF-CAV-01</td>
<td>CH05</td>
<td>24.5</td>
<td>36.8</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>I1-CH
6:RF-CAV-01</td>
<td>CH06</td>
<td>33.8</td>
<td>50.7</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>I1-CH
7:RF-CAV-01</td>
<td>CH07</td>
<td>31.1</td>
<td>46.6</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>I1-ME3:RF-CAV-01</td>
<td>CHR1</td>
<td>8.2</td>
<td>12.3</td>
<td>1 ⅝″</td>
</tr>
<tr class="even">
<td>I1-CH
8:RF-CAV-01</td>
<td>CH08</td>
<td>41.1</td>
<td>61.7</td>
<td>4 ½″</td>
</tr>
<tr class="odd">
<td>I1-CH
9:RF-CAV-01</td>
<td>CH09</td>
<td>40.7</td>
<td>61.0</td>
<td>4 ½″</td>
</tr>
<tr class="even">
<td>I1-CH
10:RF-CAV-01</td>
<td>CH10</td>
<td>40.6</td>
<td>60.9</td>
<td>4 ½″</td>
</tr>
<tr class="odd">
<td>I1-CH
11:RF-CAV-01</td>
<td>CH11</td>
<td>40.3</td>
<td>60.5</td>
<td>4 ½″</td>
</tr>
<tr class="even">
<td>I1-CH
12:RF-CAV-01</td>
<td>CH12</td>
<td>39.8</td>
<td>59.7</td>
<td>4 ½″</td>
</tr>
<tr class="odd">
<td>I1-CH
13:RF-CAV-01</td>
<td>CH13</td>
<td>39.4</td>
<td>59.0</td>
<td>4 ½″</td>
</tr>
<tr class="even">
<td>I1-CH
14:RF-CAV-01</td>
<td>CH14</td>
<td>38.9</td>
<td>58.4</td>
<td>4 ½″</td>
</tr>
<tr class="odd">
<td>I1-CH
15:RF-CAV-01</td>
<td>CH15</td>
<td>38.8</td>
<td>58.2</td>
<td>4 ½″</td>
</tr>
<tr class="even">
<td>I1-ME2:RF-CAV-01</td>
<td>CHR2</td>
<td>TBD</td>
<td>TBD</td>
<td>TBD</td>
</tr>
</tbody>
</table>
<p><span id="tabrfsysinj-amp-reqs" class="anchor"></span> </p></td>
</tr>
</tbody>
</table>

Table: Major parameters requirements for the Injector amplifiers.

 

**Table:** Major parameters requirements for the amplifiers (Spokes
1--30).

<table>
<caption>Table: Major parameters requirements for the amplifiers (Spokes 1–30).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Naming</td>
<td>Acronym</td>
<td>Nominal power</td>
<td>Required power</td>
<td>Output conn. size</td>
</tr>
<tr class="even">
<td>Convention</td>
<td> </td>
<td>incl. losses (kW)</td>
<td>incl. margins (kW)</td>
<td>(EIA standard)</td>
</tr>
<tr class="odd">
<td>LB-ME3:RF-CAV-01</td>
<td>Spoke Reb. 1</td>
<td>3.847</td>
<td>5.77</td>
<td>1 ⅝″</td>
</tr>
<tr class="even">
<td>LB-ME3:RF-CAV-02</td>
<td>Spoke Reb. 2</td>
<td>3.85</td>
<td>5.77</td>
<td>1 ⅝″</td>
</tr>
<tr class="odd">
<td>LB-S00:RF-CAV-01</td>
<td>Spoke 1</td>
<td>8.44</td>
<td>12.66</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S01:RF-CAV-02</td>
<td>Spoke 2</td>
<td>8.50</td>
<td>12.75</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S01:RF-CAV-01</td>
<td>Spoke 3</td>
<td>8.57</td>
<td>12.85</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S02:RF-CAV-02</td>
<td>Spoke 4</td>
<td>8.63</td>
<td>12.95</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S02:RF-CAV-01</td>
<td>Spoke 5</td>
<td>8.72</td>
<td>13.08</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S03:RF-CAV-02</td>
<td>Spoke 6</td>
<td>8.80</td>
<td>13.19</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S03:RF-CAV-01</td>
<td>Spoke 7</td>
<td>8.92</td>
<td>13.37</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S04:RF-CAV-02</td>
<td>Spoke 8</td>
<td>9.00</td>
<td>13.50</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S04:RF-CAV-01</td>
<td>Spoke 9</td>
<td>9.16</td>
<td>13.74</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S05:RF-CAV-02</td>
<td>Spoke 10</td>
<td>9.25</td>
<td>13.88</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S05:RF-CAV-01</td>
<td>Spoke 11</td>
<td>9.47</td>
<td>14.21</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S06:RF-CAV-02</td>
<td>Spoke 12</td>
<td>9.58</td>
<td>14.37</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S06:RF-CAV-01</td>
<td>Spoke 13</td>
<td>9.89</td>
<td>14.84</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S07:RF-CAV-02</td>
<td>Spoke 14</td>
<td>10.02</td>
<td>15.03</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S07:RF-CAV-01</td>
<td>Spoke 15</td>
<td>10.46</td>
<td>15.70</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S08:RF-CAV-02</td>
<td>Spoke 16</td>
<td>10.62</td>
<td>15.92</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S08:RF-CAV-01</td>
<td>Spoke 17</td>
<td>11.29</td>
<td>16.93</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S09:RF-CAV-02</td>
<td>Spoke 18</td>
<td>11.47</td>
<td>17.21</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S09:RF-CAV-01</td>
<td>Spoke 19</td>
<td>12.54</td>
<td>18.81</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S10:RF-CAV-02</td>
<td>Spoke 20</td>
<td>12.77</td>
<td>19.16</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S10:RF-CAV-01</td>
<td>Spoke 21</td>
<td>13.85</td>
<td>20.78</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S11:RF-CAV-02</td>
<td>Spoke 22</td>
<td>14.10</td>
<td>21.15</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S11:RF-CAV-01</td>
<td>Spoke 23</td>
<td>14.39</td>
<td>21.58</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S12:RF-CAV-02</td>
<td>Spoke 24</td>
<td>14.58</td>
<td>21.87</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S12:RF-CAV-01</td>
<td>Spoke 25</td>
<td>14.77</td>
<td>22.16</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S13:RF-CAV-02</td>
<td>Spoke 26</td>
<td>14.91</td>
<td>22.36</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S13:RF-CAV-01</td>
<td>Spoke 27</td>
<td>15.05</td>
<td>22.58</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S14:RF-CAV-02</td>
<td>Spoke 28</td>
<td>15.15</td>
<td>22.72</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S14:RF-CAV-01</td>
<td>Spoke 29</td>
<td>15.25</td>
<td>22.88</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S15:RF-CAV-02</td>
<td>Spoke 30</td>
<td>15.31</td>
<td>22.97</td>
<td>3 ⅛″</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Major parameters requirements for the amplifiers (Spokes 1--30).

 

**Table:** Major parameters requirements for the amplifiers (Spokes
31--46).

<table>
<caption>Table: Major parameters requirements for the amplifiers (Spokes 31–46).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Naming</td>
<td>Acronym</td>
<td>Nominal power</td>
<td>Required power</td>
<td>Output conn.</td>
</tr>
<tr class="even">
<td>Convention</td>
<td> </td>
<td>incl. losses (kW)</td>
<td>incl. margins (kW)</td>
<td>size (EIA standard)</td>
</tr>
<tr class="odd">
<td>LB-S15:RF-CAV-01</td>
<td>Spoke 31</td>
<td>15.38</td>
<td>23.07</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S16:RF-CAV-02</td>
<td>Spoke 32</td>
<td>15.41</td>
<td>23.12</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S16:RF-CAV-01</td>
<td>Spoke 33</td>
<td>15.46</td>
<td>23.19</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S17:RF-CAV-02</td>
<td>Spoke 34</td>
<td>15.47</td>
<td>23.21</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S17:RF-CAV-01</td>
<td>Spoke 35</td>
<td>15.50</td>
<td>23.25</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S18:RF-CAV-02</td>
<td>Spoke 36</td>
<td>15.50</td>
<td>23.25</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S18:RF-CAV-01</td>
<td>Spoke 37</td>
<td>15.51</td>
<td>23.27</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S19:RF-CAV-02</td>
<td>Spoke 38</td>
<td>15.50</td>
<td>23.25</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S19:RF-CAV-01</td>
<td>Spoke 39</td>
<td>15.50</td>
<td>23.25</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S20:RF-CAV-02</td>
<td>Spoke 40</td>
<td>15.49</td>
<td>23.23</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S20:RF-CAV-01</td>
<td>Spoke 41</td>
<td>15.48</td>
<td>23.22</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S21:RF-CAV-02</td>
<td>Spoke 42</td>
<td>15.46</td>
<td>23.19</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S21:RF-CAV-01</td>
<td>Spoke 43</td>
<td>15.45</td>
<td>23.17</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S22:RF-CAV-02</td>
<td>Spoke 44</td>
<td>15.42</td>
<td>23.13</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S22:RF-CAV-01</td>
<td>Spoke 45</td>
<td>15.41</td>
<td>23.11</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S23:RF-CAV-02</td>
<td>Spoke 46</td>
<td>15.38</td>
<td>23.07</td>
<td>3 ⅛″</td>
</tr>
</tbody>
</table>
<p><span id="tabrfsyslinac-amp-reqs-b" class="anchor"></span> </p></td>
</tr>
</tbody>
</table>

Table: Major parameters requirements for the amplifiers (Spokes 31--46).

 

**Table:** Results overview of the RF characterisation of the RFQ
amplifier.

<table>
<caption>Table: Results overview of the RF characterisation of the RFQ amplifier.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>PARAMETERS</td>
<td>UNITS</td>
<td> 
RESULTS</td>
</tr>
<tr class="even">
<td>Max output power on matched load</td>
<td>kW</td>
<td>192</td>
</tr>
<tr class="odd">
<td>Input return loss over the full power range</td>
<td>dB</td>
<td>-20</td>
</tr>
<tr class="even">
<td>Wall plug efficiency</td>
<td>%</td>
<td>73.6 (max level) / 61.5 (operating level)</td>
</tr>
<tr class="odd">
<td>AC/DC converters efficiency</td>
<td>%</td>
<td>90 – 92</td>
</tr>
<tr class="even">
<td>Harmonics level over the full power range</td>
<td>dBc</td>
<td>&lt; -45</td>
</tr>
<tr class="odd">
<td>Gain variation within ±1MHz BW</td>
<td>dB</td>
<td>1.4 (100kW), 0.95 (140kW)</td>
</tr>
<tr class="even">
<td>Bandwidth (@ -1 dB)</td>
<td>MHz</td>
<td>&gt; ±1</td>
</tr>
<tr class="odd">
<td>Maximum gain</td>
<td>dB</td>
<td>85</td>
</tr>
<tr class="even">
<td>Large dynamic range (up to required level)</td>
<td>dB</td>
<td>&gt; 20</td>
</tr>
<tr class="odd">
<td>Gain linearity over large dynamic range</td>
<td>dB</td>
<td>8</td>
</tr>
<tr class="even">
<td>Phase linearity over large dynamic range</td>
<td>deg</td>
<td>20.5</td>
</tr>
<tr class="odd">
<td>Reduced dynamic range (around operating level)</td>
<td>dB</td>
<td>±1.22</td>
</tr>
<tr class="even">
<td>Gain linearity over reduced dynamic range</td>
<td>dB</td>
<td>0.46</td>
</tr>
<tr class="odd">
<td>Phase linearity over reduced dynamic range</td>
<td>deg</td>
<td>14.5</td>
</tr>
<tr class="even">
<td>1 dB compression point</td>
<td>kW</td>
<td>70</td>
</tr>
<tr class="odd">
<td>Warm up time at 120kW</td>
<td>sec</td>
<td>60</td>
</tr>
<tr class="even">
<td>Gain variations during warm up</td>
<td>dB</td>
<td>0.15</td>
</tr>
<tr class="odd">
<td>Phase variations during warm up</td>
<td>deg</td>
<td>0.57</td>
</tr>
<tr class="even">
<td>Gain stability over 1 s after warm up</td>
<td>dB</td>
<td>&lt; 0.02</td>
</tr>
<tr class="odd">
<td>Phase stability over 1 s after warm up</td>
<td>deg</td>
<td>&lt; 0.15</td>
</tr>
<tr class="even">
<td>Pulse rise time</td>
<td>µs</td>
<td>1.6</td>
</tr>
<tr class="odd">
<td>Pulse amplitude overshoot/undershoot</td>
<td>%</td>
<td>&lt; 5</td>
</tr>
<tr class="even">
<td>Group delay</td>
<td>ns</td>
<td>87</td>
</tr>
<tr class="odd">
<td>External combiner return loss</td>
<td>dB</td>
<td>&lt; -25</td>
</tr>
<tr class="even">
<td>External combiner directivity</td>
<td>dB</td>
<td>&gt; 25</td>
</tr>
<tr class="odd">
<td>Max CW forward power under VSWR = 2</td>
<td>kW</td>
<td>143</td>
</tr>
<tr class="even">
<td>Max CW forward power under VSWR = 3</td>
<td>kW</td>
<td>124</td>
</tr>
<tr class="odd">
<td>Max pulsed forward power under full reflection</td>
<td>kW</td>
<td>92 (10 ms, 40 %)</td>
</tr>
<tr class="even">
<td>Delivery of required forward power up to VSWR</td>
<td> </td>
<td>2</td>
</tr>
<tr class="odd">
<td>RF leak level at 120kW</td>
<td>dBμV / m</td>
<td>&lt; 104.5</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Results overview of the RF characterisation of the RFQ amplifier.

 

**Table:** SSAs repartition per cabinet for the Injector prototyping
activity.

<table>
<caption>Table: SSAs repartition per cabinet for the Injector prototyping activity.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Acronym</td>
<td>QWR 1</td>
<td>QWR 2</td>
<td>CH01</td>
<td>CH02</td>
<td>CH03</td>
<td>CH04</td>
<td>CH05</td>
<td>CH06</td>
<td>CH07</td>
</tr>
<tr class="even">
<td>Naming convention</td>
<td>I1-ME1:RF-CAV-0
1</td>
<td>I1-ME1:RF-CAV-0
2</td>
<td>I1-CH
1:RF-CAV-01</td>
<td>I1-CH
2:RF-CAV-01</td>
<td>I1-CH
3:RF-CAV-01</td>
<td>I1-CH
4:RF-CAV-01</td>
<td>I1-CH
5:RF-CAV-01</td>
<td>I1-CH
6:RF-CAV-01</td>
<td>I1-CH
7:RF-CAV-01</td>
</tr>
<tr class="odd">
<td>Cabinet 1</td>
<td>6kW</td>
<td>6kW</td>
<td>24kW</td>
<td>24kW</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
</tr>
<tr class="even">
<td>Cabinet 2</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>36kW</td>
<td>36kW</td>
<td>–</td>
<td>–</td>
<td>–</td>
</tr>
<tr class="odd">
<td>Cabinet 3</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>48kW</td>
<td>–</td>
<td>48kW</td>
</tr>
<tr class="even">
<td>Cabinet 4</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>60kW</td>
<td>–</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: SSAs repartition per cabinet for the Injector prototyping
activity.

 

**Table:** Estimation of the SSA cabinet quantity for the MINERVA linac.

<table>
<caption>Table: Estimation of the SSA cabinet quantity for the MINERVA linac.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Acronym</td>
<td>RFQ</td>
<td>QWR1 ⇨ CH07</td>
<td>CHR1 ⇨ CH15</td>
<td>Spoke 1 ⇨ Spoke 46</td>
</tr>
<tr class="even">
<td>Naming</td>
<td>I1-RFQ</td>
<td>I1-ME1:RF-CAV-0
1 ⇨</td>
<td>I1-ME3:RF-CAV-01 ⇨</td>
<td>LB-S00:RF-CAV-01 ⇨</td>
</tr>
<tr class="odd">
<td>convention</td>
<td> </td>
<td>I1-CH
7:RF-CAV-01</td>
<td>I1-CH
15:RF-CAV-01</td>
<td>LB-S23:RF-CAV-02</td>
</tr>
<tr class="even">
<td>No. of cabinet</td>
<td>2</td>
<td>4</td>
<td>8</td>
<td>15</td>
</tr>
<tr class="odd">
<td>No. of output</td>
<td>0.5</td>
<td>4 (1<sup>st</sup> cabinet) 1 (4<sup>th</sup> cabinet)</td>
<td>2 (1<sup>st</sup> cabinet)</td>
<td>4</td>
</tr>
<tr class="even">
<td>per cabinet</td>
<td> </td>
<td>2 (others)</td>
<td>1 (others)</td>
<td> </td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Estimation of the SSA cabinet quantity for the MINERVA linac.

 

**Table:** Estimation of the water cooling requirements for the MINERVA
SSAs.

<table>
<caption>Table: Estimation of the water cooling requirements for the MINERVA SSAs.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td> </td>
<td>SSAs for Injector</td>
<td>SSAs for</td>
</tr>
<tr class="even">
<td>Medium</td>
<td> </td>
<td colspan="2">Normal water</td>
</tr>
<tr class="odd">
<td>Particle filtering size</td>
<td>μm</td>
<td colspan="2">&lt; 500</td>
</tr>
<tr class="even">
<td>Minimum flow rate</td>
<td>L min⁻¹</td>
<td>2105</td>
<td>1400</td>
</tr>
<tr class="odd">
<td>Input absolute pressure</td>
<td>bar</td>
<td colspan="2">&lt; 6</td>
</tr>
<tr class="even">
<td>Output absolute pressure</td>
<td>bar</td>
<td colspan="2">&gt; 1</td>
</tr>
<tr class="odd">
<td>Pressure drop</td>
<td>bar</td>
<td colspan="2">&lt; 4</td>
</tr>
<tr class="even">
<td>Input temperature</td>
<td>℃</td>
<td colspan="2">&lt; 25</td>
</tr>
<tr class="odd">
<td>Input temp. stability</td>
<td>℃</td>
<td colspan="2">&lt; ± 0.5</td>
</tr>
<tr class="even">
<td>Output temperature</td>
<td>℃</td>
<td colspan="2">&lt; 35</td>
</tr>
<tr class="odd">
<td>Delta temperature</td>
<td>℃</td>
<td colspan="2">&lt; 10</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Estimation of the water cooling requirements for the MINERVA
SSAs.

 

**Table:** Characteristics of some rectangular waveguides.

<table>
<caption>Table: Characteristics of some rectangular waveguides.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Waveguide EIA name</td>
<td>Recommended frequency band of operation (GHz)</td>
<td>Inner dimensions of waveguide opening (mm)</td>
<td>TE<sub>10</sub>-mode cut-off frequency (GHz)</td>
<td>TE<sub>20</sub>-mode cut-off frequency (GHz)</td>
</tr>
<tr class="even">
<td>WR2300</td>
<td>0.32 — 0.45</td>
<td>584.2 × 292.1</td>
<td>0.26</td>
<td>0.51</td>
</tr>
<tr class="odd">
<td>WR1150</td>
<td>0.63 — 0.97</td>
<td>292.1 × 146.05</td>
<td>0.51</td>
<td>1.03</td>
</tr>
<tr class="even">
<td>WR340</td>
<td>2.2 — 3.3</td>
<td>86.36 × 43.18</td>
<td>1.7</td>
<td>3.5</td>
</tr>
<tr class="odd">
<td>WR75</td>
<td>10 — 15</td>
<td>19.05 × 9.52</td>
<td>7.9</td>
<td>15.7</td>
</tr>
<tr class="even">
<td>WR10</td>
<td>70 — 110</td>
<td>2.54 × 1.27</td>
<td>59</td>
<td>118</td>
</tr>
<tr class="odd">
<td>WR3</td>
<td>220 — 330</td>
<td>0.864 × 0.432</td>
<td>173</td>
<td>346</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Characteristics of some rectangular waveguides.

 

**Table:** Dimensions and cut-off frequencies of most used rigid coaxial
waveguides.

<table>
<caption>Table: Dimensions and cut-off frequencies of most used rigid coaxial waveguides.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Coaxial EIA name</td>
<td colspan="2">Outer conductor</td>
<td colspan="2">Inner conductor</td>
<td>Cut-off frequency TE11-mode (GHz)</td>
</tr>
<tr class="even">
<td> </td>
<td>Outer diameter (mm)</td>
<td>Inner diameter (mm) = D₁</td>
<td>Outer diameter (mm) = D₂</td>
<td>Inner diameter (mm)</td>
<td> </td>
</tr>
<tr class="odd">
<td>⅞″</td>
<td>22.2</td>
<td>20</td>
<td>8.7</td>
<td>7.4</td>
<td>6.3</td>
</tr>
<tr class="even">
<td>1 ⅝″</td>
<td>41.3</td>
<td>38.8</td>
<td>16.9</td>
<td>15</td>
<td>3.2</td>
</tr>
<tr class="odd">
<td>3 ⅛″</td>
<td>79.4</td>
<td>76.9</td>
<td>33.4</td>
<td>31.3</td>
<td>1.6</td>
</tr>
<tr class="even">
<td>4 ½″</td>
<td>106</td>
<td>103</td>
<td>44.8</td>
<td>42.8</td>
<td>1.2</td>
</tr>
<tr class="odd">
<td>6 ⅛″</td>
<td>155.6</td>
<td>151.9</td>
<td>66</td>
<td>64</td>
<td>0.83</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Dimensions and cut-off frequencies of most used rigid coaxial
waveguides.

 

**Table:** Maximum power handling of coaxial waveguides.

<table>
<caption>Table: Maximum power handling of coaxial waveguides.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td colspan="5">CW power handling capability (kW)</td>
</tr>
<tr class="even">
<td>f (MHz)</td>
<td>⅞″</td>
<td>1 ⅝″</td>
<td>3 ⅛″</td>
<td>4 ½″</td>
<td>6 ⅛″</td>
</tr>
<tr class="odd">
<td>176.1</td>
<td>5.7</td>
<td>15</td>
<td>48</td>
<td>80</td>
<td>159</td>
</tr>
<tr class="even">
<td>352.2</td>
<td>4.1</td>
<td>11</td>
<td>34</td>
<td>57</td>
<td>111</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Maximum power handling of coaxial waveguides.

 

**Table:** Typical attenuations of coaxial waveguides.

<table>
<caption>Table: Typical attenuations of coaxial waveguides.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td colspan="5">Attenuation (dB/100m)</td>
</tr>
<tr class="even">
<td>f (MHz)</td>
<td>⅞″</td>
<td>1 ⅝″</td>
<td>3 ⅛″</td>
<td>4 ½″</td>
<td>6 ⅛″</td>
</tr>
<tr class="odd">
<td>176.1</td>
<td>1.61</td>
<td>0.99</td>
<td>0.49</td>
<td>0.37</td>
<td>0.25</td>
</tr>
<tr class="even">
<td>352.2</td>
<td>2.27</td>
<td>1.40</td>
<td>0.69</td>
<td>0.52</td>
<td>0.35</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Typical attenuations of coaxial waveguides.

 

**Table:** Configuration of the MINERVA Tx lines.

<table>
<caption>Table: Configuration of the MINERVA Tx lines.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Cavity name</td>
<td>Naming convention</td>
<td>Size of Tx line</td>
<td>Interface with SSA</td>
<td>Interface with power coupler</td>
</tr>
<tr class="even">
<td>RFQ</td>
<td>I1-RFQ</td>
<td>6 ⅛″</td>
<td>6 ⅛″ EIA</td>
<td>6 ⅛″ EIA</td>
</tr>
<tr class="odd">
<td>QWR1 &amp; 2</td>
<td>I1-ME1:RF-CAV-0
1 &amp; I1-ME1:RF-CAV-0
2</td>
<td>⅞″ (flexible)</td>
<td>1 ⅝″ EIA</td>
<td>1 ⅝″ EIA</td>
</tr>
<tr class="even">
<td>CH01 to 07</td>
<td>I1-CH
1:RF-CAV-01 to I1-CH
7:RF-CAV-01</td>
<td>3 ⅛″</td>
<td>4 ½″ EIA</td>
<td>6 ⅛″ EIA</td>
</tr>
<tr class="odd">
<td>CHR1</td>
<td>I1-ME3:RF-CAV-01</td>
<td>1 ⅝″</td>
<td>1 ⅝″ EIA</td>
<td>1 ⅝″ EIA</td>
</tr>
<tr class="even">
<td>CH08 to 15</td>
<td>I1-CH
8:RF-CAV-01 to I1-CH
15:RF-CAV-01</td>
<td>4 ½″</td>
<td>4 ½″ EIA</td>
<td>6 ⅛″ EIA</td>
</tr>
<tr class="odd">
<td>CHR2</td>
<td>I1-ME2:RF-CAV-01</td>
<td>TBD</td>
<td>TBD</td>
<td>6 ⅛″ EIA</td>
</tr>
<tr class="even">
<td>Spoke Reb. 1 &amp; 2</td>
<td>I1-ME3:RF-CAV-01 &amp; I1-ME2:RF-CAV-01</td>
<td>1 ⅝″ EIA</td>
<td>1 ⅝″ EIA</td>
<td>3 ⅛″ EIA</td>
</tr>
<tr class="odd">
<td>Spoke 1 to Spoke 46</td>
<td>LB-S00:RF-CAV-01 to LB-S23:RF-CAV-02</td>
<td>3 ⅛″ EIA</td>
<td>3 ⅛″ EIA</td>
<td>3 ⅛″ EIA</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Configuration of the MINERVA Tx lines.

 

**Table:** Transmission line ohmic losses per meter for the Injector
amplifiers.

<table>
<caption>Table: Transmission line ohmic losses per meter for the Injector amplifiers.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Naming</td>
<td>Acronym</td>
<td>Tx line losses per meter</td>
</tr>
<tr class="even">
<td>I1-RFQ</td>
<td>RFQ</td>
<td>71.89</td>
</tr>
<tr class="odd">
<td>I1-ME1:RF-CAV-0
1</td>
<td>QWR1</td>
<td>5.92</td>
</tr>
<tr class="even">
<td>I1-ME1:RF-CAV-0
2</td>
<td>QWR2</td>
<td>7.10</td>
</tr>
<tr class="odd">
<td>I1-CH
1:RF-CAV-01</td>
<td>CH01</td>
<td>11.96</td>
</tr>
<tr class="even">
<td>I1-CH
2:RF-CAV-01</td>
<td>CH02</td>
<td>17.75</td>
</tr>
<tr class="odd">
<td>I1-CH
3:RF-CAV-01</td>
<td>CH03</td>
<td>23.07</td>
</tr>
<tr class="even">
<td>I1-CH
4:RF-CAV-01</td>
<td>CH04</td>
<td>26.64</td>
</tr>
<tr class="odd">
<td>I1-CH
5:RF-CAV-01</td>
<td>CH05</td>
<td>31.61</td>
</tr>
<tr class="even">
<td>I1-CH
6:RF-CAV-01</td>
<td>CH06</td>
<td>43.59</td>
</tr>
<tr class="odd">
<td>I1-CH
7:RF-CAV-01</td>
<td>CH07</td>
<td>40.01</td>
</tr>
<tr class="even">
<td>I1-ME3:RF-CAV-01</td>
<td>CHR1</td>
<td>21.31</td>
</tr>
<tr class="odd">
<td>I1-CH
8:RF-CAV-01</td>
<td>CH08</td>
<td>34.08</td>
</tr>
<tr class="even">
<td>I1-CH
9:RF-CAV-01</td>
<td>CH09</td>
<td>33.72</td>
</tr>
<tr class="odd">
<td>I1-CH
10:RF-CAV-01</td>
<td>CH10</td>
<td>33.65</td>
</tr>
<tr class="even">
<td>I1-CH
11:RF-CAV-01</td>
<td>CH11</td>
<td>33.41</td>
</tr>
<tr class="odd">
<td>I1-CH
12:RF-CAV-01</td>
<td>CH12</td>
<td>32.96</td>
</tr>
<tr class="even">
<td>I1-CH
13:RF-CAV-01</td>
<td>CH13</td>
<td>32.61</td>
</tr>
<tr class="odd">
<td>I1-CH
14:RF-CAV-01</td>
<td>CH14</td>
<td>32.27</td>
</tr>
<tr class="even">
<td>I1-CH
15:RF-CAV-01</td>
<td>CH15</td>
<td>32.16</td>
</tr>
<tr class="odd">
<td>I1-ME2:RF-CAV-01</td>
<td>CHR2</td>
<td>TBD</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Transmission line ohmic losses per meter for the Injector
amplifiers.

 

**Table:** Transmission line ohmic losses per meter for the amplifiers
(Spoke rebunchers and spokes 1--22).

<table>
<caption>Table: Transmission line ohmic losses per meter for the amplifiers (Spoke rebunchers and spokes 1–22).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Naming</td>
<td>Acronym</td>
<td>Tx line losses per meter</td>
</tr>
<tr class="even">
<td>LB-ME3:RF-CAV-01</td>
<td>Spoke Reb. 1</td>
<td>12.38</td>
</tr>
<tr class="odd">
<td>LB-ME3:RF-CAV-02</td>
<td>Spoke Reb. 2</td>
<td>12.38</td>
</tr>
<tr class="even">
<td>LB-S00:RF-CAV-01</td>
<td>Spoke 1</td>
<td>13.40</td>
</tr>
<tr class="odd">
<td>LB-S01:RF-CAV-02</td>
<td>Spoke 2</td>
<td>13.49</td>
</tr>
<tr class="even">
<td>LB-S01:RF-CAV-01</td>
<td>Spoke 3</td>
<td>13.60</td>
</tr>
<tr class="odd">
<td>LB-S02:RF-CAV-02</td>
<td>Spoke 4</td>
<td>13.70</td>
</tr>
<tr class="even">
<td>LB-S02:RF-CAV-01</td>
<td>Spoke 5</td>
<td>13.85</td>
</tr>
<tr class="odd">
<td>LB-S03:RF-CAV-02</td>
<td>Spoke 6</td>
<td>13.96</td>
</tr>
<tr class="even">
<td>LB-S03:RF-CAV-01</td>
<td>Spoke 7</td>
<td>14.15</td>
</tr>
<tr class="odd">
<td>LB-S04:RF-CAV-02</td>
<td>Spoke 8</td>
<td>14.28</td>
</tr>
<tr class="even">
<td>LB-S04:RF-CAV-01</td>
<td>Spoke 9</td>
<td>14.54</td>
</tr>
<tr class="odd">
<td>LB-S05:RF-CAV-02</td>
<td>Spoke 10</td>
<td>14.69</td>
</tr>
<tr class="even">
<td>LB-S05:RF-CAV-01</td>
<td>Spoke 11</td>
<td>15.04</td>
</tr>
<tr class="odd">
<td>LB-S06:RF-CAV-02</td>
<td>Spoke 12</td>
<td>15.21</td>
</tr>
<tr class="even">
<td>LB-S06:RF-CAV-01</td>
<td>Spoke 13</td>
<td>15.70</td>
</tr>
<tr class="odd">
<td>LB-S07:RF-CAV-02</td>
<td>Spoke 14</td>
<td>15.90</td>
</tr>
<tr class="even">
<td>LB-S07:RF-CAV-01</td>
<td>Spoke 15</td>
<td>16.61</td>
</tr>
<tr class="odd">
<td>LB-S08:RF-CAV-02</td>
<td>Spoke 16</td>
<td>16.85</td>
</tr>
<tr class="even">
<td>LB-S08:RF-CAV-01</td>
<td>Spoke 17</td>
<td>17.92</td>
</tr>
<tr class="odd">
<td>LB-S09:RF-CAV-02</td>
<td>Spoke 18</td>
<td>18.21</td>
</tr>
<tr class="even">
<td>LB-S09:RF-CAV-01</td>
<td>Spoke 19</td>
<td>19.90</td>
</tr>
<tr class="odd">
<td>LB-S10:RF-CAV-02</td>
<td>Spoke 20</td>
<td>20.27</td>
</tr>
<tr class="even">
<td>LB-S10:RF-CAV-01</td>
<td>Spoke 21</td>
<td>21.99</td>
</tr>
<tr class="odd">
<td>LB-S11:RF-CAV-02</td>
<td>Spoke 22</td>
<td>22.38</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Transmission line ohmic losses per meter for the amplifiers
(Spoke rebunchers and spokes 1--22).

 

**Table:** Transmission line ohmic losses per meter for the amplifiers
(Spokes 23--46).

<table>
<caption>Table: Transmission line ohmic losses per meter for the amplifiers (Spokes 23–46).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Naming</td>
<td>Acronym</td>
<td>Tx line losses per meter</td>
</tr>
<tr class="even">
<td>LB-S11:RF-CAV-01</td>
<td>Spoke 23</td>
<td>22.84</td>
</tr>
<tr class="odd">
<td>LB-S12:RF-CAV-02</td>
<td>Spoke 24</td>
<td>23.14</td>
</tr>
<tr class="even">
<td>LB-S12:RF-CAV-01</td>
<td>Spoke 25</td>
<td>23.45</td>
</tr>
<tr class="odd">
<td>LB-S13:RF-CAV-02</td>
<td>Spoke 26</td>
<td>23.67</td>
</tr>
<tr class="even">
<td>LB-S13:RF-CAV-01</td>
<td>Spoke 27</td>
<td>23.90</td>
</tr>
<tr class="odd">
<td>LB-S14:RF-CAV-02</td>
<td>Spoke 28</td>
<td>24.05</td>
</tr>
<tr class="even">
<td>LB-S14:RF-CAV-01</td>
<td>Spoke 29</td>
<td>24.21</td>
</tr>
<tr class="odd">
<td>LB-S15:RF-CAV-02</td>
<td>Spoke 30</td>
<td>24.31</td>
</tr>
<tr class="even">
<td>LB-S15:RF-CAV-01</td>
<td>Spoke 31</td>
<td>24.42</td>
</tr>
<tr class="odd">
<td>LB-S16:RF-CAV-02</td>
<td>Spoke 32</td>
<td>24.47</td>
</tr>
<tr class="even">
<td>LB-S16:RF-CAV-01</td>
<td>Spoke 33</td>
<td>24.54</td>
</tr>
<tr class="odd">
<td>LB-S17:RF-CAV-02</td>
<td>Spoke 34</td>
<td>24.56</td>
</tr>
<tr class="even">
<td>LB-S17:RF-CAV-01</td>
<td>Spoke 35</td>
<td>24.60</td>
</tr>
<tr class="odd">
<td>LB-S18:RF-CAV-02</td>
<td>Spoke 36</td>
<td>24.61</td>
</tr>
<tr class="even">
<td>LB-S18:RF-CAV-01</td>
<td>Spoke 37</td>
<td>24.62</td>
</tr>
<tr class="odd">
<td>LB-S19:RF-CAV-02</td>
<td>Spoke 38</td>
<td>24.61</td>
</tr>
<tr class="even">
<td>LB-S19:RF-CAV-01</td>
<td>Spoke 39</td>
<td>24.61</td>
</tr>
<tr class="odd">
<td>LB-S20:RF-CAV-02</td>
<td>Spoke 40</td>
<td>24.59</td>
</tr>
<tr class="even">
<td>LB-S20:RF-CAV-01</td>
<td>Spoke 41</td>
<td>24.58</td>
</tr>
<tr class="odd">
<td>LB-S21:RF-CAV-02</td>
<td>Spoke 42</td>
<td>24.54</td>
</tr>
<tr class="even">
<td>LB-S21:RF-CAV-01</td>
<td>Spoke 43</td>
<td>24.52</td>
</tr>
<tr class="odd">
<td>LB-S22:RF-CAV-02</td>
<td>Spoke 44</td>
<td>24.48</td>
</tr>
<tr class="even">
<td>LB-S22:RF-CAV-01</td>
<td>Spoke 45</td>
<td>24.46</td>
</tr>
<tr class="odd">
<td>LB-S23:RF-CAV-02</td>
<td>Spoke 46</td>
<td>24.41</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Transmission line ohmic losses per meter for the amplifiers
(Spokes 23--46).

 

**Table:** General requirements for the isolators.

<table>
<caption>Table: General requirements for the isolators.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Component</td>
<td>Interface (EIA)</td>
<td>Quantity</td>
<td>Power range (kW)</td>
<td>Required power (kW)</td>
</tr>
<tr class="even">
<td>Circulator</td>
<td>1 ⅝″</td>
<td>20</td>
<td>7.3 – 10.8</td>
<td>15</td>
</tr>
<tr class="odd">
<td> —-</td>
<td>3 ⅛″</td>
<td>43</td>
<td>11.1 – 21.6</td>
<td>25</td>
</tr>
<tr class="even">
<td>Load</td>
<td>1 ⅝″</td>
<td>20</td>
<td>7.3 – 10.8</td>
<td>15</td>
</tr>
<tr class="odd">
<td> —-</td>
<td>3 ⅛″</td>
<td>43</td>
<td>11.1 – 21.6</td>
<td>25</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: General requirements for the isolators.

 

**Table:** Specific requirements for the circulators.

<table>
<caption>Table: Specific requirements for the circulators.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Parameter</td>
<td>Requirement</td>
</tr>
<tr class="even">
<td>Centre frequency</td>
<td>352.2MHz</td>
</tr>
<tr class="odd">
<td>Bandwidth</td>
<td>±1MHz</td>
</tr>
<tr class="even">
<td>CW forward RF power</td>
<td>As per “Required power including margin” given in Table <a href="#tabrfsysinj-amp-reqs">[*]</a>, Sect. <a href="#ssect:RFsys:specific:injector">[*]</a>.</td>
</tr>
<tr class="odd">
<td>CW reverse RF power</td>
<td>full reflection, any phase</td>
</tr>
<tr class="even">
<td>Pulse repetition rate</td>
<td>1–250Hz</td>
</tr>
<tr class="odd">
<td>Pulse Length</td>
<td>100μs to CW</td>
</tr>
<tr class="even">
<td>Insertion Loss</td>
<td>&lt; 0.2dB</td>
</tr>
<tr class="odd">
<td>Isolation at f0</td>
<td>&gt; 26dB</td>
</tr>
<tr class="even">
<td>Isolation in BW</td>
<td>&gt; 20dB</td>
</tr>
<tr class="odd">
<td>Return loss at f0</td>
<td>&gt; 26dB</td>
</tr>
<tr class="even">
<td>Return loss in BW</td>
<td>&gt; 20dB</td>
</tr>
<tr class="odd">
<td>Water Temperature Optimum</td>
<td>20℃</td>
</tr>
<tr class="even">
<td>Water temperature range</td>
<td>18–22℃</td>
</tr>
<tr class="odd">
<td>Arc detection</td>
<td>Integrated</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Specific requirements for the circulators.

 

**Table:** Stability R&D objectives.

<table>
<caption>Table: Stability R&amp;D objectives.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Parameter</td>
<td>LinacLLRF-system</td>
<td>Master oscillator</td>
<td>PRDS</td>
</tr>
<tr class="even">
<td>Amplitude (RMS)</td>
<td>±0.2%</td>
<td>–</td>
<td>–</td>
</tr>
<tr class="odd">
<td>Phase (RMS)</td>
<td>±0.1%</td>
<td>±0.1%</td>
<td>±0.1%</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Stability R&D objectives.

 

**Table:** Jitter requirements.

<table>
<caption>Table: Jitter requirements.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Frequency (MHz)</td>
<td>Jitter RMS (Deg)</td>
<td>Jitter RMS (ps)</td>
</tr>
<tr class="even">
<td>176.1</td>
<td>0.1</td>
<td>1.58</td>
</tr>
<tr class="odd">
<td>352.2</td>
<td>0.1</td>
<td>0.79</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Jitter requirements.

 

**Table:** Major RF amplifier parameters requirements.

<table>
<caption>Table: Major RF amplifier parameters requirements.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th>Main RF requirements</th>
<th>Unit</th>
<th>Expected</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Center frequency</td>
<td>MHz</td>
<td>176.1 or 352.2</td>
</tr>
<tr class="even">
<td>Bandwidth (@ -1dB)</td>
<td>MHz</td>
<td>&gt; +/- 1</td>
</tr>
<tr class="odd">
<td>Nominal input level</td>
<td>dBm</td>
<td>&lt; 10</td>
</tr>
<tr class="even">
<td>Output connector type</td>
<td>EIA </td>
<td>EIA coaxial flange</td>
</tr>
<tr class="odd">
<td>Rated output power including losses and margins</td>
<td>kW</td>
<td>See Tables <a href="#tabrfsysinj-amp-reqs">[*]</a>–<a href="#tabrfsyslinac-amp-reqs-b">[*]</a></td>
</tr>
<tr class="even">
<td>Gain linearity over 20dB dynamic range</td>
<td>dB</td>
<td>&lt; 10</td>
</tr>
<tr class="odd">
<td>Phase linearity over 20dB dynamic range</td>
<td>deg</td>
<td>&lt; 25</td>
</tr>
<tr class="even">
<td>Gain linearity @ ±1.22dB around nominal output level</td>
<td>dB</td>
<td>&lt; 1</td>
</tr>
<tr class="odd">
<td>Phase linearity @ ±1.22dB around nominal output level</td>
<td>deg</td>
<td>&lt; 18</td>
</tr>
<tr class="even">
<td>Pulse duration</td>
<td>μs</td>
<td>100 to CW</td>
</tr>
<tr class="odd">
<td>Pulse repetition rate</td>
<td>Hz</td>
<td>1 to 250</td>
</tr>
<tr class="even">
<td>Pulse rise time (10-90%)</td>
<td>μs</td>
<td>&lt; 2</td>
</tr>
<tr class="odd">
<td>Pulse amplitude overshoot/undershoot</td>
<td>%</td>
<td>&lt; 5</td>
</tr>
<tr class="even">
<td>Input VSWR</td>
<td> </td>
<td>&lt; 1.2</td>
</tr>
<tr class="odd">
<td>Time with full reflection @ required output level – all phases</td>
<td>μs</td>
<td>50</td>
</tr>
<tr class="even">
<td>Resilience to fast load variations (from matched to full reflection)</td>
<td>μs</td>
<td>50</td>
</tr>
<tr class="odd">
<td>@ required output level – all phases</td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>Time with VSWR up to 2 @ required output level – all phases</td>
<td>s</td>
<td>&gt; 3600</td>
</tr>
<tr class="odd">
<td>Harmonics @ required output level</td>
<td>dBc</td>
<td>&lt; -30</td>
</tr>
<tr class="even">
<td>Spurious @ required output level</td>
<td>dBc</td>
<td>&lt; -60</td>
</tr>
<tr class="odd">
<td>Overall efficiency @ nominal output level</td>
<td>%</td>
<td>&gt; 50</td>
</tr>
<tr class="even">
<td>EMC compliance</td>
<td> </td>
<td>CISPR11, IEC61000-6-2</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Major RF amplifier parameters requirements.

 

**Table:** Major amplifier control requirements.

<table>
<caption>Table: Major amplifier control requirements.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>Main control requirements</th>
<th>Expected</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td colspan="2">Permanent and Slow transfer to</td>
</tr>
<tr class="even">
<td>Global transfer time</td>
<td>&lt; 1 sec</td>
</tr>
<tr class="odd">
<td>Transfer protocol</td>
<td>Ethercat &amp; Modbus TCP/IP</td>
</tr>
<tr class="even">
<td colspan="2">Fast acquisition buffering for post or online processing</td>
</tr>
<tr class="odd">
<td>Data of interest</td>
<td>Flags, RF powers, currents and voltages</td>
</tr>
<tr class="even">
<td>Embedded buffers storage</td>
<td>&gt; 10 sec</td>
</tr>
<tr class="odd">
<td>Quantity of embedded buffers</td>
<td>2 sets</td>
</tr>
<tr class="even">
<td>Granularity</td>
<td>&lt; 2 ms</td>
</tr>
<tr class="odd">
<td>Time stamp</td>
<td>For every buffered data</td>
</tr>
<tr class="even">
<td>Transfer protocol</td>
<td>Raw TCP</td>
</tr>
<tr class="odd">
<td colspan="2">Interlocks</td>
</tr>
<tr class="even">
<td>External interlock speed</td>
<td>&lt; 5μs</td>
</tr>
<tr class="odd">
<td>Internal interlocks</td>
<td>VSWR, overdrive, RF power, voltage and current, cooling temperature, water flow...</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Major amplifier control requirements.

 

**Table:** Major parameters requirements for the Injector amplifiers.

<table>
<caption>Table: Major parameters requirements for the Injector amplifiers.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Naming</td>
<td>Acronym</td>
<td>Nominal power</td>
<td>Required power</td>
<td>Output conn. size</td>
</tr>
<tr class="even">
<td>convention</td>
<td> </td>
<td>incl. losses (kW)</td>
<td>incl. margins (kW)</td>
<td>(EIA standard)</td>
</tr>
<tr class="odd">
<td>I1-RFQ</td>
<td>RFQ</td>
<td>111.5</td>
<td>167.3</td>
<td>6 ⅛″</td>
</tr>
<tr class="even">
<td>I1-ME1:RF-CAV-0
1</td>
<td>QWR1</td>
<td>2.3</td>
<td>3.4</td>
<td>1 ⅝″</td>
</tr>
<tr class="odd">
<td>I1-ME1:RF-CAV-0
2</td>
<td>QWR2</td>
<td>2.7</td>
<td>4.1</td>
<td>1 ⅝″</td>
</tr>
<tr class="even">
<td>I1-CH
1:RF-CAV-01</td>
<td>CH01</td>
<td>9.3</td>
<td>13.9</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>I1-CH
2:RF-CAV-01</td>
<td>CH02</td>
<td>13.8</td>
<td>20.7</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>I1-CH
3:RF-CAV-01</td>
<td>CH03</td>
<td>17.9</td>
<td>26.9</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>I1-CH
4:RF-CAV-01</td>
<td>CH04</td>
<td>20.7</td>
<td>31.0</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>I1-CH
5:RF-CAV-01</td>
<td>CH05</td>
<td>24.5</td>
<td>36.8</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>I1-CH
6:RF-CAV-01</td>
<td>CH06</td>
<td>33.8</td>
<td>50.7</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>I1-CH
7:RF-CAV-01</td>
<td>CH07</td>
<td>31.1</td>
<td>46.6</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>I1-ME3:RF-CAV-01</td>
<td>CHR1</td>
<td>8.2</td>
<td>12.3</td>
<td>1 ⅝″</td>
</tr>
<tr class="even">
<td>I1-CH
8:RF-CAV-01</td>
<td>CH08</td>
<td>41.1</td>
<td>61.7</td>
<td>4 ½″</td>
</tr>
<tr class="odd">
<td>I1-CH
9:RF-CAV-01</td>
<td>CH09</td>
<td>40.7</td>
<td>61.0</td>
<td>4 ½″</td>
</tr>
<tr class="even">
<td>I1-CH
10:RF-CAV-01</td>
<td>CH10</td>
<td>40.6</td>
<td>60.9</td>
<td>4 ½″</td>
</tr>
<tr class="odd">
<td>I1-CH
11:RF-CAV-01</td>
<td>CH11</td>
<td>40.3</td>
<td>60.5</td>
<td>4 ½″</td>
</tr>
<tr class="even">
<td>I1-CH
12:RF-CAV-01</td>
<td>CH12</td>
<td>39.8</td>
<td>59.7</td>
<td>4 ½″</td>
</tr>
<tr class="odd">
<td>I1-CH
13:RF-CAV-01</td>
<td>CH13</td>
<td>39.4</td>
<td>59.0</td>
<td>4 ½″</td>
</tr>
<tr class="even">
<td>I1-CH
14:RF-CAV-01</td>
<td>CH14</td>
<td>38.9</td>
<td>58.4</td>
<td>4 ½″</td>
</tr>
<tr class="odd">
<td>I1-CH
15:RF-CAV-01</td>
<td>CH15</td>
<td>38.8</td>
<td>58.2</td>
<td>4 ½″</td>
</tr>
<tr class="even">
<td>I1-ME2:RF-CAV-01</td>
<td>CHR2</td>
<td>TBD</td>
<td>TBD</td>
<td>TBD</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Major parameters requirements for the Injector amplifiers.

 

**Table:** Major parameters requirements for the amplifiers (Spokes
1--30).

<table>
<caption>Table: Major parameters requirements for the amplifiers (Spokes 1–30).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Naming</td>
<td>Acronym</td>
<td>Nominal power</td>
<td>Required power</td>
<td>Output conn. size</td>
</tr>
<tr class="even">
<td>Convention</td>
<td> </td>
<td>incl. losses (kW)</td>
<td>incl. margins (kW)</td>
<td>(EIA standard)</td>
</tr>
<tr class="odd">
<td>LB-ME3:RF-CAV-01</td>
<td>Spoke Reb. 1</td>
<td>3.847</td>
<td>5.77</td>
<td>1 ⅝″</td>
</tr>
<tr class="even">
<td>LB-ME3:RF-CAV-02</td>
<td>Spoke Reb. 2</td>
<td>3.85</td>
<td>5.77</td>
<td>1 ⅝″</td>
</tr>
<tr class="odd">
<td>LB-S00:RF-CAV-01</td>
<td>Spoke 1</td>
<td>8.44</td>
<td>12.66</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S01:RF-CAV-02</td>
<td>Spoke 2</td>
<td>8.50</td>
<td>12.75</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S01:RF-CAV-01</td>
<td>Spoke 3</td>
<td>8.57</td>
<td>12.85</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S02:RF-CAV-02</td>
<td>Spoke 4</td>
<td>8.63</td>
<td>12.95</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S02:RF-CAV-01</td>
<td>Spoke 5</td>
<td>8.72</td>
<td>13.08</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S03:RF-CAV-02</td>
<td>Spoke 6</td>
<td>8.80</td>
<td>13.19</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S03:RF-CAV-01</td>
<td>Spoke 7</td>
<td>8.92</td>
<td>13.37</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S04:RF-CAV-02</td>
<td>Spoke 8</td>
<td>9.00</td>
<td>13.50</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S04:RF-CAV-01</td>
<td>Spoke 9</td>
<td>9.16</td>
<td>13.74</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S05:RF-CAV-02</td>
<td>Spoke 10</td>
<td>9.25</td>
<td>13.88</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S05:RF-CAV-01</td>
<td>Spoke 11</td>
<td>9.47</td>
<td>14.21</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S06:RF-CAV-02</td>
<td>Spoke 12</td>
<td>9.58</td>
<td>14.37</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S06:RF-CAV-01</td>
<td>Spoke 13</td>
<td>9.89</td>
<td>14.84</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S07:RF-CAV-02</td>
<td>Spoke 14</td>
<td>10.02</td>
<td>15.03</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S07:RF-CAV-01</td>
<td>Spoke 15</td>
<td>10.46</td>
<td>15.70</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S08:RF-CAV-02</td>
<td>Spoke 16</td>
<td>10.62</td>
<td>15.92</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S08:RF-CAV-01</td>
<td>Spoke 17</td>
<td>11.29</td>
<td>16.93</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S09:RF-CAV-02</td>
<td>Spoke 18</td>
<td>11.47</td>
<td>17.21</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S09:RF-CAV-01</td>
<td>Spoke 19</td>
<td>12.54</td>
<td>18.81</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S10:RF-CAV-02</td>
<td>Spoke 20</td>
<td>12.77</td>
<td>19.16</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S10:RF-CAV-01</td>
<td>Spoke 21</td>
<td>13.85</td>
<td>20.78</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S11:RF-CAV-02</td>
<td>Spoke 22</td>
<td>14.10</td>
<td>21.15</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S11:RF-CAV-01</td>
<td>Spoke 23</td>
<td>14.39</td>
<td>21.58</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S12:RF-CAV-02</td>
<td>Spoke 24</td>
<td>14.58</td>
<td>21.87</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S12:RF-CAV-01</td>
<td>Spoke 25</td>
<td>14.77</td>
<td>22.16</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S13:RF-CAV-02</td>
<td>Spoke 26</td>
<td>14.91</td>
<td>22.36</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S13:RF-CAV-01</td>
<td>Spoke 27</td>
<td>15.05</td>
<td>22.58</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S14:RF-CAV-02</td>
<td>Spoke 28</td>
<td>15.15</td>
<td>22.72</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S14:RF-CAV-01</td>
<td>Spoke 29</td>
<td>15.25</td>
<td>22.88</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S15:RF-CAV-02</td>
<td>Spoke 30</td>
<td>15.31</td>
<td>22.97</td>
<td>3 ⅛″</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Major parameters requirements for the amplifiers (Spokes 1--30).

 

**Table:** Major parameters requirements for the amplifiers (Spokes
31--46).

<table>
<caption>Table: Major parameters requirements for the amplifiers (Spokes 31–46).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Naming</td>
<td>Acronym</td>
<td>Nominal power</td>
<td>Required power</td>
<td>Output conn.</td>
</tr>
<tr class="even">
<td>Convention</td>
<td> </td>
<td>incl. losses (kW)</td>
<td>incl. margins (kW)</td>
<td>size (EIA standard)</td>
</tr>
<tr class="odd">
<td>LB-S15:RF-CAV-01</td>
<td>Spoke 31</td>
<td>15.38</td>
<td>23.07</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S16:RF-CAV-02</td>
<td>Spoke 32</td>
<td>15.41</td>
<td>23.12</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S16:RF-CAV-01</td>
<td>Spoke 33</td>
<td>15.46</td>
<td>23.19</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S17:RF-CAV-02</td>
<td>Spoke 34</td>
<td>15.47</td>
<td>23.21</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S17:RF-CAV-01</td>
<td>Spoke 35</td>
<td>15.50</td>
<td>23.25</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S18:RF-CAV-02</td>
<td>Spoke 36</td>
<td>15.50</td>
<td>23.25</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S18:RF-CAV-01</td>
<td>Spoke 37</td>
<td>15.51</td>
<td>23.27</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S19:RF-CAV-02</td>
<td>Spoke 38</td>
<td>15.50</td>
<td>23.25</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S19:RF-CAV-01</td>
<td>Spoke 39</td>
<td>15.50</td>
<td>23.25</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S20:RF-CAV-02</td>
<td>Spoke 40</td>
<td>15.49</td>
<td>23.23</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S20:RF-CAV-01</td>
<td>Spoke 41</td>
<td>15.48</td>
<td>23.22</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S21:RF-CAV-02</td>
<td>Spoke 42</td>
<td>15.46</td>
<td>23.19</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S21:RF-CAV-01</td>
<td>Spoke 43</td>
<td>15.45</td>
<td>23.17</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S22:RF-CAV-02</td>
<td>Spoke 44</td>
<td>15.42</td>
<td>23.13</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S22:RF-CAV-01</td>
<td>Spoke 45</td>
<td>15.41</td>
<td>23.11</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S23:RF-CAV-02</td>
<td>Spoke 46</td>
<td>15.38</td>
<td>23.07</td>
<td>3 ⅛″</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Major parameters requirements for the amplifiers (Spokes 31--46).

 

**Table:** Results overview of the RF characterisation of the RFQ
amplifier.

<table>
<caption>Table: Results overview of the RF characterisation of the RFQ amplifier.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>PARAMETERS</td>
<td>UNITS</td>
<td> 
RESULTS</td>
</tr>
<tr class="even">
<td>Max output power on matched load</td>
<td>kW</td>
<td>192</td>
</tr>
<tr class="odd">
<td>Input return loss over the full power range</td>
<td>dB</td>
<td>-20</td>
</tr>
<tr class="even">
<td>Wall plug efficiency</td>
<td>%</td>
<td>73.6 (max level) / 61.5 (operating level)</td>
</tr>
<tr class="odd">
<td>AC/DC converters efficiency</td>
<td>%</td>
<td>90 – 92</td>
</tr>
<tr class="even">
<td>Harmonics level over the full power range</td>
<td>dBc</td>
<td>&lt; -45</td>
</tr>
<tr class="odd">
<td>Gain variation within ±1MHz BW</td>
<td>dB</td>
<td>1.4 (100kW), 0.95 (140kW)</td>
</tr>
<tr class="even">
<td>Bandwidth (@ -1 dB)</td>
<td>MHz</td>
<td>&gt; ±1</td>
</tr>
<tr class="odd">
<td>Maximum gain</td>
<td>dB</td>
<td>85</td>
</tr>
<tr class="even">
<td>Large dynamic range (up to required level)</td>
<td>dB</td>
<td>&gt; 20</td>
</tr>
<tr class="odd">
<td>Gain linearity over large dynamic range</td>
<td>dB</td>
<td>8</td>
</tr>
<tr class="even">
<td>Phase linearity over large dynamic range</td>
<td>deg</td>
<td>20.5</td>
</tr>
<tr class="odd">
<td>Reduced dynamic range (around operating level)</td>
<td>dB</td>
<td>±1.22</td>
</tr>
<tr class="even">
<td>Gain linearity over reduced dynamic range</td>
<td>dB</td>
<td>0.46</td>
</tr>
<tr class="odd">
<td>Phase linearity over reduced dynamic range</td>
<td>deg</td>
<td>14.5</td>
</tr>
<tr class="even">
<td>1 dB compression point</td>
<td>kW</td>
<td>70</td>
</tr>
<tr class="odd">
<td>Warm up time at 120kW</td>
<td>sec</td>
<td>60</td>
</tr>
<tr class="even">
<td>Gain variations during warm up</td>
<td>dB</td>
<td>0.15</td>
</tr>
<tr class="odd">
<td>Phase variations during warm up</td>
<td>deg</td>
<td>0.57</td>
</tr>
<tr class="even">
<td>Gain stability over 1 s after warm up</td>
<td>dB</td>
<td>&lt; 0.02</td>
</tr>
<tr class="odd">
<td>Phase stability over 1 s after warm up</td>
<td>deg</td>
<td>&lt; 0.15</td>
</tr>
<tr class="even">
<td>Pulse rise time</td>
<td>µs</td>
<td>1.6</td>
</tr>
<tr class="odd">
<td>Pulse amplitude overshoot/undershoot</td>
<td>%</td>
<td>&lt; 5</td>
</tr>
<tr class="even">
<td>Group delay</td>
<td>ns</td>
<td>87</td>
</tr>
<tr class="odd">
<td>External combiner return loss</td>
<td>dB</td>
<td>&lt; -25</td>
</tr>
<tr class="even">
<td>External combiner directivity</td>
<td>dB</td>
<td>&gt; 25</td>
</tr>
<tr class="odd">
<td>Max CW forward power under VSWR = 2</td>
<td>kW</td>
<td>143</td>
</tr>
<tr class="even">
<td>Max CW forward power under VSWR = 3</td>
<td>kW</td>
<td>124</td>
</tr>
<tr class="odd">
<td>Max pulsed forward power under full reflection</td>
<td>kW</td>
<td>92 (10 ms, 40 %)</td>
</tr>
<tr class="even">
<td>Delivery of required forward power up to VSWR</td>
<td> </td>
<td>2</td>
</tr>
<tr class="odd">
<td>RF leak level at 120kW</td>
<td>dBμV / m</td>
<td>&lt; 104.5</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Results overview of the RF characterisation of the RFQ amplifier.

 

**Table:** SSAs repartition per cabinet for the Injector prototyping
activity.

<table>
<caption>Table: SSAs repartition per cabinet for the Injector prototyping activity.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Acronym</td>
<td>QWR 1</td>
<td>QWR 2</td>
<td>CH01</td>
<td>CH02</td>
<td>CH03</td>
<td>CH04</td>
<td>CH05</td>
<td>CH06</td>
<td>CH07</td>
</tr>
<tr class="even">
<td>Naming convention</td>
<td>I1-ME1:RF-CAV-0
1</td>
<td>I1-ME1:RF-CAV-0
2</td>
<td>I1-CH
1:RF-CAV-01</td>
<td>I1-CH
2:RF-CAV-01</td>
<td>I1-CH
3:RF-CAV-01</td>
<td>I1-CH
4:RF-CAV-01</td>
<td>I1-CH
5:RF-CAV-01</td>
<td>I1-CH
6:RF-CAV-01</td>
<td>I1-CH
7:RF-CAV-01</td>
</tr>
<tr class="odd">
<td>Cabinet 1</td>
<td>6kW</td>
<td>6kW</td>
<td>24kW</td>
<td>24kW</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
</tr>
<tr class="even">
<td>Cabinet 2</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>36kW</td>
<td>36kW</td>
<td>–</td>
<td>–</td>
<td>–</td>
</tr>
<tr class="odd">
<td>Cabinet 3</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>48kW</td>
<td>–</td>
<td>48kW</td>
</tr>
<tr class="even">
<td>Cabinet 4</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>60kW</td>
<td>–</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: SSAs repartition per cabinet for the Injector prototyping
activity.

 

**Table:** Estimation of the SSA cabinet quantity for the MINERVA linac.

<table>
<caption>Table: Estimation of the SSA cabinet quantity for the MINERVA linac.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Acronym</td>
<td>RFQ</td>
<td>QWR1 ⇨ CH07</td>
<td>CHR1 ⇨ CH15</td>
<td>Spoke 1 ⇨ Spoke 46</td>
</tr>
<tr class="even">
<td>Naming</td>
<td>I1-RFQ</td>
<td>I1-ME1:RF-CAV-0
1 ⇨</td>
<td>I1-ME3:RF-CAV-01 ⇨</td>
<td>LB-S00:RF-CAV-01 ⇨</td>
</tr>
<tr class="odd">
<td>convention</td>
<td> </td>
<td>I1-CH
7:RF-CAV-01</td>
<td>I1-CH
15:RF-CAV-01</td>
<td>LB-S23:RF-CAV-02</td>
</tr>
<tr class="even">
<td>No. of cabinet</td>
<td>2</td>
<td>4</td>
<td>8</td>
<td>15</td>
</tr>
<tr class="odd">
<td>No. of output</td>
<td>0.5</td>
<td>4 (1<sup>st</sup> cabinet) 1 (4<sup>th</sup> cabinet)</td>
<td>2 (1<sup>st</sup> cabinet)</td>
<td>4</td>
</tr>
<tr class="even">
<td>per cabinet</td>
<td> </td>
<td>2 (others)</td>
<td>1 (others)</td>
<td> </td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Estimation of the SSA cabinet quantity for the MINERVA linac.

 

**Table:** Estimation of the water cooling requirements for the MINERVA
SSAs.

<table>
<caption>Table: Estimation of the water cooling requirements for the MINERVA SSAs.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td> </td>
<td>SSAs for Injector</td>
<td>SSAs for</td>
</tr>
<tr class="even">
<td>Medium</td>
<td> </td>
<td colspan="2">Normal water</td>
</tr>
<tr class="odd">
<td>Particle filtering size</td>
<td>μm</td>
<td colspan="2">&lt; 500</td>
</tr>
<tr class="even">
<td>Minimum flow rate</td>
<td>L min⁻¹</td>
<td>2105</td>
<td>1400</td>
</tr>
<tr class="odd">
<td>Input absolute pressure</td>
<td>bar</td>
<td colspan="2">&lt; 6</td>
</tr>
<tr class="even">
<td>Output absolute pressure</td>
<td>bar</td>
<td colspan="2">&gt; 1</td>
</tr>
<tr class="odd">
<td>Pressure drop</td>
<td>bar</td>
<td colspan="2">&lt; 4</td>
</tr>
<tr class="even">
<td>Input temperature</td>
<td>℃</td>
<td colspan="2">&lt; 25</td>
</tr>
<tr class="odd">
<td>Input temp. stability</td>
<td>℃</td>
<td colspan="2">&lt; ± 0.5</td>
</tr>
<tr class="even">
<td>Output temperature</td>
<td>℃</td>
<td colspan="2">&lt; 35</td>
</tr>
<tr class="odd">
<td>Delta temperature</td>
<td>℃</td>
<td colspan="2">&lt; 10</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Estimation of the water cooling requirements for the MINERVA
SSAs.

 

**Table:** Characteristics of some rectangular waveguides.

<table>
<caption>Table: Characteristics of some rectangular waveguides.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Waveguide EIA name</td>
<td>Recommended frequency band of operation (GHz)</td>
<td>Inner dimensions of waveguide opening (mm)</td>
<td>TE<sub>10</sub>-mode cut-off frequency (GHz)</td>
<td>TE<sub>20</sub>-mode cut-off frequency (GHz)</td>
</tr>
<tr class="even">
<td>WR2300</td>
<td>0.32 — 0.45</td>
<td>584.2 × 292.1</td>
<td>0.26</td>
<td>0.51</td>
</tr>
<tr class="odd">
<td>WR1150</td>
<td>0.63 — 0.97</td>
<td>292.1 × 146.05</td>
<td>0.51</td>
<td>1.03</td>
</tr>
<tr class="even">
<td>WR340</td>
<td>2.2 — 3.3</td>
<td>86.36 × 43.18</td>
<td>1.7</td>
<td>3.5</td>
</tr>
<tr class="odd">
<td>WR75</td>
<td>10 — 15</td>
<td>19.05 × 9.52</td>
<td>7.9</td>
<td>15.7</td>
</tr>
<tr class="even">
<td>WR10</td>
<td>70 — 110</td>
<td>2.54 × 1.27</td>
<td>59</td>
<td>118</td>
</tr>
<tr class="odd">
<td>WR3</td>
<td>220 — 330</td>
<td>0.864 × 0.432</td>
<td>173</td>
<td>346</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Characteristics of some rectangular waveguides.

 

**Table:** Dimensions and cut-off frequencies of most used rigid coaxial
waveguides.

<table>
<caption>Table: Dimensions and cut-off frequencies of most used rigid coaxial waveguides.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Coaxial EIA name</td>
<td colspan="2">Outer conductor</td>
<td colspan="2">Inner conductor</td>
<td>Cut-off frequency TE11-mode (GHz)</td>
</tr>
<tr class="even">
<td> </td>
<td>Outer diameter (mm)</td>
<td>Inner diameter (mm) = D₁</td>
<td>Outer diameter (mm) = D₂</td>
<td>Inner diameter (mm)</td>
<td> </td>
</tr>
<tr class="odd">
<td>⅞″</td>
<td>22.2</td>
<td>20</td>
<td>8.7</td>
<td>7.4</td>
<td>6.3</td>
</tr>
<tr class="even">
<td>1 ⅝″</td>
<td>41.3</td>
<td>38.8</td>
<td>16.9</td>
<td>15</td>
<td>3.2</td>
</tr>
<tr class="odd">
<td>3 ⅛″</td>
<td>79.4</td>
<td>76.9</td>
<td>33.4</td>
<td>31.3</td>
<td>1.6</td>
</tr>
<tr class="even">
<td>4 ½″</td>
<td>106</td>
<td>103</td>
<td>44.8</td>
<td>42.8</td>
<td>1.2</td>
</tr>
<tr class="odd">
<td>6 ⅛″</td>
<td>155.6</td>
<td>151.9</td>
<td>66</td>
<td>64</td>
<td>0.83</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Dimensions and cut-off frequencies of most used rigid coaxial
waveguides.

 

**Table:** Maximum power handling of coaxial waveguides.

<table>
<caption>Table: Maximum power handling of coaxial waveguides.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td colspan="5">CW power handling capability (kW)</td>
</tr>
<tr class="even">
<td>f (MHz)</td>
<td>⅞″</td>
<td>1 ⅝″</td>
<td>3 ⅛″</td>
<td>4 ½″</td>
<td>6 ⅛″</td>
</tr>
<tr class="odd">
<td>176.1</td>
<td>5.7</td>
<td>15</td>
<td>48</td>
<td>80</td>
<td>159</td>
</tr>
<tr class="even">
<td>352.2</td>
<td>4.1</td>
<td>11</td>
<td>34</td>
<td>57</td>
<td>111</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Maximum power handling of coaxial waveguides.

 

**Table:** Typical attenuations of coaxial waveguides.

<table>
<caption>Table: Typical attenuations of coaxial waveguides.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td colspan="5">Attenuation (dB/100m)</td>
</tr>
<tr class="even">
<td>f (MHz)</td>
<td>⅞″</td>
<td>1 ⅝″</td>
<td>3 ⅛″</td>
<td>4 ½″</td>
<td>6 ⅛″</td>
</tr>
<tr class="odd">
<td>176.1</td>
<td>1.61</td>
<td>0.99</td>
<td>0.49</td>
<td>0.37</td>
<td>0.25</td>
</tr>
<tr class="even">
<td>352.2</td>
<td>2.27</td>
<td>1.40</td>
<td>0.69</td>
<td>0.52</td>
<td>0.35</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Typical attenuations of coaxial waveguides.

 

**Table:** Configuration of the MINERVA Tx lines.

<table>
<caption>Table: Configuration of the MINERVA Tx lines.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Cavity name</td>
<td>Naming convention</td>
<td>Size of Tx line</td>
<td>Interface with SSA</td>
<td>Interface with power coupler</td>
</tr>
<tr class="even">
<td>RFQ</td>
<td>I1-RFQ</td>
<td>6 ⅛″</td>
<td>6 ⅛″ EIA</td>
<td>6 ⅛″ EIA</td>
</tr>
<tr class="odd">
<td>QWR1 &amp; 2</td>
<td>I1-ME1:RF-CAV-0
1 &amp; I1-ME1:RF-CAV-0
2</td>
<td>⅞″ (flexible)</td>
<td>1 ⅝″ EIA</td>
<td>1 ⅝″ EIA</td>
</tr>
<tr class="even">
<td>CH01 to 07</td>
<td>I1-CH
1:RF-CAV-01 to I1-CH
7:RF-CAV-01</td>
<td>3 ⅛″</td>
<td>4 ½″ EIA</td>
<td>6 ⅛″ EIA</td>
</tr>
<tr class="odd">
<td>CHR1</td>
<td>I1-ME3:RF-CAV-01</td>
<td>1 ⅝″</td>
<td>1 ⅝″ EIA</td>
<td>1 ⅝″ EIA</td>
</tr>
<tr class="even">
<td>CH08 to 15</td>
<td>I1-CH
8:RF-CAV-01 to I1-CH
15:RF-CAV-01</td>
<td>4 ½″</td>
<td>4 ½″ EIA</td>
<td>6 ⅛″ EIA</td>
</tr>
<tr class="odd">
<td>CHR2</td>
<td>I1-ME2:RF-CAV-01</td>
<td>TBD</td>
<td>TBD</td>
<td>6 ⅛″ EIA</td>
</tr>
<tr class="even">
<td>Spoke Reb. 1 &amp; 2</td>
<td>I1-ME3:RF-CAV-01 &amp; I1-ME2:RF-CAV-01</td>
<td>1 ⅝″ EIA</td>
<td>1 ⅝″ EIA</td>
<td>3 ⅛″ EIA</td>
</tr>
<tr class="odd">
<td>Spoke 1 to Spoke 46</td>
<td>LB-S00:RF-CAV-01 to LB-S23:RF-CAV-02</td>
<td>3 ⅛″ EIA</td>
<td>3 ⅛″ EIA</td>
<td>3 ⅛″ EIA</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Configuration of the MINERVA Tx lines.

 

**Table:** Transmission line ohmic losses per meter for the Injector
amplifiers.

<table>
<caption>Table: Transmission line ohmic losses per meter for the Injector amplifiers.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Naming</td>
<td>Acronym</td>
<td>Tx line losses per meter</td>
</tr>
<tr class="even">
<td>I1-RFQ</td>
<td>RFQ</td>
<td>71.89</td>
</tr>
<tr class="odd">
<td>I1-ME1:RF-CAV-0
1</td>
<td>QWR1</td>
<td>5.92</td>
</tr>
<tr class="even">
<td>I1-ME1:RF-CAV-0
2</td>
<td>QWR2</td>
<td>7.10</td>
</tr>
<tr class="odd">
<td>I1-CH
1:RF-CAV-01</td>
<td>CH01</td>
<td>11.96</td>
</tr>
<tr class="even">
<td>I1-CH
2:RF-CAV-01</td>
<td>CH02</td>
<td>17.75</td>
</tr>
<tr class="odd">
<td>I1-CH
3:RF-CAV-01</td>
<td>CH03</td>
<td>23.07</td>
</tr>
<tr class="even">
<td>I1-CH
4:RF-CAV-01</td>
<td>CH04</td>
<td>26.64</td>
</tr>
<tr class="odd">
<td>I1-CH
5:RF-CAV-01</td>
<td>CH05</td>
<td>31.61</td>
</tr>
<tr class="even">
<td>I1-CH
6:RF-CAV-01</td>
<td>CH06</td>
<td>43.59</td>
</tr>
<tr class="odd">
<td>I1-CH
7:RF-CAV-01</td>
<td>CH07</td>
<td>40.01</td>
</tr>
<tr class="even">
<td>I1-ME3:RF-CAV-01</td>
<td>CHR1</td>
<td>21.31</td>
</tr>
<tr class="odd">
<td>I1-CH
8:RF-CAV-01</td>
<td>CH08</td>
<td>34.08</td>
</tr>
<tr class="even">
<td>I1-CH
9:RF-CAV-01</td>
<td>CH09</td>
<td>33.72</td>
</tr>
<tr class="odd">
<td>I1-CH
10:RF-CAV-01</td>
<td>CH10</td>
<td>33.65</td>
</tr>
<tr class="even">
<td>I1-CH
11:RF-CAV-01</td>
<td>CH11</td>
<td>33.41</td>
</tr>
<tr class="odd">
<td>I1-CH
12:RF-CAV-01</td>
<td>CH12</td>
<td>32.96</td>
</tr>
<tr class="even">
<td>I1-CH
13:RF-CAV-01</td>
<td>CH13</td>
<td>32.61</td>
</tr>
<tr class="odd">
<td>I1-CH
14:RF-CAV-01</td>
<td>CH14</td>
<td>32.27</td>
</tr>
<tr class="even">
<td>I1-CH
15:RF-CAV-01</td>
<td>CH15</td>
<td>32.16</td>
</tr>
<tr class="odd">
<td>I1-ME2:RF-CAV-01</td>
<td>CHR2</td>
<td>TBD</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Transmission line ohmic losses per meter for the Injector
amplifiers.

 

**Table:** Transmission line ohmic losses per meter for the amplifiers
(Spoke rebunchers and spokes 1--22).

<table>
<caption>Table: Transmission line ohmic losses per meter for the amplifiers (Spoke rebunchers and spokes 1–22).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Naming</td>
<td>Acronym</td>
<td>Tx line losses per meter</td>
</tr>
<tr class="even">
<td>LB-ME3:RF-CAV-01</td>
<td>Spoke Reb. 1</td>
<td>12.38</td>
</tr>
<tr class="odd">
<td>LB-ME3:RF-CAV-02</td>
<td>Spoke Reb. 2</td>
<td>12.38</td>
</tr>
<tr class="even">
<td>LB-S00:RF-CAV-01</td>
<td>Spoke 1</td>
<td>13.40</td>
</tr>
<tr class="odd">
<td>LB-S01:RF-CAV-02</td>
<td>Spoke 2</td>
<td>13.49</td>
</tr>
<tr class="even">
<td>LB-S01:RF-CAV-01</td>
<td>Spoke 3</td>
<td>13.60</td>
</tr>
<tr class="odd">
<td>LB-S02:RF-CAV-02</td>
<td>Spoke 4</td>
<td>13.70</td>
</tr>
<tr class="even">
<td>LB-S02:RF-CAV-01</td>
<td>Spoke 5</td>
<td>13.85</td>
</tr>
<tr class="odd">
<td>LB-S03:RF-CAV-02</td>
<td>Spoke 6</td>
<td>13.96</td>
</tr>
<tr class="even">
<td>LB-S03:RF-CAV-01</td>
<td>Spoke 7</td>
<td>14.15</td>
</tr>
<tr class="odd">
<td>LB-S04:RF-CAV-02</td>
<td>Spoke 8</td>
<td>14.28</td>
</tr>
<tr class="even">
<td>LB-S04:RF-CAV-01</td>
<td>Spoke 9</td>
<td>14.54</td>
</tr>
<tr class="odd">
<td>LB-S05:RF-CAV-02</td>
<td>Spoke 10</td>
<td>14.69</td>
</tr>
<tr class="even">
<td>LB-S05:RF-CAV-01</td>
<td>Spoke 11</td>
<td>15.04</td>
</tr>
<tr class="odd">
<td>LB-S06:RF-CAV-02</td>
<td>Spoke 12</td>
<td>15.21</td>
</tr>
<tr class="even">
<td>LB-S06:RF-CAV-01</td>
<td>Spoke 13</td>
<td>15.70</td>
</tr>
<tr class="odd">
<td>LB-S07:RF-CAV-02</td>
<td>Spoke 14</td>
<td>15.90</td>
</tr>
<tr class="even">
<td>LB-S07:RF-CAV-01</td>
<td>Spoke 15</td>
<td>16.61</td>
</tr>
<tr class="odd">
<td>LB-S08:RF-CAV-02</td>
<td>Spoke 16</td>
<td>16.85</td>
</tr>
<tr class="even">
<td>LB-S08:RF-CAV-01</td>
<td>Spoke 17</td>
<td>17.92</td>
</tr>
<tr class="odd">
<td>LB-S09:RF-CAV-02</td>
<td>Spoke 18</td>
<td>18.21</td>
</tr>
<tr class="even">
<td>LB-S09:RF-CAV-01</td>
<td>Spoke 19</td>
<td>19.90</td>
</tr>
<tr class="odd">
<td>LB-S10:RF-CAV-02</td>
<td>Spoke 20</td>
<td>20.27</td>
</tr>
<tr class="even">
<td>LB-S10:RF-CAV-01</td>
<td>Spoke 21</td>
<td>21.99</td>
</tr>
<tr class="odd">
<td>LB-S11:RF-CAV-02</td>
<td>Spoke 22</td>
<td>22.38</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Transmission line ohmic losses per meter for the amplifiers
(Spoke rebunchers and spokes 1--22).

 

**Table:** Transmission line ohmic losses per meter for the amplifiers
(Spokes 23--46).

<table>
<caption>Table: Transmission line ohmic losses per meter for the amplifiers (Spokes 23–46).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Naming</td>
<td>Acronym</td>
<td>Tx line losses per meter</td>
</tr>
<tr class="even">
<td>LB-S11:RF-CAV-01</td>
<td>Spoke 23</td>
<td>22.84</td>
</tr>
<tr class="odd">
<td>LB-S12:RF-CAV-02</td>
<td>Spoke 24</td>
<td>23.14</td>
</tr>
<tr class="even">
<td>LB-S12:RF-CAV-01</td>
<td>Spoke 25</td>
<td>23.45</td>
</tr>
<tr class="odd">
<td>LB-S13:RF-CAV-02</td>
<td>Spoke 26</td>
<td>23.67</td>
</tr>
<tr class="even">
<td>LB-S13:RF-CAV-01</td>
<td>Spoke 27</td>
<td>23.90</td>
</tr>
<tr class="odd">
<td>LB-S14:RF-CAV-02</td>
<td>Spoke 28</td>
<td>24.05</td>
</tr>
<tr class="even">
<td>LB-S14:RF-CAV-01</td>
<td>Spoke 29</td>
<td>24.21</td>
</tr>
<tr class="odd">
<td>LB-S15:RF-CAV-02</td>
<td>Spoke 30</td>
<td>24.31</td>
</tr>
<tr class="even">
<td>LB-S15:RF-CAV-01</td>
<td>Spoke 31</td>
<td>24.42</td>
</tr>
<tr class="odd">
<td>LB-S16:RF-CAV-02</td>
<td>Spoke 32</td>
<td>24.47</td>
</tr>
<tr class="even">
<td>LB-S16:RF-CAV-01</td>
<td>Spoke 33</td>
<td>24.54</td>
</tr>
<tr class="odd">
<td>LB-S17:RF-CAV-02</td>
<td>Spoke 34</td>
<td>24.56</td>
</tr>
<tr class="even">
<td>LB-S17:RF-CAV-01</td>
<td>Spoke 35</td>
<td>24.60</td>
</tr>
<tr class="odd">
<td>LB-S18:RF-CAV-02</td>
<td>Spoke 36</td>
<td>24.61</td>
</tr>
<tr class="even">
<td>LB-S18:RF-CAV-01</td>
<td>Spoke 37</td>
<td>24.62</td>
</tr>
<tr class="odd">
<td>LB-S19:RF-CAV-02</td>
<td>Spoke 38</td>
<td>24.61</td>
</tr>
<tr class="even">
<td>LB-S19:RF-CAV-01</td>
<td>Spoke 39</td>
<td>24.61</td>
</tr>
<tr class="odd">
<td>LB-S20:RF-CAV-02</td>
<td>Spoke 40</td>
<td>24.59</td>
</tr>
<tr class="even">
<td>LB-S20:RF-CAV-01</td>
<td>Spoke 41</td>
<td>24.58</td>
</tr>
<tr class="odd">
<td>LB-S21:RF-CAV-02</td>
<td>Spoke 42</td>
<td>24.54</td>
</tr>
<tr class="even">
<td>LB-S21:RF-CAV-01</td>
<td>Spoke 43</td>
<td>24.52</td>
</tr>
<tr class="odd">
<td>LB-S22:RF-CAV-02</td>
<td>Spoke 44</td>
<td>24.48</td>
</tr>
<tr class="even">
<td>LB-S22:RF-CAV-01</td>
<td>Spoke 45</td>
<td>24.46</td>
</tr>
<tr class="odd">
<td>LB-S23:RF-CAV-02</td>
<td>Spoke 46</td>
<td>24.41</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Transmission line ohmic losses per meter for the amplifiers
(Spokes 23--46).

 

**Table:** General requirements for the isolators.

<table>
<caption>Table: General requirements for the isolators.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Component</td>
<td>Interface (EIA)</td>
<td>Quantity</td>
<td>Power range (kW)</td>
<td>Required power (kW)</td>
</tr>
<tr class="even">
<td>Circulator</td>
<td>1 ⅝″</td>
<td>20</td>
<td>7.3 – 10.8</td>
<td>15</td>
</tr>
<tr class="odd">
<td> —-</td>
<td>3 ⅛″</td>
<td>43</td>
<td>11.1 – 21.6</td>
<td>25</td>
</tr>
<tr class="even">
<td>Load</td>
<td>1 ⅝″</td>
<td>20</td>
<td>7.3 – 10.8</td>
<td>15</td>
</tr>
<tr class="odd">
<td> —-</td>
<td>3 ⅛″</td>
<td>43</td>
<td>11.1 – 21.6</td>
<td>25</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: General requirements for the isolators.

 

**Table:** Specific requirements for the circulators.

<table>
<caption>Table: Specific requirements for the circulators.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Parameter</td>
<td>Requirement</td>
</tr>
<tr class="even">
<td>Centre frequency</td>
<td>352.2MHz</td>
</tr>
<tr class="odd">
<td>Bandwidth</td>
<td>±1MHz</td>
</tr>
<tr class="even">
<td>CW forward RF power</td>
<td>As per “Required power including margin” given in Table <a href="#tabrfsysinj-amp-reqs">[*]</a>, Sect. <a href="#ssect:RFsys:specific:injector">[*]</a>.</td>
</tr>
<tr class="odd">
<td>CW reverse RF power</td>
<td>full reflection, any phase</td>
</tr>
<tr class="even">
<td>Pulse repetition rate</td>
<td>1–250Hz</td>
</tr>
<tr class="odd">
<td>Pulse Length</td>
<td>100μs to CW</td>
</tr>
<tr class="even">
<td>Insertion Loss</td>
<td>&lt; 0.2dB</td>
</tr>
<tr class="odd">
<td>Isolation at f0</td>
<td>&gt; 26dB</td>
</tr>
<tr class="even">
<td>Isolation in BW</td>
<td>&gt; 20dB</td>
</tr>
<tr class="odd">
<td>Return loss at f0</td>
<td>&gt; 26dB</td>
</tr>
<tr class="even">
<td>Return loss in BW</td>
<td>&gt; 20dB</td>
</tr>
<tr class="odd">
<td>Water Temperature Optimum</td>
<td>20℃</td>
</tr>
<tr class="even">
<td>Water temperature range</td>
<td>18–22℃</td>
</tr>
<tr class="odd">
<td>Arc detection</td>
<td>Integrated</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Specific requirements for the circulators.

 

**Table:** Stability R&D objectives.

<table>
<caption>Table: Stability R&amp;D objectives.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Parameter</td>
<td>LinacLLRF-system</td>
<td>Master oscillator</td>
<td>PRDS</td>
</tr>
<tr class="even">
<td>Amplitude (RMS)</td>
<td>±0.2%</td>
<td>–</td>
<td>–</td>
</tr>
<tr class="odd">
<td>Phase (RMS)</td>
<td>±0.1%</td>
<td>±0.1%</td>
<td>±0.1%</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Stability R&D objectives.

 

**Table:** Jitter requirements.

<table>
<caption>Table: Jitter requirements.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Frequency (MHz)</td>
<td>Jitter RMS (Deg)</td>
<td>Jitter RMS (ps)</td>
</tr>
<tr class="even">
<td>176.1</td>
<td>0.1</td>
<td>1.58</td>
</tr>
<tr class="odd">
<td>352.2</td>
<td>0.1</td>
<td>0.79</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Jitter requirements.

 

**Table:** Major RF amplifier parameters requirements.

<table>
<caption>Table: Major RF amplifier parameters requirements.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th>Main RF requirements</th>
<th>Unit</th>
<th>Expected</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Center frequency</td>
<td>MHz</td>
<td>176.1 or 352.2</td>
</tr>
<tr class="even">
<td>Bandwidth (@ -1dB)</td>
<td>MHz</td>
<td>&gt; +/- 1</td>
</tr>
<tr class="odd">
<td>Nominal input level</td>
<td>dBm</td>
<td>&lt; 10</td>
</tr>
<tr class="even">
<td>Output connector type</td>
<td>EIA </td>
<td>EIA coaxial flange</td>
</tr>
<tr class="odd">
<td>Rated output power including losses and margins</td>
<td>kW</td>
<td>See Tables <a href="#tabrfsysinj-amp-reqs">[*]</a>–<a href="#tabrfsyslinac-amp-reqs-b">[*]</a></td>
</tr>
<tr class="even">
<td>Gain linearity over 20dB dynamic range</td>
<td>dB</td>
<td>&lt; 10</td>
</tr>
<tr class="odd">
<td>Phase linearity over 20dB dynamic range</td>
<td>deg</td>
<td>&lt; 25</td>
</tr>
<tr class="even">
<td>Gain linearity @ ±1.22dB around nominal output level</td>
<td>dB</td>
<td>&lt; 1</td>
</tr>
<tr class="odd">
<td>Phase linearity @ ±1.22dB around nominal output level</td>
<td>deg</td>
<td>&lt; 18</td>
</tr>
<tr class="even">
<td>Pulse duration</td>
<td>μs</td>
<td>100 to CW</td>
</tr>
<tr class="odd">
<td>Pulse repetition rate</td>
<td>Hz</td>
<td>1 to 250</td>
</tr>
<tr class="even">
<td>Pulse rise time (10-90%)</td>
<td>μs</td>
<td>&lt; 2</td>
</tr>
<tr class="odd">
<td>Pulse amplitude overshoot/undershoot</td>
<td>%</td>
<td>&lt; 5</td>
</tr>
<tr class="even">
<td>Input VSWR</td>
<td> </td>
<td>&lt; 1.2</td>
</tr>
<tr class="odd">
<td>Time with full reflection @ required output level – all phases</td>
<td>μs</td>
<td>50</td>
</tr>
<tr class="even">
<td>Resilience to fast load variations (from matched to full reflection)</td>
<td>μs</td>
<td>50</td>
</tr>
<tr class="odd">
<td>@ required output level – all phases</td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>Time with VSWR up to 2 @ required output level – all phases</td>
<td>s</td>
<td>&gt; 3600</td>
</tr>
<tr class="odd">
<td>Harmonics @ required output level</td>
<td>dBc</td>
<td>&lt; -30</td>
</tr>
<tr class="even">
<td>Spurious @ required output level</td>
<td>dBc</td>
<td>&lt; -60</td>
</tr>
<tr class="odd">
<td>Overall efficiency @ nominal output level</td>
<td>%</td>
<td>&gt; 50</td>
</tr>
<tr class="even">
<td>EMC compliance</td>
<td> </td>
<td>CISPR11, IEC61000-6-2</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Major RF amplifier parameters requirements.

 

**Table:** Major amplifier control requirements.

<table>
<caption>Table: Major amplifier control requirements.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>Main control requirements</th>
<th>Expected</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td colspan="2">Permanent and Slow transfer to</td>
</tr>
<tr class="even">
<td>Global transfer time</td>
<td>&lt; 1 sec</td>
</tr>
<tr class="odd">
<td>Transfer protocol</td>
<td>Ethercat &amp; Modbus TCP/IP</td>
</tr>
<tr class="even">
<td colspan="2">Fast acquisition buffering for post or online processing</td>
</tr>
<tr class="odd">
<td>Data of interest</td>
<td>Flags, RF powers, currents and voltages</td>
</tr>
<tr class="even">
<td>Embedded buffers storage</td>
<td>&gt; 10 sec</td>
</tr>
<tr class="odd">
<td>Quantity of embedded buffers</td>
<td>2 sets</td>
</tr>
<tr class="even">
<td>Granularity</td>
<td>&lt; 2 ms</td>
</tr>
<tr class="odd">
<td>Time stamp</td>
<td>For every buffered data</td>
</tr>
<tr class="even">
<td>Transfer protocol</td>
<td>Raw TCP</td>
</tr>
<tr class="odd">
<td colspan="2">Interlocks</td>
</tr>
<tr class="even">
<td>External interlock speed</td>
<td>&lt; 5μs</td>
</tr>
<tr class="odd">
<td>Internal interlocks</td>
<td>VSWR, overdrive, RF power, voltage and current, cooling temperature, water flow...</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Major amplifier control requirements.

 

**Table:** Major parameters requirements for the Injector amplifiers.

<table>
<caption>Table: Major parameters requirements for the Injector amplifiers.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Naming</td>
<td>Acronym</td>
<td>Nominal power</td>
<td>Required power</td>
<td>Output conn. size</td>
</tr>
<tr class="even">
<td>convention</td>
<td> </td>
<td>incl. losses (kW)</td>
<td>incl. margins (kW)</td>
<td>(EIA standard)</td>
</tr>
<tr class="odd">
<td>I1-RFQ</td>
<td>RFQ</td>
<td>111.5</td>
<td>167.3</td>
<td>6 ⅛″</td>
</tr>
<tr class="even">
<td>I1-ME1:RF-CAV-0
1</td>
<td>QWR1</td>
<td>2.3</td>
<td>3.4</td>
<td>1 ⅝″</td>
</tr>
<tr class="odd">
<td>I1-ME1:RF-CAV-0
2</td>
<td>QWR2</td>
<td>2.7</td>
<td>4.1</td>
<td>1 ⅝″</td>
</tr>
<tr class="even">
<td>I1-CH
1:RF-CAV-01</td>
<td>CH01</td>
<td>9.3</td>
<td>13.9</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>I1-CH
2:RF-CAV-01</td>
<td>CH02</td>
<td>13.8</td>
<td>20.7</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>I1-CH
3:RF-CAV-01</td>
<td>CH03</td>
<td>17.9</td>
<td>26.9</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>I1-CH
4:RF-CAV-01</td>
<td>CH04</td>
<td>20.7</td>
<td>31.0</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>I1-CH
5:RF-CAV-01</td>
<td>CH05</td>
<td>24.5</td>
<td>36.8</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>I1-CH
6:RF-CAV-01</td>
<td>CH06</td>
<td>33.8</td>
<td>50.7</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>I1-CH
7:RF-CAV-01</td>
<td>CH07</td>
<td>31.1</td>
<td>46.6</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>I1-ME3:RF-CAV-01</td>
<td>CHR1</td>
<td>8.2</td>
<td>12.3</td>
<td>1 ⅝″</td>
</tr>
<tr class="even">
<td>I1-CH
8:RF-CAV-01</td>
<td>CH08</td>
<td>41.1</td>
<td>61.7</td>
<td>4 ½″</td>
</tr>
<tr class="odd">
<td>I1-CH
9:RF-CAV-01</td>
<td>CH09</td>
<td>40.7</td>
<td>61.0</td>
<td>4 ½″</td>
</tr>
<tr class="even">
<td>I1-CH
10:RF-CAV-01</td>
<td>CH10</td>
<td>40.6</td>
<td>60.9</td>
<td>4 ½″</td>
</tr>
<tr class="odd">
<td>I1-CH
11:RF-CAV-01</td>
<td>CH11</td>
<td>40.3</td>
<td>60.5</td>
<td>4 ½″</td>
</tr>
<tr class="even">
<td>I1-CH
12:RF-CAV-01</td>
<td>CH12</td>
<td>39.8</td>
<td>59.7</td>
<td>4 ½″</td>
</tr>
<tr class="odd">
<td>I1-CH
13:RF-CAV-01</td>
<td>CH13</td>
<td>39.4</td>
<td>59.0</td>
<td>4 ½″</td>
</tr>
<tr class="even">
<td>I1-CH
14:RF-CAV-01</td>
<td>CH14</td>
<td>38.9</td>
<td>58.4</td>
<td>4 ½″</td>
</tr>
<tr class="odd">
<td>I1-CH
15:RF-CAV-01</td>
<td>CH15</td>
<td>38.8</td>
<td>58.2</td>
<td>4 ½″</td>
</tr>
<tr class="even">
<td>I1-ME2:RF-CAV-01</td>
<td>CHR2</td>
<td>TBD</td>
<td>TBD</td>
<td>TBD</td>
</tr>
</tbody>
</table>
<p><span id="tabrfsysinj-amp-reqs" class="anchor"></span> </p></td>
</tr>
</tbody>
</table>

Table: Major parameters requirements for the Injector amplifiers.

 

**Table:** Major parameters requirements for the amplifiers (Spokes
1--30).

<table>
<caption>Table: Major parameters requirements for the amplifiers (Spokes 1–30).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Naming</td>
<td>Acronym</td>
<td>Nominal power</td>
<td>Required power</td>
<td>Output conn. size</td>
</tr>
<tr class="even">
<td>Convention</td>
<td> </td>
<td>incl. losses (kW)</td>
<td>incl. margins (kW)</td>
<td>(EIA standard)</td>
</tr>
<tr class="odd">
<td>LB-ME3:RF-CAV-01</td>
<td>Spoke Reb. 1</td>
<td>3.847</td>
<td>5.77</td>
<td>1 ⅝″</td>
</tr>
<tr class="even">
<td>LB-ME3:RF-CAV-02</td>
<td>Spoke Reb. 2</td>
<td>3.85</td>
<td>5.77</td>
<td>1 ⅝″</td>
</tr>
<tr class="odd">
<td>LB-S00:RF-CAV-01</td>
<td>Spoke 1</td>
<td>8.44</td>
<td>12.66</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S01:RF-CAV-02</td>
<td>Spoke 2</td>
<td>8.50</td>
<td>12.75</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S01:RF-CAV-01</td>
<td>Spoke 3</td>
<td>8.57</td>
<td>12.85</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S02:RF-CAV-02</td>
<td>Spoke 4</td>
<td>8.63</td>
<td>12.95</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S02:RF-CAV-01</td>
<td>Spoke 5</td>
<td>8.72</td>
<td>13.08</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S03:RF-CAV-02</td>
<td>Spoke 6</td>
<td>8.80</td>
<td>13.19</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S03:RF-CAV-01</td>
<td>Spoke 7</td>
<td>8.92</td>
<td>13.37</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S04:RF-CAV-02</td>
<td>Spoke 8</td>
<td>9.00</td>
<td>13.50</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S04:RF-CAV-01</td>
<td>Spoke 9</td>
<td>9.16</td>
<td>13.74</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S05:RF-CAV-02</td>
<td>Spoke 10</td>
<td>9.25</td>
<td>13.88</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S05:RF-CAV-01</td>
<td>Spoke 11</td>
<td>9.47</td>
<td>14.21</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S06:RF-CAV-02</td>
<td>Spoke 12</td>
<td>9.58</td>
<td>14.37</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S06:RF-CAV-01</td>
<td>Spoke 13</td>
<td>9.89</td>
<td>14.84</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S07:RF-CAV-02</td>
<td>Spoke 14</td>
<td>10.02</td>
<td>15.03</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S07:RF-CAV-01</td>
<td>Spoke 15</td>
<td>10.46</td>
<td>15.70</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S08:RF-CAV-02</td>
<td>Spoke 16</td>
<td>10.62</td>
<td>15.92</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S08:RF-CAV-01</td>
<td>Spoke 17</td>
<td>11.29</td>
<td>16.93</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S09:RF-CAV-02</td>
<td>Spoke 18</td>
<td>11.47</td>
<td>17.21</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S09:RF-CAV-01</td>
<td>Spoke 19</td>
<td>12.54</td>
<td>18.81</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S10:RF-CAV-02</td>
<td>Spoke 20</td>
<td>12.77</td>
<td>19.16</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S10:RF-CAV-01</td>
<td>Spoke 21</td>
<td>13.85</td>
<td>20.78</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S11:RF-CAV-02</td>
<td>Spoke 22</td>
<td>14.10</td>
<td>21.15</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S11:RF-CAV-01</td>
<td>Spoke 23</td>
<td>14.39</td>
<td>21.58</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S12:RF-CAV-02</td>
<td>Spoke 24</td>
<td>14.58</td>
<td>21.87</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S12:RF-CAV-01</td>
<td>Spoke 25</td>
<td>14.77</td>
<td>22.16</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S13:RF-CAV-02</td>
<td>Spoke 26</td>
<td>14.91</td>
<td>22.36</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S13:RF-CAV-01</td>
<td>Spoke 27</td>
<td>15.05</td>
<td>22.58</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S14:RF-CAV-02</td>
<td>Spoke 28</td>
<td>15.15</td>
<td>22.72</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S14:RF-CAV-01</td>
<td>Spoke 29</td>
<td>15.25</td>
<td>22.88</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S15:RF-CAV-02</td>
<td>Spoke 30</td>
<td>15.31</td>
<td>22.97</td>
<td>3 ⅛″</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Major parameters requirements for the amplifiers (Spokes 1--30).

 

**Table:** Major parameters requirements for the amplifiers (Spokes
31--46).

<table>
<caption>Table: Major parameters requirements for the amplifiers (Spokes 31–46).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Naming</td>
<td>Acronym</td>
<td>Nominal power</td>
<td>Required power</td>
<td>Output conn.</td>
</tr>
<tr class="even">
<td>Convention</td>
<td> </td>
<td>incl. losses (kW)</td>
<td>incl. margins (kW)</td>
<td>size (EIA standard)</td>
</tr>
<tr class="odd">
<td>LB-S15:RF-CAV-01</td>
<td>Spoke 31</td>
<td>15.38</td>
<td>23.07</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S16:RF-CAV-02</td>
<td>Spoke 32</td>
<td>15.41</td>
<td>23.12</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S16:RF-CAV-01</td>
<td>Spoke 33</td>
<td>15.46</td>
<td>23.19</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S17:RF-CAV-02</td>
<td>Spoke 34</td>
<td>15.47</td>
<td>23.21</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S17:RF-CAV-01</td>
<td>Spoke 35</td>
<td>15.50</td>
<td>23.25</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S18:RF-CAV-02</td>
<td>Spoke 36</td>
<td>15.50</td>
<td>23.25</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S18:RF-CAV-01</td>
<td>Spoke 37</td>
<td>15.51</td>
<td>23.27</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S19:RF-CAV-02</td>
<td>Spoke 38</td>
<td>15.50</td>
<td>23.25</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S19:RF-CAV-01</td>
<td>Spoke 39</td>
<td>15.50</td>
<td>23.25</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S20:RF-CAV-02</td>
<td>Spoke 40</td>
<td>15.49</td>
<td>23.23</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S20:RF-CAV-01</td>
<td>Spoke 41</td>
<td>15.48</td>
<td>23.22</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S21:RF-CAV-02</td>
<td>Spoke 42</td>
<td>15.46</td>
<td>23.19</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S21:RF-CAV-01</td>
<td>Spoke 43</td>
<td>15.45</td>
<td>23.17</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S22:RF-CAV-02</td>
<td>Spoke 44</td>
<td>15.42</td>
<td>23.13</td>
<td>3 ⅛″</td>
</tr>
<tr class="odd">
<td>LB-S22:RF-CAV-01</td>
<td>Spoke 45</td>
<td>15.41</td>
<td>23.11</td>
<td>3 ⅛″</td>
</tr>
<tr class="even">
<td>LB-S23:RF-CAV-02</td>
<td>Spoke 46</td>
<td>15.38</td>
<td>23.07</td>
<td>3 ⅛″</td>
</tr>
</tbody>
</table>
<p><span id="tabrfsyslinac-amp-reqs-b" class="anchor"></span> </p></td>
</tr>
</tbody>
</table>

Table: Major parameters requirements for the amplifiers (Spokes 31--46).

 

**Table:** Results overview of the RF characterisation of the RFQ
amplifier.

<table>
<caption>Table: Results overview of the RF characterisation of the RFQ amplifier.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>PARAMETERS</td>
<td>UNITS</td>
<td> 
RESULTS</td>
</tr>
<tr class="even">
<td>Max output power on matched load</td>
<td>kW</td>
<td>192</td>
</tr>
<tr class="odd">
<td>Input return loss over the full power range</td>
<td>dB</td>
<td>-20</td>
</tr>
<tr class="even">
<td>Wall plug efficiency</td>
<td>%</td>
<td>73.6 (max level) / 61.5 (operating level)</td>
</tr>
<tr class="odd">
<td>AC/DC converters efficiency</td>
<td>%</td>
<td>90 – 92</td>
</tr>
<tr class="even">
<td>Harmonics level over the full power range</td>
<td>dBc</td>
<td>&lt; -45</td>
</tr>
<tr class="odd">
<td>Gain variation within ±1MHz BW</td>
<td>dB</td>
<td>1.4 (100kW), 0.95 (140kW)</td>
</tr>
<tr class="even">
<td>Bandwidth (@ -1 dB)</td>
<td>MHz</td>
<td>&gt; ±1</td>
</tr>
<tr class="odd">
<td>Maximum gain</td>
<td>dB</td>
<td>85</td>
</tr>
<tr class="even">
<td>Large dynamic range (up to required level)</td>
<td>dB</td>
<td>&gt; 20</td>
</tr>
<tr class="odd">
<td>Gain linearity over large dynamic range</td>
<td>dB</td>
<td>8</td>
</tr>
<tr class="even">
<td>Phase linearity over large dynamic range</td>
<td>deg</td>
<td>20.5</td>
</tr>
<tr class="odd">
<td>Reduced dynamic range (around operating level)</td>
<td>dB</td>
<td>±1.22</td>
</tr>
<tr class="even">
<td>Gain linearity over reduced dynamic range</td>
<td>dB</td>
<td>0.46</td>
</tr>
<tr class="odd">
<td>Phase linearity over reduced dynamic range</td>
<td>deg</td>
<td>14.5</td>
</tr>
<tr class="even">
<td>1 dB compression point</td>
<td>kW</td>
<td>70</td>
</tr>
<tr class="odd">
<td>Warm up time at 120kW</td>
<td>sec</td>
<td>60</td>
</tr>
<tr class="even">
<td>Gain variations during warm up</td>
<td>dB</td>
<td>0.15</td>
</tr>
<tr class="odd">
<td>Phase variations during warm up</td>
<td>deg</td>
<td>0.57</td>
</tr>
<tr class="even">
<td>Gain stability over 1 s after warm up</td>
<td>dB</td>
<td>&lt; 0.02</td>
</tr>
<tr class="odd">
<td>Phase stability over 1 s after warm up</td>
<td>deg</td>
<td>&lt; 0.15</td>
</tr>
<tr class="even">
<td>Pulse rise time</td>
<td>µs</td>
<td>1.6</td>
</tr>
<tr class="odd">
<td>Pulse amplitude overshoot/undershoot</td>
<td>%</td>
<td>&lt; 5</td>
</tr>
<tr class="even">
<td>Group delay</td>
<td>ns</td>
<td>87</td>
</tr>
<tr class="odd">
<td>External combiner return loss</td>
<td>dB</td>
<td>&lt; -25</td>
</tr>
<tr class="even">
<td>External combiner directivity</td>
<td>dB</td>
<td>&gt; 25</td>
</tr>
<tr class="odd">
<td>Max CW forward power under VSWR = 2</td>
<td>kW</td>
<td>143</td>
</tr>
<tr class="even">
<td>Max CW forward power under VSWR = 3</td>
<td>kW</td>
<td>124</td>
</tr>
<tr class="odd">
<td>Max pulsed forward power under full reflection</td>
<td>kW</td>
<td>92 (10 ms, 40 %)</td>
</tr>
<tr class="even">
<td>Delivery of required forward power up to VSWR</td>
<td> </td>
<td>2</td>
</tr>
<tr class="odd">
<td>RF leak level at 120kW</td>
<td>dBμV / m</td>
<td>&lt; 104.5</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Results overview of the RF characterisation of the RFQ amplifier.

 

**Table:** SSAs repartition per cabinet for the Injector prototyping
activity.

<table>
<caption>Table: SSAs repartition per cabinet for the Injector prototyping activity.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Acronym</td>
<td>QWR 1</td>
<td>QWR 2</td>
<td>CH01</td>
<td>CH02</td>
<td>CH03</td>
<td>CH04</td>
<td>CH05</td>
<td>CH06</td>
<td>CH07</td>
</tr>
<tr class="even">
<td>Naming convention</td>
<td>I1-ME1:RF-CAV-0
1</td>
<td>I1-ME1:RF-CAV-0
2</td>
<td>I1-CH
1:RF-CAV-01</td>
<td>I1-CH
2:RF-CAV-01</td>
<td>I1-CH
3:RF-CAV-01</td>
<td>I1-CH
4:RF-CAV-01</td>
<td>I1-CH
5:RF-CAV-01</td>
<td>I1-CH
6:RF-CAV-01</td>
<td>I1-CH
7:RF-CAV-01</td>
</tr>
<tr class="odd">
<td>Cabinet 1</td>
<td>6kW</td>
<td>6kW</td>
<td>24kW</td>
<td>24kW</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
</tr>
<tr class="even">
<td>Cabinet 2</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>36kW</td>
<td>36kW</td>
<td>–</td>
<td>–</td>
<td>–</td>
</tr>
<tr class="odd">
<td>Cabinet 3</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>48kW</td>
<td>–</td>
<td>48kW</td>
</tr>
<tr class="even">
<td>Cabinet 4</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>–</td>
<td>60kW</td>
<td>–</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: SSAs repartition per cabinet for the Injector prototyping
activity.

 

**Table:** Estimation of the SSA cabinet quantity for the MINERVA linac.

<table>
<caption>Table: Estimation of the SSA cabinet quantity for the MINERVA linac.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Acronym</td>
<td>RFQ</td>
<td>QWR1 ⇨ CH07</td>
<td>CHR1 ⇨ CH15</td>
<td>Spoke 1 ⇨ Spoke 46</td>
</tr>
<tr class="even">
<td>Naming</td>
<td>I1-RFQ</td>
<td>I1-ME1:RF-CAV-0
1 ⇨</td>
<td>I1-ME3:RF-CAV-01 ⇨</td>
<td>LB-S00:RF-CAV-01 ⇨</td>
</tr>
<tr class="odd">
<td>convention</td>
<td> </td>
<td>I1-CH
7:RF-CAV-01</td>
<td>I1-CH
15:RF-CAV-01</td>
<td>LB-S23:RF-CAV-02</td>
</tr>
<tr class="even">
<td>No. of cabinet</td>
<td>2</td>
<td>4</td>
<td>8</td>
<td>15</td>
</tr>
<tr class="odd">
<td>No. of output</td>
<td>0.5</td>
<td>4 (1<sup>st</sup> cabinet) 1 (4<sup>th</sup> cabinet)</td>
<td>2 (1<sup>st</sup> cabinet)</td>
<td>4</td>
</tr>
<tr class="even">
<td>per cabinet</td>
<td> </td>
<td>2 (others)</td>
<td>1 (others)</td>
<td> </td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Estimation of the SSA cabinet quantity for the MINERVA linac.

 

**Table:** Estimation of the water cooling requirements for the MINERVA
SSAs.

<table>
<caption>Table: Estimation of the water cooling requirements for the MINERVA SSAs.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td> </td>
<td>SSAs for Injector</td>
<td>SSAs for</td>
</tr>
<tr class="even">
<td>Medium</td>
<td> </td>
<td colspan="2">Normal water</td>
</tr>
<tr class="odd">
<td>Particle filtering size</td>
<td>μm</td>
<td colspan="2">&lt; 500</td>
</tr>
<tr class="even">
<td>Minimum flow rate</td>
<td>L min⁻¹</td>
<td>2105</td>
<td>1400</td>
</tr>
<tr class="odd">
<td>Input absolute pressure</td>
<td>bar</td>
<td colspan="2">&lt; 6</td>
</tr>
<tr class="even">
<td>Output absolute pressure</td>
<td>bar</td>
<td colspan="2">&gt; 1</td>
</tr>
<tr class="odd">
<td>Pressure drop</td>
<td>bar</td>
<td colspan="2">&lt; 4</td>
</tr>
<tr class="even">
<td>Input temperature</td>
<td>℃</td>
<td colspan="2">&lt; 25</td>
</tr>
<tr class="odd">
<td>Input temp. stability</td>
<td>℃</td>
<td colspan="2">&lt; ± 0.5</td>
</tr>
<tr class="even">
<td>Output temperature</td>
<td>℃</td>
<td colspan="2">&lt; 35</td>
</tr>
<tr class="odd">
<td>Delta temperature</td>
<td>℃</td>
<td colspan="2">&lt; 10</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Estimation of the water cooling requirements for the MINERVA
SSAs.

 

**Table:** Characteristics of some rectangular waveguides.

<table>
<caption>Table: Characteristics of some rectangular waveguides.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Waveguide EIA name</td>
<td>Recommended frequency band of operation (GHz)</td>
<td>Inner dimensions of waveguide opening (mm)</td>
<td>TE<sub>10</sub>-mode cut-off frequency (GHz)</td>
<td>TE<sub>20</sub>-mode cut-off frequency (GHz)</td>
</tr>
<tr class="even">
<td>WR2300</td>
<td>0.32 — 0.45</td>
<td>584.2 × 292.1</td>
<td>0.26</td>
<td>0.51</td>
</tr>
<tr class="odd">
<td>WR1150</td>
<td>0.63 — 0.97</td>
<td>292.1 × 146.05</td>
<td>0.51</td>
<td>1.03</td>
</tr>
<tr class="even">
<td>WR340</td>
<td>2.2 — 3.3</td>
<td>86.36 × 43.18</td>
<td>1.7</td>
<td>3.5</td>
</tr>
<tr class="odd">
<td>WR75</td>
<td>10 — 15</td>
<td>19.05 × 9.52</td>
<td>7.9</td>
<td>15.7</td>
</tr>
<tr class="even">
<td>WR10</td>
<td>70 — 110</td>
<td>2.54 × 1.27</td>
<td>59</td>
<td>118</td>
</tr>
<tr class="odd">
<td>WR3</td>
<td>220 — 330</td>
<td>0.864 × 0.432</td>
<td>173</td>
<td>346</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Characteristics of some rectangular waveguides.

 

**Table:** Dimensions and cut-off frequencies of most used rigid coaxial
waveguides.

<table>
<caption>Table: Dimensions and cut-off frequencies of most used rigid coaxial waveguides.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Coaxial EIA name</td>
<td colspan="2">Outer conductor</td>
<td colspan="2">Inner conductor</td>
<td>Cut-off frequency TE11-mode (GHz)</td>
</tr>
<tr class="even">
<td> </td>
<td>Outer diameter (mm)</td>
<td>Inner diameter (mm) = D₁</td>
<td>Outer diameter (mm) = D₂</td>
<td>Inner diameter (mm)</td>
<td> </td>
</tr>
<tr class="odd">
<td>⅞″</td>
<td>22.2</td>
<td>20</td>
<td>8.7</td>
<td>7.4</td>
<td>6.3</td>
</tr>
<tr class="even">
<td>1 ⅝″</td>
<td>41.3</td>
<td>38.8</td>
<td>16.9</td>
<td>15</td>
<td>3.2</td>
</tr>
<tr class="odd">
<td>3 ⅛″</td>
<td>79.4</td>
<td>76.9</td>
<td>33.4</td>
<td>31.3</td>
<td>1.6</td>
</tr>
<tr class="even">
<td>4 ½″</td>
<td>106</td>
<td>103</td>
<td>44.8</td>
<td>42.8</td>
<td>1.2</td>
</tr>
<tr class="odd">
<td>6 ⅛″</td>
<td>155.6</td>
<td>151.9</td>
<td>66</td>
<td>64</td>
<td>0.83</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Dimensions and cut-off frequencies of most used rigid coaxial
waveguides.

 

**Table:** Maximum power handling of coaxial waveguides.

<table>
<caption>Table: Maximum power handling of coaxial waveguides.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td colspan="5">CW power handling capability (kW)</td>
</tr>
<tr class="even">
<td>f (MHz)</td>
<td>⅞″</td>
<td>1 ⅝″</td>
<td>3 ⅛″</td>
<td>4 ½″</td>
<td>6 ⅛″</td>
</tr>
<tr class="odd">
<td>176.1</td>
<td>5.7</td>
<td>15</td>
<td>48</td>
<td>80</td>
<td>159</td>
</tr>
<tr class="even">
<td>352.2</td>
<td>4.1</td>
<td>11</td>
<td>34</td>
<td>57</td>
<td>111</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Maximum power handling of coaxial waveguides.

 

**Table:** Typical attenuations of coaxial waveguides.

<table>
<caption>Table: Typical attenuations of coaxial waveguides.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td colspan="5">Attenuation (dB/100m)</td>
</tr>
<tr class="even">
<td>f (MHz)</td>
<td>⅞″</td>
<td>1 ⅝″</td>
<td>3 ⅛″</td>
<td>4 ½″</td>
<td>6 ⅛″</td>
</tr>
<tr class="odd">
<td>176.1</td>
<td>1.61</td>
<td>0.99</td>
<td>0.49</td>
<td>0.37</td>
<td>0.25</td>
</tr>
<tr class="even">
<td>352.2</td>
<td>2.27</td>
<td>1.40</td>
<td>0.69</td>
<td>0.52</td>
<td>0.35</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Typical attenuations of coaxial waveguides.

 

**Table:** Configuration of the MINERVA Tx lines.

<table>
<caption>Table: Configuration of the MINERVA Tx lines.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Cavity name</td>
<td>Naming convention</td>
<td>Size of Tx line</td>
<td>Interface with SSA</td>
<td>Interface with power coupler</td>
</tr>
<tr class="even">
<td>RFQ</td>
<td>I1-RFQ</td>
<td>6 ⅛″</td>
<td>6 ⅛″ EIA</td>
<td>6 ⅛″ EIA</td>
</tr>
<tr class="odd">
<td>QWR1 &amp; 2</td>
<td>I1-ME1:RF-CAV-0
1 &amp; I1-ME1:RF-CAV-0
2</td>
<td>⅞″ (flexible)</td>
<td>1 ⅝″ EIA</td>
<td>1 ⅝″ EIA</td>
</tr>
<tr class="even">
<td>CH01 to 07</td>
<td>I1-CH
1:RF-CAV-01 to I1-CH
7:RF-CAV-01</td>
<td>3 ⅛″</td>
<td>4 ½″ EIA</td>
<td>6 ⅛″ EIA</td>
</tr>
<tr class="odd">
<td>CHR1</td>
<td>I1-ME3:RF-CAV-01</td>
<td>1 ⅝″</td>
<td>1 ⅝″ EIA</td>
<td>1 ⅝″ EIA</td>
</tr>
<tr class="even">
<td>CH08 to 15</td>
<td>I1-CH
8:RF-CAV-01 to I1-CH
15:RF-CAV-01</td>
<td>4 ½″</td>
<td>4 ½″ EIA</td>
<td>6 ⅛″ EIA</td>
</tr>
<tr class="odd">
<td>CHR2</td>
<td>I1-ME2:RF-CAV-01</td>
<td>TBD</td>
<td>TBD</td>
<td>6 ⅛″ EIA</td>
</tr>
<tr class="even">
<td>Spoke Reb. 1 &amp; 2</td>
<td>I1-ME3:RF-CAV-01 &amp; I1-ME2:RF-CAV-01</td>
<td>1 ⅝″ EIA</td>
<td>1 ⅝″ EIA</td>
<td>3 ⅛″ EIA</td>
</tr>
<tr class="odd">
<td>Spoke 1 to Spoke 46</td>
<td>LB-S00:RF-CAV-01 to LB-S23:RF-CAV-02</td>
<td>3 ⅛″ EIA</td>
<td>3 ⅛″ EIA</td>
<td>3 ⅛″ EIA</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Configuration of the MINERVA Tx lines.

 

**Table:** Transmission line ohmic losses per meter for the Injector
amplifiers.

<table>
<caption>Table: Transmission line ohmic losses per meter for the Injector amplifiers.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Naming</td>
<td>Acronym</td>
<td>Tx line losses per meter</td>
</tr>
<tr class="even">
<td>I1-RFQ</td>
<td>RFQ</td>
<td>71.89</td>
</tr>
<tr class="odd">
<td>I1-ME1:RF-CAV-0
1</td>
<td>QWR1</td>
<td>5.92</td>
</tr>
<tr class="even">
<td>I1-ME1:RF-CAV-0
2</td>
<td>QWR2</td>
<td>7.10</td>
</tr>
<tr class="odd">
<td>I1-CH
1:RF-CAV-01</td>
<td>CH01</td>
<td>11.96</td>
</tr>
<tr class="even">
<td>I1-CH
2:RF-CAV-01</td>
<td>CH02</td>
<td>17.75</td>
</tr>
<tr class="odd">
<td>I1-CH
3:RF-CAV-01</td>
<td>CH03</td>
<td>23.07</td>
</tr>
<tr class="even">
<td>I1-CH
4:RF-CAV-01</td>
<td>CH04</td>
<td>26.64</td>
</tr>
<tr class="odd">
<td>I1-CH
5:RF-CAV-01</td>
<td>CH05</td>
<td>31.61</td>
</tr>
<tr class="even">
<td>I1-CH
6:RF-CAV-01</td>
<td>CH06</td>
<td>43.59</td>
</tr>
<tr class="odd">
<td>I1-CH
7:RF-CAV-01</td>
<td>CH07</td>
<td>40.01</td>
</tr>
<tr class="even">
<td>I1-ME3:RF-CAV-01</td>
<td>CHR1</td>
<td>21.31</td>
</tr>
<tr class="odd">
<td>I1-CH
8:RF-CAV-01</td>
<td>CH08</td>
<td>34.08</td>
</tr>
<tr class="even">
<td>I1-CH
9:RF-CAV-01</td>
<td>CH09</td>
<td>33.72</td>
</tr>
<tr class="odd">
<td>I1-CH
10:RF-CAV-01</td>
<td>CH10</td>
<td>33.65</td>
</tr>
<tr class="even">
<td>I1-CH
11:RF-CAV-01</td>
<td>CH11</td>
<td>33.41</td>
</tr>
<tr class="odd">
<td>I1-CH
12:RF-CAV-01</td>
<td>CH12</td>
<td>32.96</td>
</tr>
<tr class="even">
<td>I1-CH
13:RF-CAV-01</td>
<td>CH13</td>
<td>32.61</td>
</tr>
<tr class="odd">
<td>I1-CH
14:RF-CAV-01</td>
<td>CH14</td>
<td>32.27</td>
</tr>
<tr class="even">
<td>I1-CH
15:RF-CAV-01</td>
<td>CH15</td>
<td>32.16</td>
</tr>
<tr class="odd">
<td>I1-ME2:RF-CAV-01</td>
<td>CHR2</td>
<td>TBD</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Transmission line ohmic losses per meter for the Injector
amplifiers.

 

**Table:** Transmission line ohmic losses per meter for the amplifiers
(Spoke rebunchers and spokes 1--22).

<table>
<caption>Table: Transmission line ohmic losses per meter for the amplifiers (Spoke rebunchers and spokes 1–22).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Naming</td>
<td>Acronym</td>
<td>Tx line losses per meter</td>
</tr>
<tr class="even">
<td>LB-ME3:RF-CAV-01</td>
<td>Spoke Reb. 1</td>
<td>12.38</td>
</tr>
<tr class="odd">
<td>LB-ME3:RF-CAV-02</td>
<td>Spoke Reb. 2</td>
<td>12.38</td>
</tr>
<tr class="even">
<td>LB-S00:RF-CAV-01</td>
<td>Spoke 1</td>
<td>13.40</td>
</tr>
<tr class="odd">
<td>LB-S01:RF-CAV-02</td>
<td>Spoke 2</td>
<td>13.49</td>
</tr>
<tr class="even">
<td>LB-S01:RF-CAV-01</td>
<td>Spoke 3</td>
<td>13.60</td>
</tr>
<tr class="odd">
<td>LB-S02:RF-CAV-02</td>
<td>Spoke 4</td>
<td>13.70</td>
</tr>
<tr class="even">
<td>LB-S02:RF-CAV-01</td>
<td>Spoke 5</td>
<td>13.85</td>
</tr>
<tr class="odd">
<td>LB-S03:RF-CAV-02</td>
<td>Spoke 6</td>
<td>13.96</td>
</tr>
<tr class="even">
<td>LB-S03:RF-CAV-01</td>
<td>Spoke 7</td>
<td>14.15</td>
</tr>
<tr class="odd">
<td>LB-S04:RF-CAV-02</td>
<td>Spoke 8</td>
<td>14.28</td>
</tr>
<tr class="even">
<td>LB-S04:RF-CAV-01</td>
<td>Spoke 9</td>
<td>14.54</td>
</tr>
<tr class="odd">
<td>LB-S05:RF-CAV-02</td>
<td>Spoke 10</td>
<td>14.69</td>
</tr>
<tr class="even">
<td>LB-S05:RF-CAV-01</td>
<td>Spoke 11</td>
<td>15.04</td>
</tr>
<tr class="odd">
<td>LB-S06:RF-CAV-02</td>
<td>Spoke 12</td>
<td>15.21</td>
</tr>
<tr class="even">
<td>LB-S06:RF-CAV-01</td>
<td>Spoke 13</td>
<td>15.70</td>
</tr>
<tr class="odd">
<td>LB-S07:RF-CAV-02</td>
<td>Spoke 14</td>
<td>15.90</td>
</tr>
<tr class="even">
<td>LB-S07:RF-CAV-01</td>
<td>Spoke 15</td>
<td>16.61</td>
</tr>
<tr class="odd">
<td>LB-S08:RF-CAV-02</td>
<td>Spoke 16</td>
<td>16.85</td>
</tr>
<tr class="even">
<td>LB-S08:RF-CAV-01</td>
<td>Spoke 17</td>
<td>17.92</td>
</tr>
<tr class="odd">
<td>LB-S09:RF-CAV-02</td>
<td>Spoke 18</td>
<td>18.21</td>
</tr>
<tr class="even">
<td>LB-S09:RF-CAV-01</td>
<td>Spoke 19</td>
<td>19.90</td>
</tr>
<tr class="odd">
<td>LB-S10:RF-CAV-02</td>
<td>Spoke 20</td>
<td>20.27</td>
</tr>
<tr class="even">
<td>LB-S10:RF-CAV-01</td>
<td>Spoke 21</td>
<td>21.99</td>
</tr>
<tr class="odd">
<td>LB-S11:RF-CAV-02</td>
<td>Spoke 22</td>
<td>22.38</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Transmission line ohmic losses per meter for the amplifiers
(Spoke rebunchers and spokes 1--22).

 

**Table:** Transmission line ohmic losses per meter for the amplifiers
(Spokes 23--46).

<table>
<caption>Table: Transmission line ohmic losses per meter for the amplifiers (Spokes 23–46).</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Naming</td>
<td>Acronym</td>
<td>Tx line losses per meter</td>
</tr>
<tr class="even">
<td>LB-S11:RF-CAV-01</td>
<td>Spoke 23</td>
<td>22.84</td>
</tr>
<tr class="odd">
<td>LB-S12:RF-CAV-02</td>
<td>Spoke 24</td>
<td>23.14</td>
</tr>
<tr class="even">
<td>LB-S12:RF-CAV-01</td>
<td>Spoke 25</td>
<td>23.45</td>
</tr>
<tr class="odd">
<td>LB-S13:RF-CAV-02</td>
<td>Spoke 26</td>
<td>23.67</td>
</tr>
<tr class="even">
<td>LB-S13:RF-CAV-01</td>
<td>Spoke 27</td>
<td>23.90</td>
</tr>
<tr class="odd">
<td>LB-S14:RF-CAV-02</td>
<td>Spoke 28</td>
<td>24.05</td>
</tr>
<tr class="even">
<td>LB-S14:RF-CAV-01</td>
<td>Spoke 29</td>
<td>24.21</td>
</tr>
<tr class="odd">
<td>LB-S15:RF-CAV-02</td>
<td>Spoke 30</td>
<td>24.31</td>
</tr>
<tr class="even">
<td>LB-S15:RF-CAV-01</td>
<td>Spoke 31</td>
<td>24.42</td>
</tr>
<tr class="odd">
<td>LB-S16:RF-CAV-02</td>
<td>Spoke 32</td>
<td>24.47</td>
</tr>
<tr class="even">
<td>LB-S16:RF-CAV-01</td>
<td>Spoke 33</td>
<td>24.54</td>
</tr>
<tr class="odd">
<td>LB-S17:RF-CAV-02</td>
<td>Spoke 34</td>
<td>24.56</td>
</tr>
<tr class="even">
<td>LB-S17:RF-CAV-01</td>
<td>Spoke 35</td>
<td>24.60</td>
</tr>
<tr class="odd">
<td>LB-S18:RF-CAV-02</td>
<td>Spoke 36</td>
<td>24.61</td>
</tr>
<tr class="even">
<td>LB-S18:RF-CAV-01</td>
<td>Spoke 37</td>
<td>24.62</td>
</tr>
<tr class="odd">
<td>LB-S19:RF-CAV-02</td>
<td>Spoke 38</td>
<td>24.61</td>
</tr>
<tr class="even">
<td>LB-S19:RF-CAV-01</td>
<td>Spoke 39</td>
<td>24.61</td>
</tr>
<tr class="odd">
<td>LB-S20:RF-CAV-02</td>
<td>Spoke 40</td>
<td>24.59</td>
</tr>
<tr class="even">
<td>LB-S20:RF-CAV-01</td>
<td>Spoke 41</td>
<td>24.58</td>
</tr>
<tr class="odd">
<td>LB-S21:RF-CAV-02</td>
<td>Spoke 42</td>
<td>24.54</td>
</tr>
<tr class="even">
<td>LB-S21:RF-CAV-01</td>
<td>Spoke 43</td>
<td>24.52</td>
</tr>
<tr class="odd">
<td>LB-S22:RF-CAV-02</td>
<td>Spoke 44</td>
<td>24.48</td>
</tr>
<tr class="even">
<td>LB-S22:RF-CAV-01</td>
<td>Spoke 45</td>
<td>24.46</td>
</tr>
<tr class="odd">
<td>LB-S23:RF-CAV-02</td>
<td>Spoke 46</td>
<td>24.41</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Transmission line ohmic losses per meter for the amplifiers
(Spokes 23--46).

 

**Table:** General requirements for the isolators.

<table>
<caption>Table: General requirements for the isolators.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Component</td>
<td>Interface (EIA)</td>
<td>Quantity</td>
<td>Power range (kW)</td>
<td>Required power (kW)</td>
</tr>
<tr class="even">
<td>Circulator</td>
<td>1 ⅝″</td>
<td>20</td>
<td>7.3 – 10.8</td>
<td>15</td>
</tr>
<tr class="odd">
<td> —-</td>
<td>3 ⅛″</td>
<td>43</td>
<td>11.1 – 21.6</td>
<td>25</td>
</tr>
<tr class="even">
<td>Load</td>
<td>1 ⅝″</td>
<td>20</td>
<td>7.3 – 10.8</td>
<td>15</td>
</tr>
<tr class="odd">
<td> —-</td>
<td>3 ⅛″</td>
<td>43</td>
<td>11.1 – 21.6</td>
<td>25</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: General requirements for the isolators.

 

**Table:** Specific requirements for the circulators.

<table>
<caption>Table: Specific requirements for the circulators.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Parameter</td>
<td>Requirement</td>
</tr>
<tr class="even">
<td>Centre frequency</td>
<td>352.2MHz</td>
</tr>
<tr class="odd">
<td>Bandwidth</td>
<td>±1MHz</td>
</tr>
<tr class="even">
<td>CW forward RF power</td>
<td>As per “Required power including margin” given in Table <a href="#tabrfsysinj-amp-reqs">[*]</a>, Sect. <a href="#ssect:RFsys:specific:injector">[*]</a>.</td>
</tr>
<tr class="odd">
<td>CW reverse RF power</td>
<td>full reflection, any phase</td>
</tr>
<tr class="even">
<td>Pulse repetition rate</td>
<td>1–250Hz</td>
</tr>
<tr class="odd">
<td>Pulse Length</td>
<td>100μs to CW</td>
</tr>
<tr class="even">
<td>Insertion Loss</td>
<td>&lt; 0.2dB</td>
</tr>
<tr class="odd">
<td>Isolation at f0</td>
<td>&gt; 26dB</td>
</tr>
<tr class="even">
<td>Isolation in BW</td>
<td>&gt; 20dB</td>
</tr>
<tr class="odd">
<td>Return loss at f0</td>
<td>&gt; 26dB</td>
</tr>
<tr class="even">
<td>Return loss in BW</td>
<td>&gt; 20dB</td>
</tr>
<tr class="odd">
<td>Water Temperature Optimum</td>
<td>20℃</td>
</tr>
<tr class="even">
<td>Water temperature range</td>
<td>18–22℃</td>
</tr>
<tr class="odd">
<td>Arc detection</td>
<td>Integrated</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Specific requirements for the circulators.

 

**Table:** The assumptions and data used to estimate the heat loads at
different temperature levels in Table [\[\*\]](#tabhldistro).

<table>
<caption>Table: The assumptions and data used to estimate the heat loads at different temperature levels in Table [*].</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Accelerator</td>
<td>32 cryomodules</td>
</tr>
<tr class="even">
<td> </td>
<td>32 valve boxes</td>
</tr>
<tr class="odd">
<td> </td>
<td>Q̇<sub>2K</sub> = 1 W m<sup>-1</sup> to 5 W m<sup>-1</sup><img src="media/rId68.png" style="width:3.91304in;height:1.37458in" alt="Table 5, from D-MYRTE-2.6, rendered as an image" /> </td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7208, img55</strong></td>
</tr>
<tr class="odd">
<td>Cavity</td>
<td><strong>TBD: inline6910, img2</strong></td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7212, img57</strong> per cavity</td>
</tr>
<tr class="odd">
<td> </td>
<td><strong>TBD: inline7214, img58</strong> cavities</td>
</tr>
<tr class="even">
<td>RF coupler</td>
<td><strong>TBD: inline7216, img59</strong> per coupler</td>
</tr>
<tr class="odd">
<td> </td>
<td><strong>TBD: inline7218, img60</strong> per coupler</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7220, img61</strong> per coupler</td>
</tr>
<tr class="odd">
<td> </td>
<td><strong>TBD: inline7222, img62</strong> couplers per cryomodule</td>
</tr>
<tr class="even">
<td>Thermal shield</td>
<td><strong>TBD: inline7224, img63</strong> per shield</td>
</tr>
<tr class="odd">
<td> </td>
<td><strong>TBD: inline7226, img64</strong> shield per cryomodule</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: The assumptions and data used to estimate the heat loads at
different temperature levels in Table \[\*\].

 

**Table:** The heat load distribution estimated for the MINERVA
accelerator.

<table>
<caption>Table: The heat load distribution estimated for the MINERVA accelerator.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Component</td>
<td><strong>TBD: inline7228, img65</strong></td>
<td>Heat load<strong>TBD: inline7230, img66</strong></td>
<td>Carnot factor</td>
<td>Heat load <strong>TBD: inline7232, img67</strong></td>
</tr>
<tr class="even">
<td> </td>
<td>K</td>
<td>W</td>
<td>-</td>
<td>W</td>
</tr>
<tr class="odd">
<td>Cavity, coupler,<br />
CM static</td>
<td>2</td>
<td>994</td>
<td>2.27</td>
<td>2255</td>
</tr>
<tr class="even">
<td>Coupler</td>
<td>10</td>
<td>186</td>
<td>0.44</td>
<td>82</td>
</tr>
<tr class="odd">
<td> </td>
<td>60</td>
<td>558</td>
<td>0.06</td>
<td>34</td>
</tr>
<tr class="even">
<td>Thermal shield</td>
<td>40</td>
<td>2720</td>
<td>0.10</td>
<td>272</td>
</tr>
<tr class="odd">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td>2640</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
<td>(<strong>TBD: inline7234, img68</strong> over-cap)</td>
<td>3960</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: The heat load distribution estimated for the MINERVA accelerator.

 

**Table:** The assumptions and data used to estimate the heat loads at
different temperature levels in Table [\[\*\]](#tabhldistro).

<table>
<caption>Table: The assumptions and data used to estimate the heat loads at different temperature levels in Table [*].</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Accelerator</td>
<td>32 cryomodules</td>
</tr>
<tr class="even">
<td> </td>
<td>32 valve boxes</td>
</tr>
<tr class="odd">
<td> </td>
<td>Q̇<sub>2K</sub> = 1 W m<sup>-1</sup> to 5 W m<sup>-1</sup><img src="media/rId68.png" style="width:3.91304in;height:1.37458in" alt="Table 5, from D-MYRTE-2.6, rendered as an image" /> </td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7208, img55</strong></td>
</tr>
<tr class="odd">
<td>Cavity</td>
<td><strong>TBD: inline6910, img2</strong></td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7212, img57</strong> per cavity</td>
</tr>
<tr class="odd">
<td> </td>
<td><strong>TBD: inline7214, img58</strong> cavities</td>
</tr>
<tr class="even">
<td>RF coupler</td>
<td><strong>TBD: inline7216, img59</strong> per coupler</td>
</tr>
<tr class="odd">
<td> </td>
<td><strong>TBD: inline7218, img60</strong> per coupler</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7220, img61</strong> per coupler</td>
</tr>
<tr class="odd">
<td> </td>
<td><strong>TBD: inline7222, img62</strong> couplers per cryomodule</td>
</tr>
<tr class="even">
<td>Thermal shield</td>
<td><strong>TBD: inline7224, img63</strong> per shield</td>
</tr>
<tr class="odd">
<td> </td>
<td><strong>TBD: inline7226, img64</strong> shield per cryomodule</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: The assumptions and data used to estimate the heat loads at
different temperature levels in Table \[\*\].

 

**Table:** The heat load distribution estimated for the MINERVA
accelerator.

<table>
<caption>Table: The heat load distribution estimated for the MINERVA accelerator.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Component</td>
<td><strong>TBD: inline7228, img65</strong></td>
<td>Heat load<strong>TBD: inline7230, img66</strong></td>
<td>Carnot factor</td>
<td>Heat load <strong>TBD: inline7232, img67</strong></td>
</tr>
<tr class="even">
<td> </td>
<td>K</td>
<td>W</td>
<td>-</td>
<td>W</td>
</tr>
<tr class="odd">
<td>Cavity, coupler,<br />
CM static</td>
<td>2</td>
<td>994</td>
<td>2.27</td>
<td>2255</td>
</tr>
<tr class="even">
<td>Coupler</td>
<td>10</td>
<td>186</td>
<td>0.44</td>
<td>82</td>
</tr>
<tr class="odd">
<td> </td>
<td>60</td>
<td>558</td>
<td>0.06</td>
<td>34</td>
</tr>
<tr class="even">
<td>Thermal shield</td>
<td>40</td>
<td>2720</td>
<td>0.10</td>
<td>272</td>
</tr>
<tr class="odd">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td>2640</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
<td>(<strong>TBD: inline7234, img68</strong> over-cap)</td>
<td>3960</td>
</tr>
</tbody>
</table>
<p><span id="tabhldistro" class="anchor"></span> </p></td>
</tr>
</tbody>
</table>

Table: The heat load distribution estimated for the MINERVA accelerator.

 

**Table:** The assumptions and data used to estimate the heat loads at
different temperature levels in Table [\[\*\]](#tabhldistro).

<table>
<caption>Table: The assumptions and data used to estimate the heat loads at different temperature levels in Table [*].</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Accelerator</td>
<td>32 cryomodules</td>
</tr>
<tr class="even">
<td> </td>
<td>32 valve boxes</td>
</tr>
<tr class="odd">
<td> </td>
<td>Q̇<sub>2K</sub> = 1 W m<sup>-1</sup> to 5 W m<sup>-1</sup><img src="media/rId68.png" style="width:3.91304in;height:1.37458in" alt="Table 5, from D-MYRTE-2.6, rendered as an image" /> </td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7208, img55</strong></td>
</tr>
<tr class="odd">
<td>Cavity</td>
<td><strong>TBD: inline6910, img2</strong></td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7212, img57</strong> per cavity</td>
</tr>
<tr class="odd">
<td> </td>
<td><strong>TBD: inline7214, img58</strong> cavities</td>
</tr>
<tr class="even">
<td>RF coupler</td>
<td><strong>TBD: inline7216, img59</strong> per coupler</td>
</tr>
<tr class="odd">
<td> </td>
<td><strong>TBD: inline7218, img60</strong> per coupler</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7220, img61</strong> per coupler</td>
</tr>
<tr class="odd">
<td> </td>
<td><strong>TBD: inline7222, img62</strong> couplers per cryomodule</td>
</tr>
<tr class="even">
<td>Thermal shield</td>
<td><strong>TBD: inline7224, img63</strong> per shield</td>
</tr>
<tr class="odd">
<td> </td>
<td><strong>TBD: inline7226, img64</strong> shield per cryomodule</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: The assumptions and data used to estimate the heat loads at
different temperature levels in Table \[\*\].

 

**Table:** The heat load distribution estimated for the MINERVA
accelerator.

<table>
<caption>Table: The heat load distribution estimated for the MINERVA accelerator.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Component</td>
<td><strong>TBD: inline7228, img65</strong></td>
<td>Heat load<strong>TBD: inline7230, img66</strong></td>
<td>Carnot factor</td>
<td>Heat load <strong>TBD: inline7232, img67</strong></td>
</tr>
<tr class="even">
<td> </td>
<td>K</td>
<td>W</td>
<td>-</td>
<td>W</td>
</tr>
<tr class="odd">
<td>Cavity, coupler,<br />
CM static</td>
<td>2</td>
<td>994</td>
<td>2.27</td>
<td>2255</td>
</tr>
<tr class="even">
<td>Coupler</td>
<td>10</td>
<td>186</td>
<td>0.44</td>
<td>82</td>
</tr>
<tr class="odd">
<td> </td>
<td>60</td>
<td>558</td>
<td>0.06</td>
<td>34</td>
</tr>
<tr class="even">
<td>Thermal shield</td>
<td>40</td>
<td>2720</td>
<td>0.10</td>
<td>272</td>
</tr>
<tr class="odd">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td>2640</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
<td>(<strong>TBD: inline7234, img68</strong> over-cap)</td>
<td>3960</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: The heat load distribution estimated for the MINERVA accelerator.

 

**Table:** The assumptions and data used to estimate the heat loads at
different temperature levels in Table [\[\*\]](#tabhldistro).

<table>
<caption>Table: The assumptions and data used to estimate the heat loads at different temperature levels in Table [*].</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Accelerator</td>
<td>32 cryomodules</td>
</tr>
<tr class="even">
<td> </td>
<td>32 valve boxes</td>
</tr>
<tr class="odd">
<td> </td>
<td>Q̇<sub>2K</sub> = 1 W m<sup>-1</sup> to 5 W m<sup>-1</sup><img src="media/rId68.png" style="width:3.91304in;height:1.37458in" alt="Table 5, from D-MYRTE-2.6, rendered as an image" /> </td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7208, img55</strong></td>
</tr>
<tr class="odd">
<td>Cavity</td>
<td><strong>TBD: inline6910, img2</strong></td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7212, img57</strong> per cavity</td>
</tr>
<tr class="odd">
<td> </td>
<td><strong>TBD: inline7214, img58</strong> cavities</td>
</tr>
<tr class="even">
<td>RF coupler</td>
<td><strong>TBD: inline7216, img59</strong> per coupler</td>
</tr>
<tr class="odd">
<td> </td>
<td><strong>TBD: inline7218, img60</strong> per coupler</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7220, img61</strong> per coupler</td>
</tr>
<tr class="odd">
<td> </td>
<td><strong>TBD: inline7222, img62</strong> couplers per cryomodule</td>
</tr>
<tr class="even">
<td>Thermal shield</td>
<td><strong>TBD: inline7224, img63</strong> per shield</td>
</tr>
<tr class="odd">
<td> </td>
<td><strong>TBD: inline7226, img64</strong> shield per cryomodule</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: The assumptions and data used to estimate the heat loads at
different temperature levels in Table \[\*\].

 

**Table:** The heat load distribution estimated for the MINERVA
accelerator.

<table>
<caption>Table: The heat load distribution estimated for the MINERVA accelerator.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Component</td>
<td><strong>TBD: inline7228, img65</strong></td>
<td>Heat load<strong>TBD: inline7230, img66</strong></td>
<td>Carnot factor</td>
<td>Heat load <strong>TBD: inline7232, img67</strong></td>
</tr>
<tr class="even">
<td> </td>
<td>K</td>
<td>W</td>
<td>-</td>
<td>W</td>
</tr>
<tr class="odd">
<td>Cavity, coupler,<br />
CM static</td>
<td>2</td>
<td>994</td>
<td>2.27</td>
<td>2255</td>
</tr>
<tr class="even">
<td>Coupler</td>
<td>10</td>
<td>186</td>
<td>0.44</td>
<td>82</td>
</tr>
<tr class="odd">
<td> </td>
<td>60</td>
<td>558</td>
<td>0.06</td>
<td>34</td>
</tr>
<tr class="even">
<td>Thermal shield</td>
<td>40</td>
<td>2720</td>
<td>0.10</td>
<td>272</td>
</tr>
<tr class="odd">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td>2640</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
<td>(<strong>TBD: inline7234, img68</strong> over-cap)</td>
<td>3960</td>
</tr>
</tbody>
</table>
<p><span id="tabhldistro" class="anchor"></span> </p></td>
</tr>
</tbody>
</table>

Table: The heat load distribution estimated for the MINERVA accelerator.

 

**Table:** BPM type and location along the accelerator.

<table>
<caption>Table: BPM type and location along the accelerator.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th>Proton Energy</th>
<th> BPM type</th>
<th>BPM Location</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>1.516.67MeV</td>
<td>Buttons</td>
<td>Between CH cavities</td>
</tr>
<tr class="even">
<td>16.67100MeV</td>
<td>Buttons</td>
<td>Inside quadrupoles</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: BPM type and location along the accelerator.

 

**Table:** Summary of the main characteristics of the detectors commonly
used in BLM systems.

<table>
<caption>Table: Summary of the main characteristics of the detectors commonly used in BLM systems.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Ionisa-</td>
<td>Photomulti-</td>
<td>Neutron</td>
<td>PlN Diodes</td>
<td>Charge</td>
</tr>
<tr class="even">
<td> </td>
<td>tion</td>
<td>plier tube</td>
<td>sensitive</td>
<td> </td>
<td>multiplying</td>
</tr>
<tr class="odd">
<td> </td>
<td>Chamber</td>
<td>+ PMT</td>
<td>scintillator</td>
<td> </td>
<td>structures</td>
</tr>
<tr class="even">
<td>Detector type</td>
<td>Gas-filled</td>
<td>Plastic</td>
<td>Neutron sensitive</td>
<td>Solid</td>
<td>Micromegas</td>
</tr>
<tr class="odd">
<td> </td>
<td>ionisation</td>
<td>scintillator +</td>
<td>plastic scintil-</td>
<td>state</td>
<td>(nBLM), Neutron sen-</td>
</tr>
<tr class="even">
<td> </td>
<td>chamber</td>
<td>PMT</td>
<td>lator + PMT</td>
<td>detector</td>
<td>sitive; PC tubes</td>
</tr>
<tr class="odd">
<td> </td>
<td>Ar, N2</td>
<td> </td>
<td> </td>
<td>doped</td>
<td>PTE moderated</td>
</tr>
<tr class="even">
<td>Sensitive</td>
<td> </td>
<td>NaI(Tl), BGO,</td>
<td>PTE moderator,</td>
<td>Si-crystal+</td>
<td>+convector,</td>
</tr>
<tr class="odd">
<td>medium</td>
<td> </td>
<td>EJ-208</td>
<td>ZnS scint.</td>
<td>harge</td>
<td>Micromegas:</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td>convector</td>
<td>He+10% CO2</td>
</tr>
<tr class="odd">
<td>Typical HV, V</td>
<td>1000 (negative)</td>
<td>700–1000 (neg.)</td>
<td>700–1000 (neg.)</td>
<td>30 V to 40 V</td>
<td>800 V (positive)</td>
</tr>
<tr class="even">
<td>Typical response</td>
<td>≈2 <strong>TBD: inline7236, img69</strong> s (fast)</td>
<td>≈10 ns (fast)</td>
<td>-</td>
<td>≈5 ns (fast)</td>
<td>≈20 ns (fast)</td>
</tr>
<tr class="odd">
<td>time (fast, slow)</td>
<td>≈1 ms (Slow)</td>
<td> </td>
<td>≈50 <strong>TBD: inline7236, img69</strong> s (Slow)</td>
<td> </td>
<td>≈200 <strong>TBD: inline7236, img69</strong> s (Slow)</td>
</tr>
<tr class="even">
<td>Particle sensitivity</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>neutron</td>
<td>neutron</td>
<td>neutron</td>
</tr>
<tr class="odd">
<td>Energy range</td>
<td>&gt; 500keV</td>
<td>&gt; 60keV</td>
<td>0.03 eV to 3MeV</td>
<td>10keV-10MeV</td>
<td>1 eV - 100MeV</td>
</tr>
<tr class="even">
<td>Dynamic range</td>
<td>Up to 10<sup>8</sup></td>
<td>10<sup>8</sup> achievable</td>
<td> </td>
<td>10<sup>4</sup>- 10<sup>6</sup></td>
<td>Up to 10<sup>5</sup></td>
</tr>
<tr class="odd">
<td>Typical</td>
<td>0.1 – 1 <strong>TBD: inline7236, img69</strong> C/Gy</td>
<td>2mA/R/h</td>
<td>80 pC/n/cm<sup>2</sup></td>
<td>≈1 pC/n/cm<sup>2</sup></td>
<td>about mV and</td>
</tr>
<tr class="even">
<td>sensitivity</td>
<td> </td>
<td>(50pC/MeV)</td>
<td> </td>
<td> </td>
<td>mA achievable</td>
</tr>
<tr class="odd">
<td>Operation mode</td>
<td>current</td>
<td>pulse</td>
<td>Current</td>
<td>Current/pulse</td>
<td>Current</td>
</tr>
<tr class="even">
<td>(Pulse, current)</td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td>&amp; pulse</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Summary of the main characteristics of the detectors commonly
used in BLM systems.

 

**Table:** Existing High Power Hadron Accelerators and related BLM
Systems.

<table>
<caption>Table: Existing High Power Hadron Accelerators and related BLM Systems.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
</colgroup>
<tbody>
<tr class="odd">
<td>#</td>
<td>HPHA Machines</td>
<td>Beam Current</td>
<td>Beam Energy</td>
<td>Nominal Power</td>
<td>Current Status</td>
<td>BLM System</td>
</tr>
<tr class="even">
<td>–   –</td>
<td> </td>
<td>(mA)</td>
<td>(GeV)</td>
<td>(MW)</td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>  —  
1</td>
<td>FNAL MI-60</td>
<td>0.003</td>
<td>120</td>
<td>0.36</td>
<td>Existing</td>
<td>IC+ Scintillator + PMT</td>
</tr>
<tr class="even">
<td>2</td>
<td>PS</td>
<td>0.0015</td>
<td>25</td>
<td>0.038</td>
<td>Existing</td>
<td>IC (two types: high &amp; low)</td>
</tr>
<tr class="odd">
<td>3</td>
<td>SPL</td>
<td>0.8</td>
<td>5</td>
<td>4</td>
<td>Planned</td>
<td>IC (two types: high &amp; low)</td>
</tr>
<tr class="even">
<td>4</td>
<td>LINAC4</td>
<td>0.03</td>
<td>0.16</td>
<td>0.005</td>
<td>U/Constr.</td>
<td>IC+ Scintillator/PMT</td>
</tr>
<tr class="odd">
<td>5</td>
<td>TRIUMF</td>
<td>0.25</td>
<td>0.5</td>
<td>0.125</td>
<td>Existing</td>
<td>IC(Argon)+BGO crystal/PMT</td>
</tr>
<tr class="even">
<td>6</td>
<td>SNS-1/ORNL</td>
<td>1.2</td>
<td>0.9</td>
<td>1.08</td>
<td>Existing</td>
<td>IC+NE102 Plastic Scintillator/PMT</td>
</tr>
<tr class="odd">
<td>7</td>
<td>SNS-2/ORNL</td>
<td>1.5</td>
<td>1</td>
<td>1.5</td>
<td>Existing</td>
<td>IC+NE102 Plastic Scintillator/PMT</td>
</tr>
<tr class="even">
<td>8</td>
<td>PSI</td>
<td>2</td>
<td>0.6</td>
<td>1.2</td>
<td>Existing</td>
<td>IC</td>
</tr>
<tr class="odd">
<td>9</td>
<td>ESS</td>
<td>2.5</td>
<td>2</td>
<td>5</td>
<td>U/Constr.</td>
<td>IC + nBLM (micromegas)</td>
</tr>
<tr class="even">
<td>10</td>
<td>SPIRAL-2</td>
<td>5</td>
<td>0.04</td>
<td>0.2</td>
<td>U/Constr.</td>
<td>Scintillator + Thermal BLM</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Existing High Power Hadron Accelerators and related BLM Systems.

 

**Table:** Distribution of the 107 BLM Detectors proposed for the
MINERVA BLM Systems.

<table>
<caption>Table: Distribution of the 107 BLM Detectors proposed for the MINERVA BLM Systems.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Accelerator section</td>
<td>5.9 Injector-MEBT-2</td>
<td>(CH8–CH15)</td>
<td>MEBT-3</td>
<td>SC LinacMain Linac</td>
<td>TOTAL</td>
</tr>
<tr class="even">
<td>Ionisation Chambers</td>
<td>1</td>
<td>3</td>
<td>6</td>
<td>60
(2 per cryomodule)</td>
<td>70</td>
</tr>
<tr class="odd">
<td>Neutron Detectors
(1 per ≈ 1 m)</td>
<td>10</td>
<td>8</td>
<td>6</td>
<td>-</td>
<td>24</td>
</tr>
<tr class="even">
<td>Photomultiplier tubes</td>
<td>1</td>
<td>1</td>
<td>1</td>
<td>10</td>
<td>13</td>
</tr>
<tr class="odd">
<td>Subtotal</td>
<td>12</td>
<td>12</td>
<td>13</td>
<td>70</td>
<td>107</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Distribution of the 107 BLM Detectors proposed for the MINERVA
BLM Systems.

  

**Table:** The MINERVA 107 BLM system distribution and naming
convention.

 

**Table:** BLM of the beam line (SC Linacmain linac) system -- LB-ME

15\.

<table>
<caption>Table: BLM of the beam line (SC Linacmain linac) system – LB-ME15.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td>INJECTOR1 – MEBT3
<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<tbody>
<tr class="odd">
<td>25</td>
<td>I1-ME3-BD-BLM-ND-01</td>
<td>nBLM</td>
<td>MEBT3</td>
<td>Neutron Detector</td>
<td>n</td>
<td>&lt; 3.5MeV</td>
<td>&gt; 0.05ms</td>
<td>16.6</td>
<td>32.5</td>
</tr>
<tr class="even">
<td>26</td>
<td>I1-ME3-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>MEBT3</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1ms</td>
<td>16.6</td>
<td>32.5</td>
</tr>
<tr class="odd">
<td>27</td>
<td>I1-ME3-BD-BLM-ND-02</td>
<td>nBLM</td>
<td>MEBT3</td>
<td>Neutron Detector</td>
<td>n</td>
<td>&lt; 3.5MeV</td>
<td>&gt; 0.05ms</td>
<td>16.6</td>
<td>34.2</td>
</tr>
<tr class="even">
<td>28</td>
<td>I1-ME3-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>MEBT3</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1ms</td>
<td>16.6</td>
<td>34.2</td>
</tr>
<tr class="odd">
<td>29</td>
<td>I1-ME3-BD-BLM-ND-03</td>
<td>nBLM</td>
<td>MEBT3</td>
<td>Neutron Detector</td>
<td>n</td>
<td>&lt; 3.5MeV</td>
<td>&gt; 0.05ms</td>
<td>16.6</td>
<td>36.93</td>
</tr>
<tr class="even">
<td>30</td>
<td>I1-ME3-BD-BLM-IC-03</td>
<td>iBLM</td>
<td>MEBT3</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1ms</td>
<td>16.6</td>
<td>36.93</td>
</tr>
<tr class="odd">
<td>31</td>
<td>I1-ME3-BD-BLM-ND-04</td>
<td>nBLM</td>
<td>MEBT3</td>
<td>Neutron Detector</td>
<td>n</td>
<td>&lt; 3.5MeV</td>
<td>&gt; 0.05ms</td>
<td>16.6</td>
<td>38.07</td>
</tr>
<tr class="even">
<td>32</td>
<td>I1-ME3-BD-BLM-IC-04</td>
<td>iBLM</td>
<td>MEBT3</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1ms</td>
<td>16.6</td>
<td>38.07</td>
</tr>
<tr class="odd">
<td>37</td>
<td>I1-ME3-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>MEBT3</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>16.6</td>
<td>38.07</td>
</tr>
</tbody>
</table>
<p>BEAM LINE (MAIN LINAC) System - LB-MEBT3</p>
<p> </p>
<p><strong>Table:</strong> BLM between Injector 1 and MEBT-3.</p>
<table style="width:100%;">
<caption>Table: BLM between Injector 1 and MEBT-3.</caption>
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<tbody>
<tr class="odd">
<td>38</td>
<td>I1-ME3-BD-BLM-ND-05</td>
<td>nBLM</td>
<td>LB - MEBT3</td>
<td>Neutron Detector</td>
<td>n</td>
<td>&lt; 3.5MeV</td>
<td>&gt; 0.05ms</td>
<td>16.6</td>
<td>41.0</td>
</tr>
<tr class="even">
<td>39</td>
<td>I1-ME3-BD-BLM-IC-05</td>
<td>iBLM</td>
<td>LB - MEBT3</td>
<td>ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1ms</td>
<td>16.6</td>
<td>41.0</td>
</tr>
<tr class="odd">
<td>40</td>
<td>I1-ME3-BD-BLM-ND-06</td>
<td>nBLM</td>
<td>LB - MEBT3</td>
<td>Neutron Detector</td>
<td>n</td>
<td>&lt; 3.5MeV</td>
<td>&gt; 0.05ms</td>
<td>16.6</td>
<td>42.8</td>
</tr>
<tr class="even">
<td>41</td>
<td>I1-ME3-BD-BLM-IC-06</td>
<td>iBLM</td>
<td>LB - MEBT3</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1ms</td>
<td>16.6</td>
<td>42.8</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: BLM of the beam line (SC Linacmain linac) system -- LB-ME15.

 

**Table:** BLM of the LB sections (S01--S18)

<table>
<caption>Table: BLM of the LB sections (S01–S18)</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td>LB-SECTIONS (S01-S18)
<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<tbody>
<tr class="odd">
<td>42</td>
<td>LB-S01-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-01</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>18.0</td>
<td>46.2</td>
</tr>
<tr class="even">
<td>43</td>
<td>LB-S01-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-01</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>19.4</td>
<td>46.9</td>
</tr>
<tr class="odd">
<td>44</td>
<td>LB-S02-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-02</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>20.8</td>
<td>49.3</td>
</tr>
<tr class="even">
<td>45</td>
<td>LB-S02-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-02</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>22.3</td>
<td>50.0</td>
</tr>
<tr class="odd">
<td>42</td>
<td>LB-S03-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-03</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>23.7</td>
<td>52.4</td>
</tr>
<tr class="even">
<td>43</td>
<td>LB-S03-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-03</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>25.1</td>
<td>53.0</td>
</tr>
<tr class="odd">
<td>44</td>
<td>LB-S03-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-03</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>25.1</td>
<td>53.0</td>
</tr>
<tr class="even">
<td>45</td>
<td>LB-S04-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-04</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>26.5</td>
<td>55.4</td>
</tr>
<tr class="odd">
<td>46</td>
<td>LB-S04-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-04</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>27.9</td>
<td>56.1</td>
</tr>
<tr class="even">
<td>47</td>
<td>LB-S05-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-05</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>29.3</td>
<td>58.5</td>
</tr>
<tr class="odd">
<td>48</td>
<td>LB-S05-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-05</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>30.7</td>
<td>59.2</td>
</tr>
<tr class="even">
<td>49</td>
<td>LB-S06-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-06</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>32.1</td>
<td>61.6</td>
</tr>
<tr class="odd">
<td>50</td>
<td>LB-S06-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-06</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>33.6</td>
<td>62.3</td>
</tr>
<tr class="even">
<td>51</td>
<td>LB-S06-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-06</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>33.6</td>
<td>62.3</td>
</tr>
<tr class="odd">
<td>52</td>
<td>LB-S07-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-07</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>35.0</td>
<td>64.7</td>
</tr>
<tr class="even">
<td>53</td>
<td>LB-S07-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-07</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>36.4</td>
<td>65.4</td>
</tr>
<tr class="odd">
<td>54</td>
<td>LB-S08-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-08</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>37.8</td>
<td>67.8</td>
</tr>
<tr class="even">
<td>55</td>
<td>LB-S08-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-08</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>39.2</td>
<td>68.4</td>
</tr>
<tr class="odd">
<td>56</td>
<td>LB-S09-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-09</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>40.6</td>
<td>70.8</td>
</tr>
<tr class="even">
<td>57</td>
<td>LB-S09-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-09</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>42.0</td>
<td>71.5</td>
</tr>
<tr class="odd">
<td>58</td>
<td>LB-S09-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-09</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>42.0</td>
<td>71.5</td>
</tr>
<tr class="even">
<td>59</td>
<td>LB-S10-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-10</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>43.5</td>
<td>73.9</td>
</tr>
<tr class="odd">
<td>60</td>
<td>LB-S10-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-10</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>44.9</td>
<td>74.6</td>
</tr>
<tr class="even">
<td>61</td>
<td>LB-S11-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-11</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>46.3</td>
<td>77.0</td>
</tr>
<tr class="odd">
<td>62</td>
<td>LB-S11-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-11</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>47.7</td>
<td>77.7</td>
</tr>
<tr class="even">
<td>63</td>
<td>LB-S12-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-12</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>49.1</td>
<td>80.1</td>
</tr>
<tr class="odd">
<td>64</td>
<td>LB-S12-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-12</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>50.5</td>
<td>80.8</td>
</tr>
<tr class="even">
<td>65</td>
<td>LB-S12-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-12</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>50.5</td>
<td>80.8</td>
</tr>
<tr class="odd">
<td>66</td>
<td>LB-S13-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-13</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>51.9</td>
<td>83.2</td>
</tr>
<tr class="even">
<td>67</td>
<td>LB-S13-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-13</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>53.4</td>
<td>83.8</td>
</tr>
<tr class="odd">
<td>68</td>
<td>LB-S14-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-14</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>54.8</td>
<td>86.2</td>
</tr>
<tr class="even">
<td>69</td>
<td>LB-S14-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-14</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>56.2</td>
<td>86.9</td>
</tr>
<tr class="odd">
<td>70</td>
<td>LB-S15-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-15</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>57.6</td>
<td>89.3</td>
</tr>
<tr class="even">
<td>71</td>
<td>LB-S15-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-15</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>59.0</td>
<td>90.0</td>
</tr>
<tr class="odd">
<td>72</td>
<td>LB-S15-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-15</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>59.0</td>
<td>90.0</td>
</tr>
<tr class="even">
<td>73</td>
<td>LB-S16-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-16</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>60.4</td>
<td>92.4</td>
</tr>
<tr class="odd">
<td>74</td>
<td>LB-S16-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-16</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>61.8</td>
<td>93.1</td>
</tr>
<tr class="even">
<td>75</td>
<td>LB-S17-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-17</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>63.2</td>
<td>95.5</td>
</tr>
<tr class="odd">
<td>76</td>
<td>LB-S17-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-17</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>64.7</td>
<td>96.2</td>
</tr>
<tr class="even">
<td>77</td>
<td>LB-S18-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-18</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>66.1</td>
<td>98.6</td>
</tr>
<tr class="odd">
<td>78</td>
<td>LB-S18-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-18</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>67.5</td>
<td>99.2</td>
</tr>
<tr class="even">
<td>79</td>
<td>LB-S18-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-18</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>67.5</td>
<td>99.2</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: BLM of the LB sections (S01--S18)

 

**Table:** BLM of the LB sections (S19--S30)

<table>
<caption>Table: BLM of the LB sections (S19–S30)</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td>LB-SECTIONS (S19-S30)
<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<tbody>
<tr class="odd">
<td>80</td>
<td>LB-S19-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-19</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>68.9</td>
<td>101.6</td>
</tr>
<tr class="even">
<td>81</td>
<td>LB-S19-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-19</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>70.3</td>
<td>102.3</td>
</tr>
<tr class="odd">
<td>82</td>
<td>LB-S20-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-20</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>71.7</td>
<td>104.7</td>
</tr>
<tr class="even">
<td>83</td>
<td>LB-S20-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-20</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>73.1</td>
<td>105.4</td>
</tr>
<tr class="odd">
<td>84</td>
<td>LB-S21-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-21</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>74.6</td>
<td>107.8</td>
</tr>
<tr class="even">
<td>85</td>
<td>LB-S21-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-21</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>76.0</td>
<td>108.5</td>
</tr>
<tr class="odd">
<td>86</td>
<td>LB-S21-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-21</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>76.0</td>
<td>108.5</td>
</tr>
<tr class="even">
<td>87</td>
<td>LB-S22-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-22</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>77.4</td>
<td>110.9</td>
</tr>
<tr class="odd">
<td>88</td>
<td>LB-S22-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-22</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>78.8</td>
<td>111.6</td>
</tr>
<tr class="even">
<td>89</td>
<td>LB-S23-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-23</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>80.2</td>
<td>114.0</td>
</tr>
<tr class="odd">
<td>90</td>
<td>LB-S23-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-23</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>81.6</td>
<td>114.6</td>
</tr>
<tr class="even">
<td>91</td>
<td>LB-S24-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-24</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>83.0</td>
<td>117.0</td>
</tr>
<tr class="odd">
<td>92</td>
<td>LB-S24-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-24</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>84.4</td>
<td>117.7</td>
</tr>
<tr class="even">
<td>93</td>
<td>LB-S24-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-24</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>84.4</td>
<td>117.7</td>
</tr>
<tr class="odd">
<td>94</td>
<td>LB-S25-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-25</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>85.9</td>
<td>120.1</td>
</tr>
<tr class="even">
<td>95</td>
<td>LB-S25-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-25</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>87.3</td>
<td>120.8</td>
</tr>
<tr class="odd">
<td>96</td>
<td>LB-S26-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-26</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>88.7</td>
<td>123.2</td>
</tr>
<tr class="even">
<td>97</td>
<td>LB-S26-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-26</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>90.1</td>
<td>123.9</td>
</tr>
<tr class="odd">
<td>98</td>
<td>LB-S27-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-27</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>91.5</td>
<td>126.3</td>
</tr>
<tr class="even">
<td>99</td>
<td>LB-S27-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-27</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>92.9</td>
<td>127.0</td>
</tr>
<tr class="odd">
<td>100</td>
<td>LB-S27-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-27</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>92.9</td>
<td>127.0</td>
</tr>
<tr class="even">
<td>101</td>
<td>LB-S28-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-28</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>94.3</td>
<td>129.4</td>
</tr>
<tr class="odd">
<td>102</td>
<td>LB-S28-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-28</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>95.8</td>
<td>130.0</td>
</tr>
<tr class="even">
<td>103</td>
<td>LB-S29-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-29</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>97.2</td>
<td>132.4</td>
</tr>
<tr class="odd">
<td>104</td>
<td>LB-S29-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-29</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>98.6</td>
<td>133.1</td>
</tr>
<tr class="even">
<td>105</td>
<td>LB-S30-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-30</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>100.0</td>
<td>135.5</td>
</tr>
<tr class="odd">
<td>106</td>
<td>LB-S30-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-30</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1ms</td>
<td>101.4</td>
<td>136.2</td>
</tr>
<tr class="even">
<td>107</td>
<td>LB-S30-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-30</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>101.4</td>
<td>136.2</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: BLM of the LB sections (S19--S30)

 

**Table:** Provisional beam instrumentation needs for the MINERVA
Accelerator.

<table>
<caption>Table: Provisional beam instrumentation needs for the MINERVA Accelerator.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Beam Instrumentation</td>
<td>Injector</td>
<td>MEBT-3</td>
<td>SC LinacMain linac 100 MeV</td>
<td>100 MeV</td>
<td>Total</td>
</tr>
<tr class="even">
<td>Beam position monitors</td>
<td>6</td>
<td>8</td>
<td>50</td>
<td>15</td>
<td>79</td>
</tr>
<tr class="odd">
<td>Beam transverse profilers</td>
<td>10</td>
<td>6</td>
<td>10</td>
<td>15</td>
<td>41</td>
</tr>
<tr class="even">
<td>Emittance-metres</td>
<td>4</td>
<td>4</td>
<td>-</td>
<td>-</td>
<td>8</td>
</tr>
<tr class="odd">
<td>Beam current monitors</td>
<td>6</td>
<td>4</td>
<td>2</td>
<td>2 to 6</td>
<td>14 to 18</td>
</tr>
<tr class="even">
<td>Faraday cups</td>
<td>8</td>
<td>8</td>
<td>-</td>
<td>-</td>
<td>16</td>
</tr>
<tr class="odd">
<td>Beam energy monitors</td>
<td>2</td>
<td>2</td>
<td>-</td>
<td>1</td>
<td>5</td>
</tr>
<tr class="even">
<td>Bunch length monitors</td>
<td>4</td>
<td>2</td>
<td>12</td>
<td>-</td>
<td>18</td>
</tr>
<tr class="odd">
<td>Beam loss monitors</td>
<td>24</td>
<td>13</td>
<td>70</td>
<td>&gt; 12</td>
<td>&gt; 119</td>
</tr>
<tr class="even">
<td>Halo monitors and slits</td>
<td>5</td>
<td>5</td>
<td>-</td>
<td>13</td>
<td>23</td>
</tr>
<tr class="odd">
<td>Beam monitoring on target</td>
<td>-</td>
<td>-</td>
<td>-</td>
<td>1</td>
<td>1</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Provisional beam instrumentation needs for the MINERVA
Accelerator.

 

**Table:** BPM type and location along the accelerator.

<table>
<caption>Table: BPM type and location along the accelerator.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th>Proton Energy</th>
<th> BPM type</th>
<th>BPM Location</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>1.516.67MeV</td>
<td>Buttons</td>
<td>Between CH cavities</td>
</tr>
<tr class="even">
<td>16.67100MeV</td>
<td>Buttons</td>
<td>Inside quadrupoles</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: BPM type and location along the accelerator.

 

**Table:** Summary of the main characteristics of the detectors commonly
used in BLM systems.

<table>
<caption>Table: Summary of the main characteristics of the detectors commonly used in BLM systems.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Ionisa-</td>
<td>Photomulti-</td>
<td>Neutron</td>
<td>PlN Diodes</td>
<td>Charge</td>
</tr>
<tr class="even">
<td> </td>
<td>tion</td>
<td>plier tube</td>
<td>sensitive</td>
<td> </td>
<td>multiplying</td>
</tr>
<tr class="odd">
<td> </td>
<td>Chamber</td>
<td>+ PMT</td>
<td>scintillator</td>
<td> </td>
<td>structures</td>
</tr>
<tr class="even">
<td>Detector type</td>
<td>Gas-filled</td>
<td>Plastic</td>
<td>Neutron sensitive</td>
<td>Solid</td>
<td>Micromegas</td>
</tr>
<tr class="odd">
<td> </td>
<td>ionisation</td>
<td>scintillator +</td>
<td>plastic scintil-</td>
<td>state</td>
<td>(nBLM), Neutron sen-</td>
</tr>
<tr class="even">
<td> </td>
<td>chamber</td>
<td>PMT</td>
<td>lator + PMT</td>
<td>detector</td>
<td>sitive; PC tubes</td>
</tr>
<tr class="odd">
<td> </td>
<td>Ar, N2</td>
<td> </td>
<td> </td>
<td>doped</td>
<td>PTE moderated</td>
</tr>
<tr class="even">
<td>Sensitive</td>
<td> </td>
<td>NaI(Tl), BGO,</td>
<td>PTE moderator,</td>
<td>Si-crystal+</td>
<td>+convector,</td>
</tr>
<tr class="odd">
<td>medium</td>
<td> </td>
<td>EJ-208</td>
<td>ZnS scint.</td>
<td>harge</td>
<td>Micromegas:</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td>convector</td>
<td>He+10% CO2</td>
</tr>
<tr class="odd">
<td>Typical HV, V</td>
<td>1000 (negative)</td>
<td>700–1000 (neg.)</td>
<td>700–1000 (neg.)</td>
<td>30 V to 40 V</td>
<td>800 V (positive)</td>
</tr>
<tr class="even">
<td>Typical response</td>
<td>≈2 <strong>TBD: inline7236, img69</strong> s (fast)</td>
<td>≈10 ns (fast)</td>
<td>-</td>
<td>≈5 ns (fast)</td>
<td>≈20 ns (fast)</td>
</tr>
<tr class="odd">
<td>time (fast, slow)</td>
<td>≈1 ms (Slow)</td>
<td> </td>
<td>≈50 <strong>TBD: inline7236, img69</strong> s (Slow)</td>
<td> </td>
<td>≈200 <strong>TBD: inline7236, img69</strong> s (Slow)</td>
</tr>
<tr class="even">
<td>Particle sensitivity</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>neutron</td>
<td>neutron</td>
<td>neutron</td>
</tr>
<tr class="odd">
<td>Energy range</td>
<td>&gt; 500keV</td>
<td>&gt; 60keV</td>
<td>0.03 eV to 3MeV</td>
<td>10keV-10MeV</td>
<td>1 eV - 100MeV</td>
</tr>
<tr class="even">
<td>Dynamic range</td>
<td>Up to 10<sup>8</sup></td>
<td>10<sup>8</sup> achievable</td>
<td> </td>
<td>10<sup>4</sup>- 10<sup>6</sup></td>
<td>Up to 10<sup>5</sup></td>
</tr>
<tr class="odd">
<td>Typical</td>
<td>0.1 – 1 <strong>TBD: inline7236, img69</strong> C/Gy</td>
<td>2mA/R/h</td>
<td>80 pC/n/cm<sup>2</sup></td>
<td>≈1 pC/n/cm<sup>2</sup></td>
<td>about mV and</td>
</tr>
<tr class="even">
<td>sensitivity</td>
<td> </td>
<td>(50pC/MeV)</td>
<td> </td>
<td> </td>
<td>mA achievable</td>
</tr>
<tr class="odd">
<td>Operation mode</td>
<td>current</td>
<td>pulse</td>
<td>Current</td>
<td>Current/pulse</td>
<td>Current</td>
</tr>
<tr class="even">
<td>(Pulse, current)</td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td>&amp; pulse</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Summary of the main characteristics of the detectors commonly
used in BLM systems.

 

**Table:** Existing High Power Hadron Accelerators and related BLM
Systems.

<table>
<caption>Table: Existing High Power Hadron Accelerators and related BLM Systems.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
</colgroup>
<tbody>
<tr class="odd">
<td>#</td>
<td>HPHA Machines</td>
<td>Beam Current</td>
<td>Beam Energy</td>
<td>Nominal Power</td>
<td>Current Status</td>
<td>BLM System</td>
</tr>
<tr class="even">
<td>–   –</td>
<td> </td>
<td>(mA)</td>
<td>(GeV)</td>
<td>(MW)</td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>  —  
1</td>
<td>FNAL MI-60</td>
<td>0.003</td>
<td>120</td>
<td>0.36</td>
<td>Existing</td>
<td>IC+ Scintillator + PMT</td>
</tr>
<tr class="even">
<td>2</td>
<td>PS</td>
<td>0.0015</td>
<td>25</td>
<td>0.038</td>
<td>Existing</td>
<td>IC (two types: high &amp; low)</td>
</tr>
<tr class="odd">
<td>3</td>
<td>SPL</td>
<td>0.8</td>
<td>5</td>
<td>4</td>
<td>Planned</td>
<td>IC (two types: high &amp; low)</td>
</tr>
<tr class="even">
<td>4</td>
<td>LINAC4</td>
<td>0.03</td>
<td>0.16</td>
<td>0.005</td>
<td>U/Constr.</td>
<td>IC+ Scintillator/PMT</td>
</tr>
<tr class="odd">
<td>5</td>
<td>TRIUMF</td>
<td>0.25</td>
<td>0.5</td>
<td>0.125</td>
<td>Existing</td>
<td>IC(Argon)+BGO crystal/PMT</td>
</tr>
<tr class="even">
<td>6</td>
<td>SNS-1/ORNL</td>
<td>1.2</td>
<td>0.9</td>
<td>1.08</td>
<td>Existing</td>
<td>IC+NE102 Plastic Scintillator/PMT</td>
</tr>
<tr class="odd">
<td>7</td>
<td>SNS-2/ORNL</td>
<td>1.5</td>
<td>1</td>
<td>1.5</td>
<td>Existing</td>
<td>IC+NE102 Plastic Scintillator/PMT</td>
</tr>
<tr class="even">
<td>8</td>
<td>PSI</td>
<td>2</td>
<td>0.6</td>
<td>1.2</td>
<td>Existing</td>
<td>IC</td>
</tr>
<tr class="odd">
<td>9</td>
<td>ESS</td>
<td>2.5</td>
<td>2</td>
<td>5</td>
<td>U/Constr.</td>
<td>IC + nBLM (micromegas)</td>
</tr>
<tr class="even">
<td>10</td>
<td>SPIRAL-2</td>
<td>5</td>
<td>0.04</td>
<td>0.2</td>
<td>U/Constr.</td>
<td>Scintillator + Thermal BLM</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Existing High Power Hadron Accelerators and related BLM Systems.

 

**Table:** Distribution of the 107 BLM Detectors proposed for the
MINERVA BLM Systems.

<table>
<caption>Table: Distribution of the 107 BLM Detectors proposed for the MINERVA BLM Systems.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Accelerator section</td>
<td>5.9 Injector-MEBT-2</td>
<td>(CH8–CH15)</td>
<td>MEBT-3</td>
<td>SC LinacMain Linac</td>
<td>TOTAL</td>
</tr>
<tr class="even">
<td>Ionisation Chambers</td>
<td>1</td>
<td>3</td>
<td>6</td>
<td>60
(2 per cryomodule)</td>
<td>70</td>
</tr>
<tr class="odd">
<td>Neutron Detectors
(1 per ≈ 1 m)</td>
<td>10</td>
<td>8</td>
<td>6</td>
<td>-</td>
<td>24</td>
</tr>
<tr class="even">
<td>Photomultiplier tubes</td>
<td>1</td>
<td>1</td>
<td>1</td>
<td>10</td>
<td>13</td>
</tr>
<tr class="odd">
<td>Subtotal</td>
<td>12</td>
<td>12</td>
<td>13</td>
<td>70</td>
<td>107</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Distribution of the 107 BLM Detectors proposed for the MINERVA
BLM Systems.

  

**Table:** The MINERVA 107 BLM system distribution and naming
convention.

 

**Table:** BLM of the beam line (SC Linacmain linac) system -- LB-ME

3\.

<table>
<caption>Table: BLM of the beam line (SC Linacmain linac) system – LB-ME3.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td>INJECTOR1 – MEBT3
<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<tbody>
<tr class="odd">
<td>25</td>
<td>I1-ME3-BD-BLM-ND-01</td>
<td>nBLM</td>
<td>MEBT3</td>
<td>Neutron Detector</td>
<td>n</td>
<td>&lt; 3.5MeV</td>
<td>&gt; 0.05ms</td>
<td>16.6</td>
<td>32.5</td>
</tr>
<tr class="even">
<td>26</td>
<td>I1-ME3-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>MEBT3</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1ms</td>
<td>16.6</td>
<td>32.5</td>
</tr>
<tr class="odd">
<td>27</td>
<td>I1-ME3-BD-BLM-ND-02</td>
<td>nBLM</td>
<td>MEBT3</td>
<td>Neutron Detector</td>
<td>n</td>
<td>&lt; 3.5MeV</td>
<td>&gt; 0.05ms</td>
<td>16.6</td>
<td>34.2</td>
</tr>
<tr class="even">
<td>28</td>
<td>I1-ME3-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>MEBT3</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1ms</td>
<td>16.6</td>
<td>34.2</td>
</tr>
<tr class="odd">
<td>29</td>
<td>I1-ME3-BD-BLM-ND-03</td>
<td>nBLM</td>
<td>MEBT3</td>
<td>Neutron Detector</td>
<td>n</td>
<td>&lt; 3.5MeV</td>
<td>&gt; 0.05ms</td>
<td>16.6</td>
<td>36.93</td>
</tr>
<tr class="even">
<td>30</td>
<td>I1-ME3-BD-BLM-IC-03</td>
<td>iBLM</td>
<td>MEBT3</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1ms</td>
<td>16.6</td>
<td>36.93</td>
</tr>
<tr class="odd">
<td>31</td>
<td>I1-ME3-BD-BLM-ND-04</td>
<td>nBLM</td>
<td>MEBT3</td>
<td>Neutron Detector</td>
<td>n</td>
<td>&lt; 3.5MeV</td>
<td>&gt; 0.05ms</td>
<td>16.6</td>
<td>38.07</td>
</tr>
<tr class="even">
<td>32</td>
<td>I1-ME3-BD-BLM-IC-04</td>
<td>iBLM</td>
<td>MEBT3</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1ms</td>
<td>16.6</td>
<td>38.07</td>
</tr>
<tr class="odd">
<td>37</td>
<td>I1-ME3-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>MEBT3</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>16.6</td>
<td>38.07</td>
</tr>
</tbody>
</table>
<p>BEAM LINE (MAIN LINAC) System - LB-MEBT3</p>
<p> </p>
<p><strong>Table:</strong> BLM between Injector 1 and MEBT-3.</p>
<table style="width:100%;">
<caption>Table: BLM between Injector 1 and MEBT-3.</caption>
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<tbody>
<tr class="odd">
<td>38</td>
<td>I1-ME3-BD-BLM-ND-05</td>
<td>nBLM</td>
<td>LB - MEBT3</td>
<td>Neutron Detector</td>
<td>n</td>
<td>&lt; 3.5MeV</td>
<td>&gt; 0.05ms</td>
<td>16.6</td>
<td>41.0</td>
</tr>
<tr class="even">
<td>39</td>
<td>I1-ME3-BD-BLM-IC-05</td>
<td>iBLM</td>
<td>LB - MEBT3</td>
<td>ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1ms</td>
<td>16.6</td>
<td>41.0</td>
</tr>
<tr class="odd">
<td>40</td>
<td>I1-ME3-BD-BLM-ND-06</td>
<td>nBLM</td>
<td>LB - MEBT3</td>
<td>Neutron Detector</td>
<td>n</td>
<td>&lt; 3.5MeV</td>
<td>&gt; 0.05ms</td>
<td>16.6</td>
<td>42.8</td>
</tr>
<tr class="even">
<td>41</td>
<td>I1-ME3-BD-BLM-IC-06</td>
<td>iBLM</td>
<td>LB - MEBT3</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1ms</td>
<td>16.6</td>
<td>42.8</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: BLM of the beam line (SC Linacmain linac) system -- LB-ME3.

 

**Table:** BLM of the LB sections (S01--S18)

<table>
<caption>Table: BLM of the LB sections (S01–S18)</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td>LB-SECTIONS (S01-S18)
<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<tbody>
<tr class="odd">
<td>42</td>
<td>LB-S01-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-01</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>18.0</td>
<td>46.2</td>
</tr>
<tr class="even">
<td>43</td>
<td>LB-S01-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-01</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>19.4</td>
<td>46.9</td>
</tr>
<tr class="odd">
<td>44</td>
<td>LB-S02-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-02</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>20.8</td>
<td>49.3</td>
</tr>
<tr class="even">
<td>45</td>
<td>LB-S02-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-02</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>22.3</td>
<td>50.0</td>
</tr>
<tr class="odd">
<td>42</td>
<td>LB-S03-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-03</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>23.7</td>
<td>52.4</td>
</tr>
<tr class="even">
<td>43</td>
<td>LB-S03-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-03</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>25.1</td>
<td>53.0</td>
</tr>
<tr class="odd">
<td>44</td>
<td>LB-S03-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-03</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>25.1</td>
<td>53.0</td>
</tr>
<tr class="even">
<td>45</td>
<td>LB-S04-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-04</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>26.5</td>
<td>55.4</td>
</tr>
<tr class="odd">
<td>46</td>
<td>LB-S04-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-04</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>27.9</td>
<td>56.1</td>
</tr>
<tr class="even">
<td>47</td>
<td>LB-S05-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-05</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>29.3</td>
<td>58.5</td>
</tr>
<tr class="odd">
<td>48</td>
<td>LB-S05-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-05</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>30.7</td>
<td>59.2</td>
</tr>
<tr class="even">
<td>49</td>
<td>LB-S06-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-06</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>32.1</td>
<td>61.6</td>
</tr>
<tr class="odd">
<td>50</td>
<td>LB-S06-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-06</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>33.6</td>
<td>62.3</td>
</tr>
<tr class="even">
<td>51</td>
<td>LB-S06-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-06</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>33.6</td>
<td>62.3</td>
</tr>
<tr class="odd">
<td>52</td>
<td>LB-S07-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-07</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>35.0</td>
<td>64.7</td>
</tr>
<tr class="even">
<td>53</td>
<td>LB-S07-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-07</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>36.4</td>
<td>65.4</td>
</tr>
<tr class="odd">
<td>54</td>
<td>LB-S08-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-08</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>37.8</td>
<td>67.8</td>
</tr>
<tr class="even">
<td>55</td>
<td>LB-S08-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-08</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>39.2</td>
<td>68.4</td>
</tr>
<tr class="odd">
<td>56</td>
<td>LB-S09-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-09</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>40.6</td>
<td>70.8</td>
</tr>
<tr class="even">
<td>57</td>
<td>LB-S09-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-09</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>42.0</td>
<td>71.5</td>
</tr>
<tr class="odd">
<td>58</td>
<td>LB-S09-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-09</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>42.0</td>
<td>71.5</td>
</tr>
<tr class="even">
<td>59</td>
<td>LB-S10-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-10</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>43.5</td>
<td>73.9</td>
</tr>
<tr class="odd">
<td>60</td>
<td>LB-S10-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-10</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>44.9</td>
<td>74.6</td>
</tr>
<tr class="even">
<td>61</td>
<td>LB-S11-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-11</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>46.3</td>
<td>77.0</td>
</tr>
<tr class="odd">
<td>62</td>
<td>LB-S11-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-11</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>47.7</td>
<td>77.7</td>
</tr>
<tr class="even">
<td>63</td>
<td>LB-S12-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-12</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>49.1</td>
<td>80.1</td>
</tr>
<tr class="odd">
<td>64</td>
<td>LB-S12-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-12</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>50.5</td>
<td>80.8</td>
</tr>
<tr class="even">
<td>65</td>
<td>LB-S12-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-12</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>50.5</td>
<td>80.8</td>
</tr>
<tr class="odd">
<td>66</td>
<td>LB-S13-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-13</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>51.9</td>
<td>83.2</td>
</tr>
<tr class="even">
<td>67</td>
<td>LB-S13-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-13</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>53.4</td>
<td>83.8</td>
</tr>
<tr class="odd">
<td>68</td>
<td>LB-S14-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-14</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>54.8</td>
<td>86.2</td>
</tr>
<tr class="even">
<td>69</td>
<td>LB-S14-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-14</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>56.2</td>
<td>86.9</td>
</tr>
<tr class="odd">
<td>70</td>
<td>LB-S15-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-15</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>57.6</td>
<td>89.3</td>
</tr>
<tr class="even">
<td>71</td>
<td>LB-S15-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-15</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>59.0</td>
<td>90.0</td>
</tr>
<tr class="odd">
<td>72</td>
<td>LB-S15-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-15</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>59.0</td>
<td>90.0</td>
</tr>
<tr class="even">
<td>73</td>
<td>LB-S16-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-16</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>60.4</td>
<td>92.4</td>
</tr>
<tr class="odd">
<td>74</td>
<td>LB-S16-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-16</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>61.8</td>
<td>93.1</td>
</tr>
<tr class="even">
<td>75</td>
<td>LB-S17-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-17</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>63.2</td>
<td>95.5</td>
</tr>
<tr class="odd">
<td>76</td>
<td>LB-S17-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-17</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>64.7</td>
<td>96.2</td>
</tr>
<tr class="even">
<td>77</td>
<td>LB-S18-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-18</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>66.1</td>
<td>98.6</td>
</tr>
<tr class="odd">
<td>78</td>
<td>LB-S18-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-18</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>67.5</td>
<td>99.2</td>
</tr>
<tr class="even">
<td>79</td>
<td>LB-S18-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-18</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>67.5</td>
<td>99.2</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: BLM of the LB sections (S01--S18)

 

**Table:** BLM of the LB sections (S19--S30)

<table>
<caption>Table: BLM of the LB sections (S19–S30)</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td>LB-SECTIONS (S19-S30)
<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<tbody>
<tr class="odd">
<td>80</td>
<td>LB-S19-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-19</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>68.9</td>
<td>101.6</td>
</tr>
<tr class="even">
<td>81</td>
<td>LB-S19-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-19</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>70.3</td>
<td>102.3</td>
</tr>
<tr class="odd">
<td>82</td>
<td>LB-S20-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-20</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>71.7</td>
<td>104.7</td>
</tr>
<tr class="even">
<td>83</td>
<td>LB-S20-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-20</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>73.1</td>
<td>105.4</td>
</tr>
<tr class="odd">
<td>84</td>
<td>LB-S21-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-21</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>74.6</td>
<td>107.8</td>
</tr>
<tr class="even">
<td>85</td>
<td>LB-S21-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-21</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>76.0</td>
<td>108.5</td>
</tr>
<tr class="odd">
<td>86</td>
<td>LB-S21-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-21</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>76.0</td>
<td>108.5</td>
</tr>
<tr class="even">
<td>87</td>
<td>LB-S22-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-22</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>77.4</td>
<td>110.9</td>
</tr>
<tr class="odd">
<td>88</td>
<td>LB-S22-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-22</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>78.8</td>
<td>111.6</td>
</tr>
<tr class="even">
<td>89</td>
<td>LB-S23-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-23</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>80.2</td>
<td>114.0</td>
</tr>
<tr class="odd">
<td>90</td>
<td>LB-S23-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-23</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>81.6</td>
<td>114.6</td>
</tr>
<tr class="even">
<td>91</td>
<td>LB-S24-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-24</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>83.0</td>
<td>117.0</td>
</tr>
<tr class="odd">
<td>92</td>
<td>LB-S24-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-24</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>84.4</td>
<td>117.7</td>
</tr>
<tr class="even">
<td>93</td>
<td>LB-S24-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-24</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>84.4</td>
<td>117.7</td>
</tr>
<tr class="odd">
<td>94</td>
<td>LB-S25-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-25</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>85.9</td>
<td>120.1</td>
</tr>
<tr class="even">
<td>95</td>
<td>LB-S25-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-25</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>87.3</td>
<td>120.8</td>
</tr>
<tr class="odd">
<td>96</td>
<td>LB-S26-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-26</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>88.7</td>
<td>123.2</td>
</tr>
<tr class="even">
<td>97</td>
<td>LB-S26-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-26</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>90.1</td>
<td>123.9</td>
</tr>
<tr class="odd">
<td>98</td>
<td>LB-S27-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-27</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>91.5</td>
<td>126.3</td>
</tr>
<tr class="even">
<td>99</td>
<td>LB-S27-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-27</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>92.9</td>
<td>127.0</td>
</tr>
<tr class="odd">
<td>100</td>
<td>LB-S27-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-27</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>92.9</td>
<td>127.0</td>
</tr>
<tr class="even">
<td>101</td>
<td>LB-S28-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-28</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>94.3</td>
<td>129.4</td>
</tr>
<tr class="odd">
<td>102</td>
<td>LB-S28-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-28</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>95.8</td>
<td>130.0</td>
</tr>
<tr class="even">
<td>103</td>
<td>LB-S29-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-29</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>97.2</td>
<td>132.4</td>
</tr>
<tr class="odd">
<td>104</td>
<td>LB-S29-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-29</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>98.6</td>
<td>133.1</td>
</tr>
<tr class="even">
<td>105</td>
<td>LB-S30-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-30</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>100.0</td>
<td>135.5</td>
</tr>
<tr class="odd">
<td>106</td>
<td>LB-S30-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-30</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1ms</td>
<td>101.4</td>
<td>136.2</td>
</tr>
<tr class="even">
<td>107</td>
<td>LB-S30-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-30</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>101.4</td>
<td>136.2</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: BLM of the LB sections (S19--S30)

 

**Table:** Provisional beam instrumentation needs for the MINERVA
Accelerator.

<table>
<caption>Table: Provisional beam instrumentation needs for the MINERVA Accelerator.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Beam Instrumentation</td>
<td>Injector</td>
<td>MEBT-3</td>
<td>SC LinacMain linac 100 MeV</td>
<td>100 MeV</td>
<td>Total</td>
</tr>
<tr class="even">
<td>Beam position monitors</td>
<td>6</td>
<td>8</td>
<td>50</td>
<td>15</td>
<td>79</td>
</tr>
<tr class="odd">
<td>Beam transverse profilers</td>
<td>10</td>
<td>6</td>
<td>10</td>
<td>15</td>
<td>41</td>
</tr>
<tr class="even">
<td>Emittance-metres</td>
<td>4</td>
<td>4</td>
<td>-</td>
<td>-</td>
<td>8</td>
</tr>
<tr class="odd">
<td>Beam current monitors</td>
<td>6</td>
<td>4</td>
<td>2</td>
<td>2 to 6</td>
<td>14 to 18</td>
</tr>
<tr class="even">
<td>Faraday cups</td>
<td>8</td>
<td>8</td>
<td>-</td>
<td>-</td>
<td>16</td>
</tr>
<tr class="odd">
<td>Beam energy monitors</td>
<td>2</td>
<td>2</td>
<td>-</td>
<td>1</td>
<td>5</td>
</tr>
<tr class="even">
<td>Bunch length monitors</td>
<td>4</td>
<td>2</td>
<td>12</td>
<td>-</td>
<td>18</td>
</tr>
<tr class="odd">
<td>Beam loss monitors</td>
<td>24</td>
<td>13</td>
<td>70</td>
<td>&gt; 12</td>
<td>&gt; 119</td>
</tr>
<tr class="even">
<td>Halo monitors and slits</td>
<td>5</td>
<td>5</td>
<td>-</td>
<td>13</td>
<td>23</td>
</tr>
<tr class="odd">
<td>Beam monitoring on target</td>
<td>-</td>
<td>-</td>
<td>-</td>
<td>1</td>
<td>1</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Provisional beam instrumentation needs for the MINERVA
Accelerator.

 

**Table:** BPM type and location along the accelerator.

<table>
<caption>Table: BPM type and location along the accelerator.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th>Proton Energy</th>
<th> BPM type</th>
<th>BPM Location</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>1.516.67MeV</td>
<td>Buttons</td>
<td>Between CH cavities</td>
</tr>
<tr class="even">
<td>16.67100MeV</td>
<td>Buttons</td>
<td>Inside quadrupoles</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: BPM type and location along the accelerator.

 

**Table:** Summary of the main characteristics of the detectors commonly
used in BLM systems.

<table>
<caption>Table: Summary of the main characteristics of the detectors commonly used in BLM systems.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Ionisa-</td>
<td>Photomulti-</td>
<td>Neutron</td>
<td>PlN Diodes</td>
<td>Charge</td>
</tr>
<tr class="even">
<td> </td>
<td>tion</td>
<td>plier tube</td>
<td>sensitive</td>
<td> </td>
<td>multiplying</td>
</tr>
<tr class="odd">
<td> </td>
<td>Chamber</td>
<td>+ PMT</td>
<td>scintillator</td>
<td> </td>
<td>structures</td>
</tr>
<tr class="even">
<td>Detector type</td>
<td>Gas-filled</td>
<td>Plastic</td>
<td>Neutron sensitive</td>
<td>Solid</td>
<td>Micromegas</td>
</tr>
<tr class="odd">
<td> </td>
<td>ionisation</td>
<td>scintillator +</td>
<td>plastic scintil-</td>
<td>state</td>
<td>(nBLM), Neutron sen-</td>
</tr>
<tr class="even">
<td> </td>
<td>chamber</td>
<td>PMT</td>
<td>lator + PMT</td>
<td>detector</td>
<td>sitive; PC tubes</td>
</tr>
<tr class="odd">
<td> </td>
<td>Ar, N2</td>
<td> </td>
<td> </td>
<td>doped</td>
<td>PTE moderated</td>
</tr>
<tr class="even">
<td>Sensitive</td>
<td> </td>
<td>NaI(Tl), BGO,</td>
<td>PTE moderator,</td>
<td>Si-crystal+</td>
<td>+convector,</td>
</tr>
<tr class="odd">
<td>medium</td>
<td> </td>
<td>EJ-208</td>
<td>ZnS scint.</td>
<td>harge</td>
<td>Micromegas:</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td>convector</td>
<td>He+10% CO2</td>
</tr>
<tr class="odd">
<td>Typical HV, V</td>
<td>1000 (negative)</td>
<td>700–1000 (neg.)</td>
<td>700–1000 (neg.)</td>
<td>30 V to 40 V</td>
<td>800 V (positive)</td>
</tr>
<tr class="even">
<td>Typical response</td>
<td>≈2 <strong>TBD: inline7236, img69</strong> s (fast)</td>
<td>≈10 ns (fast)</td>
<td>-</td>
<td>≈5 ns (fast)</td>
<td>≈20 ns (fast)</td>
</tr>
<tr class="odd">
<td>time (fast, slow)</td>
<td>≈1 ms (Slow)</td>
<td> </td>
<td>≈50 <strong>TBD: inline7236, img69</strong> s (Slow)</td>
<td> </td>
<td>≈200 <strong>TBD: inline7236, img69</strong> s (Slow)</td>
</tr>
<tr class="even">
<td>Particle sensitivity</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>neutron</td>
<td>neutron</td>
<td>neutron</td>
</tr>
<tr class="odd">
<td>Energy range</td>
<td>&gt; 500keV</td>
<td>&gt; 60keV</td>
<td>0.03 eV to 3MeV</td>
<td>10keV-10MeV</td>
<td>1 eV - 100MeV</td>
</tr>
<tr class="even">
<td>Dynamic range</td>
<td>Up to 10<sup>8</sup></td>
<td>10<sup>8</sup> achievable</td>
<td> </td>
<td>10<sup>4</sup>- 10<sup>6</sup></td>
<td>Up to 10<sup>5</sup></td>
</tr>
<tr class="odd">
<td>Typical</td>
<td>0.1 – 1 <strong>TBD: inline7236, img69</strong> C/Gy</td>
<td>2mA/R/h</td>
<td>80 pC/n/cm<sup>2</sup></td>
<td>≈1 pC/n/cm<sup>2</sup></td>
<td>about mV and</td>
</tr>
<tr class="even">
<td>sensitivity</td>
<td> </td>
<td>(50pC/MeV)</td>
<td> </td>
<td> </td>
<td>mA achievable</td>
</tr>
<tr class="odd">
<td>Operation mode</td>
<td>current</td>
<td>pulse</td>
<td>Current</td>
<td>Current/pulse</td>
<td>Current</td>
</tr>
<tr class="even">
<td>(Pulse, current)</td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td>&amp; pulse</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Summary of the main characteristics of the detectors commonly
used in BLM systems.

 

**Table:** Existing High Power Hadron Accelerators and related BLM
Systems.

<table>
<caption>Table: Existing High Power Hadron Accelerators and related BLM Systems.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
</colgroup>
<tbody>
<tr class="odd">
<td>#</td>
<td>HPHA Machines</td>
<td>Beam Current</td>
<td>Beam Energy</td>
<td>Nominal Power</td>
<td>Current Status</td>
<td>BLM System</td>
</tr>
<tr class="even">
<td>–   –</td>
<td> </td>
<td>(mA)</td>
<td>(GeV)</td>
<td>(MW)</td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>  —  
1</td>
<td>FNAL MI-60</td>
<td>0.003</td>
<td>120</td>
<td>0.36</td>
<td>Existing</td>
<td>IC+ Scintillator + PMT</td>
</tr>
<tr class="even">
<td>2</td>
<td>PS</td>
<td>0.0015</td>
<td>25</td>
<td>0.038</td>
<td>Existing</td>
<td>IC (two types: high &amp; low)</td>
</tr>
<tr class="odd">
<td>3</td>
<td>SPL</td>
<td>0.8</td>
<td>5</td>
<td>4</td>
<td>Planned</td>
<td>IC (two types: high &amp; low)</td>
</tr>
<tr class="even">
<td>4</td>
<td>LINAC4</td>
<td>0.03</td>
<td>0.16</td>
<td>0.005</td>
<td>U/Constr.</td>
<td>IC+ Scintillator/PMT</td>
</tr>
<tr class="odd">
<td>5</td>
<td>TRIUMF</td>
<td>0.25</td>
<td>0.5</td>
<td>0.125</td>
<td>Existing</td>
<td>IC(Argon)+BGO crystal/PMT</td>
</tr>
<tr class="even">
<td>6</td>
<td>SNS-1/ORNL</td>
<td>1.2</td>
<td>0.9</td>
<td>1.08</td>
<td>Existing</td>
<td>IC+NE102 Plastic Scintillator/PMT</td>
</tr>
<tr class="odd">
<td>7</td>
<td>SNS-2/ORNL</td>
<td>1.5</td>
<td>1</td>
<td>1.5</td>
<td>Existing</td>
<td>IC+NE102 Plastic Scintillator/PMT</td>
</tr>
<tr class="even">
<td>8</td>
<td>PSI</td>
<td>2</td>
<td>0.6</td>
<td>1.2</td>
<td>Existing</td>
<td>IC</td>
</tr>
<tr class="odd">
<td>9</td>
<td>ESS</td>
<td>2.5</td>
<td>2</td>
<td>5</td>
<td>U/Constr.</td>
<td>IC + nBLM (micromegas)</td>
</tr>
<tr class="even">
<td>10</td>
<td>SPIRAL-2</td>
<td>5</td>
<td>0.04</td>
<td>0.2</td>
<td>U/Constr.</td>
<td>Scintillator + Thermal BLM</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Existing High Power Hadron Accelerators and related BLM Systems.

 

**Table:** Distribution of the 107 BLM Detectors proposed for the
MINERVA BLM Systems.

<table>
<caption>Table: Distribution of the 107 BLM Detectors proposed for the MINERVA BLM Systems.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Accelerator section</td>
<td>5.9 Injector-MEBT-2</td>
<td>(CH8–CH15)</td>
<td>MEBT-3</td>
<td>SC LinacMain Linac</td>
<td>TOTAL</td>
</tr>
<tr class="even">
<td>Ionisation Chambers</td>
<td>1</td>
<td>3</td>
<td>6</td>
<td>60
(2 per cryomodule)</td>
<td>70</td>
</tr>
<tr class="odd">
<td>Neutron Detectors
(1 per ≈ 1 m)</td>
<td>10</td>
<td>8</td>
<td>6</td>
<td>-</td>
<td>24</td>
</tr>
<tr class="even">
<td>Photomultiplier tubes</td>
<td>1</td>
<td>1</td>
<td>1</td>
<td>10</td>
<td>13</td>
</tr>
<tr class="odd">
<td>Subtotal</td>
<td>12</td>
<td>12</td>
<td>13</td>
<td>70</td>
<td>107</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Distribution of the 107 BLM Detectors proposed for the MINERVA
BLM Systems.

  

**Table:** The MINERVA 107 BLM system distribution and naming
convention.

 

**Table:** BLM of the beam line (SC Linacmain linac) system -- LB-ME

3\.

<table>
<caption>Table: BLM of the beam line (SC Linacmain linac) system – LB-ME3.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td>INJECTOR1 – MEBT3
<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<tbody>
<tr class="odd">
<td>25</td>
<td>I1-ME3-BD-BLM-ND-01</td>
<td>nBLM</td>
<td>MEBT3</td>
<td>Neutron Detector</td>
<td>n</td>
<td>&lt; 3.5MeV</td>
<td>&gt; 0.05ms</td>
<td>16.6</td>
<td>32.5</td>
</tr>
<tr class="even">
<td>26</td>
<td>I1-ME3-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>MEBT3</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1ms</td>
<td>16.6</td>
<td>32.5</td>
</tr>
<tr class="odd">
<td>27</td>
<td>I1-ME3-BD-BLM-ND-02</td>
<td>nBLM</td>
<td>MEBT3</td>
<td>Neutron Detector</td>
<td>n</td>
<td>&lt; 3.5MeV</td>
<td>&gt; 0.05ms</td>
<td>16.6</td>
<td>34.2</td>
</tr>
<tr class="even">
<td>28</td>
<td>I1-ME3-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>MEBT3</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1ms</td>
<td>16.6</td>
<td>34.2</td>
</tr>
<tr class="odd">
<td>29</td>
<td>I1-ME3-BD-BLM-ND-03</td>
<td>nBLM</td>
<td>MEBT3</td>
<td>Neutron Detector</td>
<td>n</td>
<td>&lt; 3.5MeV</td>
<td>&gt; 0.05ms</td>
<td>16.6</td>
<td>36.93</td>
</tr>
<tr class="even">
<td>30</td>
<td>I1-ME3-BD-BLM-IC-03</td>
<td>iBLM</td>
<td>MEBT3</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1ms</td>
<td>16.6</td>
<td>36.93</td>
</tr>
<tr class="odd">
<td>31</td>
<td>I1-ME3-BD-BLM-ND-04</td>
<td>nBLM</td>
<td>MEBT3</td>
<td>Neutron Detector</td>
<td>n</td>
<td>&lt; 3.5MeV</td>
<td>&gt; 0.05ms</td>
<td>16.6</td>
<td>38.07</td>
</tr>
<tr class="even">
<td>32</td>
<td>I1-ME3-BD-BLM-IC-04</td>
<td>iBLM</td>
<td>MEBT3</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1ms</td>
<td>16.6</td>
<td>38.07</td>
</tr>
<tr class="odd">
<td>37</td>
<td>I1-ME3-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>MEBT3</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>16.6</td>
<td>38.07</td>
</tr>
</tbody>
</table>
<p>BEAM LINE (MAIN LINAC) System - LB-MEBT3</p>
<p> </p>
<p><strong>Table:</strong> BLM between Injector 1 and MEBT-3.</p>
<table style="width:100%;">
<caption>Table: BLM between Injector 1 and MEBT-3.</caption>
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<tbody>
<tr class="odd">
<td>38</td>
<td>I1-ME3-BD-BLM-ND-05</td>
<td>nBLM</td>
<td>LB - MEBT3</td>
<td>Neutron Detector</td>
<td>n</td>
<td>&lt; 3.5MeV</td>
<td>&gt; 0.05ms</td>
<td>16.6</td>
<td>41.0</td>
</tr>
<tr class="even">
<td>39</td>
<td>I1-ME3-BD-BLM-IC-05</td>
<td>iBLM</td>
<td>LB - MEBT3</td>
<td>ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1ms</td>
<td>16.6</td>
<td>41.0</td>
</tr>
<tr class="odd">
<td>40</td>
<td>I1-ME3-BD-BLM-ND-06</td>
<td>nBLM</td>
<td>LB - MEBT3</td>
<td>Neutron Detector</td>
<td>n</td>
<td>&lt; 3.5MeV</td>
<td>&gt; 0.05ms</td>
<td>16.6</td>
<td>42.8</td>
</tr>
<tr class="even">
<td>41</td>
<td>I1-ME3-BD-BLM-IC-06</td>
<td>iBLM</td>
<td>LB - MEBT3</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1ms</td>
<td>16.6</td>
<td>42.8</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: BLM of the beam line (SC Linacmain linac) system -- LB-ME3.

 

**Table:** BLM of the LB sections (S01--S18)

<table>
<caption>Table: BLM of the LB sections (S01–S18)</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td>LB-SECTIONS (S01-S18)
<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<tbody>
<tr class="odd">
<td>42</td>
<td>LB-S01-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-01</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>18.0</td>
<td>46.2</td>
</tr>
<tr class="even">
<td>43</td>
<td>LB-S01-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-01</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>19.4</td>
<td>46.9</td>
</tr>
<tr class="odd">
<td>44</td>
<td>LB-S02-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-02</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>20.8</td>
<td>49.3</td>
</tr>
<tr class="even">
<td>45</td>
<td>LB-S02-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-02</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>22.3</td>
<td>50.0</td>
</tr>
<tr class="odd">
<td>42</td>
<td>LB-S03-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-03</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>23.7</td>
<td>52.4</td>
</tr>
<tr class="even">
<td>43</td>
<td>LB-S03-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-03</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>25.1</td>
<td>53.0</td>
</tr>
<tr class="odd">
<td>44</td>
<td>LB-S03-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-03</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>25.1</td>
<td>53.0</td>
</tr>
<tr class="even">
<td>45</td>
<td>LB-S04-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-04</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>26.5</td>
<td>55.4</td>
</tr>
<tr class="odd">
<td>46</td>
<td>LB-S04-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-04</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>27.9</td>
<td>56.1</td>
</tr>
<tr class="even">
<td>47</td>
<td>LB-S05-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-05</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>29.3</td>
<td>58.5</td>
</tr>
<tr class="odd">
<td>48</td>
<td>LB-S05-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-05</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>30.7</td>
<td>59.2</td>
</tr>
<tr class="even">
<td>49</td>
<td>LB-S06-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-06</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>32.1</td>
<td>61.6</td>
</tr>
<tr class="odd">
<td>50</td>
<td>LB-S06-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-06</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>33.6</td>
<td>62.3</td>
</tr>
<tr class="even">
<td>51</td>
<td>LB-S06-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-06</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>33.6</td>
<td>62.3</td>
</tr>
<tr class="odd">
<td>52</td>
<td>LB-S07-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-07</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>35.0</td>
<td>64.7</td>
</tr>
<tr class="even">
<td>53</td>
<td>LB-S07-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-07</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>36.4</td>
<td>65.4</td>
</tr>
<tr class="odd">
<td>54</td>
<td>LB-S08-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-08</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>37.8</td>
<td>67.8</td>
</tr>
<tr class="even">
<td>55</td>
<td>LB-S08-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-08</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>39.2</td>
<td>68.4</td>
</tr>
<tr class="odd">
<td>56</td>
<td>LB-S09-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-09</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>40.6</td>
<td>70.8</td>
</tr>
<tr class="even">
<td>57</td>
<td>LB-S09-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-09</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>42.0</td>
<td>71.5</td>
</tr>
<tr class="odd">
<td>58</td>
<td>LB-S09-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-09</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>42.0</td>
<td>71.5</td>
</tr>
<tr class="even">
<td>59</td>
<td>LB-S10-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-10</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>43.5</td>
<td>73.9</td>
</tr>
<tr class="odd">
<td>60</td>
<td>LB-S10-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-10</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>44.9</td>
<td>74.6</td>
</tr>
<tr class="even">
<td>61</td>
<td>LB-S11-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-11</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>46.3</td>
<td>77.0</td>
</tr>
<tr class="odd">
<td>62</td>
<td>LB-S11-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-11</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>47.7</td>
<td>77.7</td>
</tr>
<tr class="even">
<td>63</td>
<td>LB-S12-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-12</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>49.1</td>
<td>80.1</td>
</tr>
<tr class="odd">
<td>64</td>
<td>LB-S12-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-12</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>50.5</td>
<td>80.8</td>
</tr>
<tr class="even">
<td>65</td>
<td>LB-S12-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-12</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>50.5</td>
<td>80.8</td>
</tr>
<tr class="odd">
<td>66</td>
<td>LB-S13-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-13</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>51.9</td>
<td>83.2</td>
</tr>
<tr class="even">
<td>67</td>
<td>LB-S13-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-13</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>53.4</td>
<td>83.8</td>
</tr>
<tr class="odd">
<td>68</td>
<td>LB-S14-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-14</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>54.8</td>
<td>86.2</td>
</tr>
<tr class="even">
<td>69</td>
<td>LB-S14-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-14</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>56.2</td>
<td>86.9</td>
</tr>
<tr class="odd">
<td>70</td>
<td>LB-S15-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-15</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>57.6</td>
<td>89.3</td>
</tr>
<tr class="even">
<td>71</td>
<td>LB-S15-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-15</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>59.0</td>
<td>90.0</td>
</tr>
<tr class="odd">
<td>72</td>
<td>LB-S15-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-15</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>59.0</td>
<td>90.0</td>
</tr>
<tr class="even">
<td>73</td>
<td>LB-S16-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-16</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>60.4</td>
<td>92.4</td>
</tr>
<tr class="odd">
<td>74</td>
<td>LB-S16-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-16</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>61.8</td>
<td>93.1</td>
</tr>
<tr class="even">
<td>75</td>
<td>LB-S17-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-17</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>63.2</td>
<td>95.5</td>
</tr>
<tr class="odd">
<td>76</td>
<td>LB-S17-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-17</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>64.7</td>
<td>96.2</td>
</tr>
<tr class="even">
<td>77</td>
<td>LB-S18-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-18</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>66.1</td>
<td>98.6</td>
</tr>
<tr class="odd">
<td>78</td>
<td>LB-S18-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-18</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>67.5</td>
<td>99.2</td>
</tr>
<tr class="even">
<td>79</td>
<td>LB-S18-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-18</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>67.5</td>
<td>99.2</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: BLM of the LB sections (S01--S18)

 

**Table:** BLM of the LB sections (S19--S30)

<table>
<caption>Table: BLM of the LB sections (S19–S30)</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td>LB-SECTIONS (S19-S30)
<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<tbody>
<tr class="odd">
<td>80</td>
<td>LB-S19-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-19</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>68.9</td>
<td>101.6</td>
</tr>
<tr class="even">
<td>81</td>
<td>LB-S19-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-19</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>70.3</td>
<td>102.3</td>
</tr>
<tr class="odd">
<td>82</td>
<td>LB-S20-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-20</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>71.7</td>
<td>104.7</td>
</tr>
<tr class="even">
<td>83</td>
<td>LB-S20-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-20</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>73.1</td>
<td>105.4</td>
</tr>
<tr class="odd">
<td>84</td>
<td>LB-S21-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-21</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>74.6</td>
<td>107.8</td>
</tr>
<tr class="even">
<td>85</td>
<td>LB-S21-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-21</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>76.0</td>
<td>108.5</td>
</tr>
<tr class="odd">
<td>86</td>
<td>LB-S21-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-21</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>76.0</td>
<td>108.5</td>
</tr>
<tr class="even">
<td>87</td>
<td>LB-S22-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-22</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>77.4</td>
<td>110.9</td>
</tr>
<tr class="odd">
<td>88</td>
<td>LB-S22-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-22</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>78.8</td>
<td>111.6</td>
</tr>
<tr class="even">
<td>89</td>
<td>LB-S23-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-23</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>80.2</td>
<td>114.0</td>
</tr>
<tr class="odd">
<td>90</td>
<td>LB-S23-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-23</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>81.6</td>
<td>114.6</td>
</tr>
<tr class="even">
<td>91</td>
<td>LB-S24-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-24</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>83.0</td>
<td>117.0</td>
</tr>
<tr class="odd">
<td>92</td>
<td>LB-S24-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-24</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>84.4</td>
<td>117.7</td>
</tr>
<tr class="even">
<td>93</td>
<td>LB-S24-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-24</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>84.4</td>
<td>117.7</td>
</tr>
<tr class="odd">
<td>94</td>
<td>LB-S25-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-25</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>85.9</td>
<td>120.1</td>
</tr>
<tr class="even">
<td>95</td>
<td>LB-S25-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-25</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>87.3</td>
<td>120.8</td>
</tr>
<tr class="odd">
<td>96</td>
<td>LB-S26-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-26</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>88.7</td>
<td>123.2</td>
</tr>
<tr class="even">
<td>97</td>
<td>LB-S26-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-26</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>90.1</td>
<td>123.9</td>
</tr>
<tr class="odd">
<td>98</td>
<td>LB-S27-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-27</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>91.5</td>
<td>126.3</td>
</tr>
<tr class="even">
<td>99</td>
<td>LB-S27-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-27</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>92.9</td>
<td>127.0</td>
</tr>
<tr class="odd">
<td>100</td>
<td>LB-S27-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-27</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>92.9</td>
<td>127.0</td>
</tr>
<tr class="even">
<td>101</td>
<td>LB-S28-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-28</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>94.3</td>
<td>129.4</td>
</tr>
<tr class="odd">
<td>102</td>
<td>LB-S28-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-28</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>95.8</td>
<td>130.0</td>
</tr>
<tr class="even">
<td>103</td>
<td>LB-S29-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-29</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>97.2</td>
<td>132.4</td>
</tr>
<tr class="odd">
<td>104</td>
<td>LB-S29-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-29</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>98.6</td>
<td>133.1</td>
</tr>
<tr class="even">
<td>105</td>
<td>LB-S30-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-30</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>100.0</td>
<td>135.5</td>
</tr>
<tr class="odd">
<td>106</td>
<td>LB-S30-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-30</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1ms</td>
<td>101.4</td>
<td>136.2</td>
</tr>
<tr class="even">
<td>107</td>
<td>LB-S30-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-30</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>101.4</td>
<td>136.2</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: BLM of the LB sections (S19--S30)

 

**Table:** Provisional beam instrumentation needs for the MINERVA
Accelerator.

<table>
<caption>Table: Provisional beam instrumentation needs for the MINERVA Accelerator.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Beam Instrumentation</td>
<td>Injector</td>
<td>MEBT-3</td>
<td>SC LinacMain linac 100 MeV</td>
<td>100 MeV</td>
<td>Total</td>
</tr>
<tr class="even">
<td>Beam position monitors</td>
<td>6</td>
<td>8</td>
<td>50</td>
<td>15</td>
<td>79</td>
</tr>
<tr class="odd">
<td>Beam transverse profilers</td>
<td>10</td>
<td>6</td>
<td>10</td>
<td>15</td>
<td>41</td>
</tr>
<tr class="even">
<td>Emittance-metres</td>
<td>4</td>
<td>4</td>
<td>-</td>
<td>-</td>
<td>8</td>
</tr>
<tr class="odd">
<td>Beam current monitors</td>
<td>6</td>
<td>4</td>
<td>2</td>
<td>2 to 6</td>
<td>14 to 18</td>
</tr>
<tr class="even">
<td>Faraday cups</td>
<td>8</td>
<td>8</td>
<td>-</td>
<td>-</td>
<td>16</td>
</tr>
<tr class="odd">
<td>Beam energy monitors</td>
<td>2</td>
<td>2</td>
<td>-</td>
<td>1</td>
<td>5</td>
</tr>
<tr class="even">
<td>Bunch length monitors</td>
<td>4</td>
<td>2</td>
<td>12</td>
<td>-</td>
<td>18</td>
</tr>
<tr class="odd">
<td>Beam loss monitors</td>
<td>24</td>
<td>13</td>
<td>70</td>
<td>&gt; 12</td>
<td>&gt; 119</td>
</tr>
<tr class="even">
<td>Halo monitors and slits</td>
<td>5</td>
<td>5</td>
<td>-</td>
<td>13</td>
<td>23</td>
</tr>
<tr class="odd">
<td>Beam monitoring on target</td>
<td>-</td>
<td>-</td>
<td>-</td>
<td>1</td>
<td>1</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Provisional beam instrumentation needs for the MINERVA
Accelerator.

 

**Table:** BPM type and location along the accelerator.

<table>
<caption>Table: BPM type and location along the accelerator.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th>Proton Energy</th>
<th> BPM type</th>
<th>BPM Location</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>1.516.67MeV</td>
<td>Buttons</td>
<td>Between CH cavities</td>
</tr>
<tr class="even">
<td>16.67100MeV</td>
<td>Buttons</td>
<td>Inside quadrupoles</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: BPM type and location along the accelerator.

 

**Table:** Summary of the main characteristics of the detectors commonly
used in BLM systems.

<table>
<caption>Table: Summary of the main characteristics of the detectors commonly used in BLM systems.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>Ionisa-</td>
<td>Photomulti-</td>
<td>Neutron</td>
<td>PlN Diodes</td>
<td>Charge</td>
</tr>
<tr class="even">
<td> </td>
<td>tion</td>
<td>plier tube</td>
<td>sensitive</td>
<td> </td>
<td>multiplying</td>
</tr>
<tr class="odd">
<td> </td>
<td>Chamber</td>
<td>+ PMT</td>
<td>scintillator</td>
<td> </td>
<td>structures</td>
</tr>
<tr class="even">
<td>Detector type</td>
<td>Gas-filled</td>
<td>Plastic</td>
<td>Neutron sensitive</td>
<td>Solid</td>
<td>Micromegas</td>
</tr>
<tr class="odd">
<td> </td>
<td>ionisation</td>
<td>scintillator +</td>
<td>plastic scintil-</td>
<td>state</td>
<td>(nBLM), Neutron sen-</td>
</tr>
<tr class="even">
<td> </td>
<td>chamber</td>
<td>PMT</td>
<td>lator + PMT</td>
<td>detector</td>
<td>sitive; PC tubes</td>
</tr>
<tr class="odd">
<td> </td>
<td>Ar, N2</td>
<td> </td>
<td> </td>
<td>doped</td>
<td>PTE moderated</td>
</tr>
<tr class="even">
<td>Sensitive</td>
<td> </td>
<td>NaI(Tl), BGO,</td>
<td>PTE moderator,</td>
<td>Si-crystal+</td>
<td>+convector,</td>
</tr>
<tr class="odd">
<td>medium</td>
<td> </td>
<td>EJ-208</td>
<td>ZnS scint.</td>
<td>harge</td>
<td>Micromegas:</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td>convector</td>
<td>He+10% CO2</td>
</tr>
<tr class="odd">
<td>Typical HV, V</td>
<td>1000 (negative)</td>
<td>700–1000 (neg.)</td>
<td>700–1000 (neg.)</td>
<td>30 V to 40 V</td>
<td>800 V (positive)</td>
</tr>
<tr class="even">
<td>Typical response</td>
<td>≈2 <strong>TBD: inline7236, img69</strong> s (fast)</td>
<td>≈10 ns (fast)</td>
<td>-</td>
<td>≈5 ns (fast)</td>
<td>≈20 ns (fast)</td>
</tr>
<tr class="odd">
<td>time (fast, slow)</td>
<td>≈1 ms (Slow)</td>
<td> </td>
<td>≈50 <strong>TBD: inline7236, img69</strong> s (Slow)</td>
<td> </td>
<td>≈200 <strong>TBD: inline7236, img69</strong> s (Slow)</td>
</tr>
<tr class="even">
<td>Particle sensitivity</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>neutron</td>
<td>neutron</td>
<td>neutron</td>
</tr>
<tr class="odd">
<td>Energy range</td>
<td>&gt; 500keV</td>
<td>&gt; 60keV</td>
<td>0.03 eV to 3MeV</td>
<td>10keV-10MeV</td>
<td>1 eV - 100MeV</td>
</tr>
<tr class="even">
<td>Dynamic range</td>
<td>Up to 10<sup>8</sup></td>
<td>10<sup>8</sup> achievable</td>
<td> </td>
<td>10<sup>4</sup>- 10<sup>6</sup></td>
<td>Up to 10<sup>5</sup></td>
</tr>
<tr class="odd">
<td>Typical</td>
<td>0.1 – 1 <strong>TBD: inline7236, img69</strong> C/Gy</td>
<td>2mA/R/h</td>
<td>80 pC/n/cm<sup>2</sup></td>
<td>≈1 pC/n/cm<sup>2</sup></td>
<td>about mV and</td>
</tr>
<tr class="even">
<td>sensitivity</td>
<td> </td>
<td>(50pC/MeV)</td>
<td> </td>
<td> </td>
<td>mA achievable</td>
</tr>
<tr class="odd">
<td>Operation mode</td>
<td>current</td>
<td>pulse</td>
<td>Current</td>
<td>Current/pulse</td>
<td>Current</td>
</tr>
<tr class="even">
<td>(Pulse, current)</td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td>&amp; pulse</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Summary of the main characteristics of the detectors commonly
used in BLM systems.

 

**Table:** Existing High Power Hadron Accelerators and related BLM
Systems.

<table>
<caption>Table: Existing High Power Hadron Accelerators and related BLM Systems.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
</colgroup>
<tbody>
<tr class="odd">
<td>#</td>
<td>HPHA Machines</td>
<td>Beam Current</td>
<td>Beam Energy</td>
<td>Nominal Power</td>
<td>Current Status</td>
<td>BLM System</td>
</tr>
<tr class="even">
<td>–   –</td>
<td> </td>
<td>(mA)</td>
<td>(GeV)</td>
<td>(MW)</td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>  —  
1</td>
<td>FNAL MI-60</td>
<td>0.003</td>
<td>120</td>
<td>0.36</td>
<td>Existing</td>
<td>IC+ Scintillator + PMT</td>
</tr>
<tr class="even">
<td>2</td>
<td>PS</td>
<td>0.0015</td>
<td>25</td>
<td>0.038</td>
<td>Existing</td>
<td>IC (two types: high &amp; low)</td>
</tr>
<tr class="odd">
<td>3</td>
<td>SPL</td>
<td>0.8</td>
<td>5</td>
<td>4</td>
<td>Planned</td>
<td>IC (two types: high &amp; low)</td>
</tr>
<tr class="even">
<td>4</td>
<td>LINAC4</td>
<td>0.03</td>
<td>0.16</td>
<td>0.005</td>
<td>U/Constr.</td>
<td>IC+ Scintillator/PMT</td>
</tr>
<tr class="odd">
<td>5</td>
<td>TRIUMF</td>
<td>0.25</td>
<td>0.5</td>
<td>0.125</td>
<td>Existing</td>
<td>IC(Argon)+BGO crystal/PMT</td>
</tr>
<tr class="even">
<td>6</td>
<td>SNS-1/ORNL</td>
<td>1.2</td>
<td>0.9</td>
<td>1.08</td>
<td>Existing</td>
<td>IC+NE102 Plastic Scintillator/PMT</td>
</tr>
<tr class="odd">
<td>7</td>
<td>SNS-2/ORNL</td>
<td>1.5</td>
<td>1</td>
<td>1.5</td>
<td>Existing</td>
<td>IC+NE102 Plastic Scintillator/PMT</td>
</tr>
<tr class="even">
<td>8</td>
<td>PSI</td>
<td>2</td>
<td>0.6</td>
<td>1.2</td>
<td>Existing</td>
<td>IC</td>
</tr>
<tr class="odd">
<td>9</td>
<td>ESS</td>
<td>2.5</td>
<td>2</td>
<td>5</td>
<td>U/Constr.</td>
<td>IC + nBLM (micromegas)</td>
</tr>
<tr class="even">
<td>10</td>
<td>SPIRAL-2</td>
<td>5</td>
<td>0.04</td>
<td>0.2</td>
<td>U/Constr.</td>
<td>Scintillator + Thermal BLM</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Existing High Power Hadron Accelerators and related BLM Systems.

 

**Table:** Distribution of the 107 BLM Detectors proposed for the
MINERVA BLM Systems.

<table>
<caption>Table: Distribution of the 107 BLM Detectors proposed for the MINERVA BLM Systems.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Accelerator section</td>
<td>5.9 Injector-MEBT-2</td>
<td>(CH8–CH15)</td>
<td>MEBT-3</td>
<td>SC LinacMain Linac</td>
<td>TOTAL</td>
</tr>
<tr class="even">
<td>Ionisation Chambers</td>
<td>1</td>
<td>3</td>
<td>6</td>
<td>60
(2 per cryomodule)</td>
<td>70</td>
</tr>
<tr class="odd">
<td>Neutron Detectors
(1 per ≈ 1 m)</td>
<td>10</td>
<td>8</td>
<td>6</td>
<td>-</td>
<td>24</td>
</tr>
<tr class="even">
<td>Photomultiplier tubes</td>
<td>1</td>
<td>1</td>
<td>1</td>
<td>10</td>
<td>13</td>
</tr>
<tr class="odd">
<td>Subtotal</td>
<td>12</td>
<td>12</td>
<td>13</td>
<td>70</td>
<td>107</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Distribution of the 107 BLM Detectors proposed for the MINERVA
BLM Systems.

  

**Table:** The MINERVA 107 BLM system distribution and naming
convention.

 

**Table:** BLM of the beam line (SC Linacmain linac) system -- LB-ME

3\.

<table>
<caption>Table: BLM of the beam line (SC Linacmain linac) system – LB-ME3.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td>INJECTOR1 – MEBT3
<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<tbody>
<tr class="odd">
<td>25</td>
<td>I1-ME3-BD-BLM-ND-01</td>
<td>nBLM</td>
<td>MEBT3</td>
<td>Neutron Detector</td>
<td>n</td>
<td>&lt; 3.5MeV</td>
<td>&gt; 0.05ms</td>
<td>16.6</td>
<td>32.5</td>
</tr>
<tr class="even">
<td>26</td>
<td>I1-ME3-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>MEBT3</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1ms</td>
<td>16.6</td>
<td>32.5</td>
</tr>
<tr class="odd">
<td>27</td>
<td>I1-ME3-BD-BLM-ND-02</td>
<td>nBLM</td>
<td>MEBT3</td>
<td>Neutron Detector</td>
<td>n</td>
<td>&lt; 3.5MeV</td>
<td>&gt; 0.05ms</td>
<td>16.6</td>
<td>34.2</td>
</tr>
<tr class="even">
<td>28</td>
<td>I1-ME3-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>MEBT3</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1ms</td>
<td>16.6</td>
<td>34.2</td>
</tr>
<tr class="odd">
<td>29</td>
<td>I1-ME3-BD-BLM-ND-03</td>
<td>nBLM</td>
<td>MEBT3</td>
<td>Neutron Detector</td>
<td>n</td>
<td>&lt; 3.5MeV</td>
<td>&gt; 0.05ms</td>
<td>16.6</td>
<td>36.93</td>
</tr>
<tr class="even">
<td>30</td>
<td>I1-ME3-BD-BLM-IC-03</td>
<td>iBLM</td>
<td>MEBT3</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1ms</td>
<td>16.6</td>
<td>36.93</td>
</tr>
<tr class="odd">
<td>31</td>
<td>I1-ME3-BD-BLM-ND-04</td>
<td>nBLM</td>
<td>MEBT3</td>
<td>Neutron Detector</td>
<td>n</td>
<td>&lt; 3.5MeV</td>
<td>&gt; 0.05ms</td>
<td>16.6</td>
<td>38.07</td>
</tr>
<tr class="even">
<td>32</td>
<td>I1-ME3-BD-BLM-IC-04</td>
<td>iBLM</td>
<td>MEBT3</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1ms</td>
<td>16.6</td>
<td>38.07</td>
</tr>
<tr class="odd">
<td>37</td>
<td>I1-ME3-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>MEBT3</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>16.6</td>
<td>38.07</td>
</tr>
</tbody>
</table>
<p>BEAM LINE (MAIN LINAC) System - LB-MEBT3</p>
<p> </p>
<p><strong>Table:</strong> BLM between Injector 1 and MEBT-3.</p>
<table style="width:100%;">
<caption>Table: BLM between Injector 1 and MEBT-3.</caption>
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<tbody>
<tr class="odd">
<td>38</td>
<td>I1-ME3-BD-BLM-ND-05</td>
<td>nBLM</td>
<td>LB - MEBT3</td>
<td>Neutron Detector</td>
<td>n</td>
<td>&lt; 3.5MeV</td>
<td>&gt; 0.05ms</td>
<td>16.6</td>
<td>41.0</td>
</tr>
<tr class="even">
<td>39</td>
<td>I1-ME3-BD-BLM-IC-05</td>
<td>iBLM</td>
<td>LB - MEBT3</td>
<td>ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1ms</td>
<td>16.6</td>
<td>41.0</td>
</tr>
<tr class="odd">
<td>40</td>
<td>I1-ME3-BD-BLM-ND-06</td>
<td>nBLM</td>
<td>LB - MEBT3</td>
<td>Neutron Detector</td>
<td>n</td>
<td>&lt; 3.5MeV</td>
<td>&gt; 0.05ms</td>
<td>16.6</td>
<td>42.8</td>
</tr>
<tr class="even">
<td>41</td>
<td>I1-ME3-BD-BLM-IC-06</td>
<td>iBLM</td>
<td>LB - MEBT3</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1ms</td>
<td>16.6</td>
<td>42.8</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: BLM of the beam line (SC Linacmain linac) system -- LB-ME3.

 

**Table:** BLM of the LB sections (S01--S18)

<table>
<caption>Table: BLM of the LB sections (S01–S18)</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td>LB-SECTIONS (S01-S18)
<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<tbody>
<tr class="odd">
<td>42</td>
<td>LB-S01-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-01</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>18.0</td>
<td>46.2</td>
</tr>
<tr class="even">
<td>43</td>
<td>LB-S01-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-01</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>19.4</td>
<td>46.9</td>
</tr>
<tr class="odd">
<td>44</td>
<td>LB-S02-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-02</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>20.8</td>
<td>49.3</td>
</tr>
<tr class="even">
<td>45</td>
<td>LB-S02-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-02</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>22.3</td>
<td>50.0</td>
</tr>
<tr class="odd">
<td>42</td>
<td>LB-S03-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-03</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>23.7</td>
<td>52.4</td>
</tr>
<tr class="even">
<td>43</td>
<td>LB-S03-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-03</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>25.1</td>
<td>53.0</td>
</tr>
<tr class="odd">
<td>44</td>
<td>LB-S03-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-03</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>25.1</td>
<td>53.0</td>
</tr>
<tr class="even">
<td>45</td>
<td>LB-S04-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-04</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>26.5</td>
<td>55.4</td>
</tr>
<tr class="odd">
<td>46</td>
<td>LB-S04-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-04</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>27.9</td>
<td>56.1</td>
</tr>
<tr class="even">
<td>47</td>
<td>LB-S05-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-05</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>29.3</td>
<td>58.5</td>
</tr>
<tr class="odd">
<td>48</td>
<td>LB-S05-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-05</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>30.7</td>
<td>59.2</td>
</tr>
<tr class="even">
<td>49</td>
<td>LB-S06-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-06</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>32.1</td>
<td>61.6</td>
</tr>
<tr class="odd">
<td>50</td>
<td>LB-S06-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-06</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>33.6</td>
<td>62.3</td>
</tr>
<tr class="even">
<td>51</td>
<td>LB-S06-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-06</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>33.6</td>
<td>62.3</td>
</tr>
<tr class="odd">
<td>52</td>
<td>LB-S07-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-07</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>35.0</td>
<td>64.7</td>
</tr>
<tr class="even">
<td>53</td>
<td>LB-S07-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-07</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>36.4</td>
<td>65.4</td>
</tr>
<tr class="odd">
<td>54</td>
<td>LB-S08-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-08</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>37.8</td>
<td>67.8</td>
</tr>
<tr class="even">
<td>55</td>
<td>LB-S08-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-08</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>39.2</td>
<td>68.4</td>
</tr>
<tr class="odd">
<td>56</td>
<td>LB-S09-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-09</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>40.6</td>
<td>70.8</td>
</tr>
<tr class="even">
<td>57</td>
<td>LB-S09-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-09</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>42.0</td>
<td>71.5</td>
</tr>
<tr class="odd">
<td>58</td>
<td>LB-S09-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-09</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>42.0</td>
<td>71.5</td>
</tr>
<tr class="even">
<td>59</td>
<td>LB-S10-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-10</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>43.5</td>
<td>73.9</td>
</tr>
<tr class="odd">
<td>60</td>
<td>LB-S10-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-10</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>44.9</td>
<td>74.6</td>
</tr>
<tr class="even">
<td>61</td>
<td>LB-S11-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-11</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>46.3</td>
<td>77.0</td>
</tr>
<tr class="odd">
<td>62</td>
<td>LB-S11-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-11</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>47.7</td>
<td>77.7</td>
</tr>
<tr class="even">
<td>63</td>
<td>LB-S12-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-12</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>49.1</td>
<td>80.1</td>
</tr>
<tr class="odd">
<td>64</td>
<td>LB-S12-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-12</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>50.5</td>
<td>80.8</td>
</tr>
<tr class="even">
<td>65</td>
<td>LB-S12-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-12</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>50.5</td>
<td>80.8</td>
</tr>
<tr class="odd">
<td>66</td>
<td>LB-S13-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-13</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>51.9</td>
<td>83.2</td>
</tr>
<tr class="even">
<td>67</td>
<td>LB-S13-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-13</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>53.4</td>
<td>83.8</td>
</tr>
<tr class="odd">
<td>68</td>
<td>LB-S14-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-14</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>54.8</td>
<td>86.2</td>
</tr>
<tr class="even">
<td>69</td>
<td>LB-S14-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-14</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>56.2</td>
<td>86.9</td>
</tr>
<tr class="odd">
<td>70</td>
<td>LB-S15-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-15</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>57.6</td>
<td>89.3</td>
</tr>
<tr class="even">
<td>71</td>
<td>LB-S15-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-15</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>59.0</td>
<td>90.0</td>
</tr>
<tr class="odd">
<td>72</td>
<td>LB-S15-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-15</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>59.0</td>
<td>90.0</td>
</tr>
<tr class="even">
<td>73</td>
<td>LB-S16-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-16</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>60.4</td>
<td>92.4</td>
</tr>
<tr class="odd">
<td>74</td>
<td>LB-S16-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-16</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>61.8</td>
<td>93.1</td>
</tr>
<tr class="even">
<td>75</td>
<td>LB-S17-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-17</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>63.2</td>
<td>95.5</td>
</tr>
<tr class="odd">
<td>76</td>
<td>LB-S17-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-17</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>64.7</td>
<td>96.2</td>
</tr>
<tr class="even">
<td>77</td>
<td>LB-S18-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-18</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>66.1</td>
<td>98.6</td>
</tr>
<tr class="odd">
<td>78</td>
<td>LB-S18-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-18</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>67.5</td>
<td>99.2</td>
</tr>
<tr class="even">
<td>79</td>
<td>LB-S18-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-18</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>67.5</td>
<td>99.2</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: BLM of the LB sections (S01--S18)

 

**Table:** BLM of the LB sections (S19--S30)

<table>
<caption>Table: BLM of the LB sections (S19–S30)</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td>LB-SECTIONS (S19-S30)
<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<tbody>
<tr class="odd">
<td>80</td>
<td>LB-S19-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-19</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>68.9</td>
<td>101.6</td>
</tr>
<tr class="even">
<td>81</td>
<td>LB-S19-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-19</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>70.3</td>
<td>102.3</td>
</tr>
<tr class="odd">
<td>82</td>
<td>LB-S20-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-20</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>71.7</td>
<td>104.7</td>
</tr>
<tr class="even">
<td>83</td>
<td>LB-S20-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-20</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>73.1</td>
<td>105.4</td>
</tr>
<tr class="odd">
<td>84</td>
<td>LB-S21-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-21</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>74.6</td>
<td>107.8</td>
</tr>
<tr class="even">
<td>85</td>
<td>LB-S21-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-21</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>76.0</td>
<td>108.5</td>
</tr>
<tr class="odd">
<td>86</td>
<td>LB-S21-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-21</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>76.0</td>
<td>108.5</td>
</tr>
<tr class="even">
<td>87</td>
<td>LB-S22-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-22</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>77.4</td>
<td>110.9</td>
</tr>
<tr class="odd">
<td>88</td>
<td>LB-S22-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-22</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>78.8</td>
<td>111.6</td>
</tr>
<tr class="even">
<td>89</td>
<td>LB-S23-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-23</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>80.2</td>
<td>114.0</td>
</tr>
<tr class="odd">
<td>90</td>
<td>LB-S23-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-23</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>81.6</td>
<td>114.6</td>
</tr>
<tr class="even">
<td>91</td>
<td>LB-S24-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-24</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>83.0</td>
<td>117.0</td>
</tr>
<tr class="odd">
<td>92</td>
<td>LB-S24-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-24</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>84.4</td>
<td>117.7</td>
</tr>
<tr class="even">
<td>93</td>
<td>LB-S24-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-24</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>84.4</td>
<td>117.7</td>
</tr>
<tr class="odd">
<td>94</td>
<td>LB-S25-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-25</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>85.9</td>
<td>120.1</td>
</tr>
<tr class="even">
<td>95</td>
<td>LB-S25-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-25</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>87.3</td>
<td>120.8</td>
</tr>
<tr class="odd">
<td>96</td>
<td>LB-S26-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-26</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>88.7</td>
<td>123.2</td>
</tr>
<tr class="even">
<td>97</td>
<td>LB-S26-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-26</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>90.1</td>
<td>123.9</td>
</tr>
<tr class="odd">
<td>98</td>
<td>LB-S27-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-27</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>91.5</td>
<td>126.3</td>
</tr>
<tr class="even">
<td>99</td>
<td>LB-S27-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-27</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>92.9</td>
<td>127.0</td>
</tr>
<tr class="odd">
<td>100</td>
<td>LB-S27-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-27</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>92.9</td>
<td>127.0</td>
</tr>
<tr class="even">
<td>101</td>
<td>LB-S28-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-28</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>94.3</td>
<td>129.4</td>
</tr>
<tr class="odd">
<td>102</td>
<td>LB-S28-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-28</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>95.8</td>
<td>130.0</td>
</tr>
<tr class="even">
<td>103</td>
<td>LB-S29-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-29</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>97.2</td>
<td>132.4</td>
</tr>
<tr class="odd">
<td>104</td>
<td>LB-S29-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-29</td>
<td>Ionisation chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>98.6</td>
<td>133.1</td>
</tr>
<tr class="even">
<td>105</td>
<td>LB-S30-BD-BLM-IC-01</td>
<td>iBLM</td>
<td>CMD-30</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1 <strong>TBD: inline7236, img69</strong> s</td>
<td>100.0</td>
<td>135.5</td>
</tr>
<tr class="odd">
<td>106</td>
<td>LB-S30-BD-BLM-IC-02</td>
<td>iBLM</td>
<td>CMD-30</td>
<td>Ionisation Chamber</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.5MeV</td>
<td>≈1ms</td>
<td>101.4</td>
<td>136.2</td>
</tr>
<tr class="even">
<td>107</td>
<td>LB-S30-BD-BLM-FD-01</td>
<td>fBLM</td>
<td>CMD-30</td>
<td>Fast BLM (S.+PM)</td>
<td><strong>TBD: inline7242, img70</strong></td>
<td>&gt; 0.05MeV</td>
<td>&lt; 10 ns</td>
<td>101.4</td>
<td>136.2</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: BLM of the LB sections (S19--S30)

 

**Table:** Provisional beam instrumentation needs for the MINERVA
Accelerator.

<table>
<caption>Table: Provisional beam instrumentation needs for the MINERVA Accelerator.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Beam Instrumentation</td>
<td>Injector</td>
<td>MEBT-3</td>
<td>SC LinacMain linac 100 MeV</td>
<td>100 MeV</td>
<td>Total</td>
</tr>
<tr class="even">
<td>Beam position monitors</td>
<td>6</td>
<td>8</td>
<td>50</td>
<td>15</td>
<td>79</td>
</tr>
<tr class="odd">
<td>Beam transverse profilers</td>
<td>10</td>
<td>6</td>
<td>10</td>
<td>15</td>
<td>41</td>
</tr>
<tr class="even">
<td>Emittance-metres</td>
<td>4</td>
<td>4</td>
<td>-</td>
<td>-</td>
<td>8</td>
</tr>
<tr class="odd">
<td>Beam current monitors</td>
<td>6</td>
<td>4</td>
<td>2</td>
<td>2 to 6</td>
<td>14 to 18</td>
</tr>
<tr class="even">
<td>Faraday cups</td>
<td>8</td>
<td>8</td>
<td>-</td>
<td>-</td>
<td>16</td>
</tr>
<tr class="odd">
<td>Beam energy monitors</td>
<td>2</td>
<td>2</td>
<td>-</td>
<td>1</td>
<td>5</td>
</tr>
<tr class="even">
<td>Bunch length monitors</td>
<td>4</td>
<td>2</td>
<td>12</td>
<td>-</td>
<td>18</td>
</tr>
<tr class="odd">
<td>Beam loss monitors</td>
<td>24</td>
<td>13</td>
<td>70</td>
<td>&gt; 12</td>
<td>&gt; 119</td>
</tr>
<tr class="even">
<td>Halo monitors and slits</td>
<td>5</td>
<td>5</td>
<td>-</td>
<td>13</td>
<td>23</td>
</tr>
<tr class="odd">
<td>Beam monitoring on target</td>
<td>-</td>
<td>-</td>
<td>-</td>
<td>1</td>
<td>1</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Provisional beam instrumentation needs for the MINERVA
Accelerator.

 

**Table:** LEBT solenoids parameters.

<table>
<caption>Table: LEBT solenoids parameters.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.025</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>central field <strong>TBD: inline7522, img72</strong> [T]</td>
<td>0.25</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>radius of good field zone [mm]</td>
<td>50</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>magnetic length <strong>TBD: inline7524, img73</strong> [mm]</td>
<td>241</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>effective length<strong>TBD: inline7526, img74</strong> <strong>TBD: inline7528, img75</strong> [mm]</td>
<td>184</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td>construction</td>
<td>overall solenoid length incl. end plates <strong>TBD: inline7530, img76</strong> [mm]</td>
<td>260</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>end plates thickness (clamp) [mm]</td>
<td>22</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>bore radius with steerers installed <strong>TBD: inline7532, img77</strong> [mm]</td>
<td>76</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> solenoid [ampere · turns]</td>
<td>48900</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td>[] solenoid coil</td>
<td>effective coil cross section [mm^2]</td>
<td>10500</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>electrical power [W]</td>
<td>4000</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td colspan="3"><strong>TBD: inline7538, img79</strong> the solenoid effective length for its focusing strength is <strong>TBD: inline7540, img80</strong></td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.025</td>
</tr>
<tr class="even">
<td> </td>
<td>central field <strong>TBD: inline7522, img72</strong> [T]</td>
<td>0.25</td>
</tr>
<tr class="odd">
<td> </td>
<td>radius of good field zone [mm]</td>
<td>50</td>
</tr>
<tr class="even">
<td> </td>
<td>magnetic length <strong>TBD: inline7524, img73</strong> [mm]</td>
<td>241</td>
</tr>
<tr class="odd">
<td> </td>
<td>effective length <strong>TBD: inline7528, img75</strong> [mm]</td>
<td>184</td>
</tr>
<tr class="even">
<td>construction</td>
<td>overall solenoid length incl. end plates <strong>TBD: inline7530, img76</strong> [mm]</td>
<td>260</td>
</tr>
<tr class="odd">
<td> </td>
<td>end plates thickness (clamp) [mm]</td>
<td>22</td>
</tr>
<tr class="even">
<td> </td>
<td>bore radius with steerers installed <strong>TBD: inline7532, img77</strong> [mm]</td>
<td>76</td>
</tr>
<tr class="odd">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> solenoid [ampere · turns]</td>
<td>48900</td>
</tr>
<tr class="even">
<td>solenoid coil</td>
<td>effective coil cross section [mm^2]</td>
<td>10500</td>
</tr>
<tr class="odd">
<td> </td>
<td>electrical power [W]</td>
<td>4000</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: LEBT solenoids parameters.

 

**Table:** LEBT steerers parameters.

<table>
<caption>Table: LEBT steerers parameters.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.025</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>central field H <strong>TBD: inline7560, img81</strong> [mT]</td>
<td>2.38</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>central field V <strong>TBD: inline7562, img82</strong> [mT]</td>
<td>2.24</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>effective length <strong>TBD: inline7564, img83</strong> [mm]</td>
<td>168</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>steering angles H [mradian]</td>
<td>16.0</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>steering angles V [mradian]</td>
<td>14.8</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td>construction</td>
<td>bore radius <strong>TBD: inline7566, img84</strong> [mm]</td>
<td>76</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> steerer H and V [ampere · turns]</td>
<td>500</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td>steerer coils</td>
<td>coil cross section [mm^2]</td>
<td>80</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>total electrical power (1 direction) [W]</td>
<td>40</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.025</td>
</tr>
<tr class="even">
<td> </td>
<td>central field H <strong>TBD: inline7560, img81</strong> [mT]</td>
<td>2.38</td>
</tr>
<tr class="odd">
<td> </td>
<td>central field V <strong>TBD: inline7562, img82</strong> [mT]</td>
<td>2.24</td>
</tr>
<tr class="even">
<td> </td>
<td>effective length <strong>TBD: inline7564, img83</strong> [mm]</td>
<td>168</td>
</tr>
<tr class="odd">
<td> </td>
<td>steering angles H [mradian]</td>
<td>16.0</td>
</tr>
<tr class="even">
<td> </td>
<td>steering angles V [mradian]</td>
<td>14.8</td>
</tr>
<tr class="odd">
<td>construction</td>
<td>bore radius <strong>TBD: inline7566, img84</strong> [mm]</td>
<td>76</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> steerer H and V [ampere · turns]</td>
<td>500</td>
</tr>
<tr class="odd">
<td>steerer coils</td>
<td>coil cross section [mm^2]</td>
<td>80</td>
</tr>
<tr class="even">
<td> </td>
<td>total electrical power (1 direction) [W]</td>
<td>40</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: LEBT steerers parameters.

 

**Table:** MEBT-1 me1s-subclass singlets.

<table>
<caption>Table: MEBT-1 me1s-subclass singlets.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.18</td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>19.1</td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;15</td>
<td>tolerance</td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.18</td>
</tr>
<tr class="even">
<td> </td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>19.1</td>
</tr>
<tr class="odd">
<td> </td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;15</td>
</tr>
<tr class="even">
<td> </td>
<td>tolerance</td>
<td>e-2</td>
</tr>
<tr class="odd">
<td> </td>
<td>effective length <strong>TBD: inline7590, img86</strong> (assembled) [mm]</td>
<td>51.6</td>
</tr>
<tr class="even">
<td>construction</td>
<td>pole length <strong>TBD: inline7592, img87</strong> [mm]</td>
<td>40.0</td>
</tr>
<tr class="odd">
<td> </td>
<td>pole radius <strong>TBD: inline7594, img88</strong> [mm]</td>
<td>20.0</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> per pole [ampere·turns]</td>
<td>3108</td>
</tr>
<tr class="odd">
<td>coil</td>
<td>coil effective cross section [mm^2]</td>
<td>311</td>
</tr>
<tr class="even">
<td> </td>
<td>electrical power per coil [W]</td>
<td>150</td>
</tr>
<tr class="odd">
<td> </td>
<td>total magnet electrical power [W]</td>
<td>599</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: MEBT-1 me1s-subclass singlets.

 

**Table:** MEBT-1 me1l-subclass singlets.

<table>
<caption>Table: MEBT-1 me1l-subclass singlets.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.18</td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>22.5</td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;15</td>
<td>tolerance</td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.18</td>
</tr>
<tr class="even">
<td> </td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>22.5</td>
</tr>
<tr class="odd">
<td> </td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;15</td>
</tr>
<tr class="even">
<td> </td>
<td>tolerance</td>
<td>e-2</td>
</tr>
<tr class="odd">
<td> </td>
<td>effective length <strong>TBD: inline7590, img86</strong> (assembled) [mm]</td>
<td>78.2</td>
</tr>
<tr class="even">
<td>construction</td>
<td>pole length <strong>TBD: inline7592, img87</strong> [mm]</td>
<td>72.3</td>
</tr>
<tr class="odd">
<td> </td>
<td>pole radius <strong>TBD: inline7594, img88</strong> [mm]</td>
<td>20</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> per pole [ampere · turns]</td>
<td>3668</td>
</tr>
<tr class="odd">
<td>coil</td>
<td>coil effective cross section [mm^2]</td>
<td>311</td>
</tr>
<tr class="even">
<td> </td>
<td>electrical power per coil [W]</td>
<td>255</td>
</tr>
<tr class="odd">
<td> </td>
<td>total magnet electrical power [W]</td>
<td>1022</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: MEBT-1 me1l-subclass singlets.

 

**Table:** MEBT-1 H- or V-steerers.

<table>
<caption>Table: MEBT-1 H- or V-steerers.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.18</td>
<td>central field <strong>TBD: inline7644, img89</strong> [T]</td>
<td>0.030</td>
<td>gap size <strong>TBD: inline7646, img90</strong> [mm]</td>
<td>40</td>
<td>effective length <strong>TBD: inline7564, img83</strong> [mm]</td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.18</td>
</tr>
<tr class="even">
<td> </td>
<td>central field <strong>TBD: inline7644, img89</strong> [T]</td>
<td>0.030</td>
</tr>
<tr class="odd">
<td> </td>
<td>gap size <strong>TBD: inline7646, img90</strong> [mm]</td>
<td>40</td>
</tr>
<tr class="even">
<td> </td>
<td>effective length <strong>TBD: inline7564, img83</strong> [mm]</td>
<td>85</td>
</tr>
<tr class="odd">
<td> </td>
<td>steering angles H [mradian]</td>
<td>99.9</td>
</tr>
<tr class="even">
<td>construction</td>
<td>pole length <strong>TBD: inline7650, img91</strong> [mm]</td>
<td>20</td>
</tr>
<tr class="odd">
<td> </td>
<td>pole width <strong>TBD: inline7652, img92</strong> [mm]</td>
<td>65</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> total [ampere · turns]</td>
<td>1440</td>
</tr>
<tr class="odd">
<td>coils</td>
<td>number of coils</td>
<td>2</td>
</tr>
<tr class="even">
<td> </td>
<td>coil effective cross section [mm^2]</td>
<td>314</td>
</tr>
<tr class="odd">
<td> </td>
<td>electrical power per coil [W]</td>
<td>49</td>
</tr>
<tr class="even">
<td> </td>
<td>total magnet electrical power (1 direction) [W]</td>
<td>97</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: MEBT-1 H- or V-steerers.

 

**Table:** CH01 (I1-CH

3:RF-CAV-01) -- CH07 (I1-CH

1:RF-CAV-01): CHa-subclass singlets.

<table>
<caption>Table: CH01 (I1-CH3:RF-CAV-01) – CH07 (I1-CH1:RF-CAV-01): CHa-subclass singlets.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7676, img93</strong></td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>35.6</td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;15</td>
<td>tolerance</td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7676, img93</strong></td>
</tr>
<tr class="even">
<td> </td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>35.6</td>
</tr>
<tr class="odd">
<td> </td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;15</td>
</tr>
<tr class="even">
<td> </td>
<td>tolerance</td>
<td>e-2</td>
</tr>
<tr class="odd">
<td> </td>
<td>effective length <strong>TBD: inline7590, img86</strong> (assembled) [mm]</td>
<td>51.9</td>
</tr>
<tr class="even">
<td>construction</td>
<td>pole length <strong>TBD: inline7592, img87</strong> [mm]</td>
<td>40</td>
</tr>
<tr class="odd">
<td> </td>
<td>pole radius <strong>TBD: inline7594, img88</strong> [mm]</td>
<td>17</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> per pole [ampere · turns]</td>
<td>4508</td>
</tr>
<tr class="odd">
<td>coil</td>
<td>coil effective cross section [mm^2]</td>
<td>311</td>
</tr>
<tr class="even">
<td> </td>
<td>electrical power per coil [W]</td>
<td>310</td>
</tr>
<tr class="odd">
<td> </td>
<td>total magnet electrical power [W]</td>
<td>1240</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: CH01 (I1-CH3:RF-CAV-01) -- CH07 (I1-CH1:RF-CAV-01): CHa-subclass
singlets.

 

**Table:** CH08--CH15: CHb-subclass singlets.

<table>
<caption>Table: CH08–CH15: CHb-subclass singlets.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7708, img94</strong></td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>32.8</td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;17</td>
<td>tolerance</td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7708, img94</strong></td>
</tr>
<tr class="even">
<td> </td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>32.8</td>
</tr>
<tr class="odd">
<td> </td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;17</td>
</tr>
<tr class="even">
<td> </td>
<td>tolerance</td>
<td>e-2</td>
</tr>
<tr class="odd">
<td> </td>
<td>effective length <strong>TBD: inline7590, img86</strong> [mm]</td>
<td>71</td>
</tr>
<tr class="even">
<td>construction</td>
<td>pole length <strong>TBD: inline7592, img87</strong> (estimated) [mm]</td>
<td>59</td>
</tr>
<tr class="odd">
<td> </td>
<td>pole radius <strong>TBD: inline7594, img88</strong> [mm]</td>
<td>21</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> per pole [ampere · turns] ( <strong>TBD: inline7722, img95</strong> )</td>
<td>5761</td>
</tr>
<tr class="odd">
<td>coil</td>
<td>coil effective cross section (estimated) [mm^2]</td>
<td>311</td>
</tr>
<tr class="even">
<td> </td>
<td>electrical power per coil (estimated) [W]</td>
<td>576</td>
</tr>
<tr class="odd">
<td> </td>
<td>total magnet electrical power (estimated) [W]</td>
<td>2305</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: CH08--CH15: CHb-subclass singlets.

 

**Table:** MEBT-3: i-subclass dipoles.

<table>
<caption>Table: MEBT-3: i-subclass dipoles.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.59</td>
<td>central field <strong>TBD: inline7644, img89</strong> [T]</td>
<td>0.49</td>
<td>width of constant field zone [mm]</td>
<td>tbd</td>
<td>tolerance</td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.59</td>
</tr>
<tr class="even">
<td> </td>
<td>central field <strong>TBD: inline7644, img89</strong> [T]</td>
<td>0.49</td>
</tr>
<tr class="odd">
<td> </td>
<td>width of constant field zone [mm]</td>
<td>tbd</td>
</tr>
<tr class="even">
<td> </td>
<td>tolerance</td>
<td>tbd</td>
</tr>
<tr class="odd">
<td> </td>
<td>gap size <strong>TBD: inline7646, img90</strong> [mm]</td>
<td>80</td>
</tr>
<tr class="even">
<td> </td>
<td>effective length <strong>TBD: inline7564, img83</strong> [mm]</td>
<td>942</td>
</tr>
<tr class="odd">
<td> </td>
<td>Entrance face angle <strong>TBD: inline7750, img96</strong> [°]</td>
<td>0</td>
</tr>
<tr class="even">
<td> </td>
<td>Exit face angle <strong>TBD: inline7752, img97</strong> [°]</td>
<td>0</td>
</tr>
<tr class="odd">
<td>construction</td>
<td>pole length <strong>TBD: inline7650, img91</strong> [mm]</td>
<td>tbd</td>
</tr>
<tr class="even">
<td> </td>
<td>pole width <strong>TBD: inline7652, img92</strong> [mm]</td>
<td>100</td>
</tr>
<tr class="odd">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> total [ampere · turns] ( <strong>TBD: inline7762, img98</strong> )</td>
<td>31371</td>
</tr>
<tr class="even">
<td>coils</td>
<td>number of coils</td>
<td>2</td>
</tr>
<tr class="odd">
<td> </td>
<td>coil cross section [mm^2]</td>
<td>tbd</td>
</tr>
<tr class="even">
<td> </td>
<td>electrical power per coil [W]</td>
<td>2518</td>
</tr>
<tr class="odd">
<td> </td>
<td>total magnet electrical power [W]</td>
<td>5036</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: MEBT-3: i-subclass dipoles.

 

**Table:** : lb-subclass singlets.

<table>
<caption>Table: : lb-subclass singlets.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7788, img99</strong></td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>16.3</td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;25</td>
<td>tolerance</td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7788, img99</strong></td>
</tr>
<tr class="even">
<td> </td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>16.3</td>
</tr>
<tr class="odd">
<td> </td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;25</td>
</tr>
<tr class="even">
<td> </td>
<td>tolerance</td>
<td>e-2</td>
</tr>
<tr class="odd">
<td> </td>
<td>effective length <strong>TBD: inline7590, img86</strong> [mm]</td>
<td>123</td>
</tr>
<tr class="even">
<td>construction</td>
<td>pole length <strong>TBD: inline7592, img87</strong> [mm]</td>
<td>120</td>
</tr>
<tr class="odd">
<td> </td>
<td>pole radius <strong>TBD: inline7594, img88</strong> [mm]</td>
<td>36.5</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> per pole [ampere · turns] ( <strong>TBD: inline7722, img95</strong> )</td>
<td>8619</td>
</tr>
<tr class="odd">
<td>coil</td>
<td>coil effective cross section [mm^2]</td>
<td>455</td>
</tr>
<tr class="even">
<td> </td>
<td>electrical power per coil [W]</td>
<td>1956</td>
</tr>
<tr class="odd">
<td> </td>
<td>total magnet electrical power [W]</td>
<td>7825</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: : lb-subclass singlets.

 

**Table:** : lb-subclass steerers.

<table>
<caption>Table: : lb-subclass steerers.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7788, img99</strong></td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>central field H <strong>TBD: inline7560, img81</strong> [mT]</td>
<td>16.3</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>central field V <strong>TBD: inline7562, img82</strong> [mT]</td>
<td>16.3</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>effective length <strong>TBD: inline7564, img83</strong> [mm]</td>
<td>123</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>steering angles H [mradian]</td>
<td><strong>TBD: inline7832, img100</strong></td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>steering angles V [mradian]</td>
<td><strong>TBD: inline7832, img100</strong></td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td>construction</td>
<td>bore radius <strong>TBD: inline7566, img84</strong> [mm]</td>
<td>36.5</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> steerer H and V [ampere · turns]</td>
<td>947</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td>steerer coils</td>
<td>coil effective cross section [mm^2]</td>
<td>140</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>total electrical power (1 direction) [W]</td>
<td>95</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7788, img99</strong></td>
</tr>
<tr class="even">
<td> </td>
<td>central field H <strong>TBD: inline7560, img81</strong> [mT]</td>
<td>16.3</td>
</tr>
<tr class="odd">
<td> </td>
<td>central field V <strong>TBD: inline7562, img82</strong> [mT]</td>
<td>16.3</td>
</tr>
<tr class="even">
<td> </td>
<td>effective length <strong>TBD: inline7564, img83</strong> [mm]</td>
<td>123</td>
</tr>
<tr class="odd">
<td> </td>
<td>steering angles H [mradian]</td>
<td><strong>TBD: inline7832, img100</strong></td>
</tr>
<tr class="even">
<td> </td>
<td>steering angles V [mradian]</td>
<td><strong>TBD: inline7832, img100</strong></td>
</tr>
<tr class="odd">
<td>construction</td>
<td>bore radius <strong>TBD: inline7566, img84</strong> [mm]</td>
<td>36.5</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> steerer H and V [ampere · turns]</td>
<td>947</td>
</tr>
<tr class="odd">
<td>steerer coils</td>
<td>coil effective cross section [mm^2]</td>
<td>140</td>
</tr>
<tr class="even">
<td> </td>
<td>total electrical power (1 direction) [W]</td>
<td>95</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: : lb-subclass steerers.

 

**Table:** Input parameters to the switching magnet study.

<table>
<caption>Table: Input parameters to the switching magnet study.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Parameter</td>
<td>Value</td>
</tr>
<tr class="even">
<td>Input energy (MeV)</td>
<td>16.6</td>
</tr>
<tr class="odd">
<td>Magnetic rigidity protons at input energy (Tm)</td>
<td>0.59</td>
</tr>
<tr class="even">
<td>Beam RMS diameter in dipole (mm)</td>
<td>&lt;20</td>
</tr>
<tr class="odd">
<td>Angle input line/ high energy line (°)</td>
<td>45</td>
</tr>
<tr class="even">
<td>Max nominal field switching time (s)</td>
<td>1.5</td>
</tr>
<tr class="odd">
<td>Angle input and output faces of dipoles/beam angle (degree)</td>
<td>0</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Input parameters to the switching magnet study.

 

**Table:** Switching dipole magnetic parameters.

|     |
|-----|
|     |

Table: Switching dipole magnetic parameters.

 

**Table:** Dynamic dipole working parameters.

<table>
<caption>Table: Dynamic dipole working parameters.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Parameter</td>
<td>Value</td>
</tr>
<tr class="even">
<td>Size of conductor (mm × mm)</td>
<td>9 × 9</td>
</tr>
<tr class="odd">
<td>Diameter water cooling pipe (mm)</td>
<td>4</td>
</tr>
<tr class="even">
<td>Length of 1 turn (m)</td>
<td>2</td>
</tr>
<tr class="odd">
<td>Number of turns per pancake</td>
<td>6</td>
</tr>
<tr class="even">
<td>Number of pancakes per coil</td>
<td>4</td>
</tr>
<tr class="odd">
<td>Total magnet resistance (mOhms)</td>
<td>24.16</td>
</tr>
<tr class="even">
<td>Total magnet inductance (mH)</td>
<td>0.64</td>
</tr>
<tr class="odd">
<td>Coil current (A)</td>
<td>900</td>
</tr>
<tr class="even">
<td>Electromotive force (A·turns)</td>
<td>21600</td>
</tr>
<tr class="odd">
<td>Effective current density (A / )</td>
<td>13.3</td>
</tr>
<tr class="even">
<td>Input temperature water (℃)</td>
<td>20</td>
</tr>
<tr class="odd">
<td>Output temperature water (℃)</td>
<td>38</td>
</tr>
<tr class="even">
<td>Water flow per pancake (l/min)</td>
<td>2</td>
</tr>
<tr class="odd">
<td>Pressure drop per pancake (bars)</td>
<td>4</td>
</tr>
<tr class="even">
<td>Total water flow per coil (l/min)</td>
<td>8</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Dynamic dipole working parameters.

 

**Table:** Summary of parameters of the MINERVA deflecting and focusing
devices.

<table>
<caption>Table: Summary of parameters of the MINERVA deflecting and focusing devices.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>2<strong>Injector</strong></td>
<td>2<strong>MEBT-3</strong></td>
<td>2<strong>Single-spoke</strong><br />
SC Linac<strong>linac</strong></td>
<td>2</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td><strong>Solenoids</strong></td>
<td>2</td>
<td>0</td>
<td>0</td>
<td>tbd</td>
</tr>
<tr class="even">
<td>2<strong>Steerers</strong><br />
<strong>(H and V)</strong></td>
<td>2<strong>TBD: inline7864, img101</strong></td>
<td>2tbd</td>
<td>230</td>
<td>2tbd</td>
</tr>
<tr class="odd">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td><strong>Dipoles</strong></td>
<td>0</td>
<td>1× i</td>
<td>0</td>
<td>tbd</td>
</tr>
<tr class="odd">
<td>2<strong>Quadrupole</strong><br />
<strong>doublets</strong></td>
<td>27 × (2× CHa)<br />
9 × (2× CHb)</td>
<td>22 × (2× lb)</td>
<td>230 ×<br />
(2× lb)</td>
<td>2tbd</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>2<strong>Quadrupole</strong><br />
<strong>triplets</strong></td>
<td>21 × (2×<br />
me1s + 1× me2l)</td>
<td>23 × (3× lb)</td>
<td>20</td>
<td>2tbd</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>2<strong>Specific</strong><br />
<strong>devices</strong></td>
<td>21 × a-chopper</td>
<td>21 × s-dipole<br />
1 × i-rastering</td>
<td>20</td>
<td>2tbd</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>2<strong>Total nominal</strong><br />
<strong>power (kW)</strong></td>
<td>251</td>
<td>245</td>
<td>2232</td>
<td>2tbd</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Summary of parameters of the MINERVA deflecting and focusing
devices.

 

**Table:** LEBT solenoids parameters.

<table>
<caption>Table: LEBT solenoids parameters.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.025</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>central field <strong>TBD: inline7522, img72</strong> [T]</td>
<td>0.25</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>radius of good field zone [mm]</td>
<td>50</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>magnetic length <strong>TBD: inline7524, img73</strong> [mm]</td>
<td>241</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>effective length<strong>TBD: inline7526, img74</strong> <strong>TBD: inline7528, img75</strong> [mm]</td>
<td>184</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td>construction</td>
<td>overall solenoid length incl. end plates <strong>TBD: inline7530, img76</strong> [mm]</td>
<td>260</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>end plates thickness (clamp) [mm]</td>
<td>22</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>bore radius with steerers installed <strong>TBD: inline7532, img77</strong> [mm]</td>
<td>76</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> solenoid [ampere · turns]</td>
<td>48900</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td>[] solenoid coil</td>
<td>effective coil cross section [mm^2]</td>
<td>10500</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>electrical power [W]</td>
<td>4000</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td colspan="3"><strong>TBD: inline7538, img79</strong> the solenoid effective length for its focusing strength is <strong>TBD: inline7540, img80</strong></td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.025</td>
</tr>
<tr class="even">
<td> </td>
<td>central field <strong>TBD: inline7522, img72</strong> [T]</td>
<td>0.25</td>
</tr>
<tr class="odd">
<td> </td>
<td>radius of good field zone [mm]</td>
<td>50</td>
</tr>
<tr class="even">
<td> </td>
<td>magnetic length <strong>TBD: inline7524, img73</strong> [mm]</td>
<td>241</td>
</tr>
<tr class="odd">
<td> </td>
<td>effective length <strong>TBD: inline7528, img75</strong> [mm]</td>
<td>184</td>
</tr>
<tr class="even">
<td>construction</td>
<td>overall solenoid length incl. end plates <strong>TBD: inline7530, img76</strong> [mm]</td>
<td>260</td>
</tr>
<tr class="odd">
<td> </td>
<td>end plates thickness (clamp) [mm]</td>
<td>22</td>
</tr>
<tr class="even">
<td> </td>
<td>bore radius with steerers installed <strong>TBD: inline7532, img77</strong> [mm]</td>
<td>76</td>
</tr>
<tr class="odd">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> solenoid [ampere · turns]</td>
<td>48900</td>
</tr>
<tr class="even">
<td>solenoid coil</td>
<td>effective coil cross section [mm^2]</td>
<td>10500</td>
</tr>
<tr class="odd">
<td> </td>
<td>electrical power [W]</td>
<td>4000</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: LEBT solenoids parameters.

 

**Table:** LEBT steerers parameters.

<table>
<caption>Table: LEBT steerers parameters.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.025</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>central field H <strong>TBD: inline7560, img81</strong> [mT]</td>
<td>2.38</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>central field V <strong>TBD: inline7562, img82</strong> [mT]</td>
<td>2.24</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>effective length <strong>TBD: inline7564, img83</strong> [mm]</td>
<td>168</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>steering angles H [mradian]</td>
<td>16.0</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>steering angles V [mradian]</td>
<td>14.8</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td>construction</td>
<td>bore radius <strong>TBD: inline7566, img84</strong> [mm]</td>
<td>76</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> steerer H and V [ampere · turns]</td>
<td>500</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td>steerer coils</td>
<td>coil cross section [mm^2]</td>
<td>80</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>total electrical power (1 direction) [W]</td>
<td>40</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.025</td>
</tr>
<tr class="even">
<td> </td>
<td>central field H <strong>TBD: inline7560, img81</strong> [mT]</td>
<td>2.38</td>
</tr>
<tr class="odd">
<td> </td>
<td>central field V <strong>TBD: inline7562, img82</strong> [mT]</td>
<td>2.24</td>
</tr>
<tr class="even">
<td> </td>
<td>effective length <strong>TBD: inline7564, img83</strong> [mm]</td>
<td>168</td>
</tr>
<tr class="odd">
<td> </td>
<td>steering angles H [mradian]</td>
<td>16.0</td>
</tr>
<tr class="even">
<td> </td>
<td>steering angles V [mradian]</td>
<td>14.8</td>
</tr>
<tr class="odd">
<td>construction</td>
<td>bore radius <strong>TBD: inline7566, img84</strong> [mm]</td>
<td>76</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> steerer H and V [ampere · turns]</td>
<td>500</td>
</tr>
<tr class="odd">
<td>steerer coils</td>
<td>coil cross section [mm^2]</td>
<td>80</td>
</tr>
<tr class="even">
<td> </td>
<td>total electrical power (1 direction) [W]</td>
<td>40</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: LEBT steerers parameters.

 

**Table:** MEBT-1 me1s-subclass singlets.

<table>
<caption>Table: MEBT-1 me1s-subclass singlets.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.18</td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>19.1</td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;15</td>
<td>tolerance</td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.18</td>
</tr>
<tr class="even">
<td> </td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>19.1</td>
</tr>
<tr class="odd">
<td> </td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;15</td>
</tr>
<tr class="even">
<td> </td>
<td>tolerance</td>
<td>e-2</td>
</tr>
<tr class="odd">
<td> </td>
<td>effective length <strong>TBD: inline7590, img86</strong> (assembled) [mm]</td>
<td>51.6</td>
</tr>
<tr class="even">
<td>construction</td>
<td>pole length <strong>TBD: inline7592, img87</strong> [mm]</td>
<td>40.0</td>
</tr>
<tr class="odd">
<td> </td>
<td>pole radius <strong>TBD: inline7594, img88</strong> [mm]</td>
<td>20.0</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> per pole [ampere·turns]</td>
<td>3108</td>
</tr>
<tr class="odd">
<td>coil</td>
<td>coil effective cross section [mm^2]</td>
<td>311</td>
</tr>
<tr class="even">
<td> </td>
<td>electrical power per coil [W]</td>
<td>150</td>
</tr>
<tr class="odd">
<td> </td>
<td>total magnet electrical power [W]</td>
<td>599</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: MEBT-1 me1s-subclass singlets.

 

**Table:** MEBT-1 me1l-subclass singlets.

<table>
<caption>Table: MEBT-1 me1l-subclass singlets.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.18</td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>22.5</td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;15</td>
<td>tolerance</td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.18</td>
</tr>
<tr class="even">
<td> </td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>22.5</td>
</tr>
<tr class="odd">
<td> </td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;15</td>
</tr>
<tr class="even">
<td> </td>
<td>tolerance</td>
<td>e-2</td>
</tr>
<tr class="odd">
<td> </td>
<td>effective length <strong>TBD: inline7590, img86</strong> (assembled) [mm]</td>
<td>78.2</td>
</tr>
<tr class="even">
<td>construction</td>
<td>pole length <strong>TBD: inline7592, img87</strong> [mm]</td>
<td>72.3</td>
</tr>
<tr class="odd">
<td> </td>
<td>pole radius <strong>TBD: inline7594, img88</strong> [mm]</td>
<td>20</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> per pole [ampere · turns]</td>
<td>3668</td>
</tr>
<tr class="odd">
<td>coil</td>
<td>coil effective cross section [mm^2]</td>
<td>311</td>
</tr>
<tr class="even">
<td> </td>
<td>electrical power per coil [W]</td>
<td>255</td>
</tr>
<tr class="odd">
<td> </td>
<td>total magnet electrical power [W]</td>
<td>1022</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: MEBT-1 me1l-subclass singlets.

 

**Table:** MEBT-1 H- or V-steerers.

<table>
<caption>Table: MEBT-1 H- or V-steerers.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.18</td>
<td>central field <strong>TBD: inline7644, img89</strong> [T]</td>
<td>0.030</td>
<td>gap size <strong>TBD: inline7646, img90</strong> [mm]</td>
<td>40</td>
<td>effective length <strong>TBD: inline7564, img83</strong> [mm]</td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.18</td>
</tr>
<tr class="even">
<td> </td>
<td>central field <strong>TBD: inline7644, img89</strong> [T]</td>
<td>0.030</td>
</tr>
<tr class="odd">
<td> </td>
<td>gap size <strong>TBD: inline7646, img90</strong> [mm]</td>
<td>40</td>
</tr>
<tr class="even">
<td> </td>
<td>effective length <strong>TBD: inline7564, img83</strong> [mm]</td>
<td>85</td>
</tr>
<tr class="odd">
<td> </td>
<td>steering angles H [mradian]</td>
<td>99.9</td>
</tr>
<tr class="even">
<td>construction</td>
<td>pole length <strong>TBD: inline7650, img91</strong> [mm]</td>
<td>20</td>
</tr>
<tr class="odd">
<td> </td>
<td>pole width <strong>TBD: inline7652, img92</strong> [mm]</td>
<td>65</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> total [ampere · turns]</td>
<td>1440</td>
</tr>
<tr class="odd">
<td>coils</td>
<td>number of coils</td>
<td>2</td>
</tr>
<tr class="even">
<td> </td>
<td>coil effective cross section [mm^2]</td>
<td>314</td>
</tr>
<tr class="odd">
<td> </td>
<td>electrical power per coil [W]</td>
<td>49</td>
</tr>
<tr class="even">
<td> </td>
<td>total magnet electrical power (1 direction) [W]</td>
<td>97</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: MEBT-1 H- or V-steerers.

 

**Table:** CH01 (I1-CH

7:RF-CAV-01) -- CH07 (I1-CH

1:RF-CAV-01): CHa-subclass singlets.

<table>
<caption>Table: CH01 (I1-CH7:RF-CAV-01) – CH07 (I1-CH1:RF-CAV-01): CHa-subclass singlets.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7676, img93</strong></td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>35.6</td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;15</td>
<td>tolerance</td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7676, img93</strong></td>
</tr>
<tr class="even">
<td> </td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>35.6</td>
</tr>
<tr class="odd">
<td> </td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;15</td>
</tr>
<tr class="even">
<td> </td>
<td>tolerance</td>
<td>e-2</td>
</tr>
<tr class="odd">
<td> </td>
<td>effective length <strong>TBD: inline7590, img86</strong> (assembled) [mm]</td>
<td>51.9</td>
</tr>
<tr class="even">
<td>construction</td>
<td>pole length <strong>TBD: inline7592, img87</strong> [mm]</td>
<td>40</td>
</tr>
<tr class="odd">
<td> </td>
<td>pole radius <strong>TBD: inline7594, img88</strong> [mm]</td>
<td>17</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> per pole [ampere · turns]</td>
<td>4508</td>
</tr>
<tr class="odd">
<td>coil</td>
<td>coil effective cross section [mm^2]</td>
<td>311</td>
</tr>
<tr class="even">
<td> </td>
<td>electrical power per coil [W]</td>
<td>310</td>
</tr>
<tr class="odd">
<td> </td>
<td>total magnet electrical power [W]</td>
<td>1240</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: CH01 (I1-CH7:RF-CAV-01) -- CH07 (I1-CH1:RF-CAV-01): CHa-subclass
singlets.

 

**Table:** CH08--CH15: CHb-subclass singlets.

<table>
<caption>Table: CH08–CH15: CHb-subclass singlets.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7708, img94</strong></td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>32.8</td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;17</td>
<td>tolerance</td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7708, img94</strong></td>
</tr>
<tr class="even">
<td> </td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>32.8</td>
</tr>
<tr class="odd">
<td> </td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;17</td>
</tr>
<tr class="even">
<td> </td>
<td>tolerance</td>
<td>e-2</td>
</tr>
<tr class="odd">
<td> </td>
<td>effective length <strong>TBD: inline7590, img86</strong> [mm]</td>
<td>71</td>
</tr>
<tr class="even">
<td>construction</td>
<td>pole length <strong>TBD: inline7592, img87</strong> (estimated) [mm]</td>
<td>59</td>
</tr>
<tr class="odd">
<td> </td>
<td>pole radius <strong>TBD: inline7594, img88</strong> [mm]</td>
<td>21</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> per pole [ampere · turns] ( <strong>TBD: inline7722, img95</strong> )</td>
<td>5761</td>
</tr>
<tr class="odd">
<td>coil</td>
<td>coil effective cross section (estimated) [mm^2]</td>
<td>311</td>
</tr>
<tr class="even">
<td> </td>
<td>electrical power per coil (estimated) [W]</td>
<td>576</td>
</tr>
<tr class="odd">
<td> </td>
<td>total magnet electrical power (estimated) [W]</td>
<td>2305</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: CH08--CH15: CHb-subclass singlets.

 

**Table:** MEBT-3: i-subclass dipoles.

<table>
<caption>Table: MEBT-3: i-subclass dipoles.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.59</td>
<td>central field <strong>TBD: inline7644, img89</strong> [T]</td>
<td>0.49</td>
<td>width of constant field zone [mm]</td>
<td>tbd</td>
<td>tolerance</td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.59</td>
</tr>
<tr class="even">
<td> </td>
<td>central field <strong>TBD: inline7644, img89</strong> [T]</td>
<td>0.49</td>
</tr>
<tr class="odd">
<td> </td>
<td>width of constant field zone [mm]</td>
<td>tbd</td>
</tr>
<tr class="even">
<td> </td>
<td>tolerance</td>
<td>tbd</td>
</tr>
<tr class="odd">
<td> </td>
<td>gap size <strong>TBD: inline7646, img90</strong> [mm]</td>
<td>80</td>
</tr>
<tr class="even">
<td> </td>
<td>effective length <strong>TBD: inline7564, img83</strong> [mm]</td>
<td>942</td>
</tr>
<tr class="odd">
<td> </td>
<td>Entrance face angle <strong>TBD: inline7750, img96</strong> [°]</td>
<td>0</td>
</tr>
<tr class="even">
<td> </td>
<td>Exit face angle <strong>TBD: inline7752, img97</strong> [°]</td>
<td>0</td>
</tr>
<tr class="odd">
<td>construction</td>
<td>pole length <strong>TBD: inline7650, img91</strong> [mm]</td>
<td>tbd</td>
</tr>
<tr class="even">
<td> </td>
<td>pole width <strong>TBD: inline7652, img92</strong> [mm]</td>
<td>100</td>
</tr>
<tr class="odd">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> total [ampere · turns] ( <strong>TBD: inline7762, img98</strong> )</td>
<td>31371</td>
</tr>
<tr class="even">
<td>coils</td>
<td>number of coils</td>
<td>2</td>
</tr>
<tr class="odd">
<td> </td>
<td>coil cross section [mm^2]</td>
<td>tbd</td>
</tr>
<tr class="even">
<td> </td>
<td>electrical power per coil [W]</td>
<td>2518</td>
</tr>
<tr class="odd">
<td> </td>
<td>total magnet electrical power [W]</td>
<td>5036</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: MEBT-3: i-subclass dipoles.

 

**Table:** : lb-subclass singlets.

<table>
<caption>Table: : lb-subclass singlets.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7788, img99</strong></td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>16.3</td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;25</td>
<td>tolerance</td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7788, img99</strong></td>
</tr>
<tr class="even">
<td> </td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>16.3</td>
</tr>
<tr class="odd">
<td> </td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;25</td>
</tr>
<tr class="even">
<td> </td>
<td>tolerance</td>
<td>e-2</td>
</tr>
<tr class="odd">
<td> </td>
<td>effective length <strong>TBD: inline7590, img86</strong> [mm]</td>
<td>123</td>
</tr>
<tr class="even">
<td>construction</td>
<td>pole length <strong>TBD: inline7592, img87</strong> [mm]</td>
<td>120</td>
</tr>
<tr class="odd">
<td> </td>
<td>pole radius <strong>TBD: inline7594, img88</strong> [mm]</td>
<td>36.5</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> per pole [ampere · turns] ( <strong>TBD: inline7722, img95</strong> )</td>
<td>8619</td>
</tr>
<tr class="odd">
<td>coil</td>
<td>coil effective cross section [mm^2]</td>
<td>455</td>
</tr>
<tr class="even">
<td> </td>
<td>electrical power per coil [W]</td>
<td>1956</td>
</tr>
<tr class="odd">
<td> </td>
<td>total magnet electrical power [W]</td>
<td>7825</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: : lb-subclass singlets.

 

**Table:** : lb-subclass steerers.

<table>
<caption>Table: : lb-subclass steerers.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7788, img99</strong></td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>central field H <strong>TBD: inline7560, img81</strong> [mT]</td>
<td>16.3</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>central field V <strong>TBD: inline7562, img82</strong> [mT]</td>
<td>16.3</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>effective length <strong>TBD: inline7564, img83</strong> [mm]</td>
<td>123</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>steering angles H [mradian]</td>
<td><strong>TBD: inline7832, img100</strong></td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>steering angles V [mradian]</td>
<td><strong>TBD: inline7832, img100</strong></td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td>construction</td>
<td>bore radius <strong>TBD: inline7566, img84</strong> [mm]</td>
<td>36.5</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> steerer H and V [ampere · turns]</td>
<td>947</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td>steerer coils</td>
<td>coil effective cross section [mm^2]</td>
<td>140</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>total electrical power (1 direction) [W]</td>
<td>95</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7788, img99</strong></td>
</tr>
<tr class="even">
<td> </td>
<td>central field H <strong>TBD: inline7560, img81</strong> [mT]</td>
<td>16.3</td>
</tr>
<tr class="odd">
<td> </td>
<td>central field V <strong>TBD: inline7562, img82</strong> [mT]</td>
<td>16.3</td>
</tr>
<tr class="even">
<td> </td>
<td>effective length <strong>TBD: inline7564, img83</strong> [mm]</td>
<td>123</td>
</tr>
<tr class="odd">
<td> </td>
<td>steering angles H [mradian]</td>
<td><strong>TBD: inline7832, img100</strong></td>
</tr>
<tr class="even">
<td> </td>
<td>steering angles V [mradian]</td>
<td><strong>TBD: inline7832, img100</strong></td>
</tr>
<tr class="odd">
<td>construction</td>
<td>bore radius <strong>TBD: inline7566, img84</strong> [mm]</td>
<td>36.5</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> steerer H and V [ampere · turns]</td>
<td>947</td>
</tr>
<tr class="odd">
<td>steerer coils</td>
<td>coil effective cross section [mm^2]</td>
<td>140</td>
</tr>
<tr class="even">
<td> </td>
<td>total electrical power (1 direction) [W]</td>
<td>95</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: : lb-subclass steerers.

 

**Table:** Input parameters to the switching magnet study.

<table>
<caption>Table: Input parameters to the switching magnet study.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Parameter</td>
<td>Value</td>
</tr>
<tr class="even">
<td>Input energy (MeV)</td>
<td>16.6</td>
</tr>
<tr class="odd">
<td>Magnetic rigidity protons at input energy (Tm)</td>
<td>0.59</td>
</tr>
<tr class="even">
<td>Beam RMS diameter in dipole (mm)</td>
<td>&lt;20</td>
</tr>
<tr class="odd">
<td>Angle input line/ high energy line (°)</td>
<td>45</td>
</tr>
<tr class="even">
<td>Max nominal field switching time (s)</td>
<td>1.5</td>
</tr>
<tr class="odd">
<td>Angle input and output faces of dipoles/beam angle (degree)</td>
<td>0</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Input parameters to the switching magnet study.

 

**Table:** Switching dipole magnetic parameters.

|     |
|-----|
|     |

Table: Switching dipole magnetic parameters.

 

**Table:** Dynamic dipole working parameters.

<table>
<caption>Table: Dynamic dipole working parameters.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Parameter</td>
<td>Value</td>
</tr>
<tr class="even">
<td>Size of conductor (mm × mm)</td>
<td>9 × 9</td>
</tr>
<tr class="odd">
<td>Diameter water cooling pipe (mm)</td>
<td>4</td>
</tr>
<tr class="even">
<td>Length of 1 turn (m)</td>
<td>2</td>
</tr>
<tr class="odd">
<td>Number of turns per pancake</td>
<td>6</td>
</tr>
<tr class="even">
<td>Number of pancakes per coil</td>
<td>4</td>
</tr>
<tr class="odd">
<td>Total magnet resistance (mOhms)</td>
<td>24.16</td>
</tr>
<tr class="even">
<td>Total magnet inductance (mH)</td>
<td>0.64</td>
</tr>
<tr class="odd">
<td>Coil current (A)</td>
<td>900</td>
</tr>
<tr class="even">
<td>Electromotive force (A·turns)</td>
<td>21600</td>
</tr>
<tr class="odd">
<td>Effective current density (A / )</td>
<td>13.3</td>
</tr>
<tr class="even">
<td>Input temperature water (℃)</td>
<td>20</td>
</tr>
<tr class="odd">
<td>Output temperature water (℃)</td>
<td>38</td>
</tr>
<tr class="even">
<td>Water flow per pancake (l/min)</td>
<td>2</td>
</tr>
<tr class="odd">
<td>Pressure drop per pancake (bars)</td>
<td>4</td>
</tr>
<tr class="even">
<td>Total water flow per coil (l/min)</td>
<td>8</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Dynamic dipole working parameters.

 

**Table:** Summary of parameters of the MINERVA deflecting and focusing
devices.

<table>
<caption>Table: Summary of parameters of the MINERVA deflecting and focusing devices.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>2<strong>Injector</strong></td>
<td>2<strong>MEBT-3</strong></td>
<td>2<strong>Single-spoke</strong><br />
SC Linac<strong>linac</strong></td>
<td>2</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td><strong>Solenoids</strong></td>
<td>2</td>
<td>0</td>
<td>0</td>
<td>tbd</td>
</tr>
<tr class="even">
<td>2<strong>Steerers</strong><br />
<strong>(H and V)</strong></td>
<td>2<strong>TBD: inline7864, img101</strong></td>
<td>2tbd</td>
<td>230</td>
<td>2tbd</td>
</tr>
<tr class="odd">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td><strong>Dipoles</strong></td>
<td>0</td>
<td>1× i</td>
<td>0</td>
<td>tbd</td>
</tr>
<tr class="odd">
<td>2<strong>Quadrupole</strong><br />
<strong>doublets</strong></td>
<td>27 × (2× CHa)<br />
9 × (2× CHb)</td>
<td>22 × (2× lb)</td>
<td>230 ×<br />
(2× lb)</td>
<td>2tbd</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>2<strong>Quadrupole</strong><br />
<strong>triplets</strong></td>
<td>21 × (2×<br />
me1s + 1× me2l)</td>
<td>23 × (3× lb)</td>
<td>20</td>
<td>2tbd</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>2<strong>Specific</strong><br />
<strong>devices</strong></td>
<td>21 × a-chopper</td>
<td>21 × s-dipole<br />
1 × i-rastering</td>
<td>20</td>
<td>2tbd</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>2<strong>Total nominal</strong><br />
<strong>power (kW)</strong></td>
<td>251</td>
<td>245</td>
<td>2232</td>
<td>2tbd</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Summary of parameters of the MINERVA deflecting and focusing
devices.

 

**Table:** LEBT solenoids parameters.

<table>
<caption>Table: LEBT solenoids parameters.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.025</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>central field <strong>TBD: inline7522, img72</strong> [T]</td>
<td>0.25</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>radius of good field zone [mm]</td>
<td>50</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>magnetic length <strong>TBD: inline7524, img73</strong> [mm]</td>
<td>241</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>effective length<strong>TBD: inline7526, img74</strong> <strong>TBD: inline7528, img75</strong> [mm]</td>
<td>184</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td>construction</td>
<td>overall solenoid length incl. end plates <strong>TBD: inline7530, img76</strong> [mm]</td>
<td>260</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>end plates thickness (clamp) [mm]</td>
<td>22</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>bore radius with steerers installed <strong>TBD: inline7532, img77</strong> [mm]</td>
<td>76</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> solenoid [ampere · turns]</td>
<td>48900</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td>[] solenoid coil</td>
<td>effective coil cross section [mm^2]</td>
<td>10500</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>electrical power [W]</td>
<td>4000</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td colspan="3"><strong>TBD: inline7538, img79</strong> the solenoid effective length for its focusing strength is <strong>TBD: inline7540, img80</strong></td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.025</td>
</tr>
<tr class="even">
<td> </td>
<td>central field <strong>TBD: inline7522, img72</strong> [T]</td>
<td>0.25</td>
</tr>
<tr class="odd">
<td> </td>
<td>radius of good field zone [mm]</td>
<td>50</td>
</tr>
<tr class="even">
<td> </td>
<td>magnetic length <strong>TBD: inline7524, img73</strong> [mm]</td>
<td>241</td>
</tr>
<tr class="odd">
<td> </td>
<td>effective length <strong>TBD: inline7528, img75</strong> [mm]</td>
<td>184</td>
</tr>
<tr class="even">
<td>construction</td>
<td>overall solenoid length incl. end plates <strong>TBD: inline7530, img76</strong> [mm]</td>
<td>260</td>
</tr>
<tr class="odd">
<td> </td>
<td>end plates thickness (clamp) [mm]</td>
<td>22</td>
</tr>
<tr class="even">
<td> </td>
<td>bore radius with steerers installed <strong>TBD: inline7532, img77</strong> [mm]</td>
<td>76</td>
</tr>
<tr class="odd">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> solenoid [ampere · turns]</td>
<td>48900</td>
</tr>
<tr class="even">
<td>solenoid coil</td>
<td>effective coil cross section [mm^2]</td>
<td>10500</td>
</tr>
<tr class="odd">
<td> </td>
<td>electrical power [W]</td>
<td>4000</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: LEBT solenoids parameters.

 

**Table:** LEBT steerers parameters.

<table>
<caption>Table: LEBT steerers parameters.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.025</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>central field H <strong>TBD: inline7560, img81</strong> [mT]</td>
<td>2.38</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>central field V <strong>TBD: inline7562, img82</strong> [mT]</td>
<td>2.24</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>effective length <strong>TBD: inline7564, img83</strong> [mm]</td>
<td>168</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>steering angles H [mradian]</td>
<td>16.0</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>steering angles V [mradian]</td>
<td>14.8</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td>construction</td>
<td>bore radius <strong>TBD: inline7566, img84</strong> [mm]</td>
<td>76</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> steerer H and V [ampere · turns]</td>
<td>500</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td>steerer coils</td>
<td>coil cross section [mm^2]</td>
<td>80</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>total electrical power (1 direction) [W]</td>
<td>40</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.025</td>
</tr>
<tr class="even">
<td> </td>
<td>central field H <strong>TBD: inline7560, img81</strong> [mT]</td>
<td>2.38</td>
</tr>
<tr class="odd">
<td> </td>
<td>central field V <strong>TBD: inline7562, img82</strong> [mT]</td>
<td>2.24</td>
</tr>
<tr class="even">
<td> </td>
<td>effective length <strong>TBD: inline7564, img83</strong> [mm]</td>
<td>168</td>
</tr>
<tr class="odd">
<td> </td>
<td>steering angles H [mradian]</td>
<td>16.0</td>
</tr>
<tr class="even">
<td> </td>
<td>steering angles V [mradian]</td>
<td>14.8</td>
</tr>
<tr class="odd">
<td>construction</td>
<td>bore radius <strong>TBD: inline7566, img84</strong> [mm]</td>
<td>76</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> steerer H and V [ampere · turns]</td>
<td>500</td>
</tr>
<tr class="odd">
<td>steerer coils</td>
<td>coil cross section [mm^2]</td>
<td>80</td>
</tr>
<tr class="even">
<td> </td>
<td>total electrical power (1 direction) [W]</td>
<td>40</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: LEBT steerers parameters.

 

**Table:** MEBT-1 me1s-subclass singlets.

<table>
<caption>Table: MEBT-1 me1s-subclass singlets.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.18</td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>19.1</td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;15</td>
<td>tolerance</td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.18</td>
</tr>
<tr class="even">
<td> </td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>19.1</td>
</tr>
<tr class="odd">
<td> </td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;15</td>
</tr>
<tr class="even">
<td> </td>
<td>tolerance</td>
<td>e-2</td>
</tr>
<tr class="odd">
<td> </td>
<td>effective length <strong>TBD: inline7590, img86</strong> (assembled) [mm]</td>
<td>51.6</td>
</tr>
<tr class="even">
<td>construction</td>
<td>pole length <strong>TBD: inline7592, img87</strong> [mm]</td>
<td>40.0</td>
</tr>
<tr class="odd">
<td> </td>
<td>pole radius <strong>TBD: inline7594, img88</strong> [mm]</td>
<td>20.0</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> per pole [ampere·turns]</td>
<td>3108</td>
</tr>
<tr class="odd">
<td>coil</td>
<td>coil effective cross section [mm^2]</td>
<td>311</td>
</tr>
<tr class="even">
<td> </td>
<td>electrical power per coil [W]</td>
<td>150</td>
</tr>
<tr class="odd">
<td> </td>
<td>total magnet electrical power [W]</td>
<td>599</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: MEBT-1 me1s-subclass singlets.

 

**Table:** MEBT-1 me1l-subclass singlets.

<table>
<caption>Table: MEBT-1 me1l-subclass singlets.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.18</td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>22.5</td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;15</td>
<td>tolerance</td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.18</td>
</tr>
<tr class="even">
<td> </td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>22.5</td>
</tr>
<tr class="odd">
<td> </td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;15</td>
</tr>
<tr class="even">
<td> </td>
<td>tolerance</td>
<td>e-2</td>
</tr>
<tr class="odd">
<td> </td>
<td>effective length <strong>TBD: inline7590, img86</strong> (assembled) [mm]</td>
<td>78.2</td>
</tr>
<tr class="even">
<td>construction</td>
<td>pole length <strong>TBD: inline7592, img87</strong> [mm]</td>
<td>72.3</td>
</tr>
<tr class="odd">
<td> </td>
<td>pole radius <strong>TBD: inline7594, img88</strong> [mm]</td>
<td>20</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> per pole [ampere · turns]</td>
<td>3668</td>
</tr>
<tr class="odd">
<td>coil</td>
<td>coil effective cross section [mm^2]</td>
<td>311</td>
</tr>
<tr class="even">
<td> </td>
<td>electrical power per coil [W]</td>
<td>255</td>
</tr>
<tr class="odd">
<td> </td>
<td>total magnet electrical power [W]</td>
<td>1022</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: MEBT-1 me1l-subclass singlets.

 

**Table:** MEBT-1 H- or V-steerers.

<table>
<caption>Table: MEBT-1 H- or V-steerers.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.18</td>
<td>central field <strong>TBD: inline7644, img89</strong> [T]</td>
<td>0.030</td>
<td>gap size <strong>TBD: inline7646, img90</strong> [mm]</td>
<td>40</td>
<td>effective length <strong>TBD: inline7564, img83</strong> [mm]</td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.18</td>
</tr>
<tr class="even">
<td> </td>
<td>central field <strong>TBD: inline7644, img89</strong> [T]</td>
<td>0.030</td>
</tr>
<tr class="odd">
<td> </td>
<td>gap size <strong>TBD: inline7646, img90</strong> [mm]</td>
<td>40</td>
</tr>
<tr class="even">
<td> </td>
<td>effective length <strong>TBD: inline7564, img83</strong> [mm]</td>
<td>85</td>
</tr>
<tr class="odd">
<td> </td>
<td>steering angles H [mradian]</td>
<td>99.9</td>
</tr>
<tr class="even">
<td>construction</td>
<td>pole length <strong>TBD: inline7650, img91</strong> [mm]</td>
<td>20</td>
</tr>
<tr class="odd">
<td> </td>
<td>pole width <strong>TBD: inline7652, img92</strong> [mm]</td>
<td>65</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> total [ampere · turns]</td>
<td>1440</td>
</tr>
<tr class="odd">
<td>coils</td>
<td>number of coils</td>
<td>2</td>
</tr>
<tr class="even">
<td> </td>
<td>coil effective cross section [mm^2]</td>
<td>314</td>
</tr>
<tr class="odd">
<td> </td>
<td>electrical power per coil [W]</td>
<td>49</td>
</tr>
<tr class="even">
<td> </td>
<td>total magnet electrical power (1 direction) [W]</td>
<td>97</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: MEBT-1 H- or V-steerers.

 

**Table:** CH01 (I1-CH

7:RF-CAV-01) -- CH07 (I1-CH

1:RF-CAV-01): CHa-subclass singlets.

<table>
<caption>Table: CH01 (I1-CH7:RF-CAV-01) – CH07 (I1-CH1:RF-CAV-01): CHa-subclass singlets.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7676, img93</strong></td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>35.6</td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;15</td>
<td>tolerance</td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7676, img93</strong></td>
</tr>
<tr class="even">
<td> </td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>35.6</td>
</tr>
<tr class="odd">
<td> </td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;15</td>
</tr>
<tr class="even">
<td> </td>
<td>tolerance</td>
<td>e-2</td>
</tr>
<tr class="odd">
<td> </td>
<td>effective length <strong>TBD: inline7590, img86</strong> (assembled) [mm]</td>
<td>51.9</td>
</tr>
<tr class="even">
<td>construction</td>
<td>pole length <strong>TBD: inline7592, img87</strong> [mm]</td>
<td>40</td>
</tr>
<tr class="odd">
<td> </td>
<td>pole radius <strong>TBD: inline7594, img88</strong> [mm]</td>
<td>17</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> per pole [ampere · turns]</td>
<td>4508</td>
</tr>
<tr class="odd">
<td>coil</td>
<td>coil effective cross section [mm^2]</td>
<td>311</td>
</tr>
<tr class="even">
<td> </td>
<td>electrical power per coil [W]</td>
<td>310</td>
</tr>
<tr class="odd">
<td> </td>
<td>total magnet electrical power [W]</td>
<td>1240</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: CH01 (I1-CH7:RF-CAV-01) -- CH07 (I1-CH1:RF-CAV-01): CHa-subclass
singlets.

 

**Table:** CH08--CH15: CHb-subclass singlets.

<table>
<caption>Table: CH08–CH15: CHb-subclass singlets.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7708, img94</strong></td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>32.8</td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;17</td>
<td>tolerance</td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7708, img94</strong></td>
</tr>
<tr class="even">
<td> </td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>32.8</td>
</tr>
<tr class="odd">
<td> </td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;17</td>
</tr>
<tr class="even">
<td> </td>
<td>tolerance</td>
<td>e-2</td>
</tr>
<tr class="odd">
<td> </td>
<td>effective length <strong>TBD: inline7590, img86</strong> [mm]</td>
<td>71</td>
</tr>
<tr class="even">
<td>construction</td>
<td>pole length <strong>TBD: inline7592, img87</strong> (estimated) [mm]</td>
<td>59</td>
</tr>
<tr class="odd">
<td> </td>
<td>pole radius <strong>TBD: inline7594, img88</strong> [mm]</td>
<td>21</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> per pole [ampere · turns] ( <strong>TBD: inline7722, img95</strong> )</td>
<td>5761</td>
</tr>
<tr class="odd">
<td>coil</td>
<td>coil effective cross section (estimated) [mm^2]</td>
<td>311</td>
</tr>
<tr class="even">
<td> </td>
<td>electrical power per coil (estimated) [W]</td>
<td>576</td>
</tr>
<tr class="odd">
<td> </td>
<td>total magnet electrical power (estimated) [W]</td>
<td>2305</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: CH08--CH15: CHb-subclass singlets.

 

**Table:** MEBT-3: i-subclass dipoles.

<table>
<caption>Table: MEBT-3: i-subclass dipoles.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.59</td>
<td>central field <strong>TBD: inline7644, img89</strong> [T]</td>
<td>0.49</td>
<td>width of constant field zone [mm]</td>
<td>tbd</td>
<td>tolerance</td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.59</td>
</tr>
<tr class="even">
<td> </td>
<td>central field <strong>TBD: inline7644, img89</strong> [T]</td>
<td>0.49</td>
</tr>
<tr class="odd">
<td> </td>
<td>width of constant field zone [mm]</td>
<td>tbd</td>
</tr>
<tr class="even">
<td> </td>
<td>tolerance</td>
<td>tbd</td>
</tr>
<tr class="odd">
<td> </td>
<td>gap size <strong>TBD: inline7646, img90</strong> [mm]</td>
<td>80</td>
</tr>
<tr class="even">
<td> </td>
<td>effective length <strong>TBD: inline7564, img83</strong> [mm]</td>
<td>942</td>
</tr>
<tr class="odd">
<td> </td>
<td>Entrance face angle <strong>TBD: inline7750, img96</strong> [°]</td>
<td>0</td>
</tr>
<tr class="even">
<td> </td>
<td>Exit face angle <strong>TBD: inline7752, img97</strong> [°]</td>
<td>0</td>
</tr>
<tr class="odd">
<td>construction</td>
<td>pole length <strong>TBD: inline7650, img91</strong> [mm]</td>
<td>tbd</td>
</tr>
<tr class="even">
<td> </td>
<td>pole width <strong>TBD: inline7652, img92</strong> [mm]</td>
<td>100</td>
</tr>
<tr class="odd">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> total [ampere · turns] ( <strong>TBD: inline7762, img98</strong> )</td>
<td>31371</td>
</tr>
<tr class="even">
<td>coils</td>
<td>number of coils</td>
<td>2</td>
</tr>
<tr class="odd">
<td> </td>
<td>coil cross section [mm^2]</td>
<td>tbd</td>
</tr>
<tr class="even">
<td> </td>
<td>electrical power per coil [W]</td>
<td>2518</td>
</tr>
<tr class="odd">
<td> </td>
<td>total magnet electrical power [W]</td>
<td>5036</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: MEBT-3: i-subclass dipoles.

 

**Table:** : lb-subclass singlets.

<table>
<caption>Table: : lb-subclass singlets.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7788, img99</strong></td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>16.3</td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;25</td>
<td>tolerance</td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7788, img99</strong></td>
</tr>
<tr class="even">
<td> </td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>16.3</td>
</tr>
<tr class="odd">
<td> </td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;25</td>
</tr>
<tr class="even">
<td> </td>
<td>tolerance</td>
<td>e-2</td>
</tr>
<tr class="odd">
<td> </td>
<td>effective length <strong>TBD: inline7590, img86</strong> [mm]</td>
<td>123</td>
</tr>
<tr class="even">
<td>construction</td>
<td>pole length <strong>TBD: inline7592, img87</strong> [mm]</td>
<td>120</td>
</tr>
<tr class="odd">
<td> </td>
<td>pole radius <strong>TBD: inline7594, img88</strong> [mm]</td>
<td>36.5</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> per pole [ampere · turns] ( <strong>TBD: inline7722, img95</strong> )</td>
<td>8619</td>
</tr>
<tr class="odd">
<td>coil</td>
<td>coil effective cross section [mm^2]</td>
<td>455</td>
</tr>
<tr class="even">
<td> </td>
<td>electrical power per coil [W]</td>
<td>1956</td>
</tr>
<tr class="odd">
<td> </td>
<td>total magnet electrical power [W]</td>
<td>7825</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: : lb-subclass singlets.

 

**Table:** : lb-subclass steerers.

<table>
<caption>Table: : lb-subclass steerers.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7788, img99</strong></td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>central field H <strong>TBD: inline7560, img81</strong> [mT]</td>
<td>16.3</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>central field V <strong>TBD: inline7562, img82</strong> [mT]</td>
<td>16.3</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>effective length <strong>TBD: inline7564, img83</strong> [mm]</td>
<td>123</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>steering angles H [mradian]</td>
<td><strong>TBD: inline7832, img100</strong></td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>steering angles V [mradian]</td>
<td><strong>TBD: inline7832, img100</strong></td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td>construction</td>
<td>bore radius <strong>TBD: inline7566, img84</strong> [mm]</td>
<td>36.5</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> steerer H and V [ampere · turns]</td>
<td>947</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td>steerer coils</td>
<td>coil effective cross section [mm^2]</td>
<td>140</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>total electrical power (1 direction) [W]</td>
<td>95</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7788, img99</strong></td>
</tr>
<tr class="even">
<td> </td>
<td>central field H <strong>TBD: inline7560, img81</strong> [mT]</td>
<td>16.3</td>
</tr>
<tr class="odd">
<td> </td>
<td>central field V <strong>TBD: inline7562, img82</strong> [mT]</td>
<td>16.3</td>
</tr>
<tr class="even">
<td> </td>
<td>effective length <strong>TBD: inline7564, img83</strong> [mm]</td>
<td>123</td>
</tr>
<tr class="odd">
<td> </td>
<td>steering angles H [mradian]</td>
<td><strong>TBD: inline7832, img100</strong></td>
</tr>
<tr class="even">
<td> </td>
<td>steering angles V [mradian]</td>
<td><strong>TBD: inline7832, img100</strong></td>
</tr>
<tr class="odd">
<td>construction</td>
<td>bore radius <strong>TBD: inline7566, img84</strong> [mm]</td>
<td>36.5</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> steerer H and V [ampere · turns]</td>
<td>947</td>
</tr>
<tr class="odd">
<td>steerer coils</td>
<td>coil effective cross section [mm^2]</td>
<td>140</td>
</tr>
<tr class="even">
<td> </td>
<td>total electrical power (1 direction) [W]</td>
<td>95</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: : lb-subclass steerers.

 

**Table:** Input parameters to the switching magnet study.

<table>
<caption>Table: Input parameters to the switching magnet study.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Parameter</td>
<td>Value</td>
</tr>
<tr class="even">
<td>Input energy (MeV)</td>
<td>16.6</td>
</tr>
<tr class="odd">
<td>Magnetic rigidity protons at input energy (Tm)</td>
<td>0.59</td>
</tr>
<tr class="even">
<td>Beam RMS diameter in dipole (mm)</td>
<td>&lt;20</td>
</tr>
<tr class="odd">
<td>Angle input line/ high energy line (°)</td>
<td>45</td>
</tr>
<tr class="even">
<td>Max nominal field switching time (s)</td>
<td>1.5</td>
</tr>
<tr class="odd">
<td>Angle input and output faces of dipoles/beam angle (degree)</td>
<td>0</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Input parameters to the switching magnet study.

 

**Table:** Switching dipole magnetic parameters.

|     |
|-----|
|     |

Table: Switching dipole magnetic parameters.

 

**Table:** Dynamic dipole working parameters.

<table>
<caption>Table: Dynamic dipole working parameters.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Parameter</td>
<td>Value</td>
</tr>
<tr class="even">
<td>Size of conductor (mm × mm)</td>
<td>9 × 9</td>
</tr>
<tr class="odd">
<td>Diameter water cooling pipe (mm)</td>
<td>4</td>
</tr>
<tr class="even">
<td>Length of 1 turn (m)</td>
<td>2</td>
</tr>
<tr class="odd">
<td>Number of turns per pancake</td>
<td>6</td>
</tr>
<tr class="even">
<td>Number of pancakes per coil</td>
<td>4</td>
</tr>
<tr class="odd">
<td>Total magnet resistance (mOhms)</td>
<td>24.16</td>
</tr>
<tr class="even">
<td>Total magnet inductance (mH)</td>
<td>0.64</td>
</tr>
<tr class="odd">
<td>Coil current (A)</td>
<td>900</td>
</tr>
<tr class="even">
<td>Electromotive force (A·turns)</td>
<td>21600</td>
</tr>
<tr class="odd">
<td>Effective current density (A / )</td>
<td>13.3</td>
</tr>
<tr class="even">
<td>Input temperature water (℃)</td>
<td>20</td>
</tr>
<tr class="odd">
<td>Output temperature water (℃)</td>
<td>38</td>
</tr>
<tr class="even">
<td>Water flow per pancake (l/min)</td>
<td>2</td>
</tr>
<tr class="odd">
<td>Pressure drop per pancake (bars)</td>
<td>4</td>
</tr>
<tr class="even">
<td>Total water flow per coil (l/min)</td>
<td>8</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Dynamic dipole working parameters.

 

**Table:** Summary of parameters of the MINERVA deflecting and focusing
devices.

<table>
<caption>Table: Summary of parameters of the MINERVA deflecting and focusing devices.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>2<strong>Injector</strong></td>
<td>2<strong>MEBT-3</strong></td>
<td>2<strong>Single-spoke</strong><br />
SC Linac<strong>linac</strong></td>
<td>2</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td><strong>Solenoids</strong></td>
<td>2</td>
<td>0</td>
<td>0</td>
<td>tbd</td>
</tr>
<tr class="even">
<td>2<strong>Steerers</strong><br />
<strong>(H and V)</strong></td>
<td>2<strong>TBD: inline7864, img101</strong></td>
<td>2tbd</td>
<td>230</td>
<td>2tbd</td>
</tr>
<tr class="odd">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td><strong>Dipoles</strong></td>
<td>0</td>
<td>1× i</td>
<td>0</td>
<td>tbd</td>
</tr>
<tr class="odd">
<td>2<strong>Quadrupole</strong><br />
<strong>doublets</strong></td>
<td>27 × (2× CHa)<br />
9 × (2× CHb)</td>
<td>22 × (2× lb)</td>
<td>230 ×<br />
(2× lb)</td>
<td>2tbd</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>2<strong>Quadrupole</strong><br />
<strong>triplets</strong></td>
<td>21 × (2×<br />
me1s + 1× me2l)</td>
<td>23 × (3× lb)</td>
<td>20</td>
<td>2tbd</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>2<strong>Specific</strong><br />
<strong>devices</strong></td>
<td>21 × a-chopper</td>
<td>21 × s-dipole<br />
1 × i-rastering</td>
<td>20</td>
<td>2tbd</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>2<strong>Total nominal</strong><br />
<strong>power (kW)</strong></td>
<td>251</td>
<td>245</td>
<td>2232</td>
<td>2tbd</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Summary of parameters of the MINERVA deflecting and focusing
devices.

 

**Table:** LEBT solenoids parameters.

<table>
<caption>Table: LEBT solenoids parameters.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.025</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>central field <strong>TBD: inline7522, img72</strong> [T]</td>
<td>0.25</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>radius of good field zone [mm]</td>
<td>50</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>magnetic length <strong>TBD: inline7524, img73</strong> [mm]</td>
<td>241</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>effective length<strong>TBD: inline7526, img74</strong> <strong>TBD: inline7528, img75</strong> [mm]</td>
<td>184</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td>construction</td>
<td>overall solenoid length incl. end plates <strong>TBD: inline7530, img76</strong> [mm]</td>
<td>260</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>end plates thickness (clamp) [mm]</td>
<td>22</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>bore radius with steerers installed <strong>TBD: inline7532, img77</strong> [mm]</td>
<td>76</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> solenoid [ampere · turns]</td>
<td>48900</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td>[] solenoid coil</td>
<td>effective coil cross section [mm^2]</td>
<td>10500</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>electrical power [W]</td>
<td>4000</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td colspan="3"><strong>TBD: inline7538, img79</strong> the solenoid effective length for its focusing strength is <strong>TBD: inline7540, img80</strong></td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.025</td>
</tr>
<tr class="even">
<td> </td>
<td>central field <strong>TBD: inline7522, img72</strong> [T]</td>
<td>0.25</td>
</tr>
<tr class="odd">
<td> </td>
<td>radius of good field zone [mm]</td>
<td>50</td>
</tr>
<tr class="even">
<td> </td>
<td>magnetic length <strong>TBD: inline7524, img73</strong> [mm]</td>
<td>241</td>
</tr>
<tr class="odd">
<td> </td>
<td>effective length <strong>TBD: inline7528, img75</strong> [mm]</td>
<td>184</td>
</tr>
<tr class="even">
<td>construction</td>
<td>overall solenoid length incl. end plates <strong>TBD: inline7530, img76</strong> [mm]</td>
<td>260</td>
</tr>
<tr class="odd">
<td> </td>
<td>end plates thickness (clamp) [mm]</td>
<td>22</td>
</tr>
<tr class="even">
<td> </td>
<td>bore radius with steerers installed <strong>TBD: inline7532, img77</strong> [mm]</td>
<td>76</td>
</tr>
<tr class="odd">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> solenoid [ampere · turns]</td>
<td>48900</td>
</tr>
<tr class="even">
<td>solenoid coil</td>
<td>effective coil cross section [mm^2]</td>
<td>10500</td>
</tr>
<tr class="odd">
<td> </td>
<td>electrical power [W]</td>
<td>4000</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: LEBT solenoids parameters.

 

**Table:** LEBT steerers parameters.

<table>
<caption>Table: LEBT steerers parameters.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.025</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>central field H <strong>TBD: inline7560, img81</strong> [mT]</td>
<td>2.38</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>central field V <strong>TBD: inline7562, img82</strong> [mT]</td>
<td>2.24</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>effective length <strong>TBD: inline7564, img83</strong> [mm]</td>
<td>168</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>steering angles H [mradian]</td>
<td>16.0</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>steering angles V [mradian]</td>
<td>14.8</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td>construction</td>
<td>bore radius <strong>TBD: inline7566, img84</strong> [mm]</td>
<td>76</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> steerer H and V [ampere · turns]</td>
<td>500</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td>steerer coils</td>
<td>coil cross section [mm^2]</td>
<td>80</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>total electrical power (1 direction) [W]</td>
<td>40</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.025</td>
</tr>
<tr class="even">
<td> </td>
<td>central field H <strong>TBD: inline7560, img81</strong> [mT]</td>
<td>2.38</td>
</tr>
<tr class="odd">
<td> </td>
<td>central field V <strong>TBD: inline7562, img82</strong> [mT]</td>
<td>2.24</td>
</tr>
<tr class="even">
<td> </td>
<td>effective length <strong>TBD: inline7564, img83</strong> [mm]</td>
<td>168</td>
</tr>
<tr class="odd">
<td> </td>
<td>steering angles H [mradian]</td>
<td>16.0</td>
</tr>
<tr class="even">
<td> </td>
<td>steering angles V [mradian]</td>
<td>14.8</td>
</tr>
<tr class="odd">
<td>construction</td>
<td>bore radius <strong>TBD: inline7566, img84</strong> [mm]</td>
<td>76</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> steerer H and V [ampere · turns]</td>
<td>500</td>
</tr>
<tr class="odd">
<td>steerer coils</td>
<td>coil cross section [mm^2]</td>
<td>80</td>
</tr>
<tr class="even">
<td> </td>
<td>total electrical power (1 direction) [W]</td>
<td>40</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: LEBT steerers parameters.

 

**Table:** MEBT-1 me1s-subclass singlets.

<table>
<caption>Table: MEBT-1 me1s-subclass singlets.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.18</td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>19.1</td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;15</td>
<td>tolerance</td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.18</td>
</tr>
<tr class="even">
<td> </td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>19.1</td>
</tr>
<tr class="odd">
<td> </td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;15</td>
</tr>
<tr class="even">
<td> </td>
<td>tolerance</td>
<td>e-2</td>
</tr>
<tr class="odd">
<td> </td>
<td>effective length <strong>TBD: inline7590, img86</strong> (assembled) [mm]</td>
<td>51.6</td>
</tr>
<tr class="even">
<td>construction</td>
<td>pole length <strong>TBD: inline7592, img87</strong> [mm]</td>
<td>40.0</td>
</tr>
<tr class="odd">
<td> </td>
<td>pole radius <strong>TBD: inline7594, img88</strong> [mm]</td>
<td>20.0</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> per pole [ampere·turns]</td>
<td>3108</td>
</tr>
<tr class="odd">
<td>coil</td>
<td>coil effective cross section [mm^2]</td>
<td>311</td>
</tr>
<tr class="even">
<td> </td>
<td>electrical power per coil [W]</td>
<td>150</td>
</tr>
<tr class="odd">
<td> </td>
<td>total magnet electrical power [W]</td>
<td>599</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: MEBT-1 me1s-subclass singlets.

 

**Table:** MEBT-1 me1l-subclass singlets.

<table>
<caption>Table: MEBT-1 me1l-subclass singlets.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.18</td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>22.5</td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;15</td>
<td>tolerance</td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.18</td>
</tr>
<tr class="even">
<td> </td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>22.5</td>
</tr>
<tr class="odd">
<td> </td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;15</td>
</tr>
<tr class="even">
<td> </td>
<td>tolerance</td>
<td>e-2</td>
</tr>
<tr class="odd">
<td> </td>
<td>effective length <strong>TBD: inline7590, img86</strong> (assembled) [mm]</td>
<td>78.2</td>
</tr>
<tr class="even">
<td>construction</td>
<td>pole length <strong>TBD: inline7592, img87</strong> [mm]</td>
<td>72.3</td>
</tr>
<tr class="odd">
<td> </td>
<td>pole radius <strong>TBD: inline7594, img88</strong> [mm]</td>
<td>20</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> per pole [ampere · turns]</td>
<td>3668</td>
</tr>
<tr class="odd">
<td>coil</td>
<td>coil effective cross section [mm^2]</td>
<td>311</td>
</tr>
<tr class="even">
<td> </td>
<td>electrical power per coil [W]</td>
<td>255</td>
</tr>
<tr class="odd">
<td> </td>
<td>total magnet electrical power [W]</td>
<td>1022</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: MEBT-1 me1l-subclass singlets.

 

**Table:** MEBT-1 H- or V-steerers.

<table>
<caption>Table: MEBT-1 H- or V-steerers.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.18</td>
<td>central field <strong>TBD: inline7644, img89</strong> [T]</td>
<td>0.030</td>
<td>gap size <strong>TBD: inline7646, img90</strong> [mm]</td>
<td>40</td>
<td>effective length <strong>TBD: inline7564, img83</strong> [mm]</td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.18</td>
</tr>
<tr class="even">
<td> </td>
<td>central field <strong>TBD: inline7644, img89</strong> [T]</td>
<td>0.030</td>
</tr>
<tr class="odd">
<td> </td>
<td>gap size <strong>TBD: inline7646, img90</strong> [mm]</td>
<td>40</td>
</tr>
<tr class="even">
<td> </td>
<td>effective length <strong>TBD: inline7564, img83</strong> [mm]</td>
<td>85</td>
</tr>
<tr class="odd">
<td> </td>
<td>steering angles H [mradian]</td>
<td>99.9</td>
</tr>
<tr class="even">
<td>construction</td>
<td>pole length <strong>TBD: inline7650, img91</strong> [mm]</td>
<td>20</td>
</tr>
<tr class="odd">
<td> </td>
<td>pole width <strong>TBD: inline7652, img92</strong> [mm]</td>
<td>65</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> total [ampere · turns]</td>
<td>1440</td>
</tr>
<tr class="odd">
<td>coils</td>
<td>number of coils</td>
<td>2</td>
</tr>
<tr class="even">
<td> </td>
<td>coil effective cross section [mm^2]</td>
<td>314</td>
</tr>
<tr class="odd">
<td> </td>
<td>electrical power per coil [W]</td>
<td>49</td>
</tr>
<tr class="even">
<td> </td>
<td>total magnet electrical power (1 direction) [W]</td>
<td>97</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: MEBT-1 H- or V-steerers.

 

**Table:** CH01 (I1-CH

7:RF-CAV-01) -- CH07 (I1-CH

1:RF-CAV-01): CHa-subclass singlets.

<table>
<caption>Table: CH01 (I1-CH7:RF-CAV-01) – CH07 (I1-CH1:RF-CAV-01): CHa-subclass singlets.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7676, img93</strong></td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>35.6</td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;15</td>
<td>tolerance</td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7676, img93</strong></td>
</tr>
<tr class="even">
<td> </td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>35.6</td>
</tr>
<tr class="odd">
<td> </td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;15</td>
</tr>
<tr class="even">
<td> </td>
<td>tolerance</td>
<td>e-2</td>
</tr>
<tr class="odd">
<td> </td>
<td>effective length <strong>TBD: inline7590, img86</strong> (assembled) [mm]</td>
<td>51.9</td>
</tr>
<tr class="even">
<td>construction</td>
<td>pole length <strong>TBD: inline7592, img87</strong> [mm]</td>
<td>40</td>
</tr>
<tr class="odd">
<td> </td>
<td>pole radius <strong>TBD: inline7594, img88</strong> [mm]</td>
<td>17</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> per pole [ampere · turns]</td>
<td>4508</td>
</tr>
<tr class="odd">
<td>coil</td>
<td>coil effective cross section [mm^2]</td>
<td>311</td>
</tr>
<tr class="even">
<td> </td>
<td>electrical power per coil [W]</td>
<td>310</td>
</tr>
<tr class="odd">
<td> </td>
<td>total magnet electrical power [W]</td>
<td>1240</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: CH01 (I1-CH7:RF-CAV-01) -- CH07 (I1-CH1:RF-CAV-01): CHa-subclass
singlets.

 

**Table:** CH08--CH15: CHb-subclass singlets.

<table>
<caption>Table: CH08–CH15: CHb-subclass singlets.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7708, img94</strong></td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>32.8</td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;17</td>
<td>tolerance</td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7708, img94</strong></td>
</tr>
<tr class="even">
<td> </td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>32.8</td>
</tr>
<tr class="odd">
<td> </td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;17</td>
</tr>
<tr class="even">
<td> </td>
<td>tolerance</td>
<td>e-2</td>
</tr>
<tr class="odd">
<td> </td>
<td>effective length <strong>TBD: inline7590, img86</strong> [mm]</td>
<td>71</td>
</tr>
<tr class="even">
<td>construction</td>
<td>pole length <strong>TBD: inline7592, img87</strong> (estimated) [mm]</td>
<td>59</td>
</tr>
<tr class="odd">
<td> </td>
<td>pole radius <strong>TBD: inline7594, img88</strong> [mm]</td>
<td>21</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> per pole [ampere · turns] ( <strong>TBD: inline7722, img95</strong> )</td>
<td>5761</td>
</tr>
<tr class="odd">
<td>coil</td>
<td>coil effective cross section (estimated) [mm^2]</td>
<td>311</td>
</tr>
<tr class="even">
<td> </td>
<td>electrical power per coil (estimated) [W]</td>
<td>576</td>
</tr>
<tr class="odd">
<td> </td>
<td>total magnet electrical power (estimated) [W]</td>
<td>2305</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: CH08--CH15: CHb-subclass singlets.

 

**Table:** MEBT-3: i-subclass dipoles.

<table>
<caption>Table: MEBT-3: i-subclass dipoles.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.59</td>
<td>central field <strong>TBD: inline7644, img89</strong> [T]</td>
<td>0.49</td>
<td>width of constant field zone [mm]</td>
<td>tbd</td>
<td>tolerance</td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td>0.59</td>
</tr>
<tr class="even">
<td> </td>
<td>central field <strong>TBD: inline7644, img89</strong> [T]</td>
<td>0.49</td>
</tr>
<tr class="odd">
<td> </td>
<td>width of constant field zone [mm]</td>
<td>tbd</td>
</tr>
<tr class="even">
<td> </td>
<td>tolerance</td>
<td>tbd</td>
</tr>
<tr class="odd">
<td> </td>
<td>gap size <strong>TBD: inline7646, img90</strong> [mm]</td>
<td>80</td>
</tr>
<tr class="even">
<td> </td>
<td>effective length <strong>TBD: inline7564, img83</strong> [mm]</td>
<td>942</td>
</tr>
<tr class="odd">
<td> </td>
<td>Entrance face angle <strong>TBD: inline7750, img96</strong> [°]</td>
<td>0</td>
</tr>
<tr class="even">
<td> </td>
<td>Exit face angle <strong>TBD: inline7752, img97</strong> [°]</td>
<td>0</td>
</tr>
<tr class="odd">
<td>construction</td>
<td>pole length <strong>TBD: inline7650, img91</strong> [mm]</td>
<td>tbd</td>
</tr>
<tr class="even">
<td> </td>
<td>pole width <strong>TBD: inline7652, img92</strong> [mm]</td>
<td>100</td>
</tr>
<tr class="odd">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> total [ampere · turns] ( <strong>TBD: inline7762, img98</strong> )</td>
<td>31371</td>
</tr>
<tr class="even">
<td>coils</td>
<td>number of coils</td>
<td>2</td>
</tr>
<tr class="odd">
<td> </td>
<td>coil cross section [mm^2]</td>
<td>tbd</td>
</tr>
<tr class="even">
<td> </td>
<td>electrical power per coil [W]</td>
<td>2518</td>
</tr>
<tr class="odd">
<td> </td>
<td>total magnet electrical power [W]</td>
<td>5036</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: MEBT-3: i-subclass dipoles.

 

**Table:** : lb-subclass singlets.

<table>
<caption>Table: : lb-subclass singlets.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7788, img99</strong></td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>16.3</td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;25</td>
<td>tolerance</td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7788, img99</strong></td>
</tr>
<tr class="even">
<td> </td>
<td>central gradient <strong>TBD: inline7588, img85</strong> [T/m]</td>
<td>16.3</td>
</tr>
<tr class="odd">
<td> </td>
<td>radius of constant gradient zone [mm]</td>
<td>&gt;25</td>
</tr>
<tr class="even">
<td> </td>
<td>tolerance</td>
<td>e-2</td>
</tr>
<tr class="odd">
<td> </td>
<td>effective length <strong>TBD: inline7590, img86</strong> [mm]</td>
<td>123</td>
</tr>
<tr class="even">
<td>construction</td>
<td>pole length <strong>TBD: inline7592, img87</strong> [mm]</td>
<td>120</td>
</tr>
<tr class="odd">
<td> </td>
<td>pole radius <strong>TBD: inline7594, img88</strong> [mm]</td>
<td>36.5</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> per pole [ampere · turns] ( <strong>TBD: inline7722, img95</strong> )</td>
<td>8619</td>
</tr>
<tr class="odd">
<td>coil</td>
<td>coil effective cross section [mm^2]</td>
<td>455</td>
</tr>
<tr class="even">
<td> </td>
<td>electrical power per coil [W]</td>
<td>1956</td>
</tr>
<tr class="odd">
<td> </td>
<td>total magnet electrical power [W]</td>
<td>7825</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: : lb-subclass singlets.

 

**Table:** : lb-subclass steerers.

<table>
<caption>Table: : lb-subclass steerers.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7788, img99</strong></td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>central field H <strong>TBD: inline7560, img81</strong> [mT]</td>
<td>16.3</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>central field V <strong>TBD: inline7562, img82</strong> [mT]</td>
<td>16.3</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>effective length <strong>TBD: inline7564, img83</strong> [mm]</td>
<td>123</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td> </td>
<td>steering angles H [mradian]</td>
<td><strong>TBD: inline7832, img100</strong></td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>steering angles V [mradian]</td>
<td><strong>TBD: inline7832, img100</strong></td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td>construction</td>
<td>bore radius <strong>TBD: inline7566, img84</strong> [mm]</td>
<td>36.5</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> steerer H and V [ampere · turns]</td>
<td>947</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="odd">
<td>steerer coils</td>
<td>coil effective cross section [mm^2]</td>
<td>140</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
<tr class="even">
<td> </td>
<td>total electrical power (1 direction) [W]</td>
<td>95</td>
<td> </td>
<td> </td>
<td> </td>
<td></td>
<td> </td>
</tr>
</tbody>
</table>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>beam dynamics</td>
<td><strong>TBD: inline7520, img71</strong> [Tm]</td>
<td><strong>TBD: inline7788, img99</strong></td>
</tr>
<tr class="even">
<td> </td>
<td>central field H <strong>TBD: inline7560, img81</strong> [mT]</td>
<td>16.3</td>
</tr>
<tr class="odd">
<td> </td>
<td>central field V <strong>TBD: inline7562, img82</strong> [mT]</td>
<td>16.3</td>
</tr>
<tr class="even">
<td> </td>
<td>effective length <strong>TBD: inline7564, img83</strong> [mm]</td>
<td>123</td>
</tr>
<tr class="odd">
<td> </td>
<td>steering angles H [mradian]</td>
<td><strong>TBD: inline7832, img100</strong></td>
</tr>
<tr class="even">
<td> </td>
<td>steering angles V [mradian]</td>
<td><strong>TBD: inline7832, img100</strong></td>
</tr>
<tr class="odd">
<td>construction</td>
<td>bore radius <strong>TBD: inline7566, img84</strong> [mm]</td>
<td>36.5</td>
</tr>
<tr class="even">
<td> </td>
<td><strong>TBD: inline7534, img78</strong> steerer H and V [ampere · turns]</td>
<td>947</td>
</tr>
<tr class="odd">
<td>steerer coils</td>
<td>coil effective cross section [mm^2]</td>
<td>140</td>
</tr>
<tr class="even">
<td> </td>
<td>total electrical power (1 direction) [W]</td>
<td>95</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: : lb-subclass steerers.

 

**Table:** Input parameters to the switching magnet study.

<table>
<caption>Table: Input parameters to the switching magnet study.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Parameter</td>
<td>Value</td>
</tr>
<tr class="even">
<td>Input energy (MeV)</td>
<td>16.6</td>
</tr>
<tr class="odd">
<td>Magnetic rigidity protons at input energy (Tm)</td>
<td>0.59</td>
</tr>
<tr class="even">
<td>Beam RMS diameter in dipole (mm)</td>
<td>&lt;20</td>
</tr>
<tr class="odd">
<td>Angle input line/ high energy line (°)</td>
<td>45</td>
</tr>
<tr class="even">
<td>Max nominal field switching time (s)</td>
<td>1.5</td>
</tr>
<tr class="odd">
<td>Angle input and output faces of dipoles/beam angle (degree)</td>
<td>0</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Input parameters to the switching magnet study.

 

**Table:** Switching dipole magnetic parameters.

|     |
|-----|
|     |

Table: Switching dipole magnetic parameters.

 

**Table:** Dynamic dipole working parameters.

<table>
<caption>Table: Dynamic dipole working parameters.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Parameter</td>
<td>Value</td>
</tr>
<tr class="even">
<td>Size of conductor (mm × mm)</td>
<td>9 × 9</td>
</tr>
<tr class="odd">
<td>Diameter water cooling pipe (mm)</td>
<td>4</td>
</tr>
<tr class="even">
<td>Length of 1 turn (m)</td>
<td>2</td>
</tr>
<tr class="odd">
<td>Number of turns per pancake</td>
<td>6</td>
</tr>
<tr class="even">
<td>Number of pancakes per coil</td>
<td>4</td>
</tr>
<tr class="odd">
<td>Total magnet resistance (mOhms)</td>
<td>24.16</td>
</tr>
<tr class="even">
<td>Total magnet inductance (mH)</td>
<td>0.64</td>
</tr>
<tr class="odd">
<td>Coil current (A)</td>
<td>900</td>
</tr>
<tr class="even">
<td>Electromotive force (A·turns)</td>
<td>21600</td>
</tr>
<tr class="odd">
<td>Effective current density (A / )</td>
<td>13.3</td>
</tr>
<tr class="even">
<td>Input temperature water (℃)</td>
<td>20</td>
</tr>
<tr class="odd">
<td>Output temperature water (℃)</td>
<td>38</td>
</tr>
<tr class="even">
<td>Water flow per pancake (l/min)</td>
<td>2</td>
</tr>
<tr class="odd">
<td>Pressure drop per pancake (bars)</td>
<td>4</td>
</tr>
<tr class="even">
<td>Total water flow per coil (l/min)</td>
<td>8</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Dynamic dipole working parameters.

 

**Table:** Summary of parameters of the MINERVA deflecting and focusing
devices.

<table>
<caption>Table: Summary of parameters of the MINERVA deflecting and focusing devices.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td>2<strong>Injector</strong></td>
<td>2<strong>MEBT-3</strong></td>
<td>2<strong>Single-spoke</strong><br />
SC Linac<strong>linac</strong></td>
<td>2</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td><strong>Solenoids</strong></td>
<td>2</td>
<td>0</td>
<td>0</td>
<td>tbd</td>
</tr>
<tr class="even">
<td>2<strong>Steerers</strong><br />
<strong>(H and V)</strong></td>
<td>2<strong>TBD: inline7864, img101</strong></td>
<td>2tbd</td>
<td>230</td>
<td>2tbd</td>
</tr>
<tr class="odd">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td><strong>Dipoles</strong></td>
<td>0</td>
<td>1× i</td>
<td>0</td>
<td>tbd</td>
</tr>
<tr class="odd">
<td>2<strong>Quadrupole</strong><br />
<strong>doublets</strong></td>
<td>27 × (2× CHa)<br />
9 × (2× CHb)</td>
<td>22 × (2× lb)</td>
<td>230 ×<br />
(2× lb)</td>
<td>2tbd</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>2<strong>Quadrupole</strong><br />
<strong>triplets</strong></td>
<td>21 × (2×<br />
me1s + 1× me2l)</td>
<td>23 × (3× lb)</td>
<td>20</td>
<td>2tbd</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>2<strong>Specific</strong><br />
<strong>devices</strong></td>
<td>21 × a-chopper</td>
<td>21 × s-dipole<br />
1 × i-rastering</td>
<td>20</td>
<td>2tbd</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>2<strong>Total nominal</strong><br />
<strong>power (kW)</strong></td>
<td>251</td>
<td>245</td>
<td>2232</td>
<td>2tbd</td>
</tr>
<tr class="even">
<td> </td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Summary of parameters of the MINERVA deflecting and focusing
devices.

 

**Table:** LEBT vacuum equipment list.

<table>
<caption>Table: LEBT vacuum equipment list.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Equipment</strong></td>
<td><strong>Quantity</strong></td>
</tr>
<tr class="even">
<td><strong>Primary dry pump 10 or 15m³ / h (scroll or multistage roots), with safety valve</strong></td>
<td>3</td>
</tr>
<tr class="odd">
<td><strong>Turbo molecular pump 300L / s, with venting valve</strong></td>
<td>3</td>
</tr>
<tr class="even">
<td><strong>Turbo molecular pump 80L / s, with venting valve</strong></td>
<td>1</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: LEBT vacuum equipment list.

 

**Table:** RFQ vacuum pump list.

<table>
<caption>Table: RFQ vacuum pump list.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Equipment</strong></td>
<td><strong>Quantity</strong></td>
</tr>
<tr class="even">
<td><strong>Primary dry pump 10 or 15m³ / h (scroll or multistage roots), with safety valve</strong></td>
<td>3</td>
</tr>
<tr class="odd">
<td><strong>Turbo-molecular pump 700L / s with venting valve</strong></td>
<td>3</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: RFQ vacuum pump list.

 

**Table:** CH Booster vacuum pump list.

<table>
<caption>Table: CH Booster vacuum pump list.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Equipment</strong></td>
<td><strong>MEBT-1</strong></td>
<td><strong>CH-section 1</strong></td>
<td><strong>MEBT-2</strong></td>
<td><strong>CH-section 2</strong></td>
<td><strong>Total</strong></td>
</tr>
<tr class="even">
<td><strong>Primary dry pump 10 or 15m³ / h (scroll or multistage roots), with safety valve</strong></td>
<td>1</td>
<td>3</td>
<td>1</td>
<td>3</td>
<td>8</td>
</tr>
<tr class="odd">
<td><strong>Turbo-molecular pump 700L / s, with venting valve</strong></td>
<td>2</td>
<td>2</td>
<td>2</td>
<td>2</td>
<td>8</td>
</tr>
<tr class="even">
<td><strong>Ion pumps</strong></td>
<td>2</td>
<td>7</td>
<td>2</td>
<td>7</td>
<td>18</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: CH Booster vacuum pump list.

 

**Table:** MEBT-3 (I1-ME

7 and LB-ME

3\) vacuum equipment list.

<table>
<caption>Table: MEBT-3 (I1-ME7 and LB-ME3) vacuum equipment list.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Equipment</strong></td>
<td><strong>Quantity</strong></td>
</tr>
<tr class="even">
<td><strong>Pumping group (dry primary pump with safety valve + turbo molecular pump with venting valve + isolation valve + bake-able full rang gauge)</strong></td>
<td>tbd</td>
</tr>
<tr class="odd">
<td><strong>Ion pump</strong></td>
<td>7</td>
</tr>
<tr class="even">
<td><strong>Optional NEG (Non Evaporable Getter) cartridge combined with IP1, IP2 and IP5</strong></td>
<td>(3)</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: MEBT-3 (I1-ME7 and LB-ME3) vacuum equipment list.

 

**Table:** SC LinacSpoke linac vacuum equipment list.

<table>
<caption>Table: SC LinacSpoke linac vacuum equipment list.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Equipment</strong></td>
<td><strong>Total</strong></td>
</tr>
<tr class="even">
<td><strong>Pumping group (dry primary pump with safety valve + turbo molecular pump</strong></td>
<td>tbd</td>
</tr>
<tr class="odd">
<td><strong>with venting valve + isolation valve + bake-able full range gauge)</strong></td>
<td> </td>
</tr>
<tr class="even">
<td><strong>Ion pump</strong></td>
<td>46</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: SC LinacSpoke linac vacuum equipment list.

 

**Table:** Accelerator vacuum equipment list.

<table>
<caption>Table: Accelerator vacuum equipment list.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td><strong>Source and LEBT</strong></td>
<td><strong>RFQ</strong></td>
<td><strong>CH-Booster</strong></td>
<td><strong>MEBT-3</strong></td>
<td><strong>SC LinacSingle spoke Linac</strong></td>
</tr>
<tr class="even">
<td><strong>primary pumps</strong></td>
<td>3</td>
<td>2</td>
<td>8</td>
<td>tbd</td>
<td>tbd</td>
</tr>
<tr class="odd">
<td><strong>Turbo Molecular Pumps</strong></td>
<td>4</td>
<td>3</td>
<td>8</td>
<td>tbd</td>
<td>tbd</td>
</tr>
<tr class="even">
<td><strong>Ion pumps</strong></td>
<td>-</td>
<td>-</td>
<td>18</td>
<td>7</td>
<td>60</td>
</tr>
<tr class="odd">
<td><strong>Pirani gauges</strong></td>
<td>7</td>
<td><strong>TBD: inline7866, img102</strong></td>
<td>20</td>
<td>7</td>
<td>60</td>
</tr>
<tr class="even">
<td><strong>Penning gauges</strong></td>
<td>5</td>
<td><strong>TBD: inline7868, img103</strong></td>
<td>34</td>
<td>7</td>
<td>60</td>
</tr>
<tr class="odd">
<td><strong>Vacuum low range (mbar)</strong></td>
<td>d-6</td>
<td>d-7</td>
<td>d-7</td>
<td>d-10</td>
<td>d-10</td>
</tr>
<tr class="even">
<td><strong>Vacuum high range (mbar)</strong></td>
<td>d-5</td>
<td>d-6</td>
<td>d-6</td>
<td>d-9</td>
<td>d-9</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Accelerator vacuum equipment list.

 

**Table:** LEBT vacuum equipment list.

<table>
<caption>Table: LEBT vacuum equipment list.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Equipment</strong></td>
<td><strong>Quantity</strong></td>
</tr>
<tr class="even">
<td><strong>Primary dry pump 10 or 15m³ / h (scroll or multistage roots), with safety valve</strong></td>
<td>3</td>
</tr>
<tr class="odd">
<td><strong>Turbo molecular pump 300L / s, with venting valve</strong></td>
<td>3</td>
</tr>
<tr class="even">
<td><strong>Turbo molecular pump 80L / s, with venting valve</strong></td>
<td>1</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: LEBT vacuum equipment list.

 

**Table:** RFQ vacuum pump list.

<table>
<caption>Table: RFQ vacuum pump list.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Equipment</strong></td>
<td><strong>Quantity</strong></td>
</tr>
<tr class="even">
<td><strong>Primary dry pump 10 or 15m³ / h (scroll or multistage roots), with safety valve</strong></td>
<td>3</td>
</tr>
<tr class="odd">
<td><strong>Turbo-molecular pump 700L / s with venting valve</strong></td>
<td>3</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: RFQ vacuum pump list.

 

**Table:** CH Booster vacuum pump list.

<table>
<caption>Table: CH Booster vacuum pump list.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Equipment</strong></td>
<td><strong>MEBT-1</strong></td>
<td><strong>CH-section 1</strong></td>
<td><strong>MEBT-2</strong></td>
<td><strong>CH-section 2</strong></td>
<td><strong>Total</strong></td>
</tr>
<tr class="even">
<td><strong>Primary dry pump 10 or 15m³ / h (scroll or multistage roots), with safety valve</strong></td>
<td>1</td>
<td>3</td>
<td>1</td>
<td>3</td>
<td>8</td>
</tr>
<tr class="odd">
<td><strong>Turbo-molecular pump 700L / s, with venting valve</strong></td>
<td>2</td>
<td>2</td>
<td>2</td>
<td>2</td>
<td>8</td>
</tr>
<tr class="even">
<td><strong>Ion pumps</strong></td>
<td>2</td>
<td>7</td>
<td>2</td>
<td>7</td>
<td>18</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: CH Booster vacuum pump list.

 

**Table:** MEBT-3 (I1-ME

3 and LB-ME

3\) vacuum equipment list.

<table>
<caption>Table: MEBT-3 (I1-ME3 and LB-ME3) vacuum equipment list.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Equipment</strong></td>
<td><strong>Quantity</strong></td>
</tr>
<tr class="even">
<td><strong>Pumping group (dry primary pump with safety valve + turbo molecular pump with venting valve + isolation valve + bake-able full rang gauge)</strong></td>
<td>tbd</td>
</tr>
<tr class="odd">
<td><strong>Ion pump</strong></td>
<td>7</td>
</tr>
<tr class="even">
<td><strong>Optional NEG (Non Evaporable Getter) cartridge combined with IP1, IP2 and IP5</strong></td>
<td>(3)</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: MEBT-3 (I1-ME3 and LB-ME3) vacuum equipment list.

 

**Table:** SC LinacSpoke linac vacuum equipment list.

<table>
<caption>Table: SC LinacSpoke linac vacuum equipment list.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Equipment</strong></td>
<td><strong>Total</strong></td>
</tr>
<tr class="even">
<td><strong>Pumping group (dry primary pump with safety valve + turbo molecular pump</strong></td>
<td>tbd</td>
</tr>
<tr class="odd">
<td><strong>with venting valve + isolation valve + bake-able full range gauge)</strong></td>
<td> </td>
</tr>
<tr class="even">
<td><strong>Ion pump</strong></td>
<td>46</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: SC LinacSpoke linac vacuum equipment list.

 

**Table:** Accelerator vacuum equipment list.

<table>
<caption>Table: Accelerator vacuum equipment list.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td><strong>Source and LEBT</strong></td>
<td><strong>RFQ</strong></td>
<td><strong>CH-Booster</strong></td>
<td><strong>MEBT-3</strong></td>
<td><strong>SC LinacSingle spoke Linac</strong></td>
</tr>
<tr class="even">
<td><strong>primary pumps</strong></td>
<td>3</td>
<td>2</td>
<td>8</td>
<td>tbd</td>
<td>tbd</td>
</tr>
<tr class="odd">
<td><strong>Turbo Molecular Pumps</strong></td>
<td>4</td>
<td>3</td>
<td>8</td>
<td>tbd</td>
<td>tbd</td>
</tr>
<tr class="even">
<td><strong>Ion pumps</strong></td>
<td>-</td>
<td>-</td>
<td>18</td>
<td>7</td>
<td>60</td>
</tr>
<tr class="odd">
<td><strong>Pirani gauges</strong></td>
<td>7</td>
<td><strong>TBD: inline7866, img102</strong></td>
<td>20</td>
<td>7</td>
<td>60</td>
</tr>
<tr class="even">
<td><strong>Penning gauges</strong></td>
<td>5</td>
<td><strong>TBD: inline7868, img103</strong></td>
<td>34</td>
<td>7</td>
<td>60</td>
</tr>
<tr class="odd">
<td><strong>Vacuum low range (mbar)</strong></td>
<td>d-6</td>
<td>d-7</td>
<td>d-7</td>
<td>d-10</td>
<td>d-10</td>
</tr>
<tr class="even">
<td><strong>Vacuum high range (mbar)</strong></td>
<td>d-5</td>
<td>d-6</td>
<td>d-6</td>
<td>d-9</td>
<td>d-9</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Accelerator vacuum equipment list.

 

**Table:** LEBT vacuum equipment list.

<table>
<caption>Table: LEBT vacuum equipment list.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Equipment</strong></td>
<td><strong>Quantity</strong></td>
</tr>
<tr class="even">
<td><strong>Primary dry pump 10 or 15m³ / h (scroll or multistage roots), with safety valve</strong></td>
<td>3</td>
</tr>
<tr class="odd">
<td><strong>Turbo molecular pump 300L / s, with venting valve</strong></td>
<td>3</td>
</tr>
<tr class="even">
<td><strong>Turbo molecular pump 80L / s, with venting valve</strong></td>
<td>1</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: LEBT vacuum equipment list.

 

**Table:** RFQ vacuum pump list.

<table>
<caption>Table: RFQ vacuum pump list.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Equipment</strong></td>
<td><strong>Quantity</strong></td>
</tr>
<tr class="even">
<td><strong>Primary dry pump 10 or 15m³ / h (scroll or multistage roots), with safety valve</strong></td>
<td>3</td>
</tr>
<tr class="odd">
<td><strong>Turbo-molecular pump 700L / s with venting valve</strong></td>
<td>3</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: RFQ vacuum pump list.

 

**Table:** CH Booster vacuum pump list.

<table>
<caption>Table: CH Booster vacuum pump list.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Equipment</strong></td>
<td><strong>MEBT-1</strong></td>
<td><strong>CH-section 1</strong></td>
<td><strong>MEBT-2</strong></td>
<td><strong>CH-section 2</strong></td>
<td><strong>Total</strong></td>
</tr>
<tr class="even">
<td><strong>Primary dry pump 10 or 15m³ / h (scroll or multistage roots), with safety valve</strong></td>
<td>1</td>
<td>3</td>
<td>1</td>
<td>3</td>
<td>8</td>
</tr>
<tr class="odd">
<td><strong>Turbo-molecular pump 700L / s, with venting valve</strong></td>
<td>2</td>
<td>2</td>
<td>2</td>
<td>2</td>
<td>8</td>
</tr>
<tr class="even">
<td><strong>Ion pumps</strong></td>
<td>2</td>
<td>7</td>
<td>2</td>
<td>7</td>
<td>18</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: CH Booster vacuum pump list.

 

**Table:** MEBT-3 (I1-ME

3 and LB-ME

3\) vacuum equipment list.

<table>
<caption>Table: MEBT-3 (I1-ME3 and LB-ME3) vacuum equipment list.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Equipment</strong></td>
<td><strong>Quantity</strong></td>
</tr>
<tr class="even">
<td><strong>Pumping group (dry primary pump with safety valve + turbo molecular pump with venting valve + isolation valve + bake-able full rang gauge)</strong></td>
<td>tbd</td>
</tr>
<tr class="odd">
<td><strong>Ion pump</strong></td>
<td>7</td>
</tr>
<tr class="even">
<td><strong>Optional NEG (Non Evaporable Getter) cartridge combined with IP1, IP2 and IP5</strong></td>
<td>(3)</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: MEBT-3 (I1-ME3 and LB-ME3) vacuum equipment list.

 

**Table:** SC LinacSpoke linac vacuum equipment list.

<table>
<caption>Table: SC LinacSpoke linac vacuum equipment list.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Equipment</strong></td>
<td><strong>Total</strong></td>
</tr>
<tr class="even">
<td><strong>Pumping group (dry primary pump with safety valve + turbo molecular pump</strong></td>
<td>tbd</td>
</tr>
<tr class="odd">
<td><strong>with venting valve + isolation valve + bake-able full range gauge)</strong></td>
<td> </td>
</tr>
<tr class="even">
<td><strong>Ion pump</strong></td>
<td>46</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: SC LinacSpoke linac vacuum equipment list.

 

**Table:** Accelerator vacuum equipment list.

<table>
<caption>Table: Accelerator vacuum equipment list.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td><strong>Source and LEBT</strong></td>
<td><strong>RFQ</strong></td>
<td><strong>CH-Booster</strong></td>
<td><strong>MEBT-3</strong></td>
<td><strong>SC LinacSingle spoke Linac</strong></td>
</tr>
<tr class="even">
<td><strong>primary pumps</strong></td>
<td>3</td>
<td>2</td>
<td>8</td>
<td>tbd</td>
<td>tbd</td>
</tr>
<tr class="odd">
<td><strong>Turbo Molecular Pumps</strong></td>
<td>4</td>
<td>3</td>
<td>8</td>
<td>tbd</td>
<td>tbd</td>
</tr>
<tr class="even">
<td><strong>Ion pumps</strong></td>
<td>-</td>
<td>-</td>
<td>18</td>
<td>7</td>
<td>60</td>
</tr>
<tr class="odd">
<td><strong>Pirani gauges</strong></td>
<td>7</td>
<td><strong>TBD: inline7866, img102</strong></td>
<td>20</td>
<td>7</td>
<td>60</td>
</tr>
<tr class="even">
<td><strong>Penning gauges</strong></td>
<td>5</td>
<td><strong>TBD: inline7868, img103</strong></td>
<td>34</td>
<td>7</td>
<td>60</td>
</tr>
<tr class="odd">
<td><strong>Vacuum low range (mbar)</strong></td>
<td>d-6</td>
<td>d-7</td>
<td>d-7</td>
<td>d-10</td>
<td>d-10</td>
</tr>
<tr class="even">
<td><strong>Vacuum high range (mbar)</strong></td>
<td>d-5</td>
<td>d-6</td>
<td>d-6</td>
<td>d-9</td>
<td>d-9</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Accelerator vacuum equipment list.

 

**Table:** LEBT vacuum equipment list.

<table>
<caption>Table: LEBT vacuum equipment list.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Equipment</strong></td>
<td><strong>Quantity</strong></td>
</tr>
<tr class="even">
<td><strong>Primary dry pump 10 or 15m³ / h (scroll or multistage roots), with safety valve</strong></td>
<td>3</td>
</tr>
<tr class="odd">
<td><strong>Turbo molecular pump 300L / s, with venting valve</strong></td>
<td>3</td>
</tr>
<tr class="even">
<td><strong>Turbo molecular pump 80L / s, with venting valve</strong></td>
<td>1</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: LEBT vacuum equipment list.

 

**Table:** RFQ vacuum pump list.

<table>
<caption>Table: RFQ vacuum pump list.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Equipment</strong></td>
<td><strong>Quantity</strong></td>
</tr>
<tr class="even">
<td><strong>Primary dry pump 10 or 15m³ / h (scroll or multistage roots), with safety valve</strong></td>
<td>3</td>
</tr>
<tr class="odd">
<td><strong>Turbo-molecular pump 700L / s with venting valve</strong></td>
<td>3</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: RFQ vacuum pump list.

 

**Table:** CH Booster vacuum pump list.

<table>
<caption>Table: CH Booster vacuum pump list.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Equipment</strong></td>
<td><strong>MEBT-1</strong></td>
<td><strong>CH-section 1</strong></td>
<td><strong>MEBT-2</strong></td>
<td><strong>CH-section 2</strong></td>
<td><strong>Total</strong></td>
</tr>
<tr class="even">
<td><strong>Primary dry pump 10 or 15m³ / h (scroll or multistage roots), with safety valve</strong></td>
<td>1</td>
<td>3</td>
<td>1</td>
<td>3</td>
<td>8</td>
</tr>
<tr class="odd">
<td><strong>Turbo-molecular pump 700L / s, with venting valve</strong></td>
<td>2</td>
<td>2</td>
<td>2</td>
<td>2</td>
<td>8</td>
</tr>
<tr class="even">
<td><strong>Ion pumps</strong></td>
<td>2</td>
<td>7</td>
<td>2</td>
<td>7</td>
<td>18</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: CH Booster vacuum pump list.

 

**Table:** MEBT-3 (I1-ME

3 and LB-ME

3\) vacuum equipment list.

<table>
<caption>Table: MEBT-3 (I1-ME3 and LB-ME3) vacuum equipment list.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Equipment</strong></td>
<td><strong>Quantity</strong></td>
</tr>
<tr class="even">
<td><strong>Pumping group (dry primary pump with safety valve + turbo molecular pump with venting valve + isolation valve + bake-able full rang gauge)</strong></td>
<td>tbd</td>
</tr>
<tr class="odd">
<td><strong>Ion pump</strong></td>
<td>7</td>
</tr>
<tr class="even">
<td><strong>Optional NEG (Non Evaporable Getter) cartridge combined with IP1, IP2 and IP5</strong></td>
<td>(3)</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: MEBT-3 (I1-ME3 and LB-ME3) vacuum equipment list.

 

**Table:** SC LinacSpoke linac vacuum equipment list.

<table>
<caption>Table: SC LinacSpoke linac vacuum equipment list.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Equipment</strong></td>
<td><strong>Total</strong></td>
</tr>
<tr class="even">
<td><strong>Pumping group (dry primary pump with safety valve + turbo molecular pump</strong></td>
<td>tbd</td>
</tr>
<tr class="odd">
<td><strong>with venting valve + isolation valve + bake-able full range gauge)</strong></td>
<td> </td>
</tr>
<tr class="even">
<td><strong>Ion pump</strong></td>
<td>46</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: SC LinacSpoke linac vacuum equipment list.

 

**Table:** Accelerator vacuum equipment list.

<table>
<caption>Table: Accelerator vacuum equipment list.</caption>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td> </td>
<td><strong>Source and LEBT</strong></td>
<td><strong>RFQ</strong></td>
<td><strong>CH-Booster</strong></td>
<td><strong>MEBT-3</strong></td>
<td><strong>SC LinacSingle spoke Linac</strong></td>
</tr>
<tr class="even">
<td><strong>primary pumps</strong></td>
<td>3</td>
<td>2</td>
<td>8</td>
<td>tbd</td>
<td>tbd</td>
</tr>
<tr class="odd">
<td><strong>Turbo Molecular Pumps</strong></td>
<td>4</td>
<td>3</td>
<td>8</td>
<td>tbd</td>
<td>tbd</td>
</tr>
<tr class="even">
<td><strong>Ion pumps</strong></td>
<td>-</td>
<td>-</td>
<td>18</td>
<td>7</td>
<td>60</td>
</tr>
<tr class="odd">
<td><strong>Pirani gauges</strong></td>
<td>7</td>
<td><strong>TBD: inline7866, img102</strong></td>
<td>20</td>
<td>7</td>
<td>60</td>
</tr>
<tr class="even">
<td><strong>Penning gauges</strong></td>
<td>5</td>
<td><strong>TBD: inline7868, img103</strong></td>
<td>34</td>
<td>7</td>
<td>60</td>
</tr>
<tr class="odd">
<td><strong>Vacuum low range (mbar)</strong></td>
<td>d-6</td>
<td>d-7</td>
<td>d-7</td>
<td>d-10</td>
<td>d-10</td>
</tr>
<tr class="even">
<td><strong>Vacuum high range (mbar)</strong></td>
<td>d-5</td>
<td>d-6</td>
<td>d-6</td>
<td>d-9</td>
<td>d-9</td>
</tr>
</tbody>
</table>
<p> </p></td>
</tr>
</tbody>
</table>

Table: Accelerator vacuum equipment list.

## Bibliography {#bibliography}

1

:   DE FLORIO, V.  
    Dependability and fault-tolerance --- basic concepts and
    terminology.  
    https://ecm.sckcen.be/OTCS/llisapi.dll?func=ll&objaction=overview&objid=39795792/39795792, 2020.

2

:   DORDA, U.  
    Beam parameter requirements on the 17 MeV beam dump.  
    https://ecm.sckcen.be/OTCS/llisapi.dll?func=ll&objaction=overview&objid=37685655/37685655,
    v. 0.13, August 10, 2020.

3

:   URIOT, D., ET AL.  
    Deliverable 1.4: Advanced Beam Dynamics Simulations.  
    http://ecm.sckcen.be/OTCS/llisapi.dll/open/5340624/5340624,
    August 2014.
