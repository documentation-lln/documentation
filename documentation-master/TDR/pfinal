#!/usr/bin/env bash
#
# Bash script to compile the TDR
# By vdflorio, Do Jan 16 16:04:35 CET 2020

file="MINERVA-100MeV-ACCELERATOR-TDR.tex"
pdf=`basename "$file" .tex`.pdf

# Versioning (begin)
version=$(<version)
subversion=$(<subversion)
subsubversion=$(<subsubversion)

##NOT_IN_THE_FINAL_VERSION## ((subsubversion++))
# Versioning (end)


cp version.template version.tex
##NOT_IN_THE_FINAL_VERSION## fgres -v '__VERSION__' "${version}.${subversion}.${subsubversion}" version.tex
##ONLY IN THE FINAL DRAFT VERSION:
fgres -v '__VERSION__' "${version}.${subversion}" version.tex
echo "Advancing version number (current version is now set to ${version}.${subversion}.${subsubversion})"
echo "FINAL DRAFT VERSION: LLN-v. ${version}.${subversion}"

# Embedding  history of changes into the output pdf
git log > git.log.txt
## soffice --convert-to pdf git.log.txt
# enscript -2 -f NewCenturySchlbk-Roman6 -F NewCenturySchlbk-Bold6 -b "Committed Git versions of the Documentation project (output of 'git log')" -o git.log.ps git.log.txt
# ps2pdf git.log.ps git.log.pdf

name="`basename $file .tex`"
/usr/local/bin/rubber --pdf --shell-escape "$file"
retval=$?
if [ $retval -eq 0 ]; then
	# Versioning (begin)
	# Moved the update of versions.dat here => No duplicate entries in versions.dat when compilation fails
	echo "`date`: ${version}.${subversion}.${subsubversion}" >> versions.dat
	# Commit version change
	##NOT_IN_THE_FINAL_VERSION## echo $version       > version
	##NOT_IN_THE_FINAL_VERSION## echo $subversion    > subversion
	##NOT_IN_THE_FINAL_VERSION## echo $subsubversion > subsubversion
	# Versioning (end)
    else
	echo Latex compilation failed -- aborting
	#cp old.pdf MINERVA-100MeV-ACCELERATOR-TDR.pdf
	exit 1
fi


# DISABLED pdftk FrontPagePartB-v2p345.pdf "$name.pdf" cat output "MINERVA 100MeV ACCELERATOR TDR.pdf"
# gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile="MINERVA 100MeV ACCELERATOR TDR.pdf" FrontPagePartB-v2p345.pdf "$name.pdf"
pdftk FrontPage.pdf "$name.pdf" cat output "tmp.pdf"
pdftk "tmp.pdf" attach_files  \
		git.log.txt versions.dat ../CHANGELOG \
			to_page end \
	output "$name-with-attachment.pdf"
# Regrettably, command "pdftk FrontPage.pdf MINERVA-100MeV-ACCELERATOR-TDR.pdf cat output - | pdftk - -attach_files git.log.txt versions.dat ../CHANGELOG output tmp.pdf" does not work...


# This command attaches git.log.txt; versions.dat; and the Git CHANGELOG to the pdf file
#  pdftk FrontPage.pdf "$name.pdf" attach_files  \
#  		git.log.txt versions.dat ../CHANGELOG \
#  	output "$name-with-attachment.pdf"
mv "$name-with-attachment.pdf" "$name.pdf" 
# Note: In version 1.0.0 we shall NOT attach the above annexes via pdftk
#       This because pdftk removes some useful features from the output pdf (no side ToC etc)

# The following command instructs the upload of the TDR (without frontpage material) onto Alexandria
# cp MINERVA-100MeV-ACCELERATOR-TDR.pdf ~/c/Users/vdflorio/AppData/Roaming/OpenText/OTEdit/EC_sckcen/c37035102
cp MINERVA-100MeV-ACCELERATOR-TDR.pdf ~/c/Users/vdflorio/AppData/Roaming/OpenText/OTEdit/EC_sckcen/i39215119/MINERVA-ACCELERATOR-TDR-FinalDraft.pdf
# The mentioned folder corresponds to folder MINERVA ACCELERATOR TDR at
# https://ecm.sckcen.be/OTCS/llisapi.dll?func=ll&objId=19670138&objAction=browse&viewType=1

# Also archive a local copy
cp MINERVA-100MeV-ACCELERATOR-TDR.pdf ~/d/Documents/TDR/TDR-$version.$subversion.$subsubversion.pdf

# Update "old.pdf" (see p3 script)
echo cp MINERVA-100MeV-ACCELERATOR-TDR.pdf old.pdf
cp MINERVA-100MeV-ACCELERATOR-TDR.pdf old.pdf

echo "Upload file to L2? (ctrl-C = abort, any other input = yes)"
read a
scp     "$file" "$pdf" vdflorio\@sck.be@l2.sck.be:Documents/TDR

