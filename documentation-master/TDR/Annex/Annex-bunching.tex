\subsection[Longitudinal Phase \& Rebunching]{Longitudinal Phase \& Rebunching}
\label{bkm:Ref500323629}
In the {\LinacRF} drift tube linacs, charged \TBeam{beam} particles are accelerated by electric fields
between oppositely charged drift tubes.  The charge polarity of the tubes---as well as the orientation
of the electric fields in between---has a periodic time dependence. The accelerated \TBeam{beam} must
consist of particle bunches whose transitions through the acceleration gaps have to be synchronous to
the {\LinacRF} phase $\Phi_{\text{rf}}$ in order to pass each gap when the electric field is oriented
along the direction of motion of the bunch.

\subsubsection{Longitudinal Phase}
\label{bkm:Ref500329241}
If a whole bunch can be characterised by the phase $\Phi_s$ (relative to $\Phi_{\text{rf}}$) and
the energy $W_s(z)$ of an imaginary particle called ``synchronous particle'', individual bunch-particles
with total energy $W(z)$ and phase $\Phi(z)$ can be described by the difference of energy ${\Delta}W(z)$
and difference of phase ${\Delta}\Phi(z)$ to the bunch's synchronous particle.

$W_s(z)$ as well as $\Phi_s$ are set by the overall \TBeamDynamics{beam dynamics} concept of the respective
accelerator. ${\Delta}W(z)$ and ${\Delta}\Phi(z)$ constitute the phase space of the longitudinal particle
motion.


\ifdefined\EnableTeXObjects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\TeXFigure{fig:SynchronousParticle}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\else
 \begin{figure}[H]
     \centering
     \begin{subfigure}[t]{0.5\textwidth}
         \centering
         \includegraphics[width=2.7665in,height=2.0602in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img027.png}
         \caption{${\Delta}$W(${\Delta}\Phi $)/W\textsubscript{s} for $\Phi $s= 0 and various values of C = H\textsubscript{const}}
 	\label{fig:SynchronousParticle-a}
     \end{subfigure}%
    ~
     \begin{subfigure}[t]{0.5\textwidth}
         \centering
         \includegraphics[width=2.6665in,height=2.0602in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img028.png}
         \caption{E\textsubscript{z} ($\Phi$) around $\Phi_\mathit{s}$ = 0 (blue dot: synchronous particle)}
 	\label{fig:SynchronousParticle-b}
     \end{subfigure}
 	\caption[${\Delta}$W(${\Delta}\Phi $)/W\textsubscript{s} for $\Phi $s= 0 and various values of C = H\textsubscript{const}; E\textsubscript{z} ($\Phi$) around $\Phi_\mathit{s}$ = 0.]{} 
  \label{fig:SynchronousParticle}
 \end{figure}
\fi

\textit{Note}: H is the Hamilton function of the longitudinal particle motion, here set to different
constant values.

Particles that arrive at $z$ prior to the synchronous particle ($\Phi < \Phi_\mathit{s}$, green dot in
Fig.~\ref{fig:SynchronousParticle}b) experience a lower electric field level and therefore approach
it longitudinally as the bunch moves along the \TBeam{beam} axis. However, particles that arrive after the
synchronous particle ($\Phi > \Phi_\mathit{s}$, red dot in Fig.~\ref{fig:SynchronousParticle-b}a) also
see a lower field and are unable to catch up. Hence the particle bunch as a whole is unstable and will
disintegrate during the acceleration process (see also Fig.~\ref{fig:SynchronousParticle}a).



\ifdefined\EnableTeXObjects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\TeXFigure{fig:SynchronousParticle-b-lower}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\else
 \begin{figure}[H]
     \centering
     \begin{subfigure}[t]{0.5\textwidth}
         \centering
           \includegraphics[width=2.7665in,height=2.0736in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img029.png}
         \caption{${\Delta}$W(${\Delta}\Phi $)/W\textsubscript{s} for $\Phi_\mathit{s}$ % transcode01($\Phi $\textsubscript{s})
                  = --\SI{30}{\degree} and various values of C = H\textsubscript{const}.}
         \label{fig:SynchronousParticle-a-two}
     \end{subfigure}%
    ~
     \begin{subfigure}[t]{0.5\textwidth}
         \centering
           \includegraphics[width=2.6866in,height=2.1272in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img030.png}
         \caption{Ez($\Phi $) around $\Phi_\mathit{s}$ % transcode01($\Phi $\textsubscript{s})
                  = --\SI{30}{\degree} (blue dot: synchronous particle).}
         \label{fig:SynchronousParticle-a-three}
     \end{subfigure}
 	\caption[${\Delta}$W(${\Delta}\Phi $)/W\textsubscript{s} for $\Phi_\mathit{s}$= --\SI{30}{\degree}
	 and various values of C = H\textsubscript{const}; Ez($\Phi $) around $\Phi_\mathit{s}$ = --\SI{30}{\degree}.]{}
  \label{fig:SynchronousParticle-b-lower}
 \end{figure}
\fi


For lower values of $\Phi_\mathit{s}$ ($-90$ {\textless} s {\textless} 0) the achieved acceleration
voltage decreases. Like in the case of s=0, particles with $\Phi < \Phi_\mathit{s}$, green dot
in Fig.~\ref{fig:SynchronousParticle-b-lower}b) move towards the synchronous particle because of the
lower energy gain. However, particles that arrive at z at a later time with $\Phi_\mathit{s} <
\Phi < \Phi_\mathit{s}$ +2{\textperiodcentered}{\textbar}$\Phi_\mathit{s}${\textbar} (red dot in
Fig.~\ref{fig:SynchronousParticle-b-lower}b) experience a higher field level instead and are accelerated
towards the synchronous particle as well.


This provides phase focusing because particles within the separatrix (red trajectory
in Fig.~\ref{fig:SynchronousParticle-b-lower}b) remain within a stable particle bunch while being
accelerated~\cite[p.~14]{T-Frankfurt-2007-Fischer}.  The area enclosed within the separatrix is also
referred to as ``the bucket.''


\ifdefined\EnableTeXObjects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\TeXFigure{fig:SynchronousParticle-c}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\else
 \begin{figure}[H]
     \centering
     \begin{subfigure}[t]{0.5\textwidth}
         \centering
           \includegraphics[width=2.9201in,height=2.1799in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img031.png}
         \caption{${\Delta}$W(${\Delta}\Phi $)/W\textsubscript{s} for $\Phi_\mathit{s}$ % transcode01($\Phi $\textsubscript{s})
                  = $-90$\SI{}{\degree} and various values of C = H\textsubscript{const}.}
         \label{fig:SynchronousParticle-a-four}
     \end{subfigure}%
    ~
     \begin{subfigure}[t]{0.5\textwidth}
         \centering
           \includegraphics[width=2.9201in,height=2.2071in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img032.png}
         \caption{E\textsubscript{z}($\Phi $) around $\Phi_\mathit{s}$ % transcode01($\Phi $\textsubscript{s})
                  = $-90$\SI{}{\degree} (blue dot: synchronous particle).}
         \label{fig:SynchronousParticle-a-five}
     \end{subfigure}
 	\caption[${\Delta}$W(${\Delta}\Phi $)/W\textsubscript{s} for $\Phi_\mathit{s}$ = --\SI{90}{\degree}
	 and various values of C = H\textsubscript{const}; E\textsubscript{z}($\Phi $) around
	 $\Phi_\mathit{s}$ = --\SI{90}{\degree}.]{}
  \label{fig:SynchronousParticle-c}
 \end{figure}
\fi


If $\Phi_\mathit{s}$ = --\SI{90}{\degree}, the synchronous particle does not gain any energy at all by
passing the gap and the bucket reaches its maximum size (phase focusing occurs for --\SI{180}{\degree}
{\textless} ${\Delta}\Phi $ {\textless} \SI{180}{\degree}) (Fig.~\ref{fig:SynchronousParticle-c}a).
E\textsubscript{z}($\Phi $) can be considered linear for about --\SI{120}{\degree} {\textless} ${\Delta}\Phi
$ {\textless} \SI{60}{\degree} (see Sect.~\ref{bkm:Ref500329199}, ``Rebunching{}'', below.)


\subsubsection{Rebunching}
\label{bkm:Ref500329199}
In {\LinacRF} rebuncher cavities, which basically are common accelerator structures with the synchronous phase set to
$\Phi_\mathit{s}$ =\SI{-90}{\degree} and equidistant gap centres (constant $\beta$), the concept of phase
focusing (see Sect.~\ref{bkm:Ref500329241}, ``Longitudinal Phase'')
is used to generate a phase dependent energy modulation (see Fig.~\ref{fig:SynchronousParticle-c}b)
of the longitudinal particle distribution.



Due to the fact that in the longitudinal phase space particles move on circular trajectories as
shown in Fig.~\ref{fig:SynchronousParticle-c}a, an initially defocused (relating to the phase spread)
phase space ellipse (like the green ellipse in Fig.~\ref{fig:TransformationDuringBunching}a) rotates
counterclockwise around its centre as the bunch moves through the gaps of the rebuncher structure
(Fig.~\ref{fig:TransformationDuringBunching}a, step 1).


\ifdefined\EnableTeXObjects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\TeXFigure{fig:TransformationDuringBunching}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\else
 \begin{figure}[H]
 \hspace*{-0.06\textwidth}
     \begin{subfigure}[t]{0.503\textwidth}
         \centering
           \includegraphics[height=2.94in,keepaspectratio]{MYRRHATDRPartB-img/MYRRHATDRPartB-img033.png}
     \caption[Transformation of the longitudinal phase space ellipse during bunching.]{Transformation of the
	          longitudinal phase space ellipse during
                  bunching $\Theta=\arctan(300\times \Delta W / (W_s\Delta\Phi))$.}
                  %$\Theta $=arctan(300{\textperiodcentered}${\Delta}$W/(W\textsubscript{s}${\Delta}\Phi $))
         \label{fig:TransformationDuringBunching-sub-a}
     \end{subfigure}%
    ~
     \begin{subfigure}[t]{0.503\textwidth}
         \centering
           \includegraphics[height=2.94in,keepaspectratio]{MYRRHATDRPartB-img/MYRRHATDRPartB-img034.png}
         \caption{Angular velocity $\dv{\theta}{t}(\theta)$ on different phase trajectories
                  parametrised by $\Delta\Phi_0$.}
         \label{fig:TransformationDuringBunching-sub-b}
     \end{subfigure}
	 \caption[Transformation of the long. phase space ellipse during bunching.]{}
 	 %$\Theta=\arctan(300\times \Delta W / (W_s\Delta\Phi))$; angular velocity $\dv{\theta}{t}(\theta)$
 	 %on different phase trajectories parametrised by $\Delta\Phi_0$.]{}
 \label{fig:TransformationDuringBunching}
 \end{figure}
\fi

In a subsequent drift (Fig.~\ref{fig:TransformationDuringBunching}a, step 2) the particle energies remain
constant while each particle approaches the synchronous particle longitudinally due to the previously
introduced energy kick. Thereby the phase spread of the ellipse gets reduced from the beginning of the
drift (Fig.~\ref{fig:TransformationDuringBunching}a, yellow ellipse) up to the arrival at the focus,
where the phase spread is at its minimum (Fig.~\ref{fig:TransformationDuringBunching}a, red ellipse)
after a drift length of $\ell_\mathit{focal}$.

According to Liouville's theorem, the size of the enclosed area inside the phase space ellipse, which
defines the longitudinal emittance~\cite[p.~283]{B-2008-Wangler-Wiley}, remains constant during the
entire idealised bunching process described above.


Figures~\ref{fig:PhaseSpaceDistribution}a--\ref{fig:PhaseSpaceDistribution}d show
\TSimulation{simulated}\footnote{Using \CodesBENDER; no space charge;
                   W\textsubscript{s} = 1,\SI{5}{\MeV}; $U_{\mathit{eff}}$ = \SI{58}{\kV} per gap; m\textsubscript{0} =
                   m\textsubscript{p}; q = e; %\par
                   {f}{\textsubscript{0}}{ =
                   \SI{176.1}{\mega\hertz}; g = }$\beta ${\textsubscript{s}}$\lambda
                   ${\textsubscript{0}}{/4; 61529 particles.}}
longitudinal phase space distributions during the rebunching of an analytically generated perfectly
ellipsoidal entry distribution with all particles on the \TBeam{beam} axis like on a pearl necklace in a
2-gap half-wave rebuncher cavity.

Due to the non-linear time dependence of the electric field in the range of {\textbar}$\Delta \Phi
${\textbar}{\textgreater}\SI{30}{\degree} (E\textsubscript{z}($\Delta \Phi $) is approximately
linear for {\textbar}$\Delta \Phi ${\textbar}{\textless}\SI{30}{\degree}), particles on outer
phase space trajectories rotate slower around the origin than particles on inner trajectories (see
Fig.~\ref{fig:TransformationDuringBunching}b) and an initially ellipsoidal phase space distribution
gets distorted (see Fig.~\ref{fig:PhaseSpaceDistribution}b and Fig.~\ref{fig:PhaseSpaceDistribution}c)
as the angular velocities  $\dv{\theta}{t}(\theta=C, {\Delta}\Phi_0)$ on a line with $\Theta
$=constant=$C$ differ depending on $\Delta\Phi_0$ ($\Theta$ is the angle between the connecting
line of the phase space position $P$ of a particle and the origin and the $\Delta\Phi$-axis, see
Fig.~\ref{fig:TransformationDuringBunching}a).

For small values of $\Delta\Phi$ and therefore big angles for $\Theta$ (regarding the first quadrant),
where E\textsubscript{z}($\Delta \Phi $) is almost linear, $\dv{\theta}{t}(\theta=C, {\Delta}\Phi_0)$
is approximately the same on every trajectory (at $\Theta$ = \SI{90}{\degree}, it is exactly the same).


\ifdefined\EnableTeXObjects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\TeXFigure{fig:PhaseSpaceDistribution}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\else
 \begin{figure}[H]
     \centering
     \begin{subfigure}[t]{0.5\textwidth}
         \centering
           \includegraphics[width=2.8201in,height=2.1in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img035.png}
         \caption{Defocused phase space ellipse prior to rebuncher entry.}
         \label{fig:PhaseSpaceDistributiona}
     \end{subfigure}%
    ~
     \begin{subfigure}[t]{0.5\textwidth}
         \centering
           \includegraphics[width=2.7736in,height=2.1071in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img036.png}
         \caption{Rotated phase space distribution after first gap.}
         \label{fig:PhaseSpaceDistributionb}
     \end{subfigure}
    \par
     \begin{subfigure}[t]{0.5\textwidth}
         \centering
           \includegraphics[width=2.7535in,height=2.1201in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img037.png}
         \caption{Rotated phase space distribution after second gap.}
         \label{fig:PhaseSpaceDistributionc}
     \end{subfigure}%
    ~
     \begin{subfigure}[t]{0.5\textwidth}
         \centering
           \includegraphics[width=2.7535in,height=2.0736in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img038.png}
         \caption{Focused phase space distribution after a drift of $\ell $\textsubscript{focal} = \SI{335.5}{\milli\meter}.}
         \label{fig:PhaseSpaceDistributiond}
     \end{subfigure}
 \caption[Simulated longitudinal phase space distributions during the rebunching.]{}
  \label{fig:PhaseSpaceDistribution}
 \end{figure}
\fi

As well as for reducing the phase spread (leading to an increase of the energy spread) buncher cavities
can also be used for debunching, thus reducing the energy spread (resulting in an increase of the phase
spread instead) by applying a synchronous phase of $\Phi_s=$ +\SI{90}{\degree}.
