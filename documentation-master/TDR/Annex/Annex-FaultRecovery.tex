\subsection[Tolerance to RF or Focusing Failures]{Tolerance to RF or Focusing Failures}\label{concept:mtbf}
\label{bkm:Ref497895504}
As already pointed out, the \MYRRHA{} main linac is designed to ensure enhanced \TFaultTolerance{fault tolerance} capabilities,
which is a mandatory requirement towards reaching our \TReliability{reliability} design
goal---i.e., an \TMTBF{MTBF} of 250 hours.  This is done by providing
significant {\LinacRF} power and gradient overhead throughout the 3 superconducting sections. In the present
design, this operation margin in terms of acceleration capability was fixed to \SI{30}{\percent}, leading
to a main linac \TCosts{overcost} estimated to about \SI{20}{\percent}~\cite{D-EUROTRANS-1.74}. This value was
chosen considering an average \TMTBF{MTBF} value of about 10000 hours
for {\LinacRF} systems units, which is already pretty good, leading to a global \TMTBF{MTBF}
for the whole main linac accelerating {\LinacRF} system of about 70 hours: about 30 to 35
\TFailures{failures} are therefore to be expected (and \TFaultCompensation{compensated})
simultaneously during the foreseen 3 months (2190 hours) \MYRRHA{} mission time if no on-line repair can
be performed, that corresponds to \SI{25}{\percent} of the total number of cavities.


\subsubsection{RF fault-recovery using local compensation}
The present reference scheme for \TFaultCompensation{compensating} {\LinacRF}
units \TFailures{failures} is to use a local compensation method, as described in
Section~\ref{sect:fault-recovery-procedure}.  Based on the initial studies made a few
years ago~\cite{J-PhysRevAccelBeams-2008-11.072803-Bia,P-ICFA04-Bia}, new \TSimulation{simulations} have been performed
on the up-to-date \MYRRHA{} main linac design to better assess its \TFaultTolerance{fault tolerance} capability in terms
of {\LinacRF} \TFailures{failures} and evaluate more accurately the induced {\LinacRF} requirements, in particular
on \TRFAmplifier{amplifiers} power needs and power couplers coupling factors~\cite{S-MYRRHA-2012-Bouly}.


\ifdefined\EnableTeXObjects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\TeXFigure{fig:ReTuningCompOptim18}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\else
 \begin{figure}[H]%[H]
 \centering
   \includegraphics[width=5.5673in,height=1.9in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img063.png}
 \caption[Re-tuning strategy used for compensation optimisation as used in TraceWin code.]{Re-tuning strategy used
	 for \TFaultCompensation{compensation} \TOptimisation{optimisation} as used in the \CodesTraceWin{} code.
	 Case where spoke cryomodule \#18 is off-line.}
 \label{fig:ReTuningCompOptim18}
 \end{figure}
\fi

The methodology used in the \CodesTraceWin{} code\footnote{Note that this re-tuning procedure is used in
	the \TSimulation{simulations} but will of course not be feasible in the real machine conditions within
	the 3 seconds grace time: in this case, the new voltage/phase set-point values for compensating cavities
	will have to be picked in the \PutIndex{Control System} database; these values should have been determined
	beforehand, from past \TBeam{beam} experience if the \TFaults{fault} configuration has already been met,
	or from a \TPredictability{predictive} calculation using a dedicated \TBeamDynamics{beam dynamics}
	\TSimulation{simulation code} similar to \CodesTraceWin{} if not.}
to \TOptimisation{optimise} the \TBeam{beam} re-tuning in the presence of a \TFaults{fault} is described
in Fig.~\ref{fig:ReTuningCompOptim18}: the {\LinacRF} gradient and phase of the nearest accelerating
cavities are re-tuned so as to:

\begin{itemize}[noitemsep]
  \item Recover the nominal \TBeam{beam} phase and \TBeamEnergy{energy} at the first ``un-re-tuned'' linac lattice,
  \item Rematch the beam through the 4 first following lattices, both in the transverse and
	longitudinal dimensions (keeping all the \TQuadrupoles{quadrupoles} gradients unchanged),
  \item While limiting the cavity voltage \& the {\LinacRF} power (beam loading) increase below \SI{30}{\percent}
	and \SI{40}{\percent} respectively.
\end{itemize}

Figure~\ref{fig:SpokeCryo18Compensation} illustrates the new settings obtained with such a strategy in the
particular case where Spoke \PutIndex{cryomodule} \#18 has failed. In this case, 2 subsequent cavities
are off line and 8 cavities are therefore used for compensation. The \TBeamDynamics{beam dynamics}
remains very smooth without any hard mismatch, as shown in Fig.~\ref{fig:SpokeCryo18OffLine}. The
emittance growth is negligible, and the halo growth is well contained. When compared to the nominal case
of Fig.~\ref{fig:BeamDynamics-MultiParticles}, a slight (expectable) halo increase can be nevertheless
noticed in the longitudinal plane when crossing the failed lattice at z = \SI{50}{\meter}. This should
not be an issue---a matter to be investigated in future error studies.

%(to be checked with the error studies analysis foreseen in \MAX{}\index{Projects!MAX} Task 1.4).

%  \begin{Alert}{}
%  	I changed ``(to be checked with the error studies analysis foreseen in \MAX{}\index{Projects!MAX} Task 1.4)''
%  	into
%  	``a matter to be investigated in future error studies.''
%  \end{Alert}


\ifdefined\EnableTeXObjects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\TeXFigure{fig:SpokeCryo18Compensation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\else
 \begin{figure}[H]
 \centering
   \includegraphics[width=0.9\textwidth]{MYRRHATDRPartB-img/MYRRHATDRPartB-img064.png} 
 \caption[Spoke cryomodule \#18 loss compensated by spoke section new tunings.]%
	 {\TCryoModules{Spoke cryomodule} \#18 loss
	  \TFaultCompensation{compensated} by spoke section new tunings.\newline
 	  New tunings in the linac spoke section: blue plots.}
 \label{fig:SpokeCryo18Compensation}
 \end{figure}
\fi



\ifdefined\EnableTeXObjects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\TeXFigure{fig:SpokeCryo18OffLine}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\else
 \begin{figure}[H]
 \centering
   \includegraphics[width=5.2in,keepaspectratio]{MYRRHATDRPartB-img/MYRRHATDRPartB-img065.png} 
\caption[Main linac beam dynamics with spoke cryomodule \#18 off line]%
	{\TSCLinac{Main linac} \TBeamDynamics{beam dynamics} with \TCryoModules{spoke cryomodule} \#18
 	 off line.\newline
	 Multi-particle envelopes and evolution of the RMS \& \SI{99.99}{\percent} beam \TEmittance{emittances}.}
 \label{fig:SpokeCryo18OffLine}
 \end{figure}
\fi

This re-tuning strategy has been successfully assessed in several test scenarii~\cite{S-MYRRHA-2012-Bouly},
even including one case with multiple simultaneous \TFaults{faults} (1 \TCryoModules{cryomodule} off in each
section). This in particular allowed a preliminary assessment of the \MYRRHA{} solid-state {\LinacRF}
\TRFAmplifier{amplifier} power needs to be performed (basically \SI{15}{\kW} @\SI{352.2}{\mega\hertz}, \SI{30}{\kW} \& \SI{55}{\kW} @
@\SI{704.4}{\mega\hertz}), as illustrated in Fig.~\ref{fig:RFampPowerNeeds}.


\ifdefined\EnableTeXObjects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\TeXFigure{fig:RFampPowerNeeds}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\else
 \begin{figure}[H]
 \centering
   \includegraphics[width=\textwidth]{MYRRHATDRPartB-img/MYRRHATDRPartB-img066.png}
 \caption[Preliminary assessment of the solid-state RF amplifier power needs.]{Preliminary assessment of
	 the solid-state {\LinacRF} \TRFAmplifier{amplifier} power needs.\newline
	 Left: re-tuning set-points for a few \TErrorRecovery{fault recovery} test cases;
	 Right: preliminary assessments of the \MYRRHA{} {\LinacRF} \TRFAmplifier{amplifier} power needs, including margins for {\LinacRF}
 	 control~\cite{S-MYRRHA-2012-Bouly}.}
 \label{fig:RFampPowerNeeds}
 \end{figure}
\fi

The main conclusion of these \TErrorRecovery{fault-recovery} scenario analyses is that the \TErrorRecovery{fault recovery} scheme
is a priori feasible everywhere in the \MYRRHA{} main linac to
compensate for the loss of a single cavity or of even a full cryomodule. This statement should be confirmed
%by the upcoming advanced beam dynamics studies to be performed in Task 1.4, where these compensation
by advanced \TBeamDynamics{beam dynamics} studies to be carried out in the course of \MINERVA{}, where these compensation
schemes will be also assessed through full start-to-end and errors \TSimulation{simulations}. In particular, the most
sensitive cases---the low energy ones (loss of a MEBT rebunching cavity, loss of the
first spoke \TCryoModules{cryomodule}) and the multiple \TFaults{fault} cases---will have to be thoroughly analysed.

%  \begin{Alert}{}
%  	I changed ``This statement should be confirmed by the upcoming advanced beam dynamics studies to be performed in Task 1.4''
%  	into
%  	``This statement should be confirmed by advanced beam dynamics studies to be carried out in the course of \MINERVA{}''.
%  \end{Alert}

\ifdefined\EnableWarningsAndAlertsStillPending
\begin{Alert}{}
Past research and experience have shown that a fault-recovery scheme, although feasible, may not
always result in the expected enhancement of dependability. The case of the Ariane 5 maiden flight is
particularly eloquent: ESA had a fault-recovery scheme there, which effectively ``worked'' (a hot backup
spare took over a failed primary subsystem). Still, a software issue shared by both primary and backup
resulted in the failure of the mission. Software fault tolerance, in my opinion, should be considered
in our design~\cite{B-2009-DeFV-IGI}. Or at least we have to show that we are aware of the matter.
\end{Alert}
\fi


\ifdefined\EnableTeXObjects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\TeXFigure{fig:BeamLodingOnFailedSpoke}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\else
 \begin{figure}[H]
 \centering
   \includegraphics[width=0.95\textwidth]{MYRRHATDRPartB-img/MYRRHATDRPartB-img067.png}
 \caption[Effect of beam loading on a failed spoke cavity.]{Effect of \TBeam{beam} loading on a \TFailures{failed} spoke cavity.\newline
 	 If the cavity is still superconducting, the important criterion is the induced decelerating voltage
 	 (to be lower below \SI{0.5}{\percent} of nominal voltage); otherwise, it is the dissipated power, especially for
 	 a quenched but still cold cavity.}
 \label{fig:BeamLodingOnFailedSpoke}
 \end{figure}
\fi

It is finally to be underlined that a \TErrorRecovery{fault recovery} scenario requires the 2
\TFailures{failed} cavities to be sufficiently detuned to have a negligible (decelerating) effect on the
\TBeam{beam} and induce sustainable power dissipation in the helium bath. A modelling of the {\LinacRF}
cavity~\cite{T-Hamburg-1998-Schilcher} immediately shows that such conditions are fulfilled when the
cavity is detuned by more than 100 nominal bandwidths, this statement being still valid if the cavity
is quenched. This is illustrated in Fig.~\ref{fig:BeamLodingOnFailedSpoke} which shows the effect of
\TBeam{beam} loading on a \MYRRHA{} failed Spoke cavity: in this case, the required minimal detuning
to provide is about \SI{15}{\kilo\hertz}, to be compared to the cavity nominal full bandwidth in its SC
state which is \SI{160}{\hertz}.

\subsubsection{Focusing on fault-recovery}
In the case where a \TQuadrupoles{quadrupole} \TMagnets{magnet} is failing, the simplest solution is to switch off the whole
doublet~\cite{P-ICFA04-Bia} and to \TFaultCompensation{compensate} locally using the 4 nearest
doublets. The methodology used in the \CodesTraceWin{} code to \TOptimisation{optimise} the \TBeam{beam}
re-tuning in the presence of such a \TFaults{fault} is very similar to the one used for {\LinacRF}
\TFaults{faults}. In this case, the gradients of the nearest \TQuadrupoles{quadrupoles} are re-tuned so as to simply
rematch the \TBeam{beam} transversely through the 4 first lattices following the \TFailures{failed}
doublet, as shown in Fig.~\ref{fig:ReTuningCompOptim19}. In this example where the \TQuadrupoles{quadrupole} doublet \#19
is lost, the gradients of the first re-tuned doublet are lowered by about \SI{70}{\percent} while the 3
following ones are only slightly re-tuned (the gradient increase does not exceed \SI{10}{\percent}). The
\TBeamDynamics{beam dynamics} remains very smooth, with a noticeable---but slight \& manageable---transverse
emittance growth at the \TFailures{failed} doublet transition (see Fig.~\ref{fig:SpokeCryo19OffLine}).


\ifdefined\EnableTeXObjects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\TeXFigure{fig:ReTuningCompOptim19}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\else
 \begin{figure}[H]
 \centering
   \includegraphics[width=6.0071in,height=1.6866in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img068.png}
 \caption[Re-tuning strategy used for compensation optimisation.]%
	 {Re-tuning strategy used for \TFaultCompensation{compensation} \TOptimisation{optimisation}.\newline
	 As used in \CodesTraceWin{} code. Case where \TQuadrupoles{quadrupole} doublet \#19 is off-line.}
 \label{fig:ReTuningCompOptim19}
 \end{figure}
\fi


\ifdefined\EnableTeXObjects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\TeXFigure{fig:SpokeCryo19OffLine}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\else
 \begin{figure}[H]
 \centering
   \includegraphics[width=\textwidth]{MYRRHATDRPartB-img/MYRRHATDRPartB-img069.png}
	 \caption[Main linac beam dynamics with \TQuadrupoles{quadrupole} \#19 off line.]%
	 {Main linac \TBeamDynamics{beam dynamics} with \TQuadrupoles{quadrupole} \#19 off line.\newline
	 Multi-particle envelopes and evolution of the RMS \& \SI{99.99}{\percent}
	 \TBeamEmittance{beam emittances}.}
 \label{fig:SpokeCryo19OffLine}
 \end{figure}
\fi

\subsubsection{Assessment of an alternative RF fault-recovery strategy}
As already mentioned, the operation of the local {\LinacRF} \TErrorRecovery{fault-recovery} strategy
heavily relies on the capability of \TFaultCompensation{compensating} superconducting cavities to
\TReliability{reliably} increase their voltage by up to \SI{30}{\percent} in a short time (hundreds of ms) after
being run at a derated voltage for perhaps months. This point could be an issue (risks of quench,
strong field emission, \TMultipacting{multipacting}) and therefore, an alternative back-up approach has been looked at.
This alternative strategy would use a more global \TErrorRecovery{fault-recovery} system.

\begin{itemize}[noitemsep]
  \item The \MYRRHA{} linac is longitudinally tuned in a different nominal way, using maximum
	capabilities of all the accelerating cavities (i.e. \SI{8.3}{\MVpermt}, \SI{10.7}{\MVpermt}
	and \SI{11.0}{\MVpermt} in the 3 sections respectively) when allowed by the limitations imposed
	by the \TBeamDynamics{beam dynamic} rules showed in Sect.~\ref{bkm:Ref497895959}, ``linac design''.  With such a
	tuning, the final \SI{600}{\MeV} energy is reached after \SI{203}{\metre}, i.e. without the 5 last
	high-beta \PutIndex{cryomodule}s\footnote{If also used, the final energy could reach \SI{775}{\MeV}
	in this case.}.  These last 5 high-beta cryomodules (i.e. 20 elliptical $\beta=0.65$ cavities)
	are therefore operated in a non-accelerating back-up mode (with synchronous phases alternatively
	tuned at \SI{-90}{\degree} and \SI{90}{\degree} to minimise the induced energy dispersion).

  \item In the case of a {\LinacRF} \TFailures{failure}, the phases of all downstream cavities between the failed
	cavity and the end of the linac are re-tuned to simply \TErrorRecovery{recover} their initial
	synchronous phase, and the phase of a few back-up $\beta=0.65$ cavities is tuned to recover the
	\SI{600}{\MeV} final energy at the entrance of the {\LinacHEBT}.
\end{itemize}

Preliminary test cases have been analysed using this strategy. All single cavity \TFailures{failures}
could be recovered in a convincing way. The \TErrorRecovery{recovery} of a full \TCryoModules{cryomodule}
loss is also very feasible in the elliptical sections, but at lower energy it becomes rather more difficult
(if not impossible without losses below \midtilde\SI{25}{\MeV}) due to the impact of the induced energy
mismatch on the \TBeam{beam} behaviour. It is also found from these preliminary test cases that the induced
emittance growth is significantly higher than in the local \TFaultCompensation{compensation} case,
especially in the longitudinal plane. This could imply that the \TErrorRecovery{recovery} of multiple
simultaneous cavity \TFaults{faults} might become unsustainable because of the high induced energy
mismatch. This preliminary assessment will have to be better evaluated if possible
%within Task 1.4.
in the course of future studies.

%  \begin{Alert}{}
%  	I changed ``within Task 1.4''
%  	into
%  	``in the course of future studies.''
%  \end{Alert}
