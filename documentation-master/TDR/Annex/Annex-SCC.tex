\subsection[Space charge and emittance growth]{Space charge and emittance growth}
\label{annex:space-charge}
For \TBeamIntensity{high intensity} beams at low energy, the \TSpaceCharge{space charge} is particularly strong. The electric field
created by the \TSpaceCharge{space charge} tends to defocus the beam and is strongly nonlinear as it is induced by
the non-uniform distribution of the charge particles of the \TBeam{beam}. The \TSpaceCharge{space charge} force is the
result of two effects: a repelling force, due to the Coulomb interaction of the beam charged
particles, and an attractive magnetic force, induced by the movement of charged particles. In a
first approximation, by considering a continuous cylindrical \TBeam{beam} with a homogeneous and uniform
particles distribution, the resultant radial force---of these two effects---seen by a particle
inside the \TBeam{beam} is given by Eq.~\eqref{eq:RadialForce}.

\ifdefined\EnableTeXObjects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\TeXEquation{eq:RadialForce}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\else
\begin{equation}
 \EqRadialForce,
 \label{eq:RadialForce}
 % F_r=\frac{(1-\beta^2)}{\beta}\frac{\mathit{qI}}{2\pi\varepsilon_0c}\frac
 % 	r{R^2}\ \ \ \ (\hbox{for }r<R), \label{eq:RadialForce}
\end{equation}
\fi

\noindent
where $I$ is the \TBeamCurrent{beam current}, $q$ the beam particles charge, $\varepsilon_0$ the \TVacuumSystem{vacuum}
permittivity, $r$ the radial position of the particle inside the beam, $R$ the beam radius, $c$ the
celerity of light, and $\beta $ the reduced velocity of the \TBeam{beam} particles.
  

\subsubsection{High intensity and low energy ion beams: space charge compensation.}
\TSpaceChargeCompensation{Space charge compensation} (or neutralisation) occurs when a
\TBeam{beam} is propagating through a residual gas in the beam line. Its principle is described in
Fig.~\ref{fig:SpaceChargeCompensation}. The beam induces an ionisation of the residual gas, creating
pairs of ions and electrons. In the case of a proton \TBeam{beam}, the ions are repelled towards the
\TVacuumSystem{vacuum chamber} walls, while the electrons are trapped by the positive potential of the
\TBeam{beam}, until a steady state is reached. In this way the global potential of the \TBeamEnergy{low
energy beam} is neutralised. Thus, the space charge force is compensated; one could interpret this as
an artificial decrease of the \TBeamCurrent{beam current} in Eq.~\eqref{eq:RadialForce}.

\ifdefined\EnableTeXObjects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\TeXFigure{fig:SpaceChargeCompensation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\else
\begin{figure}[H]
\centering
\includegraphics[width=6.2272in,height=1.7665in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img013.png}
\caption[Description of the space charge compensation process]%
	{Description of the \TSpaceChargeCompensation{space charge compensation} process~\cite{M-MYRTE-2016-Gerardin}.}\label{fig:SpaceChargeCompensation}
\end{figure}
\fi

The \TSpaceChargeCompensation{space charge compensation} (SCC) can be of great advantage to limit non-linear
effects in the \TBeamTransport{beam transport} through the {\LinacLEBT}, and to \TOptimisation{optimise}
the injection into the \LinacRFQ. To enable \TSpaceChargeCompensation{SCC}, electrostatic elements must
be avoided lest the neutralising particles be attracted (or repulsed) by the electric field induced by
the focusing elements~\cite{P-LINAC10-Chauvin}.

In the {\LinacLEBT}, only the chopper is deflecting the \TBeam{beam} with an electric field. This
induces transients that may cause losses in the \LinacRFQ{} or in the linac. Those transients have to be
measured experimentally. Although \TSpaceChargeCompensation{SCC} has already been observed and used on
several low energy beam lines~\cite{P-LINAC06-Holli}, it is a very complex physical phenomenon to model
that depends on many different parameters: the influence of the \TVacuumSystem{vacuum chamber walls},
the beam transverse and longitudinal distributions, the different species (ions) inside the chamber,
the residual gas interactions, the electro-magnetic fields from guiding elements, etc.

% \begin{Alert}{}
% Is there anything specific to MINERVA that I should mention here?
% \mycomment{ap}{No!}
% \end{Alert}

Most of the current \TBeamDynamics{beam dynamics} codes (such as \CodesTraceWin) are modeling the
\TSpaceChargeCompensation{SCC} based on rough approximation of the process. It is of particular interest to
experimentally study the \TSpaceChargeCompensation{SCC} to develop and improve our models and to anticipate
better the \TBeam{beam} behavior during \TCommissioning{commissioning} and operation of the accelerator.
This was one of the motivations for building the {\LinacLEBT} prototype.

\subsubsection{The LEBT prototype.}

The main objectives to build the {\LinacLEBT} prototype have been:

\begin{itemize}[noitemsep]
  \item to initiate the construction of the \MINERVA{} linac and in particular of its \TInjector{injector};
  \item to test and validate the adopted technical solutions for an accelerator which has to operate with an
	extremely high level of \TReliability{reliability};
  \item to experimentally study the \TSpaceChargeCompensation{SCC} process in order to
	\TOptimisation{optimise} the \TBeamTransport{beam transmission} to the \LinacRFQ{} and to improve
	low energy \TBeamTransport{beam transport} \TModelling{models} in view of the linac operation.
\end{itemize}

The \TBeam{beam} design has been achieved within the \MAX{} project. The engineering design and the
construction have been supported by \sckcen{} and the \MARISA{} project. The final design and the
construction have been a collaborative work between LPSC (CNRS/IN2P3) and \sckcen{} teams.

The \TCommissioning{commissioning} and \TSpaceChargeCompensation{SCC} experimental program have been carried out in the \MYRTE{}
project. Said project mainly involved LPSC, \sckcen{} and CEA, as well as the COSYLAB~\cite{P-IPAC16-Modic}
and ADEX companies for dedicated \TControlSystem{control system} developments.

Please refer to Sect.~\ref{sect:history} for more information about \MAX{},
\MARISA{}, and \MYRTE{}.

\ifdefined\EnableTeXObjects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\TeXFigure{fig:GrenobleLEBT}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\else
\begin{figure}[H]
\centering
\includegraphics[width=0.9\textwidth]{MYRRHATDRPartB-img/MYRRHATDRPartB-img014-NEW.png}
\caption{The {\LinacLEBT} at LPSC Grenoble.}\label{fig:GrenobleLEBT}
\end{figure}
\fi

The \TSource{ECR proton source} and the {\LinacLEBT} installed at LPSC, in Grenoble, are presented in
Fig.~\ref{fig:GrenobleLEBT}.



The ion source and the {\LinacLEBT} have been used to perform low-energy \TBeam{beam} physics experiments on the
\TSpaceChargeCompensation{space charge compensation} phenomenon, with the support of the \MYRTE{} project.
The assembly has been moved to Louvain-la-Neuve (Belgium) to be coupled with the \LinacRFQ{} and the
first CH cavities so as to function as a demonstration platform  for the \MINERVA{} \LinacInjector{}
for energies up to \SI{6}{\MeV}.


\subsubsection{Experimental results: main objective.}
Dedicated \TSpaceChargeCompensation{SCC} experimental test campaigns have been carried out. The goal of
the experiment has been to study the effects of the residual gas and of the \TSolenoids{solenoids}' fields on the
\TBeam{beam} properties in a steady state regime. To do so, the beam properties (\TBeamCurrent{beam current}, \TBeamEmittance{emittances}, Twiss
parameters) have been measured after the first focusing \TSolenoids{solenoid}. The \TBeamDiagnostics{beam} \TFaultDiagnosis{diagnostics}
configuration is shown in Fig.~\ref{fig:GrenobleLEBT}: a Faraday cup measures the \TBeamCurrent{beam current} right
behind the first \TSolenoids{solenoid} and two \hyperref[sect:beam-inst:allison]{Allison scanners} characterise the
emittances and the Twiss parameters in the horizontal and the vertical planes at about \SI{1.5}{\metre}
after the source extraction hole.

\subsubsection{Experimental results: analysis tool development.}
A measurement example of the \TBeam{beam} particles distribution in the horizontal phase space is presented
 in Fig.~\ref{fig:BeamParticleDistr}. Here the distribution of the different species are separated
 in  phase space.  Indeed, they are not focused the same way by the \TSolenoids{solenoid}, since their mass is
different. The measurement enables to clearly identify protons, \Hplus{2} and \Hplus{3}.  Nevertheless,
 in order to extract the relevant information on the proton \TBeam{beam}, it is necessary to supress
the background noise level, to select the ions of interest and to separate the distributions.

%\mycomment{ap}{The next 2 figures are not shown in the right order!}
%\mycomment{vdflorio}{Indeed the second picture was missing a ``here'' directive (\protect\verb"[H]"). Now it should be okay\ldots}




\ifdefined\EnableTeXObjects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\TeXFigure{fig:BeamParticleDistr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\else
\begin{figure}[H]
\centering
\includegraphics[width=0.5\textwidth]{MYRRHATDRPartB-img/MYRRHATDRPartB-img015.png}
\caption[Beam particle distributions after the first focusing solenoid (hor.)]{\TBeam{Beam} particle distributions
	after the first focusing \TSolenoids{solenoid} (hor.)\newline
	Measurement of the beam particle distributions in the horizontal phase space after the first
	focusing \TSolenoids{solenoid} (Ibeam\_total = \SI{9}{\mA}, Isolenoid = \SI{93}{\ampere}).}\label{fig:BeamParticleDistr}
\end{figure}
\fi

For this purpose a dedicated analysis code, \CodesTWISSGO, has been developed in MATLAB. The principles
of this analysis tool are described in Fig.~\ref{fig:TWISSGOprinciples}.


\ifdefined\EnableTeXObjects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\TeXFigure{fig:TWISSGOprinciples}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\else
\begin{figure}[H]
\centering
\includegraphics[width=6.2992in,height=3.8528in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img016.png}
\caption{Principle of the developed emittance analysis tool \CodesTWISSGO.}\label{fig:TWISSGOprinciples}
\end{figure}
\fi


The first function of the tool enables to filter and smooth the background noise. It is also possible to
``manually'' remove the noise in some selected areas of the phase space. Then \CodesTWISSGO{} enables
to separate and analyse the protons and the \Hplus2{} distributions. By choosing some signal levels the
user can do a pre-separations of the distributions. Since the emittances distributions are crossing in
phase space, it is necessary to determine the amount of \Hplus2{} that are ``wrongly'' preselected in the
protons distributions. This is achieved by interpolating the missing particles (hole) in the \Hplus2{}
preselected distribution. According to this interpolation results, the protons distribution can be
corrected. Some final noise cleaning is then applied to the distributions and finally the statistical
emittance and the Twiss parameters are calculated.
