/*
 * endi.c : enable / disable certain sections of the TDR
 *
 * by vdflorio and jbelmans. Version v0.4 (Fr Mär 13 10:07:47 CET 2020)
 *
 * {{{ Versions history
 * History: v0.1: case sensitive enable and disable
 *          v0.2 (Do Mär 12 11:00:11 CET 2020):
 *          	 introduced aliases '+' and '-', for enable and disable (thank you for your suggestion, Jorik!)
 *               Introduced case insensitive comparison through a function found on stackoverflow (see below for URL)
 *          v0.3 (Do Mär 12 13:50:51 CET 2020):
 *               Now "enable" and "disable" have been replaced by "+" and "-".
 *               removed bug that prevented to use "-" and "+" in the search patterns
 *          v0.4 (Fr Mär 13 10:07:47 CET 2020):
 *               Structuring and clean-up
 *          v0.5 (Fr Apr  3 08:17:53 CEST 2020):
 *               Removed bug (thank you, Marco, for reporting about it!)
 * }}}
 *
 * Compile via `gcc -o endi endi.c` or `make andy`
 *
 */

/* {{{ Include files */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
/* }}} */

/* {{{ Macros */
/* current version */
#define ANDY	"0.5 (Fr Apr  3 08:17:53 CEST 2020)"
/* default input file. Can be changed via -i */
#define TDR_FILE "MINERVA-100MeV-ACCELERATOR-TDR.tex"
/* default output file. Can be changed via -o */
#define STRUCTURE_FILE "include.tex"

/* maximum number of \input statements in the input file */
#define MAX_CHAPTERS  100
#define MAX_LINE_SIZE 512
/* maximum number of malformed command lines */
#define MAX_MALFORMED  10

#define YES 1
#define  NO 0
/* }}} */

/* {{{ Classes */
/* class describing one chapter in the input */
typedef struct {
    char *chapter_name; int line_no;
} chapter_t;

/* class describing all chapters in the input */
typedef struct {
    chapter_t chapter_names[MAX_CHAPTERS]; int chapter_no;
    unsigned char enabled[MAX_CHAPTERS];
    int a, b, c;
} chapters_t;
/* }}} */

/* {{{ Function prototypes */
chapters_t* openChapters(char *fname);
void closeChapters(chapters_t* tdr);
char *parseInput(char *line);
char *stristr(const char *haystack, const char *needle);
void help();

/* }}} */

/* {{{ Global variables */
char line[MAX_LINE_SIZE];     /* current line. Caution: shared variable used in main and in subroutines */

/* flags */
unsigned char flags = 0;
#define VERBOSE 0x01

unsigned char enable;
/* }}} */

/* {{{ Main */
int main(int argc, char *argv[]) {
    /* {{{ Definition of local variables */
    int i, j;
    char *tdr_file = TDR_FILE;
    char *chapters_file = STRUCTURE_FILE; /* By default, chapters_file is */
    FILE *of;
    chapters_t* tdr;
    /* }}} */

    /* {{{ Command line parsing */
    for (i=1; i<argc; i++)
        if (argv[i][0] == '-')
            switch (argv[i][1]) {
                case 'i': tdr_file = argv[++i];
                          break;
                case 'h':
                case '?':
			  help();
                          exit(0);
		case 'v': flags |= VERBOSE;
			  break;
                case 'o': chapters_file = argv[++i];
                          break;
                default:
			  fprintf(stderr, "%s: Unknown option. Aborting...\n", argv[0]);
			  help();
			  exit(-1);
            }
    /* }}} */

    /* {{{ Phase 1: parse the TDR */
    tdr = openChapters(tdr_file);
    if (tdr == NULL) {
	fprintf(stderr, "openChapters reports an error -- aborting\n");
	exit(-2);
    }

    if (flags & VERBOSE) {
        for (i=0; i < tdr->chapter_no; i++)
	    fprintf(stderr, "Chapter %d: %s is at line %d. Currently, the chapter is %sabled\n",
			    i, tdr->chapter_names[i].chapter_name, tdr->chapter_names[i].line_no,
			    tdr->enabled[i]? "en" : "dis");
        fflush(stderr);
    }
    /* }}} */

    /* {{{ Phase 2: allow individual enabling / disabling of chapters */
    if (tdr->chapter_no < 1) {
	fprintf(stderr, "No chapters have been found in the input file. Nothing to do...\n");
	goto closings;
	return 0;
    }
    fprintf(stderr, "%d chapters have been pre-processed correctly.\n", tdr->chapter_no);
    fprintf(stderr, "Now we shall process the enable / disable statements.\n");
    fprintf(stderr, "Note: + STRING => enables  the chapter whose name includes STRING\n");
    fprintf(stderr, "      - STRING => disables the chapter whose name includes STRING\n");
    fprintf(stderr, "      EOF (ctrl-d) => closes input\n");
    fprintf(stderr, "      By default, all chapters are enabled.\n");
    fflush(stderr);

    /* {{{ Main loop */
    j = 0;
    while ( fgets(line, MAX_LINE_SIZE, stdin) ) {
	char *p;
	char *q;
	int malformed = 0;

	j++;

	/* ltrim line */
	p = line;
	while ( *p != '\0' && *p == ' ' ) p++;

	/* rtrim newline from line */
	q = strchr(line, '\n');
	if (q != NULL) *q = '\0';

        if (flags & VERBOSE)
	    fprintf(stderr, "Processing line \"%s\"\n", line);

        enable = YES;
        if ( *p == '+' )  {
            if (flags & VERBOSE)
	        fprintf(stderr, "line does contain 'enable'\n");
	    enable = YES;
	    p++;
	    while ( *p != '\0' && *p != ' ' ) p++;
	} else
        if ( *p == '-' ) {
            if (flags & VERBOSE)
	        fprintf(stderr, "line does contain 'disable'\n");
	    enable = NO;
	    p++;
	    while ( *p != '\0' && *p != ' ' ) p++;
	}

        if (flags & VERBOSE)
	    fprintf(stderr, "p now is equal to \"%s\"\n", p);

	if (p == NULL) continue;


	/* p is not NULL here */

	while ( *p != '\0' && *p == ' ' ) p++;

        if (flags & VERBOSE)
	    fprintf(stderr, "Current command is: \"%sable %s\"\n", enable? "en" : "dis", p);

	if (*p == '\0') {
	    fprintf(stderr, "Malformed command line: %s. Ignored.\n", line);
	    malformed++;
	    continue;
	}
	if (malformed > MAX_MALFORMED) {
            fprintf(stderr, "More than %d malformed line have been encountered -- aborting.\n", MAX_MALFORMED);
	    closeChapters(tdr);
	    exit(-3);
	}

	/* {{{ iterate through all chapter names looking for those who mention p */
        for (i=0; i < tdr->chapter_no; i++) {
            if (flags & VERBOSE)
	        fprintf(stderr, "Is string \"%s\" contained in chapter name \"%s\"?\n",
	            p, tdr->chapter_names[i].chapter_name);
	    if (strstr(tdr->chapter_names[i].chapter_name, p)) {
                if (flags & VERBOSE)
		    fprintf(stderr, "YES, %s is in %s\n", p, tdr->chapter_names[i].chapter_name);
	        fflush(stderr);
	        tdr->enabled[i] = enable;
                if (flags & VERBOSE)
		    fprintf(stderr, "String \"%s\" is contained in chapter name \"%s\": chapter has been %sabled\n",
		        p, tdr->chapter_names[i].chapter_name, enable? "en" : "dis");
                fprintf(stderr, "Chapter %sabled \"%s\"\n",enable? "en" : "dis",tdr->chapter_names[i].chapter_name);
            } 
	}
	/* }}} */
    }
    /* }}} */

    if (flags & VERBOSE)
        fprintf(stderr, "%d enable/disable actions have been carried out.\n", j);
    /* }}} */

    /* {{{ Phase 3: output the TDR structure */
    fprintf(stderr, "Creating TDR structure file.\n");
    if (chapters_file != NULL) {
	of = fopen(chapters_file, "w");
	if (of == NULL) of = stdout;
    } 

    /* iterate through all part A chapter names */
    j = 0;
    if (tdr->a > 0) {
        fprintf(of, "\\part{Introduction}\\label{chp:intro}\n");

        for (i=0; i < tdr->a; i++)
	    if (tdr->enabled[i] == NO)
	        fprintf(of, "%% \\input{%s} -- CHAPTER DISABLED.\n", tdr->chapter_names[i].chapter_name);
	    else
	        fprintf(of, "\\input{%s} %% -- chapter enabled.\n",  tdr->chapter_names[i].chapter_name);
	    j = tdr->a;
    }

    /* iterate through all part B chapter names */
    if (tdr->b > 0) {
        fprintf(of, "\\part{Main components}\\label{chp:main}\n");
        for (i=j; i < j + tdr->b; i++)
	    if (tdr->enabled[i] == NO)
	        fprintf(of, "%% \\input{%s} -- CHAPTER DISABLED.\n", tdr->chapter_names[i].chapter_name);
	    else
	        fprintf(of, "\\input{%s} %% -- chapter enabled.\n",  tdr->chapter_names[i].chapter_name);
	j = j + tdr->b;
    }

    /* iterate through all part C chapter names */
    if (tdr->c > 0) {
        fprintf(of, "\\part{Additional Information}\\label{chp:additional}\n");
        for (i=j; i < j + tdr->c; i++)
	    if (tdr->enabled[i] == NO)
	        fprintf(of, "%% \\input{%s} -- CHAPTER DISABLED.\n", tdr->chapter_names[i].chapter_name);
	    else
	        fprintf(of, "\\input{%s} %% -- chapter enabled.\n",  tdr->chapter_names[i].chapter_name);
    }

    fflush(of);
    /* }}} */

    /* {{{ Closings and clean-up */
closings:
    if (of != stdout) fclose(of);
    closeChapters(tdr);
    return 0;
    /* }}} */
}
/* }}} */

/* {{{ Subroutines */
/* {{{ char *parseInput(char *line) */
/* looks for pattern \input{VALUE} and returns VALUE (if found) or NULL (if no such pattern could be found) */
/* Note: line is changed by the subroutine and the pointer returned is memory from the input buffer */
char *parseInput(char *line) {
    int l=strlen(line);
    char *p;

    if ( (p = strstr(line, "\\input{")) ) {
        char *q = p;
	while (*q != '\0' && *q != '}') q++;
	if (*q == '}') {
	    *q = '\0';
	    return p + 7; /* returns VALUE */
	}
    }

    return NULL;
}
/* }}} */

/* {{{ chapters_t* openChapters(char *fname) */
chapters_t* openChapters(char *fname) {
    FILE *f;
    int line_no;        /* current line number read from fname */
    chapters_t* tdr;    /* chapters_t object to be returned */
    char *chapter_name; /* chapter name found in current line (NULL: no chapter) */

    f = fopen(fname, "r");
    if (f==NULL) return NULL;
    tdr = malloc(sizeof(chapters_t));
    if (tdr==NULL) {
	    fclose(f); return NULL;
    }
    tdr->a = tdr->b = tdr->c = 0;

    for (tdr->chapter_no=line_no=0; fgets(line, MAX_LINE_SIZE, f); line_no++) {
        if ( (chapter_name = parseInput(line)) ) {
	    tdr->chapter_names[ tdr->chapter_no ].chapter_name = strdup(chapter_name);
	    tdr->enabled[ tdr->chapter_no ] = YES; /* by default, all chapters are enabled */
	    tdr->chapter_names[ tdr->chapter_no ].line_no  = line_no;
	    if (strstr(chapter_name, "TDR-A")) tdr->a++;
	    if (strstr(chapter_name, "TDR-B")) tdr->b++;
	    if (strstr(chapter_name, "TDR-C")) tdr->c++;
	    tdr->chapter_no++;
	}
    }

    fclose(f); return tdr;
}
/* }}} */

/* {{{ void closeChapters(chapters_t* tdr) */
void closeChapters(chapters_t* tdr) {
    int i;
    /* iterate through all chapter names, deallocating all memory */
    for (i=0; i < tdr->chapter_no; i++)
        free (tdr->chapter_names[i].chapter_name);
    free (tdr);
}
/* }}} */

/* {{{ char *stristr4(const char *haystack, const char *needle)
 * from https://stackoverflow.com/questions/27303062/strstr-function-like-that-ignores-upper-or-lower-case
 */
char *stristr(const char *haystack, const char *needle) {
    int c = tolower((unsigned char)*needle);
    if (c == '\0')
        return (char *)haystack;
    for (; *haystack; haystack++) {
        if (tolower((unsigned char)*haystack) == c) {
            for (size_t i = 0;;) {
                if (needle[++i] == '\0')
                    return (char *)haystack;
                if (tolower((unsigned char)haystack[i]) != tolower((unsigned char)needle[i]))
                    break;
            }
        }
    }
return NULL;
}
/* }}} */

/* {{{ void help() */
void help() {
    fprintf(stderr, "This is Andy v.%s, a tool to enable/disable sections of the TDR.\n", ANDY);
    fprintf(stderr, "Valid options are: [ -i input-filename ] [ -h|-? ] [ -v ] [ -o chapters-file ]\n");
    fprintf(stderr, "-h and -? mean help; -v means verbose.\n");
    fprintf(stderr, "input-file    is the TDR main module (default: %s).\n", TDR_FILE);
    fprintf(stderr, "chapters-file is the file the TDR includes (default: %s), which in turn includes all enabled chapters.\n",
                     STRUCTURE_FILE);
}
/* }}} */

/* }}} */
