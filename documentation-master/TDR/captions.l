%{
// {{{ C Prologue
/******************************************************************************************************
 * captions.l
 *
 * A tool to create a list of figures plus the paths to the images that are included in those figures
 *
 * v. 0.7 (20201102T0850) (restructuring)
 * v. 0.6 (20201015T0849) (with hyperlink to image in the Git repository)
 * v. 0.5 (20201014T1651) (with page numbers)
 * v. 0.4 (20201014T0900) (LaTeX mode)
 * v. 0.3 (20201013T1602) (Adding error management, improving output)
 * v. 0.2 (20201013T1335) (Consolidated version, tested and checked multiple times)
 * v. 0.1 (20201013T1121) (Preliminary version, yet to be thoroughly tested)
 * by vdflorio (vincenzo.deflorio@sckcen.be)
 *
 * In order to compile this code, do as follows:
 *  flex captions.l ; gcc -o captions lex.yy.c -lfl
 *
 * In order to run this code, do as follows:
 *
 * cat $LATEXFILES | captions
 *
 * where LATEXFILES is the list of LaTeX files you want to create a list of figures from.
 *
 ******************************************************************************************************/

const char version[] = "v. 0.7 (20201102T0850)";

#include "vdfio.h"

#include "pages.h"	// created by figurepages.

#define MAXNARG 2048			// max size of a caption

bool  in_includegraphics = false;	// are we inside a \includegraphics?
bool  in_caption = false;		// are we inside a \caption?
bool  in_figure = false;		// are we inside a {figure}?
bool  latex_mode = false;		// do we output LaTeX code or not?
int  figure_nr = 0;			// the number of the current figure
int  subfig = 0;			// number of subfigures present
char Arg[MAXNARG];			// argument of \caption or \includegraphics
int  nArg = 0;				// strlen Arg
void getArg(), cleanArg();		// functions to get next Arg and to clear it

FILE *out;				// output stream
float width = 0.2;			// thumbnail width. Default = 0.2. Range: ]0, 1]

char url[] = "file:///home/vdflorio/d/tmp/hq.pdf#page=";
// }}}
%}

any		(.|\n)
anybutsquare	([^\]]|\n)

%%

%.*$			;

\\begin\{subfigure\}	{ subfig++; 
			}

\\begin\{(sideways)?figure\}	{ figure_nr++; in_figure = true;
			  if (latex_mode) fprintf(out, "\n\\begin{figure}[H]\n");
			}

\\end\{(sideways)?figure\}		{
			  subfig = 0;
			  in_includegraphics = false;
			  in_caption = false;
			  in_figure = false;
			  if (latex_mode) fprintf(out, "\n\\end{figure}\n");
			}

\\includegraphics\[{anybutsquare}*\]\{	{
			if (in_figure) in_includegraphics = true;
			}

\\includegraphics\{	{
			if (in_figure) in_includegraphics = true;
			}

\\caption\[{anybutsquare}*\]%\n(\ |\t)+\{	{
			if (in_figure) in_caption = true;
			}

\\caption\[{anybutsquare}*\]\{	{
			if (in_figure) in_caption = true;
			}

\\caption\{		{
			if (in_figure) in_caption = true;
			}

{any}	{
		if (in_caption) {
			getArg();
			if (Arg[nArg-1] == '}') Arg[nArg-1] = '\0';

			if (nArg > 1) {
				if (latex_mode) fputc('\n', out);

				if (subfig)
					fprintf(out, "Fig. %d.%d: Caption: %s\n", figure_nr, subfig, Arg);
				else {
					fprintf(out, "Fig. %d: Caption: %s\n", figure_nr, Arg);
					fprintf(out, "(Viewer: \\href{run:sigpage %d}{page %d}). ", fig2page[figure_nr], fig2page[figure_nr]);
					fprintf(out, "(Browser: page %d: \\url{", fig2page[figure_nr]);
					fprintf(out, "http://tdr.myrrha.lan/vfolders/");
					//fprintf(out, "file://MINERVA-100MeV-ACCELERATOR-TDR.pdf#page=%d})\n", fig2page[figure_nr]);
					// fprintf(out, "(On page %d: \\url{file://", fig2page[figure_nr]);
					// // fprintf(out, "/home/vdflorio/Documents/Git/documentation/TDR/");
					fprintf(out, "MINERVA-100MeV-ACCELERATOR-TDR.pdf#page=%d})\n", fig2page[figure_nr]);
				}
				cleanArg();
			}
			in_caption = false;
		} else
		if (in_includegraphics) {
			getArg();
			if (Arg[nArg-1] == '}') Arg[nArg-1] = '\0';

			if (latex_mode) {
				//fprintf(out, "\n\\includegraphics[width=%f\\textwidth]{%s}\n", width, Arg);
				fprintf(out, "\n\\href{https://git.myrrha.lan/vdflorio/documentation/-/tree/master/TDR/%s}{\\includegraphics[width=%f\\textwidth]{%s}}\n", Arg, width, Arg);
				if (subfig)
					fprintf(out, "Fig. %d.%d: Path: \\verb\"%s\"\n", figure_nr, subfig, Arg);
				else
					fprintf(out, "Fig. %d: Path: \\verb\"%s\"\n", figure_nr, Arg);
			} else {
				if (subfig)
					fprintf(out, "Fig. %d.%d: Path: %s\n", figure_nr, subfig, Arg);
				else
					fprintf(out, "Fig. %d: Path: %s\n", figure_nr, Arg);
			}
			cleanArg();
			in_includegraphics = false;
		}
		else {
			// just ignore the character
		}
	}

%%

// {{{ Ancillary functions

int latex_begin(FILE *f) {
	return fcat(f, "standard_tex_prologue.tex");
}

int latex_end(FILE *f) {
	return fcat(f, "standard_tex_closings.tex");
}

// {{{ getArg and cleanArg
void getArg() {
	char c, d;
	char i;
	int curly=1;
	Arg[0] = *yytext;
	nArg = 1;
	if (*yytext == '}') curly--;
	if (*yytext == '{') curly++;

	while (curly) {
		for (; (c=input()) != '}'; nArg++) {
			if (c == '{') curly++;
			if (c==EOF || nArg == MAXNARG-1) { Arg[nArg] = '\0'; return; }
			if (c == '%') { while ((d=input()) != '\n') ; c = '\n'; }
			Arg[nArg] = c;
		}
		curly--;
		Arg[nArg++] = '}';
	}
	Arg[nArg] = '\0';
}
void cleanArg() {
	nArg = 0;
}
// }}}

// {{{ args()

void args() {
	fprintf(stderr, "Legal arguments are:\n");
	fprintf(stderr, "%s -?         %s", KYEL, KNRM); fprintf(stderr, "to print this message.\n");
	fprintf(stderr, "%s -o out     %s", KYEL, KNRM); fprintf(stderr, "to select \"out\" as output file.\n");
	fprintf(stderr, "%s -w num     %s", KYEL, KNRM); fprintf(stderr, "to select \"num\" as thumbnail width (num in ]0,1]).\n");
	fprintf(stderr, "%s -l         %s", KYEL, KNRM); fprintf(stderr, "to choose LaTeX as the output format.\n");
	fprintf(stderr, "%s -v         %s", KYEL, KNRM); fprintf(stderr, "to select verbose mode.\n");
}
// }}}

// }}}

// {{{ Main
int main(int argc, char *argv[]) {
	int i;
	FILE *g = stdout; char *gName = NULL;
	bool verbose = false;
	int latex_begin(FILE *);
	int latex_end(FILE *);

	out = stdout;

	for (i=1; i<argc; i++) if (argv[i][0] == '-') switch (argv[i][1]) {
	case '?':
		args(); return (ERR_NONE);
	case 'w':
		if (i < argc - 1) {
			float f;
			i++;
			sscanf(argv[i], "%f", &f);
			if (f<=0 || f>1) {
				fprintf(stderr, "%s: Illegal width. Legal values: ]0, 1]. Aborting...\n", argv[0]);
				abnormal (ERR_RANGE);
				exit (ERR_RANGE);
			}
			width = f;
		} else {
			abnormal (ERR_MISSING_ARG); args(); exit (ERR_MISSING_ARG);
		}
		break;
	case 'o':
		if (i < argc - 1) {
			i++;
			gName = argv[i];
			g = fopen(gName, "w");
			if (g == NULL) {
				fprintf(stderr, "%s: cannot access output file %s. Aborting...\n", argv[0], gName);
				abnormal (ERR_CANT_ACCESS);
				exit (ERR_CANT_ACCESS);
			}
			out = g;
		}
		else {
			abnormal (ERR_MISSING_ARG); args(); exit (ERR_MISSING_ARG);
		}
		break;
	case 'l':
		latex_mode = true;
		break;
	case 'v':
		verbose = true;
		break;
	default:
		abnormal (ERR_WRONG_ARG); args(); exit (ERR_WRONG_ARG);
	}
	else {
		abnormal (ERR_WRONG_ARG); args(); exit (ERR_WRONG_ARG);
	}

	if (verbose) {
		fprintf(stderr, "Starting processing of standard input stream.\n");
	}
	if (latex_mode) {
		latex_begin(out);
		fprintf(out, "\\begin{center}\\large List of figures, with thumbnails, captions, and paths to the constituent images\\\\\n");
		fprintf(out, "Created by %s %s on ", argv[0], version);
		scat(out, "date");
		fprintf(out, "\\end{center}\n\n");
	}

	while (yylex()) ;

	if (latex_mode)
		latex_end(out);

	if (verbose) {
		fprintf(stderr, "Finished processing of standard input stream.\n");
		if (g == NULL) {
			fprintf(stderr, "Output has been sent onto the standard output stream.\n");
		} else {
			fprintf(stderr, "Output is in file \"%s%s%s\".\n", KGRN, gName, KNRM);
			fclose(g);
		}
	}

	verror (ERR_NONE);
	return (ERR_NONE);
}
// }}}
/* EOF captions.l */
