#!/usr/bin/env python
# TeXObjects.py is a very simple client of TeXObjectsDB, used to process (sensu TeXObjectsDB) its command line arguments
# by vdflorio, v.0.1

import sys
from TeXObjectsDB import process

argc = len(sys.argv)
if argc > 1:
    for arg in sys.argv[1:] :
        s = process(arg)
        if s != "":
            print(s)
