INCLUDE=../include
LIB=../lib
GCC=gcc -I$(INCLUDE) -L$(LIB)
GCCOPTS=-lfl -lvdfio

TDRA=TDR-A-Chapters/TDR-A01-GeneralIntro.tex TDR-A-Chapters/TDR-A02-MyrrhaOverview.tex TDR-A-Chapters/TDR-A03-ProjectHistory.tex TDR-A-Chapters/TDR-A04-AcceleratorOverview.tex TDR-A-Chapters/TDR-A05-Phases.tex TDR-A-Chapters/TDR-A06-100MeV-Overview.tex TDR-A-Chapters/TDR-A07-Scope.tex

TDRB=TDR-B-Chapters/TDR-B01-Injector.tex TDR-B-Chapters/TDR-B02-MEBT3.tex TDR-B-Chapters/TDR-B03-MainLinac.tex TDR-B-Chapters/TDR-B04-HEBT.tex TDR-B-Chapters/TDR-B05-ReaWinSpaTar.tex TDR-B-Chapters/TDR-B06-ProTarBeamLines.tex TDR-B-Chapters/TDR-B07-CHcavities.tex TDR-B-Chapters/TDR-B08-SingleSpoke.tex TDR-B-Chapters/TDR-B09-DoubleSpoke.tex TDR-B-Chapters/TDR-B10-Elliptical.tex TDR-B-Chapters/TDR-B11-RF.tex TDR-B-Chapters/TDR-B12-Cryogenic.tex TDR-B-Chapters/TDR-B13-BeamInstr.tex TDR-B-Chapters/TDR-B13-I-BLM.tex TDR-B-Chapters/TDR-B14-MagnetsSteerers.tex TDR-B-Chapters/TDR-B15-VacuumSystem.tex TDR-B-Chapters/TDR-B16-WaterCooling.tex TDR-B-Chapters/TDR-B17-ReactMonitoring.tex TDR-B-Chapters/TDR-B18-ControlSystem.tex TDR-B-Chapters/TDR-B19-SafetyReliability.tex TDR-B-Chapters/TDR-B20-Building.tex TDR-B-Chapters/TDR-B21-CommissDecommiss.tex 

TDRC=TDR-C-Chapters/TDR-C01-Reliability.tex TDR-C-Chapters/TDR-C99-AcronAbbrv.tex

BIB=../bibliography/tdr.bib

all:	captions figurepages sectionpages gettables removeindex uncomment sift s-pages nohref transnc

oldall:	andy MINERVA-100MeV-ACCELERATOR-TDR.pdf captions figurepages sectionpages

andy:	andy.c
	gcc -g -o andy andy.c

MINERVA-100MeV-ACCELERATOR-TDR.pdf:	MINERVA-100MeV-ACCELERATOR-TDR.tex $(TDRA) $(TDRB) $(TDRC) $(BIB)
					rubber --pdf --shell-escape MINERVA-100MeV-ACCELERATOR-TDR.tex

captions:	captions.l $(LIB)/libvdfio.a $(INCLUDE)/vdfio.h
	flex captions.l       &&   $(GCC) -o captions lex.yy.c $(GCCOPTS)

figurepages:	figurepages.l $(LIB)/libvdfio.a $(INCLUDE)/vdfio.h
	flex figurepages.l       &&   $(GCC) -o figurepages lex.yy.c $(GCCOPTS)

sectionpages:	sectionpages.l $(LIB)/libvdfio.a $(INCLUDE)/vdfio.h
	flex sectionpages.l       &&   $(GCC) -o sectionpages lex.yy.c $(GCCOPTS)

gettables:	gettables.l $(LIB)/libvdfio.a $(INCLUDE)/vdfio.h
	flex gettables.l       &&   $(GCC) -o gettables lex.yy.c $(GCCOPTS)

removeindex:	removeindex.l $(LIB)/libvdfio.a $(INCLUDE)/vdfio.h
	flex removeindex.l       &&   $(GCC) -o removeindex lex.yy.c $(GCCOPTS)

uncomment:	uncomment.l $(LIB)/libvdfio.a $(INCLUDE)/vdfio.h
	flex uncomment.l       &&   $(GCC) -o uncomment lex.yy.c $(GCCOPTS)
	cp uncomment ~/bin

sift:	sift.l $(LIB)/libvdfio.a $(INCLUDE)/vdfio.h
	flex sift.l       &&   $(GCC) -o sift lex.yy.c $(GCCOPTS)
	cp sift ~/bin

$(LIB)/libvdfio.a:	$(LIB)/vdfio.o
		ar r $(LIB)/libvdfio.a $(LIB)/vdfio.o
		ranlib $(LIB)/libvdfio.a

$(LIB)/vdfio.o:	$(LIB)/vdfio.c $(INCLUDE)/vdfio.h
		gcc -c -o $(LIB)/vdfio.o -I$(INCLUDE) $(LIB)/vdfio.c

s-pages:	s-pages.c s-pages.h sectionpages
		./sectionpages < MINERVA-100MeV-ACCELERATOR-TDR.toc > s-pages.h
		gcc -I$(INCLUDE) -L$(LIB) -o s-pages  s-pages.c $(GCCOPTS)


nohref:	nohref.l 
	flex nohref.l       &&   $(GCC) -o nohref lex.yy.c $(GCCOPTS)

transnc:	transnc.l 
	flex transnc.l       &&   $(GCC) -o transnc lex.yy.c $(GCCOPTS)
