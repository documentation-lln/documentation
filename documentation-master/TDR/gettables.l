%{
/**********************************************************************************************
 * gettables extracts all tables found in the standard input
 *
 * Versions:
 * 0.1 (20201105): raw version -- tables are not printed in order.
 * 0.2 (20201105): tables now in order
 *
 * Dependencies:
 * vdfio.h and libvdfio.a
 **********************************************************************************************/
#include "vdfio.h"
bool inTable = false;
bool verbose = false;

static char version[] = "0.2 (20201105)";

%}

BTABLE	\\begin\{table\}
ETABLE	\\end\{table\}

BEGINDOC	\\begin\{document\}
ENDDOC	\\end\{document\}

%x parseContents

%%

(.|\n)		ECHO;

{BEGINDOC}	{ ECHO; putchar('\n');
		  printf("\\begin{center}\\large Tables of the MINERVA TDR\\\\\n");
		  printf("Created by gettables v. %s on ", version);
		  scat(stdout, "date");
		  printf("\\end{center}\n\n");
		  BEGIN(parseContents); }

\\\\		ECHO;

<parseContents>\\\\		if (inTable) printf("\\\\\n");

<parseContents>{ENDDOC}		{ //printf("\\bibliographystyle{acm}\n\\bibliography{../bibliography/tdr}\n\\end{document}\n");
				  ECHO; BEGIN(INITIAL); }

<parseContents>[^\\][%]$	if (inTable && *yytext != '\n') putchar(*yytext);

<parseContents>[^\\][%].*$	{ if (inTable && *yytext != '\n') putchar(*yytext);
				  if (yytext[yyleng-1] == '%') putchar(' ');
				}

<parseContents>{BTABLE}(.*)$	{ printf("\\begin{table}[h]\n"); inTable = true; }

<parseContents>{ETABLE}		{ ECHO; printf("\n\\FloatBarrier\n"); inTable = false; }

<parseContents>\n		if (inTable) putchar(' ');

<parseContents>.		if (inTable) ECHO;

%%

int main() {
	while ( yylex() ) ;
	printf("\\bibliographystyle{acm}\n\\bibliography{../bibliography/tdr}\n\\end{document}\n");
}
