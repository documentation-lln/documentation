/********************************************************************************************************
 * s-pages.c
 *
 * Creates a script to partitions a PDF into chapters.
 * Location of chapters is detailed in data structures defined in external module "s-pages.h"
 * Expects the PDF filename as its only command line argument
 * Output is shell script "s-pages.sh", which needs to be executed in order to partition the PDF
 *
 * Important: s-pages.c depends on s-pages.h, and must be recompiled each time the latter module changes
 *
 * v 0.1, by vdflorio
 *
 ********************************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "vdfio.h"
#include "s-pages.h"

char *cleaname(const char *, char, char);

int main(int argc, char *argv[]) {
	FILE *g;
	char *pdf;
	int   i;
	if (argc != 2) {
		fprintf(stderr, "%s: Missing argument (pdf file to be decomposed into chapters)\n", *argv);
		exit -1;
	}
	pdf = argv[1];
	g = fopen("s-pages.sh", "w");
	if (g == NULL) {
		fprintf(stderr, "%s: Could not open output executable s-pages.sh. Using stdout instead\n", *argv);
		g = stdout;
	}
	fprintf(g, "#!/usr/bin/env bash\n\nmkdir chapters 2> /dev/null\n\n");
	for (i=1; i<=Sections; i++) {
		char *title = cleaname(LaTeXsections[i].title, ':', ' ');
		fprintf(g, "pdftk \"%s\" cat %d-%d output chapters/TDR-Chapter%02d-\"%s\".pdf\n", pdf, LaTeXsections[i].page, LaTeXsections[i+1].page, i, title);
		free(title);
	}
	if (Appendix > 0) {
		fprintf(g, "pdftk \"%s\" cat %d-%d output chapters/TDR-Appendices.pdf\n", pdf, Appendix, Bib-1);
	}
	if (Bib > 0) {
		fprintf(g, "pages=`pdftk \"%s\" dump_data | grep NumberOfPages| awk '{print $2}'`\n", pdf);

		fprintf(g, "pdftk \"%s\" cat %d-${pages} output chapters/TDR-Bibliography.pdf\n", pdf, Bib);
	}
	fprintf(g, "printf '\033[32mNo errors: processing concluded normally.\033[0m\n'\n");
	fclose(g);
	system("chmod +x s-pages.sh");

	verror (ERR_NONE);
	return (ERR_NONE);

}

char *cleaname(const char *fname, char c, char d) {
	char *p = strdup(fname), *q, *r;

	q = p;

	while( (q = strchr(q, c)) != NULL)
		   *q++ = d;
	
	return p;
}
