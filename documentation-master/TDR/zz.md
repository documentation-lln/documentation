![](https://git.myrrha.lan/vdflorio/documentation/raw/master/images/zz.png)[^zz]

In the [TMR README](https://git.myrrha.lan/vdflorio/documentation/blob/master/TMR/README.md) I mentioned that
if you want to make use of [TeXObjects](https://git.myrrha.lan/vdflorio/documentation/blob/master/LaTeX/TeXObjects.md), then you need to 
uncomment a line in file [MINERVA-100MeV-ACCELERATOR-TDR.tex](https://git.myrrha.lan/vdflorio/documentation/blob/master/TDR/MINERVA-100MeV-ACCELERATOR-TDR.tex):

> `%\newcommand*{\EnableTeXObjects}{}%`

What happens then?

Well, I simply check whether the `\EnableTeXObjects` is defined and then I execute either the [TeXObject](https://git.myrrha.lan/vdflorio/documentation/blob/master/LaTeX/TeXObjects.md) command or
 static LaTeX command. This is precisely as the C language's `#ifdef` preprocessor statement.
 This is a simple example:

 ![](https://git.myrrha.lan/vdflorio/documentation/raw/master/images/ifdefined.jpg)

 ------

 [^zz]: "When [the single Z] symbol appears  at  the  beginning  of  a  paragraph,  it  warns  of  a  “dangerous  bend”  in the train of thought;  don’t read the paragraph unless you need to.  Brave and experienced drivers at the controls of TeX will gradually enter more and moreof these hazardous areas, but for most applications the details won’t matter. [...] Some of the paragraphs in this manual are so esoteric that they are rated [double Z]; everything that was said about single dangerous-bend signs goes double for these." (Donald E. Knuth, *The TeXbook*, Addison-Wesley, 1990. ISBN 0-201-13447-0)
