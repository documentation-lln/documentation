%{
/**************************************************************************************
 uncomment is a tiny tool that (1) takes a LaTeX file as input, (2) purges it of all
 commented lines, and (3) feeds grep with the uncommented file.

 v. 1.0, 20201118T1101

 created by vdflorio
 **************************************************************************************/
#include "vdfio.h"
%}

%%

[^\\]%.*$	;
.		ECHO;


%%

int main(int argc, char *argv[]) {
	FILE *f, *g;
	char *what, *tmpfile = tmpnam(NULL), *grep_options = NULL;
	char systring[512];
	int  retval;

	if (argc < 3 || argc > 4) {
		verror(ERR_WRONG_ARG);
		fprintf(stderr, "%s: Usage: %s regexp file [ grep-options ]\n", *argv, *argv);
		exit(ERR_WRONG_ARG);
	}

	what = argv[1];

	if (argc == 4) grep_options = argv[3];

	f = fopen(argv[2], "r");
	if (f == NULL) {
		verror(ERR_CANT_ACCESS);
		fprintf(stderr, "%s: Cannot open %s for reading.\n", *argv, argv[2]);
		return ERR_CANT_ACCESS;
	}
	//fprintf(stderr, "\t%sProcessing file %s.%s\n", KGRN, argv[i], KNRM);
	//fflush(stderr);

	g = fopen(tmpfile, "w");
	if (g == NULL){
		verror(ERR_CANT_ACCESS);
		fprintf(stderr, "%s: %sCannot open %s for writing.%s\n", *argv, KRED, tmpfile, KNRM);
		return ERR_CANT_ACCESS;
	}

	yyout = g;	// attach Flex's stdout to g
	yyrestart(f);	// attach Flex's stdin  to f

	while ( yylex() ) ;

	fclose(f), fclose(g);
	
	sprintf(systring, "grep %s '%s' < %s", grep_options, what, tmpfile);
	retval = scat(stdout, systring);

	unlink(tmpfile);

	return retval;
}
