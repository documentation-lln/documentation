#pdflatex -recorder MINERVA-100MeV-ACCELERATOR-TDR
#./generate-fls
printf '\033[44;1mPhase 1/3: Generate data structures and input list\033[0m\n'
input=`cat MINERVA-100MeV-ACCELERATOR-TDR.fls|grep '[AT][nD][nR][e-][xAB]'|uniq|awk '{printf "%s ", $2}'`
cp standard_tex_prologue.tex tmp_prologue.tex
fgres 'List of figures, with thumbnails, captions, and paths' 'TDR Tables' tmp_prologue.tex
enput="$input"
input="tmp_prologue.tex $input"
# printf '\033[32mNo errors: processing concluded normally.\033[0m\n'


printf '\033[44;1mPhase 2/3: Generate and launch gettables with input list to create LaTeX document with tables\033[0m\n'
make gettables || printf '\033[w31 make gettables failed!\033[0m\n'
cat $input | ./gettables > tables-in-the-TDR.tex
# printf '\033[32mNo errors: processing concluded normally.\033[0m\n'

printf '\033[44;1mPhase 3/3: Compile LaTeX document with tables\033[0m\n'
rubber --pdf tables-in-the-TDR.tex
# gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress -dNOPAUSE -dQUIET -dBATCH -sOutputFile=compressed_tables-in-the-TDR.pdf tables-in-the-TDR.pdf
# cp compressed_thumbnails.pdf ~/c/Users/vdflorio/AppData/Roaming/OpenText/OTEdit/EC_sckcen/c40593146/thumbnails.pdf

input="no_prologue.tex $enput"
cat $input | ./gettables > s.tex
make transnc
./transnc < s.tex > t.tex

# latex t
# bibtex t
cp tables-in-the-TDR.bbl t.bbl

# latex2rtf t.tex # > /dev/null 2>&1

# latex2html -split 0 -info 0 -no_navigation -no_images -external_images -ascii_mode -no_math -verbosity 0 t.tex # > /dev/null 2>&1
latex2html -split 0 -info 0 -no_navigation -no_images -external_images -ascii_mode -verbosity 0 t.tex # > /dev/null 2>&1
make nohref
./nohref < t/t.html > tables-in-the-TDR.html 
pandoc --no-check-certificate -s -r html tables-in-the-TDR.html -o t.docx
cp t.docx ~/d/Documents/TDR/
cp t.docx tables-in-the-TDR.docx

# Pure markdown
# pandoc -f docx -t markdown_strict tables-in-the-TDR.docx -o tables-in-the-TDR.md
# Gitlab's markdown
pandoc -f docx -t commonmark_x    tables-in-the-TDR.docx -o tables-in-the-TDR.md

echo "A rough translation of tables is available in tables-in-the-TDR.[docx|html|md]"

