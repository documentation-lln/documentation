# Versioning (begin)
subversion=$(<subversion)

((subversion++))

echo "Subversion set to $subversion.0"
echo $subversion > subversion
echo 0 > subsubversion

# Versioning (end)
