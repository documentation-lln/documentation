%
% TDR-A03-ProjectHistory.tex
%

\section[MYRRHA Project History]{MYRRHA Project History}\label{sect:history}
In what follows we briefly survey the projects that had an important role in the definition of \MYRRHA{}:
\PDSXADS{}, \EUROTRANS{}, \CDTFASTEF{},
\MAX{}, \MARISA{}, and recently concluded project
\MYRTE{}.

\FloatBarrier\subsection[The PDS-XADS Project]{The PDS-XADS Project}\label{sect:projects:pds-xads}
Consecutive to the work of the European Technical Working Group (ETWG) on Accelerator Driven Systems
(ADS), the Preliminary Design Study of an Experimental ADS (PDS-XADS) was launched in 2001. A large
European Collaboration, supported by the EU within the Fifth Framework Programme (FP5), performed these studies.

Five Work Packages (WP) covered the relevant issues, where WP3 was dedicated to the design of the high
intensity proton accelerator for an XADS.

At the start-up of the PDS-XADS project, the main initial specifications for the accelerator system
(\emph{e.g.} \TBeamEnergy{beam energy}, \TBeamIntensity{beam intensity}, \TBeamProfile{beam profile}, their
\TBeamStability{stability} and the accelerator \TAvailability{availability} and \TReliability{reliability})
was defined by WP1, in connection with the other WP of the project.

From this, WP3 (`The Accelerator') assessed the main requirements and
the corresponding technical answers. A reference solution, based on a superconducting linear accelerator
(linac) with its associated doubly achromatic \TBeam{beam line}, was worked out up to some detail.

With regard to high \TReliability{reliability}, the proposed design was to be intrinsically \TFaultTolerance{fault
tolerant}, relying on highly modular ``de-rated'' components associated to a fast digital feedback
system. The proposed solution also appeared to be robust with respect to operational aspects such as
maintenance and radiation protection.

\FloatBarrier\subsection[The EUROTRANS Project]{The EUROTRANS Project}\label{sect:projects:eurotrans}
The Integrated Project EUROTRANS (EUROpean research programme for the TRANSmutation of high-level nuclear
waste in accelerator driven systems) was initiated in the year 2005 with the goal of carrying out a
first conceptual design of an approximately 50 to 100 MWth experimental facility to demonstrate the
feasibility of transmutation in an ADS (\hyperref[sect:projects:eurotrans]{XT-ADS}), as well as to accomplish a generic conceptual design
(several hundreds MWth) of a modular European Facility for Industrial Transmutation (EFIT).

\EUROTRANS{} was a 5-year programme (2005--2010) funded by the European Commission within the 6th Framework
Programme (FP6), and involving more than 40 partners (research agencies, universities, and nuclear
industries)~\cite{P-TCADS10-Knebel}.

\EUROTRANS{} extended the R\&D carried out in \PDSXADS. Its activities were split into
several main domains, devoted to:

\begin{itemize}[noitemsep]
	\item The advanced design of a transmuter demonstrator (\hyperref[sect:projects:eurotrans]{XT-ADS})
		including all its sub components.

  \item The generic conceptual design of an industrial transmutation facility (EFIT).

  \item Experimental work dealing with the coupling of an accelerator, a \TBeamDestinations{spallation target}
	and a sub-critical core, which became the very low power \GUINEVERE{}
	experiment~\cite{P-GLOBAL09-Billeb}.

  \item Studies on advanced fuels for transmuters, investigations on suitable structural
	materials and collection of nuclear data for transmutation.
\end{itemize}

The specifications for demonstrator and industrial transmuters as designed within Domain 1 (DM1) of the
\EUROTRANS{} project, are shown in Table~\ref{tab:history:transmuters}.

\begin{table}
\begin{center}
\begin{tabular}{|m{2.58in}|m{2.19in}|}
\hline
\centering Transmuter demonstrator (\hyperref[sect:projects:eurotrans]{XT-ADS}/\MYRRHA{})
						& \arraybackslash{Industrial transmuter (EFIT)}\\\hline
Power: 50--100 MW\textsubscript{th}		& { Power: several \SI{100}{\mega\watt}\textsubscript{th}}\\\hline
	\Keff{} value: \midtilde{}0.95	& { \Keff{} value: \midtilde{}0.97}\\\hline
Fuel: highly-enriched MOX			& { Fuel: Minor Actinide fuel}\\\hline
Coolant \& target: Pb-Bi eutectic		& { Coolant \& target: Pb}\\\hline
\TBeam{Beam}: CW \SI{4}{\mA} \SI{600}{\MeV} protons	& { Beam: CW \SI{20}{\mA} \SI{800}{\MeV} protons}\\\hline
\end{tabular}
\end{center}
\caption{European transmuters general specifications.}
\label{tab:history:transmuters}
\end{table}

The main objective of the \EUROTRANS{} DM1 programme~\cite{P-TCADS10-DeBruyn} was actually to pave the
way towards the construction of an eXperimental facility (this project being called \MYRRHA{}) willing to
demonstrate the technical feasibility of Transmutation in an Accelerator Driven System.  That was the
\hyperref[sect:projects:eurotrans]{XT-ADS} concept.


\FloatBarrier\subsection[The CDT/FASTEF Project]{The CDT/FASTEF Project}\label{sect:projects:cdt-fastef}
CDT (Central Design Team) is a European Atomic Energy community (EUR-ATOM) Seventh Framework Programme
(FP7) project aiming to design a fast spectrum transmutation facility (FASTEF) able to demonstrate efficient
transmutation and associated technology through a system working in sub-critical mode, namely the accelerator
driven system (ADS), and/or critical mode; it is thus the next step after the FP6 \EUROTRANS{} project.

Nineteen Institutions from all over Europe participated this project, funded under the Collaborative Projects (CP) funding
scheme in the FP7 of EURATOM.

The CDT project focused on the design of \MYRRHA{}, being a cornerstone of the European sustainable nuclear
industrial initiative (ESNII) just launched under the SET-plan. At the end of CDT, a revised design
of \MYRRHA{} was delivered for the core, the primary system and the balance of plant. This design allowed
the preparation for the front-end engineering design work to be started.

The {\CDTFASTEF} design was based on the
\MYRRHA{}/\hyperref[sect:projects:eurotrans]{XT-ADS} design developed within
\EUROTRANS{}.

One major difference in objectives of \MYRRHA{}/{\CDTFASTEF} compared to
\MYRRHA{}/\hyperref[sect:projects:eurotrans]{XT-ADS} is the fact that the FASTEF needed to be able to
operate as a sub-critical system as well as a critical system. Both operation modes had to be foreseen
in the design from the beginning.

The first objective of {\CDTFASTEF} was to design an ADS of reasonable power that could be operated as a
high-flux and flexible fast spectrum irradiation facility. Goal was to make it possible for said facility
to host several experimental devices and loops in support of material and fuel research for different
innovative reactor systems and fusion material research and for the production of radio-isotopes for
medical purposes. Said objective produced a number of requirements:

\begin{itemize}[noitemsep]
  \item Demonstrate the ADS technology for transmutation and demonstrate efficient transmutation
	in the \MYRRHA/{\CDTFASTEF} facility so as to be able to serve
	as test-bed for transmutation.

  \item Guarantee attractive irradiation conditions for the experimental devices and rigs.

  \item As \MYRRHA/{\CDTFASTEF} would be designed also to be able to operate
	in a critical mode, a third requirement was to contribute to the demonstration of the Lead Fast
	Reactor technology.
\end{itemize}

\FloatBarrier\subsection[The MAX Project]{The MAX Project}\label{sect:projects:max}
The objective of the \MYRRHA{} Accelerator eXperiment (MAX) research and development programme was to support
the \MYRRHA{} project team to build on the \hyperref[sect:projects:eurotrans]{XT-ADS} reference design
developed within the \EUROTRANS{} project and reach in 2014 an updated
coherent and consolidated design of the \MYRRHA{} \SI{600}{\MeV} accelerator.

This first implied pursuing the R\&D, \emph{i.e.} detailed design and experimental activities, on both parts
of the machine---the \TInjector{injector} on the one hand, and the main linac on the other hand---focusing on
the \TReliability{reliability} aspects. These activities, acting on completely different components, were performed in
parallel in the work packages WP2 and WP3.

\TReliability{Reliability} is a major requirement for the \MYRRHA{} accelerator, which is asked to
experience a very low number of \TBeamInterruptions{beam interruptions}. During the \EUROTRANS{} phase,
a preliminary \TReliability{reliability} study of the ADS reference accelerator was conducted in order to assess the
number of those trips per mission time. From the conclusions of this study, it seemed realistic to
ultimately reach such \TReliability{reliability} goals if appropriate design concepts were used. On the other hand,
the absolute \TReliability{reliability} figures obtained through this study remained quite questionable, because of
the somewhat crude model used for such a complex system.  A major objective of \MAX{} was therefore
to develop an accurate \TReliability{reliability} model of the \MYRRHA{} accelerator, to be used for guidance of the
engineering design and for the \TOptimisation{optimisation} of the main subsystems. Activities related
to that objective were gathered into a dedicated work package (WP4), which played the role of general
engineering expertise in view of the construction of \MYRRHA{}.

In addition to all the above-reported activities, dedicated studies were conducted within \MAX{} to ensure
the global coherence of the accelerator design, mainly through advanced \TBeamSimulations{beam} dynamics
\TSimulation{simulations}. A work package (WP1) was dedicated to this topic. WP1 was also in charge of
coordinating the activities performed in the other \MAX{} work packages, and of ensuring strong links
with the CDT and FREYA projects based in Mol (Belgium), and with the related R\&D ongoing at the time
in the accelerator community.

The main goal of the \MAX{} project was to deliver an updated consolidated reference layout of the \MYRRHA{}
LINAC with sufficient detail and adequate level of confidence in order to initiate in 2015 its engineering
design and subsequent construction phase.

In order to reach this goal, advanced \TBeamSimulations{beam} \TSimulation{simulation} activities were
undertaken and a detailed design of the major accelerating components was carried out, building on
several prototyping activities.

A strong focus was placed on all the aspects that pertain to the \TReliability{reliability} and
\TAvailability{availability} of this accelerator, since the number of \TBeamInterruptions{beam interruptions}
longer than three seconds has to be minimised. Such frequently-repeated \TBeamInterruptions{beam
interruptions} could indeed induce high \TThermalStresses{thermal stresses} and \TFatigue{fatigue} on the reactor
structures, the \TBeamDestinations{target} or the fuel elements, with possible significant damages
especially to the fuel claddings.

In this context, the \MAX{} team developed an accurate \TReliability{reliability} model of the \MYRRHA{} accelerator by
using a methodology applied for nuclear power plants. On the other hand it was foreseen to experimentally
prove the feasibility of the innovative \TFaultTolerance{fault tolerance} \TRedundancy{redundancy}
scheme, by making extensive use of the prototypical accelerating module developed during the previous
FP6 \EUROTRANS{} programme.

Please refer to
\href{https://ecm.sckcen.be/OTCS/llisapi.dll?func=ll\&objId=5328035\&objAction=browse\&viewType=1}{Alexandria folder 5328035}~\cite{D-MAX}
for the Deliverables of project \MAX{}.

\FloatBarrier\subsection[The MARISA Project]{The MARISA Project}\label{sect:projects:marisa}
At the current stage, main emphasis is on the development of the \MYRRHA{} conceptual and basic design,
the elaboration of the \MYRRHA{} consortium, the execution of the supporting R\&D and engineering programme,
and the preparation of the \MYRRHA{} licensing process.

In order to bring the \MYRRHA{} project to the degree of maturity needed to start construction, further
steps have to be taken in support of the establishing of the \MYRRHA{} consortium, the development and the
implementation of a legal and financial framework, the integration and the coordination of scientific
and technical work, as well as the deployment of adequate methods, procedures and instruments for the
management of the \MYRRHA{} project during the construction phase.

The MyrrhA Research Infrastructure Support Action (\MARISA{}) project was a Coordination and Support
Action (CSA) marking the transition from the \MYRRHA{} preparatory phase to the construction phase.
More specifically, the work programme of the \MARISA{} project encompassed the following actions:

\begin{itemize}[noitemsep]
	\item To realise essential steps related to the establishing of the \MYRRHA{} consortium,
	which is a basic condition for assembling a critical mass and sources of funding for the project;

  \item To integrate different national and international research programmes and initiatives
	related to R\&D and technology development required for the deployment of a lead-cooled fast reactor
	system including the development of a roadmap identifying the contribution of \MYRRHA{} as a fast neutron
	spectrum research facility in relation to the closing of the nuclear fuel cycle;

  \item To establish a legal framework, an organisational management structure and the
	definition of rules for the valuation of contributions from Consortium Members to \MYRRHA{};

  \item To establish a roadmap and business plan for the funding of the \MYRRHA{} project during
	its entire life-cycle;

  \item To establish and to implement state-of-the-art management principles, procedures and
	instruments allowing for the management of the \MYRRHA{} project scope, the supervision and the control
	of \TCostsPrj{project cost}, the management of the \MYRRHA{} schedule as well as the management of human resources,
	information and Intellectual Property Rights.

  \item To initiate the licensing process, in particular the drafting of the Environmental
	Impact Assessment Report, which is a basic and essential step in the \MYRRHA{} licensing process;

  \item To integrate and coordinate work related to the \MYRRHA{} accelerator and the Balance of
	Plant. The \MARISA{} project also included an external review of the \MYRRHA{} primary system, thus providing
	an additional \TValidation{validation} prior to passing on to the construction phase.
\end{itemize}

The \MARISA{} project was pivotal since results from work performed in the framework of this CSA-CA 
significantly supported the \MYRRHA{} project by creating a common foundation required to move
from the pre-project to the construction phase.

Please refer to \href{https://ecm.sckcen.be/OTCS/llisapi.dll?func=ll\&objId=4213200\&objAction=browse\&viewType=1}{Alexandria folder 4213200}~\cite{D-MARISA}
for further information about project \MARISA{}.


\FloatBarrier\subsection[The MYRTE Project]{The MYRTE Project}\label{sect:projects:myrte}
H2020 MYRTE (MYrrha Research and Transmutation Endeavour) was a Research and Innovation Action started in April
2015 and now concluded. Its second work package was dedicated to the accelerator R\&D for \MYRRHA{}. The
contents of the work package had been defined following the recommendations of the FP7 \MAX{} project final
report and addresses the topics that had been identified as priority to successfully pursue the research,
design and development of the \MYRRHA{} accelerator and prepare for its actual construction.

Such topics are listed below:

\begin{itemize}[noitemsep]
  \item \TBeamCharacterisation{Beam characterisation}. ``Correct (\emph{i.e.} conform to reality) initial beam data are of
	fundamental importance in all \TBeamDynamics{beam dynamics} \TSimulation{simulation} tools. Making
	use of the platform consisting of the ECR \LinacSource{} and the {\LinacLEBT} that is presently
	under construction in \sckcen, an experimental campaign for fully characterising the emerging
	beam (\TBeam{beam} entering into the {\LinacRFQ}) should be launched.'' This need was answered
	in particular in \MYRTE{} WP2 through Task 2.4 and 2.8.

%  \begin{Alert}{}
%  	Fr\'ed\'eric:
%  	has been launched ? Since the RFQ is connected to the LEBT (in Feb 2020), is there
%  	still a possibility to perform experimental campaign. To confirm that point w/ Angelique and
%  	Aur\'elien, but I guess this particular phase characterising the beam at the output of the LEBT
%  	is indeed done. Thus, TBC.
%  \end{Alert}

\item \TInjector{injector} demonstration. ``The injector has to be investigated in detail, in a
	step-by-step approach allowing for \TTesting{testing} with \TBeamTesting{beam}. The logical
	first step is thus the construction of the 4-rod {\LinacRFQ} that has been designed in \MAX{}
	and coupling it to the {\LinacSource}-{\LinacLEBT} assembly presently under construction. This
	obviously implies the procurement of all the auxiliary equipment. The solid state \TRF{RF}{}
	power amplifier and the digital {\LinacLLRF} will themselves be prototype items.'' This need
	was answered in \MYRTE{} WP2 through Tasks 2.1, 2.2, 2.3, and 2.7.

  \item Superconducting cavities demonstration. ``Since the superconducting linac is highly
	modular, thorough prototyping is very efficient. The \MYRRHA{}-type spoke cryomodule has been fully designed
	in \MAX{} and should now be constructed and tested.'' This need was answered in particular in \MYRTE{} WP2
	through Tasks 2.10 and 2.11.

  \item \TReliability{Reliability} \TModelling{modelling}. ``The effort around the \TReliability{reliability}
	\TModelling{modelling} tool put in place by \MAX{} should be pursued with vigour so as
	to obtain confidence and \TPredictability{predictability}. This tool is essential for the
	\TOptimisation{optimisation} task.'' This need was answered in \MYRTE{} WP2 through Tasks 2.9
	and 2.12.

  \item Smart and  \TReliability{Reliable} Control. ``A virtual accelerator, performing on-line machine
	\TModelling{modelling} with \TRealTime{real-time} machine data, will be a powerful and essential
	operational tool in the high \TReliability{reliability} context. It is challenging, though,
	both from the \TSimulation{simulation} and from the \TControlSystem{control system} point of
		view. The \TInjector{injector} tests should serve such a development.'' This need was answered in
	particular in \MYRTE{} WP2 through Tasks 2.5 and 2.6.

\end{itemize}

Please refer to
\href{https://ecm.sckcen.be/OTCS/llisapi.dll?func=ll\&objId=5734589\&objAction=browse\&viewType=1}{Alexandria
folder 5734589}~\cite{D-MYRTE} for the Deliverables of project \MYRTE{}.

\FloatBarrier\subsection[Return on Experience from Other Projects]{Return on Experience from Other Projects}
The \MYRRHA{} project capitalised on the return on experience from other
relevant endeavours. Two examples are briefly covered in this section.

\subsubsection{Reliability analysis of the SNS linac}\label{ssect:projects:SNS}
The SNS (Spallation Neutron Source) at ORNL (Oak Ridge National Laboratory)~\cite{P-PAC12-VanDP} was
selected at the very start of the \MAX{} project as the existing accelerator to be used for developing
a relevant \TReliability{reliability} \TModelling{modelling} methodology to be applied to the \MYRRHA{}
linac case. The main steps of the work and the lessons learned~\cite{P-RF13-Bia} are as follows:

\begin{itemize}[noitemsep]
  \item A complete structure of the \hyperref[ssect:projects:SNS]{SNS} linac \TReliability{fault tree reliability model} has been
	developed, reviewed and validated.

  \item The results obtained running this \hyperref[ssect:projects:SNS]{SNS} risk spectrum model have been analysed and
	compared with the \hyperref[ssect:projects:SNS]{SNS} Logbook data registered during \TBeam{beam} operation
	over the period Oct. 2011 -- Aug. 2012 in ORNL.

  \item \TSimulation{Simulation} results give an overall \TAvailability{availability} of the \hyperref[ssect:projects:SNS]{SNS}
	linac of about \SI{73}{\percent} and show that the most affected systems are:

	\begin{itemize}[noitemsep]
		\item {\LinacSCLinac}, \TInjector{injector}, diagnostics \& controls,

	  \item \TRF{RF}{} systems,

	  \item Power Supplies and associated controllers.
	\end{itemize}

  \item These results are in line with the \hyperref[ssect:projects:SNS]{SNS} operation log-book records, giving a good
	confidence in the validity of the method to be used in the case of the \MYRRHA{} linac.

\item \TReliability{Reliability} could be improved in an \hyperref[ssect:projects:SNS]{SNS}-type linac by including \TRedundancy{redundancy} in the
	  most affected system and components, \emph{e.g.} implementing intelligent \TFailures{fail}-over \TRedundancy{redundancy} in controllers
	for compensation purposes, based on suited and reliable diagnostics.
\end{itemize}

Details of this analysis can be found in~\cite{D-MAX-4.2}.

\subsubsection{Accelerator operation in the GUINEVERE experiment}\label{sect:projects:guinevere}
The \GUINEVERE{} experiment~\cite{P-AccApp11-Baylac} on the \sckcen{} site coupled a sub-critical fast
reactor core with an accelerator, generating neutrons by bombarding a tritium target with a pulsed, DC,
or DC-with-programmable-interruptions deuteron \TBeam{beam}. This accelerator-coupled experiment represented a
unique platform to study ADS \TReliability{reliability} and \TOptimisation{system optimisation}.

A detailed analysis of the \GUINEVERE{} machine with respect to operation \TReliability{reliability}, interfaces, data
recording/logging, \TSafety{safety}/licensing aspects and beam monitoring was carried out. The main problems
encountered, in particular during the last 4 years of \TCommissioning{commissioning} and operation, were thoroughly
analysed. The lessons learned that could be applied to improve the \MYRRHA{} linac operation \TReliability{reliability}
were derived. The main ones are summarised in the followingi list:

\begin{itemize}[noitemsep]
  \item Relations with \TSafety{safety} authorities had a huge impact on the facility \TCommissioning{commissioning} and
	operation; this will need to be anticipated as much as possible for \MYRRHA{}.

  \item DC mode was not as successful in \GUINEVERE{} compared to pulsed mode, especially
	because this operational mode was never \TCommissioning{commissioned} properly due to planning
	constraints. It is recommended for \MYRRHA{} to perform extensive \TBeamCommissioning{beam
	commissioning} to fully understand and master the \TBeamOperation{beam operation} before any standard operation
	of the facility.

  \item The too few on-line \TBeamDiagnostics{beam diagnostics} appeared to be a limitation in \GUINEVERE{} to be
	able to anticipate problems; because of this, it was suggested to foresee numerous
	\TBeamDiagnostics{beam diagnostics} for \MYRRHA{}, both interceptive and non-interceptive. As
	detailed in Sect.~\ref{sect:beam-inst}, this suggestion has been adopted in \MINERVA{}.
	% see message from Dahmane,  Fri 5/29, 2:14 PM, subject: "Saluti ed una domanda"

  \item An excellent level of communication between the accelerator and the reactor teams is
	mandatory since the design phase of the facility.
\end{itemize}

All of the above results and conclusions can be found in~\cite{D-MAX-4.3}. Significant progress in machine performances
and \TReliability{reliability} were accomplished during the two years of coupled operation of the facility.

\begin{figure}[h]
\centering
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    %\includegraphics[width=2.1783in,height=2.9091in]{MYRRHATDRPartA-img/MYRRHATDRPartA-img010.jpg}
    \includegraphics[height=3.1in,keepaspectratio]{MYRRHATDRPartA-img/MYRRHATDRPartA-img010.jpg}
    \caption{Accelerator during beam line handling: the vertical line is being inserted in the reactor bunker
	     while the magnet is retracted.}
  \end{subfigure}
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \includegraphics[height=3.1in,keepaspectratio]{MYRRHATDRPartA-img/MYRRHATDRPartA-img011.jpg}
	  \caption[Vertical beam line inserted into the reactor core]{Vertical beam line inserted into the reactor core.
	  \protect\hphantom{This text will be completely and totally and utterly invisible.}\vspace*{11pt}~}
  \end{subfigure}
\caption[Accelerator during beam line handling. Vertical beam line inserted into the reactor core.]{}
\end{figure}

\FloatBarrier
%
% EOF TDR-A03-ProjectHistory.tex
%
