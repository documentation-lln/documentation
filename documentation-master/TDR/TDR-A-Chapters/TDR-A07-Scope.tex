%
% TDR-A07-Scope.tex
%
\section[Scope of This Document]{Scope of This Document}

%{{{ Scope
\subsection[TDR Scope and Approach]{TDR Scope and Approach}
\label{TDR:scope}
%
% Scope-related statements
%
As mentioned in the first Part of this document and clarified in~\cite{R-SCKCEN-2020-35728497}, \MINERVA{}
is to be interpreted as a first step in an elaborated, long-lasting workflow:
\begin{quote}
	``the \MINERVA{} project is embedded in the overall \MYRRHA{} programme and shall be understood
	as the result of the phased \MYRRHA{} implementation strategy decided in 2015. It is therefore
	important that \MINERVA{} is at all times designed with \MYRRHA{} in mind as the final
	goal''~\cite{R-SCKCEN-2020-35728497}.
\end{quote}
As a consequence, although the present document's main focus is the \MINERVA{} accelerator, it also
surveys concepts and preliminary results related to \MINERVA{}'s final goal---the \MYRRHA{} \SI{600}{\MeV}
accelerator.

%  In what follows, the terms ``MINERVA accelerator'' and ``MYRRHA accelerator'' shall be assumed
%  as terms referring to instances of the same concept. Those terms will assumed as synomyms unless the context
%  requires to highlight instance-specific features and characteristics.  In such case we shall disambiguate
%  by referring to ``the \SI{100}{\MeV} accelerator'' (MINERVA's) or ``the \SI{600}{\MeV} accelerator''
%  (MYRRHA's). The terms ``accelerator'' or ``linac'' shall also be used to refer to the same concept.
%\begin{tcolorbox}

\label{TDR:MINERVA:scope}
\label{sect:general-intro}
The available results of past research and experimentation, together with external constraints (for instance,
budgetary constraints) allow already to take educated decisions and move to an effective development
phase---\MINERVA{}. Other choices are still depending on unknowns of various nature and on steps not yet
taken. As an example, choices pertaining to \TRedundancy{redundant} designs (to what extent to make use
of \TRedundancy{redundancy}, \emph{e.g.}, in \TPLCs{PLCs}) rely on advanced \TReliability{reliability}
\TModelling{modelling} the results of which are not yet available. \MINERVA{} is thus an enabling stage
that is to carry out a number of strategic development steps functional to further experimentation and
measurements, in turn providing us with characterisation of values and ranges for the above unknowns. The
present version of the TDR is thus a ``snapshot'' in the dynamic progress of the \MYRRHA{} workflow. The
level of detail provided in what follows reflects this fact and will thus only provide data and results
that have stabilised at this stage of the \MYRRHA{} workflow.

%\end{tcolorbox}

% --> Meeting with Dirk and Jorik, 20201006:
Please note that the degree of maturity and the presentation style of the chapters in this document
is not yet homogeneous. This is the case in particular for
Sect.~\ref{sect:ctrl-system}, which is a relatively recent addition and has not been yet internally reviewed.

%{{{ Living Documentation Engineering
\subsubsection{The TDR Approach of Living Documentation Engineering}
For the reasons we have just stated, the present text should be considered and managed as a \emph{living
document}, i.e., \emph{a document that is meant to evolve and stay in sync with the reference system it
focuses on.} The Editor of this document addressed the problem of engineering an approach for living document
management~\cite{B-2019-Martraire}, which he applied to this TDR.  A detailed description of said approach
is beyond the scope of the present document, though can be found in~\cite{R-SCKCEN-DeFlorio-2021-45005658}.
%}}}

%  \adrian{Dedicated chapters/sections should indicate the unknowns and risks, such that we clearly know, what we have to focus on.}

%
% end of Scope-related statements
%
%}}}

%{{{ Ancillary Information
\subsection[Ancillary Information]{Ancillary Information}
\label{TDR:structure}

In the first Part of this TDR, we summarised the history and the objective of the \MYRRHA{} accelerator,
including its realisation in several phases, with particular emphasis on the \MINERVA{}, the first and ongoing phase of \MYRRHA{}.
% A major focus of this document is given by MINERVA, the
% first and ongoing phase of MYRRHA. Being MINERVA the current phase, consolidated results and design
% choices provided in this document shall refer to the \SI{100}{\MeV} accelerator.
% 
A detailed description, as of {\TWinParseDate}, of the different components of the accelerator is given in
Part~\ref{chp:main}.
%:
%
%\ulrich{no need to repeat the table of content!}
%
%
%\begin{itemize}[noitemsep]
%\item	Injector					(Sect.~\ref{sect:injector}); 
%\item   MEBT3						(Sect.~\ref{sect:mebt-3});
%\item   Main LINAC					(Sect.~\ref{sect:sc-linac});
%\item   The High Energy Beam Transport Lines            (Sect.~\ref{sect:hebt});
%
%%\item   HEBT lines for the 600 MeV (the description of the corresponding lines for
%%	the 100 MeV being currently not yet available)	(Sect.~\ref{sect:hebt});
%
%\ifdefined\EnableBeyondMINERVA
%\item   The targets:
%	\begin{itemize}[noitemsep]
%	\item  Reactor window and spallation target	(Sect.~\ref{sect:rwst});
%	\item  Proton target beam lines			(Sect.~\ref{sect:ptbl});
%	\end{itemize}
%\fi
%
%\item   The cavities:
%
%	\begin{itemize}[noitemsep]
%	  \item Room-temperature cavities		(Sect.~\ref{sect:rt-cavities});
%	  \item Super-conductive, single spoke		(Sect.~\ref{sect:ss-cavities});
%\ifdefined\EnableBeyondMINERVA
%	  \item Double spoke				(Sect.~\ref{sect:ds-cavities});
%	  \item Elliptical				(Sect.~\ref{sect:el-cavities});

%	\end{itemize}
%
%\item  Radio-frequency system				(Sect.~\ref{sect:rf-system});
%\item  Cryogenic system					(Sect.~\ref{sect:cryogen});
%\item  Beam instrumentation				(Sect.~\ref{sect:beam-inst});
%\item  Focusing magnets \& steerers			(Sect.~\ref{sect:magsteers});
%\item  Vacuum system					(Sect.~\ref{sect:Vacuum-System}).
%\end{itemize}
%
%
%  Part~\ref{chp:main} contains also
%  \ifdefined\EnableBeyondMINERVA
%   chapters dedicated to the balance-of-plant components:
%  
%  \begin{itemize}[noitemsep]
%  \item  Water cooling					(Sect.~\ref{sect:water-cooling});
%  \item  Reactor reactivity monitoring			(Sect.~\ref{sect:reactivity});
%  %\item  Control system					(Sect.~\ref{sect:ctrl-system})%
%  \item  Reliability of the installation			(Sect.~\ref{sect:saferel});
%  \item  Buildings					(Sect.~\ref{sect:building});
%  \item  Commissioning \& decommissioning			(Sect.~\ref{sect:commiss}).
%  \end{itemize}
%  \else
%   a chapter devoted to the Control System                (Sect.~\ref{sect:ctrl-system}).
% \fi
%          \adrianreply{We need to have a common approach on the reference (numbers). It seems, that %
%          	we should carefully into a design management process.\textCR %
%          	If you claim, that some information is not coherent, which information is then relevant?}{vdflorio}{%
%          		Reference management process: an approach to the definition of meaningful reference labels has been proposed. The TDR make use of said approach for its reference labels. The reference numbers, on the other hand, depend on the bibliographic style that has been selected. Several are possible and can be changed very easily by adjusting one line of the LaTeX project. \textCR %
%          		If I understand correctly your second statement, you are referring to the fact that presenting information that has not the same degree of maturity decreases the maturity of the TDR. In my opinion this is a matter of particular importance: obviously we are striving to select sources of the highest maturity; though this is a living document, written in parallel to the progress of the system it refers to. Thus this like in Leibniz when he refers to ours as the best of all possible world: it is a statement that refers to the overall process, not to any particular step thereof.}
%Please note that some discrepancies may be noticed
%between the sections because different contributors may be referring to different as of today yet
%unconsolidated design options.
\ifdefined\EnableBeyondMINERVA
%\else
%Note that a number of sections pertaining to MYRRHA phases beyond MINERVA have been excluded from the
%current version of the TDR. Those sections describe the double-spoke and elliptical cavities;
%reactor window and spallation target; and reactor reactivity monitoring.
\fi
Supplementary, ancillary information is provided as a set of Appendices in Part~\ref{chp:annexes}.
%  \adrian{I would appreciate a dedicated chapter (or per section), which clearly indicates the risks and unknowns. indicating, where we should focus.}
In particular, a Parameters List is provided on page~\pageref{sect:parameters-list}.

An Index is available on page~\pageref{sect:index}. In order to facilitate the identification of sought
terms, some of them appear in it both as stand-alone topics and as sub-topics. This is done for instance
when a term corresponds to a sub-system of a system component, or to a sub-class of a more general class
of systems.

\ifdefined\EnableBeyondMINERVA
This Volume also includes a third Part, which focuses on cross-cutting aspects.
In the current version of this document, Part~\ref{chp:additional} only discusses \TReliability{reliability} aspects.
\else
Please note that
%a balance-of-plant section devoted to buildings has not been included in the present
%version of the TDR as the available information was not deemed as mature enough for inclusion.
%Likewise,
the present version does not include chapters specifically devoted to \TReliability{reliability} and \TSafety{safety} aspects.
%   \adrianreply{being one of the major aspects of MINERVA, this would be the most important chapter, isn't it?}{vdflorio}{%
%   	Yes I agree. A future version of this document shall foresee a specific chapter devoted to non-functional, crosscutting design concerns (in particular, dependability).\textCR %
%   	As an aside, It is my opinion that terms such as reliability, safety, etc should not be used informally. I took the liberty to upload onto Alexandria the chapter of a book I wrote, which introduces the dependability terms we often use in the TDR according to a well-known and -established model. The terminology in the TDR and in this future chapter should be coherent and compliant to such model (or some other one, specifically selected to harmonize the understanding of readers).}
A future version of this document might foresee a specific chapter devoted to non-functional,
crosscutting design concerns (in particular, dependability), although a formal decision about this
has not been taken yet.
\fi

Please note that the definitions of all terms, acronyms, and abbreviations required to unambiguously interpret
this document are provided in project-wide glossary~\cite{R-SCKCEN-2020-8905079}.
%  \footnote{Quoting from~\cite{R-SCKCEN-2020-8905079},
%  	``MINERVA documents do not need to contain a glossary.  A reference to this global glossary suffices as
%  	it will be distributed to all parties involved (internally and externally).''},
%%which is given in~\cite{R-SCKCEN-2020-8905079}.

This document makes use of the naming convention terms defined in~\cite{R-SCKCEN-2020-2374425040-VanDP}.
In order to distinguish acronyms and other terms from those in the naming convention,
the latter terms will appear in the text with a Sans-Serif typeface, as in string ``\naming{\ncSPOKE{3}}''.
Figure~\ref{fig:NamingConvention:Mnemonics} summarizes some of the terms used in the naming convention.

Hyperlinks are used extensively in this document to refer to external information or
related sections of this document. Several of said hyperlinks refer to information on Alexandria,
the internal \sckcen{} documentation system, hence they will be valid only when the requestor is
connected to the \sckcen{} corporate network.

\begin{sidewaysfigure}
%%%\centering
%%%\includegraphics[width=1.0\textwidth]{MYRRHATDRPartA-img/MINERVA-ACC-Mnemonics-Left.png}
%%%	\vbox to 1cm{\ }
%%%	%\vspace*{0.5cm}
%%%\centering
%%%\includegraphics[width=1.0\textwidth]{MYRRHATDRPartA-img/MINERVA-ACC-Mnemonics-Right.png}
%%\includegraphics[height=0.96\textheight]{MYRRHATDRPartA-img/MINERVA-ACC-Mnemonics.png}
\centering
\includegraphics[width=1.0\textwidth]{MYRRHATDRPartA-img/MINERVA-ACC-Mnemonics.png}
	\caption[A summary of the higher-level terms defined in the naming convention.]{A summary of the higher-level terms defined in the naming convention. Picture adapted from~\cite{R-SCKCEN-2020-2374425040-VanDP}.}
	%The picture has been cut into two parts in order to improve its readability.}
\label{fig:NamingConvention:Mnemonics}
\end{sidewaysfigure}

%}}}


%
% EoF TDR-A07-Scope.tex
%
