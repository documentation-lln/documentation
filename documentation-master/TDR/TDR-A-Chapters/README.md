This directory includes all files that constitute the sections of Part A of the TDR. More information will be progressively provided.

Please note that module `PartA.tex` defines the order of inclusion of the Part A sections.
Please note also that `TDR-A01-GeneralIntro.tex` is *not* included in Part A.
