%
% TDR-A04-AcceleratorOverview.tex
%


\section[The MYRRHA 600 MeV Accelerator Overview]{The MYRRHA 600 MeV Accelerator Overview}\label{sect:myrrha}
\FloatBarrier\subsection[Accelerator Design]{Accelerator Design}\label{sect:myrrha:design}
The MYRRHA ADS concept requires a \SI{2.4}{\MW} proton accelerator operating in CW mode. The extremely
high \TReliability{reliability} requirement (\TBeamInterruptions{beam trip} number) can immediately be identified as the main
technological challenge to achieve.

The conceptual design of the accelerator has been carried out during the \PDSXADS{}
project~\cite{J-NuclInstrumMethodsPhysRes-2006-562.2-Bia} and further developed during the \EUROTRANS{}
project for serving the XT ADS, primarily focusing on the improvement of the beam \TReliability{reliability} as
this is one of the key issues in order to achieve a high \TAvailability{availability} factor for the transmutation
facility~\cite{J-NuclInstrumMethodsPhysRes-2006-562.2-Bia}.

A superconducting linac-based solution has been selected~\cite[Sect.~3.1]{D-PDSXADS-63},
%%\adrian{what is the reference for this decision? What are the technical justifications?}
leading to a very modular and upgradeable machine.
\ifdefined\EnableWarningsAndAlertsStillPending
\begin{Alert}{}
	Fr\'ed\'eric:
	how modular and how upgradeable ?
\end{Alert}
\fi

Such a solution also yields high \TRF{RF}{}-to-beam efficiency thanks to the super-conductivity
(\TOptimisation{optimised} \TCostsOpr{operational cost}) and has an excellent potential
for \TReliability{reliability}, in the main linac, which may be designed to be intrinsically
\TFaultTolerance{fault-tolerant}. In the front-end section, a hot stand-by~\cite{B-1989-Johnson-Addison}
\TRedundancy{redundant} \TInjector{injector} with fast switching capability shall be installed to achieve
the \TReliability{reliability} target.

%% \input{TDR-A-Chapters/Requirements-MYRRHA.tex}

The MYRRHA accelerator main \TBeamRequirementsMYRRHA{beam requirements} are presented in Table~\ref{tab:MYRRHA-beam-requirements}.
Major references here have been~\cite{R-SCKCEN-2020-30233162,R-SCKCEN-2020-36777107}\footnote{Please note
	that the values shown here include \emph{accelerator\/} requirements, namely
	% design requirements derived from the top-level requirements of the accelerator ``users''.}
	design requirements for the accelerator to comply with the top-level requirements
	of the accelerator ``users''.}.


%  \ulrichreply{same as above: many numbers wrong, remove and instead quote the ALX-documents}{vdflorio}{The %
%  	Alx documents describe top level requirements, though here we also introduce accelerator requirements---in %
%  	other words, local prerequisites necessary in order to achieve those top-level design goals.}

%  \mycomment{vdflorio}{%
%  Our main references for Table 3 and 4 have been the documents: \textCR %
%   - ``Reactor Proton Beam Requirements'' (\cite{R-SCKCEN-2020-36777107}, Alx 36777107, ``MAN-TEMP-04 Rev. 1.0'') \textCR %
%   - ``MYRRHA reactor scope justification'', (\cite{R-SCKCEN-2019-30233162}, v. 20/12/2019, Alx 30233162. %
%      This document is not an official released, though \cite{R-SCKCEN-2020-36777107} is based on this document.\textCR %
%   - ``Facility Requirements'' (\cite{R-SCKCEN-2020-35576097}, Alx 35576097);\textCR %
%  	 \textCR%
%   Other documents that we have consulted include:\textCR %
%   - ``MYRRHA Top Level Requirements'' (\cite{R-SCKCEN-2012-1037605}, Alx 1037605);\textCR %
%   - ``MINERVA Framework'' (\cite{R-SCKCEN-2012-1037605}, Alx 1037605);\textCR %
%   - ``MINERVA high level safety requirements (\cite{R-SCKCEN-2019-36111201}, Alx 36111201);\textCR %
%   - ``Operational Concept Description -- MINERVA'' (\cite{R-SCKCEN-2020-35728497}, Alx 35728497);\textCR %
%  	\textCR %
%    Note that a new, approved release of document ``MYRRHA reactor scope justification'', v. 4.0, is available since 9 July 2020---%
%    thus, after the release of the Final Draft of the TDR.\textCR%
%    The Alx code for said document is still Alx 30233162 while the internal reference label used %
%    in the TDR is \cite{R-SCKCEN-2020-30233162}.\textCR %
%  	\textCR %
%  Regarding the values in the two tables, please see my comments in their entries.}



\begin{table}[h]
\begin{center}
%\begin{tabular}{|m{1.5941598in}|m{1.7906599in}|m{2.5802598in}|}
%\begin{tabular}{|c|l|l|}
\begin{tabular}{|cll|}
\hline
Particle                                                            & & 
					Protons\\\hline
	\TBeamEnergy{Beam energy} \textpm{} accuracy & & 
					\SI{600}{\MeV} \textpm \SI{1}{\percent}
% See ``MYRRHA reactor scope justification'': "with a nominal acceptable jitter of +-1\% (1 sigma)."}%
			\\\hline
	\multirow{3}{1.594in}{\centering Nominal instantaneous beam current \textpm{} accuracy} & &
	\multirow{3}{2.5802598in}{\SI{4}{\mA} \textpm \SI{2}{\percent}}\\
	&&\\
	&&\\\hline
%     In ``MYRRHA reactor scope justification'', v.4: ``nominal instantaneous proton beam current shall be 4 mA +-2\%''.\textCR %
%     In previous version of said document: ``Nominal Proton average beam current: up to 4 mA +-2\%''.\textCR %
%     In ``Reactor Proton Beam Requirements'': states that ``nominal instantaneous proton beam current shall be 4 mA +-2\%. \textCR%
%     In ``Facility Requirements'', requirement [TLR 001b: Performance] states that %
%     ``The nominal instantaneous beam current shall be 4mA with $<$2\% jitter.'' \textCR %
%  	Shall I revise the term ``Peak beam current reference value''? Shall I mention the jitter too?}\\
%                                                                    & &\\\hline
%  \multirow{2}{1.5941598in}{\centering Time structure}
%  	& Microstructure         & CW\\
%  	& Nominal macrostructure & \SIrange{1}{50}{\ms} beam holes (at 10 -- 250 Hz)&\\\hline
%
	\TBeamPower{Beam power stability}%\mycomment{vdflorio}{Dirk, could you please confirm that I should call this figure (and the corresponding one in next table) as accuracy rather than stability? Thank you!} 
	& & \textpm\SI{1}{\percent} (100 ms integration time)
%  	\mycomment{vdflorio}{%
%     +- 2\% power stability is likely to be an error, my apologies. A value of 1\% can be found in ``Facility Requirements'':\textCR %
%     as requirement [TLR 001a: Performance]: ``The nominal beam energy shall be 100 MeV with a stability $<$1\%''.\textCR %
%  	Dirk, may I mention 1\% here?}
	\\\hline
%
% Footprint on target                                                     &  Circular, {\o} 85 mm&\\\hline
% Footprint stability                                                     & \textpm\SI{10}{\percent} (1 s
%									     integration time)&\\\hline
\multirow{3}{1.594in}{\centering \TBeamInterruptions{Beam trip}%
	\tablefootnote{Note that the values here assume a reference reactor cycle of 90 days.} \TFaultTolerance{tolerance}%
	\tablefootnote{The values provided here are MYRRHA ADS requirements. %
		Their feasibility in a given configuration shall be verified via theoretical and experimental \TReliability{reliability} studies.}%
%  	\mycomment{vdflorio}{%
%     MTBF requirements have been formulated differently on the consulted documents: values in the three %
%     beam trip duration intervals are reported as being as follows:\textCR %
%     \textHT \textHT -- in ``Reactor Proton Beam Requirements'' and ``MYRRHA reactor scope justification'',
%  			v. 4: $>$ 10s, $>$ 900s, $>$ 250h (resp.)\textCR %
%     \textHT \textHT -- in ``MYRRHA reactor scope justification'': unlimited, $>$ 900s, $>$ 250h (resp.) %
%  	Note: the reference to these values there is %
%          Alx 24857241, namely ``MYRRHA -- 600 MeV Accelerator -- TDR''\textCR %
%     \textHT \textHT -- in ``Facility Requirements'': $>$ 1s, $>$ 85m, $>$ 1500h (resp.) (requirement [TLR 001e: Performance])\textCR %
%     	\textCR %
%  	I adopted the first option here, viz. $>$ 10s, $>$ 900s, $>$ 250h}%
  	}
& $\text{MTBF}_{\tau < \SI{0.1}{\second}}$  & $>$  \SI{10}{\second}\\
	& $\text{MTBF}_{\SI{0.1}{\second} \, < \tau < \, \SI{3}{\second}}$  &
	$>$  \SI{900}{\second}\tablefootnote{This figure roughly corresponds to 100 trips of a duration
	     between \SI{0.1}{\second} and \SI{3}{\second} per day.}\\
& $\text{MTBF}_{\tau > \, \SI{3}{\second}}$ &
	$>$  \SI{250}{\hour}\tablefootnote{With a reference reactor cycle of 90 days,
			this figure approximately expresses a \TFaultTolerance{tolerance} of 10 beam trips 
			of a duration exceeding \SI{3}{\second} per reactor cycle.}\\\hline
%
% Reactor cycle~\cite{R-SCKCEN-2020-30233162}\adrianreply{This is not clear without further explanation.}{vdflorio}{%
% 	We adopted the hopefully more clear ``reactor cycle'', with a reference to %
% 	``Reactor Scope  justification'', v.4, for further explanations. %
% 	} & & 3 months~\cite{R-SCKCEN-2020-35576097,R-SCKCEN-2019-30233162}\\\hline
%
%Corresponding MTBF   & $\tau>\hbox{\SI{3}{\second}}$                              & 250 hours&\\\hline
%
% \multirow{2}{1.5941598in}{\centering Targeted RMS beam diameter}
%  & & \multirow{2}{2.5802598in}{\SI{9}{\milli\meter}, with an RMS deviation
	% 				      not exceeding \SI{1}{\milli\meter} RMS~\protect\cite{D-CDT-2.4}}&\\
	%	& & &\\\hline
%
	Time structure                                                      & & CW with PWM at \SI{250}{\hertz}
										maximum%~\cite{R-SCKCEN-2019-30233162}
	%  \mycomment{vdflorio}{In ``MYRRHA reactor scope justification'' it is mentioned: ``Time structure: CW with %
	%  	1--50 ms beam holes at 10--250 Hz.\textCR %
	%  	Do I have to revise anything here?%
	%		}
										\\\hline
%
\TBeamDutyFactor{Beam duty factor} & & \numrange{d-4}{1}\\\hline
	%  \mycomment{vdflorio}{The following quote is from ``Reactor Proton Beam Requirements'':  %
	%  ``The beam delivery may be interrupted for 0.5 ms at 250 Hz. %
	%  Comment: Thus the duty factor F = ton / (ton + toff) =  3.5 ms / 4 ms = 0.875.''\textCR %
	%  This seems correct to me. Shall I correct the figure here?} \\\hline
%
%   Duty cycle on target & Reactor  & Up to \SI{100}{\percent}, with ``holes''%
%   \mycomment{vdflorio}{In ``Reactor Proton Beam Requirements'' it is mentioned:\textCR %
%   	``It is understood, that the duty cycle of the beam to reactor will decrease proportional %
%   	  to the requested average current (e.g. 2 mA average beam current results in 2 ms out of 4ms)''. \textCR %
%   	  Do we have to revise / correct anything here?}\\\hline
%
Minimal pulse duration                                              & &
			$\ge \SI{100}{\micro\second}$\\\hline%~\cite{R-SCKCEN-2020-36777107}\\\hline
%  Rise and fall time                                                  & &
%  	$\le \SI{10}{\micro\second}$\mycomment{vdflorio}{%
%  		In ``Reactor Proton Beam Requirements'' it is mentioned:\textCR%
%  		``The beam current transition from beam-on to beam-off shall be sharp with less than 10 us. \textCR%
%  		The beam current transition from beam-off to beam-on shall be sharp with less than 10 us.}\\\hline
%
% \multirow{2}{1.5941598in}{\centering Beam entry into the reactor} & & \multirow{2}{2.5802598in}{Vertically from above}\\
% 	& & \\\hline
 %
%  \multirow{2}{1.5941in}{\centering
%  	Beam stability on target} & Energy & \SI{1}{\percent}\\
%  				  & Current & \SI{2}{\percent}
%  				  \mycomment{vdflorio}{%
%  					  In ``Beam Reactor Scope Justification'', it is mentioned:\textCR%
%  					  ``Beam stability on the window: energy +-1\%, current +-2\%, Position and Size +-10\%''. %
%  					  This is deleted, and replaced by: ``Beam position stability: TBD.''\textCR%
%  					  Indeed, in the new version of the above document there is no mention of beam stability. %
%  					  Shall we remove this entry?%
%  				  } \\\hline
%				  & Beam duty cycle on reactor & Up to \SI{100}{\percent}, with ``holes''\\\hline
%                                  & Position \& diameter: \SI{10}{\percent}\\\hline
\end{tabular}
\end{center}
\caption[MYRRHA accelerator main beam requirements.]{MYRRHA accelerator main beam requirements.
	%  \adrianreply{These requirements need to be based on input by the user and approved accordingly. %
	%  		Please include the reference.}{vdflorio}{We have mentioned the references of the above values.}
	 $\tau$ represents here the beam trip duration. \TMTBF{MTBF}~\cite{R-SCKCEN-2020-39795792} here should be intended as ``beam MTBF'' and refer
	 to the linac actually delivering its nominal beam.}
%  \mycomment{jbelmans}{The document from the user has never been approved. Please discuss this with Dirk}
\label{tab:MYRRHA-beam-requirements}
\end{table}


\ifdefined\EnableWarningsAndAlertsStillPending
\begin{Alert}{}
I find this to be too strong a statement. From a \emph{hardware\/} fault tolerance point of you, the linac
is designed to tolerate some faults. No provision is foreseen though to tolerate software faults---see
for instance~\cite{B-2009-DeFV-IGI}. Later on in the TDR it is mentioned that the control system is a key
responsible to the emerging availability, reliability, and safety. As a consequence,
in my opinion provisions to tolerate software faults should also be considered.
\end{Alert}
\fi

An accelerator reference design has been drafted during the FP7 \MAX{} project, focusing on the
{\MYRRHA} context~\cite{D-MAX-1.4}.
%, ``the Reference ADS Accelerator'' (see Fig.~\ref{fig:overview:concept}).
This reference accelerator is mainly composed by a pair of \SI{17}{\MeV} \TInjector{injector}s followed by
a \SIrange{17}{600}{\MeV} fully \TSCLinac{superconducting linac} with independently-phased cavities.

%{{{ FORMERLY AT THE END OF A03
%\FloatBarrier\subsection[Recent Design Evolution]{Recent Design Evolution}
Compared to this project, most design differences concern the \TInjector{injector} segment.

The initial design of the \TInjector{injector} comprised a number of normal conducting (NC) cavities followed by a
number of superconducting (SC) cavities, and was to be powered by a \SI{352}{\MHz} radio frequency (\TRF{RF}) system.

At \SI{352}{\MHz} and CW operation, the only appropriate radio frequency quadrupole ({\LinacRFQ})
structure is a 4-vane {\LinacRFQ}. But this cavity type is extremely sensitive against mechanical
tolerances and mode mixing between dipole and quadrupole modes.  Therefore, during the first period of
the \MAX{} project, it has been decided to change the \TInjector{injector} frequency from \SI{352.2}{\MHz}
to {\TWinIiLEfreq} while keeping the final energy at \SI{16.6}{\MeV}. At this lower frequency, it is
possible to use a 4-rod {\LinacRFQ} technology~\cite{D-MAX-2.4}.
% Dirk suggested to mention the advantages of 4-rod RFQ cavities, and Franck kindly supplied the following sentence:
The main advantages of the 4-rod type {\LinacRFQ} cavity are compact radial size, tuning possibilities, easy
access for repair and maintenance, small \TSensitivity{sensitivity} against mechanical tolerances, and low \TCostsCon{construction costs}.

% The following sentence:
%
% In order to limit the {\LinacRFQ} length to a value close to \SI{4}{\meter}, which allows a
% single cavity to be employed, the input and output energies of the {\LinacRFQ} have been reduced from \SI{0.05}{\MeV}
% and \SI{3}{\MeV} to \SI{0.03}{\MeV} and \SI{1.5}{\MeV}, respectively.
%
% was replaced by Dirk as follows (cf. e-mail 20201112T1902, "Sentence I need to revise"):
In order to allow for a non-critical high voltage on the \TSource{ion source} platform, the preferred
input \TBeamEnergy{beam energy} of the {\LinacRFQ} was defined as \SI{30}{\keV}. Its output energy was matched to the
low energy capability of a {\TWinIiLEfreq} CH-type accelerating cavity, namely \SI{1.5}{\MeV}. These
choices, together with a conservative level of the intervane voltage, resulted in a \SI{4}{\meter}
long RFQ structure. This is compatible with a single tank realisation.


As the \MYRRHA{} facility will be operated with \TBeamIntensity{beam intensities} up to \SI{4}{\mA}, this is the design specification
taken into account.

Consequently, the inter-vane voltage has dropped from \SI{65}{\kV} to \SI{40}{\kV}, allowing to enlarge the
minimum gap between electrodes and further reduce the RF power consumption per unit length.
%%\ulrich{The RFQ is installed $\rightarrow$ no more ``could'' , real values!}
%%Correct: "could" removed

%  \begin{Alert}{}
%  	Fr\'ed\'eric:
%  	do we have an idea about the saving in power consumption ?
%  	Jorik: As $P$ is proportional to $V^2$, the saving can be calculated.
%		Possibly from 300 kW to 100 kW, but remove.
%  \end{Alert}

After several intermediate designs, a new \MYRRHA{} \TInjector{injector} layout has been
settled, with very robust \TBeamDynamics{beam dynamics}, low emittance growth rates and a smooth output particle
distribution~\cite{P-LINAC14-Mader}. Compared to the previous design, sufficient drift space also provides
adequate room for diagnostic elements.

Behind the 4-rod {\LinacRFQ} and a pair of two-gap quarter wave rebunchers ({\LinacQWR}), the \SI{1.5}{\MeV}
protons are matched into the \TRTch{CH cavity section}. A focusing triplet between the rebunchers ensures
an optimal transversal matching into the doublet lattice.

The reference design then evolved, with the SC CH cavities being replaced by
\TRTch{normal conducting (NC) CH cavities}. These cavities have a constant phase
profile and do not exceed thermal losses of \SI{25}{\kW\per\meter}. The evolution is mainly justified by a lower
technological risk in terms of design, operation (no cryogenics) and manufacturing. The impact is that
the \TInjector{injector} is slightly longer, which is still considered to be acceptable.

%% Dirk: "drop?" % With reference to the design of this evolution,
%% Dirk: "drop?" % to date cavities have been designed, though the parts around it (such as magnets, supports, etc.) have not yet.
%The engineering design of this evolution is currently in progress.

%  \begin{Alert}{}
%  	Fr\'ed\'eric:
%  	do we mention here that cavities are powered by RF SSA ?
%	Jorik: no.
%  \end{Alert}

%END OF SECTION FORMERLY AT THE END OF A03
%}}}

% THIS TEXT DELETED AS PER REQUEST BY JORIK, 20200602
%   Along the linac, various standard beam diagnostic tools are considered:
%   
%   \begin{itemize}[noitemsep]
%     \item The most common (present all along the linac): \hyperref[sect:beam-inst:bpm]{beam position monitors} (BPM), phase
%   	probes, beam loss monitors;
%   
%     \item Beam transverse profilers, bunch length monitors (which are used in the
%   	non-accelerating sections);
%   
%     \item Emittance meters and Faraday cups (which can be used in the low and medium-energy
%   	sections such as LEBT and MEBT1).
%   \end{itemize}
%   
%   Please refer to Sect.~\ref{sect:beam-inst} for more information on this.
%   
%   The beam energy is measured by the time of flight (TOF) between two beam instruments (typically two BPMs)
%   located in non-accelerating sections (the MEBT3 and the HEBT).
%   
%   Specific information on the beam current can be obtained in a continuous non-destructive way by Beam
%   Current Transformers (ACCT or DCCT). At low energy the beam current can also be obtained directly in an
%   interceptive way by water-cooled Faraday cups. Such a Faraday cup is foreseen in the front-end section
%   of the accelerator.

\begin{figure}[h]
\centering
  %\includegraphics[width=6.1728in,height=8.8799in]{MYRRHATDRPartA-img/MYRRHATDRPartA-img012.jpg}
  %\includegraphics[height=0.5\textheight,keepaspectratio]{MYRRHATDRPartA-img/MYRRHATDRPartA-img012.png}
  \includegraphics[width=\textwidth,keepaspectratio]{MYRRHATDRPartA-img/MYRRHATDRPartA-img012.png}
\caption{MYRRHA accelerator conceptual design.}
\label{fig:overview:concept}
\end{figure}


Within the FP7 \MAX{} project, extensive \TBeamDynamics{beam dynamic} \TSimulation{simulations}~\cite{D-MAX-1.4} and code benchmarking
were performed~\cite{D-MAX-1.2}. These \TSimulation{simulations} and benchmarks showed converging results. For the
end-to-end \TSimulation{simulations}, \CodesTraceWin~\cite{P-IPAC15-Uriot} was considered as the reference code. The latter
was designed at IRFU/CEA and used for a large number of similar projects---including the ESS linac
and SPIRAL2.
% This guarantees the reliability of the code.
% vdflorio: I do not agree
More recently, \TBeamDynamics{beam dynamic} \TSimulation{simulations} with a focus on the \MINERVA{} phase have been carried out
in the framework of the \MYRTE{} Project~\cite{D-MYRTE-2.6}.
% see e-mail from Dahmane, Fri 5/29, 2:25 PM, subject: "Nuova domanda :-)"

%  \begin{Alert}{}
%  Should I mention here the more recent MYRTE \T\TSimulation{Simulation}{\T\TSimulation{Simulation}{simulation}s}~\cite{D-MYRTE-2.6}? Also, should I refer here
%  more specifically to MINERVA? Jorik: ``yes!''.
%  \end{Alert}

\FloatBarrier\subsection[Accelerator General Performance]{Accelerator General Performance}
The MYRRHA accelerator's interface with the reactor will be a window. In this design, high-proton
energies
%%\ulrich{higher than what?}
%% Corrected (higher => high-)
are necessary in order to limit the energy deposited in the window. The MEGAPIE~\cite{O-MEGAPIE-20}
return on experience clearly shows that a window design can be used at \SI{600}{\MeV}.

%  \begin{Alert}{}
%  What about the 100-MeV case? Is it \emph{a fortiori\/} so? If so, I would mention this.
%  Jorik: ``no window on MINERVA.''
%  \end{Alert}

As mentioned in Table~\ref{tab:MYRRHA-beam-requirements}, reference value for the nominal instantaneous
\TBeamCurrent{beam current} is \SI{4}{\mA}~\cite{R-SCKCEN-2020-36777107,R-SCKCEN-2020-30233162}---in accordance with
core design, fuel cycle and the choice of \Keff.
% \adrianreply{where is this information from? what is the reference?}{vdflorio}{My apologies, %
% 	     I should have added citations here. This has now been corrected.}

The accelerator is basically of the continuous wave (CW) type, but with beam `holes'. Said beam holes
are necessary for the on-line measurement of the reactor's sub criticality level through dynamic
measurements, as safely operating the ADS requires knowledge of the sub-criticality level at all
times. During \TCommissioning{commissioning} and start-up, the duty cycle of the beam can be reduced.
%   \adrianreply{where is this information from? what is the reference?}{jbelmans}{%
%   	     This is the start-up of the accelerator thus it is obvious that the duty cycle beam can be reduced.}
%   
%   \mycomment{vdflorio}{I suppose Adrian is asking for references to the other parts of the above sentence: %
%   		     CW type; with beam "holes"; rationale thereof.}

\FloatBarrier\subsection[Reliability and Availability]{Reliability and Availability}
The \TReliability{reliability}~\cite{R-SCKCEN-2020-39795792} requirements are essentially related to the
number of allowable \TBeamInterruptions{beam trips}.  Frequently repeated \TBeamInterruptions{beam trips}
will significantly decrease the ADS plant \TAvailability{availability}, and \TBeamInterruptions{beam
trips} longer than \SI{3}{\second} are considered to result in a plant shutdown.

\ifdefined\EnableWarningsAndAlertsStillPending
\mycomment{vdflorio}{Reliability is not exclusively affected
by beam trips, therefore linking reliability requirements exclusively to beam trip events represents an optimistic 
assumption---see my paper on assumption failures~\cite{DBLP:journals/corr/Florio16a}.}
\fi

%  \begin{Alert}{}
%  Fr\'ed\'eric: do we have to highlight that this shutdown is necessary because of the reactor recovery/restart
%  procedure which is mandatory to happen when trips are longer than 3s, or it is obvious for the reader ?
%  \end{Alert}

%  The shape of the beam footprint is to be a disk, and the dimensions of the footprint (diameter) must be
%  stable in a range of {\textpm}\SI{10}{\percent}.
%  \ulrich{again: link to reacor requirement document and do not repeat numbers.}
%  % Targeted RMS beam diameter and allowed deviation can be found in Table~\ref{tab:MYRRHA-beam-requirements}.
%% Removed by request of Jorik

Finally, it is also worthwhile to point out that the accelerator maintenance policy influences the
\TAvailability{availability}~\cite{R-SCKCEN-2020-39795792} of the ADS. In order to be coherent with the existing
maintenance policy on Nuclear Power Plants, it is suitable to avoid the short maintenance periods and to
base the maintenance policy on longer periods: a three months' maintenance period once per year appears
to be a reasonable value. However, for the MYRRHA ADS, given the rather short cycle length (3 months)
a more lenient policy can be chosen.

Once more we refer the reader to Table~\ref{tab:MYRRHA-beam-requirements} for a
summary of accelerator requirements.

%  \begin{table}[h]
%  \begin{center}
%  \tablefirsthead{}
%  \tablehead{}
%  \tabletail{}
%  \tablelasttail{}
%  \begin{supertabular}{|m{2.48026in}|m{2.48026in}|}
%  \hline
%  { Proton beam current}            & { \SI{4}{\mA} @ \SI{100}{\percent} duty cycle}\\\hline
%  { Proton energy}                  & { 600 MeV}\\\hline
%  { Number of beam trips ($>$ 3 s)} & { Less than 10 per 3 months operation}\\\hline
%  { Beam entry into the reactor}    & { Vertically from above}\\\hline
%  { Beam stability on target}       & { Energy: \SI{1}{\percent}}\\\hline
%                                    & { Current: \SI{2}{\percent}}\\\hhline{~-}
%                                    & { Position \& diameter: \SI{10}{\percent}}\\\hhline{~-}
%  \end{supertabular}
%  \end{center}
%  \caption{Proton beam delivery specifications.}
%  \label{tab:overview:req-overview}
%  \end{table}
%  
%  \begin{Alert}{}
%  Is Table~\ref{tab:overview:req-overview} also valid for MINERVA?
%  \end{Alert}

\FloatBarrier\subsection[Operation]{Operation}
The MYRRHA plant is \TOptimisation{optimised} for operating at nominal power, though it has also to be capable to operate for
long periods at partial load without significant penalties. Moreover, during the plant \TCommissioning{commissioning} and
after a refuelling in a shutdown state, operations at very low average power ({\textless}\SI{3}{\percent}) are required.

The foreseen scenario is to vary the average \TBeamCurrent{beam current} by modulating the duty cycle and keeping the
peak current constant.

%  \begin{Alert}{}
%  Is there any MINERVA-specific consideration that I should add here?
%  \end{Alert}

\FloatBarrier
%
% EoF TDR-A04-AcceleratorOverview.tex
%
