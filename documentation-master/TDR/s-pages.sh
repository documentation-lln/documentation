#!/usr/bin/env bash

mkdir chapters 2> /dev/null

pdftk "MINERVA-100MeV-ACCELERATOR-TDR.pdf" cat 21-23 output chapters/TDR-Chapter01-"MYRRHA Project Overview".pdf
pdftk "MINERVA-100MeV-ACCELERATOR-TDR.pdf" cat 23-28 output chapters/TDR-Chapter02-"MYRRHA Project History".pdf
pdftk "MINERVA-100MeV-ACCELERATOR-TDR.pdf" cat 28-31 output chapters/TDR-Chapter03-"The MYRRHA 600 MeV Accelerator Overview".pdf
pdftk "MINERVA-100MeV-ACCELERATOR-TDR.pdf" cat 31-32 output chapters/TDR-Chapter04-"The Phases of the MYRRHA Project".pdf
pdftk "MINERVA-100MeV-ACCELERATOR-TDR.pdf" cat 32-33 output chapters/TDR-Chapter05-"The MINERVA 100 MeV Accelerator Overview".pdf
pdftk "MINERVA-100MeV-ACCELERATOR-TDR.pdf" cat 33-39 output chapters/TDR-Chapter06-"Scope of This Document".pdf
pdftk "MINERVA-100MeV-ACCELERATOR-TDR.pdf" cat 39-53 output chapters/TDR-Chapter07-"Injector".pdf
pdftk "MINERVA-100MeV-ACCELERATOR-TDR.pdf" cat 53-59 output chapters/TDR-Chapter08-"Medium Energy Beam Transport Line 3".pdf
pdftk "MINERVA-100MeV-ACCELERATOR-TDR.pdf" cat 59-69 output chapters/TDR-Chapter09-"Superconducting Linac".pdf
pdftk "MINERVA-100MeV-ACCELERATOR-TDR.pdf" cat 69-71 output chapters/TDR-Chapter10-"High Energy Beam Transport Lines".pdf
pdftk "MINERVA-100MeV-ACCELERATOR-TDR.pdf" cat 71-106 output chapters/TDR-Chapter11-"Room-Temperature Cavities".pdf
pdftk "MINERVA-100MeV-ACCELERATOR-TDR.pdf" cat 106-136 output chapters/TDR-Chapter12-"Superconducting Cavities  Single-Spoke".pdf
pdftk "MINERVA-100MeV-ACCELERATOR-TDR.pdf" cat 136-175 output chapters/TDR-Chapter13-"Radio-Frequency System".pdf
pdftk "MINERVA-100MeV-ACCELERATOR-TDR.pdf" cat 175-179 output chapters/TDR-Chapter14-"Cryogenic System".pdf
pdftk "MINERVA-100MeV-ACCELERATOR-TDR.pdf" cat 179-192 output chapters/TDR-Chapter15-"Beam Instrumentation".pdf
pdftk "MINERVA-100MeV-ACCELERATOR-TDR.pdf" cat 192-202 output chapters/TDR-Chapter16-"Deflecting and Focusing Devices".pdf
pdftk "MINERVA-100MeV-ACCELERATOR-TDR.pdf" cat 202-208 output chapters/TDR-Chapter17-"Vacuum System".pdf
pdftk "MINERVA-100MeV-ACCELERATOR-TDR.pdf" cat 208-215 output chapters/TDR-Chapter18-"Control System".pdf
pdftk "MINERVA-100MeV-ACCELERATOR-TDR.pdf" cat 215-236 output chapters/TDR-Appendices.pdf
pages=`pdftk "MINERVA-100MeV-ACCELERATOR-TDR.pdf" dump_data | grep NumberOfPages| awk '{print $2}'`
pdftk "MINERVA-100MeV-ACCELERATOR-TDR.pdf" cat 237-${pages} output chapters/TDR-Bibliography.pdf
printf '[32mNo errors: processing concluded normally.[0m
'
