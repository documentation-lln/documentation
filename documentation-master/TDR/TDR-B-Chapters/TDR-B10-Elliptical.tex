%
% TDR-B10-Elliptical.tex
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section[Superconducting Cavities: Elliptical]{Superconducting Cavities: Elliptical}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\label{sect:el-cavities}
\label{sect:elliptical-spoke-cavities}

\begin{Alert}{}
On page~\pageref{MINERVAvsMYRRHA-elliptical cryomodules} it is mentioned:
``While the series of elliptical cryomodules is only relevant for phase 2 of the MYRRHA project, the
prototyping activities for a MYRRHA-specific cryomodule (especially taking into account the CW aspects)
have to be foreseen during MINERVA (phase 1). Presently this prototyping project is not defined yet\dots''
Because of this I assumed that elliptical cavities shall not be included in the MINERVA version of the TDR.
If this is confirmed, the whole section shall be temporarily ``disabled'' (the text will still be available
though will not be made part of the TDR) by removing the definition``EnableBeyondMINERVA'' in the main TDR
module, \verb"MINERVA-100MeV-ACCELERATOR-TDR.tex".
\end{Alert}

\FloatBarrier\subsection[Introduction]{Introduction}\index{cavities!elliptical}
The injector(s)\index{linac!injector} and the linac comprise accelerating cavities, as summarised in Table~\ref{tab:ellc:cavity-types}.

\begin{table}
\begin{center}
\tablefirsthead{}
\tablehead{}
\tabletail{}
\tablelasttail{}
\begin{supertabular}{m{2.7719598in}m{3.3629599in}}
{ Injector(s)} &
{ \TRTch{15 NC CH cavities} per injector}\\
{ linac, section \#1} &
{ \TSpokeCavities{58 NC single-spoke cavities}}\\
{ linac, section \#2 (two possible alternate technologies)} &
{ SC double-spoke cavities, number TBD\footnote{The transition energies and number/type of cryomodules in each section will be
optimised in view of experimental results.}; or possibly: }

{ 28 SC medium-beta elliptical cavities}\\
{ linac, section \#3} &
{ 60 SC high-beta elliptical cavities}\\
\end{supertabular}
\end{center}
\caption{Accelerator cavity types.}
\label{tab:ellc:cavity-types}
\end{table}
This chapter discusses the elliptical cavities\index{cavities!elliptical} and their cryomodules\index{cavities!elliptical!cryomodules}.

For the \TRTch{CH cavities}, see Sect.~\ref{sect:rt-cavities};
for the \TSpokeCavities{single-spoke cavities}, see Sect.~\ref{sect:ss-cavities}%
\ifdefined\EnableBeyondMINERVA
; for the double-spoke cavities, see Sect.~\ref{sect:double-spoke-cavities}.
\else
.
\fi

\FloatBarrier\subsection[Elliptical Cavities]{Elliptical Cavities}\index{cavities!elliptical}
In RF cavities, which usually have a cylindrical geometry, the basic pillbox modes are used to generate
a longitudinal electric field along the beam axis.

In TM modes\footnote{The nomenclature of TM modes is characterised by three indices $m$, $n$ and $p$
	(TM${}_{mnp}$) defined as follows: $m$ is the number of zeros of Ez($\Phi$) in the range of 0${\leq}\Phi
	${\textless}$\pi$ ($m$ = 0, 1, 2, {\dots}); $n$ is the number of zeros of Ez(r) in the range of 0
	{\textless} r${\leq}$Rc ($n$ = 1, 2, 3, {\dots}); and $p$ is the number of half-period variations of
	Ez(z) ($p$ = 0, 1, 2, {\dots}) [40].},
for example in the case of elliptical pillbox cavities, the inherent longitudinal electric field component
can be used directly for particle acceleration.

Elliptical pillbox cavities, which are operated in the TM\textsubscript{010} mode, are typically used
with frequencies between \SI{350}{\mega\hertz} and 3 GHz for the acceleration of particles with highly relativistic
velocities like protons with $\beta ${\textgreater}0,5 ([39], p.~175 ff.). The elliptical variant of
the pillbox shape provides good \TMultipacting{multipactor effect} suppression and very low electric and magnetic peak
fields, which allows high acceleration gradients.

Often elliptical cavities are used in multi-cell structures operated in $\pi$-mode, as depicted in
Fig.~\ref{fig:ellc:elec-field} and Fig.~\ref{fig:ellc:magn-field}.

\begin{figure}[h]%[tbp]
    \centering
    \begin{subfigure}[t]{0.5\textwidth}
        \centering
		\includegraphics[width=3.1563in,height=1.4898in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img195.png}
            \caption{Electric field in a 5-cell elliptical pillbox cavity.}
        \label{fig:ellc:elec-field}
    \end{subfigure}%
   ~
    \begin{subfigure}[t]{0.5\textwidth}
        \centering
		\includegraphics[width=2.9898in,height=1.5626in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img196.png}
            \caption{Magnetic field in a 5-cell elliptical pillbox cavity.}
        \label{fig:ellc:magn-field}
    \end{subfigure}
 \caption[Electric and magnetic fields in a 5-cell elliptical pillbox cavity.]{}
 \label{fig:ellc:fields}
\end{figure}


\FloatBarrier\subsection[Cryomodules]{Cryomodules}\index{cavities!elliptical!cryomodules}
The nominal layout of the MYRRHA linac above \SI{184}{\MeV} ($\beta= 0.549$) and up to the final energy of 600
MeV ($\beta = 0.792$) features 60 elliptical superconducting cavities with 5 cells each and a geometrical
$\beta$ of 0.65. These cavities are grouped per 4 in cryomodules that are approximately 6 m long. As in
the spoke linac, transverse focusing is obtained by normal conducting quadrupole\index{magnets!quadrupole}
doublets installed in the \TWarmSection{warm sections} between the cryomodules.

\label{MINERVAvsMYRRHA-elliptical cryomodules}
While the series of elliptical cryomodules is only relevant for phase 2 of the MYRRHA project, the
prototyping activities for a MYRRHA-specific cryomodule (especially taking into account the CW aspects)
have to be foreseen during MINERVA (phase 1). Presently this prototyping project is not defined yet, but 2 ongoing
reference designs that may serve as a basis have been identified:

\begin{itemize}[noitemsep]
  \item The medium $\beta $ (0.67) cryomodule for \index{Projects!ESS}ESS. It is being developed by a collaboration
	CEA-IJCLab\footnote{Laboratoire de Physique des 2 Infinis Ir\`ene Joliot Curie, a division of IN2P3, formerly
	106 known as IPNO.}-ESS, and it is largely based on the JLAB/SNS approach. Its essential feature is the
	space frame based layout with tie rods for supporting the cavities.

  \item The $\beta=1$ cryomodule being developed by CERN, originally in the framework of the
	SPL\index{Projects!SPL} project. Its essential and innovative feature is the support of the
	cavities by the RF power couplers.
\end{itemize}

Both designs are optimised for a pulsed operation, so for either choice a MYRRHA-specific CW prototype
should be foreseen. A decision on this specific prototyping may be expected end of 2018, in agreement
with the phase 2 calendar.

\FloatBarrier
%
% EOF TDR-B10-Elliptical.tex
%
