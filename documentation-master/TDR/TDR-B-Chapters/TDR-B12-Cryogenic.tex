%
% TDR-B12-Cryogenic.tex
%
% Version 2, 2020-05-28
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section[Cryogenic System]{Cryogenic System}\label{sect:cryogen}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection[Introduction]{Introduction}

A \TCryoSystem{cryogenic plant} is needed to ensure the proper refrigeration of the \TCryoModules{cryomodule}'s
multiple crucial components \emph{i.e.}\ the cavities themselves, the magnetic \TShielding{shielding},
the thermal \TShielding{shielding}, the RF power couplers, \ldots{} Such a \TCryoSystem{cryogenic plant}
has been the subject of several studies, at first in the context of the larger \MYRRHA{} accelerator,
and later, however to a lesser extent in the specific case of its phase I, \MINERVA{}, as described above.

This section synthesises all available information on the \MINERVA{} \TCryoSystem{cryogenic plant},
taking the up-to-date numbers, requirements, and constraints that resulted from both these previously
mentioned studies, as well as internal interfaces and discussions over the last few years. As the
work on this subject is in progress, it also highlights what work is yet to be done on the \MINERVA{}
\TCryoSystem{cryogenic plant}.


\subsection[The MINERVA Cryoplant]{The MINERVA Cryoplant}

\subsubsection{Accelerator requirements and resulting constraints}

The main design constraints of the \MINERVA{} cryoplant are detailed in this section, and summarised at
its end as bullet points.

The final goal of \MYRRHA{} necessitates a highly reliable accelerator---please
refer to Sect.~\ref{sect:myrrha:design} for detailed information about reliability
\hyperref[tab:MYRRHA-beam-requirements]{requirements} and corresponding \TMTBF{MTBF} values.

%  as already stated, the set benchmark
%  for reliability is \textbf{less than 10 beam trips longer than \SI{3}{\second} per operating period of 3
%  months}. This translates into an overall Mean Time Between Failures (MTBF) larger than \SI{250}{\hour},
%  applying to all accelerator equipment that cause a beam trip upon failure.

In order to help fulfil this stringent benchmark, a so-called ``\TFaultTolerance{fault tolerance}
mode'' is going to be implemented as an accelerator operation mode. Essentially, a failure that
implies the improper operation of a \TCryoModules{cryomodule} will lead to the depowering of such
\TCryoModules{cryomodule}'s cavities, its accelerating gradient being \TFaultCompensation{compensated}
by adjacent \TCryoModules{cryomodules} and \TFaultPrevention{avoiding} a \TBeamInterruptions{beam
trip}\footnote{Similar reliability requirements led to a conceptually similar fault-tolerance strategy
	to compensate for lost cavities in the RF system (see Sect.~\ref{ssect:RFsys:general-requirements}
	and Annex~\ref{bkm:Ref497895504}).}.
As such, the cavities of \MINERVA{} are run at very conservative
accelerating gradients, with a margin large enough to tolerate adjacent \TCryoModules{cryomodule} faults%
\ifdefined\EnableBeyondMINERVA (see Sect.~\ref{ssect:RF-FT})%
\fi
. The cryoplant and its distribution must be able to handle this shift of refrigeration power balance
in a stable manner, without requiring inefficient electrical heating compensation in the unpowered
\TCryoModules{cryomodules}. Furthermore, a single \TCryoModules{cryomodule} should be able to be isolated,
warmed up, and later stably cooled down while the rest of the accelerator is already cold. This requirement
justifies the use of one \TCryoModules{cryomodule} per \TValveBoxes{valve box}.

\MINERVA{} is but the phase I of a larger linac: the phase II of \MYRRHA{} will have the accelerator extended to
600 MeV with the help of elliptical cavities
\ifdefined\EnableBeyondMINERVA
(see Sect.~\ref{sect:el-cavities})%
\fi
, which are advantageous to be cooled to \SI{2}{\kelvin}. The choice for spoke cavity operation at the same
temperature for \MINERVA{} was made with the extension to phase II in mind, and due to similar overall
power consumption but significant benefits in terms of microphonics. The current, roughly estimated
refrigeration capacity for \MINERVA{} amounts to approximately \SI{4}{\kilo\watt}~@~\SI{4.5}{\kelvin},
and the expansion for \MYRRHA{} phase II is estimated to raise this capacity to approximately
\SI{16}{\kilo\watt}~@~\SI{4.5}{\kelvin}. These estimations include overcapacity factors and are only
meant to give an idea of the order of magnitude of the needed expansion.

As the \MINERVA{} tunnel will be close to ground-level, it is possible to incorporate the \SI{2}{\kelvin}
cold compressor unit into the main cold box, as it was done at ESS, with all the advantages that this
implies for dynamic range and overall efficiency, among others.

We arrive then at the following existing constraints as of the elaboration of this document:

\begin{itemize}
  \item The dynamic range of the cryoplant required for \MINERVA{} is significant due to continuous-wave
	operation: dynamic losses are approximately \SI{58}{\percent} of the total load, requiring for
	a dynamic range of approximately $2.4$. In this sense, an integrated cold box solution with the
	\SI{4.5}{\kelvin} and \SI{2}{\kelvin} is seen as ideal, although this requires expert confirmation;

  \item The cryoplant and its distribution must be able to handle the shift of refrigeration power balance
	caused by \TFaultTolerance{fault tolerance} operation in a stable manner, minimising or eliminating
	inefficient electrical heating compensation;

  \item A single \TCryoModules{cryomodule} should be able to be isolated, warmed up, and cooled back down during cryoplant
	operation;

  \item The cryoplant is possible to be dimensioned with the possibility to expand to higher refrigeration
	capacity, for the additional \TCryoModules{cryomodules} of \MYRRHA{} phase II---this is up for discussion.
\end{itemize}


\subsubsection{Heat load distribution}

The \THeatLoadDist{distribution of the heat loads} between the multiple refrigeration stages of a cryoplant is of
extreme importance for its dimensioning. The assumptions in Table~\ref{tab:hlassump} are made
to estimate the \THeatLoadDist{heat load distribution}:

\begin{table}[h]
	\begin{center}
		\begin{tabular}{|lr|}
			\hline %\midrule
			Accelerator & 32 \TCryoModules{cryomodules} \\
			& 32 \TValveBoxes{valve boxes} \\
			& $\dot{Q}_{\SI{2}{\kelvin}} = \mbox{\SIrange{1}{5}{\watt \per \meter}}$~\tablefootnote{This estimate
			comes from statistics from several different cryogenically cooled accelerators.
			The worst case scenario of \SI{5}{\watt \per \meter} is taken, but is subject to discussion.} \\
			& $L_{CM} = \SI{2.2}{\meter}$ \\
			\hline %\midrule
			Cavity & ${Q_0}{_{est}} = \num{2e9}$ \\
			& $\dot{Q}_{\SI{2}{\kelvin}} = \SI{9.35}{\watt}$ per cavity \\
			& $62$ cavities \\
			\hline %\midrule
			RF coupler & $\dot{Q}_{\SI{2}{\kelvin}} = \SI{1}{\watt}$ per coupler \\
			& $\dot{Q}_{\SI{10}{\kelvin}} = \SI{3}{\watt}$ per coupler \\
			& $\dot{Q}_{\SI{60}{\kelvin}} = \SI{9}{\watt}$ per coupler \\
			& $2$ couplers per \TCryoModules{cryomodule} \\
			\hline %\midrule
			Thermal shield &  $\dot{Q}_{\SI{40}{\kelvin}} = \SI{85}{\watt}$ per shield \\
			& $1$ shield per \TCryoModules{cryomodule} \\
			\hline %\midrule
		\end{tabular}
	\end{center}
\caption[Assumptions and data used to estimate the \THeatLoad{heat loads} in Table~\protect\ref{tab:hldistro}]%
	{The assumptions and data used to estimate the \THeatLoad{heat loads} at different temperature
	 levels in Table~\protect\ref{tab:hldistro}.}
	\label{tab:hlassump}
\end{table}

The assumptions in Table~\ref{tab:hlassump} result in the following up-to-date figures for the
refrigeration power requirements at different temperature levels in Table~\ref{tab:hldistro}. It is
important to note that the \SI{10}{\kelvin} and \SI{60}{\kelvin} circuits for the RF coupler will be
cooled by the \SI{4.5}{\kelvin} and \SI{40}{\kelvin} refrigeration stages, respectively.

\begin{table}[h]
	\begin{center}
		\begin{tabular}{|lrrrr|}
			\hline %\midrule
			Component & $T_i$ & Heat load$_{T_i}$ & Carnot factor & Heat load$_{\SI{4.5}{\kelvin}}$ \\
			& K  & W         & -              & W   \\
			\hline %\midrule
			\makecell{Cavity, coupler,\\CM static} & 2  & 994       & 2.27          & 2255  \\
			\hline %\midrule
			Coupler          & 10 & 186       & 0.44          & 82 \\
			& 60 & 558       & 0.06          & 34 \\
			\hline %\midrule
			Thermal shield   & 40 & 2720     & 0.10          & 272 \\
			\hline %\midrule
			&    &           &               & 2640  \\
			& & & ($\times 1.5$ over-cap) & 3960 \\\hline
		\end{tabular}
		\end{center}
\caption[Heat load distribution estimated for the MINERVA accelerator.]%
	{The \THeatLoadDist{heat load distribution} estimated for the \MINERVA{} accelerator.}
\label{tab:hldistro}
\end{table}

It is immediately noticeable that the \SI{2}{\kelvin} stage takes the majority of the \THeatLoad{heat load}, about
\SI{85}{\percent} of the total capacity, arising from high dynamic losses due to CW SRF operation!

At the time of writing, the considered overcapacity factor is 1.5. This factor is foreseen to handle
uncertainties related to cavity performance, to boost cooldown speed, to handle the eventual installation
of additional equipment, etc. However, it is subject to discussion with new relevant information arising
at each step of the conceptual design process. This factor brings the current estimated equivalent
cryoplant refrigeration capacity to approximately \SI{3960}{\watt} @ \SI{4.5}{\kelvin}. When separated
per refrigeration stage: \SI{1490}{\watt} @ \SI{2}{\kelvin}; \SI{125}{\watt} @ \SI{4.5}{\kelvin}; and
\SI{4600}{\watt} @ \SI{40}{\kelvin}.

Very encouraging results from the first two spoke cavity pre-prototypes show that the initial estimation for
the dynamic losses at \SI{2}{\kelvin} might be too high for the desired accelerating gradients. Adopting
similar accelerating gradients to \MINERVA{}, the measured ${Q_0}$ was $\num{5e9}$, which would bring
the dynamic SRF cavity losses at \SI{2}{\kelvin} down to approximately \SI{3.8}{\watt}. This would reduce
the needed capacity at \SI{2}{\kelvin} to approximately \SI{650}{\watt}. As such, this very significant
contribution is still subject to discussion. Should these positive results be confirmed with the two
upcoming prototypes, this would lead to a reduction of the required refrigeration capacity.


\subsubsection{Process flow and P\&I diagrams}

The general process flow scheme is not yet fully determined, but it is \emph{a priori\/} expected to be
of the same general setup as the implementation for the ESS cryoplant:

\begin{itemize}
  \item Single cold box, integrating \SI{4.5}{\kelvin} and \SI{2}{\kelvin} units;
  \item $3$ room temperature compressors with a back-up high-pressure compressor and two identical
	sub-atmospheric and low-pressure compressors, in a floating pressure cycle;
  \item Non-centralised \TCryoDSystem{cryogenic distribution system} providing \SI{4.5}{\kelvin}, \SI{3}{\bar}
	monophase helium to each \TValveBoxes{valve box} of each \TCryoModules{cryomodule}, where a
	Joule-Thomson expansion aided by cold compressors back in the cold box produces saturated He II
	at \SI{2}{\kelvin}.
\end{itemize}

A simplified scheme based on these constraints is presented in Fig.~\ref{fig:simpleprocess}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.66\linewidth]{MYRRHATDRPartB-img/simpleprocess.png}
\caption[A proposed cold box flow scheme, based on the ESS cryogenic plant.]%
	{A proposed cold box flow scheme, based on the ESS \TCryoSystem{cryogenic plant}.}
	\label{fig:simpleprocess}
\end{figure}

An initial basic study on the \TCryoDSystem{cryogenic distribution system}, defining transfer line diameters, flows,
and pressure drops was carried out for the \SI{600}{\MeV} accelerator of phase II.


\subsubsection{Modes of operation}

No study on modes of operation specifically for the \MINERVA{} cryoplant has as of yet been carried out.

\subsubsection{Storage and utilities}

Initial estimations have been made for storage, with the philosophy that storage is to be a duplicate
of the amount of He process flow. The current proposed solution is a mixed storage: $3$ gas tanks of
\SI{70}{\metre \cubed} at \SI{20}{\bar} and $1$ liquid tank of \SI{10}{\metre \cubed} at \SI{1.5}{\bar}.

\ifdefined\EnableWarningsAndAlertsStillPending
\mycomment{deflorio}{The above sentence is unclear. What do you mean by "duplicate"? Is storage %
		     to be a function of the He process flow? Depending on it? Or what?}
\fi
%  Regarding utilities, there is a team at \sckcen{} (\textbf{N}uclear \textbf{F}acilitie\textbf{S}) dedicated
%  to buildings and utilities surrounding the accelerator. Several exchanges have taken place between this
%  team and ACS, which have resulted in some preliminary values for utilities, again scaling down from the
%  known values from ESS.
%  
%  \mycomment{deflorio}{"There is a team at SCK CEN" is too colloquial. Something like "With reference to utilities,
%  	preliminary values" (??) "have resulted from discussions with the SCK CEN team NFS. Said values were produced by
%  	scaling down known values from ESS3.}
%  
%  \mycomment{vdflorio}{SOMETHING WRONG HERE! Watt per Watt?!}
For electricity consumption, an overall figure of merit of \SI{250}{\watt} of electricity per \si{\watt}
of refrigeration at \SI{4.5}{\kelvin} was assumed, corresponding to an efficiency of \SI{26}{\percent}:
this is in line with the expected values for cryoplants of such capacity. This implies, with the total
refrigeration capacity, an equivalent of approximately \SI{1}{\mega \watt}. The overcapacity factor for
the installed electrical power is a hanging question at this stage, but was proposed to be as low as $1.5$
and as high as $2.0$.

A design engineering contract has recently been awarded to a consortium consisting of Tractebel (B) and
Empresarios Agrupados (ES) for the design of the buildings and utilities surrounding the accelerator
facility. Their work has started as of February 2020, and as such there is an urgent need for initial
cryoplant conceptual study inputs.


\subsection[Conclusions: What Still Needs To Be Developed?]{Conclusions: What Still Needs To Be Developed?}

The \MINERVA{} \TCryoSystem{cryoplant} already has some baseline figures, as presented previously. However,
considerable work needs to be done in order to consolidate those initial ideas and figures in order to
start thinking about a conceptual design. In no particular order, the following points need further study:

\begin{itemize}
	\item Establish constraints related to cavity cooldown (to be done in the short term on the \sckcen{} side);
	\item Determine cryoplant modes of operation;
	\item Study of the \TCryoDSystem{cryogenic distribution system}, its losses, and resulting constraints;
%             \mycomment{vdflorio}{Why "Study into" here?}
	\item Study into overcapacity factor, in order to avoid inefficient overdimensioning;
	\item Study of preliminary values for utilities and building dimensioning (needed as soon as
	      possible as one of the first outputs for building design engineer); in a first instance:
		\subitem List of interfaces;
		\subitem Room list;
		\subitem Flow diagram assisting layout.
	\item Possible refinement of expected \THeatLoad{heat loads} following prototype
		\TCryoModules{cryomodule} \TTesting{test} results.
\end{itemize}

%
% EOF TDR-B12-Cryogenic.tex
%
