This directory includes all files that constitute the sections of Part B of the TDR. More information will be progressively provided.

At the moment (May 5, 2020), the MINERVA TDR includes the following sections:

- TDR-B01-Injector.tex
- TDR-B02-MEBT3.tex
- TDR-B03-MainLinac.tex
- TDR-B06-ProTarBeamLines.tex
- TDR-B07-CHcavities.tex
- TDR-B08-SingleSpoke.tex
- TDR-B11-RF.tex
- TDR-B12-Cryogenic.tex
- TDR-B13-BeamInstr.tex
- TDR-B14-MagnetsSteerers.tex
- TDR-B15-VacuumSystem.tex
- TDR-B16-WaterCooling.tex
- TDR-B18-ControlSystem.tex

The following section have been "disabled" -- logically removed from the MINERVA TDR:

- TDR-B04-HEBT.tex
- TDR-B05-ReaWinSpaTar.tex
- TDR-B09-DoubleSpoke.tex
- TDR-B10-Elliptical.tex
- TDR-B17-ReactMonitoring.tex
- TDR-B19-SafetyReliability.tex
- TDR-B20-Building.tex
- TDR-B21-CommissDecommiss.tex

Sections B11, B13, and B15 have been rewritten from scratch; Sections B08, B12, B18 have been thoroughly revised.
The process is ongoing and should complete soon with the release of an official version of the "new TDR"!
