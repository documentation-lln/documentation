%
% TDR-B17-ReactMonitoring.tex
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section[Reactor Reactivity Monitoring]{Reactor Reactivity Monitoring}\label{sect:reactivity}\index{reactivity monitoring}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\FloatBarrier\subsection[Introduction]{Introduction}
In this section, we will describe how the reactivity monitoring methodology in ADS is applied in the
relevant operational modes of the facility: at start-up and during nominal or Hot Full Power (HFP) operation.


\begin{Alert}{}
	Shall I add a reference here? Would~\cite{J-AnnNuclEnergy-2019-128-Marie} be okay?
\end{Alert}

During start-up, the reactivity monitoring methodology will rely on the sub-critical approach procedure
and the \PutIndex{Sub-Criticality Monitoring System} (SCMS) that will use a Reference Sub-criticality
Measurement Technique (RSMT\index{Sub-Criticality Monitoring System!Reference Sub-criticality Measurement
Technique}) to measure the initial sub-critical state~\cite{D-KE1-2}. The RSMT has been validated
on the \GUINEVERE{} facility~\cite{D-KE1-4}. This section describes in detail how the RSMT will
be used in the ADS and provides a rationale for the choice.

\begin{Alert}{}
	The accent is on MYRRHA. Could it be possible to provide results specific to MINERVA here?
	For instance, are there calculations pertaining to 100-MeV that we could highlight here?
\end{Alert}

During nominal operation, the reactivity monitoring strategy will rely on the measurement
of the initial sub-critical state (measured during start-up) and the guaranteed anti-reactivity
margin~\cite{D-KE1-D1b}. Also, the use of the Monitoring Sub-criticality Measurement Technique
(MSMT)~\cite{D-KE1-2} is described as well. The associated instrumentation is provided
in~\cite{D-KE1-D6}.



As an introduction, first, the relevant sub-critical core characteristics will be recalled.
Afterwards, also a brief description of the relevant properties of the accelerator will be highlighted. All
the presented neutronic calculations were performed using the MCNP5-1.60 or \index{\TSimulation{Simulation}
codes!MCNPX}MCNPX-2.7.0 code with the \index{\TSimulation{Simulation} codes!JEFF-3.1.2}JEFF-3.1.2 nuclear data library
(and \index{\TSimulation{Simulation} codes!TENDL-2015}TENDL-2015 above \SI{20}{\MeV}) and depletion calculations with the
ALEPH 2.6.2\index{\TSimulation{Simulation} codes!ALEPH2.6.2} code.

\FloatBarrier\subsection[Sub-critical Core]{Sub-critical Core}
\label{bkm:Ref497909286}
The implementation of the reactivity monitoring methodology will be illustrated by using data from the
most recent design of the MYRRHA equilibrium sub-critical core (revision 1.6~\cite{R-SCKCEN-2014-I.448-Malambu}),
see Fig.~\ref{fig:reacmon:core-hori} and Fig.~\ref{fig:reacmon:core-vert}.

\begin{figure}[h]%[tbp]
\centering
  \includegraphics[width=4.4008in,height=2.8866in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img252.png}
\caption[Horizontal cut through a MYRRHA sub-critical core at BOC.]{Horizontal cut through a MYRRHA sub-critical core at BOC
	showing 72 fuel assemblies (FAs), 6 control rods (CRs), 6 fast spectrum in-pile sections, 6 thermal
	spectrum in-pile sections; the rest are BeO reflector assemblies and dummy assemblies filled with
	LBE (serving partially as a reflector too).}
\label{fig:reacmon:core-hori}
\end{figure}

\begin{figure}[h]%[tbp]
\centering
  \includegraphics[width=6.3008in,height=3.7071in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img253.png}
\caption[Vertical cut through a MYRRHA sub-critical core at BOC.]{Vertical cut through a MYRRHA sub-critical core at BOC.\newline
	\emph{Left}: cut showing the IPS and accelerator beamline inserted in the central assembly.
	\emph{Middle}: Cut through IPS and CR OUT (the absorber part below the active zone).
	\emph{Right}: Cut through IPS and CR IN (the absorber part at the active zone level).}
\label{fig:reacmon:core-vert}
\end{figure}

The nominal power of the MYRRHA sub-critical core is planned to be 70 MW, which will be reached at BOC
with \SI{1.7}{\mA} proton beam current on the spallation target\index{linac!spallation target}.

\begin{Alert}{}
	I think we should mention here the MINERVA nominal power and related characteristics.
\end{Alert}

There are two possible ways to keep the sub-critical core power constant during the entire cycle (FPD:
90 days) and compensate for the loss of reactivity due to burn-up:

\begin{itemize}[noitemsep]
\item By increasing the beam current on the spallation target\index{linac!spallation target};
\item By withdrawing of control rods (CRs).
\end{itemize}

To keep the operating conditions of the beam window\index{beam!window} constant, the second option
has been selected for MYRRHA. The beam current will be kept constant during a cycle. Burn-up will be
compensated by extraction of control rods.

Immediately after the switch-off of the beam at EOC, the core power will drop to about 4 MW (\emph{i.e.}  \SI{6}{\percent}
of the nominal power). After 1 month of cooling (planned outage time between two consecutive cycles)
the core power will decrease to \SI{105}{\kW}---see Fig.~\ref{fig:reacmon:decay-heat}.

\begin{figure}[h]%[tbp]
\centering
  \includegraphics[width=3.8335in,height=3.0138in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img254.png}
\caption[Decay heat of the MYRRHA core, 1 s until 3 months after EOI.]{Decay heat of the MYRRHA core, 1 s until 3 months after EOI.\newline
	(calculated using the ALEPH 2.6.2 code)\index{\TSimulation{Simulation} codes!ALEPH2.6.2}.}
\label{fig:reacmon:decay-heat}
\end{figure}


\begin{Alert}{}
	Several references to MYRRHA here. Are to be left unmodified, or should we provide
	MINERVA versions?
\end{Alert}

\begin{figure}[h]%[tbp]
\centering
  \includegraphics[width=5.0937in,height=3.9402in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img255.png}
\caption[MYRRHA core at BOC: neutron flux map and fission rate maps.]{MYRRHA core at BOC: neutron flux map and fission rate maps.\newline
	\emph{Left top}: Horizontal cut through a MYRRHA equilibrium sub-critical core with 72 FAs.
	\emph{Right top}: Neutron flux map.
	\emph{Left bottom}: \textsuperscript{235}U fission rate map.
	\emph{Right bottom}: \textsuperscript{238}U fission rate map.}
\label{fig:reacmon:BOC-core}
\end{figure}

The three maps are the result of \index{\TSimulation{Simulation} codes!MCNPX}MCNPX calculations averaged over the middle
20 cm height (from $z$ = -10 cm to +10 cm) of each of the 211 assemblies. The values correspond to the
nominal beam current of \SI{1.7}{\mA} at BOC. The fission cross-sections at 550 K (temperature of LBE
and cladding) were used.

Neutron flux map and fission rate maps over the entire MYRRHA core at BOC are shown on
Fig.~\ref{fig:reacmon:BOC-core}. The \textsuperscript{235}U fission rate on the core periphery is one
order of magnitude larger than in the active zone because of the low- energy neutron component in the
reflector zone compared to a hard spectrum in the active zone. The \textsuperscript{238}U fission rate in
the active zone is two orders of magnitude larger than on the core periphery because of the hard spectrum
coming from the spallation target\index{linac!spallation target} and moderation in the BeO reflector.

Calculations of the intrinsic neutron source flux from the MOX fuel (in the equilibrium core) consisting of:

\begin{itemize}[noitemsep]
\item Neutrons from ($\alpha $,n) reaction, and
\item Neutrons from spontaneous fission.
\end{itemize}
and representing background neutron flux, give values about 8 orders of magnitude smaller than the neutron
flux during nominal operation. The intrinsic neutron source therefore has negligible contribution to
the count rate of a detector (placed anywhere in the reactor) even during operation at \SI{1}{\percent} of full power.

\FloatBarrier\subsection[Accelerator Mode Operation]{Accelerator Mode Operation}
\label{ssect:reacmon:accel-mode-op}
\label{bkm:Ref500253289}\label{bkm:Ref500253269}
The MINERVA (and, ultimately, MYRRHA) accelerator will work in a continuous regime with short, periodic interruptions called beam
trip\index{beam!trip}s---see Fig.~\ref{fig:reacmon:beamtrip-mode}.


\begin{Alert}{}
	Don't we use the term ``beam trip'' also to signify \emph{erroneous\/} interruptions of the beam?
\end{Alert}

These beam interruptions will be induced using an electrostatic chopper. Such time structure is required for
the Reference Sub-criticality Measurement Technique (RSMT\index{Sub-Criticality Monitoring System!Reference
Sub-criticality Measurement Technique})~\cite{D-KE1-4}.

\begin{figure}[h]%[tbp]
\centering
  \includegraphics[width=5.8736in,height=1.5602in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img256.png}
\caption{Schematic representation of accelerator operation in a beam trip mode.}
\label{fig:reacmon:beamtrip-mode}
\end{figure}

One period $T$ consists of the peak time $t_{\text{on}}$ when the beam is on the target and the beam
trip\index{beam!trip} $t_{\text{off}}$ in the opposite case. The example shows operation with a duty cycle factor
				\( t_{\text{on}} /T = 0.9 \).


When the beam is off during the beam trip\index{beam!trip}, neutron flux drops to the delayed neutron
level, see Fig.~\ref{fig:reacmon:beamtrip-mode}.

The evolution of the neutron population during the beam interruption can be expressed (under point
kinetics approximation with a single delayed neutron group with decay constant $\lambda$) through the
formula~\eqref{eq:reacmon:neutron-evol}~\cite{T-NormandieUni-2016-Chevret}.

\begin{equation}
	\EqNeutronPopulationEvolution,
\label{eq:reacmon:neutron-evol}
\end{equation}

\noindent
where $\Lambda_\mathit{eff}$ is the effective neutron generation time, $\beta_\mathit{eff }$ is the
effective neutron delayed neutron fraction, $\rho=1-1/k_{\mathit{eff}}$ is the reactivity. Duty cycle
factor $t_{\mathit{on}} /T$ is the fraction of time ton when the beam is on the target during a period $T$
(an on-and-off cycle) of the accelerator operation.

The first exponential in Eq.~\eqref{eq:reacmon:neutron-evol} describes the decay of fast neutrons,
while the second exponential characterises the emission of delayed neutrons. The latter is almost
constant within a time range of a few millisecond and it appears as ``a delayed neutron level''---see
Fig.~\ref{fig:reacmon:ex-fission-rate}.

\begin{figure}[h]%[tbp]
\centering
  \includegraphics[width=3.4472in,height=2.9335in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img257.png}
\caption[Example of a time-dependent 235U fission rate during a 2 ms trip.]{Example of a time-dependent 235U fission rate during a 2 ms trip.
	(The stable count rate before the beam trip\index{beam!trip} and the rise when the beam is on again are also shown).}
\label{fig:reacmon:ex-fission-rate}
\end{figure}

As shown in~\eqref{fig:reacmon:ex-fission-rate}, the ratio of the neutron flux $n_0$ during the time
when the beam is on (corresponding to prompt plus delayed neutrons) to the delayed neutron level $n_1$
(reached during the time when the beam is off and prompt neutrons disappeared) is proportional to the
reactivity expressed in dollars.


\begin{equation}
 \EqNeutronFluxRatio.
 \label{eq:reacmon:neutron-flux-ratio}
\end{equation}

The reactivity unit of a dollar corresponds to the effective delayed neutron fraction 
($\rho=1\$=\beta_\mathit{eff}$)
and is thus not universal but depends on the core composition. The value for the MYRRHA core is about
330 pcm (Fig.~\ref{fig:reacmon:keff-react} shows the relation between $k_{\mathit{eff}}$ and reactivity
expressed in pcm or dollars for the MYRRHA core). As $\beta_\mathit{eff}$ depends on the fuel and neutron
spectrum (causing fission), it is practically insensitive to perturbations like IPS and various changes
in the MYRRHA core configuration.

\begin{Alert}{}
	What is the reactivity unit for MINERVA?
\end{Alert}

\begin{figure}[h]%[tbp]
\centering
  \includegraphics[width=4.1339in,height=2.6602in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img258.png}
\caption[Universal relation between $k_{\mathit{eff}}$ and reactivity.]{Universal relation between $k_{\mathit{eff}}$ and reactivity.
	Reactivity is expressed in pcm (left-hand side vertical axis).}
\label{fig:reacmon:keff-react}
\end{figure}


For the MYRRHA core with $\beta_\mathit{eff}$ = 330 pcm, reactivity expressed in dollars (right-hand
side vertical axis).

During HFP, the proton beam current will be kept constant as discussed in the previous section.  During
startup, the beam current on the spallation target\index{linac!spallation target} will be significantly
lower, as it is the factor that linearly determines the power of an ADS~\cite{D-KE1-D1a}.

There are three possible ways to adjust the beam current on the target:

\begin{Alert}{}
	Do the following actions apply to MINERVA?
	What is the minimal value of \LinacSource{} intensity for MINERVA? What are its duty cycle characteristics?
\end{Alert}

\begin{itemize}[noitemsep]
	\item Changing the \LinacSource{}\index{linac!injector!front end!proton source} intensity (minimal
	value for the MYRRHA accelerator is expected to be around \SI{0.1}{\mA});

  \item Changing the duty cycle (\emph{i.e.} the fraction of time when the beam is on). Minimal
	stable conditions for the MYRRHA accelerator are expected to be: 1 ms beam duration at a frequency
	of 1 Hz (\emph{i.e.} 1 ms beam, 999 ms beam trip\index{beam!trip}, duty cycle factor of $10^{-3}$);

  \item Collimation.
\end{itemize}

The second option has been selected as it provides sufficiently large range of average beam intensity
on the spallation target\index{linac!spallation target} under stable conditions. It also represents an
advantage for instrumentation, which is discussed in Section~\ref{bkm:Ref497909028}, ``Instrumentation'',
. The drawback is that with a shorter duty cycle the count-rate during
the beam-trip becomes significantly lower as can be seen from formula Eq.~\eqref{eq:reacmon:neutron-evol}
and Fig.~\ref{fig:reacmon:duty-cycle-react}.

\begin{figure}[h]%[tbp]
\centering
  \includegraphics[width=6.2008in,height=2.3937in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img259.png}
\caption{Effect of duty cycle factor and reactivity on delayed neutron ratio.}
\label{fig:reacmon:duty-cycle-react}
\end{figure}

Impact of the duty cycle factor and reactivity level on the ratio of the delayed neutron level during
the beam trip\index{beam!trip} to the total neutron population when the beam is on the target.

\FloatBarrier\subsection[Start-up (Sub-critical Approach)]{Start-up (Sub-critical Approach)}

\begin{Alert}{}
	Do we have to describe the sub-criticality level of MINERVA?
\end{Alert}

In this section, the application of the reactivity monitoring methodology using the sub-critical approach
and the RSMT\index{Sub-Criticality Monitoring System!Reference Sub-criticality Measurement Technique}
to determine the initial sub-criticality level of the MYRRHA core is described. Every cycle of the MYRRHA
reactor will start with a standard sub-critical approach.

In the case of MYRRHA as an ADS, the standard sub-critical approach will be subdivided into two parts. The
two-step procedure for the sub-critical approach in MYRRHA can be considered as an extrapolation of the
current two-step sub-critical approach procedure for a critical core applied in \GUINEVERE{} towards MYRRHA.

\subsubsection{First phase of sub-critical approach}
Fuel loading will be carried out using standard sub-critical approach with the intrinsic neutron source
and detectors measuring static count rates. In this stage, the accelerator beam line will be physically
inserted in the core (left part of Fig.~\ref{fig:reacmon:core-vert}) but the proton beam will not be
switched on. The control rods will be fully inserted.

The first phase of the sub-critical approach will continue until all FAs, predicted by calculation to reach
$k_{\mathit{eff}} \approx 0.95--0.96$ (with CRs completely inserted), will be loaded.  The calculated
number of FAs that would be needed to reach a critical core will be compared with the extrapolation of
the measured inverse count rate curve after each step. There will be an acceptability criterion for the
difference between measurement and calculation. The eventual discrepancy will be analysed and the impact
on \TSafety{safety} will be evaluated.

Then the core restraint system will be positioned to fasten the FAs. This can lead to a small change
in $k_{\mathit{eff}}$ (\midtilde{}200 pcm maximum~\cite{D-KE1-D1a}) as until this point the FAs
will be fixed on their top only, while their bottom part are not mechanically fixed.

Safety\index{Dependability!Safety} analysis has to point out whether this first phase
of the sub-critical approach is necessary for safety.

\subsubsection{Second phase of sub-critical approach}
As a next step, the proton beam in a beam trip\index{beam!trip} mode will be sent to the spallation
target\index{linac!spallation target}. The duty cycle will be chosen in such a way that the average
beam intensity on the target will be low enough for the reactor core to remain in the Cold Zero Power
(CZP) operation mode (less than \SI{6}{\percent} of nominal power can be considered low as it is the core power due
to decay heat at EOC immediately after the shutdown, as discussed in Section~\ref{bkm:Ref497909286},
``MYRRHA Sub-critical Core'').

The RSMT\index{Sub-Criticality Monitoring System!Reference Sub-criticality Measurement Technique} will
be applied to measure $k_{\mathit{eff}}$ of the core with the CRs completely inserted in the active
zone---see right part of Fig.~\ref{fig:reacmon:core-vert}.

Step by step, the CRs will be extracted from the active zone. At each step the RSMT\index{Sub-Criticality
Monitoring System!Reference Sub-criticality Measurement Technique} will be applied to measure
$k_{\mathit{eff}}$. The extraction of the CRs will continue until $k_{\mathit{eff}}$ ${\approx}$
0.97--0.98 at CZP is reached. This will be the initial sub-criticality level.

The part of the CRs that will remain inserted in the active zone at this point should correspond to at
least the loss of reactivity from BOC to EOC due to fuel burn-up (about 1300 pcm~\cite{D-KE1-D1b}
and which will be compensated by further extraction of CRs during the entire cycle, as described in
Section~\ref{bkm:Ref497909286}, ``Sub-critical Core''; see
Fig.~\ref{fig:reacmon:core-vert}.

If such conditions cannot be reached (too large or too small part of the CRs remain inserted when
the desired sub-criticality level is reached), the discrepancy shall be analysed and the impact on
\TSafety{safety} evaluated.

\FloatBarrier\subsection[Nominal Operation (HFP)]{Nominal Operation (HFP)}
When the second phase of sub-critical approach is completed, the beam power will be increased to the
nominal value (\SI{1.7}{\mA} in the current design) corresponding to the core power of 70 MW (HFP). The value
of $k_{\mathit{eff}}$ will drop by about 1400 pcm due to the ``power reactivity effect'' (interplay of
fuel Doppler effect, fuel expansion, temperature effects of clad, wrap, coolant and diagrid---based on
the calculation performed for MYRRHA v1.4~\cite{D-KE1-D1b} and estimated to be an envelope for
v1.6). The final $k_{\mathit{eff}} \approx 0.95--0.96$ will be kept constant during the HFP operation
from BOC until EOC.

As the initial $k_{\mathit{eff}}$ value determined during the second phase of sub-critical approach has
been measured accurately and sufficient anti-reactivity margin exists to exclude re-criticality under any
design basis accidental conditions~\cite{D-KE1-D1b}, the sub-criticality level monitoring during
HFP operation of the MYRRHA ADS is not required for \TSafety{safety}.

During normal operation, the SCMS\index{Sub-Criticality Monitoring System} will use a Monitoring
Sub-criticality Measurement Technique (MSMT) for relative online monitoring of the $k_{\mathit{eff}}$
variations during the entire cycle. The current to flux technique will be applied as MSMT as described
in~\cite{D-KE1-4}. As this technique provides only a relative value (change of reactivity), it is
planned to calibrate it regularly by applying the RSMT\index{Sub-Criticality Monitoring System!Reference
Sub-criticality Measurement Technique}. This allows a good operational follow-up of the evolution of
the burn-up and hence its compensation by the CRs.

\FloatBarrier\subsection[Instrumentation]{Instrumentation}
\label{bkm:Ref497909028}
The choice of neutron detector type and positioning for the sub-criticality monitoring
is discussed in detail in~\cite{D-KE1-D6}. Here, only the main parameters are referred to.

The same neutron detectors are planned to be used for start-up (after fuel loading) and power
operation. Reactor power is linearly proportional to the beam power on the target. Beam current will be
altered by changing the beam duty cycle while keeping the \LinacSource{}\index{linac!injector!front end!proton
source} intensity and consequently the ``peak current'' (\emph{i.e.} when the beam is on the target) constant.

Neutron detector count rate during the time when the beam is switched on will remain the same at any
reactor power. It will enable to choose such conditions (detector parameters and location) to reach optimal
counting statistics and to limit the detector dead time during HFP operation. In general, the count rate
range of $10^5$--$10^6$ cps (counts per second) is the upper limit for keeping the dead time negligible.

As shown during the RSMT\index{Sub-Criticality Monitoring System!Reference Sub-criticality Measurement
Technique} \TValidation{validation}~\cite{D-KE1-4}, the neutron detector material (deposit of a fission
chamber) impacts on the performance of RSMT. Threshold detectors are preferable for RSMT as they have low
sensitivity to perturbations (locations near IPS, CRs)---see Fig.~\ref{fig:reacmon:BOC-core}.

Additionally, a low fission cross-section does not lead to significant burn-up of a deposit.


\FloatBarrier\subsection[Conclusions]{Conclusions}
The reactivity monitoring methodology during start-up will rely on a two-step sub-critical approach
procedure and the SCMS (\PutIndex{Sub-Criticality Monitoring System}).

SCMS will use RSMT\index{Sub-Criticality Monitoring System!Reference Sub-criticality Measurement Technique}
(Reference Sub-criticality Measurement Technique) to measure sub-critical levels of the core at low
power during the second phase of the sub-critical approach, which will consist of step-by-step extraction
of control rods.

The reactor power will be increased to HFP (Hot Full Power) by extending the duty cycle of the proton
beam on the spallation target\index{linac!spallation target}. During power operation, the reactivity
monitoring strategy will rely on the initial reactivity measured during the start-up phase and the margin
to criticality.

For operational reasons, such as burn-up compensation, the SCMS\index{Sub-Criticality Monitoring System}
will use MSMT for online monitoring of the $k_{\mathit{eff}}$ variations during HFP operation.

\FloatBarrier
%
% END TDR-B17-ReactMonitoring.tex
%
