%
% TDR-B02-MEBT3.tex
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section[Medium Energy Beam Transport Line 3]{Medium Energy Beam Transport Line 3}\label{sect:mebt-3}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\begin{Alert}{}
%Again, may I use ``MINERVA (and, ultimately, MYRRHA) accelerator''? Shall I call sections such as this one Goal of the MINERVA MEBT?
%\end{Alert}
%\mycomment{ap}{As we decided to change Goal into an introduction to this section, we don't have this issue anymore!}

% \TWinIiMEiiiMAGDIbendValue = 45; see Annex/tracewin.tex, created dynamically by tracex
\def\dipoleDeviation{\TWinIiMEiiiMAGDIbend}%\SI{\TWinIiMEiiiMAGDIbendValue}{\degree}}
\def\dipoleCurvatureRadius{\TWinIiMEiiiMAGDImradius}%{\SI{\TWinIiMEiiiMAGDImradiusValue}{\meter}}

%{{{ Intro
There are several functional requirements that define the design and operation of the {\LinacMEBTiii} section (\naming{\ncMEBT3}, \naming{\ncLBMEBT3}). The
first requirement is to provide \TBeamTransport{beam transport} from the operational \TInjector{injector} towards the linac.
This involves implementing a design and tuning philosophy that should satisfy the following criteria:

%%\mycomment{vdflorio}{Here I say that MEBT-3 is actually I1-ME3 and LB-ME3. Am I correct?}

\begin{enumerate}
  \item \textbf{Beam matching.} The tuning should allow that the beam at the exit of the {\LinacMEBTiii}
	section (\naming{\ncLBMEBT3}) is matched to the required one for the beginning of the
	{\LinacSCLinac}\footnote{The terms ``SC linac''
		and ``main linac'' hereafter shall be used to refer informally to the same system component.}.
	Beam matching is done in both the transverse and the longitudinal directions.

\ifdefined\EnableWarningsAndAlertsStillPending
\mycomment{vdflorio}{Could you please check the footnote here?}
\fi

  \item \textbf{Beam losses.} The accelerator will operate using high-current proton beams
	reaching \SI{5}{\mA}; therefore, \TBeamLosses{beam losses} should be minimised. The latter
	requires minimal transverse \TBeamEnvelope{beam envelopes} through the entire section, reducing
	the probability for proton collisions with the walls of the \TVacuumSystem{vacuum chambers}.

  \item \textbf{Double achromaticity.} The use of \TDipoles{dipoles} for beam deviation requires having a lattice
	design and tuning which should restore the original double achromaticity after the selection
	\TDipoles{dipole} or selecting \TDipoles{dipole} which selects which of the \TInjector{injectors}
	delivers \TBeam{beam} to the {\LinacSCLinac}.

\item \textbf{\TEmittance{Emittance} growth.} The use of high \TBeamCurrent{beam currents} (\SI{5}{\mA})
	leads to \TSpaceCharge{space charge}-related effects. Therefore, the lattice and tuning of the
	{\LinacMEBTiii} should be made with a minimal \TEmittance{emittance} increase at the end of
	the section.

  \item \textbf{Energy preservation and synchronous phase.} The {\LinacMEBTiii} is a non-accelerating
	section, which requires preserving the initial \TBeamCurrent{beam energy} after every rebuncher
	cavity, as well as maintaining the synchronous phase.
\end{enumerate}

Another requirement is to define the \TBeamDiagnostics{diagnostics} and the \TBeamInstCollim{collimating
system}. The \TFaultDiagnosis{diagnostics} are necessary for both  tuning and monitoring  stable
operation. A system of \TBeamInstCollim{collimators} will be used to protect the downstream lines in case
of \TFailures{failures}. An \TErrors{error study} will be performed in order to evaluate the change of
the desired parameters and the possibility to make necessary corrections.
%}}}
%{{{ MEBT-3 Design
\subsection[MEBT-3 Design]{MEBT-3 Design}
The {\LinacMEBTiii} section, whose function is to \TBeamTransport{transport} \SI{16.6}{\MeV} proton
beams from the preceding \TInjector{injectors}, consists of two symmetrical lines, which split further
at the {\dipoleDeviation} deviation \TDipoles{dipoles} into two parts---one towards a \SI{70}{\kW} \TBeamDestinations{beam dump} and
the other towards a selection \TDipoles{dipole} \TMagnets{magnet}. The latter is the converging point of the
two \TBeam{beam} lines towards the main accelerator section. There are seven different branches of
the {\LinacMEBTiii} section, which are interconnected by means of three bending \TMagnets{magnets} as
shown in Fig.~\ref{fig:MEBT3Section}. The second \TInjector{injector}, its dump line and the connection to
the \TMagnets{selection magnet} are not part of \MINERVA. However, the building infrastructure will be
designed in view of their future installation.


\ifdefined\EnableTeXObjects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\TeXFigure{fig:MEBT3Section}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\else
 \begin{figure}[h]%[tbp]
 \centering
 	\includegraphics[width=\textwidth]{MYRRHATDRPartB-img/D-MYRTE-2.6-Fig50.png} % Note: template picture. See LaTeXmacros.py
	 \caption[The MEBT-3 section between the two injectors and the SC Linac]{%
% \adrianreply{The contents of this layout drawing and the related text is significantly different from the part of the booster. Can we align this to the same details. e..g the same drawing on CH1-CH16.}{agatera}{We do not have yet a mechanical integration of this section (the layout drawing of the booster was taken from there). I propose to remove the CH section from the picture and just leave the arrows indication beam sources.}%
	 The {\LinacMEBTiii} section between the two \TInjector{injectors} and the {\LinacSCLinac} (dashed green line).
%
% \adrianreply{Figures should be referenced in the text. As there is (usually) more background info.}{agatera}{it is the case, see first paragraph of section 8.1 MEBT-3 design}
%
%\mycomment{vdflorio}{Adrian highlighted an orange oval in the picture. Unclear. Please check this out}
%
It is divided into seven parts separated by two {\dipoleDeviation}-deviation \TDipoles{dipole}
\TMagnets{magnets} and a selection (switching in the figure)
\TDipoles{dipole} connecting the two \TInjector{injectors} to the linac section.}
 \label{fig:MEBT3Section}
 \end{figure}
\fi

Branches \ding{172} and \ding{173} consist of magnetic \TQuadrupoles{quadrupole} triplets and a SC rebuncher
cavity. Following the {\dipoleDeviation} deviation \TDipoles{dipoles}, the lines split into the deviation
branches \ding{174} and \ding{175}, towards the \TMagnets{selection magnet} and into branches \ding{176}
and \ding{177}, towards the \TBeamDestinations{beam dumps}. The {\dipoleDeviation} \TDipoles{dipole}
\TMagnets{magnets} have the following parameters: {\dipoleCurvatureRadius} curvature radius, 60 mm gap height and
\SI{0.6}{\Tm} magnetic \TRigidity{rigidity}. The entrance edge angle is \SI{0}{\degree}, whereas for the
exit edge angle two different solutions were studied: \SI{0}{\degree} and \SI{22.5}{\degree}. Branches
\ding{174} and \ding{175} consist of two \TQuadrupoles{quadrupole} doublets positioned symmetrically around the centre
point between the {\dipoleDeviation} deviation and the selection \TDipoles{dipole}. The defined parameters
of the latter are the same as for the {\dipoleDeviation} deviation \TDipoles{dipole} \TMagnets{magnet},
with a difference: the symmetry imposes that there are two solutions for the entrance edge angles
(\SI{0}{\degree} and \SI{22.5}{\degree}), whereas the exit edge angle is fixed to \SI{0}{\degree}. The
configuration of the \TQuadrupoles{quadrupoles} in this branch was chosen such that double achromaticity is achieved
after the selection \TDipoles{dipole} while preserving the small size of the transverse envelopes.

The interconnection \ding{174}/\ding{175}/\ding{178} is managed by the selection {\dipoleDeviation}
\TDipoles{dipole} \TMagnets{magnet} with two entries and one single exit. The exact design of the
selection \TDipoles{dipole} is not finalised but its main characteristics should be the same as for the
{\dipoleDeviation}-deviation \TDipoles{dipoles}: {\dipoleCurvatureRadius} curvature radius, \SI{60}{\milli\meter}
height, \SI{0.6}{\Tm} magnetic \TRigidity{rigidity}, \SI{0}{\degree} or \SI{22.5}{\degree} entrance edge,
\SI{0}{\degree} exit edge angle and fast polarity selection capabilities. Branch \ding{178} consists
of two SC rebuncher cavities separated by a \TQuadrupoles{quadrupole} triplet for \TBeam{beam} matching in transverse
directions. The \TSpokeCavities{SC cavities} in the {\LinacMEBTiii} are the same as the ones for the
\TSCLinac{SC linac} ($\beta=0.35$ 2-gap \TSpokeCavities{spoke cavities}).

%}}}
%{{{ Beam Dynamics Simulation with TraceWin
\subsection[Beam Dynamics Simulations with TraceWin]%
	   {Beam Dynamics \TSimulation{Simulations} with {\CodesTraceWin}}
The conceptual design of the {\LinacMEBTiii} described in the previous section was obtained by using
\TBeamDynamics{beam dynamics} \TSimulation{simulations}. The initial lattice was adapted from previous
work on the {\LinacMEBTiii} section~\cite{D-MAX-1.2}. The \TSimulation{simulations} were done with
the already introduced \TSimulation{simulation} code~\cite{P-ICCS02-Duper,P-IPAC15-Uriot}, which
allows defining the beams either as envelopes based on the initial \TBeam{beam} properties (energy, Twiss
parameters, \TEmittance{emittance}, etc.) or directly as particle distributions. For the {\LinacMEBTiii}
input, the latter were obtained by \TSimulation{simulations} on the \TInjector{injectors}' design and
performance~\cite{P-IPAC17-Haehnel}. A similar distribution was obtained for beam matching required for
the entering \TBeam{beam} at the {\LinacSCLinac}.

Two alternative designs of the {\LinacMEBTiii} have been defined by the \TSimulation{simulations},
both satisfying the main requirements defined earlier. The two designs, labelled Design \#1 and Design
\#2, are shown in Fig.~\ref{fig:MEBT3LatticeDesigns}. The main difference between the two designs is
the exit and entrance edge angle of the {\dipoleDeviation}-\TMagnets{deviation magnet and the selection
magnet} respectfully. Design \#1 uses a pole face angle of \SI{22.5}{\degree}, whereas Design \#2 does
not include an inclination of the pole faces of the two magnets. The two alternatives are proposed
in order to have some freedom on the final design of the \TMagnets{selection magnet} (described in
Sect.~\ref{sect:magsteers:subsec:switchingmagnet}), which was not finalised during the work on the
{\LinacMEBTiii} design. The main difference, imposed by the different pole face angles, is the positioning
of the \TQuadrupoles{quadrupoles} in section \ding{173} of Fig.~\ref{fig:MEBT3LatticeDesigns}.

%\mycomment{Agatera}{could you add a reference to the switching magnet section here please ?}

\ifdefined\EnableTeXObjects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\TeXFigure{fig:MEBT3LatticeDesigns}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\else
 \begin{figure}[h]%[tbp]
 \centering
 \includegraphics[width=\textwidth]{MYRRHATDRPartB-img/D-MYRTE-2.6-Fig51.png}
	 \caption[Two alternative designs of the MEBT-3 lattice]{Two alternative designs of the {\LinacMEBTiii} lattice
 	 are shown. Design \#1 (top) uses \SI{22.5}{\degree} for the pole face angles in section \ding{173},
 	 whereas Design \#2 (bottom) has straight pole faces.}
 \label{fig:MEBT3LatticeDesigns}
 \end{figure}
\fi

Since the design and tuning for the two alternatives are very similar, the results will be discussed
in detail only for Design \#2, which offers simpler \TMagnets{magnet} designing. A comparison of the
resulting envelopes, \TEmittance{emittance} growth, and tuning mismatch for the two designs is presented
in Table~\ref{tab:MEBT3:D-MYRTE-2.6-Table5}.

The transverse and longitudinal envelopes for a tuning satisfying the presented requirements are
shown in Fig.~\ref{fig:MEBT3SigmaEnvelopes}. The top left panel shows \rpm$3\sigma$ envelopes along the
{\LinacMEBTiii} section in the two transverse directions. The purple line represents the envelope in the
horizontal plane and green colour for the vertical. The top right shows the dispersion of the section
being compensated after the selection \TDipoles{dipole} and satisfying the double achromaticity condition. Lower
panels show the longitudinal envelopes---phase (left) and energy (right). The lattice and tuning
\TOptimisation{optimisation} was done using \TBeamEnvelope{beam envelopes} and a beam definition
at the entrance obtained by Twiss parameters. The proton energy was set to \SI{16.6446}{\MeV} and
the \TBeamCurrent{beam current} was set to \SI{4}{\mA}. The presented solution uses a symmetry in
the transverse planes around the centre point in the deviation section, which allows reaching double
achromaticity after the selection \TDipoles{dipole}. The envelopes (\rpm$3\sigma$) are maintained minimal along
the whole {\LinacMEBTiii} line. The maximal envelope ({\textless}\SI{12}{\milli\meter}) is reached in
the vertical direction close to the exit of the {\dipoleDeviation}-\TMagnets{deviation magnet} and the
entrance of the \TMagnets{selection magnet}. The tunings of the two quad doublets are antisymmetric, which
allows symmetric envelopes and a dispersion crossing at the centre of the deviation section, which in
turn facilitates obtaining double achromaticity. The tuning in longitudinal direction is done with the
three cavities (CH1: \naming{\ncCH1}, CH2: \naming{\ncCH2}, and CH3: \naming{\ncCH3}), though only the
first two are at significant power. The third one could be switched off completely if necessary, but a
minimal power is maintained in order to have a fast tuning \TErrorRecovery{recovery} in case of operating
cavity \TFailures{failures}. Longitudinal tuning preserves the proton \TBeamEnergy{beam energy} and the
synchronous phase at the cavities. A phase compression is also achieved at the exit of the {\LinacMEBTiii}
line compared to the entrance at the expense of a larger energy spread.

The \TOptimisation{optimisation} \TSimulation{simulation} was repeated also in particle distribution
mode showing very similar results. The matched particle distributions at the exit of the {\LinacMEBTiii}
are shown in Fig.~\ref{fig:MEBT3ProtonDistributions}. The distributions were matched to the desired
input of the {\LinacSCLinac} with a minimal mismatch. The ``tails'' in the distributions are due to
\TSpaceCharge{space charge} effects related to the high \TBeamCurrent{beam current}.


\ifdefined\EnableTeXObjects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\TeXFigure{fig:MEBT3SigmaEnvelopes}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\TeXFigure{fig:MEBT3ProtonDistributions}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\else
 \begin{figure}[h]%[tbp]
 \centering
  \includegraphics[width=\textwidth]{MYRRHATDRPartB-img/D-MYRTE-2.6-Fig52.png}
 \caption{Envelopes along the MEBT{}-3 section in the two transverse directions.
% \adrianreply{Figures should be referenced in the text. As there is (usually) more background info.}{vdflorio}{I have shortened the caption as the information was already provided in the text}.%
	 }
 \label{fig:MEBT3SigmaEnvelopes}
 \end{figure}

 \begin{figure}[h]%[tbp]
 \centering
 \includegraphics[width=\textwidth]{MYRRHATDRPartB-img/D-MYRTE-2.6-Fig53.png}
 \caption[Proton distributions at the exit of the MEBT-3 line.]{The three panels show
	 proton distributions at the exit of the {\LinacMEBTiii} line in the horizontal (left), vertical
	 (centre), and longitudinal (right) directions. The mismatch compared to the desired distributions
	 at the entrance of the {\LinacSCLinac} is smaller than \SI{0.12}{\percent} in all directions. The
	 red ellipses correspond to 9 RMS of the \TEmittance{emittance}.}
 \label{fig:MEBT3ProtonDistributions} \end{figure}
\fi

%  	in the two transverse directions. Purple line represents the envelope in the horizontal plane and
%  	green colour for the vertical. The top right shows the dispersion of the section being
%  	compensated after the selection dipole,  satisfying the double achromaticity condition. Lower
%  	panels show the longitudinal envelopes---phase (left) and energy (right).}
A comparison was made between the desired \TBeamCurrent{beam current} of \SI{4}{\mA} and lower
values (\SI{3}{\mA} and \SI{2}{\mA}). Figure~\ref{fig:MEBT3ProtonDistributions} shows the change of
\TEmittance{emittance} in the three planes along the {\LinacMEBTiii} for different \TBeamCurrent{beam
currents}. As expected, the \TEmittance{emittance} growth at the exit of the line is the largest for the
maximal \TBeamCurrent{beam current}. The increase compared to the input beam is \SI{5.4}{\percent},
\SI{3.2}{\percent}, and \SI{8.3}{\percent} in the horizontal, vertical, and longitudinal planes
respectively. For lower \TBeamCurrent{beam currents}, the \TEmittance{emittance} change is reduced (see
Table~\ref{tab:MEBT3:D-MYRTE-2.6-Table5}). The \TEmittance{emittance} growth is not the only unwanted
effect related to the use of high \TBeamCurrent{beam currents}, since matching is affected strongly
too. When using the \TOptimisation{optimised} tuning at \SI{4}{\mA} for lower \TBeamCurrent{beam currents},
the beam mismatch is increased. This is because the \TOptimisation{optimisation} compensates for the
\TSpaceCharge{space charge} effects created by the \SI{4}{\mA} beam. Lower \TBeamCurrent{beam currents}
produce smaller \TSpaceCharge{space charge} effects and the tuning will overcompensate them. The values
of the resulting mismatches are presented in Table~\ref{tab:MEBT3:D-MYRTE-2.6-Table5}. If necessary,
the tuning can be re-\TOptimisation{optimised} for the use of lower \TBeamCurrent{beam currents} to
maintain good beam matching.


\ifdefined\EnableTeXObjects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\TeXFigure{fig:MEBT3ChangeOfEmittance}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\else
 \begin{figure}[h]%[tbp]
 \centering
 \includegraphics[width=\textwidth]{MYRRHATDRPartB-img/D-MYRTE-2.6-Fig54.png}
\caption[Change of emittance in the three planes along the MEBT-3 for 3 different beam currents.]{The three panels
	show the change of \TEmittance{emittance} in the three planes along the {\LinacMEBTiii} for
	three different \TBeamCurrent{beam currents} (\SI{4}{\mA}, \SI{3}{\mA}, and \SI{2}{\mA}). Red
	and blue lines correspond to transverse emittances---horizontal and vertical, respectively. Green
	lines correspond to longitudinal emittance.}
 \label{fig:MEBT3ChangeOfEmittance} \end{figure}
\fi

As mentioned earlier, the two alternative designs produce very similar results. The main differences
between Design \#1 and Design \#2 are summarised in Table~\ref{tab:MEBT3:D-MYRTE-2.6-Table5}.

% Being a table, this environment has not been changed int a TeXFigure
\begin{table}
\includegraphics[width=\textwidth]{MYRRHATDRPartB-img/D-MYRTE-2.6-Table5.png}
\caption[Differences in the simulation results using Design \#1 and Design \#2.]{The differences in the
	\TSimulation{simulation} results using Design \#1 and Design \#2 are listed. The main difference
	is the larger maximal vertical envelope in the case of Design \#2 due to the lack of a vertical
	focusing effect in the case of straight pole faces. The results for the \TEmittance{emittance} growth
	follow the trends shown in Fig.~\ref{fig:MEBT3ChangeOfEmittance}. Since all the results presented
	here were obtained for tuning \TOptimisation{optimisations} at \SI{4}{\mA} \TBeamCurrent{beam
	current}, there are large mismatches at lower current values.}
\label{tab:MEBT3:D-MYRTE-2.6-Table5}
\end{table}
%}}}
%{{{ Summary
\subsection[Simulation results]{\TSimulation{Simulation} results}
%\adrianreply{The title is confusing as it is listed at the same level a the next section.. Can you be more specific.}{agatera}{title changed to \TSimulation{simulation} results}
The \TBeamSimulations{beam} \TSimulation{simulations} for the {\LinacMEBTiii} section were done to
define the lattice and to perform tuning \TOptimisation{optimisations}. The results are satisfactory
and allow significant freedom for eventual changes in the lattice due to space restrictions. The main
requirements were satisfied, \emph{i.e.} beam matching, double achromaticity, and minimal \TBeamLosses{beam
losses}. \TSpaceCharge{Space charge} effects are compensated as much as possible by the tuning, but at
\SI{4}{\mA} \TBeamCurrent{beam current} there is an \TEmittance{emittance} growth that can reach a few
percent in the transverse directions and above \SI{8}{\percent} in longitudinal. These unwanted effects
also require the tuning to be \TOptimisation{re-optimised} accordingly for a specific \TBeamCurrent{beam
current} in order to maintain the matching to the next section.

Two different lattice designs were presented, which will allow choosing between different \TMagnets{selection
magnet} designs. Since the distance between the \TInjector{injectors} was fixed to \SI{7}{\meter}, the only
modification of the {\LinacMEBTiii} section will be the repositioning of the \TQuadrupoles{quadrupole}
doublets in the deviation sections.

%}}}
%{{{  Rebunchers
\subsection[Rebunchers]{Rebunchers}
The accumulation of {\LinacMEBTiii} functionalities leads to a relatively long beam line
(\midtilde\SI{12}{\meter}) with challenging requirements on the longitudinal \TBeamDynamics{beam dynamics},
which are taken care of by the rebunching cavities.

Today, a detailed design of {\LinacMEBTiii} is still missing. Initial \TSimulation{simulations} performed
in the framework of the conceptual design clearly indicate the need for 3 rebunching cavities along the
line in order to preserve the beam quality in the longitudinal phase space and to perform the necessary
RF gymnastics related to the frequency jump.

% \begin{Alert}{}
% Are we sure that ``gymnastics'' is the right word here?
% \mycomment{ap}{Yes! One can find this expression in many papers. This is commonly used.}
% \end{Alert}

The 1\textsuperscript{st} rebunching cavity is placed right after the initial \TQuadrupoles{quadrupole} triplet
of each \TInjector{injector} therefore present in each of the 2 \TInjector{injector} branches. The
2\textsuperscript{nd} and 3\textsuperscript{rd} rebunching cavities are placed in the common section
after the \TMagnets{selection magnet}, and they are separated by a \TQuadrupoles{quadrupole} triplet. The full
scheme (2 \TInjector{injectors} installed) thus features 4 rebunchers (they are indicated by SC in
Fig.~\ref{fig:MEBT3Section}).

% \mycomment{vdflorio}{I think it is beneficial if we mentioned the naming convention terms for the above components.
% I propose also to add those terms in the picture of Fig. 26 (\ref{fig:MEBT3Section})}

%
%\mycomment{ap}{The following figure is not up to date. I will refer to Fig.~\ref{fig:MEBT3Section} instead.}
%\ifdefined\EnableTeXObjects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\TeXFigure{fig:MEBT3LayoutTwoInjectors}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\else
% \begin{figure}[h]%[h]%[tbp]
% \centering
% \includegraphics[width=5.6362in,height=3.5528in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img042.png}
% \caption{Layout of MEBT3 (shown with 2 injectors installed).}\label{fig:MEBT3LayoutTwoInjectors}
% \end{figure}
%\fi

Logically, the voltages to be applied in the rebunching cavities are of the same order of magnitude as
those of the subsequent accelerating \TSpokeCavities{spoke cavities}. Considering the added value of
a compact solution in terms of longitudinal space and of uniformity in techniques on one hand, and the
availability of the \TCryoSystem{cryogenic distribution} very close to {\LinacMEBTiii} on the other hand, each of the
rebunching cavities in {\LinacMEBTiii} should be a superconducting \TSpokeCavities{single-spoke cavity},
identical to the ones applied in the accelerating spoke section. The first  rebuncher requires less
field and could
%
%\ulrichreply{main advantage: vacuum level. Thus is is the new baseline.}{agatera}{added to the text}
%
be optionally replaced by a warm CH type rebuncher. It would resolve the possible \TVacuumSystem{vacuum}
level discrepancy between the CH line \TVacuumSystem{vacuum} level and the much lower level needed in
\TSCCavities{superconducting cavities}. This would also shorten the \TCryoSystem{cryogenic distribution lines} that would have to
be kept only in the {\LinacSCLinac} tunnel. The study of such CH rebuncher operating at \SI{\TWinIiMEiiiFreqValueAsInt}{\MHz}
is still pending. In the meantime, a \TSpokeCavities{spoke cavity} is used in the design.

In the {\LinacMEBTiii}, the cavity-containing
\TCryoModules{cryomodules} will house 1 \TSpokeCavities{single-spoke cavity} only,
%
%\adrianreply{Why?}{Agatera}{motivation added to text}%
%
in order to allow the implementation of a \TRedundancy{serial redundancy} in this section. This cryomodule
will be a direct and straightforward evolution of the cryomodule hosting 2 single-spoke cavities, and
being prototyped (see Sect.~\ref{sect:ss-cavities}, ``\TSpokeCavities{Single-Spoke Cavities}'').



The 2\textsuperscript{nd} and 3\textsuperscript{rd} rebuncher shall be able to participate in
\TFaultCompensation{compensating} for a \TFaults{fault} in the first accelerating cavity, and vice-versa:
in case of \TFailures{failure} of the last rebunching cavity, its role shall be taken up by reconfiguring
the first accelerating cavities.

%}}}
%{{{  Dipole Magnets
\FloatBarrier\subsection[Dipole Magnets]{\TMagnets{Dipole Magnets}}\label{sect:dipoles}

The proton \TBeam{beam} from one {\TInjector{injector} section will be sent either to the accelerator or to the
\TBeamDestinations{beam dump}. When running two \TInjector{injectors}, one \TBeam{beam} is normally sent to a
\TBeamDestinations{dump} and the other one to the accelerator. The selection of the \TBeam{beam} from the
\TInjector{injector} line is done thanks to a standard \TMagnets{magnet} \TDipoles{dipole} while the deviation of the
beams from the two \TInjector{injectors} (not simultaneously) is provided by a specific \TMagnets{selection
magnet}. The designs of those two components are presented in Sect.~\ref{sect:magsteers}.

%\mycomment{ap}{Are we going to have a paragraph on the injector selecting dipoles? If so TBD should be replaced by the reference here}
%\subsubsection{Purpose}
%The proton \PutIndex{beam} from one injector\index{linac!injector} section will be sent either to the
%accelerator or to the beam dump\index{beam!dump}.
%
%When running two injectors\index{linac!injector}, one beam is normally sent to a dump and the other one to the
%accelerator. In this case, two possible injection options are depicted in Fig.~\ref{fig:MEBT3RunningCases}.
%
%
%
%\ifdefined\EnableTeXObjects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\TeXFigure{fig:MEBT3RunningCases}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\else
% \begin{figure}[h]%[tbp]
% \centering
% \includegraphics[width=4.5673in,height=3.9071in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img043.png}
% \caption[The two MEBT3 running options.]{The two MEBT3 running options.
%	  Top: beam injection from first injector (in green); bottom: injection from second injector (in purple).}
% \label{fig:MEBT3RunningCases}
% \end{figure}
%\fi
%
%When the accelerator is running, switching from one injection option to the other is required when the
%injector\index{linac!injector} in use is faulty. In such a case, to guarantee almost continuous beam
%delivery, it is required that commuting between injection options be as fast as possible (less than
%\SI{3}{\second}).\index{Dependability!Fault tolerance!reconfiguration}
%
%In order to do that, three fast switching devices (labelled as 1, 2 and 3 in Fig.~\ref{fig:MEBT3RunningCases})
%are used to achieve the required beams deflections.
%
%Devices 1 and 2 are dipoles which are operated ON (for beam deflection) and OFF (for no beam
%deflection).
%
%Device 3 must select the input beam and provide two different deflections: it is not a simple ON
%(active state +1)/OFF(0) device but at least partially a reversible one (two active states +1 and -1).
%
%\subsubsection{Design aspects}
%At the input of device 3 and relatively to main accelerator axis, the two injector\index{linac!injector}
%beams axis have symmetrical incident angles on device 3 (objective angles are +\SI{45}{\degree} and
%-\SI{45}{\degree}).
%
%Then a possible solution is that device 3 achieve the required beam deviation in two steps:
%
%\begin{itemize}[noitemsep]
%  \item First, a static dipole achieves one deviation for one incident beam (for example
%	-\SI{40}{\degree}) and the opposite deviation for the other incident beam (for example
%	+\SI{40}{\degree}). This static dipole is operated with a constant polarity.
%
%  \item Secondly, a fast reversible dipole performs the final deviation for the beam in use
%	(for example -\SI{5}{\degree} or +\SI{5}{\degree}). The polarity of this fast reversible dipole is
%	reversed every time the injector\index{linac!injector} in use is changed.
%\end{itemize}
%
%
%
%\mycomment{ap}{Figure~\ref{fig:MEBT3SchematicsDev3} is not up to date because we have a triplet insted of two doublets. I think the concept that is shown is still valid though. Should we keep it like that Vincenzo?}
%\ifdefined\EnableTeXObjects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\TeXFigure{fig:MEBT3SchematicsDev3}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\else
% \begin{figure}[h]%[tbp]
% \centering
% \includegraphics[width=4.1807in,height=5.0138in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img044.png}
% \caption{Schematics of device 3.}\label{fig:MEBT3SchematicsDev3}
% \end{figure}
%\fi
%
%With this solution, the strongest constraint, that is polarity reversal, has to be applied to a
%small dipole---which makes its design easier. For example, it can be limited to two superposed coils,
%possibly with no polar pieces making it easier to change polarity and then reduce delay for the
%establishment of the nominal magnetic field.
%
%The major drawback of this solution is the long distance between the two dipoles (see
%Fig.~\ref{fig:MEBT3SchematicsDev3}) which may be too long for a proper transport of the beam. This may be
%overcome by the insertion of focusing elements such as a doublet of quadrupoles\index{magnets!quadrupole}
%between the two dipoles.
%
%More generally, a study aiming to fix the deviation angle repartition between the two dipoles (actual
%proposal of repartition: \rpm\SI{40}{\degree} and \rpm\SI{5}{\degree}) and consequently the distance
%between the two dipoles has to be conducted. This study must include beam optics\index{beam!optics}
%requirements, possible focusing elements and dipoles design including interaction with vacuum chamber
%and driving current power supply.
%
%\begin{Alert}{}
%Any new information about the above mentioned study?
%\mycomment{ap}{No! MEBT3 is still under design!}
%\end{Alert}
%
%\subsubsection{Preliminary data for design}
%Data from Table~\ref{tab:mebt:generic-data} are the ones providing the dipole geometries used for
%Fig.~\ref{fig:MEBT3RunningCases} on page~\pageref{fig:MEBT3RunningCases}.
%
%
%\begin{table}
%\begin{center}
%\tablefirsthead{}
%\tablehead{}
%\tabletail{}
%\tablelasttail{}
%\begin{supertabular}{m{2.4768598in}m{1.0038599in}m{1.6136599in}}
%{ Parameter}        & { Value}            & { Comment}\\
%{ Protons energy}   & { \SI{17}{\MeV}}    & { At dipole inputs}\\
%{ Beam rigidity Br} & { 0,6 T.m}          & \\
%Beam diameter at $4\sigma$
%(D\textsubscript{4}{\textsubscript{%
%	$\sigma$}}) & \SI{10}{\mm}        & Order of magnitude for all dipoles\\
%``Good'' field diameter (4 $\times$
%D\textsubscript{4}{\textsubscript{%
%	$\sigma$}}) & \SI{25}{\mm}        & \\
%{ Pole width}       & \SI{65}{\mm}        & \\
%{ Dipole gap h}     & \SI{60}{\mm}        & \\
%Half dipole width
%with coil           & \SI{140}{\mm}       & For dipoles 1,2 \& 3-1\\
%Magnetic field B    & \SI{0.5}{\tesla}    & For all dipoles\\
%Curvature radius r  & {\dipoleCurvatureRadius}    & \\
%Magnetomotive force, N I = B h /
%\hbox{$\mu^0$}      & \SI{24000}{\ampere} & \\
%Number of coil turns&  240                & \\
%Coil current        & \SI{100}{\ampere}   & \\
%\end{supertabular}
%\end{center}
%\caption{Generic Data.}
%\label{tab:mebt:generic-data}
%\end{table}
%
%A quite large curvature radius of \dipoleCurvatureRadius{} is proposed in order to limit the required magnetic field to
%\SI{0.5}{\tesla} that also limits the size of the needed dipole coils.


%\mycomment{Agatera}{Please adapt title and content to format and reference}
\FloatBarrier\subsection[Collimation System]{Collimation System}
In order to control the \TBeamLosses{beam losses} in the superconducting linac, a
\TBeamInstCollim{collimating system} has been placed in the deviation line of {\LinacMEBTiii} (in between
the two \TDipoles{dipoles})---see Fig.~\ref{fig:MEBT3CollimationSystem}. Its role is to reduce the beam
halo coming from the low energy part of the machine. $3\times2$ \TBeamInstCollim{collimators}, horizontal
or vertical, are located between the \TDipoles{dipoles}. Each of them reduces the \TBeamIntensity{beam
intensity} by \SI{0.1}{\percent} or \SI{60}{\watt}. Peak losses up to \SI{180}{\watt} are to be expected
on each \TBeamInstCollim{collimator} due to transverse and longitudinal \TErrors{errors} that may occur
in the \TInjector{injector}.

% \mycomment{vdflorio}{Could you please check if the two collimators are I1-ME3:ICD-COLH-ME3 and I1-ME3:ICD-COLV-ME3? %
% 	I would then mention them here.}

\ifdefined\EnableTeXObjects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\TeXFigure{fig:MEBT3CollimatioSystem}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\else
\begin{figure}[h]%[tbp]
\hspace*{-0.06\textwidth}\includegraphics[width=1.1\textwidth]{MYRRHATDRPartB-img/D-MYRTE-2.6-Fig80.png}
\caption[Deviation section of MEBT-3 including the 3 collimators]%
	{Deviation section of \TMEBTiii{MEBT-3} including the 3 \TBeamInstCollim{collimators} (thick red lines).}
\label{fig:MEBT3CollimationSystem}
\end{figure}
\fi

\FloatBarrier
%}}}
%{{{  70 kW Beam Dumps
\subsection[\SI{70}{\kW} Beam Dumps]{\SI{70}{\kW} \TBeamDestinations{Beam Dumps}}
%
%\ulrich{This is a a basic review of possible technologies and not a TDR. If there is nothig decided %
%	     remove it all. Instead only link to the requirement document ALX-37685655}%
	%  {vdflorio}{%
	%  This document has been built capitalizing on the effort put in the past by SCK CEN for the creation %
	%  of the document "MYRRHA Accelerator: Technical Design Report" (SCK CEN/26824240), edited %
	%  by Dr. George Braud. Said document served both as a starting point to this document and as a %
	%  guideline as to what was expected as a final result. No requirements had been formulated up front %
	%  as to the level of detail, size, and contents meant for this document. Lacking those requirements, %
	%  the authors and this editor have used as a guideline the above-mentioned document. Said document was based %
	%  on a different viewpoint on what a TDR should be, and complemented up-to-date technical information %
	%  with ancillary information useful to understand the context that had led to technical decisions. %
	%  The lack of requirements as to the intended output lasted from July 1, 2019, date in which I started %
	%  working on this document, until January 31, 2000, when you sent me an e-mail %
	%  with subject "Accelerator design report title and scope", asking me to change the title and focus %
	%  of the TDR from "Myrrha TDR" to "Minerva 100 MeV accelerator technical design report". %
	%  Please consider the context before passing judgement and throwing books at people.}

The 70 kW design is still pending. Its specifications are defined in Reference~\cite{R-SCKCEN-2020-37685655.0.13}.

\FloatBarrier
%}}}
%
% EOF TDR-B02-MEBT3.tex
%
