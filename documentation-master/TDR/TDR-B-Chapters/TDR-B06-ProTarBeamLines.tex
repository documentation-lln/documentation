%
% TDR-B06-ProTarBeamLines.tex
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section[Proton Target Beam Lines]{Proton Target Beam Lines}\label{sect:ptbl}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\FloatBarrier\subsection[Introduction]{Introduction}
The goal of these lines is to perform high beam power \index{Dependability!Availability}availability for
RIB production using ISOL technique.  Extraction of both the MINERVA \SI{100}{\MeV} beam and the MYRRHA
\SI{600}{\MeV} beam are planned, as explained in this chapter.

The operational scheme of the MYRRHA linac foresees a distributed beam delivery: since the reactor
needs periodic beam interruptions for its sub-criticality monitoring, regularly spaced time slots become
available for feeding a Proton Target Facility ({\LinacTargetPTF}) with a fraction of the total beam.  Useful beam
interruptions have a duration of 2~ms; a useful beam fraction to a {\LinacTargetPTF} is around \SI{5}{\percent}.

Combining these numbers yields a beam delivery cycle length of 40~ms (a repetition rate of 25~Hz)
-- 38~ms to the MYRRHA reactor, a gross value of 2~ms available for the {\LinacTargetPTF}. Both beam-on durations
may be individually tuned towards lower values for obtaining reduced duty factors, hence average beam
intensities. This possibility allows the linac to run at constant intensity (and therefore at constant
beam dynamics) while maintaining a regulation margin of the average currents.

The mechanism of the distributed beam delivery is that of a classical extraction scheme consisting of
a kicker magnet followed by a magnetic septum as initial beam line elements towards the {\LinacTargetPTF}.

However, in the given case the kicker has a particularly long flat top of just under 2000~\hbox{$\mu$}s and
a particularly high duty cycle of \SI{5}{\percent}. These demanding parameters will essentially be transferred to a
specially designed power converter. One obviously wants to minimise rise and fall times of the kicker
pulse. However, in the given case one cannot aim at the nanosecond or microsecond level as a realistic goal.

During the rise and fall times the beam has to be entirely suppressed: this is the role of the
low energy electrostatic chopper installed in the {\LinacLEBT}\index{linac!injector!front end!LEBT} (see
Section~\ref{s:chopper}, ``Chopper''). One possible issue with the chopper
is the recovery time of the space charge compensation in the {\LinacLEBT} after switching off the voltage
(= switching on the beam). This mechanism is and will be investigated experimentally.

%{{{ Extraction Towards Proton Target Stations / 600 MeV
\ifdefined\EnableBeyondMINERVA
\FloatBarrier\subsection[Extraction Towards Proton Target Stations (\SI{600}{\MeV})]{Extraction Towards Proton Target Stations (\SI{600}{\MeV})}
At \SI{600}{\MeV} (B$\rho$ = 4.066 Tm) a number of tentative parameters have been assumed:

\begin{itemize}[noitemsep]
\item A drift length between the kicker and the entry of the septum of 4~m;
\item A \SI{100}{\percent} beam size of 20~mm;
\item A septum thickness of 6~mm.
\end{itemize}

The kicker is then expected to have the approximate characteristics reported in Table~\ref{tab:ptbl:one}.

\begin{table}
\begin{center}
\tablefirsthead{}
\tablehead{}
\tabletail{}
\tablelasttail{}
\begin{supertabular}{m{2.4677598in}m{0.35185984in}}
{ Generated septum separation (mm)} &
{ 26}\\
{ Deflection angle (mrad)} &
{ 6.5}\\
{ Magnetic field (T)} &
{ 0.1}\\
{ B.I. (mT{\textbullet}m)} &
{ 26.4}\\
{ Kicker length (m)} &
{ 0.26}\\
\end{supertabular}
\end{center}
\caption{Kicker characteristics @ \SI{600}{\MeV}.}
\label{tab:ptbl:one}
\end{table}

If a drift space between the septum and the first downstream quadrupole\index{magnets!quadrupole} of 12~m is allowed for, the
septum may have the characteristics reported in Table~\ref{tab:ptbl:two}.

\begin{table}
\begin{center}
\tablefirsthead{}
\tablehead{}
\tabletail{}
\tablelasttail{}
\begin{supertabular}{m{1.9719598in}m{0.48305985in}}
{ End of drift separation (mm)} &
{ 960}\\
{ Deflection angle (mrad)} &
{ 80}\\
{ Magnetic field (T)} &
{ 0.2}\\
{ Gap (mm)} &
{ 60}\\
{ Current (A {\textbullet}t)} &
{ 10000}\\
{ Current density (A/mm2)} &
{ 10}\\
{ Septum thickness (mm)} &
{ 6}\\
{ Septum length (m)} &
{ 1.6}\\
\end{supertabular}
\end{center}
\caption{Septum characteristics @ \SI{600}{\MeV}.}
\label{tab:ptbl:two}
\end{table}

The total bending angle of the extraction scheme will be \SI{180}{\degree}, including kicker and septum
deflections. The total beam deflection will be laid out as an achromatic bend in order to preserve the
beam characteristics on the target. An optimised layout of this \SI{180}{\degree} achromatic bend is not
yet available.
\fi
%}}}

\FloatBarrier\subsection[Extraction Towards Proton Target Stations (\SI{100}{\MeV})]{Extraction Towards Proton
Target Stations (\SI{100}{\MeV})} For the initial \SI{100}{\MeV} setup, the kicker + septum scheme is also adopted so
as to protect the target against an accidental occurrence of it being hit by the full power proton beam
(\SI{400}{\kW}), which would lead to its destruction.

It is foreseen to physically limit the length of the kicker pulse (the pulse energy) in order to avoid
such an accidental situation. At \SI{100}{\MeV} the cycle repetition rate will rather be 250~Hz (a cycle length
of 4 ms). Keeping the \SI{5}{\percent} duty cycle leads to a nominal pulse on target of 200~\hbox{$\mu$}s. Again, this
value may be adjusted at will.

The momentum ratio between \SI{600}{\MeV} and \SI{100}{\MeV} is 1:0.365. Hence, applying the unchanged
\SI{600}{\MeV} kicker will deflect the beam by \SI{17.8}{\mrad}.

Considering that:

\begin{itemize}[noitemsep]
\item The beam separations to be obtained are equivalent in both cases (26~mm on
	the septum entrance, 960~mm at the first downstream quadrupole\index{magnets!quadrupole});
\item A septum magnet with largely unchanged characteristics may be used; the
	geometry of the extraction setup may, \emph{e.g.}, be modified as:
\item The kicker--septum distance is reduced to 1.46~m;
\item The septum--quadrupole distance is reduced to 6~m;
\item The septum distance is shortened from 1.6~m to 1.168~m.
\end{itemize}

\FloatBarrier\subsection[Conclusions]{Conclusions}
This approach allows the \SI{100}{\MeV} extraction components to be relevant prototypes of the \SI{600}{\MeV} setup. As
stated earlier, this prototyping aspect is of particular importance for the kicker power converter.

Also in the \SI{100}{\MeV} case, the total bend of the extracted beam will be \SI{180}{\degree} in an
achromatic configuration. Merging the \SI{600}{\MeV} beam line and the \SI{100}{\MeV} one into a single final target
beam line should be considered.

\begin{Alert}{}
	This section is already structured so as to highlight the MINERVA case
	and the MYRRHA case! My only question here concerns the above sentence:
	have any decisions concerning the beam line merging been taken?
	If so, we need to mention this here.
\end{Alert}

\FloatBarrier
%
% EOF TDR-B06-ProTarBeamLines.tex
%
