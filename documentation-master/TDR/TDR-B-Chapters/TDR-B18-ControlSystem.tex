%
% TDR-B18-ControlSystem.tex
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section[Control System]{Control System}\label{sect:ctrl-system}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\textbf{Please note that the present section is yet to be reviewed.}

%% \input{TDR-B-Chapters/TDR-B18-NewCtrlSys.tex}
\subsection[Introduction]{Introduction}\label{Sect:NewCtrlSys:Intro}
%\subsubsection{Purpose}
The purpose of this section is to describe the \TControlSystem{Control and Interlock Systems} for the ACC project.
%  \subsubsection{Scope}
%  The scope of this document is the Control and Interlock Systems
%  for the ACC project.
%  
%  \subsubsection{Audience}
%  The target audience for this document are the engineers of the
%  ADT and ADB groups and the MDB management.
%\subsubsection{Document Overview}
% Section~\ref{Sect:NewCtrlSys:Intro} provides a general introduction.
Section~\ref{Sect:NewCtrlSys:CtrlSys} outlines the technical design of the \MINERVA{} Control System.
Section~\ref{Sect:NewCtrlSys:InterlockSys} outlines the technical design of the \MINERVA{} \TSafety{Safety} System.

% Please note that the definitions of all terms, acronyms, and abbreviations required to properly interpret
% this document are provided in a project-wide glossary given in~\cite{R-SCKCEN-2020-8905079}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection[MINERVA Control System]{MINERVA Control System}\label{Sect:NewCtrlSys:CtrlSys}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The \MINERVA{} Control System (MCS) is the system responsible for the configuration, control, monitoring
and \TFaultDiagnosis{diagnostics} of the primary systems which include the ACC. This section provides a conceptual
overview. Detailed and final design decisions on hardware, software and concepts will be outlined in
the \TControlSystem{CIS} Architecture and its immediate subsystems architecture documents. This overview provides the
current understanding of the design of the \TControlSystem{MCS} and is subject to analysis as part of the \TControlSystem{CIS} architecture.

\subsubsection{Logical View}
The \TControlSystem{MCS} software follows a multi-layer architecture as outlined in
Fig.~\ref{fig:NewCtrlSys:MultiLayerSWArchExample}. Software in a layer may only make use of software
in the same or lower layers. The \TControlSystem{MCS} software will rely on a single common control
system framework named \SwCmpEPICS~\cite{O-WebPage-EPICS-d} to provide common communication protocols
for efficient communication and data exchange and role based authentication and authorization throughout
the control system. All software will be executed on a Linux based system based on RHEL or CENTOS.

\begin{figure}[H]%[tbp]
\centering
\includegraphics[width=0.6\textwidth]{MYRRHATDRPartB-img/MINERVA20ACC20CIS20TDR1-img002.pdf} 
\caption[Example for Multi-Layer Architecture of the MCS software (grey) and primary systems (white).]%
	{Example for Multi-Layer Architecture of the \TControlSystem{MCS} software (grey) and primary systems (white).}
\label{fig:NewCtrlSys:MultiLayerSWArchExample}
\end{figure}


\paragraph{User Interface Layer.}
The User Interface Layer provides all remote user interfaces to
control, monitor and operate the primary systems including simple state-less user interfaces and
interfaces to procedures to automate activities. User Interfaces may be non-operational without
affecting the overall \TAvailability{availability} and \TReliability{reliability} for \MYRRHA{} operation. All user interfaces will
be designed following a common user interface guidelines document considering operational aspects.
They are intended to be implemented using a common user interface framework:

\begin{itemize}
\item \textbf{Service User Interfaces} allow to remotely interact with services in the service layer
of the control system, for example alarming system and logging system.
\item \textbf{Front-End User Interfaces} allow to remotely interact with software in the front-end
layer of the control system. A dedicated user interface will be provided for each software
component.
\item \textbf{Procedure User Interfaces} allow to remotely interact with procedures in the service
layer used to automate activities, for example for machine development. 
\item \textbf{Common User Interfaces} for generic control system functionalities and view spanning
multiple device types.
\end{itemize}

\paragraph{Service Layer.}
The Service Layer provides non \TRealTime{real-time} services shared by
multiple software components. These services may be \TSWFailures{non-operational} without affecting the overall
\TAvailability{availability} and \TReliability{reliability} for \MYRRHA{} operation.

\begin{itemize}
\item The \textbf{Alarm Service} allows to define alarms based on conditions of acquisition values
which in turn are visualised to the operator and to notify the operator in case of deviations.
\item The \textbf{Archive Service }is responsible to persistently store process values of interest
(i.e. control and acquisition values) for later consumption.
\item The \textbf{Report Service} allows to define and generate reports (i.e. trending and other
offline data analysis) based on data from the Archiving System and Configuration Service.
\item The \textbf{Log Service} provides a means to collect application logging (i.e. log4j) and
persistently store it for post-mortem analysis.
\item The \textbf{LogBook Service} is an electronic logbook used to collect and store activities and
results in an informal fashion used especially during machine development.
\item The \textbf{Configuration Service} holds all configuration, calibration and control system
related data used to operate the control system and primary systems.
\item The \textbf{Allocator Service} is a service that grants users (physical and experiment)
exclusive access to a single device or a list of devices.
\item The \textbf{Procedure Service} allows to implement procedures used to automatize machine
\TCommissioning{commissioning} and operational activities.
\end{itemize}

\paragraph{Front-End Layer.}
The Front-End Layer contains Front-End Devices (FEDs) that
interface with devices of the primary systems and systems with \TRealTime{real-time} performance requirements
(i.e. Timing and Triggering System). FEDs provide unified interfaces to higher layers through
process variables. This includes a common state machine and mode specific implementations for the
specific primary system device. The FED usually consists of multiple parts that may fall into the
following categories:

\begin{enumerate}
\item \textbf{Software for \TDeterminism{non-deterministic functionality}} which is implemented in software and
executed on an operating system.
\item \textbf{Firmware for \TDeterminism{deterministic} \TRealTime{real-time} functionality} which is implemented in VHDL or
VERILOG and executed on an FPGA based card in the system
\end{enumerate}

\paragraph{Local Control Layer.}
The Local Control Layer contains regulation loops and may
provide custom interfaces to the layer above. This layer may implement operational states but
should not implement operational modes to ease integration into the \TControlSystem{MCS}. Responsibility for this
layer may be the primary system owners or the \TControlSystem{MCS} owner depending on \TAvailability{availability} as COTS and
\TAvailability{availability} of knowledge in the groups.

\subsubsection{Physical View}
Similar to the logical view, the \TControlSystem{MCS} hardware will follow a multi-tier architecture as outlined in
Fig.~\ref{fig:NewCtrlSys:MultiLayerHWArchExample}. Hardware in a tier may only make use of hardware in
the same or lower tiers.

\begin{figure}[H]%[tbp]
\centering
\includegraphics{MYRRHATDRPartB-img/MINERVA20ACC20CIS20TDR1-img003.pdf}
\caption[Example for Multi-Tier Architecture of the MCS hardware (grey) and primary systems (white).]%
	{Example for Multi-Tier Architecture of the \TControlSystem{MCS}  hardware (grey) and primary systems (white).}
\label{fig:NewCtrlSys:MultiLayerHWArchExample}
\end{figure}

\paragraph{Presentation Tier.}
The Presentation Tier contains multiple ZeroClients. Some
ZeroClients are used as either work stations in the central control room for day-to-day operation
and machine development activities and as work stations in local control rooms for debugging and
\TCommissioning{commissioning} activities close to the primary systems. Additional ZeroClients are used to display
status information in the central control room and across the facility on dedicated visualization
displays. ZeroClients connect to the virtual machine infrastructure in the Service Tier to display
the user interface and provide means for interaction through mouse and keyboard where required.

\textit{Software:} ZeroClient will only execute commercial off-the-shelf firmware but no software
outlined in the logical view. 

\textit{Proposed hardware: }Standard IT ZeroClients compatible with virtualization hardware and
software in the Service Tier.

\paragraph{Service Tier.}
The Service Tier provides physical 19-inch rack mounted servers
that execute a virtual machine monitor (hypervisor). Virtual machines will execute:

\begin{itemize}
\item \textbf{User Interfaces} with Presentation Layer software used by the Presentation tier
equipment and 
\item \textbf{Services} with Service Layer software
\end{itemize}
Depending on the IT services, remote access to dedicated User Interface virtual machines may be
granted from the office network or from offsite for suppliers to assist in debugging and
\TCommissioning{commissioning} activities.

\textit{Software:} Presentation Layer and Service Layer software executing in virtual machines

\textit{Proposed hardware:} Standard IT servers with virtualization capabilities.

\paragraph{Resource Tier.}
The Resource Tier contains Front-End Controllers (FECs) responsible for control, monitoring and triggering
of devices part of the primary systems and services with \TRealTime{real-time} performance. FECs are based on a
common and modular hardware platform that provides various cards for analogue inputs, digital inputs
and outputs, fieldbuses and the option for custom developed cards.

\textit{Software:} Front-End Layer software; Local Control Layer software if COTS items in Device
Tier do not cover the local control software.

\textit{Proposed hardware:} \SwCmpMicroTCA{} with FPGA based cards

\subparagraph{Timing and Triggering System.}
The Timing and Triggering System (TTS) provides a common timestamp to all Front-End Devices,
provides means to generate synchronised triggers for \TBeam{beam} generation and measurement acquisition.
It also processes requests from the experiments and \MYRRHA{} to generate \TBeam{beam} pulses with requested
pulse lengths to the requestors.

\textit{Proposed hardware:} MicroResearch Finland or WhiteRabbit

\subparagraph{Data Acquisition System (optional).}
The Data Acquisition System provides generic and system independent remote oscilloscope
functionality based on common FEC hardware platform.

\subparagraph{Fault Recovery System.}
The \TErrorRecovery{Fault Recovery System} (FRS) is responsible to execute the \TFaultTolerance{fault
tolerance} algorithm. It receives the following data through a \TDeterminism{terministic channel} provided by the
Timing and Triggering System:

\begin{itemize}
\item \textbf{Failure Information} indicating if and which system is in the process of failing, i.e.
from {\LinacLLRF}
\item \textbf{Monitoring information} required for executing fault-\TRedundancy{redundancy} algorithm, i.e. \TBeam{beam}
position at specific points in the accelerator
\end{itemize}
Based on this information, the FDS will use the \TDeterminism{deterministic data channel} to request
reconfiguration of the primary systems and subsequently execute the \TErrorRecovery{fault recovery}
algorithm by ramping up the \TBeamEnergy{beam energy} slowly measuring specific properties of the
\TBeam{beam} from pulse to pulse.

The \TErrorRecovery{fault recovery} algorithm is intended to be implemented using a staged approach:

\begin{enumerate}
  \item \textbf{Procedure:} Implement only \TBeam{beam} inhibit in the local FPGA and provide the \TFailures{failure}
	information through the IOC interface to be consumed by a procedure in the Service Layer which
	implements a \TFaultTolerance{fault tolerance}.
\item \textbf{Software:} Implement \TBeam{beam} inhibit in the local FPGA and the \TFaultTolerance{fault tolerance} in
	the IOC \TSWFailures{software}.
\item \textbf{Firmware:} Implement \TBeam{beam} inhibit and \TFaultTolerance{fault tolerance} in the local FPGA.
\end{enumerate}
Using this stages approach, will allow expert users to try out different \TErrorRecovery{fault recovery} algorithms
safely as the procedure service will allow a terminal like interface for expert users. The clearer
the picture of the chosen algorithm gets, the lower it shall move in the system making it less
flexible but faster and in the end \TDeterminism{deterministic}.

\paragraph{Device Tier.}
The Device Tier contains the device of the primary systems including COTS items for local control,
for example power converters and \TVacuumSystem{vacuum controllers}. In this respect, all or parts of
the Local Control Layer software will be deployed on these local control systems. If no dedicated Local
Control Layer software is available as part of the COTS items, integration into the Resource Tier shall
be considered.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection[MINERVA Interlock System]{MINERVA Interlock System}\label{Sect:NewCtrlSys:InterlockSys}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The \MINERVA{} \TInterlockSystem{Interlock System} (MIS) contributes to reduce the
risk of harming people (\TPPS{Personnel Protection}) or damaging equipment (\TMPS{Machine Protection}) due to
device malfunction and unintended irradiation with respect to the operation of the primary systems
(ACC, {\LinacTargetPTF} and {\LinacTargetFTS}). Detailed and final design decisions on hardware, software and concepts will be
outlined in the \TControlSystem{CIS} Architecture and its immediate subsystems architecture documents. This overview
provides the current understanding of the design of the \TInterlockSystem{MIS} and is subject to analysis as part of
the \TControlSystem{CIS} architecture.

Sensors, actuators, detection of \TInterlock{interlock} conditions and handling of \TInterlock{interlocks}
are responsibility of the primary and secondary systems. The \TInterlockSystem{MIS}  is responsible to
implement the logic connecting different devices to provide a \MINERVA{} wide protection of the primary
systems and their users.

The \TInterlockSystem{MIS}  should adhere to the following design guidelines:

\begin{itemize}
  \item \textbf{Keep it Simple} -- only implement \TSafety{safety} relevant functionality. Other
	functionality shall be taken care off by other systems, for example monitoring by the \MINERVA{}
	control system.
  \item \textbf{Independent of Control System }{}-- designed orthogonally to the \MINERVA{} Control System.
  \item \textbf{State and Mode Independence} -- independent of operational modes and states. Instead
	rely on information from sensors.
  \item \textbf{Uniform \TInterlockSystem{Interlock} Interfaces} -- rely on a limited number of interface types,
	preferably a single one but at most two, one for machine and one for \TPPS{personnel protection}.
  \item \textbf{Manual and Automatic \TErrorRecovery{Recovery} }{}-- support two kinds of
	\TErrorRecovery{recovery mechanisms}: (a) immediately when initial condition goes away (automatic
	\TErrorRecovery{recovery}) and (b) only upon operator acknowledgement after the initial condition
	went away (manual \TErrorRecovery{recovery}).
  \item \textbf{Fail-Safe Design} -- designed in a fail-safe manner to generate \TInterlock{interlocks}
	when an \TInterlock{interlock} condition is raised, a cable broke, a cable is not connected,
	an \TErrors{internal error} was detected or power was cut.
\end{itemize}

Both the \TMPS{Device} \TInterlockSystem{Interlock System} and the \TPPS{Personnel}
\TInterlockSystem{Interlock System} follow the same concept but differ in implementation due to differing
needs on standards and reaction times.

\subsubsection{Device Interlock System}
The \textbf{Device \TInterlockSystem{Interlock System}} contributes to reduce the risk of
damaging equipment caused by erroneous situations (i.e. conflicting commands, malfunctioning). The
\TInterlockSystem{DIS} requires reaction times in the lower microseconds range and will be designed
to be working \TReliability{reliably} but is not required to be developed in accordance to specific
\TSafety{safety} standards.

\textit{Proposed Software and Hardware:} The \TInterlockSystem{DIS} is planned to be implemented on
FPGAs and to rely on optical interfaces for fast reaction times\textbf{ }and potentially on the same
technologies as the \TPPS{Personnel} \TInterlockSystem{Interlock System} for slow reaction times.

\subsubsection{Personnel Interlock System}
The \textbf{Personnel \TInterlockSystem{Interlock System} (\TPPS{PIS})} contributes to minimize the risk
of unintended exposure to ionizing radiation and other harm for personnel. The \TInterlockSystem{PIS}
requires reaction times in the hundred milliseconds range but needs to follow specific \TSafety{safety}
standards.

\textit{Proposed Software and Hardware:} The \TPPS{PPS} is planned to be implemented on a fail-safe
\TPLCs{PLC} platform certified for \TSIL{SIL-2}~\cite{B-2016-SmithSimpson-ButterworthHeinemann} and
IEC61508~\cite{P-EMES99-Decon} and rely on relay-based, 24V and dry contact interfaces.

\FloatBarrier
%
% EOF TDR-B18-ControlSystem.tex
%
