%
% TDR-B21-CommissDecommiss.tex
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section[Commissioning]{Commissioning}\label{sect:commiss}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\FloatBarrier\subsection[Commissioning]{Commissioning}

The present section currently discusses the cavity \TCommissioning{commissioning} steps. Further \TCommissioning{commissioning} information
shall be provided in future versions of this document.

\ifdefined\UseOldCommissioningChapter
To drive the reactor power, the mean beam power needs to be controlled and varied over a quite large
range, from typically \SI{20}{\percent} to \SI{100}{\percent} of the maximum power~\cite{J-NuclTechnol-2013-184.2-Rimpault}. The
most efficient and simple way to do it is to vary the mean beam intensity keeping constant the beam
energy. In practice, this can be performed in two ways: by varying the overall peak current (see
Fig.~\ref{fig:comm-decomm:beam-time-structs}, top), or by varying the duty cycle at constant peak current
(see Fig.~\ref{fig:comm-decomm:beam-time-structs}, bottom).


\begin{Alert}{}
The examples in Fig.~\ref{fig:comm-decomm:beam-time-structs} should be valid for MINERVA too, am I correct?
\end{Alert}

\begin{figure}[h]%[tbp]
\centering
\includegraphics[width=5.8209in,height=2.6138in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img282.png}
\caption[Possible beam time structures for half-power operation]{Possible beam time structures for half-power operation.\newline
	 Top: varying the peak current: \SI{1.14}{\MW} to reactor, \SI{57}{\kW} to ISOL;
	 Bottom: varying the duty cycle (preferred solution): \SI{1.14}{\MW} to reactor, \SI{114}{\kW} to ISOL.}
\label{fig:comm-decomm:beam-time-structs}
\end{figure}

Even if the first option has the advantage of keeping a more ``close to CW'' time structure, the second
method brings two considerable advantages as far as beam operation is concerned:

\begin{itemize}[noitemsep]
  \item The peak beam current is always kept constant (\SI{4}{\mA}), meaning that \TSpaceCharge{space charge}{}
	effects and therefore beam dynamics remain exactly the same whatever the required beam power. This is a
	crucial advantage because in this case, the accelerator elements (especially the low-energy ones actually)
	do not need to be re-tuned while changing the beam power. This should translate in a greater simplification and
	\TReliability{reliability} for the whole facility operation. Only the low-energy beam
	chopper rhythm needs to be changed to cope with a beam power variation demand.

  \item Moreover, this second method allows the beam power sent to the reactor to vary
	without affecting the beam power sent to the ISOL facility, leading to a total decoupling of the two
	applications. Such independence is not feasible using the first option (unless the main beam pulse 250
	Hz repetition rate is modified and therefore all the machine synchronisation re-tuned).
\end{itemize}

The method using a pulsed beam at constant peak current is therefore to be preferred for beam power
control, at least as a baseline approach, especially as it also ensures that beam interruptions seen
by the reactor will never exceed 4 milliseconds during nominal operation (under the assumption that no
failures are experienced).

It is nevertheless to be pointed out that, due to the pulsed nature of the beam, special care will have to
be taken with beam transients' management. In particular, the beam \TSpaceChargeCompensation{space charge compensation}{} rising time,
which plays a significant role in low-energy magnetic beam transport lines, will need to be carefully
assessed and its effect on beam stability\index{beam!stability} minimised.

This physical process, which mainly depends on the gas nature and pressure conditions in the beam line,
is complex to model and understand~\cite{P-EPAC94-Reidel,P-LINAC10-Chauvin,P-IPAC13-Uriot}. Dedicated
experimental measurements at the UCLouvain injector\index{linac!injector} test stand~\cite{P-TCADS13-Salemme}
are to be foreseen on this topic.

\begin{Alert}{}
	Any update on experimental measurements?
\end{Alert}

\subsubsection{Beam power ramping up}
During the first beam tunings and the \TCommissioning{commissioning} phases of the accelerator and of the whole MYRRHA
plant, the duty cycle will need to be extremely low (typically \num{d-4} or lower) for machine protection
and \TSafety{safety} reasons. This will require to use a much lower beam repetition
rate of typically \SI{1}{\hertz}. From there, the beam duty cycle and repetition rate will then be increased step
by step to reach nominal beam operation. A possible scheme for this power ramping up is the following.

\begin{itemize}[noitemsep]
  \item Start the first tunings with a ``pencil'' beam, using for example \SI{100}{\micro\second} pulses
	at \SI{1}{\hertz} with \SI{0.1}{\mA} peak current (\emph{i.e.} a \SI{6}{\watt} beam). After this phase, the longitudinal
	tuning of the linac should be correct.

  \item Increase step-by-step the peak current from \SI{0.1}{\mA} to the nominal \SI{4}{\mA} (\emph{i.e.} increase the beam power
	from \SI{6}{\watt} to \SI{240}{\watt}). After this phase, the transverse tuning of the linac should be correct.

  \item Increase step by step the repetition frequency from \SI{1}{\hertz} to \SI{250}{\hertz} (\emph{i.e.} increase the
	beam power from \SI{240}{\watt} to \SI{60}{\kW}). During this phase, attention should be focused on the
	minimisation of possible beam pulse transients. Note that when reaching \SI{10}{\hertz} (\SI{2.4}{\kW}),
	the \SI{100}{\milli\second} threshold duration between two pulses is reached and beam can be
	sent inside the reactor ``careless''.

\begin{Alert}{}
	Is the above correct? ``Careless''?
\end{Alert}

  \item Increase step by step the pulse length from \SI{0.1}{\milli\second} to the nominal length (\emph{i.e.} increase
	the beam power from \SI{60}{\kW} to the nominal MW-class power).
\end{itemize}

Such a procedure will need to be followed in several cases: accelerator first \TCommissioning{commissioning} phase
(duration for a full power ramping up: weeks/ months); accelerator restarts after maintenance phases
(duration: hours/ days); reactor power ramping up (duration: minutes/hours); fast beam restart after a
less than 3 seconds beam interruption (duration: seconds, see after).

This very last case especially underlines that such a procedure will need to be full-term completely
managed by the machine control system and associated instrumentation in a complete automated way (which
is very atypical in existing high power accelerator).

\subsubsection{Injector beam reconfiguration in fault cases}\index{linac!injector}
The \SI{16.6}{\MeV} MINERVA injector line~\cite{P-LINAC12-Zhang} is designed to provide optimal
acceleration efficiency with a minimised number of components. It is composed with a 30 kV ECR
proton \TSource{source}\index{linac!injector!front end!proton source} and its 2-metre long Low Energy Beam
Transport ({\LinacLEBT}\index{linac!injector!front end!LEBT}), a 4-metre long
\SI{\TWinIiLEfreqValueAsInt}{\mega\hertz} 4-rod RFQ\index{linac!injector!front end!quadrupole!RFQ}~\cite{P-LINAC12-Vossberg}
accelerating the beam to \SI{1.5}{\MeV} and operating with very conservative intervane voltage
(30 kV), followed by a \index{linac!injector!booster}---still under fine optimisation within the
MAX\index{Projects!MAX} project---made with several room-temperature and then superconducting multi-cell
CH cavities~\cite{P-LINAC12-Mader}.

To increase the \TReliability{reliability}, the approach consists in doubling the whole
\SI{16.6}{\MeV} linac, providing a hot stand-by spare injector\index{linac!injector} able to quickly resume
beam operation in case of any failure in the main one~\cite{J-NuclInstrumMethodsPhysRes-2006-562.2-Bia}. The
fault-recovery procedure is based on the use of a switching dipole magnet\index{magnets!dipole} with
laminated steel yoke connecting the two injectors through a `doublebranch' Medium Energy Beam Transport
(MEBT)\index{linac!MEBT} line, as shown in Fig.~\ref{fig:comm-decomm:mebtthree}.


\begin{Alert}{}
	Could you please check whether the above is correct?
	``Doubling the whole \SI{16.6}{\MeV} linac'' is not the same as using a hot stand-by spare injector.
\end{Alert}

\begin{figure}[h]%[tbp]
\centering
\includegraphics[width=5.6272in,height=3.5472in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img283.png}
\caption{Layout of MEBT3 (2-injector layout).}
\label{fig:comm-decomm:mebtthree}
\end{figure}
In the case of a fault, the injector\index{linac!injector} beam reconfiguration should last no more than \SI{3}{\second}.
The main steps of the reference scenario for such a re-tuning are the following:

\begin{itemize}[noitemsep]
  \item In the initial configuration, one of the injectors\index{linac!injector} (\emph{e.g.} Injector-1)
	is providing beam to the main linac. The other parallel injector (\emph{e.g.} Injector-2) is also fully
	operational but the produced beam is sent to a dedicated beam dump and continuously monitored.

  \item A serious fault is detected during operation and the beam is immediately and
	automatically stopped at the \TSource{source} exit in both injectors\index{linac!injector} by the Machine Protection
	System\index{Dependability!Machine protection}\index{linac!Control System!Machine Protection System}
	(MPS), by means of the {\LinacLEBT}\index{linac!injector!front end!LEBT}
	chopper (as a first step).

  \item The origin of the fault is analysed and diagnosed by the Control System.

  \item If the fault is indeed localised in Injector-1, the necessary injector\index{linac!injector} re-tuning
	procedure is started, and a new beam path is set to be able to feed the main linac using Injector-2;
	in particular, the polarity of the power supply feeding the MEBT\index{linac!MEBT} switching magnet is
	changed and Injector-2's \SI{45}{\degree} dipole is switched on.

  \item Once steady-state is reached in the re-tuned magnets, the beam is resumed in
	Injector-2 at first with very short pulses to check the transport is up to the target; the duty cycle
	is then ramped within a second or so (``fast \TCommissioning{commissioning} mode'') to recover nominal beam operation.

  \item Once beam operation is resumed, maintenance is then started on the failed Injector-1,
	the vault of which is supposed to be accessible during operation of Injector-2. If the failure can
	be fixed, Injector-1 is then re-tuned and the beam is re-\TCommissioning{commissioned} locally on the dedicated beam dump.
	Injector-1 then stays in ``hot standby mode'', ready to relieve Injector-2 in case of failure.
\end{itemize}

\begin{Alert}{}
Note: error recovery strategies such as the one above can be expressed via
a custom scripting language~\cite{R-Arxiv-2016-1611.01690-DeFV} as detailed
\href{https://www.jottacloud.com/s/19028b507f19e8d4141943656cadad3a686}{here} (p. 15f{}f).
\end{Alert}

\subsubsection{Main linac beam reconfiguration in case of faults}
The present architecture and main parameters of our superconducting linac~\cite{P-SLHiPP12-Bia}
are summarised in Table~\ref{tab:mebt:linac-params} in Sect.~\ref{bkm:Ref497895959} on
page~\pageref{bkm:Ref497895959}. It is composed of a periodic array of independently-powered superconducting
cavities~\cite{P-TCADS13-ElYakoubi} with moderate energy gain per cavity and regular focusing lattices.

The linac is designed to increase as much as possible the tuning flexibility and ensure a very large beam
acceptance so as to provide sufficient margins for the implementation of a fault tolerance capability
by serial \TRedundancy{redundancy}, where a missing element's
functionality can be replaced by re-tuning other elements with nearly identical functionalities. Such
a fault tolerance scheme can typically be applied to the failures of focusing elements or---more
crucial because much more common---of RF units. Several beam dynamics studies have already been
performed~\cite{P-EPAC04-Bia,R-NEA-2015-2015.7-Carneiro} yielding the certainty of the theoretical
feasibility of such a fault tolerant scheme. Furthermore the scheme was verified experimentally in the
SNS to a certain extent~\cite{P-HPPA07-Galambos}.

The present reference scenario for a fault-recovery procedure in the MYRRHA main linac is based on the
use of a local compensation method, in which only adjacent elements are used to recover nominal beam
operation. This procedure should not last more than \SI{3}{\second} and is defined as follows:

\begin{itemize}[noitemsep]
  \item In the initial configuration, the main linac is operational, with all elements
	operating with nominal (derated) parameters. The typical needed derating level has been evaluated
	to about \SI{30}{\percent} for cavities (accelerating field), \SI{40}{\percent} for amplifiers (RF power) and \SI{10}{\percent} for
	quadrupoles\index{magnets!quadrupole} power supplies.

  \item A serious fault is detected during operation (abnormal \index{beam!loss}beam
	loss for example) and the beam is immediately and automatically stopped at the \TSource{source} exit in the
	operating injector\index{linac!injector} by the Machine Protection System\index{Dependability!Machine
	protection}\index{linac!Control System!Machine Protection System} by means of the
	{\LinacLEBT}\index{linac!injector!front end!LEBT} chopper.

  \item The origin of the fault is analysed: if it is successfully diagnosed and can be
	compensated, the fault recovery procedure is initiated; otherwise, the beam needs to be stopped
	permanently. The proposed basic preliminary rules to be fulfilled to make the compensation possible
	are the following:

	\begin{itemize}[noitemsep]
	  \item Fault implying a cavity: the 4 nearest neighbouring cavities operating derated
		(\emph{i.e.} not already used for compensation) are used for compensation; the maximum allowed number of
		consecutive failed cavities is 2 (in sections \#1 \& \#2) or 4 (section \#3);

	  \item Fault implying a quadrupole\index{magnets!quadrupole}: the whole doublet is switched
		off and the 4 neighbouring doublets are used for compensation; the maximum allowed number of consecutive
		failed doublets is 1.
	\end{itemize}

  \item The recovery procedure is then processed as follows (example of a cavity fault~\cite{T-ParisSud-2011-Bouly}):

	\begin{itemize}[noitemsep]
	  \item The failed cavity RF loop is immediately disabled and the cavity quickly detuned
		by typically more than 100 bandwidths to avoid the beam loading effect when the beam is resumed (time
		budget: less than \SI{2}{\second}).

	  \item In parallel, new (Voltage/Phase) set-point values for compensating cavities are
		picked in the Control System database; these values should have been determined beforehand, from past
		beam experience if the fault configuration has already been met, or from a predictive calculation using a
		dedicated beam dynamics \TSimulation{simulation} code if not. The new set-points are then applied in the corresponding
		{\LinacLLRF}\index{Radio-Frequency System!LLRF} systems.
	\end{itemize}

  \item Once steady-state is reached in the re-tuned cavities (or magnets), the beam is
	resumed in the linac at first with very short pulses to check the transport is up to the target; the duty
	cycle is then ramped within a second or so (``fast \TCommissioning{commissioning} mode'') to recover nominal beam operation.

  \item Once beam operation is resumed, maintenance is then started if possible, \emph{i.e.} if the
	faulty element is located outside the tunnel. If the repair is clearly successful, the opposite procedure
	could be envisaged to come back to the initial configuration.
\end{itemize}

Like for the injector\index{linac!injector} fast reconfiguration procedure, several points will not be
straightforward to set up and will require dedicated studies and R\&D. The switching time of \SI{3}{\second}
will clearly be a critical issue, with probably huge consequences on the required capabilities of the
machine control system (efficient and fast fault diagnostic, fast automated beam restart and associated
consequences, etc.)

Also, an efficient predictive beam \TSimulation{simulation} code will need to be developed and benchmarked during the
machine \TCommissioning{commissioning} phase so as to be able to efficiently predict the optimal re-tuning set points in
every fault configuration. On the RF cavity side, fast and reliable cold tuning systems and adequate
{\LinacLLRF}\index{Radio-Frequency System!LLRF} digital systems also need to be developed. On these aspects,
a R\&D program is on-going within the MAX\index{Projects!MAX} project to experimentally demonstrate the
main steps of this recovery procedure.

The proposed local recovery system has the advantage that a minimal number of cavity settings need to
be modified, which should optimise \TReliability{reliability}. A disadvantage is that
a significant cavity voltage increase (up to \SI{30}{\percent}) is required to be reliably delivered in a short time
(hundreds of milliseconds) after being run at the derated lower voltage for perhaps months. This possible
issue will need to be evaluated and in any case, the capability of cavities to increase their accelerating
field by \SI{30}{\percent} will have to be regularly checked, typically at each maintenance period.

Another back-up recovery approach could be to adopt a non-local recovery system in which all cavities are
running at maximum gradients while some ``back-up'' cavities are kept available at the high-energy linac
end, at full voltage but in a non-accelerating mode (at -\SI{90}{\degree} or \SI{90}{\degree} synchronous
phase). In such a recovery scheme, the phases of all downstream cavities between the failed cavity and
the end of the linac are therefore re-tuned, but the accelerating field is kept constant everywhere. This
alternative scheme is presently being analysed within the MAX\index{Projects!MAX} project, and first
results tend to show that such a global re-tuning would induce more beam mismatching than a local one,
making the compensation of multiple faults more difficult.

The final choice of strategy (local vs global) and of available acceleration margins (\SI{30}{\percent}
presently) will in any case be re-assessed in the light of the results of the on-going MYRRHA linac
\TReliability{reliability} analysis~\cite{R-NEA-2015-2015.7-Pitigoi}, that will especially
give information on the expected maximum number of failed cavities to be compensated simultaneously.

\subsubsection{Beam monitoring \& protection systems}
Beam diagnostics will need to be deployed all along the accelerator in order to monitor the beam
properties from place to place and thus be able to tune the first beams, maintain normal operation
according to specifications, and protect the beam line equipments in case of malfunctioning. Due to the
high beam power in the MINERVA and MYRRHA linacs, special care should be given to operate with the lowest possible
\index{beam!loss}beam loss (below typically 1 W/m) in order to induce a low enough activity for hands-on
maintenance. Section~\ref{sect:BLM} provides a detailed description of
the MINERVA approach to beam loss monitoring.

Table~\ref{tab:comm-decomm:instr-needs} shows a very preliminary part-count of the MYRRHA needs in terms of
beam instrumentation.

\begin{table}
\begin{center}
\tablefirsthead{}
\tablehead{}
\tabletail{}
\tablelasttail{}
\begin{supertabular}{|m{1.3663598in}|m{0.8400598in}|m{0.58655983in}|m{0.6281598in}|m{1.2004598in}|m{1.1983598in}|}
\hline
 &
{ Double injector} &
{ Main linac} &
{ {\LinacHEBT}} &
{ Used for beam tuning \& adjustment} &
{ Used as input MPS}\\\hline
{ Beam position monitors} &
{ 12} &
{ 52} &
{ 15} &
{ X} &
{ TBD}\\\hline
{ Beam transverse profilers} &
{ 15} &
{ 12} &
{ 15} &
{ X} &
\\\hline
{ Emittance-metres} &
{ 4} &
{ {}-} &
{ {}-} &
{ X} &
\\\hline
{ Beam current monitors} &
{ 6} &
{ 2} &
{ 2 to 6} &
{ X} &
{ X}\\\hline
{ Faraday cups} &
{ 8} &
{ {}-} &
{ {}-} &
{ X} &
\\\hline
{ Beam energy monitors} &
{ 2} &
{ {}-} &
{ 1} &
{ X} &
{ TBD}\\\hline
{ Bunch length monitors} &
{ 4} &
{ 12} &
{ {}-} &
{ X} &
\\\hline
{ Beam loss monitors} &
{ 4} &
{ 52} &
{ {\textgreater} 6} &
{ X} &
{ X}\\\hline
{ Halo monitors \& slits} &
{ 5} &
{ {}-} &
{ 13} &
{ X} &
{ X}\\\hline
{ Beam monitoring on target} &
{ {}-} &
{ {}-} &
{ 1} &
{ X} &
{ X}\\\hline
\end{supertabular}
\end{center}
\ifdefined\EnableBeyondMINERVA
  \caption{Approx. beam instrumentation needs for the MYRRHA linac.}
\else
  \caption{Approx. beam instrumentation needs for the MINERVA linac.}
\fi
\label{tab:comm-decomm:instr-needs}
\end{table}

Even if some intrusive sensors will be required for specific measurements (\emph{e.g.} halo monitors during nominal
operation, emittance-meters, wire profilers and bunch length monitors for first beam tunings at low duty
cycle), the large amount of beam energy deposited in any material forces to use only non-interceptive
beam sensors at full power.

In particular, the beam power sent to the reactor will be constantly monitored by means of a beam
current measurement associated to a time of flight energy measurement located in the high-energy
transport lines~\cite{D-MAX-4.3}. The obtained power value will be surveyed and will need to
stay constant within {\textpm}\SI{2}{\percent} (uncontrolled beam trip\index{beam!trip}s excluded) on a typical time
scale of \SI{100}{\milli\second}. Any deviation will trigger a (controlled) beam shutdown if needed. The beam footprint
on the reactor window, which needs to be circular \SI{85}{\milli\meter} diameter within {\textpm}\SI{10}{\percent} (with a doughnut
beam distribution profile), will be also constantly monitored using a near-target profile monitor and
associated \TSafety{safety} collimators.

It is to be pointed out that such a near-target imaging system, which could be similar to the VIMOS system
developed at PSI~\cite{P-DIPAC09-Thomsen} or to the SNS target imaging system~\cite{P-BIW10-Blokland},
will have a crucial role in both the tuning of the line and the monitoring \& surveying of the beam footprint
on target, and will require dedicated R\&D to be adapted to the MINERVA (and then MYRRHA) case.

For \TSafety{safety} reasons, the proton beam will have to respect
stringent specifications, especially in terms of beam stability\index{beam!stability} and
\TReliability{reliability}. It is therefore crucial to be able to switch off the beam
as fast as possible when some irregular circumstances are detected, either linked with the accelerator
operation (\emph{e.g.} unexpected \index{beam!loss}beam loss level), with the nuclear core operation (\emph{e.g.}
unexpected high criticality level), or with personnel \TSafety{safety} (\emph{e.g.} people
entering a forbidden zone during operation).

Such fast beam shutdowns will be managed by the Machine Protection \index{Dependability!Machine
protection}\index{linac!Control System!Machine Protection System}\index{Dependability!Fault
tolerance!interlock}interlock System (MPS) that will turn off the beam as soon as possible (in typically
less than a few tens of microseconds) when abnormal conditions are detected. In our cases, the basic
mechanism will probably be to cut the beam in the injector\index{linac!injector} section, acting on the
chopper of the {\LinacLEBT}\index{linac!injector!front end!LEBT} line.

For \TRedundancy{redundancy}, the RFQ\index{linac!injector!front
end!quadrupole!RFQ} or/and the \LinacSource{}\index{linac!injector!front end!proton source} RF
drive can be also cut off. In a second step, if the fault can not be fixed quickly by the ``fast
fault-diagnostic'' system and/or the compensation schemes mentioned earlier, the Faraday cup of the
{\LinacLEBT}\index{linac!injector!front end!LEBT} will be also inserted, meaning
that the beam is stopped for a while.

Instrumentation and especially beam diagnostics will be the main input to the fast
MPS\index{Dependability!Machine protection}\index{linac!Control System!Machine Protection System}. Beam
loss monitors detect \index{beam!loss}beam loss that can cause radiation and thermal damage to equipment in
the beam line tunnels. Differential current measurements, performed using beam current monitors, and halo
monitors are also to be used for \TRedundancy{redundancy}. Other signals
like those coming from \hyperref[sect:beam-inst:bpm]{beam position monitors} can also be useful to anticipate a \index{beam!loss}beam loss.

Moreover, several other \index{Dependability!Fault tolerance!interlock}interlock inputs, from vacuum
system\index{linac!vacuum system}s, power supply systems, RF systems, etc., must also be used to trigger
a fast beam shut-down procedure if a strong malfunctioning is detected in a critical component.

Additionally to the signals coming from the accelerator itself, it is suggested that the MYRRHA user
(\emph{i.e.} reactor) should provide an additional input signal reporting the reactor status, that would for
example be a normally present electrical current which fully reliably drops in case of emergency.

Finally, it must be noted that this fast protection system will have to be designed and implemented with special
care so as to avoid as much as possible MPS\index{Dependability!Machine protection}\index{linac!Control
System!Machine Protection System} false positives that would shut off the beam unnecessarily and
therefore decrease the \TReliability{reliability} performance of the accelerator,
which is of prime importance in the case of MYRRHA. As a matter of fact, such MPS false alarms are often
one of the main reasons for unwanted beam interruptions~\cite{P-TCADS10-Kim}.

\FloatBarrier\subsection[Decommissioning]{Decommissioning}


\begin{Alert}{}
	Decommissioning is missing! 
\end{Alert}

\FloatBarrier
\else

\subsection[Cavity \TCommissioning{commissioning} steps]{Cavity Commissioning Steps}
%To be inserted as first section in {}``Commissioning{}'' chapter
%Pompon Franck
%May 5, 2020 2:47 PM
\subsubsection{Low level measurements}
Once the cavity is manufactured, the purpose of low level measurements is
to check that the cavity behaves as expected from the RF point of view. This test is made at
atmospheric pressure and in stand-alone mode.

Auxiliaries, such as power couplers and pick-ups must be connected to the cavity. A N-type coaxial adapter
is mounted on the power coupler's input. As an example, Fig.~\ref{fig:commiss:CH:T:low-level-tests}
shows CH01 under low level tests.

\begin{figure}[h]%[tbp]
\centering
  \includegraphics[width=0.7\textwidth]{MYRRHATDRPartB-img/CH2-img009.jpg}
\caption{Example of cavity under low level tests (CH01).}
\label{fig:commiss:CH:T:low-level-tests}
\end{figure}

At first, the coupling coefficient of the power coupler must be adjusted and measured such as it is
critical with beam, that is slight over-coupled for RT cavities. The coupling coefficient without
beam to be set for each CH cavity is dependent of the beam loading.

Then, the coupling factor of the pick-up probe must be calibrated according to the LLRF
requirements.

The loaded Q-value (Q\textsubscript{L}) and the frequency resonance of the cavity can be measured,
as shown in the example in Fig.~\ref{fig:commiss:CH:U:measurements}. Knowing the coupling coefficient,
the unloaded Q-value (Q\textsubscript{0}) can be calculated by:

\begin{equation*}
Q_0=Q_L{\cdot}\left(1+\beta \right)
\end{equation*}

\begin{figure}[h]%[tbp]
\centering
  \includegraphics[width=0.7\textwidth]{MYRRHATDRPartB-img/CH2-img010.jpg}
\caption{Example of frequency resonance and Q\textsubscript{L} measurements.}
\label{fig:commiss:CH:U:measurements}
\end{figure}


Then, the static and dynamic tuners are mounted on the cavity ports and the tuning range is
measured. This range must be centred, around the operating frequency, by adjusting the position of the
static tuner. Figure`\ref{fig:commiss:CH:V:tuning-range-measurement} shows an example of tuning range
measurement with respect to dynamic tuner position. At this step, the loaded Q-value measurement should
be repeated at operating frequency of \TWinIiLEfreq.

\begin{figure}[h]%[tbp]
\centering
  \includegraphics[width=0.7\textwidth]{MYRRHATDRPartB-img/CH2-imgV.png}
	\caption{Example of tuning range measurement.}
\label{fig:commiss:CH:V:tuning-range-measurement}
\end{figure}

The so-called bead pull measurement can also be performed in order to measure the electric field
distribution within the gaps. It consists in the insertion of a Teflon bead along the beam axis while
measuring the shift in frequency resonance of the cavity. Indeed, when a small volume is removed, or
modified, from the cavity volume, this will generally produce an unbalance of the electric and magnetic
energies, and the resonant frequency will shift to restore the balance. From the Slater Perturbation
Theorem~\cite{B-2008-Wangler-Wiley}, it is known that the frequency shift is proportional to the square
of the local field.  Figure~\ref{fig:commiss:CH:W:field-distr} shows an example of field distribution
measurement along the beam axis, where a three gaps cavity can be guessed. This field measurement can
be compared to \TSimulation{simulation} and used to feed the beam dynamics calculations with more realistic data.

\begin{figure}[h]%[tbp]
\centering
  \includegraphics[width=0.7\textwidth]{MYRRHATDRPartB-img/CH2-img011.png}
\caption{Example of field distribution measurement along the beam axis.}
\label{fig:commiss:CH:W:field-distr}
\end{figure}


\subsubsection{Vacuum and cooling leak tests}
In order to ensure a good operation with RF power, the vacuum and cooling
impermeability must be verified. Indeed, any vacuum or water leak in the cavity will prevent the RF
power application and cavity operation.

After the installation of the pumping system on the cavity, the pumping should be run for 24 hours,
then followed by a He-leak tests. The achieved vacuum level should correspond to the expected level
(see Table~\ref{tab:vacuum:linac:equipment-list} in Sect.~\ref{tab:vacuum:linac:equipment-list}).

The water cooling manifold must be connected and a pressure test must be performed over at least 12
hours.

\subsubsection{Conditioning}
In preparation of this step, the Tx line between the RF power amplifier
and the power coupler must be calibrated and installed. When freshly manufactured, a cavity cannot
be immediately powered at nominal level but requires to be first conditioned. Indeed, cavity walls
and auxiliaries (coupler, tuner) produce out-gassing and multifactor effects, which results in
sudden increases of the vacuum level and the reflected power level. Thus, the RF power must be
raised in a careful manner following the vacuum variations. Once the nominal gradient is reached,
the new vacuum level without power is clearly improved thanks to the cleaning effect of the
\TConditioning{conditioning}. Figure~\ref{fig:commiss:CH:X:conditioning} shows an example of cavity  \TConditioning{conditioning} where
the vacuum sensitivity with the power raise is visible.

\begin{figure}[h]%[tbp]
\centering
  \includegraphics[width=0.7\textwidth]{MYRRHATDRPartB-img/CH2-img012.png}
\caption{Example of cavity conditioning.}
\label{fig:commiss:CH:X:conditioning}
\end{figure}


\subsubsection{X-ray measurement}
A way to directly measure the accelerating field level in a cavity is to perform X-ray spectrometry. This
measurement can be made by means of a Low Energy Germanium Detector (LEGe) connected to an Multi-Channel
Analyser (MCA), while the cavity is fed with RF power. To get usable data, the energy resolution of the
setup shall be better than \SI{1}{\percent} in the energy range to be measured.

Thus, the X-ray energy levels, being a picture of the cavity voltage distribution,
can be measured in function of the power transmitted to the cavity, as illustrated in
Fig.~\ref{fig:commiss:CH:Y:X-ray-results}. Also, the shunt impedance can be derived from this measurement
and compared to \TSimulation{simulation} results.

This measurement is an optional step in the \TCommissioning{commissioning} process as the outcome is not compulsory
for moving to the following step.

\begin{figure}[h]%[tbp]
\centering
 \includegraphics[width=0.7\textwidth]{MYRRHATDRPartB-img/CH2-img013.png}
\caption{Example of X-ray measurements result.}
\label{fig:commiss:CH:Y:X-ray-results}
\end{figure}


\subsubsection{Run test}
As a last \TCommissioning{commissioning} step before operating with beam, the cavity shall
pass a run test at nominal power level (or above), along with its complete RF system (amplifier,
LLRF, Tx line, circulator). This step is an extension of the  \TConditioning{conditioning} step as discharges may
still occasionally occur, but their mean repetition rate should decrease with time until it is
acceptable for the linac operation. This run test is also an opportunity to detect any instability
in the RF system, which could either compromise the beam operation or point out unexpected ageing of
components. Thus, the duration of the run test shall be adapted to each cavity.


\fi
%
% EOF TDR-B21-CommissDecommiss.tex
%
