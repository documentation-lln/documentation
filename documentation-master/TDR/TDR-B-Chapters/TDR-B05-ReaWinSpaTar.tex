%
% TDR-B05-ReaWinSpaTar.tex
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section[Reactor Window and Spallation Target]{Reactor Window and Spallation Target}\label{sect:rwst}\index{linac!spallation target}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{Alert}{}
	If I'm not mistaken, here the context is primarily on the \SI{600}{\MeV} system---see
	for instance the subsection below.
	Do we have specific information on the \SI{100}{\MeV}? 
\end{Alert}

\FloatBarrier
\subsection[Target Window (aka Reactor Window)]{Target Window (aka Reactor Window)}
A first \CDTFASTEF{} study indicated that a loopless window design option is a valid, simple, robust and
flexible option for the target window.  Analyses of material characteristics of the chosen T91 alloy
show that the preferential working temperature for the spallation window\index{linac!spallation target}
is found in the range around 450--\SI{500}{\celsius}.

This range is desirable from the point of view of irradiation hardening, He- and liquid metal embrittlement
and still acceptable for corrosion and material strength. The increase in proton energy from \SI{350}{\MeV}
to \SI{600}{\MeV} is a key parameter to making the thermal design of the beam window\index{beam!window}
possible. The thermal load of the window and the spallation zone is reduced by a factor of two and even
more. The design of the beam shape and the velocity distribution at the entrance of the spallation region,
together with the reduced heat deposition, permits to choose window temperature to the preferential
working temperature.

The alternative reactor design with a spallation window considerably simplifies the mechanical construction,
not only of the spallation target\index{linac!spallation target} but also of the surrounding structures
(core support plate, core barrel and plug, reactor cover). Furthermore, it facilitates the maintenance
operations and it improves the target's \TReliability{reliability}. Finally,
the other systems/parts (such as the building) will also take advantage of the less complex spallation
target\index{linac!spallation target}.

\subsubsection{Spallation target assembly}\index{linac!spallation target}
\paragraph{Functional requirements.}
The functional requirements of the spallation target\index{linac!spallation target} assembly are:

\begin{itemize}[noitemsep]
  \item  Lead the proton beam to the centre of the core;
  \item  Evacuation of the spallation heat deposit;
  \item  Guarantee the barrier between the LBE and the reactor hall.
\end{itemize}

\paragraph{Description.}
The spallation target\index{linac!spallation target} assembly brings the proton beam via the beam
tube\index{beam!tube} into the central core region and creates the optimal conditions for the spallation
reactions.  The assembly occupies the central position of the sub-critical core. The target assembly is
conceived as an in pile section. It is easily removable and replaceable. Until more experience is gathered
concerning the irradiation damage of the window material, the device shall be replaced every cycle (FPD: 90).

The \SI{600}{\MeV} protons pass through the beam window\index{beam!window} into the LBE, where
the protons interact with the heavy atoms creating spallation neutrons and spallation products. This
interaction deposits an important quantity of heat into the window and the LBE. The assembly
is designed to remove the spallation energy by means of the core coolant and to withstand the irradiation
doses by protons and neutrons.

\begin{Alert}{}
	MINERVA specificities here?
\end{Alert}

The main parts of the spallation target\index{linac!spallation target} assembly are the beam
tube\index{beam!tube}, the beam window\index{beam!window}, the protecting hexagonal wrapper, the target
assembly plug, the velocity profile device and the pressure drop device.

\paragraph{Beam window.}
The beam window\index{beam!window} is a hemispherical shell, which forms the separation between the
vacuum for the proton beam and the coolant of the reactor. The coolant acts below the window also as
spallation target\index{linac!spallation target}.

The position of the window is determined to centre the spallation zone close to the active core mid
plane. The penetration depth of the \SI{600}{\MeV} protons is about 300~mm. Most of the spallation neutrons
however are formed just below the window. According to neutronic calculations, the optimal position of
the window is situated at about 50~mm above the active core mid-plane. The outer diameter of the shell is
87.05~mm and the inner diameter is 84.30~mm, which results in a shell thickness of 1.375~mm. The chosen
material for the window is T91 alloy.

\begin{Alert}{}
	Re: the above sentences:
	Do we have to discuss:
	  A \SI{100}{\MeV} beam window? 
	  A penetration depth of the \SI{100}{\MeV} protons?
	Etc
\end{Alert}

\paragraph{Beam tube.}
The beam tube\index{beam!tube} extends the beam line from the level of the reactor cover plug to the
beam window\index{beam!window}. It guarantees the vacuum for the proton beam.

\subsubsection{Design requirements}
For the design of the spallation target\index{linac!spallation target} assembly, different operational
states for the sub-critical working conditions have been considered. The most limiting part of the target
assembly is the beam window. The window has to resist to the harsh conditions. But also the positioning
of the beam onto the axis of the window imposes stringent structural requirements to the target assembly.

\paragraph{Thermal requirements.}
Based on the current knowledge on the material properties of T91, the preferable temperature range of
the beam window\index{beam!window} is between \SI{450}{\celsius} and \SI{550}{\celsius} (compromise
between corrosion and hydrogen- and liquid metal embrittlement). By the limitation of the temperature
ranges for the 15-15 Ti and 316L induced by the oxygen concentration control (\SI{200}{\celsius} to
\SI{470}{\celsius}) however, the normal operation design working range of the window is shifted somewhat
and set to 410--\SI{470}{\celsius}.

\paragraph{Material requirements.}
The material damage induced by the protons and the neutrons, the corrosion by the LBE and the
temperature gradients are taken into account. By limiting the residence time of the target assembly
into the reactor to 1 cycle, the material damage and corrosion issues are controlled.

\paragraph{Normal operation.}
At constant reactor power level During normal operation the spallation reaction provides the neutrons
to maintain the fission into the core. The burn up of the fuel increases during the cycle, which has
to be compensated. This compensation can be made in two ways. The first way subsists in adding more
neutrons by increasing the current of the proton beam, the second reducing the anti-reactivity by means
of extraction of the control rods. At the moment preference is given to the second option: compensation
via the available control rod system but both options are still studied. The control rod compensation
allows to keep the beam current constant during the cycle.  The compensation by means of the control
rods has no direct impact on the cooling of the window which results in a constant window temperature
situation. However, as the core power is determined for each cycle depending on the loading conditions
(IPS, etc.) the beam current levels can vary between different cycles. For the thermal analyses beam
current values considered are \SI{2.8}{\mA} and \SI{3.5}{\mA}.

\paragraph{Off normal operation.}
The two off normal operation states that have been considered are the unattended beam trip\index{beam!trip} and the
misalignment of the beam axis compared to the window axis.

\begin{description}
	\item[Beam trips:] Unattended beam trip\index{beam!trip}s are very likely to occur. Operational experience of high power,
	high energetic linear accelerators show that beam trips are unavoidable. The effect of a beam trip is
	the drop of power of the full core from nominal power to decay heat power. Within the spallation zone,
		the heat deposition drops from \SI{1.3}{\MW} to near zero almost instantaneously.

\item[Beam misalignment:] \index{beam!misalignment}The positioning of the beam on the window is very important. The distance from
	the last bending magnet to the beam window\index{beam!window} is in the order of 20~m. Due to the large distance the smallest
	positioning error results in a large deviation at the window. The important buoyancy forces on the beam
	tube also result in bending of the tube and by this in horizontal displacements. A misalignment of the
	beam with respect to the window axis of 2~mm has been studied. The relative position will be monitored
	by a wire sensor near the beam window\index{beam!window}.

\item[Beam tube\index{beam!tube} failure:] For a beam tube failure inside the reactor vessel 2 positions can be distinguished,
	below the free liquid surface and above.
\end{description}

A failure into the LBE would cause the fill up of the beam tube\index{beam!tube} with
LBE. The maximum LBE level reached in the beam tube depends on the size of the
hole, but after the transient the LBE level will be comparable to the LBE level
in the upper plenum.

As the vapour pressure of LBE is low the vacuum will only be affected in a limited way. Fast
closure valves at the containment boundary protect the beam line to be contaminated outside the containment
building. A failure of the beam tube\index{beam!tube} into the cover gas would cause cover gas to enter the
beam line. The same fast closure valves can avoid contamination of the beam line outside of the containment.

\subsubsection{Thermal design}
The thermal design of the spallation target\index{linac!spallation target} assembly up to now is focused
to the temperature distribution in the spallation area and the window. Also the wrapper temperature inside
the core zone is evaluated. The thermal analyses of the structures above the core support plate have not
been performed yet because the limited information available on the heat load distribution as function
of the height of the tube. At a perfect alignment of the beam tube\index{beam!tube} with the beam, the
heat load will be small. However, at beam misalignment the heat load on the beam tube\index{beam!tube}
can be considerable and the cooling above the free surface into the cover gas limited.

\paragraph{Window thermal design.}
Previous studies showed the feasibility of the window design with respect to thermal design and stresses
into the beam window\index{beam!window}. The re-evaluation of the design was performed to be in line
with the temperature requirements imposed by the oxygen control system.

\paragraph{Beam shape.}
\index{beam!shape}
The doughnut shape beam (sweeped Gaussian beam) is the only shape retained in this study.
The criterion used to find the optimal combination is that the beam current through the window must be
\SI{99}{\percent} of the total beam current and the peak current density must be as low as possible. The heat deposited
in the window increases with increasing sweeping radius (keeping the \SI{99}{\percent} of the current through the window).

To obtain the heat deposit of the doughnut shape beam, the convolution of the Gaussian beam shape and
the pencil beam heat deposit was made. This gives the heat distribution in LBE of a beam which falls on
a flat LBE surface. To simulate the hemispherical window the heat deposit is translated parallel to the
beam axis on the hemispherical surface.

The optimal choice for the geometry remains a difficult task due to the contradictory requirements.
The outlet temperature of the LBE should be kept as low as possible but on the other hand the window
temperature should be high enough. An optimisation process considering the beam profile, the diameters
of the 2 guide tubes of the velocity profile device, the ratio of the flow rates with in the 3 annular
regions and the window thickness was performed.

The optimal configuration retained is summarised in Table~\ref{tab:rwst:one}.

\begin{table}
\begin{center}
\tablefirsthead{}
\tablehead{}
\tabletail{}
\tablelasttail{}
\begin{supertabular}{|m{4.44906in}|m{0.6101598in}|}
\hline
{ Beam shape: sigma of the Gaussian distribution (mm)} &
{ 9}\\\hline
{ Sweeping radius (mm)} &
{ 21.5}\\\hline
{ Mass flow rate kg/s (distributed as (from in to out) 20, 20, and 9 kg/s)}
&
{ 49}\\\hline
{ Window inner diameter (mm)} &
{ 84.30}\\\hline
{ Window outer diameter (mm)} &
{ 87.05}\\\hline
{ Window thickness (mm)} &
{ 1.375}\\\hline
{ Concentric tubes: inner tube Dmax (mm) (thickness: 2~mm)} &
{ 32.15}\\\hline
{ Concentric tubes: outer tube Dmax (mm) (thickness: 2~mm)} &
{ 82.15}\\\hline
\end{supertabular}
\end{center}
\caption{Beam on target and window: optimal configuration.}
\label{tab:rwst:one}
\end{table}

At the LBE side the window temperature ranges from \SI{412}{\celsius} to \SI{480}{\celsius}. The
window temperature at the vacuum side attains \SI{575}{\celsius}.

\subsubsection{Mechanical design}
Similar to the thermal design, the verification of the mechanical design has been focused on the window
\TIntegrity{integrity}. The following evaluations to be performed are the dimensioning of the beam tube\index{beam!tube} and the target
assembly wrapper above the core support plate and their supports within the above core structures. Specific
attention points are the buckling of the column as a whole, buckling of the tube due to external pressure
forces and the accuracy of the positioning of the window.

\subsubsection{Thermo-hydraulic considerations}
The beam tube\index{beam!tube} is passed through the reactor lid and introduced into the core in the central position. In
order to investigate the feasibility of the window design one needs to determine the temperature range
over it.

In addition, it encounters some more limitations such as:

\begin{itemize}[noitemsep]
	\item Maximum velocity of lead bismuth (in order to avoid erosion of the material) and
	\item Maximum pressure drop over the window device.
\end{itemize}

\paragraph{First design of the target zone assembly.}
Annular tubes are used to guide the flow from the inlet region to the spallation zone and to create an
optimal velocity profile to cool the spallation zone and the window. Porous media are used to create the
right flow distribution between the 3 zones formed by the annular tubes. The beam tube\index{beam!tube}
(outer) diameter is 87.7~mm.

The window is hemispherical and has a thickness of 1~mm. The lowest window point is positioned 150~mm
above the core mid plane to centre the spallation zone in the core. An incident beam of \SI{3.5}{\mA} and \SI{600}{\MeV}
with a flat beam profile with a radius of 40~mm is considered for this design. The detailed distribution
is determined by convolution of the step function with a pencil beam heat deposition determined by
\index{\TSimulation{Simulation} codes!MCNPX}MCNPX into a semi-infinite volume of LBE and translated to
the hemispheric window. The working fluid is lead bismuth (LBE) and the material considered
for window is T91 ferritic-martensitic steel.

Results: The correct implementation of heat input/deposition into the computational domain is necessary
in order to get the reliable prediction of temperature and velocity scales. A three dimensional heat
input (because of the incident beam) is deposited on the LBE. This gives a total power of \SI{1.3}{\MW}. In
addition a constant heat flux corresponding to \midtilde{}\SI{7.3}{\kW} is imposed on the beam side of the window
target. The maximum temperature appears in the LBE (near the target window) and is \SI{560}{\celsius}.

\TSimulation{Simulation} shows the temperature distribution on the window target is in the range of
499--\SI{530}{\celsius}. The maximum velocity magnitude which appears in the computational domain
is of the order of 2.1~m/s. Based on these CFD analyses, it was decided within the CDT project\index{Projects!CDT} to change
the parameters of the target zone assembly design. This new proposed design is discussed in the following
section.

\paragraph{Second design of the target zone assembly.}
In the second proposed design of target zone assembly presented by \sckcen{} a plate with small holes
is used to get the desired pressure drop instead of porous medium approach (which was a simplification
to avoid the \TSimulation{simulation} of the large number of small holes). The plate with the holes fixes the guide
tubes to the hexagonal wrapper of the assembly. The representation of the small holes instead of the use
of a porous medium for pressure drop makes the computational grid much more complex than the previous
design. There appears a relatively high velocity and flow gradient region, hence, it requires a fine
computational grid. The CFD tool STAR-CCM+ has been used to generate the mesh and it is consists of
\midtilde{}7.7 million grid points.

An incident beam of \SI{3.5}{\mA} and \SI{600}{\MeV} with a Gaussian distribution ($\sigma $) of 9~mm and a sweep
radius of 21.5~mm is considered to compute the heat deposition into the LBE and the hemispheric window
for this particular design.

Results: The steady state three dimensional calculations are performed for this second design of target
zone assembly. Apart from the heat input all other \TSimulation{simulation} parameters are kept same. In this design,
two different calculations are considered by varying the beam intensity from \SI{3.5}{\mA} to \SI{2.8}{\mA} (heat input),
which are as follows:

\begin{itemize}[noitemsep]
\item Case 1: Total heat input into the window target and LBE: \midtilde{} \SI{1.3}{\MW} 
	(corresponding to \SI{3.5}{\mA}):\newline

	This particular design contains small tubes to achieve the desired velocity profile in the target zone
	and around the window. As a result, there appears high velocity scales of magnitude around 2.8~m/s in
	the narrow region of the small holes, and re-circulation regions downstream to the small tubes. Moreover,
	one can also observe that a high velocity stream in the centre region of guide tubes. Further downstream
	this high velocity stream hits the window target region and eventually produces a stagnation region.

	The predicted temperature distribution of LBE near the window target region shows a maximum
	of \SI{503}{\celsius}. For beam side window target, the maximum temperature is in the range of
	\SI{527}{\celsius}, which is obvious because of the direct impact of high intensity beam.  Whereas,
	on LBE side the predicted temperature is relatively lower with a maximum of around \SI{463}{\celsius}.

\item Case 2: Total heat input into the window target and LBE: \midtilde{} \SI{1.04}{\MW}
	(corresponding to \SI{2.8}{\mA}):\newline

	For this particular case, the beam intensity is reduced by \SI{20}{\percent}, however, other parameters remained
	unchanged. Temperature distribution on the LBE near the window target region shows a maximum temperature
	of \SI{456}{\celsius}, which is \SI{47}{\celsius} less than the Case 1. In addition, the temperature
	distribution on the window target shows the maximum predicted temperature for LBE and beam sides to be
	\SI{476}{\celsius} and \SI{424}{\celsius}, respectively, which are relatively less than the Case 1.
\end{itemize}

\FloatBarrier\subsection[Beam Monitoring on the Target]{Beam Monitoring on the Target}
On-line measurements of the beam characteristics (size and position) as close as possible of the target
are a major issue. Like in all high power installations, these measurements are mandatory to be able to
tune the first beams and then to safely inject into the reactor in nominal conditions.

In the latter case, the beam footprint has to be continuously monitored and any deviation from tight
reference values must invariably lead to the prompt interruption of the irradiation.  Moreover, such a
system can also provide potential capability to track the peak power density on target during production.

The beam footprint can be determined using different (but quite similar) methods. We give here two examples:

\begin{itemize}[noitemsep]
  \item The VIMOS system at the PSI facility in Villigen~\cite{P-DIPAC09-Thomsen,J-PhysResA-2009-600.1-Thomsen}.
  \item The target imaging system (TIS) at the Spallation Neutron Source (SNS) facility in Oak
	Ridge~\cite{P-BIW10-Blokland}.
\end{itemize}

Both devices are based on optical techniques. These optical techniques can be based on different physical
effects to derive beam conditions:

\begin{itemize}[noitemsep]
	\item  Thermal incandescence
	\item  Cr:Al2O3 fluorescence
	\item  Optical transition radiation (OTR)
	\item  He fluorescence (from fill gas in beam line)
\end{itemize}

\subsubsection{The case of VIMOS at SINQ}
The Visual Monitoring System (VIMOS) at the Swiss Spallation Neutron Source (SINQ) uses the emitted
infra red (IR) and visible light of a tungsten glowing mesh located in the vacuum pipe 40~cm before the
spallation target\index{linac!spallation target} window (see Fig.~\ref{fig:rwst:one}). Tungsten wires are 0.1~mm thick in one plane
and 0.3~mm in the other plane.

The proton beam heats the wires up to \SI{1000}{\celsius}, the power losses in the glowing mesh being
around \SI{1}{\kW}. The light emitted by the tungsten goes up in the beam vacuum pipe up to a AlMgSi1 parabolic
mirror (size \midtilde{}20 cm) downstream the last deviation dipole. The distance between the mesh and the
mirror is around 10 metres. The mirror reflects the light with an angle of \SI{25}{\degree} up to a window.

After this window, 5~m optic fibres transmit the light to a CCD camera. Finally, a thermo-graphic analysis
is provided in order to determine the beam profile. The residual gas contribution on light is negligible.

With VIMOS, besides the beam footprint profile, a \SI{2}{\percent} modulation in beam intensity at 12~Hz can be
resolved, but this is close to the limit for such small changes. The time resolution is therefore 40~ms
and the size resolution is approximately {\rpm}1mm.

\begin{figure}[h]%[tbp]
\centering
  \includegraphics[width=4.5535in,height=6.4543in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img110.png}
\caption{VIMOS at SINQ facility, PSI.}
\label{fig:rwst:one}
\end{figure}

\subsubsection{The case of beam on target at SNS}
At the SNS facility, the proton beam, hitting a luminescent coating on the target window nose, produces
light that is transferred using a radiation-tolerant optical system to an image acquisition system.

The proton beam power is one megawatt (60~Hz, 1~GeV protons, pulse length less than 1~$\mu $s) and the
spallation target\index{linac!spallation target} is mercury. The target nose is coated with a layer of Cr:Al2O3. Distance from the
layer up to first parabolic mirror is 2.2~m.

Other mirrors and a fibre bundle are used in order to bring the light out to the camera in a non-radiation
environment (see Fig.~\ref{fig:rwst:two} and Fig.~\ref{fig:rwst:three}).

\begin{figure}[h]%[tbp]
\centering
  \includegraphics[width=4.5138in,height=2.9402in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img111.png}
\caption{View of optical path with \TShielding{shielding} for the SNS device.}
\label{fig:rwst:two}
\end{figure}

\begin{figure}[h]%[tbp]
\centering
  \includegraphics[width=4.5402in,height=3.1272in]{MYRRHATDRPartB-img/MYRRHATDRPartB-img112.png}
\caption{The configuration of the Target Imaging System at the SNS.}
\label{fig:rwst:three}
\end{figure}

The camera's acquisition is triggered 4$\mu$s after the beam hits the target by which time the gas
scintillation has died down but the coating is still fluorescing.

The system has a demonstrated lifetime of over 2000~MW$\cdot$h and the current rate of degradation is minimal.


\subsubsection{MYRRHA}
In the case of MYRRHA, a dedicated system will have to be designed from these developments, taken
into account the beam line configuration, which is quite longer, and the accuracy on position and size
measurements, which should be about {\rpm}0.5~mm.

The device can be developed for \SI{100}{\MeV} and planned to work at \SI{600}{\MeV} with minor modifications for the
same beam current and with the same technology.

\begin{Alert}{}
	Here I would need to specify up-to-date information about the MINERVA development.
	Are there documents, or people, I may consult for this?
\end{Alert}

\FloatBarrier\subsection[Secondary Window]{Secondary Window}
For the secondary window, see Sect.~\ref{s:secondary-win} (``Secondary Window'') at
page~\pageref{s:secondary-win} of the present volume.

\FloatBarrier
%
% EOF TDR-B05-ReaWinSpaTar.tex
%
