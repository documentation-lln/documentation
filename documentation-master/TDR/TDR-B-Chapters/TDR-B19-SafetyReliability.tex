%
% TDR-B19-SafetyReliability.tex
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section[Safety and Reliability]{Safety\index{Dependability!Safety} and Reliability}\label{sect:saferel}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\FloatBarrier\subsection[Introduction]{Introduction}
As already highlighted in Part~\ref{chp:intro}, Sect.~\ref{sect:myrrha},
our accelerator is expected to have a very limited number of unforeseen beam interruptions per
operation cycle.

This requirement is motivated by the fact that frequently-repeated beam interruptions can inflict high
thermal stresses and \TFatigue{fatigue} to the reactor structures, the target, or the fuel elements, which can cause
significant damage---especially to the fuel claddings. Moreover, these beam interruptions dramatically
decrease the plant \index{Dependability!Availability}availability, possibly implying plant shut-downs
of tens of hours in most of cases.

The allowable beam trip\index{beam!trip}s in an ADS should make a compromise between an efficient and
reliable sub-critical reactor and a reasonable cost of the accelerator.

%{{{ Reliability Studies
\FloatBarrier\subsection[Reliability Studies]{Reliability Studies}
At the beginning of the \index{Projects!\hyperref[sect:projects:eurotrans]{XT-ADS}}\hyperref[sect:projects:eurotrans]{XT-ADS} project, the order of magnitude of the allowable
duration of the beam trips was set to \SI{1}{\second} with two frequency values being defined for longer
trips followed by reactor shutdowns:

\begin{itemize}[noitemsep]
  \item Accelerator design target: no more than 5 beam shutdowns per year or less.
  \item Reactor design target: sustain at least 50 beam shutdowns per year.
\end{itemize}

These initial values have been revisited in the light of the experience gained from the PHENIX plant
design and operation (Pnom = 568 MWth). The thermal effects induced by the reactor starts and shutdowns
can be compared to the effect of the proton beam trip\index{beam!trip}s in \hyperref[sect:projects:eurotrans]{XT-ADS} in spite of its smaller
size and differences in the main systems.

Studies have been carried out demonstrating the possibility of maintaining the \TIntegrity{integrity} of both the
plant's structure and the fuel cladding under repeated beam trip\index{beam!trip}s. A revised maximum
number of allowable beam trip\index{beam!trip}s was then proposed: 10 quick transients/3 months, plus
an assessment of the potential risks if the \hyperref[sect:projects:eurotrans]{XT-ADS} accelerator missed this goal.

The initial \hyperref[sect:projects:eurotrans]{XT-ADS} recommendations concerning these beam trip\index{beam!trip}s (less than 5 beam trips
longer than \SI{1}{\second} per 3-month cycle) have been revisited at the end of the project.

The current beam trip\index{beam!trip}s tolerance goal stands as:

\begin{itemize}[noitemsep]
  \item Beam trip\index{beam!trip}s longer than \SI{3}{\second}: less than 10 per 100 day period;
  \item Beam trip\index{beam!trip}s longer than \SI{0.1}{\second} but less than \SI{3}{\second}: less than 100 per day;
  \item Beam trip\index{beam!trip}s less than \SI{0.1}{\second}: no limit considered necessary.
\end{itemize}

To reach such an ambitious goal, \TReliability{reliability}-oriented design practices
needed to be followed from the early design stage. In particular:

\begin{itemize}[noitemsep]
  \item ``Strong design'' (\emph{i.e.} over-design) is needed: every linac main component has to be
	de-rated with respect to its technological limitation.

  \item A high degree of \TRedundancy{redundancy}
	needs to be planned in critical areas; this is especially true for the identified
	``poor-\TReliability{reliability}'' components like the injector\index{linac!injector},
	which may have to be duplicated, or the RF power systems, through the use of solid-state amplifiers.

  \item The accelerator should be able, to the maximum extent, to pursue operation despite some
	major faults in basic components (``fault tolerance'' capability); it has been shown
	in~cite{P-LINAC08-Kim} that a solution based on a modular independently-phased {\LinacSCLinac} is
	indeed capable to adapt its nominal tuning in the case of a loss of any RF cavity or power loop
	unit (see ``Tolerance to RF faults in the linac'' in Part~\ref{chp:additional}, Sect.~\ref{ssect:RF-FT}).
\end{itemize}

Note: Redundancy comes in 2 flavours: classical parallel duplication, and fault tolerance.  Fault tolerance
is based on the function of a faulty element in a chain being taken up by one or several other elements,
so each element contains some \TRedundancy{redundancy} of other
elements. Parallel duplication should only be used if unavoidable, \emph{i.e.} if fault tolerance can not
be applied. Therefore the linac's reference design is based on a maximised modular configuration,
which is the baseline condition for the fault tolerant approach to be applicable. Next, for the
redundant scheme to be effective for the goal of \index{Dependability!Availability}availability, the
``\TRedundancy{redundancy} switching time'', being the time needed
to switch from an original scheme to an operational redundant alternate scheme, must be shorter than a
given delay time.


%\begin{Alert}{}
%I believe the author meant here that redundancy may be employed \emph{statically\/} (for error compensation /
%fault masking) or \emph{dynamically}, namely by actively tolerating faults. Though the just mentioned are but
%two dependability behaviours in a full spectrum of methods. More information about this may be found in my
%papers~\cite{J-JReliabIntellEnviron-2015-1.1-DeFV,J-JReliabIntellEnviron-2015-1.2-DeFV} and in forthcoming doctoral dissertation~\cite{T-UniAntwerpen-2020-Buys}.
%
%\end{Alert}

\FloatBarrier
%}}}
%
% EOF TDR-B19-SafetyReliability.tex
%
