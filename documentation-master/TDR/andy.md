# andy: selective enabling and disabling of sections from the TDR

Suppose you have checked out two sections of the TDR -- for instance, section B12, devoted to cryogenic systems,
and section B15, on vacuum systems.

Your focus is just those sections and you don't plan to provide contributions to other sections for the time being.

In this case, it may make sense to have just those two sections included in "your" TDR, while keeping all other
sections temporary disabled.

Well, this is fairly possible now. I have written a little code which I called "andy" (a pun on *en*able/*di*sable).
It's C code, so please compile it by executing the following command:

    gcc -o andy andy.c

Now please execute the command `andy -h`:


```
$ andy -h
Valid options are: [ -i input-filename ] [ -h ] [ -v ] [ -o chapters-file ]
-h means help; -v means verbose.
input-file    is typically the TDR main module.
chapters-file is typically the file the TDR includes, which in turn includes all enabled chapters.
```

andy is telling you that by default it processes the main TDR module and that it outputs a file
that the TDR uses to selectively choose which section to include and which not.

Now please execute the command without arguments:

```
$ andy
30 chapters have been pre-processed correctly.
Now we shall process the enable / disable statements.
 + STRING => enables  the chapter whose name includes STRING
 - STRING => disables the chapter whose name includes STRING
EOF (ctrl-d) => closes input
```

Please just press CTRL-D (on Linux) or CTRL-Z followed by Return (on Windows).
In what follows, I will call that sequence "EOF."

The code now says

```
Creating TDR structure file.
```

and stops. This should have created a new file called `include.tex` into your current directory:

```
$ ls -lt | head -2
total 207036
-rw-rw-r-- 1 vdflorio vdflorio     2211 Mär  9 15:46 include.tex
```

This is the file that instructs the TDR to include *all* or *a selection* of sections in your version of the TDR.
In this case it includes all sections. You may edit that file to see how this is done.

Now, if you want to only include B12 and B15, just call again andy and specify the following commands:

```
$ andy
30 chapters have been pre-processed correctly.
Now we shall process the enable / disable statements.
 + STRING => enables  the chapter whose name includes STRING
 - STRING => disables the chapter whose name includes STRING
EOF (ctrl-d) => closes input
- TDR
+ B12
+ B15
```

(You have to type the lines that start with '-' and '+' please).

At the end, once more, press the EOF sequence.

Now the file `include.tex` tells the TDR to just process B12 and B15:

![](https://git.myrrha.lan/vdflorio/documentation/-/raw/master/TDR/images/include-dot-tex.png)

Okay so now we have our selection in the `include.tex` file; we just need to tell the TDR
that we want it to process that selection and not the official, complete one.
In order to do so, please edit the main module of the TDR, `MINERVA-100MeV-ACCELERATOR-TDR.tex`,
and search for the string "`\TDRSELECTION`":

 ```
 % If TDRSELECTION is commented out, a static selection of TDR sections is included in the TDR
 % If TDRSELECTION is defined, a selection of TDR sections to be included in the TDR is read from file include.tex
 %\newcommand*{\TDRSELECTION}{}%
```

Now, where it says `%\newcommand*{\TDRSELECTION}{}%`, just remove the first character -- the leading `%`.
Save the file and compile it -- that's it!

Note that all sections are enabled by default. This means that if you want to restart from scratch and recreate
an `include.tex` that selects each and every section of the TDR, you just have to run `andy` and type EOF :notes:

----

Another way to call `andy` is of course using redirection. If you have, e.g., a file called `my-andy-commands.txt` that includes
the following three lines:

```
- TDR
+ B12
+ B15
```

then you can just call `andy` as follows:

    $ andy < my-andy-commands.txt

andy will then (1) disable all TDR section; (2) enable section B12; (3) enable section B15;
(4) produce `include.tex`; and (5) exit.

This makes it possible to avoid typing multiple times the same sequences of andy commands.

# The three parts of the TDR

I prepared three files that enable only the Part A, the Part B, and the Part C of the TDR.
If I run

    $ andy < Andy-Part-B.txt

I will have a TDR that onmy includes the sections of the Part B of the TDR.

## Return code

andy returns 0 upon success, a value less than 0 upon failure:

- `-1` if given an unknown command line option,
- `-2` if a system call failed,
- `-3` if too many enable/disable input lines are wrong.


## Note

Sections B09 (double-spoke cavities) and B10 (eliptical cavities), even when enabled via andy,
are subjected to the control of macro `\EnableBeyondMINERVA`.


## Caveat

I have coded andy during my lunch breaks and have not yet tested it thoroughly, so it is likely
that a few rounds of adjustments and debugging shall be required :sweat_smile:


![](https://git.myrrha.lan/vdflorio/documentation/-/raw/master/TDR/images/andy-dot-c.png)

## FAQ

- Q: Do I have to compile andy? Which tools do I need?
- A: Yes. You need a C compiler and compile `andy.c`. If you are on windows you can use the micro$oft compiler
     or if you prefer install cygwin or mingw64 and use gcc and make. You just type `make andy` and you should have it
     compiled on any platform.

- Q: Do I really need to use andy to enable/disable chapters?
- A: Nope. Andy actually only creates the file `include.tex`, which lists all the chapter you want to compile. In other words you can edit that file manually. For instance, to only enable the RF and control system chapters, you need to only have the following lines in your include.tex:
 
 ```
 \part{Introduction}\label{chp:intro}
 \part{Main components}\label{chp:main}
 \input{TDR-B-Chapters/TDR-B11-RF.tex} % -- chapter enabled.
 \input{TDR-B-Chapters/TDR-B18-ControlSystem.tex} % -- chapter enabled.
 \part{Additional Information}\label{chp:additional}
 ``` 

  Then in the main file, MINERVA-100MeV-ACCELERATOR-TDR.tex, you should uncomment the line

  `%\newcommand*{\TDRSELECTION}`

  by removing the `%`. That's it ^\_^

  I suggest that you make a copy of include.tex to re-enable all chapters when you're done. Also please put back the `%` to comment that line back in the main module.

- Q: Why "andy" by the way?
- A: I like Frank Zappa's music of course!
