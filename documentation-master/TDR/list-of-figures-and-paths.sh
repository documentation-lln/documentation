# echo "Phase 1/5: Generate fls"
# pdflatex -recorder MINERVA-100MeV-ACCELERATOR-TDR
#./generate-fls.sh
#printf '\033[32mNo errors: processing concluded normally.\033[0m\n'

printf '\033[46;1mPhase 1/5: Generate data structures and input list\033[0m\n'
# input=`cat MINERVA-100MeV-ACCELERATOR-TDR.fls|grep '[AT][nD][nR][e-][xAB]'|uniq|awk '{printf "%s ", $2}'`
input=`cat MINERVA-100MeV-ACCELERATOR-TDR.fls|grep '[AT][nD][nR][e-][xAB]'|sed 's/INPUT \.\//INPUT /' -|uniq |awk '{printf "%s ", $2}'`

./figurepages < MINERVA-100MeV-ACCELERATOR-TDR.lof > pages.h
# printf '\033[32mNo errors: processing concluded normally.\033[0m\n'

printf '\033[46;1mPhase 2/5: Create LaTeX scanner\033[0m\n'
make captions
# flex captions.l
# gcc -L../lib -I../include -o captions lex.yy.c -lfl -lvdfio
printf '\033[32mNo errors: processing concluded normally.\033[0m\n'

printf '\033[46;1mPhase 3/5: Feed input list into LaTeX scanner / create thumbnails.tex\033[0m\n'
# cat $input | captions -o captions.txt -v 
cat $input | captions -o thumbnails.tex -l 
# printf '\033[32mNo errors: processing concluded normally.\033[0m\n'

printf '\033[46;1mPhase 4/5: Build thumbnails.pdf\033[0m\n'
rubber --pdf thumbnails

printf '\033[46;1mPhase 5/5: Compress thumbnails.pdf / schedule its upload onto Alexandria\033[0m\n'
gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress -dNOPAUSE -dQUIET -dBATCH -sOutputFile=compressed_thumbnails.pdf thumbnails.pdf
cp compressed_thumbnails.pdf ~/c/Users/vdflorio/AppData/Roaming/OpenText/OTEdit/EC_sckcen/c40593146/thumbnails.pdf

printf '\033[32mNo errors: processing concluded normally.\033[0m\n'
