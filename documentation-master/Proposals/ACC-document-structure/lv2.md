Metadata-driven Logical Views: A proposal for a user-driven dynamic organization of the documents on Alexandria
---------------------------------------------------------------------------------------------------------------

vdflorio, 2020-12-03

**Abstract**: A file system represents a single, and static (= not
easily changeable) “physical view” to some data (in our case, the Alx
documents). Metadata in principle allows for “logical views” to be
dynamically superimposed onto the one physical view regardless of its
actual organization. There are two major advantages to this approach:

1.  Each and every user could express their ideal document organization
    / orchestration by defining a logical view – for instance, as it is
    suggested in the rest of this document.

2.  No modification needs to be applied onto the physical view, which
    stays available as the users knew and experienced it. This also
    avoids the need to perform costly and potentially error-prone
    relocations. The physical layer stays the same and constitutes an
    independent access system to the Alexandria documents the user can
    always fall back onto.

    In what follows I briefly introduce a logical view model and I
    exemplify elements of a possible software architecture for the
    dynamic composition of “folder-like” logical views to the documents
    on Alexandria.

### Introduction 

-   Let me refer to the set of documents onto Alexandria as **A**; the
    set of corresponding URLs as **U**(**A**); the metadata in the
    documents onto Alexandria as **M**(**A**). If *d* is in **A**,
    **U**(*d*) shall be its Alexandria URL and **M**(*d*) shall be its
    metadata.

-   Metadata are expressed as a function *m* from a set of tags (labels)
    to a set of strings. For any document *d* in **A** I shall write
    *m<sub>d</sub>* : *T* &rarr; *S*.

-   For any document *d* in **A**, and any tag *t* in *T*: *d*.has(*t*)
    is a Boolean function that is true if *d* includes tag *t* among its
    metadata.

-   For any documents *d* and *e* in **A**, and any tag *t* in *T*:
    *d*.shares(*t, e*) is a Boolean function that is true if and only if *d*.has(*t*) is true, *e*.has(*t*) is true, and
 *m*<sub>*d*</sub>(*t*) = *m*<sub>*e*</sub>(*t*). In other words,
 *d*.shares(*t, e*) is true if the two documents make use of the
 same tag and the tag has the same value in both.

 As an example, *d* and *e* could be two documents that both have a tag
 *t* equal to “DISCIPLINE” and corresponding value equal to “RF”.

 Obviously if *d*.shares(*t, e*) is true then *e*.shares(*t, d*) is
 true and vice-versa (that is, the relation is symmetric).

-   The shares predicate allows an equivalence relation,
    R<sub>*t*</sub>, to be defined on all documents in **A**, such that
    for any two documents *d* and *e* in **A** and for any tag *t*:

 (*d* R<sub>*t*</sub> *e*) iff *d*.shares(*t, e*) is true (i.e. both
 *d* and *e* have a metadata tag *t* in them holding the same value).

-   I shall call a “view onto the documents on Alexandria”, or $`\mathcal{V}`$, the
    quotient set **A** / R<sub>*t*</sub>.

 Set V represents a partition of documents in **A**. More formally, it
 is the partition whose blocks are the equivalence classes \[ *x*
 \]R<sub>*t*</sub>, for any *x* in **A** and *t* in *T*. As an example,
 *t* could be “PROJECT-NAME” and *x* could be representative of all
 documents that have tag “PROJECT-NAME” equal to “MINERVA”.

The idea is that views could be dynamically represented as folders: all
documents partitioned by $`\mathcal{V}`$ could be presented to the user as a root
folder. Obviously a folder is just another set, therefore it would be
fairly possible to apply a sub-view to it. This may be used to generate
sub-folders, and so forth. A query could be represented by an orderly
sequence of views: as an example, (“DISCIPLINE”, “PROJECT-NAME”) would
create a root folder including sub-folders partitioned by discipline,
each of which would gather documents partitioned by project name.



### Components

I envisage that Alexandria be interfaced with a separate metadata
server. For each document *d* in **A**, the server would store an
association (**ObjId**(*d*) &rarr; **M**(*d*)). Once fed with the ObjId of a
document, the server would return the metadata of said document. Once
fed with a metadata expression, the server would return the ObjIds
(URLs) of all documents matching that expression. Every time a document
is added to Alexandria, and metadata is associated with that document,
the metadata server would receive a request for adding a new association
to its database.

I envisage a user interface responsible for the creation of logical
views, rendered dynamically as virtual file systems. The user interface
would enable the user to visually construct a sequence of views (or more
complex expressions on views). The user interface would then query the
metadata server, receive the corresponding ObjIds, and create a virtual
file system on the fly. Selecting a node would allow to visit the node;
selecting a leaf would result in requesting the document from Alexandria
via its URL.
