Metadata-driven Logical Views: A proposal for a user-driven dynamic organization of the documents on Alexandria
---------------------------------------------------------------------------------------------------------------

vdflorio, 2020-12-03

**Abstract**: A file system represents a single, and static (= not
easily changeable) "physical view" to some data (in our case, the Alx
documents). Metadata in principle allows for "logical views" to be
dynamically superimposed onto the one physical view regardless of its
actual organization. There are two major advantages to this approach:

1.  Each and every user could express their ideal document organization
    / orchestration by defining a logical view -- for instance, as it is
    suggested in the rest of this document.

2.  No modification needs to be applied onto the physical view, which
    stays available as the users knew and experienced it. This also
    avoids the need to perform costly and potentially error-prone
    relocations. The physical layer stays the same and constitutes an
    independent access system to the Alexandria documents the user can
    always fall back onto.

    In what follows I briefly introduce a logical view model and I
    exemplify elements of a possible software architecture for the
    dynamic composition of "folder-like" logical views to the documents
    on Alexandria.

### Introduction

<ul>
<li><p>Let me refer to the set of documents onto Alexandria as <strong>A</strong>; the set of corresponding URLs as <strong>U</strong>(<strong>A</strong>); the metadata in the documents onto Alexandria as <strong>M</strong>(<strong>A</strong>). If <em>d</em> is in <strong>A</strong>, <strong>U</strong>(<em>d</em>) shall be its Alexandria URL and <strong>M</strong>(<em>d</em>) shall be its metadata.</p></li>
<li><p>Metadata are expressed as a function <em>m</em> from a set of tags (labels) to a set of strings. For any document <em>d</em> in <strong>A</strong> I shall write <em>m<sub>d</sub></em> : <em>T</em> &rarr; <em>S</em>.</p></li>
<li><p>For any document <em>d</em> in <strong>A</strong>, and any tag <em>t</em> in <em>T</em>: <em>d</em><font face = "Verdana">.has</font>(<em>t</em>) is a Boolean function that is true if <em>d</em> includes tag <em>t</em> among its metadata.</p></li>
<li><p>For any documents <em>d</em> and <em>e</em> in <strong>A</strong>, and any tag <em>t</em> in <em>T</em>: <em>d</em><font face = "Verdana">.shares</font>(<em>t, e</em>) is a Boolean function that is true if and only if</p>
<ul>
<li><p><em>d.</em><font face = "Verdana">has</font>(<em>t</em>) is true, <em>e.</em><font face = "Verdana">has</font>(<em>t</em>) is true, and <em>m<sub>d</sub></em>(<em>t</em>) = <em>m<sub>e</sub></em>(<em>t</em>). In other words, <em>d</em><font face =            "Verdana">.shares</font>(<em>t, e</em>) is true if the two documents make use of the same tag and the tag has the same value in both.</p></li>
</ul></li>
</ul>
<p>As an example, <em>d</em> and <em>e</em> could be two documents that both have a tag <em>t</em> equal to “DISCIPLINE” and corresponding value equal to “RF”.</p>
<p>Obviously if <em>d</em><font face =            "Verdana">.shares</font>(<em>t, e</em>) is true then <em>e</em><font face =            "Verdana">.shares</font>(<em>t, d</em>) is true and vice-versa (that is, the relation is symmetric).</p>
<ul>
<li><p>The <font face =            "Verdana">shares</font> predicate allows an equivalence relation, R<em><sub>t</sub></em>, to be defined on all documents in <strong>A</strong>, such that for any two documents <em>d</em> and <em>e</em> in <strong>A</strong> and for any tag <em>t</em>:</p></li>
</ul>
<p>(<em>d</em> R<em><sub>t</sub></em> <em>e</em>) iff <em>d</em><font face =            "Verdana">.shares</font>(<em>t, e</em>) is true (i.e. both <em>d</em> and <em>e</em> have a metadata tag <em>t</em> in them holding the same value).</p>
<ul>
<li><p>I shall call a “view onto the documents on Alexandria”, or 𝒱, the quotient set <strong>A</strong> / R<em><sub><em>t</em></sub>.</em></p></li>
</ul>
<p>Set 𝒱 represents a partition of documents in <strong>A</strong>. More formally, it is the partition whose blocks are the equivalence classes [ <em>x</em> ]R<em><sub>t</sub></em>, for any <em>x</em> in <strong>A</strong> and <em>t</em> in <em>T</em>. As an example, <em>t</em> could be “PROJECT-NAME” and <em>x</em> could be representative of all documents that have tag “PROJECT-NAME” equal to “MINERVA”.</p>

<p>The idea is that views could be represented as folders: all documents partitioned by 𝒱 could be presented to the user as a root folder. Obviously a folder is just another set, therefore it is fairly possible to apply a sub-view to it. This may be used to generate sub-folders, and so forth. A query could be represented by an orderly sequence of views: as an example, (“DISCIPLINE”, “PROJECT-NAME”) would create a root folder including sub-folders partitioned by discipline, each of which would gather documents partitioned by project name.</p>



### 

### Components

I envisage that Alexandria be interfaced with a separate metadata
server. For each document *d* in **A**, the server would store an
association (**ObjId**(*d)* **M**(*d*)). Once fed with the ObjId of a
document, the server would return the metadata of said document. Once
fed with a metadata expression, the server would return the ObjIds
(URLs) of all documents matching that expression. Every time a document
is added to Alexandria, and metadata is associated with that document,
the metadata server would receive a request for adding a new association
to its database.

I envisage a user interface responsible for the creation of logical
views, rendered dynamically as virtual file systems. The user interface
would enable the user to visually construct a sequence of views (or more
complex expressions on views). The user interface would then query the
metadata server, receive the corresponding ObjIds, and create a virtual
file system on the fly. Selecting a node would allow to visit the node;
selecting a leaf would result in requesting the document from Alexandria
via its URL.
