#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

#define VIEWERPID "/tmp/viewerpid"
#define PAGEFILE  "/tmp/page"


int main(int argc, char *argv[]) {
	int page, pid;
	FILE *f;
	if (argc != 2) {
		fprintf(stderr, "Usage: %s <positive-integer-smaller-than-300>\n", *argv);
		exit(-1);
	}
	page = atoi(argv[1]);
	if (page < 0 || page >= 300) {
		fprintf(stderr, "Usage: %s <positive-integer-smaller-than-300>\n", *argv);
		exit(-1);
	}
	f = fopen(PAGEFILE, "w");
	fprintf(f, "%d\n", page);
	fclose(f);


	f = fopen(VIEWERPID, "r");
	fscanf(f, "%d", &pid);
	fclose(f);

	kill(pid, SIGUSR1);
}
