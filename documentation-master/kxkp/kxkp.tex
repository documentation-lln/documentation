% Beginning of file kxkp.tex

%{{{ Prologue
% !TeX spellcheck = en_GB
% !TeX program = lualatex
%
% v 2.3  Feb 2019   Volker RW Schaa
%
\documentclass[a4paper,
               %boxit,        % check whether paper is inside correct margins
               %titlepage,    % separate title page
               %refpage       % separate references
               biblatex,     % biblatex is used
               keeplastbox,   % flushend option: not to un-indent last line in References
               %nospread,     % flushend option: do not fill with whitespace to balance columns
               %hyphens,      % allow \url to hyphenate at "-" (hyphens)
               %xetex,        % use XeLaTeX to process the file
               luatex,       % use LuaLaTeX to process the file
               ]{jacow}
%
% ONLY FOR \footnote in table/tabular
%
\usepackage{pdfpages,multirow,ragged2e} %
%
% CHANGE SEQUENCE OF GRAPHICS EXTENSION TO BE EMBEDDED
% ----------------------------------------------------
% test for XeTeX where the sequence is by default eps-> pdf, jpg, png, pdf, ...
%    and the JACoW template provides JACpic2v3.eps and JACpic2v3.jpg which
%    might generates errors, therefore PNG and JPG first
%
\makeatletter%
	\ifboolexpr{bool{xetex}}
	 {\renewcommand{\Gin@extensions}{.pdf,%
	                    .png,.jpg,.bmp,.pict,.tif,.psd,.mac,.sga,.tga,.gif,%
	                    .eps,.ps,%
	                    }}{}
\makeatother

% CHECK FOR XeTeX/LuaTeX BEFORE DEFINING AN INPUT ENCODING
% --------------------------------------------------------
%   utf8  is default for XeTeX/LuaTeX
%   utf8  in LaTeX only realises a small portion of codes
%
\ifboolexpr{bool{xetex} or bool{luatex}} % test for XeTeX/LuaTeX
 {}                                      % input encoding is utf8 by default
 {\usepackage[utf8]{inputenc}}           % switch to utf8

\usepackage[USenglish]{babel}

%
% if BibLaTeX is used
%
\ifboolexpr{bool{jacowbiblatex}}%
 {%
  \addbibresource{thesis.bib}
  % \addbibresource{jacow-test.bib}
  % \addbibresource{biblatex-examples.bib}
 }{}
\listfiles

%%
%%   Lengths for the spaces in the title
%%   \setlength\titleblockstartskip{..}  %before title, default 3pt
%%   \setlength\titleblockmiddleskip{..} %between title + author, default 1em
%%   \setlength\titleblockendskip{..}    %afterauthor, default 1em

% extra packages and definitions added by vdflorio
% \usepackage{latexsym}
% \usepackage{amsmath}
% \usepackage{amssymb}
\usepackage{graphicx}
\usepackage[pdftex,
            colorlinks=true,
            linkcolor=blue
           ]{hyperref}
\graphicspath{{./}{/home/vdflorio/Documents/Git/documentation/kw/}}
\usepackage{float}
\usepackage{alltt}
\usepackage{rotating} % sidewaysfigure
\def\sckcen{\mbox{SCK{\kern.4ex}CEN}}
\def\S{\mathcal{S}}
\def\DS{\hbox{$D_{\S}(t)$}}
\def\RS{\hbox{$R_{\S}(t)$}}
\def\kexp{KExC}
\def\deltat{\hbox{$\Delta_{\S} t$}}
\def\tracex{\texttt{tracex.py}}
\def\naming{\texttt{naming.py}}
\def\latexmacros{\texttt{LaTeXmacros.py}}
\def\linac{\texttt{linac.py}}

% \usepackage[backend=bibtex,style=numeric,sorting=none]{biblatex}
% \addbibresource{thesis}
% PDFLATEX % \DeclareUnicodeCharacter{0308}{}

\let\oldhref\href
\renewcommand{\href}[2]{\oldhref{#1}{\hbox{#2}}{}}
%}}}

\hypersetup{draft}

\begin{document}
%{{{ Front matter
\title{{\kexp}: An Approach to Living Documentation Applied to Linear Accelerator Technologies}

\author{V. De~Florio\textsuperscript{1}\thanks{vincenzo.de.florio@sckcen.be}, J. Belmans, A. Gatera, A. Ponton, D. Vandeplassche, \sckcen, Mol, Belgium\\
		% P. Contributor\textsuperscript{1}, Name of Institute or Affiliation, City, Country \\
		\textsuperscript{1}also at CLEA/VUB, Brussels, Belgium}
	
\maketitle

%
\begin{abstract}
	We describe an approach to living documentation and its application to the documentation of the MYRRHA linear accelerator.
	We detail a set of tools and show how they can help detecting and tolerating changes between a reference system
	and its documentation. We report about the use of our approach within MYRRHA, its current promising results, and possible
	future improvements.
\end{abstract}

% Former beginning of file kxkp.tex
% \documentclass{article}
% \begin{document}
%}}}
%{{{ Intro
\section{Introduction}
The design, management, and operation of technological systems relies on technical documentation (TD),
defined in~\cite{enwiki:1021916913} as ``the classes of information created to describe (in technical
language) the use, functionality or architecture of a product, system or service.''

An important caveat of TD is that, usually, documents are ``frozen in time''~\cite{De10}: their
contents implicitly refers to the time of writing, or to the time of last update.  As a consequence, a
drift~\cite{DF14a,DBLP:journals/corr/FlorioP15} usually exists between the properties and characteristics
of the product, system or service as they appear in the TD and as they actually are, or are intended to be,
at any given time.
% an either positive or negative drift! A document may be ahead of time with respect 

Let us make use of the generic term ``system'' and of symbol $\S$ to refer to the above-mentioned
``product, system or service''.
In order to express and discuss the drift, in what follows we will make use of
the two herein defined dynamic systems:

\begin{description}
  \item[\DS:] it is the TD of system $\S$ as it appears in the latest version of the TD available at time $t$.
	We will use the term ``Current Description'' to refer to {\DS} when $t=\textit{now}$.
  \item[\RS:] it is the TD of system $\S$ as $\S$ actually is, or is meant to be, at time $t$ according to its designers.
	% It is assumed that {\RS} is a parametric adaptation of {\DS}.
	We will use the term ``Trustworthy Description'' to refer to {\RS} for $t=\textit{now}$.
\end{description}

Using the just introduced terminology, we can express the above mentioned caveat by saying
that a discrepancy, or drift, usually exists between the properties
and characteristics of the Current Description and those of the Trustworthy
Description\footnote{The concept of a quality drift, also referred to as ``fidelity'' drift, as a difference
	between two dynamic systems representing a system's ideal and actual conditions, is borrowed here
	from previous works~\cite{DF10a} and~\cite{De11IMEC}.}.
Said drift could be quantified as, e.g., the ``patch'' that would be required by the outdated Current Description
in order to match the ideal Trustworthy Description.  Let us refer to this amount as 
$\deltat = \textit{patch}(\DS, \RS)$, where \textit{patch} is the classic utility described
in~\cite{diffutils}.

The existence of a description discrepancy is a problem that is particularly relevant when {\RS} is a rapidly evolving
system---as it is the case in software---or when $\S$ is a complex system requiring significant time for its
design and development, or when $\S$ is meant to be operational for a relatively long amount of time. In such cases,
the drift may grow significantly over time.


``Living Documentation'' (LD) is the term proposed in~\cite{Gojko11} to refer to approaches that aim at
minimizing the {\deltat} drift in TD. In the cited reference, the focus is software systems. LD approaches---again,
specifically addressing software---have been further developed in~\cite{Martraire}. A major principle of the latter
approach is to guarantee that Current and Trustworthy Descriptions stay in sync by turning TD into a collaboration
tool between designers, developers, and TD managers.

Living Documentation is also the topic of the present article.  In what follows, we introduce a LD
approach called {\kexp} (for ``Knowledge Extractors and Consumers'').  Although inspired by software LD
approaches, and despite its being heavily based on software tools, {\kexp} does not focus specifically
on software products.  Indeed, {\kexp} has been especially conceived to manage technical documents
that are to follow the evolution of complex, long living nuclear physics systems. One such system is the
MYRRHA Accelerator---a 600 MeV linear accelerator part of a flexible irradiation facility being created
by SCK~CEN in Belgium as a test-bed for transmutation and as a fast-spectrum facility for material and
fuel developments~\cite{P-IAEA03-Abder,P-AccApp11-VanDP}.

In what follows, first we briefly introduce in Sect.~``{\nameref{s:MYRRHA}}'' the MYRRHA project
and its linear accelerator.  Section~``{\nameref{s:kexp}}'' then discusses {\kexp}, while following
Sect.~``{\nameref{s:application}}'' explains how it is being used within MYRRHA to update and maintain
a ``living document'': the MINERVA Technical Design Report~\cite{MINERVATDR}.  Preliminary conclusions
are then drawn in the final section.

%}}}
%{{{ The Target System and Its Lifespan
\section{The Target System and Its Lifespan}\label{s:MYRRHA}
MYRRHA~\cite{P-IAEA03-Abder} may be considered as the culmination of a research and
development path that has been moving forward through more than two decades, four
European Framework Programmes, and several projects. Major milestones of this path have
been FP5 project PDS-XADS~\cite{J-NuclInstrumMethodsPhysRes-2006-562.2-Bia}; FP6 project
EUROTRANS~\cite{P-TCADS10-Knebel}; the three FP7 projects CDT/FASTEF~\cite{ALEMBERTI2014300}, MAX
(``MYRRHA Accelerator eXperiment'')~\cite{10.1007/978-3-319-26542-1_38}, and MARISA (``MyrrhA Research
Infrastructure Support Action'')~\cite{refId0}; and finally H2020 Research and Innovation Action MYRTE
(``MYrrha Research and Transmutation Endeavour'')~\cite{myrte}.

Ongoing project MYRRHA will also have a significant temporal span, subdivided into the workflows of three
relatively long-lasting sub-projects or phases.  The first MYRRHA phase, MINERVA, shall conclude in 2026
with the deployment of a 100~MeV accelerator in Mol, Belgium.  This shall be followed by a second stage
that will extend the MINERVA accelerator up to 600~MeV, ultimately to be connected to a nuclear reactor
in MYRRHA Phase 3.

As the reader will have realized, throughout MYRRHA and its predecessors a significantly complex system
$\S$ is being conceived and engineered~\cite{P-AccApp11-VanDP}. The TD of such system is therefore one
that presents an intrinsic challenge with respect to the problem of drift minimization. In particular,
we shall focus here on a key reference document describing system $\S$: the so-called Technical Design
Report (TDR). As its title already says, the MYRRHA TDR is meant to be the most up-to-date collection
of technical data describing its evolving reference system. By quoting the Editor of that very document,

\begin{quote}
The present version of the TDR is thus a ``snapshot'' in the dynamic progress of the MYRRHA workflow. The
level of detail provided in what follows reflects this fact and will thus only provide data and results
that have stabilised at this stage of the MYRRHA workflow.
\end{quote}

From its Editor's point of view, a major challenge of the MYRRHA TDR is to engineer a TD approach such
that the above ``snapshot'' closely follows the continuous evolution of $\S$. Meeting said challenge
led us to finding an answer to the following two major questions:

\begin{itemize}
  \item How to make sure that the TDR and other documents reflect the current progress of $\S$?
  \item How to detect that the TDR and other documents' contents are drifting away from the current progress of $\S$?
\end{itemize}

In what follows we describe our approach and show how it provides answers to the above questions.

%}}}
%{{{ The Approach
\section{The Approach}\label{s:kexp}
In the course of a system's studies, design, development, and operation, several tools are typically used
to abstract and reason about different aspects of the system and its parts~\cite{BOYD200135}. Analysis and
modeling, simulation, testing, development, deployment, control, validation and verification tools---all
take snapshots of specific characteristics of the system in the course of all phases of its creation
and operation. Said data may reside, e.g., in simulation codes used to assess the system's behaviors,
or in a relational database managed by the system's control system, or in spreadsheets collecting the
system's operational parameters---to name but a few of the possible cases.  Depending on the development
phase a system is in, a varying subset of said design or operational characteristics may represent a good
approximation of that system's Trustworthy Description. Let us call \emph{reference source\/} any holder of
such trustworthy snapshots.

{\kexp} is based on the assumption that a number of reference sources can be identified and are
accessible at any given time by the TD management system. In a nutshell, {\kexp} proposes to build up
\emph{living links\/} between the reference sources and the TD. Instead of manually describing the current contents
of the reference sources, the living links are created by \emph{processes\/} that semi-automatically synchronize data
in the reference sources with associated \emph{place holders\/} in the TD. Changes in the reference sources
are then systematically reflected into changes in the TD.

The basic principles of {\kexp} are similar to those of so-called ``transfer of learning'':
\begin{quote}
``initial acquisition of knowledge in a source context, mapping of the learned knowledge into a target
context, and adaptation of the transferred knowledge to improve system performance in the target''~\cite{Tolga10},
\end{quote}
with the major difference that in {\kexp} both source and target are TD and the major goals, rather than
the improvement of system performance, are the following ones:

\begin{description}
  \item[Change detection:] Detecting discrepancies between the Current and the approximated Trustworthy Description.
  \item[Change tolerance:] Minimizing the drift between the Current and the approximated Trustworthy Description.
  \item[Consistency failure detection:] Identifying inconsistencies in the approximated Trustworthy Description.
\end{description}

The proposed approach to achieve the above goals is based on the creation of two families of tools:

\begin{description}
  \item[Knowledge extractors:] Codes that achieve the above-mentioned ``initial acquisition of knowledge'':
	they parse or query the reference sources, collect values describing
	the major caracteristics of the system, and create a convenient representation of those values
	(e.g., as JSON data structures).
  \item[Knowledge consumers:] Codes that map and use the data produced by the knowledge extractors to create
	the above-mentioned living links: drift detectors; values for place holders; inconsistency detectors, etc.
	(A description of the living links is given in Sect.~``\nameref{s:kw-consumers}).''
\end{description}

Let us now make things more concrete by showing how {\kexp} is being applied to the MYRRHA TD.
%}}}
%{{{ Applying KeXP to MYRRHA

\section{Applying {\kexp} to MYRRHA}\label{s:application}

In the course of the first phase of MYRRHA, two major reference sources have spontaneously emerged:
\begin{enumerate}
  \item A TraceWin~\cite{P-IPAC15-Uriot} code used for end-to-end simulation of the accelerator's
	components and beam dynamics.  In order to simulate the intended behavior of the system and its
	beam, said code specifies the most up-to-date architecture of the target system, as well as the
	parameters used to steer each subsystem towards the intended local and global behaviors. Apart from
	the simulation code properly so called, the TraceWin code in use within MYRRHA also piggybacks
	a number of system parameter values, specified with TraceWin syntax---though, in some cases,
	commented out.

  \item A system register, running in the control system of our prototypical accelerator in Louvain-la-Neuve,
	Belgium. All of the currently installed system components are command-and-controlled via
	said system register. A full and timely Trustworthy Description of the system, in XML, can be
	retrieved at any time by means of a representational state transfer~\cite{enwiki:1045660779}
	requested via HTTP methods~\cite{enwiki:1044933145}.
\end{enumerate}

Because of their roles---the former as a design tool, the latter as an operational tool---both reference
sources represent necessary instruments towards the engineering of the target system, and as such they
provide partially overlapping Trustworthy Descriptions of the MYRRHA accelerator.

\subsection{Knowledge extractors}

Two knowledge extractors have been created to distil relevant information from the reference sources mentioned
in Sect.~``\nameref{s:application}.''

The first one---{\tracex}---parses the MYRRHA TraceWin reference source and produces a JSON data structure
summarizing the characteristics of $\S$ as they are specified in the TraceWin code.  This includes
both parametric and structural information about the MYRRHA accelerator.  Also commented TraceWin
statements are parsed, although the information extracted from those statements ``remembers'' this fact.
Consolidated information are marked as such, and unexpected deviations produce warning messages---see
Fig.~\ref{fig:tracex:unexpected}.  This allows assumption failures~\cite{De10} regarding the employed
reference sources to be detected.

\begin{figure*}[h]
\centering
	\includegraphics[width=0.90\textwidth]{tracex-unexpected.png}
\caption{The {\tracex} parsers deduce a structural information that, in this case, contradicts the consolidated architecture of $\S$.}
\label{fig:tracex:unexpected}
\end{figure*}

The second knowledge extractor---{\naming}---issues an HTTP GET request to the naming service of our
control system.  The latter offers a RESTful API~\cite{10.5555/2566876} that returns an XML string
describing the structure of our prototypical MYRRHA injector in Louvain-la-Neuve. The string is parsed
and a second JSON data structure is produced.

The data structures returned by {\tracex}  and {\naming} are not disjoint, meaning that some of the system information
that they describe pertain to the same system properties. As a consequence, in an ideally perfect
situation, the corresponding data fields should be equal. Of course in reality it is possible that a
discrepancy \emph{does\/} exist. As we shall see in the following subsection, knowledge consumers can
easily detect said inconsistency and issue warnings.


\subsection{Knowledge consumers}\label{s:kw-consumers}
Several knowledge consumers have been developed. The one best matching the case at hand---the MYRRHA TDR---is called
{\latexmacros}. 

Module {\latexmacros} makes use of the information made available by both of the above mentioned knowledge
extractors to create so-called living links and living pictures for the MYRRHA TDR.  Living links are, in
the current, \LaTeX{}-based implementation of the TDR, a set of macros. 

Three types of macros are created. Figure~\ref{fig:latexmacros:1} shows an example of the first and simplest
type: \emph{parameter macros}.  These macros code key reference parameters of $\S$ (for instance, the number of
cryomodules foreseen in the accelerator). Figure~\ref{fig:latexmacros:2} shows \emph{instance macros}---macros
that identify instances of system components (for instance, a certain cryomodule). These macros check
that their argument (an instance identifier) matches the system structure deduced from the Trustworthy
Description. Figure~\ref{fig:latexmacros:3} provides an example of \emph{consistency checking macros}: macros
that check that the values parsed from the available Trustworthy Descriptions and referring to the same
system properties are mutually consistent.

%\begin{figure*}[h]
\begin{figure}[h]
\centering
	%\includegraphics[width=0.70\textwidth]{living-macros-1.png}
	\includegraphics[width=0.50\textwidth]{living-macros-1.png}
\caption{System parameters are assigned to \LaTeX{} macros by module {\latexmacros}.}
\label{fig:latexmacros:1}
%\end{figure*}
\end{figure}

\begin{figure}[h]
\centering
	\includegraphics[width=0.50\textwidth]{living-macros-2.png}
\caption{System components---in this case, spoke cavities---are assigned to instance macros: macros that check their own arguments
	to verify whether they respect the system structure of the Trustworthy Description.}
\label{fig:latexmacros:2}
\end{figure}

\begin{figure*}[h]
\centering
	\includegraphics[width=0.94\textwidth]{living-macros-3.png}
\caption{An example of a consistency checking macros: the value of a same system parameter, parsed by two reference sources,
	is checked for consistency.}
\label{fig:latexmacros:3}
\end{figure*}

As already mentioned, module {\latexmacros} also creates so-called \emph{living
pictures}. Figure~\ref{fig:latexmacros:4} provides an example of such feature. The actual pictures to
be used in the MYRRHA TDR (or any other TD using them) are obtained from template pictures by annotating
the templates with parameter values obtained from the reference sources.

\begin{figure*}[h]
\centering
	\includegraphics[width=0.90\textwidth]{living-pictures.png}
\caption{Module {\latexmacros} creates living pictures from picture templates. }
\label{fig:latexmacros:4}
\end{figure*}

%%EnableIfSpaceAllows%  Figure~\ref{fig:latexmacros} shows how module {\latexmacros} is typically invoked when processing the MYRRHA TDR.
%%EnableIfSpaceAllows%  
%%EnableIfSpaceAllows%  The overall {\kexp} workflow is summarized in Fig.~\ref{fig:summary}.
%%EnableIfSpaceAllows%  
%%EnableIfSpaceAllows%  
%%EnableIfSpaceAllows%  \begin{figure}[h]
%%EnableIfSpaceAllows%  \centering
%%EnableIfSpaceAllows%  	\includegraphics[width=0.50\textwidth]{LaTeXmacros.png}
%%EnableIfSpaceAllows%  \caption{Module {\latexmacros} is invoked and creates living links and living pictures for the MYRRHA TDR.}
%%EnableIfSpaceAllows%  \label{fig:latexmacros}
%%EnableIfSpaceAllows%  \end{figure}
%%EnableIfSpaceAllows%  
%%EnableIfSpaceAllows%  \begin{sidewaysfigure}
%%EnableIfSpaceAllows%  \centering
%%EnableIfSpaceAllows%  	\includegraphics[width=0.90\textwidth]{scheme3.2.png}
%%EnableIfSpaceAllows%  	\caption{A summary of the MYRRHA knowledge extractors and knowledge consumers.}
%%EnableIfSpaceAllows%  \label{fig:summary}
%%EnableIfSpaceAllows%  \end{sidewaysfigure}

% The first is a design tool, the second a control tool: both necessary instruments towards the engineering of the system.

%}}}
%{{{ Conclusions
\section{Conclusions}
We have presented a living documentation approach and its application to a long living system from the domain of nuclear physics---the MYRRHA linear accelerator---and to its major TD item---the MYRRHA TDR.
The approach is currently being used and evaluated. Preliminary results are promising.
Change detection warnings were issued when values contradicting the reference sources were erroneously introduced.
Our tools facilitated the management of the required corrections by highlighting the ``document loci'' that had experienced
the inconsistencies. When simulating inconsistencies in the reference sources, those were correctly identified.
Automatic adjustments of parameters were checked and, after the removal of a few glitches in our tools
it was observed that the contents of the text and pictures correctly tracked the contents of the reference sources, thus
leading to a minimization of the {\deltat}.
As a consequence, the maintenance of the TDR has been considerably simplified and automatized.

The adoption of open tools and de-facto standard data formats makes it possible to
independently create new knowledge consumers. This has led to an unforeseen beneficial development:
the knowledge distilled from the reference sources is currently being used also for purposes
other than those that led to the creation of our approach. As an example, a new tool now consumes the JSON
data structures produced by {\tracex} to create graphical representations of the beam flow through our accelerator,
with the leaves of the system tree being annotated with a human-readable description of the TraceWin statements pertaining to that node (see Fig.~\ref{fig:linac_svg:1}, Fig.~\ref{fig:linac_svg:2}, and Fig.~\ref{fig:linac_svg:3}).

\begin{figure}[h]
\centering
	\includegraphics[width=0.50\textwidth]{linac_svg_01.png}
\caption{Beam flow produced by the {\linac} knowledge consumer: Top level.}
\label{fig:linac_svg:1}
\end{figure}

\begin{figure}[h]
\centering
	\includegraphics[width=0.50\textwidth]{linac_svg_02.png}
	\caption{Beam flow produced by the {\linac} knowledge consumer: Mid level (lbs section).}
\label{fig:linac_svg:2}
\end{figure}

\begin{figure*}[h]
\centering
	\includegraphics[width=0.80\textwidth]{linac_svg_03.png}
	\caption{Beam flow produced by the {\linac} knowledge consumer: Bottom level (TraceWin statements).}
\label{fig:linac_svg:3}
\end{figure*}


Future extensions to {\kexp} might include the creation of a knowledge extractor able to query Polarion documents~\cite{PolarionWebsite}
for parameter values and other ``living'' resources.
Other work might involve the use of semantic technologies to produce more ``intelligent'' knowledge consumers,
able to detected semantic inconsistencies in the reference sources.
%}}}
\printbibliography
\end{document}

%{{{ garbage
@article{DBLP:journals/corr/Florio16a,
  author    = {Vincenzo {De~Florio}},
  title     = {Software Assumptions Failure Tolerance: Role, Strategies, and Visions},
  journal   = {CoRR},
  volume    = {abs/1605.02040},
  year      = {2016},
  url       = {http://arxiv.org/abs/1605.02040},
  eprinttype = {arXiv},
  eprint    = {1605.02040},
  timestamp = {Mon, 13 Aug 2018 16:48:11 +0200},
  biburl    = {https://dblp.org/rec/journals/corr/Florio16a.bib},
  bibsource = {dblp computer science bibliography, https://dblp.org}
}

@misc{ enwiki:1021916913,
    author = "{Wikipedia contributors}",
    title = "Technical documentation --- {Wikipedia}{,} The Free Encyclopedia",
    year = "2021",
    url = "https://en.wikipedia.org/w/index.php?title=Technical_documentation",
    note = "[Online; accessed 15-September-2021]"
  }

  "Cost-Effective Software Reliability Through Autonomic Tuning of System Resources," De Florio, V., the 2010 Int.l Applied Reliability Symposium, Europe, Berlin, Germany, April 7-9, 2010

    It is reliable - all documentation products are always accurate and in sync with the actual code

    Producing and updating it is low effort

    It is both a product and a medium of collaboration between all involved people

    It is insightful for its consumers and sheds light on the important aspects of the system

%}}}
% End of file kxkp.tex
