#!/usr/bin/env python3

#
# epicspost.py, by vdflorio
#
# Queries process variable MyPV and issues an HTTP POST to BackEndURL
# If BackEndURL is the URL of the backend of the fault tracking system, this results in logging the value of MyPV into the faults db
#
# v. 1.3 ( Mi 26 Jan 2022 15:48:33 CET )
# Removed bug (caget always queried the default PV)
# The logged record now specifies the system components the PV originates from

# default values
MyPV = "I1-LE:VAC-AV-04:ValveMainView"
BackEndURL = "http://tdr.myrrha.lan/vfolders/fault-tracking/form.cgi"
THIS_CODE = "epicspost.py"

import epics
import requests
import datetime
import sys

def parsePV(pv):
    try:
        l = pv.replace('-', ':').split(':')
        if len(l) > 4:
            return [ l[0], l[1], l[2], '.'.join(l[3:]) ]
        return l
    except:
        return False

def epicspost(processVariable=MyPV, backEndURL=BackEndURL, verbose=False, submit=True):
    # query a PV
    pv=epics.caget(processVariable)
    if pv == None: return False, f'could not query PV {processVariable}'

    if verbose: print(f"Process Variable {processVariable} is equal to {pv}")

    # curl -X POST -F "Description=..." -F "Minerva 600MeV (Phase 2) (1)=..." -F "Status=..." 
    #              -F "MYRRHA+REACTOR (Phase 3)=..." -F "Repair action:=..." -F "Date and Time (of Failure)=..."
    #              -F "Failure Description:=..." -F "Duration OP was interrupted because of failure=..."
    #              -F "Component:=..." -F "Comments=..." -F "Minerva 600MeV (Phase 2)=..." -F "comment:=..."
    #              -F "Resolution status=..." -F "user_id=..." -F "MINERVA 100MeV (Phase1)=..."
    #              -F "Parent Fault (report ID)=..." -F "Previous occurrences (report ID)=..." -F "comment: (1)=..."
    #              -F "Report ID:=..." -F "MINERVA 100MeV (Phase1) (1)=..." -F "Cause(s)=..."
    #              -F "MYRRHA+REACTOR (Phase 3) (1)=..."
    #                  http://tdr.myrrha.lan/vfolders/fault-tracking/form.cgi

    now = datetime.datetime.now()

    l = parsePV(processVariable)
    if not l:
        # print("exception!")
        return False, f'could not parse PV {processVariable}'

    try:
        record = {
                "user_id" : f"EPICS({processVariable})",
                "Description" : f"Microfault({processVariable})",
                "Date and Time (of Failure)" : now,
                "Report ID:" : "10000",
                "MINERVA 100MeV (Phase1) (1)" : "yes",
                "MINERVA 100MeV (Phase1)" : "yes",
                "Status" : str(pv),
                "comment:" : f"Injected via {THIS_CODE} on {now}.",
                "Sub-Section:" :      f"{l[0]}",
                "Device instance:" :  f"{l[1]}",
                "Component:" :        f"{l[2]}",
                "Sub-Component" :     f"{l[3]}",

                "Minerva 600MeV (Phase 2)" : "no",
                "MYRRHA+REACTOR (Phase 3)" : "no",
                "Minerva 600MeV (Phase 2) (1)" : "no",
                "MYRRHA+REACTOR (Phase 3) (1)" : "no",

                "Repair action:" : "...",
                "Failure Description:" : "...",
                "Duration OP was interrupted because of failure" : "...",
                "Comments" : "...",
                "Resolution status" : "...",
                "Parent Fault (report ID)" : "...",
                "Previous occurrences (report ID)" : "...",
                "comment: (1)" : "...",
                "Cause(s)" : "...",
                }
    except:
        return False, 'could not set fault tracking record'

    if submit:
        # https://stackoverflow.com/questions/10231707/how-can-i-simulate-a-web-page-form-submit-with-a-python-code-behind
        try:
            r = requests.post(
                backEndURL,
                data=record,
                # headers={"Content-Type": "multipart/form-data"},
                # files={},
                verify=False,
            )
            # if verbose: print(f"The post request returned: status: {r.status_code}")
            # if verbose: print(f"The post request returned: text {r.text}")
            return r, 'Success!'
        except:
            # print("exception!")
            return False, 'POST request failure'
    return True, 'Success!'


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(epilog=f'default values: -g {MyPV} -b {BackEndURL}. To log the value, add -s.')
    parser.add_argument('-g', '--getPV',   default=MyPV)
    parser.add_argument('-b', '--backend', default=BackEndURL)
    parser.add_argument('-v', '--verbose', default=False, action='store_true')
    parser.add_argument('-s', '--submit',  default=False, action='store_true')
    parser.add_argument('-n', '--nodump',  default=False, action='store_true')
    args = parser.parse_args()

    # if args.verbose:
    #    print("{THIS_CODE} queries process variable --getPV and issues an HTTP POST to --backend", file=sys.stderr)
    #    print(f"default values are --getPV {MyPV} and --backed {BackEndURL}", file=sys.stderr)
    #    sys.exit(0)

    r, expl = epicspost(processVariable=args.getPV, backEndURL=args.backend, verbose=args.verbose, submit=args.submit)

    if r == False:
        print(f'{THIS_CODE} encountered an exception: {expl}.', file=sys.stderr)
        sys.exit(-1)
    elif r == True:
        print(f'{THIS_CODE} correctly processed the request, though did not submit it to the backend.', file=sys.stderr)
        sys.exit(0)

    if args.verbose: print("Record successfully handed to the backend.", file=sys.stderr)
    if not args.nodump: print(f"The post request returned: text {r.text}\nreturn code: {r.status_code}", file=sys.stderr)

# EoF epicspost.py
