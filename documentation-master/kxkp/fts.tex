% Beginning of file fts.tex

%{{{ Prologue
% !TeX spellcheck = en_GB
% !TeX program = lualatex
%
% v 2.3  Feb 2019   Volker RW Schaa
%
\documentclass[a4paper,
               %boxit,        % check whether paper is inside correct margins
               %titlepage,    % separate title page
               %refpage       % separate references
               biblatex,     % biblatex is used
               keeplastbox,   % flushend option: not to un-indent last line in References
               %nospread,     % flushend option: do not fill with whitespace to balance columns
               %hyphens,      % allow \url to hyphenate at "-" (hyphens)
               %xetex,        % use XeLaTeX to process the file
               luatex,       % use LuaLaTeX to process the file
               ]{jacow}
%
% ONLY FOR \footnote in table/tabular
%
\usepackage{pdfpages,multirow,ragged2e} %
%
% CHANGE SEQUENCE OF GRAPHICS EXTENSION TO BE EMBEDDED
% ----------------------------------------------------
% test for XeTeX where the sequence is by default eps-> pdf, jpg, png, pdf, ...
%    and the JACoW template provides JACpic2v3.eps and JACpic2v3.jpg which
%    might generates errors, therefore PNG and JPG first
%
\makeatletter%
	\ifboolexpr{bool{xetex}}
	 {\renewcommand{\Gin@extensions}{.pdf,%
	                    .png,.jpg,.bmp,.pict,.tif,.psd,.mac,.sga,.tga,.gif,%
	                    .eps,.ps,%
	                    }}{}
\makeatother

% CHECK FOR XeTeX/LuaTeX BEFORE DEFINING AN INPUT ENCODING
% --------------------------------------------------------
%   utf8  is default for XeTeX/LuaTeX
%   utf8  in LaTeX only realises a small portion of codes
%
\ifboolexpr{bool{xetex} or bool{luatex}} % test for XeTeX/LuaTeX
 {}                                      % input encoding is utf8 by default
 {\usepackage[utf8]{inputenc}}           % switch to utf8

\usepackage[USenglish]{babel}

%
% if BibLaTeX is used
%
\ifboolexpr{bool{jacowbiblatex}}%
 {%
  \addbibresource{thesis.bib}
  % \addbibresource{jacow-test.bib}
  % \addbibresource{biblatex-examples.bib}
 }{}
\listfiles

%%
%%   Lengths for the spaces in the title
%%   \setlength\titleblockstartskip{..}  %before title, default 3pt
%%   \setlength\titleblockmiddleskip{..} %between title + author, default 1em
%%   \setlength\titleblockendskip{..}    %afterauthor, default 1em

% extra packages and definitions added by vdflorio
% \usepackage{latexsym}
% \usepackage{amsmath}
% \usepackage{amssymb}
\usepackage{graphicx}

\usepackage{float}

\usepackage[pdftex,
            colorlinks=true,
	    % plainpages=true,
            linkcolor=blue
           ]{hyperref}
\graphicspath{{./}{/home/vdflorio/Documents/Git/documentation/kw/}}
% \usepackage{bookmark}% http://ctan.org/pkg/bookmark
\usepackage{cleveref}

% \usepackage{nameref}
% \GetTitleStringSetup{expand}

\usepackage{alltt}
\usepackage{rotating} % sidewaysfigure

% definitions
\def\sckcen{\mbox{SCK{\kern.4ex}CEN}}
\def\S{\mathcal{S}}
\def\DS{\hbox{$D_{\S}(t)$}}
\def\RS{\hbox{$R_{\S}(t)$}}
\def\kexp{KExC}
\def\deltat{\hbox{$\Delta_{\S} t$}}
\def\tracex{\texttt{tracex.py}}
\def\naming{\texttt{naming.py}}
\def\latexmacros{\texttt{LaTeXmacros.py}}
\def\linac{\texttt{linac.py}}
\def\mF{\hbox{$\cal{F}_{\kern -0.2ex \mu}$}}
\def\MF{\hbox{$\cal{F}_{\kern -0.35ex\raisebox{-0.02ex}{${}_M$}}$}}

% \usepackage[backend=bibtex,style=numeric,sorting=none]{biblatex}
% \addbibresource{thesis}

%% \DeclareUnicodeCharacter{0308}{}

%%%\let\oldhref\href
%%%\renewcommand{\href}[2]{\oldhref{#1}{\hbox{#2}}{}}
%}}}

%\hypersetup{draft}

\begin{document}
%{{{ Front matter
\title{A Fault Tracking Service for the MYRRHA Accelerator}

\author{V. De~Florio\textsuperscript{1}\thanks{vincenzo.de.florio@sckcen.be}, J. Belmans, P. Della~Faille,
	A. Gatera, A. Ponton, D. Vandeplassche, \\ \sckcen, Mol, Belgium\\
	L. Felsberger, J. Uythoven, \\ CERN, Meyrin, Switzerland\\
		% P. Contributor\textsuperscript{1}, Name of Institute or Affiliation, City, Country \\
		\textsuperscript{1}also at CLEA/VUB, Brussels, Belgium}
	
\maketitle

%
\begin{abstract}
	The stringent reliability requirements of the MYRRHA accelerator call for a reliability model able
	to characterize the behavior of the system in the face of faults, and
	to guide system design towards reliability-improving enhacements.
	In turn, the MYRRHA reliability model calls for detailed ``snapshots'' of the faults and disruptions
	experienced during operation.  Taking and managing those snapshots is the main objective of the MYRRHA
	fault tracking service. This paper describes that service and highlights the characteristics of
	its peculiar design and how it matches the MYRRHA requirements on reliability and adaptability.
\end{abstract}

%}}}
%{{{ Intro
\section{Introduction}
MYRRHA~\cite{P-IAEA03-Abder,P-AccApp11-VanDP} (Multi-purpose hYbrid Research Reactor for High-tech
Applications) is an ongoing {\sckcen} project that aims to create a so-called accelerator-driven system: A sub-critical nuclear
reactor coupled to a 600~MeV proton accelerator. The current stage of MYRRHA is called MINERVA. Its aim
is the design of a 100~MeV linac, which is planned to be built in Mol, Belgium, by 2026.

A peculiar characteristic differentiating MYRRHA's from other accelerators of the same
class~\cite{P-PAC12-VanDP} is given by its comparably harder reliability requirements: in MYRRHA,
beam trips lasting longer than 3 seconds need to be minimized as they are deemed to lead to system failures.
% would induce high thermal stresses and fatigue on the reactor
% structures, with possible significant damages especially to the fuel claddings. 
When considering the
standard operating cycle of the MYRRHA linac, namely 90 days, minimizing such beam trips translates
in the requirement for a mean time between failures (MTBF) of at least 250 hours.

%%  This article describes the design of the fault tracking system (FTS)
%%  of the MYRRHA accelerator---a 600 MeV linear accelerator that is being developed by SCK~CEN in
%%  Belgium~\cite{P-IAEA03-Abder,P-AccApp11-VanDP}. A key aspect that characterizes MYRRHA is given by its
%%  peculiarly high reliability requirements: It has been estimated that beam interruptions (``beam trips'')
%%  lasting more than 3~s, when occurring with a frequence greater than 10 within the 3-month operational
%%  cycle of the accelerator, would induce high thermal stresses and fatigue on the reactor structures---the
%%  target or the fuel elements---with possible significant damages especially to the fuel claddings.

This amount of 3~s is an important design parameter. In layman's terms, we could say that it represents the
border that separates a ``green'' region of regular, unharmful events from a ``red'' region characterized
by faults that, without proper handling, would trigger damage and system failures. In MYRRHA, the ``red''
region extends throughout an area much larger than in other accelerators. This fact leads to a number
of cascading requirements:

\begin{enumerate}
  \item An effectively dimensioned fault-tolerance design~\cite{DF15b,John89a} matching MYRRHA's
	``red region'' (namely, its intended MTBF) is required.

  \item As well-known, a fault-tolerant design is in itself no guarantee that the intended reliability be
	actually sustained: the effectiveness of the available failure mitigation strategies and their
	global effect on the reliability of the accelerator must be verified.  Therefore, the MYRRHA
	architecture and its fault-tolerance design patterns must be chosen and made redundant in function
	of the expected reliability requirements.

% Collecting and maintaining a database of these data is the key objective of the FTS.

\item A common and cost-effective way to achieve a MYRRHA reliability-compliant design is by means of
	a reliability model~\cite{P-CaEdPN20-Bouly} able to capture trustworthily the behavior of the
	accelerator both in the absence and in the face of faults and failures.  One such model is currently
	being created for MYRRHA at CERN in the framework of the H2020 project ``Patricia''~\cite{Patricia}.

  \item In order to be effective, said model must be close to reality: Therefore, it must be tuned with
	real data collected while the accelerator is in operation and representing in a convenient
	form the faults experienced by the machine. These experimental reliability statistics must be
	collected in the course of so-called ``reliability runs'', namely practical experience with the
	machine~\cite{P-IPAC18-Rey}.

  \item A tool needs to be set in place in order to keep track and maintain the adverse events that occorred
	during the reliability runs.
	Said tool---the MYRRHA fault tracking system (FTS)---is the central topic of the present article.

  \item Another requirement derives from the peculiar ``red region'' of MYRRHA.
	Said region experiences two types of events: quick, short-lasting failures, often too subtle and
	potentially too many for being timely logged, or even perceived, by human beings, and regular,
	``slowlier'' failures, which fall within the human perception and reaction domains. We call
	the faults associated with the former failures	\emph{microscopic faults\/} (\mF), and those
	associated with the latter \emph{macroscopic faults\/} (\MF). The MYRRHA FTS needs
	to be designed so as to deal with both types of faults.
\end{enumerate}

An additional requirement of the FTS, unrelated to its peculiar ``red region'', is high adaptability:
at the current design stage, system components and their hierarchy have not been fully defined. The FTS
is expected to adapt dynamically after the evolving system specifications of MYRRHA.

In what follows, we describe how the design of the FTS matches its two major design requirements:
\begin{description}
	\item[R1:] management of both {\mF} and {\MF}.
	\item[R2:] high adaptability.
\end{description}
%}}}
%{{{ R1: Management of micro- and macrofaults
\section{Management of micro- and macrofaults}
The strategy adopted in the FTS to manage both {\mF} and {\MF} is based on the adoption of a user interface
intended for human operators and of an API allowing the MYRRHA control system to log events autonomously.

Management of {\MF} is based on a web front-end application with an HTML/Javascript form handing data to
a back-end via HTTP POST (see Fig.~\ref{f:frontend}).  The back-end receives the data and logs them
into a PostgreSQL database (see Fig.~\ref{f:backend}).  A detailed description of the interface is given
in next subsection.

Management of {\mF} is done through an API that queries EPICS~\cite{O-WebPage-EPICS-d}
process variables (PVs).  As well known, PVs are ``reflective variables''~\cite{DB07a}, namely objects whose
value changes asynchronously after a given physical or logical variable of interest. Therefore, failures
correspond to PVs assuming values that go beyond their specific interval of ``safe values''. When this
happens, an HTTP POST is initiated and sent to the back-end in order to log the corresponding {\mF}. Python
module \texttt{epicspost} was written for this purpose---see Fig.~\ref{f:epicspost} and \ref{f:epicspost2}.

\begin{figure*}
\centering
 \includegraphics[width=0.7\textwidth]{frontend.png}
\caption{Front-end of the FTS. Some of the fields are pre-filled. The picture has been sliced and recomposed
	in order to make it more readable.}
\label{f:frontend}
\end{figure*}

\begin{figure*}
\centering
 \includegraphics[width=0.7\textwidth]{response.png}
\caption{Back-end of the FTS showing a response to an HTTP POST. Portions of the picture have been removed
	in order to make it more readable. The response includes an equivalent cURL command~\cite{enwiki:1069321402}.}
\label{f:backend}
\end{figure*}

Failures associated with the current value of multiple PVs are currently more difficult to express
in a generic way.  A possible solution to this limitation could be given by a lightweight programming
language such as Ariel~\cite{DF00}, which would permit to express guarded actions triggered by logical
and arithmetical expressions on the current value of a set of PVs.  This may allow failures based on
a plurality of inputs---for instance through voting~\cite{DeDL98e,DeDe02c}---to be defined, and could
be used to facilitate the detection of \emph{trends\/} with the aim to anticipate
failures\footnote{An approach for this is described in~\cite{DF15d,Buys20.3}: There, by monitoring the
	amount of redundancy effectively used, one can compute a ``distance-to-failure'' specifying the risk to
	approach and go beyond a previously defined worst-case scenario.}.

The same language could be used to easily express a ``reactive layer'' on top of the MYRRHA control system.
User-defined error recovery ``snippets'' could be associated to a subset of the conditions logged into
the FTS database. Each time one such condition would be detected, the control system would execute the
associated snippet to execute user-defined ``error recovery actions''~\cite{DF00}.

\begin{figure}
\centering
 \includegraphics[width=0.5\textwidth]{epicspost.png}
\caption{Module \texttt{epicspost} is used here as a command line tool to log the value of a PV into the FTS database.}
\label{f:epicspost}
\end{figure}

\begin{figure}
\centering
 \includegraphics[width=0.5\textwidth]{epicspost2.png}
\caption{Excerpt from the output of a successful call to \texttt{epicspost}.}
\label{f:epicspost2}
\end{figure}

%%   This paves the way to fault tracking and post-mortem logging driven by the MINERVA control system
%%   
%%   Provided that a mechanism exists able to submit ``fast events'', the FTS may also log those events and associate them
%%   to an emerging ``slow event''
%%   
%%   Ongoing work: windowing mechanism in EPICS
%}}}
%{{{ R2: High adaptability
\section{High adaptability}
%%% In what follows we show that goal \textbf{R2} is achieved
%%% by deriving the FTS' structure and data model from ``living documents'' and live data representing
%%% the most up-to-date design specifications, and by constructing and deploying the FTS accordingly.
%%% This is reached by extending the approach developed in~\cite{kexp21a} and briefly presented in next
%%% subsection.

% Once closely tuned, the Patricia model would allow to estimate the impact on reliability of various system configurations
% and fault-tolerance design patterns. In turn, this would assist the system designers
% in the decision strategy to allocate redundant resources, considering both local
% and global reliability and availability improvements;

%The primary design goal of the FTS described in this paper is to create a database of ``quickly occurring'' as well as


% record deviations occurring at all system levels and taking place very rapidly.

% As a consequence, a reliability model and a fault tracking service (FTS) are being created for the MINERVA
% accelerator. The FTS is to feed the reliability model with operational data collected while running the
% accelerator and describing faults, service deviations, and their characteristics.  The collection of
% operational data corresponding to two or more operational cycles shall be used to validate and refine
% the reliability model.
% 
% A major difference characterizing the MINERVA FTS with respect to other services such as the LINAC4
% FTS is given by the fact that the logging of reliability events (such as faults or service disruptions)
% need to be operated autonomously: for those events that occur too quickly and too frequently for a human
% operator to manage them, it is the MINERVA control system that is expected to operate the logging as
% soon as each event is detected.
% 
% (Question for Philippe: is there a ``system database'' in the control system? How does the control system
% react, or shall react, to faults?)

%This paper focuses on the prototypic implementation of a FTS for MINERVA.
%Design requirements for the FTS include:

%{{{ Knowledge extractors and consumers
%% \subsection{Living documents and living code}
MYRRHA builds on top of a long-lasting research effort that
is foreseen to further continue for several years. As a consequence, it is paramount that the MYRRHA
technical documentation track closely the progress and evolution of its reference systems. An approach
that facilitates the update of technical documentation with respect to the actual or intended documented
system has been proposed in~\cite{Gojko11,Martraire} and is known as ``living documentation'' (LD). Living
documents are those that can be (semi-)automatically updated with respect to a number of trustworthy
design or operational sources---design diagrams, simulation models, control system databases, etc.

\begin{figure*}
\centering
 \includegraphics[width=1.0\textwidth]{data-model.png}
\caption{Worksheet used to specify the data acquisition model for the FTS. The picture has been sliced and recomposed
	in order to make it more readable.}
\label{f:data-model}
\end{figure*}

As a response to the requirement for a living documentation,
within the MINERVA phase of MYRRHA we have designed a LD approach~\cite{kexp21a} based on two sets of tools:

\begin{itemize}
  \item Knowledge extractors: tools that either parse a trustworthy design source or query dynamically a trustworthy operational
	source. Said tools ``take a snapshot'' of the reference system in the form of a standard data structure (e.g. JSON).
  \item Knowledge consumers: tools that make use of the extracted data structure to perform some task---for instance,
	to facilitate the update of the technical documentation.
\end{itemize}

In order to achieve requirement \textbf{R2}, we implemented two knowledge extractors:
\begin{itemize}
  \item A first one that derives the data acquisition model and the system hierarchy intended for our FTS from a trustworthy source:
	an excel spreadsheet that spontaneously emerged as a means to discuss the shape and look of
	the FTS.
	Figure~\ref{f:data-model} shows the data acquisition model as it appears in the specification worksheet.
	Figure~\ref{f:system-hierarchy-excel} shows the system herarchy, rendered as a hypertextual SVG graph.
  \item A second one that queries the RESTful interface~\cite{10.5555/2566876} of the MYRRHA control system
	at Louvain-la-Neuve in order to derive an alternative system hierarchy.
	Figure~\ref{f:system-hierarchy-rest} shows the second system herarchy as a hypertextual SVG graph.
\end{itemize}

The two system hierarchies are interchangeable and a mapping exist from one to the other.

In addition to the above knowledge extractors, we also created a custom knowledge consumer. A peculiar
aspect of said tool is that it does not update a living document; rather, it performs parametric adaptation~\cite{Tolga10}
of a software application, carried out according to the data structures provided by the two knowledge
extractors. Said application then generates both back-end and front-end---see Fig.~\ref{f:kc}.

%{{{ Rapid application prototyping the FTS tool
%%% \subsubsection{FTS as a living code.}
%%% As previously mentioned, the FTS tool has been developed following the {\kexp} approach~\cite{kexp21a}.
%%% In more detail:
%%% \begin{itemize}
%%%   \item The data acquisition model of the FTS is derived from a specification model---an excel worksheet.
%%% 	A knowledge extractor outputs the model as a JSON data structure.
%%% 	Figure~\ref{f:data-model} shows the data acquisition model as it appears in the specification worksheet.
%%%   \item Two alternative system hierarchies are then obtained as follows:
%%%   \begin{itemize}
%%%     \item A first system hierarchy, partially shown in Fig.~\ref{f:system-hierarchy-excel},
%%% 	  is extracted from a second excel worksheet.
%%%     \item A second system hierarchy is derived by issuing an HTTP GET request to the naming service of the
%%% 	  MYRRHA control system at Louvain-la-Neuve. The latter offers a RESTful API that returns an XML string
%%% 	  representing the system components. The string is parsed and a second JSON data structure is produced.
%%% 	  Figure~\ref{f:system-hierarchy-rest} provides an excerpt of this second hierarchy.
%%% 		  % , created by invoking the knowledge extractor as depicted in Fig.~\ref{f:i1}.
%%%   \end{itemize}
%%% \item The FTS application---both back-end and front-end---is then produced by a knowledge consumer as shown in Fig.~\ref{f:kc}.
%%% \end{itemize}

\begin{figure*}
\centering
 \includegraphics[width=0.80\textwidth]{kc.png}
\caption{A knowledge consumer creates and deploys all components of the FTS application.}
\label{f:kc}
\end{figure*}

\begin{sidewaysfigure}
\centering
 \includegraphics[width=1.0\textwidth]{sys-hierarchy-1.png}
\caption{System hierarchy, as extracted from the specification model. The hierarchy is converted into a SVG
	 picture annotated with hyperlinks to the front-end of the FTS.}
\label{f:system-hierarchy-excel}
\end{sidewaysfigure}

\begin{sidewaysfigure}
\centering
 \includegraphics[width=1.0\textwidth]{system-hierarchy.png}
\caption{System hierarchy, as extracted from the MYRRHA control system.}
\label{f:system-hierarchy-rest}
\end{sidewaysfigure}

%% \begin{figure}
%% \centering
%%  \includegraphics[width=0.5\textwidth]{i1.png}
%% \caption{The ``livingdata'' knowledge extractor queries the naming service of the MINERVA control system.}
%% \label{f:i1}
%% \end{figure}

%}}}
%}}}
%}}}
%{{{ Conclusions & future work
\section{Conclusions and Possible Future Work}\label{s:conclusions}
We have introduced the fault tracking service for the MYRRHA accelerator, designed
to match two challenging requirements: autonomic logging of ``fast'' events and high adaptability.
The developed tool is still prototypical, and several aspects are still in need of an optimal solution.
In particular, 

\begin{itemize}
  \item Faults are assumed to be detected via PVs, though at the moment there is no simple way
	to express conditions based on expressions on multiple PVs. Also there is no support for the
	dynamic tracking of PV values, which could be used to detect trends and anticipate faults. The
	use of an ad hoc language such as Ariel~\cite{DeDL98e,DF00,DeDe02c}, embedded in our EPICS
	control system, could solve this issue.

  \item The FTS currently has currenly no internal support for security. The only support we currently
	have is external and based on the use of a private network for protecting the access to front-
	and back-end of the FTS.

  \item The protocol used by the FTS is based on standard HTTP POSTs. This may prove to be inadequate in the face
	of bursts of many ``fast requests'' (\mF). A sliding window mechanism is currently being considered
	as a way to buffer those bursts and then emptying the circular buffer at the end of each burst.
\end{itemize}

Finally, we remark how the FTS and its approach may be used as a foundation for the definition of a reactive layer
to facilitate the expression and the management of error recovery in the MYRRHA accelerator.
%}}}

\printbibliography

\end{document}

% End of file fts.tex
