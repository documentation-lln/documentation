\begin{thebibliography}{10}

\bibitem{Gojko11}
{\sc Adzic, G.}
\newblock {\em Specification by Example: How Successful Teams Deliver the Right
  Software}.
\newblock Manning, 2011.

\bibitem{MINERVATDR}
{\sc {Belmans,~Jorik,~et~al.}}
\newblock {\em {MINERVA 100-MeV Accelerator Technical Design Report}}.
\newblock 2021.
\newblock {SCK~CEN/37038477}.

\bibitem{BOYD200135}
{\sc Boyd, D.~W.}
\newblock {\em Systems Analysis and Modeling}.
\newblock Academic Press, San Diego, 2001.

\bibitem{DF10a}
{\sc De~Florio, V.}
\newblock Cost-effective software reliability through autonomic tuning of
  system resources.
\newblock In {\em Proceedings of the Applied Reliability Symposium, Europe\/}
  (April 2010).

\bibitem{De10}
{\sc De~Florio, V.}
\newblock Software assumptions failure tolerance: Role, strategies, and
  visions.
\newblock In {\em Architecting Dependable Systems VII}, A.~Casimiro,
  R.~de~Lemos, and C.~Gacek, Eds., vol.~6420 of {\em Lecture Notes in Computer
  Science}. Springer Berlin / Heidelberg, 2010, pp.~249--272.
\newblock 10.1007/978-3-642-17245-8\_11.

\bibitem{De11IMEC}
{\sc De~Florio, V.}
\newblock Imec academy / sset seminar cost-effective software reliability
  through autonomic tuning of system resources, May 2011.
\newblock Available online through
  \url{http://mediasite.imec.be/mediasite/SilverlightPlayer/Default.aspx?peid=a66bb1768e184e86b5965b13ad24b7dd}.

\bibitem{DF14a}
{\sc {De~Florio}, V.}
\newblock Antifragility = elasticity + resilience + machine learning. {Models}
  and algorithms for open system fidelity.
\newblock {\em Procedia Computer Science 32\/} (2014), 834--841.
\newblock 1st ANTIFRAGILE workshop (ANTIFRAGILE-2015), the 5th International
  Conference on Ambient Systems, Networks and Technologies (ANT-2014).

\bibitem{DBLP:journals/corr/FlorioP15}
{\sc {De Florio}, V., and Primiero, G.}
\newblock A framework for trustworthiness assessment based on fidelity in cyber
  and physical domains.
\newblock {\em Procedia Computer Science 52\/} (2015), 996--1003.
\newblock Proc. of the 2nd Workshop on Computational Antifragility and
  Antifragile Engineering (ANTIFRAGILE'15), in the framework of the 6th
  International Conference on Ambient Systems, Networks and Technologies
  (ANT-2015).

\bibitem{Tolga10}
{\sc K{\"o}nik, T., Ali, K., Shapiro, D., Li, N., and Stracuzzi, D.}
\newblock Improving structural knowledge transfer with parametric adaptation.
\newblock In {\em Proc. of the 23th Int.l Florida Artificial Intelligence
  Research Society Conference (FLAIRS 2010)\/} (Daytona Beach, FL, May 2010).

\bibitem{Martraire}
{\sc Martraire, C.}
\newblock {\em Living Documentation: Continuous Knowledge Sharing by Design}.
\newblock Addison-Wesley Professional, 2019.

\bibitem{P-IPAC15-Uriot}
{\sc Uriot, D., and Pichoff, N.}
\newblock {Status of TraceWin code}.
\newblock In {\em {Proc. IPAC'15, Richmond, USA}\/} (May 2015).

\bibitem{diffutils}
{\sc {Wall,~Larry,~et~al.}}
\newblock {GNU Diffutils}, 2021.
\newblock [Online; accessed 15-September-2021].

\bibitem{enwiki:1044933145}
{\sc {Wikipedia contributors}}.
\newblock Hypertext transfer protocol --- {Wikipedia}{,} the free encyclopedia.
\newblock
  \url{https://en.wikipedia.org/w/index.php?title=Hypertext_Transfer_Protocol\#Request_methods},
  2021.
\newblock [Online; accessed 22-September-2021].

\bibitem{enwiki:1045660779}
{\sc {Wikipedia contributors}}.
\newblock Representational state transfer --- {Wikipedia}{,} the free
  encyclopedia.
\newblock
  \url{https://en.wikipedia.org/w/index.php?title=Representational_state_transfer&oldid=1045660779},
  2021.
\newblock [Online; accessed 22-September-2021].

\bibitem{enwiki:1021916913}
{\sc {Wikipedia contributors}}.
\newblock Technical documentation --- {Wikipedia}{,} the free encyclopedia,
  2021.
\newblock [Online; accessed 15-September-2021].

\end{thebibliography}
