# KxKp

This folder contains the LaTeX files required to create the two articles:

- Knowledge extractors and knowledge consumers -- A structured approach towards maintainable documentation

- A Fault Tracking Service for the MINERVA Accelerator

![](epicspost.png)
*The epicspost command is shown and demonstrated.*

![](epicspost2.png)
*Tail of the request's output of the command shown in the previous picture.*

