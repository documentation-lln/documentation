## Fr Dez 13 11:00:04 CET 2019
A first, purely "syntactic" first pass of the TDR has been completed. I am now going systematically through the various chapters
of Part B, checking the content together with the various domain experts.
In this same period, thanks to Marco's assistance, I moved from Mercurial to Git and started the "documentation" Git project.
More information can be gathered by reading the top-level [README](https://git.myrrha.lan/vdflorio/documentation/blob/master/README.md) file.

