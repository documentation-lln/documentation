# A few remarks on editing text for LaTeX

I would kindly ask all contributors to adhere to the following guidalines -- thank you very much!

- In your editors, please set your encoding to standard ASCII. Do not use UNICODE or other beyond-7-bit encodings please,
  otherwise I will have problems when compiling your branches. If you have to input, say, an accented letter, please use
  the LaTeX code for that character. So for instance write "é" as `\'e`, and "à" as `\```.
  For more information, please have a look at the following stackexchange article: [How to type special/accented letters in LaTeX?](https://tex.stackexchange.com/questions/8857/how-to-type-special-accented-letters-in-latex\
- Please do not use characters such as `@` or `$`, as they are interpreted as special characters by LaTeX.
  If you need those characters, preceed them with a backslash, as in `\@` or `\$`.
- Other special characters in LaTeX are the curly brackets. It is very important that they are evenly matched, otherwise
  the LaTeX compiler is going to complain :sweat_smile:
- If you need to typeset values with units, e.g. `300 K`, then use the siunitx package and write e.g. `\SI{300}{\kelvin}`
  More info [here](http://tug.ctan.org/macros/latex/exptl/siunitx/siunitx.pdf).
  Many thanks to Mario for this really nice suggestion!
- If you need to deal with bibliographic references, please do not forget to have a look [here](https://git.myrrha.lan/vdflorio/documentation/tree/master/bibliography). And never forget, I'm here to help
  and provide explanations!
