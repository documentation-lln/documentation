# Knowledge extractors and knowledge consumers

# A structured approach towards maintainable documentation
vdflorio, 2021-06-16
v.0.1

# Documents…

* …Theypersistknowledge
* …butthey _hide_ knowledgeas well\!
  * Knowledge isrepresentedin ascatteredform – as datawithlimitedorganization
* Theyoftenrepresentastaticsnapshot ofcertainreferencedynamicprocesses\, taken atthetime ofwriting
  * Incertaincases\,thedynamicprocessesare longlasting–thustherelationshipbetweendocumentandprocessesmustbekeptuptodate: we talk of “ _living_  _documents_ ”

# Key problems

* Howtoguarantee\, or atleastfacilitatemaintainability?
  * HowtomakesurethattheTDRandotherdocumentsreflectthecurrentprogress? \[ _change_  _tolerance_ \]
  * HowtodetectthattheTDRandotherdocuments’ contents is _drifting_  _away_ fromthecurrentprogress? \[ _change_  _detection_ \]
* Howtomanifest adocument’sknowledgebest matchingtherequirementsofthecurrentprocessstage?
  * Howtoputtotheforegroundtheinformationthatbest matchesthecurrentneeds?

# Our case

* In MINERVA\,severalkeylivingdocumentsexist
  * Referencedocuments\(e\.g\.\,theTDR\)
  * Referencespecifications\(e\.g\.\,faulttracking servicespecs\)
  * Referencemodels\(e\.g\.\,thelinacTraceWinreferencecode\)
* Referencedocumentsdependonreferencespecsandmodels
  * Seeproblem1\.
  * The approachsofar:specsandmodelschangedocumentsmustbemanuallyadapted
    * Error\-prone\,tediousactivity

# Knowledge extractors and knowledge consumers

An approachtoimprovethestatus quo

Kwextractors: codesthatparsethereferencespecificationsandmodelsandcreateaconciserepresentation\(Python datastructures\, JSON\,etc\)

Kwconsumer: codesthatusetheextractedkwtofacilitateadaptation\(detectionof “drifts”\,adaptationofmacros\(seeexplanationlater on\)\)

# MINERVA accelerator kw extractors and consumers

* A set of toolsthat
  * extractknowledgefromreferencespecsandmodelsoftheMINERVA acceleratorand
  * produceartifactsthatfacilitatemaintenance ofreferencedocuments\(suchastheMINERVA Accelerator TDR\)

# 1. Tracex

* Within MINERVA\, the most up\-to\-date system configuration for the accelerator is maintained via a reference specification: aTraceWinfile
  * Configurations\, parameters\, and related information about Ion Source\, LEBT\, MEBT\-1\, RFQ\, the CH cavities\, MEBT\-2\, both sides of the MEBT\-3\, and theCryomodules\, are detailed in that file both asTraceWinstatements and asTraceWincommented statements\.
* Tracex: a family of knowledge extractors all taking their input fromTraceWinfiles

<img src="img/Knowledge extractors0.png" width=500px />

# How it looks like

* Severalsectionsdescribetheaccelerator’scomponents\(here\, MEBT\-1\)
* A model\,thoughalsothemajor acceleratorreferenceofourproject
  * Piggybackinginformation

# Observation

* Information isrepresentedin aformal\(or semi\-formal\) way\,usingtheTraceWinsyntax
* TraceWinmethodsarepositionalonly
  * Competenceisrequiredtointerpretthemeaningofpassedparameters \(mostlynumeric\)
  * Error\-prone

# Tracex

<img src="img/Knowledge extractors1.png" width=500px />

# Flow.svg

TraceWinstatements are

1\)Partitionedintoblocks

correspondingtotheLinacsub\-systems

2\)Printedin adescriptiveform

Futurework:editableentries

# LaTeX2macros

Macros are used extensively in the TDR\.

“Self\-checking” macros\. Example: \`\\ncCH\{16\}\` issues an error\, because its definition specifies that at most 15 CH cavities are present\.

Maximum and minimum values are inherited from theTraceWinfile\!

<span style="color:#FFFF00">When values are changed in the</span>  <span style="color:#FFFF00">TraceWin</span>  <span style="color:#FFFF00">configuration file\, macros are automatically adjusted\.</span>

<span style="color:#FFFF00">Change detection / consistency failures in the TDR\.</span>

# Consistency failure detection

* As an example\, when I changed the RF chapter as per Franck's instructions\, I specified thatcryomoduleswere not 30 but 23\.
* WhenIupdatedthemacrosfromtheTraceWin\,the\`\\ncCRYOMODULE\` macro was adapted\.
  * Max = 23 instead of 30
* Several warnings were issued in three other chapters\. Wherever I had \`\\ncCRYOMODULE\{ _x_ \}\` with _x_ >23\, generated a warning
* In other words\, inconsistencies became apparent\!

# Trace_svg

AhierarchicalhypergraphoftheLinaciscreatedfromtheTraceWin

<img src="img/Knowledge extractors2.png" width=500px />

Top level view

Every block is a hyperlink

“LBS” e\.g\. pointsto

graphlsb\.svg

AhierarchicalhypergraphoftheLinaciscreatedfromtheTraceWin

<img src="img/Knowledge extractors3.png" width=500px />

Mid\-level view

Again\,everyblock is a

hyperlink

“LBS19” e\.g\. pointsto

lbs19\.html

AhierarchicalhypergraphoftheLinaciscreatedfromtheTraceWin

<img src="img/Knowledge extractors4.png" width=500px />

Bottom\-level view

Here\, I haveannotatedTraceWincommandspertainingtoLB\-S19

Butthiscanbethefront\-endtomultipleotherservices:forinstance\,theMINERVAfaulttracking service

Apossibleapproach\,alsobasedonknowledgeextractors\, isdescribednext

# 2. Fault Tracking services

* A fault tracking service \(FTS\) is an important pre\-requisite to the evaluation of important dependability metrics \(esp\. availability and reliability\)
* Aknowledgeextractorcanbeusedtoquicklyexperimentwithvariousdesign options
* Toexemplifythis\, I haveusedareferencespecificationdocumentthathas beenkindlyprovidedtomebyAurélien
* The document is inthiscaseanexcelfilethatdescribes
  * A data model
  * A systemhierarchy

# Fault_Tracking.xlsx

<img src="img/Knowledge extractors5.png" width=500px />

<img src="img/Knowledge extractors6.png" width=500px />

# Idea: quick prototypingvia knowledge extractors and consumers

First\,structureanddata model areextractedasdonewiththeTraceWincase

Ahypertextualgraphiscreateddynamicallysoastorepresentthesystemstructure

# Knowledge consumers create front- & back-end

<img src="img/Knowledge extractors7.png" width=500px />

Note:Hypergraphcreatedonthefly

* Every node pointstoanHTML form
* The HTML formreceives\(GETmethod\)thenodeitoriginatedfrom
* Nothingfancyyet– asimpleproofofconcepts
  * No CSSyet\,etc

<img src="img/Knowledge extractors8.png" width=500px />

The form is pre\-filled

Knowledgeconsumerscreatefront\- __&__  __back\-end__

Aback\-endiscreatedontheflytoreceivethedatasubmittedbytheuser

<img src="img/Knowledge extractors9.png" width=500px />

<img src="img/Knowledge extractors10.png" width=500px />

Fault

Tracking

DBMS

# Bill-of-materials: Similar approach

Fromanexceltoahypergraphpointingtodesigndiagrams:

<img src="img/Knowledge extractors11.png" width=500px />

# Preliminary conclusions

* Thedependencebetweenliving documentandreferencespecs&modelsis made asmuchaspossibleexplicit
  * E\.g\.\,macrosareupdatedtoreflectthestatus quo
* Knowledge isextracted\,organized\,andconvenientlyvisualized
* Theuseof hyperlinkscreatesadynamicreference:pointedinformationcanthenvary
  * Foregroundinformation changesitmayreflectthedesign stage we are in
    * Re\-commissioning?  Hyperlinks settorecommissioning\-specificinformation
* Muchstillcanbedone\!
  * Semanticrelationshipsbetweendocumentsanddata\,etc

