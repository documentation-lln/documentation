@startuml
skinparam componentStyle rectangle
skinparam noteTextAlignment center
skinparam DefaultTextAlignment center
skinparam backgroundColor #EEEBDC
' skinparam handwritten true


title Tracex knowledge extractors and consumers

database "TraceWin" { 
    [ TraceWin.dat ] as tw #Yellow
} 

note bottom of tw
        **1.**
Input file.
end note

package "Tracex" {
    [Tracex.py] as tx
}

note top of tx
        **2.**
Knowledge extractor.
Produces the Python
data structure used
by all other packages.
end note

database "Py-modules" {
    [modules.py] as md #Green
}

note bottom of md
        **3.**
Python data structure
packaging the data in
the input file.
end note

tx ..> tw : reads
tx --> md : writes

package "Flow" {
    [flow.py] as fl
}

note top of fl
        **4.**
Knowledge consumer.
Reads the Python
data structure and
creates a diagram
with a structured
description of the
TraceWin contents.
end note

database "SVG\nTraceView\ndiagram" {
    [flow.svg] as pl
}

note bottom of pl
        **5.**
Output of flow.py.
end note


fl ..> md : reads
fl --> pl : writes

package "LaTeXmacros" {
    [LaTeXmacros.py] as ltx
}

note top of ltx
        **6.**
Knowledge consumer.
Reads the Python
data structure and
creates LaTeX macros
used in "living"
documents.
end note

database "macros.tex" {
    [macros.tex] as mac
}

note bottom of mac
        **7.**
Output of
LaTeXmacros.py.
end note


ltx ..> md  : reads
ltx --> mac : writes

database "TDR sources" {
    [MINERVA-TDR.tex] as tdr
}

note top of tdr
        **8.**
Living document.
A "user" of file
macros.tex produced
by LaTeXmacros.py.
end note

package "LaTeX" {
    [LaTeX produ-\nction packages] as pdflatex
}

note top of pdflatex
        **9.**
External tool.
Compiles the living
document into a PDF.
end note

database "TDR" {
    [MINERVA-TDR.pdf] as tdrpdf
}

note bottom of tdrpdf
        **10.**
Living document
compiled into
a PDF file.
end note

pdflatex ..> mac    : reads
tdr      ..> mac    : includes
pdflatex --> tdrpdf : writes

package "Trace SVG" {
    [trace_svg.py] as trace_svg
}

note top of trace_svg
        **11.**
Knowledge consumer.
Reads the Python data
structure and produces
a hierarchical repre-
sentation of the linac,
with leaves annotated
with TraceWin statements.
end note

database "SVG\nLinac\ndiagrams" {
    [linac.svg] as linac
    [sub-systems\ndiagrams] as sub_linac
    [html tran-\nscriptions] as tw_html
}

'note bottom of database
'        12.
'Hierarchical represention of the Linac,
'annotated with TraceWin statements.
'end note

note as belownote
        **12.**
Hierarchical represention of the Linac,
annotated with TraceWin statements.
end note

belownote .up. linac
belownote .up. sub_linac
belownote .up. tw_html

trace_svg ..> md        : reads
trace_svg --> linac     : writes
trace_svg --> sub_linac : writes
trace_svg --> tw_html   : writes

url of linac is [[http://tdr.myrrha.lan/vfolders/tracex/linac.svg]]
url of pl    is [[http://tdr.myrrha.lan/vfolders/tracex/flow.svg]]

@enduml
