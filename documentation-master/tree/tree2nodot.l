%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define GitRepo	"/home/vdflorio/Documents/Git/documentation"

static int input();

int gitLsFiles(char filename[]) {
	char systring[512];
	sprintf(systring, "git -C %s ls-files|fgrep -q %s", GitRepo, filename);
	return system(systring);
}

int till_eol() {
	int c;
	while ( (c = input()) != EOF ) {
	   if (c == '\n') break;
	   putchar(c);
	}
	putchar(c);
	return c;
}
int ignore_till_eol() {
	int c;
	while ( (c = input()) != EOF ) {
	   if (c == '\n') break;
	}
	return c;
}
char *str_till_eol() {
	static char buffer[512];
	int c;
	char *p = buffer;
	while ( (c = input()) != EOF ) {
	   if (c == '\n') break;
	   *p++ = c;
	}
	*p++ = c;
	*p = '\0';
	return buffer;
}
/*
^│[ ][ ][ ]├──[ ]  {
		printf("  |_ ");
		till_eol();
	}
^{elem1}{elem2}{7,}{last1} {
		fprintf(stderr, "beyond 7 levels, elements are ignored\n");
		ignore_till_eol();
	}

*/
%}

last1	"&#9500;&#9472;&#9472; "
last2	"&#9492;&#9472;&#9472; "
elem1	"&#9474;&#160;&#160; "

sp3	([ ][ ][ ])
sp4	([ ][ ][ ][ ])
elem2	({elem1}|{sp4})
lastall	({last1}|{last2})

%%

^{elem2}*{lastall} {
		char *p = yytext;
		int level = 1;
		char *filename;
		int retval;
		while (*p == '&' || *p == ' ') {
			//if (*p == ';') p++;
			if (*p == '&') {
				while (*p != ';' && *p != ' ') p++;
				p++;
				level++;
				continue;
				}
			if (*p == '\0') break;
			if (*p == ' ') {
				if (p[1] == '\0') break;
				while (*p == ' ') p++;
				level++; continue;
				}
			p++;
		}
		filename = str_till_eol();
		retval = gitLsFiles(filename);
		if (retval == 0) {
			level /= 4;
			while (--level) printf("  ");
			printf("|_ %s", filename);
		}
	}

^[0-9]*[ ]directories[,][ ][0-9]*[ ]files.*	;

%%
// ^[0-9]*.*$	;

int main() {
	printf("[plantuml, format=svg, opts=\"inline\", target=\"tree\"]\n");
	printf("----\n");
	//printf("!include nodot-asciidoctor-style.iuml\n");
	printf("!include /home/vdflorio/Documents/Git/documentation/include/nodot-asciidoctor-style.iuml\n");
	printf("legend\n");
	while (yylex() != 0) ;
	printf("end legend\n");
	printf("----\n");
}

