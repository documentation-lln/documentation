#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
from sys import argv
from pathlib import Path
from bs4 import BeautifulStoneSoup
import html

def HTMLEntitiesToUnicode(text):
    """Converts HTML entities to unicode.  For example '&amp;' becomes '&'."""
    text = unicode(BeautifulStoneSoup(text, convertEntities=BeautifulStoneSoup.ALL_ENTITIES))
    text = BeautifulStoneSoup(text, convertEntities=BeautifulStoneSoup.ALL_ENTITIES)
    return text

def unicodeToHTMLEntities(text):
    """Converts unicode to HTML entities.  For example '&' becomes '&amp;'."""
    text = html.escape(text).encode('ascii', 'xmlcharrefreplace')
    return text

def transcode(filename):
    try:
        f = open(filename, 'r', encoding='utf-8')
        txt = f.read()
        f.close()
    except:
        sys.stderr.write('could not open input file')
        return None

    filename2 = 'meta-' + filename
    try:
        g = open(filename2, 'w')
    except:
        sys.stderr.write('could not open output file')
        return None

    out = unicodeToHTMLEntities(txt)
    g.write(out.decode())
    g.close()
    sys.exit()

    out = ''
    length = len(txt)
    for i in range(length):
        if txt[i] == '│':
            out += '::BIGPIPE'
        elif txt[i] == '├':
            out += 'NONASCII_DERIVES'
        elif txt[i] == '─':
            out += 'NONASCII_ENDASH'
        elif txt[i] == '└':
            out += 'NONASCII_BOTTOMLEFTCORNER'
        else:
            out += txt[i]

    g.write(out)
    g.close()


length = len(argv)
for i in range(1,length):
    transcode(argv[i])


