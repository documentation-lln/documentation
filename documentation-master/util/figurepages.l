%{
/***************************************************************************************************
 figurepages reads a .lof file from stdin and generates a statically allocated C array that
 associates figure numbers to page numbers. If l is the latex source that produced the .lof file,
 then Figure i appears in page fig2page[i] of l. The array is output on stdout.

 By vdflorio, October 2020
 ***************************************************************************************************/
#define TRUE   (1)
#define FALSE  (0)
int col = 0;
int fig;
unsigned char clang = FALSE, pythonlang = FALSE;
%}

%%
\{[0-9]*\}\{figure.caption.[0-9]*\}\%$	{
		char *p = yytext;
		col++;
		fig++;
		while (*++p != '}') ;
		*p = '\0';
		if (clang) {
			// printf("%3s - 4,", yytext+1); // "-4" because we will not actually have a \listoffigures in the TDR
			printf("%3s,", yytext+1); // "-4" because we will not actually have a \listoffigures in the TDR
			if (col == 10) { putchar('\n'); col = 0; } else putchar(' ');
		} else { // pythonlang
			printf("%d: %s, ", fig, yytext+1);
		}
	}
.	;
\n	;
%%

int main(int argc, char *argv[]) {
	int i;

	for (i=1; i<argc; i++)
	if (argv[i][0] == '-')
	switch (argv[i][1]) {
		case 'c':
			clang = TRUE;
			break;
		case 'p':
			pythonlang = TRUE;
			break;
		default:
			fprintf(stderr, "%s: invalid option. Valid option are '-p' and '-c'\n", *argv);
			exit (-1);
		}

	if (clang && pythonlang) {
		fprintf(stderr, "%s: Option '-p' and '-c' are mutually exclusive\n", *argv);
		exit (-2);
	}

	// Default: clang
	if (clang == FALSE && pythonlang == FALSE) clang = TRUE;


	if (clang) {
		printf("static int fig2page[] = { 0, \n");
		while ( yylex() ) ;
		printf("\t};\n");
	} else { // pythonlang
		printf("fig2page = { ");
		while ( yylex() ) ;
		printf(" }\n");
	}
}
