%{
// {{{ C Prologue
/******************************************************************************************************
 * captions.l
 *
 * A tool to create a list of figures plus the paths to the images that are included in those figures
 *
 * v. 0.6 (20201015T0849) (with hyperlink to image in the Git repository)
 * v. 0.5 (20201014T1651) (with page numbers)
 * v. 0.4 (20201014T0900) (LaTeX mode)
 * v. 0.3 (20201013T1602) (Adding error management, improving output)
 * v. 0.2 (20201013T1335) (Consolidated version, tested and checked multiple times)
 * v. 0.1 (20201013T1121) (Preliminary version, yet to be thoroughly tested)
 * by vdflorio (vincenzo.deflorio@sckcen.be)
 *
 * In order to compile this code, do as follows:
 *  flex captions.l ; gcc -o captions lex.yy.c -lfl
 *
 * In order to run this code, do as follows:
 *
 * cat $LATEXFILES | captions
 *
 * where LATEXFILES is the list of LaTeX files you want to create a list of figures from.
 *
 ******************************************************************************************************/

const char version[] = "v. 0.6 (20201015T0849)";

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pages.h"	// created by figurepages.

#define TRUE	1
#define FALSE	0
#define MAXNARG 2048			// max size of a caption

#define ERR_NONE        (0)
#define ERR_CANTACCESS  (-1)
#define ERR_WRONGARG    (-2)
#define ERR_RANGE       (-3)
#define ERR_MISSING_ARG (-4)

#define S_ERR_NONE		"Processing concluded normally"
#define S_ERR_CANTACCESS 	"Cannot access a file"
#define S_ERR_WRONGARG		"Undefined command line option"
#define S_ERR_RANGE		"Illegal value for a parameter"
#define S_MISSING_ARG		"Missing value for a command line option"

const char *ErrMsgs[] = { S_ERR_NONE, S_ERR_CANTACCESS,
			  S_ERR_WRONGARG, S_ERR_RANGE,
			  S_MISSING_ARG,
			};

typedef	unsigned char boolean_t;	// like bool_t
boolean_t  in_includegraphics = FALSE;	// are we inside a \includegraphics?
boolean_t  in_caption = FALSE;		// are we inside a \caption?
boolean_t  in_figure = FALSE;		// are we inside a {figure}?
boolean_t latex_mode = FALSE;		// do we output LaTeX code or not?
int  figure_nr = 0;			// the number of the current figure
int  subfig = 0;			// number of subfigures present
char Arg[MAXNARG];			// argument of \caption or \includegraphics
int  nArg = 0;				// strlen Arg
void getArg(), cleanArg();		// functions to get next Arg and to clear it

FILE *out;				// output stream
int  scat(FILE *, char []);		// :r ! command
int  fcat(FILE *, char []);		// :r file
float width = 0.2;			// thumbnail width. Default = 0.2. Range: ]0, 1]

/* ANSI terminals color codes */
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

// }}}
%}

any		(.|\n)
anybutsquare	([^\]]|\n)

%%

%.*$			;

\\begin\{subfigure\}	{ subfig++; 
			}

\\begin\{(sideways)?figure\}	{ figure_nr++; in_figure = TRUE;
			  if (latex_mode) fprintf(out, "\n\\begin{figure}[H]\n");
			}

\\end\{(sideways)?figure\}		{
			  subfig = 0;
			  in_includegraphics = FALSE;
			  in_caption = FALSE;
			  in_figure = FALSE;
			  if (latex_mode) fprintf(out, "\n\\end{figure}\n");
			}

\\includegraphics\[{anybutsquare}*\]\{	{
			if (in_figure) in_includegraphics = TRUE;
			}

\\includegraphics\{	{
			if (in_figure) in_includegraphics = TRUE;
			}

\\caption\[{anybutsquare}*\]\{	{
			if (in_figure) in_caption = TRUE;
			}

\\caption\{		{
			if (in_figure) in_caption = TRUE;
			}

{any}	{
		if (in_caption) {
			getArg();
			if (Arg[nArg-1] == '}') Arg[nArg-1] = '\0';

			if (nArg > 1) {
				if (latex_mode) fputc('\n', out);

				if (subfig)
					fprintf(out, "Fig. %d.%d: Caption: %s\n", figure_nr, subfig, Arg);
				else
					fprintf(out, "Fig. %d: Caption: %s\n", figure_nr, Arg);
				fprintf(out, "(on page %d)\n", fig2page[figure_nr]);
				cleanArg();
			}
			in_caption = FALSE;
		} else
		if (in_includegraphics) {
			getArg();
			if (Arg[nArg-1] == '}') Arg[nArg-1] = '\0';

			if (latex_mode) {
				//fprintf(out, "\n\\includegraphics[width=%f\\textwidth]{%s}\n", width, Arg);
				fprintf(out, "\n\\href{https://git.myrrha.lan/vdflorio/documentation/-/tree/master/TDR/%s}{\\includegraphics[width=%f\\textwidth]{%s}}\n", Arg, width, Arg);
				if (subfig)
					fprintf(out, "Fig. %d.%d: Path: \\verb\"%s\"\n", figure_nr, subfig, Arg);
				else
					fprintf(out, "Fig. %d: Path: \\verb\"%s\"\n", figure_nr, Arg);
			} else {
				if (subfig)
					fprintf(out, "Fig. %d.%d: Path: %s\n", figure_nr, subfig, Arg);
				else
					fprintf(out, "Fig. %d: Path: %s\n", figure_nr, Arg);
			}
			cleanArg();
			in_includegraphics = FALSE;
		}
		else {
			// just ignore the character
		}
	}

%%

// {{{ Ancillary functions
int scat(FILE *g, char cmd[]) {
	int c;
	FILE *f = popen(cmd, "r");
	if (f == NULL) return ERR_CANTACCESS;
	while ( (c=fgetc(f)) != EOF ) fputc(c, g);
	fclose(f);
	return ERR_NONE;
}

int fcat(FILE *g, char fname[]) {
	int c;
	FILE *f = fopen(fname, "r");
	if (f == NULL) return ERR_CANTACCESS;
	while ( (c=fgetc(f)) != EOF ) fputc(c, g);
	fclose(f);
	return ERR_NONE;
}

int latex_begin(FILE *f) {
	return fcat(f, "standard_tex_prologue.tex");
}

int latex_end(FILE *f) {
	return fcat(f, "standard_tex_closings.tex");
}

// {{{ getArg and cleanArg
void getArg() {
	char c, d;
	char i;
	int curly=1;
	Arg[0] = *yytext;
	nArg = 1;
	if (*yytext == '}') curly--;
	if (*yytext == '{') curly++;

	while (curly) {
		for (; (c=input()) != '}'; nArg++) {
			if (c == '{') curly++;
			if (c==EOF || nArg == MAXNARG-1) { Arg[nArg] = '\0'; return; }
			if (c == '%') { while ((d=input()) != '\n') ; c = '\n'; }
			Arg[nArg] = c;
		}
		curly--;
		Arg[nArg++] = '}';
	}
	Arg[nArg] = '\0';
}
void cleanArg() {
	nArg = 0;
}
// }}}

// {{{ abnormal() and args()
void abnormal(int code) {
	fprintf(stderr, "%sSomething went wrong and I cannot proceed, sorry (code = %d).%s\n", KRED, code, KNRM);
	fprintf(stderr, "%sDescription: %s.%s\n", KYEL, ErrMsgs[-code], KNRM);
}

void args() {
	fprintf(stderr, "Legal arguments are:\n");
	fprintf(stderr, "%s -?         %s", KYEL, KNRM); fprintf(stderr, "to print this message.\n");
	fprintf(stderr, "%s -o out     %s", KYEL, KNRM); fprintf(stderr, "to select \"out\" as output file.\n");
	fprintf(stderr, "%s -w num     %s", KYEL, KNRM); fprintf(stderr, "to select \"num\" as thumbnail width (num in ]0,1]).\n");
	fprintf(stderr, "%s -l         %s", KYEL, KNRM); fprintf(stderr, "to choose LaTeX as the output format.\n");
	fprintf(stderr, "%s -v         %s", KYEL, KNRM); fprintf(stderr, "to select verbose mode.\n");
}
// }}}

// }}}

// {{{ Main
int main(int argc, char *argv[]) {
	int i;
	FILE *g = stdout; char *gName = NULL;
	boolean_t verbose = FALSE;
	int latex_begin(FILE *);
	int latex_end(FILE *);

	out = stdout;

	for (i=1; i<argc; i++) if (argv[i][0] == '-') switch (argv[i][1]) {
	case '?':
		args(); return (ERR_NONE);
	case 'w':
		if (i < argc - 1) {
			float f;
			i++;
			sscanf(argv[i], "%f", &f);
			if (f<=0 || f>1) {
				fprintf(stderr, "%s: Illegal width. Legal values: ]0, 1]. Aborting...\n", argv[0]);
				abnormal (ERR_RANGE);
				exit (ERR_RANGE);
			}
			width = f;
		} else {
			abnormal (ERR_MISSING_ARG); args(); exit (ERR_MISSING_ARG);
		}
		break;
	case 'o':
		if (i < argc - 1) {
			i++;
			gName = argv[i];
			g = fopen(gName, "w");
			if (g == NULL) {
				fprintf(stderr, "%s: cannot access output file %s. Aborting...\n", argv[0], gName);
				abnormal (ERR_CANTACCESS);
				exit (ERR_CANTACCESS);
			}
			out = g;
		}
		else {
			abnormal (ERR_MISSING_ARG); args(); exit (ERR_MISSING_ARG);
		}
		break;
	case 'l':
		latex_mode = TRUE;
		break;
	case 'v':
		verbose = TRUE;
		break;
	default:
		abnormal (ERR_WRONGARG); args(); exit (ERR_WRONGARG);
	}
	else {
		abnormal (ERR_WRONGARG); args(); exit (ERR_WRONGARG);
	}

	if (verbose) {
		fprintf(stderr, "Starting processing of stdandard input stream.\n");
	}
	if (latex_mode) {
		latex_begin(out);
		fprintf(out, "\\begin{center}\\large List of figures, with thumbnails, captions, and paths to the constituent images\\\\\n");
		fprintf(out, "Created by %s %s on ", argv[0], version);
		scat(out, "date");
		fprintf(out, "\\end{center}\n\n");
	}

	while (yylex()) ;

	if (latex_mode)
		latex_end(out);

	if (verbose) {
		fprintf(stderr, "Finished processing of stdandard input stream.\n");
		if (g == NULL) {
			fprintf(stderr, "Output has been sent onto the standard output stream.\n");
		} else {
			fprintf(stderr, "Output is in file \"%s%s%s\".\n", KGRN, gName, KNRM);
			fclose(g);
		}
	}

	return (ERR_NONE);
}
// }}}
/* EOF captions.l */
