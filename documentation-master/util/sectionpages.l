%{
/***************************************************************************************************
 sectionpages reads a .toc file from stdin and generates a statically allocated C array that
 associates section numbers to page numbers. If l is the latex source that produced the .toc file,
 then Section i begins at page sect2page[i] of l. The array is output on stdout.

 By vdflorio
 Version 0.1: 20201027
 Version 0.2: 20201029T1419
 ***************************************************************************************************/

 #include <stdio.h>
 #include <stdlib.h>
 #include <string.h>
 #include <ctype.h>

#define TRUE   (1)
#define FALSE  (0)
#define BUFSIZE 4096

int  col = 0;
int  section = 1;
unsigned char Inside = FALSE;
int Appendix = -1, Bib = -1;
unsigned char clang = FALSE, pythonlang = FALSE;

// We look for patterns structured as follows:
// \contentsline {section}{\numberline {\leavevmode {\color {blue}1}}TitleOfTheSection}{

// \contentsline {section}{\numberline {A}Parameters List}{215}{section.Alph0.1}%
// \contentsline {part}{Bibliography}{235}{section*.301}%

char myytext[BUFSIZE];
int  myysp = 0;

%}

NEWLINE \n
ANY	.

PREFIX ^(\\contentsline[ ]\{section\}\{\\numberline[ ]\{\\leavevmode[ ]\{\\color[ ]\{blue\}[0-9]*\}\}[^}]*\}\{)
APPENDIX	^(\\contentsline[ ]\{section\}\{\\numberline[ ]\{A\}[^}]*\}\{)
BIB	^(\\contentsline[ ]\{part\}\{Bibliography\}\{)

%%

{APPENDIX} {
		Appendix = 0;
		Inside = TRUE;
		myysp = 0;
	}

{BIB} {
		Bib = 0;
		Inside = TRUE;
		myysp = 0;
	}

{PREFIX}	{
	Inside = TRUE;
	myysp = 0;
	}

{ANY}	{
		if (Inside)
			/*
			 If we are Inside, then what follows has the following structure:
			
			 PageWeAreLookingFor}{section.SomeNumber.SomeNumber}%
			 e.g.
			 21}{section.0.1}%
			*/
			if (isdigit(*yytext)) {
				/* At first, we expect digits, which are pushed onto myytext */
				myytext[myysp++] = *yytext;
			} else {
				/*
				  Whatever follows is garbage; actually, garbage structured as follows:

					}{section.SomeNumber.SomeNumber}%
					e.g.
					}{section.0.1}%
	
				  We simply don't care: "close" the stack, declare that we are no more Inside, and leave
				*/
				myytext[myysp] = '\0';
				Inside = FALSE;
			}
		/* if we are not Inside, we just ignore the character */
	}

{NEWLINE}	{
			/* When we encounter a \n, we check whether myytext holds something: */
			if (myysp) {
				if (Appendix == 0) { /* is it the appendix? */
					Appendix = atoi(myytext);
					myysp = 0;
				} else if (Bib == 0) { /* is it the bibliography? */
					Bib = atoi(myytext);
					myysp = 0;
				} else { /* otherwise, it is a section, and we issue a new entry in sect2pages... */
					if (clang) {
						if (section > 0)
							printf("\t/* Sect. %3d begins at page */%8s,\n", section, myytext);
						else
							printf("%4s,", myytext);
					} else {
						printf("%d: %s, ", section, myytext);
					}

					section++;
					myysp = 0; /* ...and "clear" the buffer */
				}
			} /* otherwise, we ignore the \n */
		}

%%

int main(int argc, char *argv[]) {
	int i;

	for (i=1; i<argc; i++)
	if (argv[i][0] == '-')
	switch (argv[i][1]) {
		case 'c':
			clang = TRUE;
			break;
		case 'p':
			pythonlang = TRUE;
			break;
		default:
			fprintf(stderr, "%s: invalid option. Valid option are '-p' and '-c'\n", *argv);
			exit (-1);
		}

	if (clang && pythonlang) {
		fprintf(stderr, "%s: Option '-p' and '-c' are mutually exclusive\n", *argv);
		exit (-2);
	}

	// Default: clang
	if (clang == FALSE && pythonlang == FALSE) clang = TRUE;


	if (clang) {
		printf("static int sect2page[] = { 0, \n");

		while ( yylex() ) ;

		printf("\t/* Appendices then start on page */%8d\n\t};\n", Appendix);
		printf("\n/* Pages where the Appendices and Bibliography begin */\n");
		printf("\tint Appendix = %4d;\n", Appendix);
		printf("\tint Bib = %4d;\n", Bib);
		printf("\n/* Number of sections (cardinality of sect2page[]) */\n");
		printf("\tint Sections = %4d;\n", section-1);
	} else { // pythonlang
		printf("sect2page = { ");

		while ( yylex() ) ;

		printf("'Appendix' : %d, 'Bibliography' : %d, 'Number of Sections' : %d }\n", Appendix, Bib, section-1);
	}

	// ^"\\"contentsline[ ][{]section[}][{]"\\"numberline[ ][{]"\\"leavevmode[ ][{]"\\"color[ ][{]blue[}][0-9]*[}][}][A-Za-z ]*[}][{] 
}

/* EoF sectionpages.l */
