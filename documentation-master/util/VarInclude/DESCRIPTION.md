# Conditional include of LaTeX files

Some while ago, Angélique asked me for a LaTeX solution for the conditional inclusion of a LaTeX file.
This can be done via the following steps:

- You set an environment variable with the name of file to be included. For instance you may have

  ```
      $ export MYINCLUDE='myinclude1.tex'
  ```

- Then you place the varinput.sh shell script in your local bin or any other place mentioned in your PATH.
  varinput.sh simply that types out the contents of the files whose names are in one or more environment variables
  mentioned in the command line. As an example,

  ```
      $ ./varinput.sh '$MYINCLUDE'
  ```

  types out the contents of file myinclude1.tex.

- Then in the preamble of the LaTeX code, I define the following command:

  ```
    \newcommand{\varinput}[1]{\immediate\input|"./varinput.sh #1"\unskip}
  ```

That's it.  Now, in your LaTeX source, you can use for instance

```
    \varinput{'$MYINCLUDE'}
```

to include the LaTeX file whose name is in environment variable MYINCLUDE. If no such variable exists, nothing is included.

Oh, I had nearly forgotten -- when compiling your LaTeX source you need to also specify "--shell-escape".
