#!/usr/bin/env bash

# Checks the content of one or more environmental variables as names of files to be included
#
# For instance, $ ./varinclude.sh '$MYLATEXMODULE'
# types out the content of the file in $MYLATEXMODULE
# When called withut arguments, it is equivalent to  $ ./varinclude.sh '$VARINCLUDE'

if [ $# -eq 0 ] ; then
    if [ ! -z $VARINCLUDE ] ; then
        cat $VARINCLUDE
    fi
else
    for var in "$@"
    do
	tmp=$(eval echo "$var")
	if [ ! -z $tmp ] ; then
            cat $tmp
        fi
    done
fi
