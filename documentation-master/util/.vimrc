let @a = "\n" . '\begin{Alert}{}' . "\n" . '\end{Alert}' . "\n"
let @b = '\begin{tcolorbox}' . "\n" . '\textbf{}' . "\n" . '\end{tcolorbox}'
let @c = '7h7s~\cite{f]x'
let @e = "\\begin{equation}" . "\n" . "\\label{eq:commiss:}" . "\n" . "\\end{equation}" . "\n"
let @f = "\\begin{figure}[H]%[tbp]" . "\n" . "\\centering" . "\n" . "\\caption{}" . "\n" . "\\label{fig:commiss:}" . "\n" . "\\end{figure}" . "\n"
let @i = "\\begin{itemize}[noitemsep]" . "\n" . "\\end{itemize}" . "\n"
let @j = 'gwip'
let @j = '!}fmt -w 110' . "\n"
let @n = "\\begin{enumerate}[noitemsep]" . "\n" . "\\item " . "\n" . "\\end{enumerate}" . "\n"
let @r = "6s\\ref"
let @s = "\\begin{figure}[tbp]" . "\n" .  "    \\centering" . "\n" .  "    \\begin{subfigure}[t]{0.5\\textwidth}" . "\n" .  "        \\centering" . "\n" .  "            \\caption{}" . "\n" .  "        \\label{fig:commiss:}" . "\n" .  "    \\end{subfigure}%" . "\n" .  "   ~" . "\n" .  "    \\begin{subfigure}[t]{0.5\\textwidth}" . "\n" .  "        \\centering" . "\n" .  "            \\caption{}" . "\n" .  "        \\label{fig:commiss:}" . "\n" .  "    \\end{subfigure}" . "\n" .  " \\caption{}" . "\n" .  " \\label{fig:commiss:}" . "\n" .  "\\end{figure}" . "\n"
let @t = "\\begin{table}[H]" . "\n" . "\\caption{}" . "\n" . "\\label{tab:commiss:}" . "\n" . "\\end{table}" . "\n"
let @x = 'di{va{p'
let @y = 'di{va{p!}fmt -w 110' . "\n"
let @w = "14s  \\itemkdi{va{p!}fmt -w 110" . "\n"
let @z = 'bcw\PutIndex{}P'
let @z = 'yiwcw\PutIndex{}P'
let @q = '@x@x^i  \item '
let @s = "\n\n%-----------%" . "\n" . "\\begin{frame}{TITLE}" . "\n" . "\\vspace{20pt}" . "\n" . "\\centerline{\\includegraphics[width=\\textwidth]{IMAGE.png}}" . "\n" . "\\end{frame}" . "\n" . "%---------%" . "\n"

set autoread

map <C-X> di{va{p
map F2 di{va{p

" Return to last edit position when opening files (You want this!)
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

" Tell vim to remember certain things when we exit
"  '10  :  marks will be remembered for up to 10 previously edited files
"  "100 :  will save up to 100 lines for each register
"  :20  :  up to 20 lines of command-line history will be remembered
"  %    :  saves and restores the buffer list
"  n... :  where to save the viminfo files
set viminfo='10,\"100,:20,%,n~/.viminfo
function! ResCur()
  if line("'\"") <= line("$")
    normal! g`"
    return 1
  endif
endfunction

augroup resCur
  autocmd!
  autocmd BufWinEnter * call ResCur()
augroup END

" source $VIMRUNTIME/vimrc_example.vim
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

" Vim initialisation file (Unix version: ".vimrc")
" 
" Maintainer:	Mario Schweigler <ms44@kent.ac.uk>
" Last Change:	23 April 2003
" https://www.informatico.de/kent-vim-ext/

"{{{  Colour scheme (must be done first!)
colorscheme nirvana
" colorscheme delek
"}}}

" {{{ Look & Feel
" Filetype detection on
filetype on
" Filetype plugin on
filetype plugin on
" Syntax highlighting on
syntax on
" Show line number
set number
" Show command
set showcmd
" Show current mode
set showmode
" Show ruler in status line
set ruler
" Wrap lines at line break
set wrap
set linebreak
let &breakat = ' 	'
" Show status line for all files
set laststatus=2
" Enable autoindent
set autoindent
" Enable filetype indent
filetype indent on
" Ignore case on pure lower case search patterns
set ignorecase
set smartcase
" Increment search as you type search pattern
set incsearch
" Highlight search patterns
set hlsearch
" Show matching brackets
set showmatch
set matchtime=3
" Create backup files
set backup
" Add $TEMP directory to swap file directory list
if exists('$TEMP')
  let &directory = &directory . ',' . $TEMP
endif
" }}}

"{{{  Call external runtime files
" Some improvements
runtime kent-improve.vim
" Entering folds and other fold improvements
runtime kent-folding.vim
" Programming scripts
runtime kent-programming.vim
" Occam scripts
runtime kent-occam.vim 
"}}}

