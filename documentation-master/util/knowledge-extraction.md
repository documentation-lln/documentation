# Extracting knowledge from a LaTeX document

Some of our documents are referred to as "*living documents*", meaning that their rationale is not restricted to
a very limited amount of time. A document such as the Accelerator TDR is meant to "live" for years, and describe
the state and context of a long-lasting project such as MYRRHA throughout all its phases.

Such a peculiar characteristic led to the choice of LaTeX -- an open markup language for the writing
of scientific documents. In fact, the use of an open "standard" such as LaTeX allows the user to have
access to all data structures collected by the system while compiling the sources into the output
document. The hierarchical structure of the document; the location of sections, images, and tables;
the bibliography records embedded in the final document; and much, much more, is available to the LaTeX
user in convenient forms.

In order to support the processing of this information, I have been designing
a number of tools, which I call the *knowledge extractors*. Mostly written in
[Flex](https://en.wikipedia.org/wiki/Flex_(lexical_analyser_generator)) and C, such tools read by-products
of LaTeX compilations and produce data structures filled in with useful information. Other tools invoke
the knowledge extractors and use the resulting information to provide higher-level services.

Obviously, it is the *living* nature of documents such as the Accelerator TDR that justify the design of
the above sketched strategy.  Manual processing would be error-prone and require considerable repeated
effort. A machine-driven approach allows to guarantee correctness and reliable synchronization. In what
follows, I will describe the currently available knowledge extractors and exemplify their use showing
how I designed three higher-level services:

- A tool to partition the PDF of the TDR into PDF chapters.
- A tool that creates a document summarizing the location of all images used in the TDR, with
  thumbnail-hyperlinks pointing to corresponding images in the Git master branch of the TDR.
- A tool that creates a document with all the tables used in the TDR.


## partition-pdf.sh
Here is the source of `partition-pdf.sh`:

```
./sectionpages < MINERVA-100MeV-ACCELERATOR-TDR.toc > s-pages.h
gcc -o s-pages  s-pages.c
./s-pages MINERVA-100MeV-ACCELERATOR-TDR.pdf
./s-pages.sh
cp chapters/* ~/c/Users/vdflorio/AppData/Roaming/OpenText/OTEdit/EC_sckcen/c40793049/
```

The tool first invokes `sectionpages` to process the `.toc` byproduct of the TDR compilation. The output
goes into a local header files, `s-pages.h`, which is compiled with a C module called `s-pages.c`. The
resulting executable, `s-pages`, is invoked with the TDR as argument so as to create the shell script
that actually produces the chapters: `s-pages.sh`. After its invoking, the partitioned chapters are
copied onto the directory monitored by the Alexandria services, and from there will be automatically
uploaded onto Alexandria.

### sectionpages
Sectionpages is a Lex parser that "packages" a static array with the starting page of the TDR sections.

![](sectionpages.png)

Option `-t` means "take as input the TDR `.toc` by-product`". By default the tool outputs a C data
structure, though it is possible to output a Python dictionary via option `-p`.



## list-of-figures-and-paths.sh

The script of `list-of-figures-and-paths.sh` is similar, though a little more complex:

```
./generate-fls
input=`cat MINERVA-100MeV-ACCELERATOR-TDR.fls|grep '[AT][nD][nR][e-][xAB]'|uniq|awk '{printf "%s ", $2}'`
./figurepages < MINERVA-100MeV-ACCELERATOR-TDR.lof > pages.h
flex captions.l
gcc -L../lib -I../include -o captions lex.yy.c -lfl -lvdfio
cat $input | captions -o thumbnails.tex -v -l && rubber --pdf thumbnails
gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress -dNOPAUSE -dQUIET \
   -dBATCH -sOutputFile=compressed_thumbnails.pdf thumbnails.pdf
cp compressed_thumbnails.pdf \
   ~/c/Users/vdflorio/AppData/Roaming/OpenText/OTEdit/EC_sckcen/c40593146/thumbnails.pdf
```

Here we first generate an optional by-product, `MINERVA-100MeV-ACCELERATOR-TDR.fls` and then we use it
to derive a list of all the modules input by the TDR. This goes in shell variable `input`.
Then we compile knowledge extractor `captions` by invoking Lex on `captions.l` and then gcc.
`captions` is then fed with the modules in `input`. Output goes into LaTeX source `thumbnails.tex`,
which is compiled into `thumbnails.pdf`. The latter is compressed and then stored in the directory monitored by
the Alexandria services, and from there will be automatically uploaded onto Alexandria.


## list-of-tables.sh
This script is very similar to `list-of-figures-and-paths.sh`:

```
./generate-fls
input=`cat MINERVA-100MeV-ACCELERATOR-TDR.fls|grep '[AT][nD][nR][e-][xAB]'|uniq|awk '{printf "%s ", $2}'`
cp standard_tex_prologue.tex tmp_prologue.tex
fgres 'List of figures, with thumbnails, captions, and paths' 'TDR Tables' tmp_prologue.tex
input="tmp_prologue.tex $input"
make gettables
cat $input | ./gettables > tables-in-the-TDR.tex
rubber --pdf tables-in-the-TDR.tex
```

As done in `list-of-figures-and-paths.sh`, we generate file `MINERVA-100MeV-ACCELERATOR-TDR.fls` and then use it
to derive a list of all the modules input by the TDR. This goes in shell variable `input`.
Then we create a LaTeX prologue by modifying `standard_tex_prologue.tex`.
Then we compile knowledge extractor `gettables` by invoking `make`.
`gettables` is then fed with the modules in `input`. Output goes into LaTeX source `tables-in-the-TDR.tex`,
which is compiled into `tables-in-the-TDR.pdf`.

The `list-of-tables.sh` script also invokes `latex2rtf` to perform a very crude conversion of the LaTeX tables into rich text format. The latter can then be read by Word. The result is not always clean, though...
