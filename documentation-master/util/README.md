# Utility folder

Here I collect a number of tools that I find useful. Or that I wrote myself 'cause I needed them \^\_\^

## Createx
One such tool is _createx_. By running 

    $ createx $NAME

where $NAME stands for "any valid LaTeX filename", you create
in the current directory a set of mini-scripts to facilitate the processing of LaTeX file $NAME.tex
A file $NAME.tex is also created, or updated with a LaTeX prologue:

```
$ createx prova
The following files have been created: [vpuce].
Usage:  => v <= Edits prova.tex
        => p <= Processes prova.tex and creates prova.pdf
        => e <= Renders prova.pdf
        => c <= Compresses prova.pdf into prova-compressed.pdf
        => u <= git commit prova, Uploads prova.tex on l2.sck.be and in D:\Documents
The following file has been updated: prova.tex.
vdflorio@vdflorio-VirtualBox:~/tmp$ cat prova.tex 
% Beginning of file prova.tex
\documentclass{article}
\usepackage{latexsym}
\usepackage{amsmath}
\usepackage{amssymb}
%\usepackage{makeidx}
\usepackage{graphicx}
\usepackage[pdftex,
            pagebackref=true,
            colorlinks=true,
            linkcolor=blue
           ]{hyperref}
\usepackage{float}
\usepackage{alltt}
\begin{document}
%\centerline{\includegraphics[width=0.84\textwidth]{fname.png}}


\end{document}

% End of file prova.tex
vdflorio@vdflorio-VirtualBox:~/tmp$ 

```

I plan to enrich that LaTeX prologue with the packages and tools that I commonly use.

More information will follow...

## Conditional include of LaTeX files
Some while ago, Angélique asked me for a LaTeX solution for the conditional inclusion of a LaTeX file.
This can be done via the following steps:

- You set an environment variable with the name of file to be included. For instance you may have

  ```
      $ export MYINCLUDE='myinclude1.tex'
  ```

- Then you place the varinput.sh shell script in your local bin or any other place mentioned in your PATH.
  varinput.sh simply that types out the contents of the files whose names are in one or more environment variables
  mentioned in the command line. As an example,

  ```
      $ ./varinput.sh '$MYINCLUDE'
  ```

  types out the contents of file myinclude1.tex.

- Then in the preamble of the LaTeX code, I define the following command:

  ```
    \newcommand{\varinput}[1]{\immediate\input|"./varinput.sh #1"\unskip}
  ```

That's it.  Now, in your LaTeX source, you can use for instance

```
    \varinput{'$MYINCLUDE'}
```

to include the LaTeX file whose name is in environment variable MYINCLUDE. If no such variable exists, nothing is included.

Oh, I had nearly forgotten -- when compiling your LaTeX source you need to also specify "--shell-escape".

The shell script [`varinput.sh`](https://git.myrrha.lan/vdflorio/documentation/blob/master/util/VarInclude/varinput.sh) and a test file [`mytest.tex`](https://git.myrrha.lan/vdflorio/documentation/blob/master/util/VarInclude/mytest.tex) can be found in the [VarInclude](VarInclude) folder.


## Printing out all comments in a PDF file

There are cases where you want to print out all comments in a PDF file -- for instance, to create
a document in which you reply to each of those comments. To do this, you could use [`catpdfcomments`](https://git.myrrha.lan/vdflorio/documentation/-/blob/master/util/catpdfcomments).
That is a tool that takes as input one or more PDF and creates a LaTeX file that lists all comments.
At the end, it also prints some simple statistics.

![](catpdfcomments.png)
*Example of usage of `catpdfcomments`.*

![](pdfannotations.png)
*Output document produced by `catpdfcomments MINERVA-100MeV-ACCELERATOR-TDR.pdf`.*

## Git commit and push
Two very simple scripts have been created:
- `co` commits the mentioned files, or all changed files if no argument is passed;
- `pu` is a git push plus some extra work that only makes sense on my machine:
   -- it connects to the "right" network, specifies the "right" DNS server, `git push`es, and then connects back to the default network on my machine


## Knowledge extraction tools

[Please visit this node](knowledge-extraction.md)

## captions

Suppose you want to reuse one of the image of the TDR. A first question might then be: which file should I use, and
how may I locate it in the Git repository? I think the easiest way to do so is by using a tool that I've just written.
The tool first invokes `pdflatex` with option `-remember`, which produces a list of all the files included while
compiling the TDR. After that, the file is filtered out and then fed into a Lex analyzer called `captions`. The
result is stored in a file called `captions.txt`. To execute all the above actions, just call `list-of-figures-and-paths.sh`:

![](captions.png)

As from version 0.4, it is possible to call `captions` with option `-l`. Said option produces a LaTeX files with thumbnails
for all pictures! The option has been integrated in `list-of-figures-and-paths.sh`, which now creates a `thumbnails.pdf`
file.

![](captions-thumbnails.png)

# lagrep

Suppose you want to look for a string in a LaTeX file, though only when the string is not part of a comment.
How would you do that? My answer is, use `lagrep`. `lagrep` cleans up your input and then pipes the result onto your search module.
So if you want to look for string Pirani in all LaTeX files in your current directory, instead of using

    fgrep Pirani *.tex

as done here:

![](searching-with-grep.png)

you could use

    lagrep Pirani *tex

as it is done here:

![](searching-with-lagrep.png)

As you can see, only two of the lines selected by `fgrep` are printed by `lagrep`, namely the ones that are not commented out.
