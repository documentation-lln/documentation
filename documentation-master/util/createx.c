#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv)
{
	FILE *f;
	char basename[128];

	if (argc == 1)
	{
		printf("Please insert the base name\n");
		scanf("%s", basename);
	}
	else
		strcpy(basename, argv[1]);

	if (argc > 2)
	{
		fprintf(stderr,
			"%s: Warning: Too many args -- ignoring all but `%s'\n",
			argv[0], argv[1]);
	}

	f = fopen("v", "w");
	fprintf(f, "vim \"%s.tex\"\n", basename);
	fclose(f);

	f = fopen("p", "w");
	fprintf(f, "rubber --pdf \"%s.tex\"\n", basename);
	fclose(f);

	f = fopen("u", "w");
	fprintf(f, "git commit \"%s\".tex\nscp \"%s.tex\" vdflorio\\@sck.be@l2.sck.be:Documents\ncp \"%s\".tex ~/d/Documents/\n", basename, basename, basename);
	fclose(f);

	f = fopen("c", "w");
	fprintf(f, "gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.6 -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH -sOutputFile=\"%s-compressed.pdf\" \"%s\".pdf\n", basename, basename);
	fclose(f);

	f = fopen("a", "w");
	fprintf(f, "evince \"%s\".pdf &\n", basename);
	fclose(f);

	system("chmod +x v p u c e");
	printf("The following files have been created: [vpuce].\n");
	printf("Usage:	=> v <= Edits %s.tex\n", basename);
	printf("    	=> p <= Processes %s.tex and creates %s.pdf\n", basename, basename);
	printf("    	=> a <= visuAlize %s.pdf\n",  basename);
	printf("    	=> c <= Compresses %s.pdf into %s-compressed.pdf\n", basename, basename);
	printf("    	=> u <= git commit %s, Uploads %s.tex on l2.sck.be and in D:\\Documents\n", basename, basename);

	strcat(basename, ".tex");
	f = fopen(basename, "a");
	if (fseek(f, 0L, SEEK_SET) != 0)
	{
		fprintf(stderr, "fseek failed\n");
		perror("fseek");
	}
	fprintf(f, "%% Beginning of file %s\n", basename);
	fprintf(f, "\\documentclass{article}\n");
	fprintf(f, "\\usepackage{latexsym}\n");
	fprintf(f, "\\usepackage{amsmath}\n");
	fprintf(f, "\\usepackage{amssymb}\n");
	fprintf(f, "%%\\usepackage{makeidx}\n");
	fprintf(f, "\\usepackage{graphicx}\n");
	fprintf(f, "\\usepackage[pdftex,\n");
	fprintf(f, "            pagebackref=true,\n");
	fprintf(f, "            colorlinks=true,\n");
	fprintf(f, "            linkcolor=blue\n");
	fprintf(f, "           ]{hyperref}\n");
	fprintf(f, "\\usepackage{float}\n");
	fprintf(f, "\\usepackage{alltt}\n");
	fprintf(f, "\\begin{document}\n");
	fprintf(f, "%%\\centerline{\\includegraphics[width=0.84\\textwidth]{fname.png}}\n");
	fprintf(f, "\n\n\\end{document}\n\n");
	fprintf(f, "%% End of file %s\n", basename);
	fclose(f);

	printf("The following file has been updated: %s.\n", basename);
}
